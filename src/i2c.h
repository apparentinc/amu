/**
* \file
*
* \brief Apparent I2C Definitions.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _I2C_H_
#define _I2C_H_

#define I2C_DATA_TYPE_BYTE             1
#define I2C_DATA_TYPE_WORD             2


#define I2C_DEVICE_TYPE_POE            0
#define I2C_DEVICE_TYPE_FAN            1
#define I2C_DEVICE_TYPE_MAX            1


// To support accessing the correct register type based on device type:
typedef struct s_i2cRegisterMapElement
{
    U32  control;
    U32  masterMode;
    U32  masterModeSetup;
    U32  iadr;
    U32  status;
    U32  rcvHolding;
} t_i2cRegisterMapElement;



// External Function Prototypes
BOOL readI2cStatusUntilReady      (U32 statusAddress, U32 bitSelector);
BOOL i2cReadGenericData           (U32 deviceType,    U32 command, U32 dataType, U16 *pData);
BOOL i2cWriteGenericData          (U32 command,       U32 data,    U32 dataType);
BOOL i2cSuperReadGenericData      (U32 deviceType,    U32 command, U32 *pData);

 
/*----------------------------------------------------------------------------*/
#endif   /* _I2C_H_ */
