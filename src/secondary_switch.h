/**
* \file
*
* \brief Apparent Application Definitions for the "SECONDARY" KSZ8795 Switch Controller.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _SECONDARY_SWITCH_H_
#define _SECONDARY_SWITCH_H_


// From the KSZ8795CLX data sheet (page 47 onwards), the following SECONDARY switch controller registers are defined.
//
// Most do not apply to the Apparent application.

// Register 0 (0x00): Chip ID0
// Read only.
#define GLOBAL_REGISTER_CHIP_ID_FAMILY_REG_00_ADDR         0x00     // register number 00 address
#define GLOBAL_REGISTER_CHIP_ID_FAMILY_REG_00_VALUE        0x87
#define GLOBAL_REGISTER_CHIP_ID_FAMILY_REG_00_DEFAULT      GLOBAL_REGISTER_CHIP_ID_FAMILY_REG_00_VALUE


// Register 1 (0x01): Chip ID1/Start Switch
// This register contains the "start the switch function of the chip" bit[0] 
// bits [7:4] = read only = [1 0 0 1] = 0x90 indicating the 8795 family
// bits [3:1] = read only = [0 0 0]   = 0x00 indicating the revision ID
// bit [0]    = R/W : 1 = START switch function, 0 = STOP switch function
//
// By default, ethernet service is enabled, so "start switch" bit[0] is to be SET.

#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_ADDR        0x01     // register number 01 address

#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_CHIP_ID     0x90
#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_REVISION_ID 0x00
#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_MASK  0x01

#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_DEFAULT        GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_MASK  | (GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_CHIP_ID | GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_REVISION_ID)
#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_SWITCH   GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_MASK  | (GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_CHIP_ID | GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_REVISION_ID)
#define GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_STOP_SWITCH  (~GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_MASK) & (GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_CHIP_ID | GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_REVISION_ID)




// Register 2 (0x02): Global Control 0
// - bit[7]: "New Back-Off Enable". 1 = enable, 0 = disable. It is to be disabled so CLEAR the bit.
// - bit[6]: "Global Soft Reset Enable". 1 = enable, 0 = disable. DEFAULT = 0. To reset the controller: SET the bit - it is self-clearing. <- THE ONLY ONE OF INTEREST IN THE APPARENT APPLICATION.
// - bit[5]: "Flush Dynamic MAC Table". 1 = trigger the flush, 0 = normal operation. So CLEAR the bit.
// - bit[4]: "Flush Static MAC Table".  1 = trigger the flush, 0 = normal operation. So CLEAR the bit.
// - bit[3]: reserved = 0.
// - bit[2]: reserved = 0.
// - bit[1]: "UNH Mode".        Leave this bit CLEAR = 0.
// - bit[0]: "Link Change Age". Leave this bit CLEAR = 0.

#define GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR             0x02     // register number 02 address

#define GLOBAL_REGISTER_02_NEW_BACKOFF_DISABLE_DEFAULT     0x00
#define GLOBAL_REGISTER_02_FLUSH_DYNAMIC_MAC_DEFAULT       0x00
#define GLOBAL_REGISTER_02_FLUSH_STAIC_MAC_DEFAULT         0x00
#define GLOBAL_REGISTER_02_RESERVED_BIT_3_DEFAULT          0x00
#define GLOBAL_REGISTER_02_RESERVED_BIT_2_DEFAULT          0x00
#define GLOBAL_REGISTER_02_UNH_MODE_DEFAULT                0x00
#define GLOBAL_REGISTER_02_LINK_CHANGE_AGE_DEFAULT         0x00

#define GLOBAL_REGISTER_02_GENERAL_DEFAULT                (GLOBAL_REGISTER_02_NEW_BACKOFF_DISABLE_DEFAULT | GLOBAL_REGISTER_02_FLUSH_DYNAMIC_MAC_DEFAULT | GLOBAL_REGISTER_02_FLUSH_STAIC_MAC_DEFAULT    \
                                                         | GLOBAL_REGISTER_02_RESERVED_BIT_3_DEFAULT      | GLOBAL_REGISTER_02_RESERVED_BIT_2_DEFAULT    | GLOBAL_REGISTER_02_UNH_MODE_DEFAULT           \
                                                         | GLOBAL_REGISTER_02_LINK_CHANGE_AGE_DEFAULT)
#define GLOBAL_SOFT_RESET_ENABLE_REG_02_ENABLE_MASK        0x40      // 0 1 0 0 0 0 0 0 = 0x40 bit[6]
#define GLOBAL_SOFT_RESET_ENABLE_REG_02_DEFAULT            (~GLOBAL_SOFT_RESET_ENABLE_REG_02_ENABLE_MASK) & GLOBAL_REGISTER_02_GENERAL_DEFAULT


// Port registers. Only those registers accessed by the Apparent application are defined.

//
// Transmit and receive link is enabled or disabled registers:
// Registers 18/34/50/55/82 for "downstream ports 5/6/7/8".
// Port 5 (RGMII) is not RESERVED, but not not defined.
// - bit [2]: 1 = enable packet transmission, 0 = disable packet transmission
// - bit [1]: 1 = enable packet reception,    0 = disable packet reception
#define PORT_REGISTER_PORT_1_ADDRESS_CONTROL_2             0x12     // Register number 18
#define PORT_REGISTER_PORT_2_ADDRESS_CONTROL_2             0x22     // Register number 34
#define PORT_REGISTER_PORT_3_ADDRESS_CONTROL_2             0x32     // Register number 50
#define PORT_REGISTER_PORT_4_ADDRESS_CONTROL_2             0x42     // Register number 66

#define PORT_CONTROL_2_ENABLE_DISABLE_TRANSMIT_MASK        0x04      // bit [2] = 00000100 = 0x04
#define PORT_CONTROL_2_ENABLE_RECEIVE_RECEIVE_MASK         0x02      // bit [1] = 00000010 = 0x02
#define PORT_CONTROL_2_ENABLE_DISABLE_TRANSMIT_ENABLE      0x04      // bit [2] is SET
#define PORT_CONTROL_2_ENABLE_DISABLE_TRANSMIT_DISABLE     0x00      // bit [2] is CLEAR
#define PORT_CONTROL_2_ENABLE_DISABLE_RECEIVE_ENABLE       0x02      // bit [1] is SET
#define PORT_CONTROL_2_ENABLE_DISABLE_RECEIVE_DISABLE      0x00      // bit [1] is CLEAR
#define PORT_CONTROL_2_DEFAULT                             PORT_CONTROL_2_ENABLE_DISABLE_TRANSMIT_ENABLE | PORT_CONTROL_2_ENABLE_DISABLE_RECEIVE_ENABLE
//


// PORT 1/2/3/4 "Status 1" registers:
// Registers 25/41/57/73 for "downstream ports 5/6/7/8"
// These are status registers, so READ ONLY. Exception: HP_MDIX bit[7]
// that must be configured as part of the implementation of the errata.
// Port 5 is RESERVED, so it is not defined.
#define PORT_REGISTER_PORT_1_ADDRESS_STATUS_1              0x19     // Register number 25
#define PORT_REGISTER_PORT_2_ADDRESS_STATUS_1              0x29     // Register number 41
#define PORT_REGISTER_PORT_3_ADDRESS_STATUS_1              0x39     // Register number 57
#define PORT_REGISTER_PORT_4_ADDRESS_STATUS_1              0x49     // Register number 73

#define PORT_STATUS_1_HP_MDIX_MASK                         0x80      // bit [7]    = 10000000 = 0x80
#define PORT_STATUS_1_HP_MDIX_HP_MODE                      0x80      // bit [7]  is SET   = 1: HP Auto MDI/MDIX Mode
#define PORT_STATUS_1_HP_MDIX_MICROCHIP_MODE               0x00      // bit [7]  is CLEAR = 0: Microchip Auto MDI/MDIX Mode
#define PORT_STATUS_1_HP_MDIX_DEFAULT                      PORT_STATUS_1_HP_MDIX_MICROCHIP_MODE


// PHY Control:
#define PORT_REGISTER_PORT_1_PHY_CONTROL_8                 0x1A     // Register number 26
#define PORT_REGISTER_PORT_2_PHY_CONTROL_8                 0x2A     // Register number 42
#define PORT_REGISTER_PORT_3_PHY_CONTROL_8                 0x3A     // Register number 58
#define PORT_REGISTER_PORT_4_PHY_CONTROL_8                 0x4A     // Register number 74
#define PORT_REGISTER_RESERVED_PHY_CONTROL                 0x5A     // Register number 90

// Link MD:
#define PORT_REGISTER_PORT_1_LINKMD_RESULT                 0x1B     // Register number 27
#define PORT_REGISTER_PORT_2_LINKMD_RESULT                 0x2B     // Register number 43
#define PORT_REGISTER_PORT_3_LINKMD_RESULT                 0x3B     // Register number 59
#define PORT_REGISTER_PORT_4_LINKMD_RESULT                 0x4B     // Register number 75
#define PORT_REGISTER_RESERVED_LINKMD_RESULT               0x5B     // Register number 91


// Auto-negotiate, forced speed, forced duplex registers:
// Registers 28/44/60/76 for "downstream ports 5/6/7/8".
// Port 5 is RESERVED, so it is not defined.
// - bit [7]: 1 = disable auto-negotiate
// - bit [6]: 1 = force to 100BASE_T
// - bit [5]: 1 = force full duplex
// - bits [4:0]: reserve bits left to their 0x1F default
#define PORT_REGISTER_PORT_1_ADDRESS_CONTROL_9             0x1C     // Register number 28
#define PORT_REGISTER_PORT_2_ADDRESS_CONTROL_9             0x2C     // Register number 44
#define PORT_REGISTER_PORT_3_ADDRESS_CONTROL_9             0x3C     // Register number 60
#define PORT_REGISTER_PORT_4_ADDRESS_CONTROL_9             0x4C     // Register number 76

#define PORT_CONTROL_9_AUTO_NEGOTIATE_DISABLE_MASK         0x80      // bit [7]    = 10000000 = 0x80    -> auto-negotiate disabled
#define PORT_CONTROL_9_AUTO_NEGOTIATE_DISABLE              0x80      // bit [7] is SET
#define PORT_CONTROL_9_AUTO_NEGOTIATE_ENABLE               0x00      // bit [7] is CLEAR
#define PORT_CONTROL_9_AUTO_NEGOTIATE_DEFAULT             PORT_CONTROL_9_AUTO_NEGOTIATE_DISABLE

#define PORT_CONTROL_9_FORCE_100_BASE_T_MASK               0x40      // bit [6]    = 01000000 = 0x40    -> force to 100BaseT
#define PORT_CONTROL_9_FORCE_100_BASE_T_ENABLE             0x40      // bit [6] is SET
#define PORT_CONTROL_9_FORCE_100_BASE_T_DISABLE            0x00      // bit [6] is CLEAR
#define PORT_CONTROL_9_FORCE_100_BASE_T_DEFAULT            PORT_CONTROL_9_FORCE_100_BASE_T_ENABLE

#define PORT_CONTROL_9_FORCE_FULL_DUPLEX_MASK              0x20      // bit [5]    = 00100000 = 0x20    -> full duplex
#define PORT_CONTROL_9_FORCE_FULL_DUPLEX_ENABLE            0x20      // bit [5]  is SET
#define PORT_CONTROL_9_FORCE_FULL_DUPLEX_DISABLE           0x00      // bit [5]  is CLEAR
#define PORT_CONTROL_9_FORCE_FULL_DUPLEX_DEFAULT           PORT_CONTROL_9_FORCE_FULL_DUPLEX_ENABLE

#define PORT_CONTROL_9_RESERVED_MASK                       0x1F      // bits [4:0] = 00011111 = 0x1F    -> reserved

#define PORT_CONTROL_9_DEFAULT                             PORT_CONTROL_9_AUTO_NEGOTIATE_DEFAULT        \
                                                         | PORT_CONTROL_9_FORCE_100_BASE_T_DEFAULT      \
                                                         | PORT_CONTROL_9_FORCE_FULL_DUPLEX_DEFAULT     \
                                                         | PORT_CONTROL_9_RESERVED_MASK
// End of Auto-negotiate, forced speed, forced duplex register.


// PORT POWER DOWN registers:
// Registers 29/45/61/77 for "port power down of downstream ports 5/6/7/8":
// Port 5 is RESERVED, so it is not defined.
// "Control 11/status 3" contains soft reset bit[4], Force Link bit[3]

#define PORT_REGISTER_PORT_1_ADDRESS_CONTROL_10            0x1D     // Register number 29
#define PORT_REGISTER_PORT_2_ADDRESS_CONTROL_10            0x2D     // Register number 45
#define PORT_REGISTER_PORT_3_ADDRESS_CONTROL_10            0x3D     // Register number 61
#define PORT_REGISTER_PORT_4_ADDRESS_CONTROL_10            0x4D     // Register number 77

#define PORT_CONTROL_10_TXDIS_TRANSMITTER_MASK             0x40     // bit[6] = 01000000 = 0x40
#define PORT_CONTROL_10_TXDIS_TRANSMITTER_DISABLE          0x40     // bit[6] is SET
#define PORT_CONTROL_10_TXDIS_TRANSMITTER_ENABLE           0x00     // bit[6] is CLEAR
#define PORT_CONTROL_10_TXDIS_TRANSMITTER_DEFAULT          PORT_CONTROL_10_TXDIS_TRANSMITTER_ENABLE

#define PORT_CONTROL_10_AUTO_NEGOTIATION_RESTART_MASK      0x20     // bit[5] = 00100000 = 0x20
#define PORT_CONTROL_10_AUTO_NEGOTIATION_RESTART           0x20     // bit[5] is SET
#define PORT_CONTROL_10_AUTO_NEGOTIATION_NORMAL_OP         0x00     // bit[5] is CLEAR
#define PORT_CONTROL_10_AUTO_NEGOTIATION_DEFAULT           PORT_CONTROL_10_AUTO_NEGOTIATION_NORMAL_OP

#define PORT_CONTROL_10_POWER_MASK                         0x08     // bit[3] = 00001000 = 0x08
#define PORT_CONTROL_10_POWER_DOWN                         0x08     // bit[3] is SET
#define PORT_CONTROL_10_POWER_NORMAL_OPERATION             0x00     // bit[3] is CLEAR
#define PORT_CONTROL_10_POWER_DEFAULT                      PORT_CONTROL_10_POWER_NORMAL_OPERATION

#define PORT_CONTROL_10_AUTO_MDI_MDIX_MASK                 0x04     // bit[2] = 00000100 = 0x04
#define PORT_CONTROL_10_AUTO_MDI_MDIX_DISABLE              0x04     // bit[2] is SET
#define PORT_CONTROL_10_AUTO_MDI_MDIX_ENABLE               0x00     // bit[2] is CLEAR
#define PORT_CONTROL_10_AUTO_MDI_MDIX_DEFAULT              PORT_CONTROL_10_AUTO_MDI_MDIX_DISABLE

#define PORT_CONTROL_10_FORCED_MDI_MODE_MASK               0x02     // bit[1] = 00000010 = 0x02
#define PORT_CONTROL_10_FORCED_MDI_MODE_DISABLED           0x02     // bit[1] is SET
#define PORT_CONTROL_10_FORCED_MDI_MODE_ENABLED            0x00     // bit[1] is CLEAR
#define PORT_CONTROL_10_FORCED_MDI_MODE_DEFAULT            PORT_CONTROL_10_FORCED_MDI_MODE_DISABLED

#define PORT_CONTROL_10_MAC_LOOPBACK_MASK                  0x01     // bit[0] = 00000001 = 0x01
#define PORT_CONTROL_10_MAC_LOOPBACK_LOOPBACK              0x01     // bit[0] is SET
#define PORT_CONTROL_10_MAC_LOOPBACK_NORMAL_OPERATION      0x00     // bit[0] is CLEAR
#define PORT_CONTROL_10_MAC_LOOPBACK_DEFAULT               PORT_CONTROL_10_MAC_LOOPBACK_NORMAL_OPERATION

#define PORT_CONTROL_10_DEFAULT                            PORT_CONTROL_10_TXDIS_TRANSMITTER_DEFAULT    \
                                                         | PORT_CONTROL_10_AUTO_NEGOTIATION_DEFAULT     \
                                                         | PORT_CONTROL_10_POWER_DEFAULT                \
                                                         | PORT_CONTROL_10_AUTO_MDI_MDIX_DEFAULT        \
                                                         | PORT_CONTROL_10_FORCED_MDI_MODE_DEFAULT      \
                                                         | PORT_CONTROL_10_MAC_LOOPBACK_DEFAULT
// End of PORT POWER DOWN register.


// LINK and AUTO-NEGOTIATE STATUS registers:
// Registers 30/46/62/78/94 for "downstream ports 5/6/7/8":
// Port 5 is RESERVED, so it is not defined.
// "status 2" contains auto-negotiation done bit[6], Link Good bit[5] 
#define PORT_REGISTER_PORT_1_ADDRESS_STATUS_2              0x1E     // Register number 30
#define PORT_REGISTER_PORT_2_ADDRESS_STATUS_2              0x2E     // Register number 46
#define PORT_REGISTER_PORT_3_ADDRESS_STATUS_2              0x3E     // Register number 62
#define PORT_REGISTER_PORT_4_ADDRESS_STATUS_2              0x4E     // Register number 78

#define PORT_STATUS_2_MDIX_STATUS_MASK                     0x80     // bit[7] = 10000000 = 0x80
#define PORT_STATUS_2_MDIX_STATUS_CONNECTED                0x80     // bit[7] is SET
#define PORT_STATUS_2_MDIX_STATUS_DISCONNECTED             0x00     // bit[7] is CLEAR

#define PORT_STATUS_2_AUTO_NEGOTIATION_MASK                0x40     // bit[6] = 01000000 = 0x40
#define PORT_STATUS_2_AUTO_NEGOTIATION_DONE                0x40     // bit[6] is SET
#define PORT_STATUS_2_AUTO_NEGOTIATION_NOT_DONE            0x00     // bit[6] is CLEAR

#define PORT_STATUS_2_LINK_STATUS_MASK                     0x20     // bit[5] = 00100000 = 0x20
#define PORT_STATUS_2_LINK_STATUS_LINK_UP                  0x20     // bit[5] is SET
#define PORT_STATUS_2_LINK_STATUS_LINK_DOWN                0x00     // bit[5] is CLEAR

// End of LINK and AUTO-NEGOTIATE STATUS registers.


// SOFT RESET and FORCE LINK STATUS registers:
// Registers 31/47/63/79 for "downstream ports 5/6/7/8":
// Port 5 is RESERVED, so it is not defined.
// "Control 11/status 3" contains soft reset bit[4], Force Link bit[3]
#define PORT_REGISTER_PORT_1_ADDRESS_CONTROL_11_STATUS_3   0x1F     // Register number 31
#define PORT_REGISTER_PORT_2_ADDRESS_CONTROL_11_STATUS_3   0x2F     // Register number 47
#define PORT_REGISTER_PORT_3_ADDRESS_CONTROL_11_STATUS_3   0x3F     // Register number 63
#define PORT_REGISTER_PORT_4_ADDRESS_CONTROL_11_STATUS_3   0x4F     // Register number 79

#define PORT_CONTROL_11_STATUS_3_PHY_LOOP_BACK_MASK        0x80     // bit[7] = 10000000 = 0x80
#define PORT_CONTROL_11_STATUS_3_PHY_LOOP_BACK_ENABLE      0x80     // bit[7] is SET
#define PORT_CONTROL_11_STATUS_3_PHY_LOOP_BACK_NORMAL_OP   0x00     // bit[7] is CLEAR

#define PORT_CONTROL_11_STATUS_3_SOFT_RESET_MASK           0x10     // bit[4] = 00010000 = 0x10
#define PORT_CONTROL_11_STATUS_3_PHY_RESET                 0x10     // bit[4] is SET
#define PORT_CONTROL_11_STATUS_3_PHY_NORMAL_OPERATION      0x00     // bit[4] is CLEAR

#define PORT_CONTROL_11_STATUS_3_FORCE_LINK_MASK           0x08     // bit[3] = 000001000 = 0x08
#define PORT_CONTROL_11_STATUS_3_FORCE_LINK_IN_PHY         0x08     // bit[3] is SET
#define PORT_CONTROL_11_STATUS_3_LINK_NORMAL_OPERATION     0x00     // bit[3] is CLEAR

#define PORT_CONTROL_11_STATUS_3_PORT_OPERATION_MODE_MASK  0x07     // bits[2..0] = 00000111 = 0x07

#define PORT_CONTROL_11_STATUS_3_DEFAULT                   PORT_CONTROL_11_STATUS_3_PHY_LOOP_BACK_NORMAL_OP | PORT_CONTROL_11_STATUS_3_PHY_NORMAL_OPERATION | PORT_CONTROL_11_STATUS_3_LINK_NORMAL_OPERATION

// End of SOFT RESET and FORCE LINK STATUS registers.


// MAC.
//
// From the data sheet (page 64): Registers 104 to 109 define the switching engine�s MAC address. This 48-bit address is used as
// the source address in MAC pause control frames. Use Registers 110 and 111 to read or write data to the static MAC address table,
// LAN table, dynamic address table, PME registers, ACL tables, EEE registers and the MIB counters.
#define SECONDARY_MAC0_REG_68    0x68 // Register number 104 (0x68): MAC Address Register 07 - 0 MACA[47:40] � R/W 0x00
#define SECONDARY_MAC1_REG_69    0x69 // Register number 105 (0x69): MAC Address Register 17 - 0 MACA[39:32] � R/W 0x10
#define SECONDARY_MAC2_REG_6A    0x6A // Register number 106 (0x6A): MAC Address Register 27 - 0 MACA[31:24] � R/W 0xA1
#define SECONDARY_MAC3_REG_6B    0x6B // Register number 107 (0x6B): MAC Address Register 37 - 0 MACA[23:16]
#define SECONDARY_MAC4_REG_6C    0x6C // Register number 108 (0x6C): MAC Address Register 47 - 0 MACA[15:8]
#define SECONDARY_MAC5_REG_6D    0x6D // Register number 109 (0X6D): MAC Address Register 57 - 0 MACA[7:0]

// A small set of control registers 0x80-0x87: "Global Control registers 12 - 19 is specified,
// 67 - 70. They are very low-level controls, not applicable to the Apparent application.

// Another set of port control registers are defined, not applicable to the Apparent application:
//
// 0xB0-0xBE: Port 1 control registers
// 0xC0-0xCE: Port 2 control registers
// 0xD0-0xDE: Port 3 control registers
// 0xE0-0xEE: Port 4 control registers
// 0xF0-0xFE: Port 5 control registers
//
// where: 0xB0/C0/D0/E0/F0 = port 1/2/3/4/5 Control 12                         register number 176/192/208/224/240
//        0xB1/C1/D1/E1/F1 = port 1/2/3/4/5 Control 13                         register number 177/193/209/225/241
//        0xB2/C2/D2/E2/F2 = port 1/2/3/4/5 Control 14                         register number 178/194/210/226/242
//        0xB3/C3/D3/E3/F3 = port 1/2/3/4/5 Control 15                         register number 179/195/211/227/243
//        0xB4/C4/D4/E4/F4 = port 1/2/3/4/5 Control 16                         register number 180/196/212/228/244
//        0xB5/C5/D5/E5/F5 = port 1/2/3/4/5 Control 17                         register number 181/197/213/229/245
//        0xB6/C6/D6/E6/F6 = port 1/2/3/4/5 Rate Limit Control                 register number 182/198/214/230/246
//        0xB7/C7/D7/E7/F7 = port 1/2/3/4/5 Priority 0 Ingress Limit Control 1 register number 183/199/215/231/247
//        0xB8/C8/D8/E8/F8 = port 1/2/3/4/5 Priority 1 Ingress Limit Control 2 register number 184/200/216/232/248
//        0xB9/C9/D9/E9/F9 = port 1/2/3/4/5 Priority 2 Ingress Limit Control 3 register number 185/201/217/233/249
//        0xBA/CA/DA/EA/FA = port 1/2/3/4/5 Priority 3 Ingress Limit Control 4 register number 186/202/218/234/250
//        0xBB/CB/DB/EB/FB = port 1/2/3/4/5 Queue 0 Egress Limit Control 1     register number 187/203/219/235/251
//        0xBC/CC/DC/EC/FC = port 1/2/3/4/5 Queue 1 Egress Limit Control 2     register number 188/204/220/236/252
//        0xBD/CD/DD/ED/FD = port 1/2/3/4/5 Queue 2 Egress Limit Control 3     register number 189/205/221/237/253
//        0xBE/CE/DE/EE/FE = port 1/2/3/4/5 Queue 3 Egress Limit Control 4     register number 190/206/222/238/254


// The KSZ8795 supports 4 general purpose ethernet service ports 0..3.
// There is a 5th RGMII port supporting the traffic path between the
// primary KSZ9897 and the secondary KSZ8795 chips.
#define SECONDARY_SWITCH_PORT_NUMBER_MIN              1
#define SECONDARY_SWITCH_PORT_NUMBER_MAX              4
#define NUMBER_SECONDARY_SWITCH_PORTS                 4



#define SECONDARY_READ_COMMAND                        0x03
#define SECONDARY_WRITE_COMMAND                       0x02

// When constructing the 24-bit/3-byte set to access the secondary registers (read and write):
// CCC 0000AAAAAAAA X DDDDDDDD   <- 3 bits command  +  12-bits (0000 + 8 actual bits) address  +  1 bit donut care  +  8 bits data = 24 bits
// CCC0000AAAAAAAAXDDDDDDDD
// CCC0000A AAAAAAAX DDDDDDDD
#define SECONDARY_COMMAND_FIELD_BIT_LEFT_SHIFT        5   // shift the 3-bit read/write command to the left
#define SECONDARY_ADDRESS_PART_0_RIGHT_SHIFT          7   // shift the 8-bit address to the RIGHT by 7 bits: the most-significant bit only is retained
#define SECONDARY_ADDRESS_PART_1_LEFT_SHIFT           1   // shift the 8-bit address to the LEFT by 1 bit:   the most significant bit only is lost

// ALU

#define MAX_SECONDARY_ALU_INDEX                       0x03FF

// The 2 control registers from table 4-6 (page 64/65):
#define ALU_CONTROL_110_6E_REGISTER_ADDRESS           0x6E
#define ALU_CONTROL_111_6F_REGISTER_ADDRESS           0x6F

#define ALU_CONTROL_110_6E_FUNCTION_SELECT_MASK       0b11100000

#define ALU_CONTROL_110_6E_FUNCTION_INDIRECT          0b00000000
#define ALU_CONTROL_110_6E_FUNCTION_GLOBAL            0b00100000
#define ALU_CONTROL_110_6E_FUNCTION_ACL               0b01000000
#define ALU_CONTROL_110_6E_FUNCTION_RESERVED_1        0b01100000
#define ALU_CONTROL_110_6E_FUNCTION_PME               0b10000000
#define ALU_CONTROL_110_6E_FUNCTION_RESERVED_2        0b10100000

#define ALU_CONTROL_110_6E_ACTION_MASK                0b00010000
#define ALU_CONTROL_110_6E_ACTION_READ                0b00010000
#define ALU_CONTROL_110_6E_ACTION_WRITE               0b00000000

#define ALU_CONTROL_110_6E_TABLE_SELECT_MASK          0b00001100
#define ALU_CONTROL_110_6E_TABLE_SELECT_STATIC        0b00000000
#define ALU_CONTROL_110_6E_TABLE_SELECT_VLAN          0b00000100
#define ALU_CONTROL_110_6E_TABLE_SELECT_DYNAMIC       0b00001000
#define ALU_CONTROL_110_6E_TABLE_SELECT_MIB           0b00001100

// There are 10 bits of indirect addressing to the ALU of length 1024.
// - the upper 2 of those 10 bits are in the lower 2 bits of 110/6E
// - the remaining 8 bits are in 111/6F:
#define ALU_CONTROL_110_6E_INDIRECT_ADDRESSING_MASK   0b00000011
#define ALU_CONTROL_111_6F_INDIRECT_ADDRESSING_MASK   0b11111111


// Data register address set for the ALU:
#define ALU_DATA_112_70_REGISTER_ADDRESS              0x70
#define ALU_DATA_113_71_REGISTER_ADDRESS              0x71
#define ALU_DATA_114_72_REGISTER_ADDRESS              0x72
#define ALU_DATA_115_73_REGISTER_ADDRESS              0x73
#define ALU_DATA_116_74_REGISTER_ADDRESS              0x74
#define ALU_DATA_117_75_REGISTER_ADDRESS              0x75
#define ALU_DATA_118_76_REGISTER_ADDRESS              0x76
#define ALU_DATA_119_77_REGISTER_ADDRESS              0x77
#define ALU_DATA_120_78_REGISTER_ADDRESS              0x78

// Definitions for the bits in the above register addresses.
//
// NOTE: location/interpretation of similar data depends on the entry type (DYNAMIC = learned, or STATIC = configured).
//
//       Example: for a DYNAMIC entry type: "source port" is 3 bits location [58:56]
//                for a STAIC entry type:   "forwarding bits" is 5 bits in location [52:48]
//
//       SO BE CAREFUL!

// From the data sheet, info for DYNAMIC ADDRESS LOOKUP:
//
// - in the Apparent application: all notion of "Dynamic MAC Address Table" is referred to as "ALU",
//                                to be consistent with that on the PRIMARY controller
// - table 4-7 "ADVANCED CONTROL REGISTER" (indirect data register, page 65)  <- declaration of the 9-register set, not much info there
// - section 4.6 "Dynamic MAC Address table" (page 84)                        <- detailed description + ALU read example
//
// Table is funky, some of the bit descriptions are not clear, but here goes. Generally:
//
// - bytes[0,1]  = register 0x70/112 and 0x71/113 -> overall result of the read
// - byte[2]     = register 0x72/114              -> data is or is not ready, forcing read re-try until ready
// - bytes[3..9] = registers 0x74..78/115..120    -> 6 bytes, 48-bit MAC address
//
// The following variables and masks taken from the "Name" column of table 4-19 (page 84):
//

// The MAC that is read out is found in registers (the same for DYNAMIC and STATIC):
#define ALU_MAC_DATA_BYTE_1_ADDRESS                   ALU_DATA_115_73_REGISTER_ADDRESS
#define ALU_MAC_DATA_BYTE_2_ADDRESS                   ALU_DATA_116_74_REGISTER_ADDRESS
#define ALU_MAC_DATA_BYTE_3_ADDRESS                   ALU_DATA_117_75_REGISTER_ADDRESS
#define ALU_MAC_DATA_BYTE_4_ADDRESS                   ALU_DATA_118_76_REGISTER_ADDRESS
#define ALU_MAC_DATA_BYTE_5_ADDRESS                   ALU_DATA_119_77_REGISTER_ADDRESS
#define ALU_MAC_DATA_BYTE_6_ADDRESS                   ALU_DATA_120_78_REGISTER_ADDRESS


// DYNAMIC ENTRIES DEFINITIONS:

// bit [71] in register 0x70/112:
#define ALU_DYNAMIC_REG_112_70_VALID_ENTRIES_MASK        0b10000000
#define ALU_DYNAMIC_REG_112_70_VALID_ENTRIES_NONE_VALUE  0b10000000
#define ALU_DYNAMIC_REG_112_70_VALID_ENTRIES_YES_VALUE   0b00000000

// bits [70..61] over registers 0x70/112 and 0x71/113 = 10 bits. Description rather confusing.
//
//       |<------     112/0x70    ------>|<------     113/0x71    ------> |
//       | 71  70  69  68  67  66  65  64  63  62  61  60  59  58  57  56 |
//       |    |                                      |       |            |
//       |    |<-------    # valid entries   ------->| AGING |<-SRC PORT->| 
// bits: |  1 |                 10                   |   2   |     3      |  <- 1+10+2+3=16 bits
//
#define ALU_DYNAMIC_REG_112_70_MAC EMPTY                   0b10000000

#define ALU_DYNAMIC_REG_112_70_NUMBER_OF_ENTRIES_MASK      0b01111111
#define ALU_DYNAMIC_REG_112_70_NUMBER_BITS_SHIFT_LEFT      8
#define ALU_DYNAMIC_REG_113_71_NUMBER_OF_ENTRIES_MASK      0b11100000
#define ALU_DYNAMIC_REG_113_71_NUMBER_BITS_SHIFT_RIGHT     5

#define ALU_DYNAMIC_REG_113_71_AGING_TIMESTAMP_MASK        0b00011000
#define ALU_DYNAMIC_REG_113_71_AGING_BITS_SHIFT_RIGHT      3

#define ALU_DYNAMIC_REG_113_71_SOURCE_PORT_MASK            0b00000111
#define ALU_DYNAMIC_REG_113_71_SOURCE_PORT_1_VALUE         0b00000000
#define ALU_DYNAMIC_REG_113_71_SOURCE_PORT_2_VALUE         0b00000001
#define ALU_DYNAMIC_REG_113_71_SOURCE_PORT_3_VALUE         0b00000010
#define ALU_DYNAMIC_REG_113_71_SOURCE_PORT_4_VALUE         0b00000011
#define ALU_DYNAMIC_REG_113_71_SOURCE_PORT_RGMII_VALUE     0b00000100

// Data is/is not ready: yes, 1 means IS NOT READY as per the data sheet:
#define ALU_DYNAMIC_REG_114_72_DATA_READY_MASK             0b10000000
#define ALU_DYNAMIC_REG_114_72_DATA_IS_NOT_READY           0b10000000
#define ALU_DYNAMIC_REG_114_72_DATA_IS_READY               0b00000000

#define ALU_DYNAMIC_REG_114_72_FILTER_ID_MASK              0b01111111


// STATIC ENTRIES DEFINITIONS.
//
// NOTE: the location of FID/USE FID bits are not the same for READ and WRITE.
//       Also, PORT_FORWARDING uses 5 bits in register 114/0x72 (different from
//       "SOURCE_PORT of a DYNAMIC entry).
// READ (table 4-16 page 80):
#define ALU_STATIC_READ_REG_113_71_FID_MASK                0b11111110
#define ALU_STATIC_READ_REG_113_71_FID_DEFAULT             0b00000000
#define ALU_STATIC_READ_REG_113_71_USE_FID_AND_MAC         0b00000001
#define ALU_STATIC_READ_REG_113_71_USE_MAC_ONLY            0b00000000

#define ALU_STATIC_READ_REG_114_72_RESERVED_MASK           0b10000000
#define ALU_STATIC_READ_REG_114_72_RESERVED_DEFAULT        0b00000000

#define ALU_STATIC_READ_REG_114_72_OVERRIDE_MASK           0b01000000
#define ALU_STATIC_READ_REG_114_72_OVERRIDE_YES            0b01000000
#define ALU_STATIC_READ_REG_114_72_OVERRIDE_NO             0b00000000


#if 0 // DATA SHEET ERROR? Page 81 STATIC ALU bit[53] states "Valid: 1 = This entry is valid". However,
      // during testing, this bit never goes to 1, and the expected data is read out when it is = 0.
      // On the other hand, page 84 DYNAMIC ALU bit[55] states "Data READY 1 = the entry is not ready".
      // The assumption is made that the data sheet is incorrect, and the definitions for STATIC VALID
      // is reversed - so it is now consistent with the definition for DYNAMIC DATA READY.
#define ALU_STATIC_READ_REG_114_72_VALID_MASK              0b00100000
#define ALU_STATIC_READ_REG_114_72_VALID_YES               0b00100000   <- seemingly incorrect based on testing
#define ALU_STATIC_READ_REG_114_72_VALID_NO                0b00000000   <- seemingly incorrect based on testing
#else
#define ALU_STATIC_READ_REG_114_72_VALID_MASK              0b00100000
#define ALU_STATIC_READ_REG_114_72_VALID_YES               0b00000000
#define ALU_STATIC_READ_REG_114_72_VALID_NO                0b01000000
#endif


#define ALU_STATIC_READ_REG_114_72_FORWARDING_PORTS_MASK   0b00011111
#define ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_1       0b00000001
#define ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_2       0b00000010
#define ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_3       0b00000100
#define ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_4       0b00001000
#define ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_RGMII   0b00010000


// WRITE (table 4-16 page 81):
// NOTE: forwarding ports definitions are the same for read and write, but defined separately.
#define ALU_STATIC_WRITE_REG_113_71_FID_MASK               0b01111111
#define ALU_STATIC_WRITE_REG_113_71_FID_DEFAULT            0b00000000

#define ALU_STATIC_WRITE_REG_114_72_USE_FID_MASK           0b10000000
#define ALU_STATIC_WRITE_REG_114_72_USE_FID_AND_MAC        0b10000000
#define ALU_STATIC_WRITE_REG_114_72_USE_MAC_ONLY           0b00000000
#define ALU_STATIC_WRITE_REG_114_72_USE_FID_DEFAULT        ALU_STATIC_WRITE_REG_114_72_USE_MAC_ONLY

#define ALU_STATIC_WRITE_REG_114_72_OVERRIDE_MASK          0b01000000
#define ALU_STATIC_WRITE_REG_114_72_OVERRIDE_YES           0b01000000
#define ALU_STATIC_WRITE_REG_114_72_OVERRIDE_NO            0b00000000
#define ALU_STATIC_WRITE_REG_114_72_OVERRIDE_DEFAULT       ALU_STATIC_WRITE_REG_114_72_OVERRIDE_NO

// The remaining bits are the same for READ and WRITE:
#define ALU_STATIC_WRITE_REG_114_72_VALID_MASK             0b00100000
#define ALU_STATIC_WRITE_REG_114_72_VALID_YES              0b00100000
#define ALU_STATIC_WRITE_REG_114_72_VALID_NO               0b00000000
#define ALU_STATIC_WRITE_REG_114_72_VALID_DEFAULT          ALU_STATIC_WRITE_REG_114_72_VALID_NO

#define ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORTS_MASK  0b00011111
#define ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORT_1      0b00000001
#define ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORT_2      0b00000010
#define ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORT_3      0b00000100
#define ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORT_4      0b00001000
#define ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORT_RGMII  0b00010000

// The 6-byte MAC is the same for READ and WRITE: use the same 115/0x73 .. 120/0x78 register address set.




// For the MAC resolution process: when reading the secondary ALU DYNAMIC table by index,
// 9 bytes of data are returned, written to the pData[xxx] of the following structure. 
//
// Note: padding is desirable to ensure 32-bit alignment. Here, none was required.
typedef struct s_aluReadDataResults
{
    U8    pData[9];               // ALU_INDIRECT_DATA_REGISTER_8_ADDRESS  112/0x70 ..120/0x78
    U8    timestamp;              // bits from register7
    U8    sourcePort;             // bits from register7
    U8    kszPort;                // derived from sourcePort
    char *pDownstreamPort;        // derived from sourcePort
    BOOL  dataReady;              // 1 bit from register6
    U8    filterId;               // bits from register6
    U16   numberEntries;          // number of entries (over 2 registers)
} t_aluReadDataResults;


#if 0
// Base MIB addresses when extracting MIB counters. These are used in conjunction with the "port key" (1..5)
// that is used to construct the actual register address based on the port.
#define MIB_SET_MIB_READ_ENABLE_BASE_ADDRESS_N500     500
#define MIB_SET_MIB_INDEX_BASE_ADDRESS_N501           501
#define MIB_COUNTER_BYTE_4_BASE_ADDRESS_N503          503
#define MIB_COUNTER_BYTE_3_BASE_ADDRESS_N504          504
#define MIB_COUNTER_BYTE_2_BASE_ADDRESS_N505          505
#define MIB_COUNTER_BYTE_1_BASE_ADDRESS_N506          506
#define MIB_COUNTER_BYTE_0_BASE_ADDRESS_N507          507


// Bit settings for 0xN500 MIB Control register. The bit location as specified in the KSZ9897
// data sheet is translated to a bit position in an 8-bit register of 0xN500, N501, N502, N503:
//
// - "MIB Counter Overflow Indication": bit [31] of the CONTROL MIB or 0x80 of N500 register
// - reserved [30:2]:                   don't touch me ...
// - "MIB Read Enable/Count valid":     bit [25] of the CONTROL MIB or 0x02 of N500 register
// - "MIB Flush and Freeze  Enable":    bit [24] of the CONTROL MIB or 0x01 of N500 register
#define MIB_COUNTER_OVERFLOW_INDICATION_BIT_MASK 0x80
#define MIB_READ_ENABLE_COUNT_VALID_BIT_MASK     0x02
#define MIB_FLUSH_AND_FREEZE_ENABLE_BIT_MASK     0x01


// To support the FLUSH, FREEZE and UNFREEZE MIB counter operation in the "Switch MIB Control Register", address 0x0336
#define MIB_CONTROL_FLUSH_MIB_COUNTERS_MASK      0x80
#define MIB_CONTROL_FREEZE_MIB_COUNTERS_MASK     0x40
#define SWITCH_MIB_CONTROL_REGISTER_ADDRESS      0x0336


#endif

// Secondary switch info, on a per-port basis.
typedef struct s_secondarySwitchInfo
{
    U8   controlReg_2;
    U8   controlReg_9;
    U8   controlReg_10;
    U8   controlReg_11;
    U8   status2;                 // register 30/46/62/78 Port N (1,2,3,4) "Status 2".
    BOOL portPoweredUp;           // configuration from the gateway
    BOOL restored02;                // control reg 02 was restored to configured value
    BOOL restored09;                // control reg 09 was restored to configured value
    BOOL restored10;                // control reg 10 was restored to configured value
    BOOL restored11;                // control reg 11 was restored to configured value
} t_secondarySwitchInfo;


// External prototypes:
void  initSecondarySwitchData                    (void);
void  configureSecondarySwitchController         (void);
void  configureSecondaryPortPowerBit             (U16 portIndex, U8 portKey, BOOL enablePort);

void  enableSecondaryEthernetService             (void);
void  checkSecondaryEthernetService              (void);
void  secondaryControllerReset                   (void);

void  checkSecondaryControlReg_02                (U32 portRank);
void  checkSecondaryControlReg_09                (U32 portRank);
void  checkSecondaryControlReg_10                (U32 portRank);
void  checkSecondaryControlReg_11                (U32 portRank);

void  checkSecondaryStatusRegisters              (U32 portRank);
BOOL  isCurrentSecondaryLinkStatusUp             (U32 portRank);

void  readCoreSecondaryGlobalRegisters           (void);

U8    readFromSecondarySwitchByAddress           (U8 address);
void  writeToSecondaryByAddress                  (U8 registerAddress, U8 registerValue);

void  readSecondaryPortControlAndStatusInfo      (void);
U16   getSecondaryStatusByPortIndex              (U32 portIndex);
U16   getSecondaryControl10ByPortIndex           (U32 portIndex);
BOOL  isSecondaryLinkStatusLinkUp                (U16 registerStatus);
BOOL  isSecondaryLinkControl10PoweredUp          (U16 control10Register);

void  readSecondaryMac                           (void);


// ALU:
void  userReadAluSecondaryDynamicByIndex         (char *pUserInput);
void  readAluSecondaryDynamicByIndex             (U32 aluIndex, U8 *pData);

BOOL  isConfigSecondaryStaticAluMac              (U8 *pMac);
BOOL  isReadSecondaryStaticInfo                  (U16 someIndex, U8 *portForward, U8 *pMac);

void  readAluSecondaryRange                      (char *pUserInput);
void  readOneSecondaryDynamicAluEntry            (U16 index, t_aluReadDataResults *pAluReadData);
void  processSecondaryAluDynamicRawData          (U8 *pData,  U8 *pSourcePort,    U16 *pValidCount);

char *getSecondaryPortStringFromStatic(U8 forwardingPort);

/*----------------------------------------------------------------------------*/
#endif   /* _SECONDARY_SWITCH_H_ */
