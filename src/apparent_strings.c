/**
 *
 * \brief Apparent strings-only definition module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2022 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 * ----- Licensing info, if any, should go here. -----
 *
 *
 * This module contains strings-only definitions, mostly for CLI processing and response to the caller.
 *
 *
 */


// CLI COMMANDS STRING DEFINITIONS. Every good system has a set. And so do bad ones.
//
// Note: all commands are available for execution over the USB interface. The help facility could make for a
//       dangerous user "on the other side". Its removal is under consideration.
//
// CLI Group 1: the set of mandatory high-level commands, used by the SBC/Gateway to access various AMU supported
//              features, to maintain overall AMU integrity and sanity. The responses in this set are JSON-formatted.
//              Changes to this set normally require a corresponding change on the SBC/Gateway controller.
const char GET_AUTONOMOUS_EVENTS_CMD[]                = "get_auto_events";                    //   0
const char KEEP_ALIVE_HELLO_CMD[]                     = "hello";                              //   1
const char COUNTERS_CMD[]                             = "counters";                           //   2
const char UPGRADE_CMD[]                              = "upgrade";                            //   3
const char RESET_CMD[]                                = "reset";                              //   4
const char JUMP_TO_BOOT_CMD[]                         = "jump_to_boot";                       //   5
const char PROGRAM_CMD[]                              = "program";                            //   6
const char PROGRAM_SIGNATURES_CMD[]                   = "program_signatures";                 //   7
const char CONTACT_CLOSURE_OUTPUT_CLOSE_CMD[]         = "cc_output_close";                    //   8
const char CONTACT_CLOSURE_OUTPUT_OPEN_CMD[]          = "cc_output_open";                     //   9
const char PORT_POWER_UP_CMD[]                        = "port_power_up";                      //  10
const char PORT_POWER_DOWN_CMD[]                      = "port_power_down";                    //  11
const char POE_ONLY_PORT_POWER_UP_CMD[]               = "poe_only_port_power_up";             //  12
const char POE_ONLY_PORT_POWER_DOWN_CMD[]             = "poe_only_port_power_down";           //  13
const char SET_FAN_SPEED_SETTINGS_CMD[]               = "set_fan_speed_settings";             //  14
const char RESOLVE_MAC_LIST_CMD[]                     = "resolve_mac_list";                   //  15
const char GET_MAC_RESOLUTION_RESULT_CMD[]            = "get_mac_resolution_result";          //  16
const char LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD[]        = "launch_alu_info_by_port_index";      //  17
const char GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD[]    = "get_alu_info_by_port_index_result";  //  18
const char CANCEL_GET_ALU_MAC_RESULT_CMD[]            = "cancel_get_alu_mac_result";          //  19
const char CONFIG_STATIC_LAN_MAC_CMD[]                = "config_static_lan_mac";              //  20
const char GET_STATIC_LAN_MAC_INFO_CMD[]              = "get_static_lan_mac_info";            //  21

// CLI Group 2: generally used for engineering/debug purpose. Their output is mostly json-formatted, but not all.
const char DISPLAY_BANNER_CMD[]                       = "banner";                             //  22
const char ETH_ONLY_PORT_ENABLE_CMD[]                 = "eth_only_port_enable";               //  23
const char ETH_ONLY_PORT_DISABLE_CMD[]                = "eth_only_port_disable";              //  24
const char READ_ALU_PRI_BY_INDEX_CMD[]                = "read_alu_pri_by_index";              //  25
const char READ_ALU_PRI_RANGE_CMD[]                   = "read_alu_pri_range";                 //  26
const char READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD[]       = "read_alu_pri_with_mac_as_index";     //  27
const char READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD[]        = "read_alu_sec_dynamic_by_index";      //  28
const char READ_ALU_SEC_RANGE_CMD[]                   = "read_alu_sec_dynamic_range";         //  29
const char ALU_MAC_RESO_PARAM_CMD[]                   = "alu_mac_reso_param";                 //  30
const char USER_SIGNATURE_READ_FLASH_CMD[]            = "r_us_flash";                         //  31
const char USER_SIGNATURE_READ_RAM_CMD[]              = "r_us_ram";                           //  32
const char USER_SIGNATURE_WRITE_CMD[]                 = "us_write";                           //  33
const char USER_SIGNATURE_ERASE_CMD[]                 = "us_erase";                           //  34
const char READ_BOTH_KSZ_MAC_CMD[]                    = "r_both_ksz_mac";                     //  35
const char SET_LOCAL_TESTING_CMD[]                    = "set_local_testing";                  //  36
const char POE_GENERAL_INFO_CMD[]                     = "poe_general_info";                   //  37
const char POE_CONTROLLER_INPUT_VOLTAGE_CMD[]         = "poe_controller_input_voltage";       //  38
const char ETHERNET_CONTROLLER_RESET_CMD[]            = "eth_controller_reset";               //  39
const char TEMPERATURE_CMD[]                          = "temperature";                        //  40
const char TEMP_FAN_INFO_CMD[]                        = "temp_fan_info";                      //  41
const char FAN_INFO_CMD[]                             = "fan_info";                           //  42
const char FAN_TASTIC_CMD[]                           = "fan_tastic";                         //  43
const char READ_PCBA_JUMPERS_CMD[]                    = "read_pcba_jumpers";                  //  44
const char DISPLAY_HELP_CMD[]                         = "heeeeelp";                           //  45
const char LAN_PORT_ENABLE_CMD[]                      = "lan_port_enable";                    //  46
const char LAN_PORT_DISABLE_CMD[]                     = "lan_port_disable";                   //  47
const char DISPLAY_POE_PORT_POWER_CMD[]               = "display_poe_port_power";             //  48
const char DISPLAY_PORT_INFO_CMD[]                    = "display_port_info";                  //  49
const char GET_CC_INFO_CMD[]                          = "get_cc_info";                        //  50
const char ENABLE_SWITCH_CHECKING_CMD[]               = "enable_switch_checking";             //  51
const char DISABLE_SWITCH_CHECKING_CMD[]              = "disable_switch_checking";            //  52
const char ENABLE_POE_CHECKING_CMD[]                  = "enable_poe_checking";                //  53
const char DISABLE_POE_CHECKING_CMD[]                 = "disable_poe_checking";               //  54
const char POE_READ_PORT_EVENT_REGISTER_CMD[]         = "poe_read_port_event_register";       //  55
const char POE_READ_SUPPLY_EVENT_REGISTER_CMD[]       = "poe_read_supply_event_register";     //  56

// CLI Group 3: very low-level commands, for deep (very deep) debug and info gathering.
const char POE_DEVICE_ID_CMD[]                        = "poe_dev_id";                         //  57
const char POE_MFR_ID_CMD[]                           = "poe_mfr_id";                         //  58
const char POE_READ_BYTE_CMD[]                        = "poe_read_byte";                      //  59
const char POE_READ_WORD_CMD[]                        = "poe_read_word";                      //  60
const char POE_WRITE_BYTE_CMD[]                       = "poe_write_byte";                     //  61
const char POE_WRITE_WORD_CMD[]                       = "poe_write_word";                     //  62
const char GET_SWITCH_MIB_COUNTER_CMD[]               = "get_mib_counter";                    //  63
const char ENABLE_MIB_FLUSH_FREEZE_CMD[]              = "enable_mib_flush_freeze";            //  64
const char DISABLE_MIB_FLUSH_FREEZE_CMD[]             = "disable_mib_flush_freeze";           //  65
const char FLUSH_MIB_COUNTERS_CMD[]                   = "flush_mib_counters";                 //  66
const char FREEZE_MIB_COUNTERS_CMD[]                  = "freeze_mib_counters";                //  67
const char UNFREEZE_MIB_COUNTERS_CMD[]                = "unfreeze_mib_counters";              //  68
const char SAME70_READ_REG_CMD[]                      = "read_reg";                           //  69
const char SAME70_WRITE_REG_CMD[]                     = "write_reg";                          //  70
const char READ_5PORTS_BYTE_BY_ADDRESS_CMD[]          = "r_5p_byte_by_addr";                  //  71
const char WRITE_5PORTS_BYTE_BY_ADDRESS_CMD[]         = "w_5p_byte_by_addr";                  //  72
const char READ_5PORTS_WORD_BY_ADDRESS_CMD[]          = "r_5p_word_by_addr";                  //  73
const char WRITE_5PORTS_WORD_BY_ADDRESS_CMD[]         = "w_5p_word_by_addr";                  //  74
const char ENABLE_25MHZ_CLOCK_OUTPUT_CMD[]            = "25mhz_clock_enable";                 //  75
const char DISABLE_25MHZ_CLOCK_OUTPUT_CMD[]           = "25mhz_clock_disable";                //  76
const char READ_PRI_BYTE_BY_ADDRESS_CMD[]             = "r_pbyte_by_addr";                    //  77
const char WRITE_PRI_BYTE_BY_ADDRESS_CMD[]            = "w_pbyte_by_addr";                    //  78
const char READ_PRI_WORD_BY_ADDRESS_CMD[]             = "r_pword_by_addr";                    //  79
const char WRITE_PRI_WORD_BY_ADDRESS_CMD[]            = "w_pword_by_addr";                    //  80
const char READ_SEC_BYTE_ADDRESS_CMD[]                = "r_sbyte_by_addr";                    //  81
const char READ_SEC_4PORTS_BY_BASE_ADDR_CMD[]         = "r_s4p_by_baddr";                     //  82
const char WRITE_SEC_BYTE_BY_ADDRESS_CMD[]            = "w_sbyte_by_addr";                    //  83
const char UART0_READ_MODE_STATUS_CMD[]               = "uart0_info";                         //  84
const char USART2_READ_MODE_STATUS_CMD[]              = "usart2_info";                        //  85
const char USART_RESET_STATUS_CMD[]                   = "ureset_status";                      //  86
const char RGB_LED_RED_MEDIUM_CMD[]                   = "red_medium";                         //  87
const char RGB_LED_RED_OFF_CMD[]                      = "red_off";                            //  88
const char RGB_LED_RED_BRILLIANT_CMD[]                = "red_brilliant";                      //  89
const char RGB_LED_GREEN_MEDIUM_CMD[]                 = "green_medium";                       //  90
const char RGB_LED_GREEN_OFF_CMD[]                    = "green_off";                          //  91
const char RGB_LED_GREEN_BRILLIANT_CMD[]              = "green_brilliant";                    //  92
const char RGB_LED_BLUE_MEDIUM_CMD[]                  = "blue_medium";                        //  93
const char RGB_LED_BLUE_OFF_CMD[]                     = "blue_off";                           //  94
const char RGB_LED_BLUE_BRILLIANT_CMD[]               = "blue_brilliant";                     //  95
const char RGB_LED_ENABLE_CMD[]                       = "rgb_led_ena";                        //  96
const char RGB_LED_DISABLE_CMD[]                      = "rgb_led_dis";                        //  97

// CLI Group 4: the remaining very low-level commands, for deep (very deep) debug and info gathering.
const char ENABLE_CRAFT_DEBUG_OUTPUT_CMD[]            = "enable_craft_debug_output";          //  98
const char DISABLE_CRAFT_DEBUG_OUTPUT_CMD[]           = "disable_craft_debug_output";         //  99
const char ENABLE_POE_DEBUG_OUTPUT_CMD[]              = "enable_poe_debug_output";            // 100
const char DISABLE_POE_DEBUG_OUTPUT_CMD[]             = "disable_poe_debug_output";           // 101
const char ENABLE_I2C_FULL_OUTPUT_CMD[]               = "enable_i2c_output";                  // 102
const char DISABLE_I2C_FULL_OUTPUT_CMD[]              = "disable_i2c_output";                 // 103
const char ENABLE_UPGRADE_INFO_CMD[]                  = "enable_upgrade_info";                // 104
const char DISABLE_UPGRADE_INFO_CMD[]                 = "disable_upgrade_info";               // 105
const char ENABLE_UPGRADE_HEX_INFO_CMD[]              = "enable_upgrade_hex_info";            // 106
const char DISABLE_UPGRADE_HEX_INFO_CMD[]             = "disable_upgrade_hex_info";           // 107
const char DROP_UPGRADE_DATA_PACKET_CMD[]             = "drop_upgrade_data_packet";           // 108
const char DROP_UPGRADE_PACKET_ACK_CMD[]              = "drop_upgrade_packet_ack";            // 109

// These may or may not be included in the build, depending on the INCLUDE_CLI_FLASH_COMMANDS in the
// apparent.h header file. For a build to be deployed, normally they are not compiled into the build.
// Only in an engineering/test environment should they be included.
const char PRINT_FH_CMD[]                             = "print_fh";                           // 110
const char SET_FH_PATTERN_CMD[]                       = "set_fh_pattern";                     // 111
const char CRC_FLASH_BLOCK_CMD[]                      = "crc_flash_block";                    // 112
const char IS_FLASH_RANGE_ERASED_CMD[]                = "is_flash_range_erased";              // 113
const char READ_FLASH_LONG_CMD[]                      = "read_flash_long";                    // 114
const char WRITE_FLASH_LONG_CMD[]                     = "write_flash_long";                   // 115
const char READ_FLASH_BLOCK_CMD[]                     = "read_flash_block";                   // 116
const char WRITE_FLASH_BLOCK_CMD[]                    = "write_flash_block";                  // 117
const char SOFTWARE_RESET_CMD[]                       = "software_reset";                     // 118
const char NOP_CMD[]                                  = "test_nop";                           // 119
const char WATCHDOG_DISABLE_TEST_CMD[]                = "watchdog_disable_test";              // 120
// END OF THE SET OF CLI COMMANDS.

const char AMU_ATIRA_PRODUCT_DEFAULT[]                     = "ATIRA0000";
const char AMU_SERIAL_NUMBER_DEFAULT[]                     = "AE123456-0000-000";
const char AMU_MODEL_DEFAULT[]                             = "PCBA-1";


// The following string definitions support the parsing for arguments where applicable:
const char CLI_ARG_ADDR_STRING[]                           = "address";
const char CLI_ARG_LONG_STRING[]                           = "long";
const char CLI_ARG_LENGTH_STRING[]                         = "len";
const char CLI_ARG_VALUE_STRING[]                          = "value";
const char CLI_ARG_SERIAL_NUMBER_STRING[]                  = "sn";
const char CLI_ARG_PRODUCT_STRING[]                        = "product";
const char CLI_ARG_MODEL_STRING[]                          = "model";
const char CLI_ARG_UART_STRING[]                           = "uart";
const char CLI_ARG_PORT_NUMBER_SWITCH_MIB_COUNTER_STRING[] = "port_number";
const char CLI_ARG_MIB_INDEX_SWITCH_MIB_COUNTER_STRING[]   = "mib_index";
const char CLI_ARG_CLOCK_STRING[]                          = "clock";
const char CLI_ARG_BAUD_STRING[]                           = "baud";
const char CLI_ARG_PORT_STRING[]                           = "port";
const char CLI_ARG_CONFIG_STRING[]                         = "config";
const char CLI_ARG_TERSE_STRING[]                          = "terse";

const char CLI_ARG_WATTS_STRING[]                          = "watts";
const char CLI_ARG_READ_FAN_MFR_ID_STRING[]                = "mfr_id";
const char CLI_ARG_READ_FAN_VERSION_ID_STRING[]            = "ver_id";
const char CLI_ARG_READ_FAN_STATUS_ID_STRING[]             = "status";
const char CLI_ARG_READ_FAN_SPEED_STRING[]                 = "speed";
const char CLI_ARG_WRITE_FAN_SPEED_STRING[]                = "w_speed";
const char CLI_ARG_SPEED_STRING[]                          = "speed";
const char CLI_ARG_FAN_SPEED_OFF_STRING[]                  = "off";
const char CLI_ARG_FAN_SPEED_LOW_STRING[]                  = "low";
const char CLI_ARG_FAN_SPEED_MAX_STRING[]                  = "max";
const char CLI_ARG_LOW_STRING[]                            = "low";
const char CLI_ARG_MAX_STRING[]                            = "max";
const char CLI_ARG_CC_STRING[]                             = "cc";
const char CLI_ARG_ACTION_STRING[]                         = "action";
const char CLI_ARG_CLOSE_STRING[]                          = "close";
const char CLI_ARG_OPEN_STRING[]                           = "open";
const char CLI_ARG_FID_STRING[]                            = "fid";
const char CLI_ARG_INDEX_STRING[]                          = "index";
const char CLI_ARG_ALU_START_INDEX_STRING[]                = "alu_start_index";
const char CLI_ARG_MAC_STRING[]                            = "mac";
const char CLI_ARG_CTRL_0_STRING[]                         = "ctrl0";
const char CLI_ARG_CTRL_1_STRING[]                         = "ctrl1";
const char CLI_ARG_START_STRING[]                          = "start";
const char CLI_ARG_END_STRING[]                            = "end";
const char CLI_ARG_CONTROLLER_STRING[]                     = "controller";
const char CLI_ARG_PRIMARY_STRING[]                        = "primary";
const char CLI_ARG_SECONDARY_STRING[]                      = "secondary";
const char CLI_ARG_BOTH_STRING[]                           = "both";
const char CLI_ARG_MAC_LIST_STRING[]                       = "mac_list";
const char CLI_ARG_RGB_COLOR_STRING[]                      = "color";
const char CLI_ARG_BLUE_COLOR_STRING[]                     = "blue";
const char CLI_ARG_GREEN_COLOR_STRING[]                    = "green";
const char CLI_ARG_RED_COLOR_STRING[]                      = "red";
const char CLI_ARG_ALL_COLOR_STRING[]                      = "all";
const char CLI_ARG_ON_PERIOD_STRING[]                      = "on";
const char CLI_ARG_OFF_PERIOD_STRING[]                     = "off";
const char CLI_ARG_LOOP_LIMIT_STRING[]                     = "loop_limit";
const char CLI_ARG_ALU_ACTION_STRING[]                     = "alu_action";
const char CLI_ARG_NO_OP_STRING[]                          = "no_op";
const char CLI_ARG_WRITE_STRING[]                          = "write";
const char CLI_ARG_READ_STRING[]                           = "read";
const char CLI_ARG_SEARCH_STRING[]                         = "search";
const char CLI_ARG_WAIT_ACTION_STRING[]                    = "wait_action";
const char CLI_ARG_GROUP_STRING[]                          = "group";
const char CLI_ARG_LIST_STRING[]                           = "list";


// The following constant strings are used by the SBC to process data reported by the AMU in the HELLO ACK response.
// NOTE: that some string are enclosed in quotes as part of the string, some are not.


const char COMMAND_STRING[]                                = "\"COMMAND\"";
const char RESPONSE_STRING[]                               = "\"RESPONSE\"";
const char REASON_STRING[]                                 = "\"REASON\"";
const char NOT_AVAILABLE_STRING[]                          = "\"NOT AVAILABLE\"";

const char ARGUMENTS_STRING[]                              = "\"ARGUMENTS\"";
const char RESULT_STRING[]                                 = "\"RESULT\"";
const char INVALID_ARGUMENTS_STRING[]                      = "\"INVALID ARGUMENTS\"";
const char NULL_STRING[]                                   = "\"NULL\"";

const char CLI_FLASHING_IS_DISABLED_STRING[]               = "\"FLASHING IS DISABLED\"";
const char CLI_FLASHING_IS_ENABLED_STRING[]                = "\"FLASHING IS ENABLED\"";

const char ETH_CONTROLLER_CHECKING_ENABLED_STRING[]        = "ethernet controller checking is ENABLED";
const char ETH_CONTROLLER_CHECKING_DISABLED_STRING[]       = "ethernet controller checking is *DISABLED*";
const char ETH_CONTROLLER_CHECKING_WAS_ENABLED_STRING[]    = "ethernet controller checking was ENABLED - no change made";
const char ETH_CONTROLLER_CHECKING_WAS_DISABLED_STRING[]   = "ethernet controller checking was *DISABLED* - no change made";

const char POE_CHECKING_ENABLED_STRING[]                   = "POE checking is ENABLED";
const char POE_CHECKING_DISABLED_STRING[]                  = "POE checking is *DISABLED*";
const char POE_CHECKING_WAS_ENABLED_STRING[]               = "POE checking was ENABLED - no change made";
const char POE_CHECKING_WAS_DISABLED_STRING[]              = "POE checking was *DISABLED* - no change made";

// For MAC resolution:
const char MAC_INFO_LIST_STRING[]                          = "\"MAC INFO LIST\"";
const char CURRENTLY_RUNNING_STRING[]                      = "\"CURRENTLY RUNNING\"";
const char STATE_MACHINE_LAUNCHED_STRING[]                 = "\"STATE MACHINE LAUNCHED\"";
const char INVALID_MAC_LIST_STRING[]                       = "\"INVALID MAC LIST\"";
const char EXTRACT_1_MAC_FAILED_STRING[]                   = "\"EXTRACT 1 MAC FAILED\"";
const char END_OF_MAC_NOT_FOUND_STRING[]                   = "\"END OF MAC NOT FOUND\"";
const char STATE_MACHINE_STRING[]                          = "\"STATE MACHINE\"";
const char IDLE_STRING[]                                   = "\"IDLE\"";
const char READING_FROM_PRIMARY_STRING[]                   = "\"READING FROM PRIMARY\"";
const char PREPARE_SECONDARY_READ_STRING[]                 = "\"PREPARE SECONDARY READ\"";
const char READING_FROM_SECONDARY_STRING[]                 = "\"READING FROM SECONDARY\"";

// ALU:
const char CLI_MAC_AS_INDEX_STRING[]                       = "\"MAC-as-index\"";
const char CLI_MAC_FOUND_STRING[]                          = "\"MAC found\"";

const char PROCESS_STRING[]                                = "\"PROCESS\"";
const char LAUNCHED_STRING[]                               = "\"LAUNCHED\"";
const char DATA_STRING[]                                   = "\"DATA\"";
const char PORT_INDEX_STRING[]                             = "\"PORT INDEX\"";
const char ALU_MAC_RUNNING_STRING[]                        = "\"ALU MAC RUNNING\"";
const char MAC_RESOLUTION_RUNNING_STRING[]                 = "\"MAC RESOLUTION RUNNING\"";
const char MISSING_PORT_ARG_STRING[]                       = "\"MISSING PORT ARG\"";
const char MISSING_INDEX_ARG_STRING[]                      = "\"MISSING INDEX ARG\"";
const char INVALID_PORT_STRING[]                           = "\"INVALID PORT\"";
const char MAX_ALU_INDEX_STRING[]                          = "\"MAX ALU INDEX\"";
const char CURRENT_ALU_INDEX_STRING[]                      = "\"CURRENT ALU INDEX\"";
const char RUNNING_STRING[]                                = "\"RUNNING\"";
const char NO_REQUEST_MADE_STRING[]                        = "\"NO REQUEST MADE\"";
const char CANCELED_STRING[]                               = "\"CANCELED\"";
const char USER_COMMAND_STRING[]                           = "\"USER COMMAND\"";
const char START_READ_TICK_STRING[]                        = "\"START READ TICK\"";
const char LAST_READ_TICK_STRING[]                         = "\"END READ TICK\"";
const char ELAPSED_TICK_STRING[]                           = "\"ELASPED TICK\"";
const char ALU_INDEX_OUT_OF_RANGE_STRING[]                 = "\"ALU INDEX OUT OF RANGE\"";
const char USER_PORT_STRING[]                              = "\"USER PORT\"";
const char USER_ALU_INDEX_STRING[]                         = "\"USER ALU INDEX\"";
const char KSZ_VALID_COUNT_STRING[]                        = "\"KSZ VALID COUNT\"";
const char KSZ_PORT_FORWARD_STRING[]                       = "\"KSZ PORT FORWARD\"";
const char LAST_KSZ_PORT_FORWARD_STRING[]                  = "\"LAST KSZ PORT FORWARD\"";
const char DEAD_QUIET_STRING[]                             = "\"DEAD QUIET\"";
const char ALU_START_INDEX_STRING[]                        = "\"ALU START INDEX\"";
const char NUMBER_DATA_READS_STRING[]                      = "\"NUMBER DATA READS\"";
const char MISSING_ALU_START_INDEX_ARG_STRING[]            = "\"MISSING ALU START INDEX ARG\"";
const char PRI_ALU_CONTROL_REG_READ_LIMIT_STRING[]         = "\"PRI ALU CONTROL REG READ LIMIT\"";
const char LOOP_LIMIT_MSEC_STRING[]                        = "\"LOOP LIMIT (msec)\"";
const char ALU_ACTION_STRING[]                             = "\"ALU ACTION\"";
const char WAIT_ACTION_STRING[]                            = "\"WAIT ACTION\"";
const char MISSING_ARGUMENTS_STRING[]                      = "\"MISSING ARGUMENTS\"";
const char END_INDEX_LESS_THAN_START_STRING[]              = "\"END INDEX LESS THAN START\"";
const char RANGE_TOO_LARGE_STRING[]                        = "\"RANGE TOO LARGE\"";
const char SECOND_PRI_ALU_READ_STRING[]                    = "\"SECOND PRI ALU READ\"";
const char REQUIRED_STRING[]                               = "\"REQUIRED\"";
const char NOT_REQUIRED_STRING[]                           = "\"NOT REQUIRED\"";
const char READ_FROM_SECONDARY_FAILED_STRING[]             = "\"READ FROM SECONDARY FAILED\"";
const char READ_FROM_PRIMARY_FAILED_STRING[]               = "\"READ FROM PRIMARY FAILED\"";
const char STATIC_LAN_MAC_INFO_STRING[]                    = "\"STATIC LAN MAC INFO\"";
const char STATIC_LAN_MAC_CONFIG_PRIMARY_FAILED_STRING[]   = "\"STATIC LAN MAC CONFIG PRIMARY FAILED\"";
const char STATIC_LAN_MAC_CONFIG_SECONDARY_FAILED_STRING[] = "\"STATIC LAN MAC CONFIG SECONDARY FAILED\"";
const char STATIC_LAN_MAC_CONFIG_STRING[]                  = "\"STATIC LAN MAC CONFIG\"";

const char CLI_PORT_AGING_STRING[]                         = "\"port aging\"";
const char CLI_ENTRY_TYPE_STRING[]                         = "\"entry type\"";
const char CLI_ENTRY_TYPE_DYNAMIC_STRING[]                 = "\"dynamic\"";
const char CLI_ENTRY_TYPE_UNKNOWN_STRING[]                 = "\"unknown\"";
const char CLI_ENTRY_TYPE_STATIC_STRING[]                  = "\"static\"";
const char INDEX_STRING[]                                  = "\"INDEX\"";
const char INFO_STRING[]                                   = "\"INFO\"";
const char RAW_REG_STRING[]                                = "\"RAW REG\"";
const char VALID_DATA_STRING[]                             = "\"VALID DATA\"";
const char NUMBER_OF_ENTRIES_STRING[]                      = "\"NUMBER OF ENTRIES\"";
const char TIMESTAMP_STRING[]                              = "\"TIMESTAMP\"";
const char KSZ_PORT_STRING[]                               = "\"KSZ PORT\"";
const char DOWNSTREAM_PORT_STRING[]                        = "\"DOWNSTREAM PORT\"";
const char DATA_READY_STRING[]                             = "\"DATA READY\"";
const char FILTER_ID_STRING[]                              = "\"FILTER ID\"";
const char ELAPSED_STRING[]                                = "\"ELAPSED\"";
const char PORT_FORWARD_STRING[]                           = "\"PORT FORWARD\"";
const char VALID_COUNT_STRING[]                            = "\"VALID COUNT\"";
const char TOTAL_ELAPSED_TIME_STRING[]                     = "\"TOTAL ELAPSED TIME\"";
const char SECONDARY_NUMBER_ENTRIES_STRING[]               = "\"SECONDARY NUMBER ENTRIES\"";
const char UNCHANGED_STRING[]                              = "\"UNCHANGED\"";
const char INCREASED_STRING[]                              = "\"INCREASED\"";
const char DECREASED_STRING[]                              = "\"DECREASED\"";
const char CONTROLLER_STRING[]                             = "\"CONTROLLER\"";
const char PRIMARY_STRING[]                                = "\"PRIMARY\"";
const char SECONDARY_STRING[]                              = "\"SECONDARY\"";
const char UNKNOWN_STRING[]                                = "\"UNKNOWN\"";


// POE:
const char CLI_POE_REG_ADDR_STRING[]                       = "\"ADDR\"";
const char CLI_MSB_STRING[]                                = "\"MSB\"";
const char CLI_LSB_STRING[]                                = "\"LSB\"";
const char CLI_LSB_MSB_STRING[]                            = "\"LSB_MSB\"";
const char CLI_VALUE_STRING[]                              = "\"VALUE\"";
const char POE_CONTROLLER_INPUT_VOLTAGE_STRING[]           = "\"CONTROLLER INPUT VOLTAGE\"";
const char POE_LDWPP_DETECT_TIMEOUT_STRING[]               = "\"DETECT LDWPP TIMEOUT(MSEC)\"";
const char POE_APPLY_LDWPP_WORK_AROUND_STRING[]            = "\"APPLY LDWPP WORK-AROUND\"";
const char POE_LDWPP_POWERED_DOWN_TIMEOUT_STRING[]         = "\"LINK POWERED DOWN DURING LDWPP TIMEOUT(MSEC)\"";

const char END_OF_INPUT_SIGNATURE_STRING[]                 = "END-OF-INPUT";
const char END_OF_OUTPUT_SIGNATURE_STRING[]                = "END-OF-OUTPUT";


// END ... OF CLI RELATED ITEMS.


// ----------------------------------------------------------------------------
//
// SBC/GATEWAY ... ALERT!!! ALERT!!! ALERT!!! 
//
// Definitions in this section cannot be changed unless there is a corresponding change on the SBC/Gateway-python
// side of life. If the changes are not coordinated, the AMU-to-SBC info exchange could crash and burn.
//

// These constant strings are used by the SBC to process data reported by the AMU in the HELLO ACK response.
// NOTE: that some string are enclosed in quotes as part of the string, some are not.
const char CLI_STRING[]                                   = "\"CLI\"";
const char NONE_STRING[]                                  = "\"NONE\"";
const char CODE_STRING[]                                  = "\"CODE\"";
const char STATE_STRING[]                                 = "\"state\"";
const char FLASH_ADDR_CRC_STRING[]                        = "\"flash addr/crc\"";
const char AMU_STARTUP_STRING[]                           = "\"AMU STARTUP\"";
const char WATCHDOG_STRING[]                              = "\"WATCHDOG\"";
const char WATCHDOG_ENABLED_STRING[]                      = "\"WATCHDOG: ENABLED\"";
const char WATCHDOG_DISABLED_STRING[]                     = "\"WATCHDOG:_DISABLED\"";
const char UPTIME_STRING[]                                = "\"UPTIME (SECS)\"";
const char PRODUCT_STRING[]                               = "\"PRODUCT\"";
const char MODEL_STRING[]                                 = "\"MODEL\"";
const char SERIAL_NUMBER_STRING[]                         = "\"SERIAL NUMBER\"";
const char HARDWARE_REVISION_STRING[]                     = "\"HW REVISION\"";
const char CLI_READY_FOR_JUMP_TO_BOOT[]                   = "\"ready for jump to boot\"";
const char CLI_FLASH_FAIL_NO_BOOT_SIG_STRING[]            = "\"FLASH FAIL NO BOOT SIGNATURE\"";
const char CLI_FLASH_FAIL_SET_WAIT_STATE_STRING[]         = "\"FLASH FAIL SET WAIT STATE\"";
const char PROGRAM_STRING[]                               = "\"PROGRAM\"";
const char FLASH_LOAD_ADDRESS_STRING[]                    = "\"FLASH LOAD ADDRESS\"";
const char FLASH_SIGNATURES_STRING[]                      = "\"FLASH SIGNATURES\"";
const char FLASH_READ_FAIL_STRING[]                       = "\"flash read fail\"";

const char ERASE_USER_SIGNATURE_STRING[]                  = "\"ERASE USER SIGNATURE\"";
const char USER_SIGNATURE_ERASED_STRING[]                 = "\"USER SIGNATURE ERASED - RESET REQUIRED\"";
const char USER_SIGNATURE_ERASE_FAILED_STRING[]           = "\"USER SIGNATURE ERASE *FAILED*\"";
const char USER_SIGNATURE_ERASE_DISABLED_STRING[]         = "\"USER SIGNATURE ERASE *DISABLED*\"";
const char UPDATE_USER_SIGNATURE_STRING[]                 = "\"UPDATE USER SIGNATURE\"";
const char CALCULATED_CRC_STRING[]                        = "\"CALCULATED CRC\"";
const char NUMBER_OF_BYTES_STRING[]                       = "\"NUMBER OF BYTES\"";
const char USER_SIGNATURE_UPDATED_STRING[]                = "\"USER SIGNATURE UPDATED\"";
const char USER_SIGNATURE_CURRENTLY_WRITTEN_STRING[]      = "\"USER SIGNATURE CURRENTLY WRITTEN\"";
const char USER_SIGNATURE_MUST_BE_ERASED_STRING[]         = "\"USER SIGNATURE MUST BE ERASED\"";
const char USER_SIGNATURE_WRITE_FAILED_STRING[]           = "\"USER SIGNATURE WRITE *FAILED*\"";
const char APPLICATION_STRING[]                           = "\"APPLICATION\"";
const char BOOTLOADER_STRING[]                            = "\"BOOTLOADER\"";
const char RELEASE_STRING[]                               = "\"RELEASE\"";
const char JUMP_TO_BOOT_STRING[]                          = "\"JUMP TO BOOT\"";
const char SUCCESS_STRING[]                               = "\"SUCCESS\"";
const char FAIL_STRING[]                                  = "\"FAIL\"";
const char FAIL_NO_BOOT_SIGNATURE[]                       =  "\"FAIL DUE TO NO BOOT SIGNATURES\"";
const char RESET_STRING[]                                 = "\"RESET\"";
const char RESET_IN_5_SECONDS_STRING[]                    = "\"RESET IN 5 SECONDS\"";
const char JUMP_TO_BOOT_IN_5_SECONDS_STRING[]             = "\"JUMP TO BOOT IN 5 SECONDS\"";
const char PRIMARY_GLOBAL_REGISTERS_STRING[]              = "\"PRIMARY GLOBAL REGISTERS\"";
const char GLOBAL_REGISTER_0000_STRING[]                  = "\"GLOBAL REGISTER 0x0000\"";
const char GLOBAL_REGISTER_0001_STRING[]                  = "\"GLOBAL REGISTER 0x0001\"";
const char GLOBAL_REGISTER_0002_STRING[]                  = "\"GLOBAL REGISTER 0x0002\"";
const char GLOBAL_REGISTER_0003_STRING[]                  = "\"GLOBAL REGISTER 0x0003\"";
const char GLOBAL_REGISTER_0103_STRING[]                  = "\"GLOBAL REGISTER 0x0103\"";
const char GLOBAL_REGISTER_0300_STRING[]                  = "\"GLOBAL REGISTER 0x0300\"";

const char MAC_INFO_STRING[]                              = "\"MAC INFO\"";
const char PRIMARY_MAC_STRING[]                           = "\"PRIMARY MAC\"";
const char SECONDARY_MAC_STRING[]                         = "\"SECONDARY MAC\"";
const char MAC_STRING[]                                   = "\"MAC\"";

const char SECONDARY_GLOBAL_REGISTERS_STRING[]            = "\"SECONDARY GLOBAL REGISTERS\"";
const char GLOBAL_REGISTER_00_STRING[]                    = "\"GLOBAL REGISTER 0x00\"";
const char GLOBAL_REGISTER_01_STRING[]                    = "\"GLOBAL REGISTER 0x01\"";
const char GLOBAL_REGISTER_02_STRING[]                    = "\"GLOBAL REGISTER 0x02\"";
const char GLOBAL_REGISTER_03_STRING[]                    = "\"GLOBAL REGISTER 0x03\"";
const char GLOBAL_REGISTER_04_STRING[]                    = "\"GLOBAL REGISTER 0x04\"";
const char GLOBAL_REGISTER_05_STRING[]                    = "\"GLOBAL REGISTER 0x05\"";
const char GLOBAL_REGISTER_06_STRING[]                    = "\"GLOBAL REGISTER 0x06\"";
const char GLOBAL_REGISTER_07_STRING[]                    = "\"GLOBAL REGISTER 0x07\"";
const char GLOBAL_REGISTER_08_STRING[]                    = "\"GLOBAL REGISTER 0x08\"";
const char GLOBAL_REGISTER_09_STRING[]                    = "\"GLOBAL REGISTER 0x09\"";
const char GLOBAL_REGISTER_0A_STRING[]                    = "\"GLOBAL REGISTER 0x0A\"";
const char GLOBAL_REGISTER_0B_STRING[]                    = "\"GLOBAL REGISTER 0x0B\"";
const char GLOBAL_REGISTER_0C_STRING[]                    = "\"GLOBAL REGISTER 0x0C\"";
const char GLOBAL_REGISTER_0D_STRING[]                    = "\"GLOBAL REGISTER 0x0D\"";
const char GLOBAL_REGISTER_0E_STRING[]                    = "\"GLOBAL REGISTER 0x0E\"";
const char GLOBAL_REGISTER_0F_STRING[]                    = "\"GLOBAL REGISTER 0x0F\"";

const char PRIMARY_CONTROL_REGISTERS_STRING[]             = "\"PRIMARY CONTROL REGISTERS\"";
const char PRIMARY_STATUS_REGISTERS_STRING[]              = "\"PRIMARY STATUS REGISTERS\"";
const char SECONDARY_CONTROL_REGISTERS_STRING[]           = "\"SECONDARY CONTROL REGISTERS\"";
const char SECONDARY_STATUS_REGISTERS_STRING[]            = "\"SECONDARY STATUS REGISTERS\"";

const char SECONDARY_CONTROL_REGISTER_02_STRING[]         =  "\"CONTROL_02\"";
const char SECONDARY_CONTROL_REGISTER_09_STRING[]         =  "\"CONTROL_09\"";
const char SECONDARY_CONTROL_REGISTER_10_STRING[]         =  "\"CONTROL_10\"";
const char SECONDARY_CONTROL_REGISTER_11_STRING[]         =  "\"CONTROL_11\"";

const char AUTONOMOUS_EVENTS_LIST_STRING[]                = "\"AUTO EVENTS LIST\"";
const char EVT_STRING[]                                   = "\"EVT\"";
const char DEFAULT_STRING[]                               = "\"DEFAULT\"";

const char AMU_COUNTERS_STRING[]                          = "\"AMU COUNTERS\"";
const char RANK_STRING[]                                  = "\"RANK\"";
const char COUNT_STRING[]                                 = "\"COUNT\"";


// The following support the construction of the JSON-like string that contains
// the data upon receipt of a request to retrieve a specific MIB counter. Note
// that they are all defined as being enclosed in quotes.
const char MIB_COUNTER_STRING[]                           = "\"MIB COUNTER\"";
const char MIB_INDEX_STRING[]                             = "\"MIB INDEX\"";
const char ACTION_STRING[]                                = "ACTION";                      // <- intentionaly NOT enclosed in quotes
const char ENABLE_FLUSH_FREEZE_STRING[]                   = "\"ENABLE FLUSH FREEZE\"";
const char DISABLE_FLUSH_FREEZE_STRING[]                  = "\"DISABLE FLUSH FREEZE\"";
const char PRIMARY_SWITCH_LIST_STRING[]                   = "\"PRIMARY SWITCH LIST\"";
const char SECONDARY_SWITCH_LIST_STRING[]                 = "\"SECONDARY SWITCH LIST\"";
const char EXECUTION_DONE_STRING[]                        = "\"DONE\"";
const char MIB_RESULT_STRING[]                            = "\"MIB RESULT\"";
const char MIB_RESULT_SUCCESS_STRING[]                    = "\"SUCCESS\"";
const char MIB_RESULT_FAILED_STRING[   ]                  = "\"FAILED\"";
const char MIB_RESULT_MISSING_ARG_REASON_STRING[]         = "\"MISSING ARGUMENT\"";
const char MIB_RESULT_INVAL_MIB_INDEX_REASON_STRING[]     = "\"INVALID MIB INDEX\"";
const char MIB_RESULT_NOT_READY_REASON_STRING[]           = "\"NOT READY\"";
const char MIB_RESULT_INVAL_PORT_REASON_STRING[]          = "\"INVALID PORT\"";
const char MIB_READ_ELAPSED_MSECS_STRING[]                = "\"ELAPSED MSECS\"";
const char VALUE_AS_HEX_STRING[]                          = "\"VALUE AS HEX\"";
const char VALUE_AS_DECIMAL_STRING[]                      = "\"VALUE AS DECIMAL\"";
const char ERROR_STRING[]                                 = "\"ERROR\"";
const char SET_MIB_INDEX_STRING[]                         = "\"SET MIB INDEX\"";
const char SET_MIB_READ_ENABLE_STRING[]                   = "\"SET MIB READ ENABLE\"";
const char ADDRESS_STRING[]                               = "\"ADDRESS\"";
const char ADDRESS_LIST_STRING[]                          = "\"ADDRESS LIST\"";
const char READ_N503_4_5_6_7_STRING[]                     = "\"READ N503/504/505/506/507\"";
const char BYTE_SEQUENCE_STRING[]                         = "\"BYTE SEQUENCE\"";
const char VALUE_STRING[]                                 = "\"VALUE\"";
const char CHECK_MIB_READ_ENABLE_STRING[]                 = "\"CHECK MIB READ ENABLE\"";
const char READ_ATTEMPTS_STRING[]                         = "\"READ ATTEMPTS\"";
const char COUNTER_OVERFLOW_STRING[]                      = "\"COUNTER OVERFLOW\"";
const char YES_STRING[]                                   = "\"YES\"";
const char NO_STRING[]                                    = "\"NO\"";
const char ETHERNET_ENABLED_STRING[]                      = "\"ETH ENABLED\"";
const char SECONDARY_ENABLED_ON_STARTUP_STRING[]          = "\"SEC ETH CONTROLLER ENABLED DURING AMU STARTUP\"";


// I (Ed) ran into some difficulty displaying from a table of constant strings, and didn't have the patience
// to deal with it in a proper programmatic way. So displaying from a list of ports (1, 2, 3, 4, LAN, ...) was
// implemented in the following clunky manner.
const char PORT_1_STRING[]                                = "1";
const char PORT_2_STRING[]                                = "2";
const char PORT_3_STRING[]                                = "3";
const char PORT_4_STRING[]                                = "4";
const char PORT_5_STRING[]                                = "5";
const char PORT_6_STRING[]                                = "6";
const char PORT_7_STRING[]                                = "7";
const char PORT_8_STRING[]                                = "8";
const char PORT_LAN_STRING[]                              = "LAN";
const char PORT_RGMII_STRING[]                            = "RGMII";
const char PORT_UNKNOWN_STRING[]                          = "UNKNOWN"; // Should not see this as it is based on a for loop

// Autonomous notification of changes in the ethernet port control or status registers are written to this string.
// If the string already contains text, it is appended to. This string is included in the HELLO ACK response, for
// processing by the SBC. The string is cleared after sending HELLO ACK response. The SBC HELLO could be from its
// slow-polling keep-alive (45-sec rate), or from its faster "gimme autonomous events" polling (5sec rate).
//
// When an autonomous event occurs (say: port link down), a spontaneous printf is performed when the string is constructed,
// to be made available to the SBC as notification to the SBC/Gateway, for retrieval when it sends the HELLO command.
//
// The string is constructed as JSON, quite terse to contain the data for up to 8 events in a single JSON string.
// Example:
//
// {"AUTONOMOUS EVENTS LIST":{"X":{"P":"LAN","SF":"0x794D","ST":"0x7449","EI":2,"TICK":12345678}, ... }}
//                            |<------                single event data                 ------>|
//
// Note the inclusion of the quotes as part of these string definitions.
const char PORT_SHORT_STRING[]                             = "\"P\"";               // ethernet port number - short string
const char CONTROL_FROM_STRING[]                           = "\"CF\"";              // control register FROM value
const char CONTROL_TO_STRING[]                             = "\"CT\"";              // control register TO value
const char STATUS_FROM_STRING[]                            = "\"SF\"";              // status register FROM value
const char STATUS_TO_STRING[]                              = "\"ST\"";              // status register TO value
const char EVENT_ID_STRING[]                               = "\"EI\"";              // event identifier
const char TICK_STRING[]                                   = "\"TICK\"";            // tick counter when event occurred
const char COMMA_ONLY_STRING[]                             = ",";                   // comma separator for multiple JSON components              <- use this one instead
const char CC_IN_STATUS_STRING[]                           = "\"CC IN STATUS\"";    // contact closure input status

const char EXPECTED_VALUE_STRING[]                         = "\"EXPECTED VALUE\"";
const char ACTUAL_VALUE_STRING[]                           = "\"ACTUAL VALUE\"";


// For regular reporting of the CONTROL and STATUS registers for the primary ethernet chip controller (included in
// the HELLO ACK response), the following are used to construct the JSON-like strings.
//
// Example of the reporting of the list of "PRIMARY CONTROL REGISTERS":
//
// { "PRIMARY CONTROL REGISTERS" : {"PORT 1" : "0x2100", "PORT 2" : "0x2100", "PORT 3" : "0x2100", "PORT 4" : "0x2100", "PORT LAN" : "0x2100"}}
//
// Example of the reporting of the list of "PRIMARY STATUS REGISTERS":
//
// { "PRIMARY STATUS REGISTERS" : {"PORT LAN" : {"STATUS" : "0x7949", "LINK" : "UP"}, ... "PORT 4" : {"STATUS" : "0x7949", "LINK" : "UP"}}}
//
const char PORT_NO_QUOTES_STRING[]                         = "PORT";          // <- intentionaly NOT enclosed in quotes 
const char CONFIG_STRING[]                                 = "\"CONFIG\"";
const char CONTROL_STRING[]                                = "\"CONTROL\"";
const char STATUS_STRING[]                                 = "\"STATUS\"";
const char LINK_STRING[]                                   = "\"LINK\"";
const char LINK_STATUS_UP_STRING[]                         = "\"UP\"";
const char LINK_STATUS_DOWN_STRING[]                       = "\"DOWN\"";
const char PORT_CONFIG_POWERED_UP_STRING[]                 = "\"POWERED UP\"";
const char PORT_CONFIG_POWERED_DOWN_STRING[]               = "\"POWERED DOWN\"";
const char PORT_COMMAND_POWER_UP_STRING[]                  = "\"POWER UP\"";
const char PORT_COMMAND_POWER_DOWN_STRING[]                = "\"POWER DOWN\"";
const char UP_STRING[]                                     = "\"UP\"";
const char DOWN_STRING[]                                   = "\"DOWN\"";

// This one: not to be confused with PORT_NO_QUOTES_STRING from above.
const char PORT_STRING[]                                   = "\"PORT\"";

// When re-configuring the power bit of a primary or secondary port:
const char PRIMARY_PORT_CONFIGURED_STRING[]                = "\"PRIMARY PORT CONFIGURED\"";
const char SECONDARY_PORT_CONFIGURED_STRING[]              = "\"SECONDARY PORT CONFIGURED\"";
const char PORT_CONFIG_STRING[]                            = "\"PORT CONFIG\"";
const char PORT_KEY_STRING[]                               = "\"PORT KEY\"";
const char PORT_RANK_STRING[]                              = "\"PORT RANK\"";
const char REG_ADDR_STRING[]                               = "\"REG ADDR\"";
const char CONTROL_BEFORE_STRING[]                         = "\"CONTROL BEFORE\"";
const char CONTROL_AFTER_STRING[]                          = "\"CONTROL AFTER\"";
const char STATUS_BEFORE_STRING[]                          = "\"STATUS BEFORE\"";
const char STATUS_AFTER_STRING[]                           = "\"STATUS AFTER\"";
const char EXPECTED_SAME_AS_ACTUAL_STRING[]                = "\"EXPECTED SAME AS ACTUAL\"";
const char PORT_LIST_STRING[]                              = "\"PORT LIST\"";

// Other miscellaneous strings for which the SBC/Gateway will parse:
const char CC_INFO_STRING[]                                = "\"CC_INFO\"";
const char CC_STATUS_STRING[]                              = "\"STATUS\"";
const char CC_OUTPUT_STRING[]                              = "\"OUTPUT\"";
const char CC_INPUT_STRING[]                               = "\"INPUT\"";
const char CC_OPEN_STRING[]                                = "\"OPEN\"";
const char CC_CLOSE_STRING[]                               = "\"CLOSE\"";
const char CC_NUMBER_STRING[]                              = "\"CC_NUM\"";
const char INVALID_PARAMETERS_STRING[]                     = "\"INVALID PARAMETERS\"";
const char INVALID_MAC_STRING[]                            = "\"INVALID MAC\"";
const char INVALID_COMMAND_STRING[]                        = "\"INVALID COMMAND\"";
const char NOW_OPEN_STRING[]                               = "\"NOW OPEN\"";
const char NOW_CLOSE_STRING[]                              = "\"NOW CLOSE\"";
const char WAS_ALREADY_OPEN_STRING[]                       = "\"WAS ALREADY OPEN\"";
const char WAS_ALREADY_CLOSE_STRING[]                      = "\"WAS ALREADY CLOSE\"";

const char SPEED_STRING[]                                  = "\"SPEED\"";
const char FAN_SPEED_MAX_STRING[]                          = "\"MAX\"";
const char FAN_SPEED_LOW_STRING[]                          = "\"LOW\"";
const char FAN_SPEED_OFF_STRING[]                          = "\"OFF\"";

const char TEMPERATURE_STRING[]                            = "\"TEMPERATURE\"";
const char POE_TEMPERATURE_STRING[]                        = "\"POE TEMPERATURE\"";

const char PORT_DATA_STRING[]                              = "\"PORT DATA\"";
const char POWER_STRING[]                                  = "\"POWER\"";
const char PAC_STRING[]                                    = "\"PAC\"";
const char AUTO_CLASS_STRING[]                             = "\"AUTO CLASS\"";
const char AC_SUPPORT_STRING[]                             = "\"AC SUPPORT\"";

const char TEMPERATURE_CHANGE_STRING[]                     = "\"TEMPERATURE CHANGE\"";
const char RAW_REGISTER_STRING[]                           = "\"RAW REGISTER\"";
const char FAN_SPEED_CHANGE_STRING[]                       = "\"FAN SPEED CHANGE\"";
const char CURRENT_FAN_SPEED_STRING[]                      = "\"CURRENT FAN SPEED\"";
const char TEMP_SPEED_SETTINGS_STRING[]                    = "\"TEMPERATURE SPEED SETTINGS\"";
const char TEMP_FAN_INFO_STRING[]                          = "\"TEMP/FAN INFO\"";
const char POE_INFO_STRING[]                               = "\"POE INFO\"";

const char LOW_TEMP_SPEED_SETTING_STRING[]                 = "\"LOW TEMP SPEED SETTING\"";
const char MAX_TEMP_SPEED_SETTING_STRING[]                 = "\"MAX TEMP SPEED SETTING\"";

const char POE_PORT_ENABLE_STRING[]                        = "\"POE PORT ENABLE\"";
const char POE_OPERATING_MODE_STRING[]                     = "\"POE OPERATING MODE\"";
const char POE_OPERATING_MODE_ERROR_STRING[]               = "\"POE OPERATING MODE ERROR\"";
const char EXPECTED_STRING[]                               = "\"EXPECTED\"";
const char READ_BACK_STRING[]                              = "\"READ BACK\"";

const char COMMAND_REGISTER_STRING[]                       = "\"COMMAND REGISTER\"";
const char LAST_OPERATING_MODE_VALUE_STRING[]              = "\"LAST OPERATING MODE VALUE\"";
const char LAST_POWER_ENABLE_VALUE_STRING[]                = "\"LAST POWER ENABLE VALUE\"";
const char FORMULA_STRING[]                                = "\"FORMULA\"";

const char POE_AUTO_CLASS_1_STRING[]                       = "\"CLASS 1\"";
const char POE_AUTO_CLASS_2_STRING[]                       = "\"CLASS 2\"";
const char POE_AUTO_CLASS_3_STRING[]                       = "\"CLASS 3\"";
const char POE_AUTO_CLASS_4_STRING[]                       = "\"CLASS 4\"";
const char POE_AUTO_CLASS_NOT_POE_STRING[]                 = "\"CLASS NOT POE\"";
const char POE_AUTO_CLASS_UNKNOWN_STRING[]                 = "\"CLASS UNKNOWN\"";
const char POE_AUTO_CLASS_STRING[]                         = "\"AUTO CLASS\"";
const char POE_AUTO_CLASS_LIMIT_STRING[]                   = "\"AUTO CLASS LIMIT\"";
const char POE_AUTO_CLASS_STATUS_REGISTER_STRING[]         = "\"POE AUTO CLASS STATUS REGISTER\"";

// These relate to the 25Mhz clock output configuration by CLI:
const char MHZ_25_CLOCK_OUTPUT_STRING[]                    = "\"MHZ 25 CLOCK OUTPUT\"";
const char DISABLE_STRING[]                                = "\"DISABLE\"";
const char ENABLE_STRING[]                                 = "\"ENABLE\"";
const char REG_VALUE_STRING[]                              = "\"REG VALUE\"";

// POE AUTONOMOUS EVENT STRINGS:
const char POE_PORT_POWER_STRING[]                         = "\"POE PORT POWER\"";                 // AUTONOMOUS_EVENT_POE_PORT_POWER_CHANGE                 18
const char POE_PORT_AUTO_CLASS_STRING[]                    = "\"POE PORT AUTO CLASS\"";            // AUTONOMOUS_EVENT_POE_PORT_AUTO_CLASS_CONFIG_CHANGE     19
const char POE_PORT_PAC_STRING[]                           = "\"POE PORT PAC\"";                   // AUTONOMOUS_EVENT_POE_PORT_PAC_CHANGE                   20
const char POE_AUTO_CLASS_STATUS_STRING[]                  = "\"POE AUTO CLASS STATUS\"";          // AUTONOMOUS_EVENT_POE_PORT_AC_STATUS_CHANGE             21
const char POE_POWER_THRESHOLD_EXCEEDED_STRING[]           = "\"POE POWER THRESHOLD EXCEEDED\"";   // AUTONOMOUS_EVENT_POE_PORT_POWER_THRESHOLD_EXCEEDED     22
const char POE_LINK_DOWN_WITH_POWER_STRING[]               = "\"POE LINK DOWN WITH POWER\"";       // AUTONOMOUS_EVENT_POE_PORT_LINK_DOWN_WITH_POWER         23

// Members of a POE autonomous event:
const char CLASS_CONFIG_STRING[]                           = "\"CLASS CONFIG\"";
const char SUPPORTED_STRING[]                              = "\"SUPPORTED\"";
const char NOT_SUPPORTED_STRING[]                          = "\"NOT SUPPORTED\"";


// To support the CLI POE event command:
const char CLI_ARG_POE_EVENT_EVENT_STRING[]                = "event";
const char CLI_ARG_POE_EVENT_POWER_STRING[]                = "power";
const char CLI_ARG_POE_EVENT_DETECTION_STRING[]            = "detection";
const char CLI_ARG_POE_EVENT_FAULT_STRING[]                = "fault";
const char CLI_ARG_POE_EVENT_START_STRING[]                = "start";
const char CLI_ARG_POE_EVENT_SUPPLY_STRING[]               = "supply";
const char CLI_ARG_POE_EVENT_ALL_STRING[]                  = "all";

const char CLI_POE_SUPPLY_FAULT_EVENTS_STRING[]            = "\"supply/fault events\"";
const char CLI_POE_NO_SUPPLY_NO_FAULTS_STRING[]            = "\"NO-SUPPLY NO-FAULT\"";
const char CLI_POE_I2C_READ_FAILED_STRING[]                = "\"POE I2C read failed\"";

const char POE_SUPPLY_FAULT_EVENT_TSD_STRING[]             = "TSD ";
const char POE_SUPPLY_FAULT_EVENT_VDUV_STRING[]            = "VDUV ";
const char POE_SUPPLY_FAULT_EVENT_VDWRN_STRING[]           = "VDWRN ";
const char POE_SUPPLY_FAULT_EVENT_VPUV_STRING[]            = "VPUV ";
const char POE_SUPPLY_FAULT_EVENT_OSSE_STRING[]            = "OSSE ";
const char POE_SUPPLY_FAULT_EVENT_RAMFLT_STRING[]          = "RAMFLT ";

const char POE_SUPPLY_FAULT_EVENT_STRING[]                 = "\"POE SUPPLY FAULT EVENT\"";         // AEI_POE_SUPPLY_FAULT_EVENT_DETECTED      35
const char POE_PORT_EVENT_DETECTED_STRING[]                = "\"POE PORT EVENT REGISTER CHANGE\""; // AEI_POE_PORT_EVENT_DETECTED              36

const char EVENT_NAME_STRING[]                             = "\"EVENT NAME\"";
const char EVENT_INFO_STRING[]                             = "\"EVENT INFO\"";

const char POWER_GOOD_STATUS_STRING[]                      = "\"POWER GOOD STATUS\"";
const char POWER_ENABLE_STATUS_STRING[]                    = "\"POWER ENABLE STATUS\"";
const char DETECTION_CHANGE_OF_CLASS_STRING[]              = "\"DETECTION CHANGE OF CLASS\"";
const char DETECTION_CHANGE_IN_DETECTION_STRING[]          = "\"DETECTION CHANGE IN DETECTION\"";
const char FAULT_DISCONNECT_STRING[]                       = "\"FAULT DISCONNECT\"";
const char FAULT_OVERLOAD_STRING[]                         = "\"FAULT OVERLOAD\"";
const char START_ILIM_FAULT_STRING[]                       = "\"START ILIM FAULT\"";
const char START_CLASS_DETECT_STRING[]                     = "\"START CLASS DETECT\"";
const char TICK_INFO_STRING[]                              = "\"TICK INFO\"";

const char POWER_EVENT_STRING[]                            = "\"POWER EVENT\"";
const char DETECTION_EVENT_STRING[]                        = "\"DETECTION EVENT\"";
const char FAULT_EVENT_STRING[]                            = "\"FAULT EVENT\"";
const char START_EVENT_STRING[]                            = "\"START EVENT\"";
const char FROM_VALUE_STRING[]                             = "\"FROM VALUE\"";
const char TO_VALUE_STRING[]                               = "\"TO VALUE\"";

const char EVENT_REGISTERS_STRING[] = "\"EVENT REGISTERS\"";
const char DETECTION_STRING[]       = "\"DETECTION\"";
const char FAULT_STRING[]           = "\"FAULT\"";
const char START_STRING[]           = "\"START\"";
const char SUPPLY_FAULT_STRING[]    = "\"SUPPLY/FAULT\"";

// To support bootloader upgrade by the application:
const char CLI_ARG_COMMAND_STRING[]                        = "command"; // no quotes
const char CLI_ARG_UPGRADE_START_STRING[]                  = "start";
const char CLI_ARG_UPGRADE_DATA_STRING[]                   = "data";
const char CLI_ARG_UPGRADE_END_STRING[]                    = "end";
const char CLI_ARG_UPGRADE_COMMIT_STRING[]                 = "commit";
const char CLI_ARG_UPGRADE_CANCEL_STRING[]                 = "cancel";
const char CLI_ARG_UPGRADE_STATS_STRING[]                  = "stats";
const char CLI_ARG_DATA_EQUAL_STRING[]                     = "data=";
const char CLI_COMMAND_EQUAL_NOT_FOUND_STRING[]            = "\"COMMAND EQUAL NOT FOUND\"";
const char CLI_DATA_FIELD_TOO_LONG_STRING[]                = "\"DATA FIELD TOO LONG\"";
const char CLI_DATA_EQUAL_NOT_FOUND_STRING[]               = "\"DATA EQUAL NOT FOUND\"";
const char CLI_INVALID_UPGRADE_COMMAND_STRING[]            = "\"INVALID UPGRADE COMMAND\"";

// Firmware upgrade response strings returned to the SBC/Gateway. They are
// accompanied by the HRC numeric codes as defined in the apparent.h header file:
const char  UPGRADE_STATE_MACHINE_STRING[]                   = "\"UPGRADE STATE MACHINE\"";
const char  UPGRADE_PACKET_SIZE_STRING[]                     = "\"UPGRADE PACKET SIZE\"";

const char SM_INVALID_STATE_STRING[]                         = "\"INVALID\"";
const char SM_IDLE_STATE_STRING[]                            = "\"IDLE\"";
const char SM_WAIT_NEXT_DATA_PACKET_STATE_STRING[]           = "\"WAIT NEXT DATA PACKET\"";
const char SM_WAIT_END_TRANSFER_STATE_STRING[]               = "\"WAIT END TRANSFER\"";
const char SM_WAIT_APP_FINAL_COMMIT_STATE_STRING[]           = "\"WAIT APP FINAL COMMIT\"";


// The following strings are returned to the SBC/Gateway in response to an upgrade command:

// "Positive" ACKnowledgement strings:
const char  START_ACK_STRING[]                               = "\"start ack\"";
const char  RECEIVE_ACK_STRING[]                             = "\"receive ack\"";
const char  END_ACK_STRING[]                                 = "\"end ack\"";
const char  FINAL_COMMIT_ACK_STRING[]                        =  "\"commit ack\"";
const char  CANCEL_DATA_PKT_STATE_ACK_STRING[]               = "\"cancel in data packet state ack\"";
const char  CANCEL_WAIT_END_TRANSFER_STATE_ACK_STRING[]      = "\"cancel in wait end transfer state ack\"";
const char  CANCEL_WAIT_COMMIT_STATE_ACK_STRING[]            = "\"cancel in wait commit state ack\"";
const char  CANCEL_IDLE_STATE_ACK_STRING[]                   = "\"cancel in idle state ack\"";
const char  STATS_ACK_STRING[]                               = "\"stats ack\"";

// "Negative" N-ACKnowledgements when bad things happen:
const char  AMU_CODING_ERROR_STRING[]                        = "TERRIBLE AMU CODING ERROR"; // <- never expected. Unless the impossible happens.
const char  XFER_COMPONENT_HEX_CONVERT_FAIL_NACK_STRING[]    = "\"XFER COMPONENT HEX CONVERT FAIL NACK\"";
const char  XFER_COMPONENT_CHECKSUM_FAIL_NACK_STRING[]       = "\"XFER COMPONENT CHECKSUM FAIL NACK\"";
const char  XFER_COMPONENT_EVEN_LENGTH_NACK_STRING[]         = "\"XFER COMPONENT EVEN LENGTH NACK\"";
const char  XFER_COMPONENT_MISSING_COLON_NACK_STRING[]       = "\"XFER COMPONENT MISSING COLON NACK\"";
const char  INVAL_STATE_MACHINE_COMMAND_NACK_STRING[]        = "\"INVALID STATE MACHINE COMMAND NACK\"";
const char  INVAL_CMD_IDLE_STATE_NACK_STRING[]               = "\"INVALID UPGRADE COMMAND IN IDLE STATE NACK\"";
const char  INVAL_CMD_NEXT_DATA_PKT_STATE_NACK_STRING[]      = "\"INVALID UPGRADE COMMAND IN NEXT DATA PKT STATE NACK\"";
const char  INVAL_CMD_END_TRANSFER_STATE_NACK_STRING[]       = "\"INVALID UPGRADE COMMAND IN END TRANSFER STATE NACK\"";
const char  XFER_PACKET_COMPONENT_TOO_LONG_NACK_STRING[]     = "\"XFER PACKET COMPONENT TOO LONG NACK\"";
const char  SEGMENT_ADDRESS_INVAL_LENGTH_NACK_STRING[]       = "\"SEGMENT ADDRESS INVAL LENGTH NACK\"";
const char  SEG_ADDR_BUT_HOLD_BUFF_NOT_EMPTY_NACK_STRING[]   = "\"SEG ADDR RECORD BUT HOLD BFFER NOT EMPTY NACK\"";
const char  SEG_ADDR_INVALID_LENGTH_NACK_STRING[]            = "\"SEG ADDR DATA RECORD INVALID LENGTH NACK\"";
const char  DATA_START_INVALID_LENGTH_NACK_STRING[]          = "\"DATA START DATA RECORD INVALID LENGTH NACK\"";
const char  DATA_START_ADDRESS_NOT_ZERO_NACK_STRING[]        = "\"DATA START ADDRESS NOT ZERO NACK\"";
const char  EOF_RECORD_INVALID_STRUCTURE_NACK_STRING[]       = "\"EOF RECORD INVALID STRUCTURE NACK\"";
const char  RELATIVE_ADDR_OUT_OF_SEQUENCE_NACK_STRING[]      = "\"RELATIVE ADDR OUT OF SEQUENCE NACK\"";
const char  DATA_RECORD_NOT_WHOLE_LONGS_NACK_STRING[]        = "\"DATA RECORD NOT WHOLE LONGS NACK\"";
const char  INTERIM_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[]  = "\"HRC INTERIM HOLDING BLOCK COMMIT FAIL NACK\"";
const char  LAST_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[]     = "\"HRC LAST HOLDING BLOCK COMMIT FAIL NACK\"";
const char  EXPECTED_EXT_SEG_ADDR_RECORD_NACK_STRING[]       = "\"HRC EXPECTED EXT SEG ADDR RECORD NACK\"";
const char  EXPECTED_END_OF_FILE_RECORD_NACK_STRING[]        = "\"HRC EXPECTED END OF FILE RECORD NACK\"";
const char  EXPECTED_DATA_RECORD_NACK_STRING[]               = "\"HRC EXPECTED DATA RECORD NACK\"";
const char  UNEXPECTED_SLA_RECORD_NACK_STRING[]              = "\"HRC UNEXPECTED START LINEAR ADDR RECORD NACK\"";
const char  START_EXT_SEG_ADDR_INVALID_NACK_STRING[]         = "\"START EXTENDED SEGMENT ADDRESS INVALID NACK\"";
const char  FLASH_APP_SIGNATURE_ERASE_FAIL_NACK_STRING[]     = "\"FLASH APP SIGNATURE ERASE FAIL NACK\"";
const char  INVALID_COMMAND_IN_COMMIT_STATE_NACK_STRING[]    = "\"INVALID UPGRADE COMMAND IN COMMIT STATE NACK\"";
const char  COMMIT_FAIL_NACK_STRING[]                        = "\"COMMIT FAIL NACK - WRITE FLASH SIG FAILED\"";
const char  RELATIVE_ADDR_NOT_ZERO_NACK_STRING[]             = "\"RELATIVE ADDR NOT ZERO NACK\"";
const char  READ_FLASH_TO_HOLDING_FAIL_NACK_STRING[]         = "\"READ FLASH TO HOLDING FAIL NACK\"";
const char  FLASH_ADDRESS_LOW_ZONE_NACK_STRING[]             = "\"FLASH ADDRESS LOW ZONE NACK\"";
const char  END_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[]       = "\"END ADDRESS WITHIN BOOT ZONE NACK\"";
const char  START_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[]     = "\"START ADDRESS WITHIN BOOT ZONE NACK\"";
const char  WRITE_HOLD_TO_FLASH_GENERIC_FAIL_NACK_STRING[]   = "\"WRITE HOLD TO FLASH GENERIC FAIL NACK\"";
const char  HEX_CHARACTER_NOT_HEX_NACK_STRING[]              = "\"HEX CHARACTER NOT HEX NACK\"";

const char  SAME70_RESET_REASON_STRING[]                     = "\"SAME70 RESET REASON\"";
const char  RESET_INFO_STRING[]                              = "\"RESET INFO\"";
const char  RESET_REASON_UNKNOWN_STRING[]                    = "\"UNKNOWN\"";
const char  RESET_REASON_GENERAL_STRING[]                    = "\"GENERAL\"";
const char  RESET_REASON_BACKUP_STRING[]                     = "\"BACKUP\"";
const char  RESET_REASON_WATCHDOG_STRING[]                   = "\"WATCHDOG\"";
const char  RESET_REASON_SOFTWARE_STRING[]                   = "\"SOFTWARE\"";
const char  RESET_REASON_NRST_LOW_STRING[]                   = "\"NRST LOW\"";

// End of SBC/GATEWAY sensitive definitions.
//
// ----------------------------------------------------------------------------


const char  CRAFT_STRING_TO_PROCESS_STRING[]                 = "\r\nSTRING TO PROCESS: ";



/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
