/**
 *
 * Apparent MAC Resolution utilities and state machine module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2022 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains the state machine and utility support routines for the Apparent MAC Resolution feature..
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include "asf.h"
#include "apparent_strings.h"
#include "apparent.h"
#include "primary_switch.h"
#include "secondary_switch.h"
#include "mac_resolution.h"
#include "alu_mac_info.h"


// In main.c module:
extern void mdelay       (uint32_t ul_dly_ticks);
extern U32  g_ul_ms_ticks;
extern char  pCraftPortDebugData[];

// MACRO FOR TESTING:
#define UART1_WRITE_PACKET_MACRO usart_serial_write_packet((usart_if)UART1, (uint8_t *)pCraftPortDebugData, strlen(pCraftPortDebugData));

// The info control block that maintains the state machine:
static t_macResolutionInfoControlBlock mrInfoCb;


// The data block that maintains info for each being resolved.
static t_macResolutionDataBlock macResoDb[MAC_RESOLUTION_LIST_LENGTH_MAX];

static t_macAsIndexReadResults primaryReadResults;
static t_aluReadDataResults    secondaryReadResults;


// Static Module prototypes:

static void initMacDataBlock           (void);
static BOOL getOneMacFromArgs          (char *pArguments, U32 argListRank, U32 argListlength, U8 *pOneGoodMac);
static void verifySecondaryMacFromDynamicAlu (t_aluReadDataResults *pAluReadData);


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond


// Exported/external functions from this module:



// LOCAL FUNCTIONS:

// getOneMacFromArgs
//
// Get the next mac from an argument "list of MACs" of the form:
//
// 11:11:11:11:11:11,22:22:22:22:22:22, ... ,99:99:99:99:99:99
// 012345678901234567890
//
static BOOL getOneMacFromArgs(char *pArguments, U32 argListRank, U32 argListlength, U8 *pOneGoodMac)
{
    U32   iggy;
    BOOL  getMacResult = TRUE;
    U32   potentialLength;
    char  pOneMacString[MAX_CHARS_IN_MAC_STRING + 1];
    char  pOneMacByteAsString[3];
    U8    oneByte;
    U32   macWriteRank = 0;

    // Check to make sure the search will not go over a cliff.
    potentialLength = argListRank + MAX_CHARS_IN_MAC_STRING;
    if (potentialLength <= argListlength) {
        // Transfer the "next" mac from the argument to the array.
        memset(&pOneMacString[0], 0, MAX_CHARS_IN_MAC_STRING+1);
        memcpy(&pOneMacString[0], &pArguments[argListRank], MAX_CHARS_IN_MAC_STRING);
        // TODO: consider making this an exported function  of sorts.
        // 2 5 8 11 14
        if ((pOneMacString[2] == ':') && (pOneMacString[5] == ':') && (pOneMacString[8] == ':') && (pOneMacString[11] == ':') && (pOneMacString[14] == ':')) {
            // The set of 5 colons in the mac are there.
            // Now check the chars that represent the mac.
            pOneMacByteAsString[2] = 0; // null char
            for (iggy=0; iggy < MAX_CHARS_IN_MAC_STRING; iggy+=3) {
                pOneMacByteAsString[0] = pOneMacString[iggy];
                pOneMacByteAsString[1] = pOneMacString[iggy + 1];
                if (isStringHexDigit(pOneMacByteAsString)) {
                    //
                    oneByte = atoh(pOneMacByteAsString);
                    pOneGoodMac[macWriteRank++] = oneByte;
                } else {
                    // Bad hex digit
                    getMacResult = FALSE;
                    break;
                }
            } // for ...
        } else {
            // Bad colon placement, maybe missing. Bad in any case.
            getMacResult = FALSE;
        }
    } else {
        // The search is off into the weeds.
        getMacResult = FALSE;
    }
    return getMacResult;
}


// initMacDataBlock
//
//
static void initMacDataBlock(void)
{
    U32 iggy;
    for (iggy = 0; iggy < MAC_RESOLUTION_LIST_LENGTH_MAX; iggy++) {
        memset(&macResoDb[iggy].pMac[0], 0, 6);
        macResoDb[iggy].controllerId = CONTROLLER_ID_UNKNOWN;
        macResoDb[iggy].kszPort      = 0;
        macResoDb[iggy].pPort        = (char *)&PORT_UNKNOWN_STRING[0];
    }
}

// initMacResolutionData
//
//
//
//
void initMacResolutionData(void)
{
    mrInfoCb.stateMachineState    = MR_SM_IDLE;
    mrInfoCb.macListLength        = 0;
    mrInfoCb.macListRank          = 0;
    mrInfoCb.secondaryEntries     = 0;
    mrInfoCb.secondaryIndex       = 0;
    mrInfoCb.startTimestamp       = 0;
    mrInfoCb.endTimestamp         = 0;
    mrInfoCb.numberEntriesChanged = NUMBER_OF_ENTRIES_NO_CHANGE;
    initMacDataBlock();
}


// verifySecondaryMacFromDynamicAlu
//
//
//
//
static void verifySecondaryMacFromDynamicAlu(t_aluReadDataResults *pAluReadData)
{
    U32 iggy;

    for (iggy = 0; iggy < mrInfoCb.macListLength; iggy++) {
        if (macResoDb[iggy].controllerId == CONTROLLER_ID_UNKNOWN) {
            // Is this a MAC of interest?
            if (memcmp(&macResoDb[iggy].pMac[0], &pAluReadData->pData[3], 6) == 0) {
                // Found a MAC of interest.
                macResoDb[iggy].controllerId = CONTROLLER_ID_SECONDARY;
                macResoDb[iggy].kszPort      = pAluReadData->kszPort;
                macResoDb[iggy].pPort        = pAluReadData->pDownstreamPort;
                break;
            }
            // The MAC from the secondary MAC table is not that in the resolution list, so move on
            // and check the next MAC in the resolution list.
            //
            // But before doing so, check the filter ID from the MAC table: if it is not 0x00, then this
            // signifies that the index used for reading is past the last valid entry in the secondary ALU.
            // There's no point checking the returned "MAC" (typically bogus) against the remaining entries
            // in the MAC resolution table.
            if (pAluReadData->filterId != 0x00) { break; } // no point checking any further
            else                                {}         // try the next MAC in the resolution table
        }            
    } // for ...
}


// getMacResolutionResult
//
// Get the current result of the MAC resolution process. Output is of the form:
//
// {"CLI":{"COMMAND":"get_mac_resolution_result",
//         "ARGUMENTS":"NONE",
//         "CODE":0,
//         "RESULT":"SUCCESS",
//         "RESPONSE":{"MAC INFO LIST":[{"MAC":"11:11:11:11:11:11","CONTROLLER":"PRIMARY","KSZ_PORT":2,"PORT":"2"},
//                                      {"MAC":"22:22:22:22:22:22","CONTROLLER":"SECONDARY","KSZ_PORT":2,"PORT":"2"},
//                                         .
//                                         .
//                                      {"MAC":"99:99:99:99:99:99","CONTROLLER":"UNKNOWN","KSZ_PORT":255,"PORT":"UNKN OWN"}],
//                     "STATE MACHINE":"IDLE | READING FROM PRIMARY | PREPARE SECONDARY READ | READING FROM SECONDARY",
//                     "SECONDARY ROW COUNT":"NO CHANGE | CHANGED"
// }}}
//
// Note that the last 3 components are within the RESPONSE component.
//
void getMacResolutionResult(void)
{
    U32 iggy;
    BOOL firstTime = TRUE; // to control comma in the json list
    const char *pControllerString;

    // Output the main json title:
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:[",
           CLI_STRING, COMMAND_STRING,   GET_MAC_RESOLUTION_RESULT_CMD,
                       ARGUMENTS_STRING, NONE_STRING, 
                       CODE_STRING,      0,
                       RESULT_STRING,    SUCCESS_STRING,
                       RESPONSE_STRING,  MAC_INFO_LIST_STRING);

    // For each valid MAC in the data block:
    for (iggy = 0; iggy < MAC_RESOLUTION_LIST_LENGTH_MAX; iggy++) {
        if (!isMacNull(&macResoDb[iggy].pMac[0])) {
            //
            if (firstTime) { firstTime = FALSE; }
            else           { printf(",");       }
            if      (macResoDb[iggy].controllerId == CONTROLLER_ID_PRIMARY)   { pControllerString = PRIMARY_STRING;   }
            else if (macResoDb[iggy].controllerId == CONTROLLER_ID_SECONDARY) { pControllerString = SECONDARY_STRING; }
            else  /*(macResoDb[iggy].controllerId == CONTROLLER_ID_UNKNOWN)*/ { pControllerString = UNKNOWN_STRING;   }
            printf("{%s:\"%02X:%02X:%02X:%02X:%02X:%02X\",%s:%s,%s:%lu,%s:\"%s\",%s:%s}", MAC_STRING, macResoDb[iggy].pMac[0], macResoDb[iggy].pMac[1], macResoDb[iggy].pMac[2], 
                                                                                                      macResoDb[iggy].pMac[3], macResoDb[iggy].pMac[4], macResoDb[iggy].pMac[5],
                                                                                          CONTROLLER_STRING,          pControllerString,
                                                                                          KSZ_PORT_STRING,            macResoDb[iggy].kszPort,
                                                                                          PORT_STRING,                macResoDb[iggy].pPort,
                                                                                          SECOND_PRI_ALU_READ_STRING, (macResoDb[iggy].secondAluReadRequired == TRUE ? REQUIRED_STRING : NOT_REQUIRED_STRING));
        } else {
            // This entry has no mac, nothing to display.
        }
    } // for ...
    printf("],"); // Terminate the mac info list with a square bracket. Add a comma for the component.

    // Display the current state of the state machine:
    if      (mrInfoCb.stateMachineState == MR_SM_IDLE)                     { printf("%s:%s,", STATE_MACHINE_STRING, IDLE_STRING);              }
    else if (mrInfoCb.stateMachineState == MR_SM_READING_FROM_PRIMARY)     { printf("%s:%s,", STATE_MACHINE_STRING, CURRENTLY_RUNNING_STRING); }
    else if (mrInfoCb.stateMachineState == MR_SM_PREPARE_SECONDARY_READ)   { printf("%s:%s,", STATE_MACHINE_STRING, CURRENTLY_RUNNING_STRING); }
    else  /*(mrInfoCb.stateMachineState == MR_SM_READING_FROM_SECONDARY)*/ { printf("%s:%s,", STATE_MACHINE_STRING, CURRENTLY_RUNNING_STRING); }

    // Display the number of (secondary) entries changed or not indicator (omit the last comma, this is the last item in the json-like string):
    if      (mrInfoCb.numberEntriesChanged == NUMBER_OF_ENTRIES_NO_CHANGE)  { printf("%s:%s", SECONDARY_NUMBER_ENTRIES_STRING, UNCHANGED_STRING); }
    else if (mrInfoCb.numberEntriesChanged == NUMBER_OF_ENTRIES_INCREASED)  { printf("%s:%s", SECONDARY_NUMBER_ENTRIES_STRING, INCREASED_STRING); }
    else                                    /*NUMBER_OF_ENTRIES_DECREASED*/ { printf("%s:%s", SECONDARY_NUMBER_ENTRIES_STRING, DECREASED_STRING); }

    printf("}}}\r\n"); // Close off the entire json string with 3 curlies.
}


// resolveMacList
//
// launch the mac resolution process. The CLI is of the form:
//
// resolve_mac_list mac_list=11:11:11:11:11:11,22:22:22:22:22:22, .. ,99:99:99:99:99:99
//
// with a list of up to 25 MACs to be resolved. The response is json, of 1 of 2 forms.
// If the command is valid:
//
// {"CLI":{"COMMAND":"resolve_mac_list","ARGUMENTS":"mac_list=11:11:11:11:11:11...",CODE:0,"RESULT":"SUCCESS","RESPONSE":{"INFO":"STATE MACHINE LAUNCHED"}}
//
// If the command is rejected:
//
//
// This: {"CLI":{"COMMAND":"resolve_mac_list","ARGUMENTS":"mac_list=11:11:11:11:11:11...",CODE=0,"RESULT":"FAIL","RESPONSE":{"INFO":"GET ALU MAC SM RUNNING"}}}
// or:   {"CLI":{"COMMAND":"resolve_mac_list","ARGUMENTS":"mac_list=11:11:11:11:11:11...",CODE=0,"RESULT":"FAIL","RESPONSE":{"INFO":"INVALID ARGUMENTS"}}}
// or:   {"CLI":{"COMMAND":"resolve_mac_list","ARGUMENTS":"mac_list=11:11:11:11:11:11...",CODE=0,"RESULT":"FAIL","RESPONSE":{"INFO":"INVALID MAC LIST"}}}
// or:   {"CLI":{"COMMAND":"resolve_mac_list","ARGUMENTS":"mac_list=11:11:11:11:11:11...",CODE=0,"RESULT":"FAIL","RESPONSE":{"INFO":"CURRENTLY RUNNING"}}}
//
void resolveMacList(char *pUserInput)
{
    U32    iggy;
    char *pArguments;
    U32    argListlength;
    U32    argListRank;
    U8    pOneGoodMac[6];
    BOOL   getMacResult;
    U32    macDbInsertRank       = 0;
    U32    numberOfMacsToResolve = 0;

    pArguments = &pUserInput[strlen(RESOLVE_MAC_LIST_CMD) + 1];
    if (pArguments == NULL) {
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                           ARGUMENTS_STRING, NULL_STRING,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  INFO_STRING, INVALID_ARGUMENTS_STRING);
    } else if (mrInfoCb.stateMachineState != MR_SM_IDLE) {
        // This state machine is currently active.
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                           ARGUMENTS_STRING, pArguments,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  INFO_STRING, MAC_RESOLUTION_RUNNING_STRING);
    } else if (isAluMacInfoSmIdle() == FALSE) {
        // The GET ALU MAC INFO state machine is currently active.
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                           ARGUMENTS_STRING, pArguments,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  INFO_STRING, ALU_MAC_RUNNING_STRING);
    } else {
        // Neither is active, so launching the state machine can now be considered.
        // Parse the argument list for all valid MACs.
        argListlength = strlen(pArguments); // Avoids having to compute this multiple times.
        // Advance the argument list rank to index the 1st char past "mac list" argument specifier.
        // It should point to the equal sign.
        argListRank = strlen(CLI_ARG_MAC_LIST_STRING);
        if (pArguments[argListRank] == '=') {
            // Found the equal. Increment by 1 to index the 1st character of the list of comma-separated MACs.
            // Clear the table of MACs that the state machine will be working on (i.e., discard leftovers from
            // a previous resolution process).
            initMacDataBlock();
            argListRank++;

            for (iggy = 0; iggy < MAC_RESOLUTION_LIST_LENGTH_MAX; iggy++) {
                getMacResult = getOneMacFromArgs(pArguments, argListRank, argListlength, &pOneGoodMac[0]);
                if (getMacResult) {
                    // Insert this MAC into the MAC data block.
                    memcpy(&macResoDb[macDbInsertRank++].pMac[0], &pOneGoodMac[0], 6);
                    numberOfMacsToResolve++;
                } else {
                    //
                    numberOfMacsToResolve = 0;
                    initMacDataBlock();
                    printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                            CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                                        ARGUMENTS_STRING, pArguments,
                                        CODE_STRING,      0,
                                        RESULT_STRING,    FAIL_STRING,
                                        RESPONSE_STRING,  INFO_STRING, EXTRACT_1_MAC_FAILED_STRING);
                    break;
                }
                // Advance the argument list rank to index the 1st char of the next MAC and prepare to extract that next MAC.
                argListRank += MAX_CHARS_IN_MAC_STRING;
                if (pArguments[argListRank] == 0) {
                    // The next char to check would be the null char - end of the line
                    break;
                } else if (pArguments[argListRank] == ',') {
                    // More to check. Advance the rank by 1 to index the 1st char of the next MAC.
                    argListRank++;
                } else if (pArguments[argListRank] == ' ') {
                    // Consider this the end of the list, with the assumption that the END-OF-INPUT signature follows.
                    break;
                } else {
                    // Not the end of the string, and not a comma - the user's mac list smells funky.
                    numberOfMacsToResolve = 0;
                    initMacDataBlock();
                    printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                           CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                                       ARGUMENTS_STRING, pArguments,
                                       CODE_STRING,      0,
                                       RESULT_STRING,    FAIL_STRING,
                                       RESPONSE_STRING,  INFO_STRING, INVALID_MAC_LIST_STRING);
                    break;
                }                        
            } // for ...

            // There should have been at least 1 or more MACs to resolve.
            if (numberOfMacsToResolve > 0) {
                // Print the reply and launch the state machine.
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                       CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESULT_STRING,    SUCCESS_STRING,
                                   RESPONSE_STRING,  INFO_STRING, STATE_MACHINE_LAUNCHED_STRING);
                mrInfoCb.macListRank       = 0;
                mrInfoCb.macListLength     = numberOfMacsToResolve;
                mrInfoCb.stateMachineState = MR_SM_LAUNCH;
            } else {} // There was an error - already caught above. Nothing to do here.
        } else {
            //
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                   CLI_STRING, COMMAND_STRING,   RESOLVE_MAC_LIST_CMD,
                               ARGUMENTS_STRING, pArguments,
                               CODE_STRING,      0,  
                               RESULT_STRING,    FAIL_STRING,
                               RESPONSE_STRING,  INFO_STRING, INVALID_PARAMETERS_STRING);
        }
    }
}


BOOL isMacResolutionSmIdle (void)
{
    return (mrInfoCb.stateMachineState == MR_SM_IDLE ? TRUE : FALSE);
}




// macResolutionStateMachine
//
// Called from main. The state machine will transition from IDLE to the next state if the
// resolution control block indicates the presence of a MAC list. See resolveMacList(xxx)
// above on the setting up of the MAC resolution control block.
//
void checkMacResolutionSm(void)
{
    U32  iggy;
    BOOL allResolved = TRUE;

    switch (mrInfoCb.stateMachineState) {
        case MR_SM_IDLE:
            // Nothing to do, so do plenty of it. Remain in the IDLE state.
            break;

        case MR_SM_LAUNCH:
            if (mrInfoCb.macListLength == 0) {
                // Nothing to do - coming here is a coding error. Return to the IDLE state.
                mrInfoCb.stateMachineState = MR_SM_IDLE;
            } else {
                // Resolve 1 or more MACs. Start with the PRIMARY controller.
                mrInfoCb.startTimestamp    = g_ul_ms_ticks;
                mrInfoCb.stateMachineState = MR_SM_READING_FROM_PRIMARY;

#if ENABLE_ALU_MAC_DEBUG == TRUE
                sprintf(&pCraftPortDebugData[0], "CMR: READ PRI LAUNCH MAC=%02X:%02X:%02X:%02X:%02X:%02X\r\n",
                        macResoDb[mrInfoCb.macListRank].pMac[0], macResoDb[mrInfoCb.macListRank].pMac[1],
                        macResoDb[mrInfoCb.macListRank].pMac[2], macResoDb[mrInfoCb.macListRank].pMac[3],
                        macResoDb[mrInfoCb.macListRank].pMac[4], macResoDb[mrInfoCb.macListRank].pMac[5]);
                UART1_WRITE_PACKET_MACRO
#endif

            }
            break;

        case MR_SM_READING_FROM_PRIMARY:
            // Check the primary controller for the current MAC.
            readAluPrimaryWithMacAsIndex(&macResoDb[mrInfoCb.macListRank].pMac[0], &primaryReadResults);

            macResoDb[mrInfoCb.macListRank].secondAluReadRequired = primaryReadResults.secondAluReadRequired;

            // If the MAC was found and is valid, it is echo'd in the results block. Check it.
            if (memcmp(&macResoDb[mrInfoCb.macListRank].pMac[0], &primaryReadResults.pMac[0], 6) == 0) {
                // The current MAC is in the primary ALU. It might be one of the downstream ports 1/2/3/4/LAN,
                // in which case it need not be checked in the secondary ALU. Or it could be the RGMII port,
                // in which case the MAC is on a downstream port of the SECONDARY ALU.
                macResoDb[mrInfoCb.macListRank].kszPort      = primaryReadResults.kszPort;
                macResoDb[mrInfoCb.macListRank].pPort        = primaryReadResults.pDownstreamPort;
                if ((macResoDb[mrInfoCb.macListRank].kszPort >= VALID_PRIMARY_KSZ_PORT_NUMBER_MIN) && (macResoDb[mrInfoCb.macListRank].kszPort <= VALID_PRIMARY_KSZ_PORT_NUMBER_MAX)) {
                    macResoDb[mrInfoCb.macListRank].controllerId = CONTROLLER_ID_PRIMARY;

                } else {
                    // The mac used as index was read back, but with an invalid downstream port. Most likely
                    // it is the RGMII port. The lookup will continue with the secondary controller.
                    // Otherwise, nothing else to do here.
                }                          
            } else {
                // The returned MAC is not the desired MAC. This MAC is unknown to the primary controller. This can be due to several reasons,
                // such as: a bogus MAC, a MAC that does not exist in the system, or a MAC that was aged out of the
                // primary ALU. It is entirely possible that this MAC is currently being served by the primary controller, 
                // but aging removed it, and there has since been no ethernet packets exchanged between the SBC and the device.
                // It is possible it may be found in the ALU of the secondary controller. Otherwise, nothing else to do here.
            }

            // Prepare to resolve the next entry in the list if any remain to be resolved.
            mrInfoCb.macListRank++;
            if (mrInfoCb.macListRank >= mrInfoCb.macListLength) {
                // All the MACs in the resolution list have been attempted via the primary controller.
                // There may be 1 or more MACs in the table that did not resolve, so time to try with
                // the secondary controller if necessary.

                for (iggy = 0; iggy < mrInfoCb.macListLength; iggy++) {
                    if (macResoDb[iggy].controllerId == CONTROLLER_ID_UNKNOWN) {
                        // This entry did not resolve, so check the macResoDb against the secondary controller.
                        allResolved = FALSE;
                        break;
                    }
                }
                if (allResolved == TRUE) {
                    mrInfoCb.stateMachineState = MR_SM_IDLE;
                    mrInfoCb.macListLength     = 0;
                    mrInfoCb.endTimestamp      = g_ul_ms_ticks;

#if ENABLE_ALU_MAC_DEBUG == TRUE
                    sprintf(&pCraftPortDebugData[0], "CMR DONE, READ SEC NOT REQUIRED\r\n"); UART1_WRITE_PACKET_MACRO
#endif

                } else {
                    mrInfoCb.stateMachineState = MR_SM_PREPARE_SECONDARY_READ;
                    mrInfoCb.macListRank       = 0;
                }                

            } else {} // There's at least 1 more MAC to resolve. Resume the next time from this state.

            pokeWatchdog();

            break;

        case MR_SM_PREPARE_SECONDARY_READ:
            // Read ALU entry with index 0 to get the number of valid entries in the ALU. This count is spread out over 2 registers.
            // The data sheet does not say much about this count, but it appears that it is an "index", so add 1 to get the "actual"
            // count, and add 2 extra for good luck and insurance. This will likely result in reading 1 or more bogus entry from the
            // secondary ALU, but at least the reading will not stop with valid entries remaining in the table, in case new entries
            // are added based on network activity. In addition, the "number of entries" retrieved from each ALU reading is checked
            // against the last one to see if it has been updated.
            readOneSecondaryDynamicAluEntry(0, &secondaryReadResults);
            mrInfoCb.secondaryEntries     = secondaryReadResults.numberEntries + EXTRA_SECONDARY_READS;
            mrInfoCb.numberEntriesChanged = NUMBER_OF_ENTRIES_NO_CHANGE;
            mrInfoCb.stateMachineState    = MR_SM_READING_FROM_SECONDARY;

#if ENABLE_ALU_MAC_DEBUG == TRUE
            sprintf(&pCraftPortDebugData[0], "CMR: READ SEC LAUNCH, entries=%lu\r\n", mrInfoCb.secondaryEntries); UART1_WRITE_PACKET_MACRO
#endif

            // Although this is not the state that reads from the secondary ALU, data from the 1st secondary ALU entry that was just
            // read out might contain valid data to resolve an entry in the resolution table. Check it. And prepare to go to the reading
            // loop.
            verifySecondaryMacFromDynamicAlu(&secondaryReadResults);
            mrInfoCb.secondaryIndex = 1;

            pokeWatchdog();

            break;

        case MR_SM_READING_FROM_SECONDARY:
            // Read an entry and verify the results. 
            readOneSecondaryDynamicAluEntry(mrInfoCb.secondaryIndex, &secondaryReadResults);
            verifySecondaryMacFromDynamicAlu(&secondaryReadResults);

            // Any change in number of entries from the previous read?
            if (mrInfoCb.numberEntriesChanged == NUMBER_OF_ENTRIES_NO_CHANGE) {
                // Only check if there has been no change - so far. It is very possible for the
                // number of entries to increase or decrease when reading a large number of rows
                // from the secondary ALU, due to aging or re-insertion of 1 or more devices.
                if ((secondaryReadResults.numberEntries + EXTRA_SECONDARY_READS) > (U16)mrInfoCb.secondaryEntries) {
                    mrInfoCb.numberEntriesChanged = NUMBER_OF_ENTRIES_INCREASED;
                } else if ((secondaryReadResults.numberEntries + EXTRA_SECONDARY_READS) < (U16)mrInfoCb.secondaryEntries) {
                    mrInfoCb.numberEntriesChanged = NUMBER_OF_ENTRIES_DECREASED;
                } else {} // ??? can this happen? Probably not.
            }

            // Prepare for the next read when called from main.
            mrInfoCb.secondaryIndex++;
            if (mrInfoCb.secondaryIndex >= mrInfoCb.secondaryEntries) {
                // Done.
                mrInfoCb.stateMachineState = MR_SM_IDLE;
                mrInfoCb.macListLength     = 0;
                mrInfoCb.endTimestamp      = g_ul_ms_ticks;

#if ENABLE_ALU_MAC_DEBUG == TRUE
                sprintf(&pCraftPortDebugData[0], "CMR: DONE SEC KSZPORT/MAC=%lu/%02X:%02X:%02X:%02X:%02X:%02X\r\n",
                                                 macResoDb[mrInfoCb.macListRank].kszPort,
                                                 macResoDb[mrInfoCb.macListRank].pMac[0], macResoDb[mrInfoCb.macListRank].pMac[1],
                                                 macResoDb[mrInfoCb.macListRank].pMac[2], macResoDb[mrInfoCb.macListRank].pMac[3],
                                                 macResoDb[mrInfoCb.macListRank].pMac[4], macResoDb[mrInfoCb.macListRank].pMac[5]);
                UART1_WRITE_PACKET_MACRO
#endif

            }

            pokeWatchdog();

            break;

        default:
            // Coding error. Increment a counter.
            incrementBadEventCounter(CHECK_MAC_RESOLUTION_SM_CODING_ERROR);
            mrInfoCb.stateMachineState = MR_SM_IDLE;
            mrInfoCb.macListLength     = 0;
            break;
    }
}


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
