/**
 *
 * Apparent POE utilities module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2021 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains utility support routines for the Apparent Gateway PCBA system.
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "pmc.h" // ???
#include "ASF/sam/utils/cmsis/same70/include/instance/pioc.h"

#include "usart.h"
#include "ASF/sam/utils/cmsis/same70/include/component/twihs.h"
#include "status_codes.h"
#include "uart_serial.h"

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "board.h"

#include "apparent_strings.h"
#include "apparent.h"
#include "primary_switch.h"
#include "secondary_switch.h"
#include "poe.h"
#include "i2c.h"


extern U32  g_ul_ms_ticks;

// In main.c module:
extern void mdelay       (uint32_t ul_dly_ticks);

// In apparent.c module:
extern char pCraftPortDebugData[];

// POE OPERATING MODE AUTOMATIC by port index lookup table:
//
//  BITS:   xx  xx  xx  xx  xx  xx  xx  xx 
//  PORT:   44  33  22  11  88  77  66  55
static const U16 POE_OPERATING_MODE_AUTOMATIC_BY_PORT_INDEX_LOOKUP [] = { POE_OPERATING_MODE_AUTOMATIC_PORT_1,   // [0] port 1: 0b11 <<  8
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_2,   // [1] port 2: 0b11 << 10
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_3,   // [2] port 3: 0b11 << 12
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_4,   // [3] port 4: 0b11 << 14
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_5,   // [4] port 5: 0b11 <<  0
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_6,   // [5] port 6: 0b11 <<  2
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_7,   // [6] port 7: 0b11 <<  4
                                                                          POE_OPERATING_MODE_AUTOMATIC_PORT_8    // [7] port 8: 0b11 <<  6
                                                                        };

// POE POWER ENABLE by port index lookup table:
//
// Used to evaluate the value on a per-port basis to use when enabling or disabling
// POE power via the POE "POWER ENABLE" command register.
//
static const U16 POE_POWER_ENABLE_BY_PORT_INDEX_LOOKUP []  = { POE_POWER_ENABLE_PORT_1,   // [0] port 1 
                                                               POE_POWER_ENABLE_PORT_2,   // [1] port 2 
                                                               POE_POWER_ENABLE_PORT_3,   // [2] port 3 
                                                               POE_POWER_ENABLE_PORT_4,   // [3] port 4 
                                                               POE_POWER_ENABLE_PORT_5,   // [4] port 5 
                                                               POE_POWER_ENABLE_PORT_6,   // [5] port 6 
                                                               POE_POWER_ENABLE_PORT_7,   // [6] port 7 
                                                               POE_POWER_ENABLE_PORT_8    // [7] port 8 
                                                             };
static const U16 POE_POWER_DISABLE_BY_PORT_INDEX_LOOKUP [] = { POE_POWER_DISABLE_PORT_1,  // [0] port 1 
                                                               POE_POWER_DISABLE_PORT_2,  // [1] port 2 
                                                               POE_POWER_DISABLE_PORT_3,  // [2] port 3 
                                                               POE_POWER_DISABLE_PORT_4,  // [3] port 4 
                                                               POE_POWER_DISABLE_PORT_5,  // [4] port 5 
                                                               POE_POWER_DISABLE_PORT_6,  // [5] port 6 
                                                               POE_POWER_DISABLE_PORT_7,  // [6] port 7 
                                                               POE_POWER_DISABLE_PORT_8   // [7] port 8 
                                                             };

// From the data sheet register 50 section 9.6.2.61 "AUTO CLASS CONTROL Register" (page 95):
// - bits [11..8] and [3..0] = Auto Class Auto Adjustment Enable bits are 0
// - bit [15]=port 4, [14]=port 3, [13]=port 2, [12]=port 1,
// - bit  [7]=port 8,  [6]=port 7,  [5]=port 6,  [4]=port 5,
static const U16 POE_PAC_CONTROL_BIT_BY_PORT_INDEX_LOOKUP [] = { 0b0001000000000000,  // [0] port 1
                                                                 0b0010000000000000,  // [1] port 2
                                                                 0b0100000000000000,  // [2] port 3
                                                                 0b1000000000000000,  // [3] port 4
                                                                 0b0000000000010000,  // [4] port 5
                                                                 0b0000000000100000,  // [5] port 6
                                                                 0b0000000001000000,  // [6] port 7
                                                                 0b0000000010000000   // [7] port 8
                                                               };




// From the data sheet register 1C section 9.6.2.24 "Connection Check and Auto Class Status Register" (page 70):
// - bits [11..8] and [3..0] = reserved
// - bit [15]=port 4, [14]=port 3, [13]=port 2, [12]=port 1,
// - bit  [7]=port 8,  [6]=port 7,  [5]=port 6,  [4]=port 5
//
// - if the bit is set:   auto class IS     supported
// - if the bit is clear: auto class IS NOT supported
static const U16 POE_AUTO_CLASS_STATUS_BIT_BY_PORT_INDEX_LOOKUP [] = { 0b0001000000000000,  // [0] port 1
                                                                       0b0010000000000000,  // [1] port 2
                                                                       0b0100000000000000,  // [2] port 3
                                                                       0b1000000000000000,  // [3] port 4
                                                                       0b0000000000010000,  // [4] port 5
                                                                       0b0000000000100000,  // [5] port 6
                                                                       0b0000000001000000,  // [6] port 7
                                                                       0b0000000010000000   // [7] port 8
                                                                     };

static const t_viCommandsElement POE_VI_COMMANDS_BY_PORT_PAIR_INDEX_LOOKUP [] =
                       { {POE_VOLTAGE_PORT_1_5_COMMAND, POE_CURRENT_PORT_1_5_COMMAND, 0, 4},  // [0][4] port pair 1/5
                         {POE_VOLTAGE_PORT_2_6_COMMAND, POE_CURRENT_PORT_2_6_COMMAND, 1, 5},  // [1][5] port pair 2/6
                         {POE_VOLTAGE_PORT_3_7_COMMAND, POE_CURRENT_PORT_3_7_COMMAND, 2, 6},  // [2][6] port pair 3/7
                         {POE_VOLTAGE_PORT_4_8_COMMAND, POE_CURRENT_PORT_4_8_COMMAND, 3, 7}   // [3][7] port pair 4/8
                       };

static const t_autoClassCommandsElement POE_AUTO_CLASS_CONFIG_PORT_PAIR_INDEX_LOOKUP [] =
                       { {POE_POLICE_CONFIG_PORT_15_COMMAND, 0, 4},  // [0] port pair 1/5 command 0x1E
                         {POE_POLICE_CONFIG_PORT_26_COMMAND, 1, 5},  // [1] port pair 2/6 command 0x1F
                         {POE_POLICE_CONFIG_PORT_37_COMMAND, 2, 6},  // [2] port pair 3/7 command 0x20
                         {POE_POLICE_CONFIG_PORT_48_COMMAND, 3, 7}   // [3] port pair 4/8 command 0x21
                       };

static const t_pacCommandsElement POE_PAC_PORT_PAIR_INDEX_LOOKUP [] = { { POE_PEAK_AVE_POWER_PORT_1_5_COMMAND, 0, 4}, // [0] port pair 1/5 command 0x51
                                                                        { POE_PEAK_AVE_POWER_PORT_2_6_COMMAND, 1, 5}, // [1] port pair 2/6 command 0x52
                                                                        { POE_PEAK_AVE_POWER_PORT_3_7_COMMAND, 2, 6}, // [2] port pair 3/7 command 0x53
                                                                        { POE_PEAK_AVE_POWER_PORT_4_8_COMMAND, 3, 7}  // [3] port pair 4/8 command 0x54
                                                                      };

#if 1
static const U32 POE_PORT_RANK_TO_KSZ_PORT_RANK_LOOKUP []  = { 0,   // [0] poe port rank 0 equates to KSZ primary   port rank 0
                                                               1,   // [1] poe port rank 1 equates to KSZ primary   port rank 2
                                                               2,   // [2] poe port rank 2 equates to KSZ primary   port rank 3
                                                               3,   // [3] poe port rank 3 equates to KSZ primary   port rank 4
                                                               0,   // [4] poe port rank 4 equates to KSZ secondary port rank 0
                                                               1,   // [5] poe port rank 5 equates to KSZ secondary port rank 1
                                                               2,   // [6] poe port rank 6 equates to KSZ secondary port rank 2
                                                               3    // [7] poe port rank 7 equates to KSZ secondary port rank 3
                                                           };
#endif

// The data block that maintains POE-centric info on a per-port basis.
static t_portPoeInfoElement portPoeInfoDb[MAX_NUMBER_DOWNSTREAM_PORTS];

// The data control block that maintains info relative to the TPS POE controller subsystem.
static t_poeInfoControlBlock poeInfoCb;

// For autonomous event reporting.
static char pPoeJsonString[POE_SINGLE_JSON_EVENT_STRING_SIZE];
static char pMiniJsonString[MINI_JSON_STRING_SIZE];


// Static Module prototypes:

static void  displayPoeHighLevelBaseInfo         (void);
static BOOL  poeGetPowerReadings                 (U32 portPairRank);
static BOOL  isPortLinkStatusUp                  (U32 portIndex);
static void  poeCheckPortPowerChanges            (void);
static void  poeCheckPacChanges                  (void);
static void  poeCheckAutoClassConfigChanges      (void);
static BOOL  isReadPoeEventRegister              (U32 eventIndex, U16 *pPoeData);
static BOOL  isUpdateAutoClassConfigOnePair      (U32 portPairRank);
static char *getAutoClassString                  (U8 autoClass);
static void  rewritePowerEnableRegister          (void);
static void  checkPoeDeviceTemperature           (void);
static void  checkPoeDeviceVoltage               (void);
static BOOL  checkPoePortPower                   (void);
static void  checkPortPowerThresholds            (void);
static F32   getPowerClassLimit                  (U8 autoClass);
static BOOL  checkPoePortAutoClassConfig         (void);
static BOOL  pacStateMachine                     (void);
static void  checkAutoClassStatus                (void);
static void  checkSupplyFaultRegister            (void);
static void  checkEventRegisters                 (void);
static void  pGetPortsListFromChangedData        (U16 thisWord, U16 thatWord, char *pSomeString);

static BOOL  isDevicePoe                         (U8 autoClass);
static void  initPoeControllerStateMachine       (void);

// MACRO FOR TESTING:
#define UART1_WRITE_PACKET_MACRO usart_serial_write_packet((usart_if)UART1, (uint8_t *)pCraftPortDebugData, strlen(pCraftPortDebugData));


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond


// Exported/external functions from this module:


// configurePoeController
//
// Called from main during system restart, to configure I2C over the SAME70 TWISH1 interface.
// This enables access to all functions of the TPS23882 POE controller. Refer to the Texas Instruments
// "TPS23882 Type-3 2-Pair 8-Channel PoE 2 PSE Controller" data sheet.
//
// Configuring the I2C interface over the SAME70 TWISH1 requires that:
//
// - configure CCFG_SYSIO to select PB4,PB5 function
// - enable Peripheral Clocks for TWIHS1
// - configure PA3,PA4 for TWIHS1 Peripheral A Function
// - set TWIHS1 SCLK Frequency to 99kHz
// - enable TWIHS1 Master Mode and disable all other modes
// - set FIFODIS, ACMDIS, PECDIS, SMBDIS, HSDIS, SVDIS, MSEN
//
void configurePoeController(void)
{
    U32 regValue;
    U32 newRegValue;
    U16 poeRegisterValue;
    U16 poeNewRegisterValue;
    U32 iggy;
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    U8  deviceId;
    U8  manufacturerId;
    #endif

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    // Called during system initialization.
    printf("-- Configuring I2C/TWIHS1 POE controller interface (PIO ***B***)\r\n");
    #endif

    // 1: Init control block data:
    poeInfoCb.lastCheckPowerEnableTick     = 0;
    poeInfoCb.powerEnableRegister          = POE_POWER_ENABLE_REGISTER_DEFAULT;
    poeInfoCb.padWithWord1                 = 0xFFFF;
    poeInfoCb.inputVoltage                 = 0.00;
    poeInfoCb.lastCheckTemperatureTick     = 0;
    poeInfoCb.lastCheckVoltageTick         = 0;

    // POE port power:
    poeInfoCb.checkPortPowerTick           = 0;
    poeInfoCb.nextRankPortPairViToRead     = 0;

    // POE auto class get-config data:
    poeInfoCb.lastCheckAutoClassConfigTick = 0;
    poeInfoCb.checkAutoClassConfigTimeout  = POE_CHECK_AC_CONFIG_TIMEOUT;
    poeInfoCb.nexRankPortPairAcToRead      = 0;

    // POE PAC (Peak Average power Calculation):
    poeInfoCb.pacCommandRegisterValue      = 0;
    poeInfoCb.padWithWord2                 = 0xFFFF;
    poeInfoCb.lastCheckPacTick             = 0;
    poeInfoCb.nexRankPortPairPacToRead     = 0;
    poeInfoCb.pacSmState                   = PAC_SM_IDLE;
    poeInfoCb.pacReadLoopCount             = 0;

    // POE auto class Status register:
    poeInfoCb.lastCheckAutoClassStatusTick = 0;
    poeInfoCb.acStatusRegister             = 0;
    poeInfoCb.padWithWord3                 = 0xFFFF;

    // POE supply/status register checking:
    poeInfoCb.lastCheckSupplyStatusTick    = 0;
    poeInfoCb.supplyStatusRegistervalue    = 0;
    poeInfoCb.padWithWord4                 = 0xFFFF;

    poeInfoCb.poeDebugOutputEnabled        = FALSE; // TRUE when testing

    poeInfoCb.controllerCheckingState      = PCC_SM_START;
    poeInfoCb.poeCheckingStartedTick       = 0;
    poeInfoCb.checkingEnabled              = FALSE;  // wait for SBC/Gateway keep-alive before setting to TRUE

    // The set of port-based event registers.
    poeInfoCb.nextEventCommand             = POE_POWER_EVENT_RO_CLEAR_COMMAND;
    poeInfoCb.powerEventRegisterValue      = 0xFFFF;
    poeInfoCb.detectionEventRegisterValue  = 0xFFFF;
    poeInfoCb.faultEventRegisterValue      = 0xFFFF;
    poeInfoCb.startEventRegisterValue      = 0xFFFF;
    poeInfoCb.lastDetectionChangeTick      = 0;
    poeInfoCb.lastFaultChangeTick          = 0;
    poeInfoCb.lastStartChangeTick          = 0;

    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {
        portPoeInfoDb[iggy].voltageRegRawValue             = 0;
        portPoeInfoDb[iggy].amperageRegRawValue            = 0;
        portPoeInfoDb[iggy].volts                          = 0.00;
        portPoeInfoDb[iggy].amps                           = 0.00;
        portPoeInfoDb[iggy].wattsPrevious                  = 0.00;
        portPoeInfoDb[iggy].wattsPresent                   = 0.00;
        portPoeInfoDb[iggy].acConfigPresent                = POE_AUTO_CLASS_NOT_POE;
        portPoeInfoDb[iggy].acConfigPrevious               = POE_AUTO_CLASS_NOT_POE;
        portPoeInfoDb[iggy].pacRegRawValuePresent          = 0;
        portPoeInfoDb[iggy].pacRegRawValuePrevious         = 0;
    }


    // 2: CCFG_SYSIO must be configured to select PB4,PB5 function
    regValue    =  readSame70Register((uint32_t)&REG_CCFG_SYSIO);

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - CCFG_SYSIO:                      read_reg  addr=%08lX = 0x%08lX     <- current value\r\n", (uint32_t)&REG_CCFG_SYSIO, regValue);
    #endif

    newRegValue = regValue | CCFG_SYSIO_SYSIO4 | CCFG_SYSIO_SYSIO5;

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Config CCFG_SYSIO/PB4,PB5:       write_reg addr=%08lX value=%08lX (from %08lX)\r\n", (uint32_t)&REG_CCFG_SYSIO, newRegValue, regValue);
    #endif

    writeSame70Register((uint32_t)&REG_CCFG_SYSIO, newRegValue);

    // 3: Enable the peripheral clocks - display before/after status.
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Enable PMC_PCER1/TWIHS1:         write_reg addr=%08lX value=%08X (expected 0x200)\r\n",  (uint32_t)&REG_PMC_PCER1, TWISH1_CLOCK_ENABLE_VALUE);
    printf("  - *BEFORE* PMC_PCER1 STATUS:       read_reg  addr=%08lx = 0x%08lX\r\n", (uint32_t)&REG_PMC_PCSR1, readSame70Register((uint32_t)&REG_PMC_PCSR1));
    #endif

    writeSame70Register((uint32_t)&REG_PMC_PCER1, TWISH1_CLOCK_ENABLE_VALUE);

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - *AFTER*  PMC_PCER1 STATUS:       read_reg  addr=%08lx = 0x%08lX\r\n", (uint32_t)&REG_PMC_PCSR1, readSame70Register((uint32_t)&REG_PMC_PCSR1));
    #endif

    // 4: Configure PB4 (PIO_PDR_P4) and PB5 (PIO_PDR_P5) for TWIHS1 Peripheral B Function (line 606 onwards of the H/W test script).
    regValue = PIO_PDR_P4 + PIO_PDR_P5; // 0x30

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - CHANGED CONFIG FOR PIO **B**\r\n");
    printf("  - Configure PIO/PB4,PB5:           write_reg addr=%08lX value=%08lX\r\n", (uint32_t)&REG_PIOB_PDR, regValue);
    #endif

    writeSame70Register((uint32_t)&REG_PIOB_PDR, regValue);

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - REG_PIOB_PSR:                    read_reg  addr=%08lX = 0x%08lX\r\n", (uint32_t)&REG_PIOB_PSR,    readSame70Register((uint32_t)&REG_PIOB_PSR));
    printf("  - REG_PIOB_ABCDSR:                 read_reg  addr=%08lX = 0x%08lX\r\n", (uint32_t)&REG_PIOB_ABCDSR, readSame70Register((uint32_t)&REG_PIOB_ABCDSR));
    #endif

    // 5: Set TWIHS1 SCLK Frequency (100 kHz max)
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Set TWIHS1 SCK to 99 kHz:        write_reg addr=%08lX value=%08X)\r\n", (uint32_t)&REG_TWIHS1_CWGR, TWIHS1_SCLK_FREQUENCY_VALUE);
    #endif

    writeSame70Register((uint32_t)&REG_TWIHS1_CWGR, TWIHS1_SCLK_FREQUENCY_VALUE);

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - TWIHS1_CWGR:                     read_reg  addr=%08lX = 0x%08lX\r\n", (uint32_t)&REG_TWIHS1_CWGR, readSame70Register((uint32_t)&REG_TWIHS1_CWGR));
    #endif

    // 6: Enable TWIHS1 Master Mode and disable all other modes. Set FIFODIS, ACMDIS, PECDIS, SMBDIS, HSDIS, SVDIS, MSEN
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Enable TWIHS1 Master Mode:       write_reg addr=%08lX value=%08X)\r\n", POE_REG_TWIHS_CR, TWIHS1_ENABLE_MM_DISABLE_OTHERS);
    #endif

    writeSame70Register(POE_REG_TWIHS_CR, TWIHS1_ENABLE_MM_DISABLE_OTHERS);


    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP

    // 7: Read the POE device ID, ensure it is the expected value.
    deviceId = poeReadDeviceId();
    printf("  - POE DEVICE ID/0x%lX = %02X\r\n", POE_DEVICE_ID_COMMAND, deviceId);

    // 8: Read the POE manufacturer ID, ensure it is the expected value.
    manufacturerId = poeReadManufacturerId(OUTPUT_NOTHING);
    printf("  - POE MFR ID/0x%lX = %02X\r\n", POE_MFR_ID_COMMAND, manufacturerId);

    #endif

    // 9. Clear the POE temperature register. It is updated from the main loop.
    poeInfoCb.temperatureRegister = (U8)0;

    // 10: Configure the POE for Configuration B register access (allows for 16-bit read and write):
    if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_GENERAL_MASK_COMMAND, I2C_DATA_TYPE_BYTE, &poeRegisterValue)) {
        pokeWatchdog();
        poeNewRegisterValue = poeRegisterValue | POE_N_BIT_ACC_MASK | POE_CLCHE_MASK | POE_DECHE_MASK;
        if (poeNewRegisterValue == poeRegisterValue) {

            #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
            printf("  - GENERAL MASK/0X%lX already set to 0x%08lX)\r\n", POE_GENERAL_MASK_COMMAND, poeRegisterValue);
            #endif

        } else {
            #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
            printf("  - set GENERAL MASK/0X%lX register from 0x%08lX -> 0x%08lX)\r\n", POE_GENERAL_MASK_COMMAND, poeRegisterValue, poeNewRegisterValue);
            #endif

            i2cWriteGenericData(POE_GENERAL_MASK_COMMAND, poeNewRegisterValue, I2C_DATA_TYPE_BYTE);
        }
    } else {
        // Never observed. Question: what kind of evasive action is expected if it should?
        incrementBadEventCounter(POE_STARTUP_CONFIG_ERROR);
    }

    // 11. Set the operating mode for all downstream ports to their defaults, in this application: AUTOMATIC.
    poeInfoCb.operatingModeRegister = POE_OPERATING_MODE_REGISTER_DEFAULT;

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - set POE OPERATING MODE/0x%lX register to default = 0x%04X\r\n", POE_OPERATING_MODE_COMMAND, poeInfoCb.operatingModeRegister);
    #endif

    if (!i2cWriteGenericData(POE_OPERATING_MODE_COMMAND, poeInfoCb.operatingModeRegister, I2C_DATA_TYPE_WORD)) {
        // Never observed. Same question as above ...
        incrementBadEventCounter(POE_STARTUP_CONFIG_ERROR);
    }

    // 12. Finally, write to the "power enable" register to power up all ports.
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - set POE POWER ENABLE/0x%lX register to default = 0x%04X\r\n", POE_POWER_ENABLE_COMMAND, poeInfoCb.powerEnableRegister);
    #endif

    if (!i2cWriteGenericData(POE_POWER_ENABLE_COMMAND, poeInfoCb.powerEnableRegister, I2C_DATA_TYPE_WORD)) {
        // Never observed. Again, same question as above ...
        incrementBadEventCounter(POE_STARTUP_CONFIG_ERROR);
    }

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - I2C/TWIHS1 POE controller config complete\r\n");
    #endif

}


void setPoeEnabled(BOOL enable) {
    poeInfoCb.checkingEnabled = enable;
}


// enablePoeChecking//
//
//
// {"CLI":{"COMMAND":"enable_poe_checking","ARGUMENTS":"NONE","CODE":1,"RESPONSE":{"enable_poe_checking":"POE checking is ENABLED"},"RESULT":"SUCCESS"}}
// {"CLI":{"COMMAND":"disable_poe_checking","ARGUMENTS":"NONE","CODE":1,"RESPONSE":{"disable_poe_checking":"POE checking is *DISABLED*"},"RESULT":"SUCCESS"}}
void enablePoeCheckingCmd(BOOL enable)
{
    if (enable == TRUE) {
        if (poeInfoCb.checkingEnabled == FALSE) {
            poeInfoCb.checkingEnabled = TRUE;
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   ENABLE_POE_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  ENABLE_POE_CHECKING_CMD, POE_CHECKING_ENABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            poeInfoCb.controllerCheckingState = PCC_SM_START;
            sprintf(&pCraftPortDebugData[0], "POE checking is enabled\r\n"); UART1_WRITE_PACKET_MACRO
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   ENABLE_POE_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  ENABLE_POE_CHECKING_CMD, POE_CHECKING_WAS_ENABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            sprintf(&pCraftPortDebugData[0], "POE checking was already enabled - no change made\r\n"); UART1_WRITE_PACKET_MACRO
        }
    } else {
        if (poeInfoCb.checkingEnabled == TRUE) {
            poeInfoCb.checkingEnabled = FALSE;
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   DISABLE_POE_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  DISABLE_POE_CHECKING_CMD, POE_CHECKING_DISABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            poeInfoCb.controllerCheckingState = PCC_SM_START;
            sprintf(&pCraftPortDebugData[0], "POE checking is DISABLED\r\n"); UART1_WRITE_PACKET_MACRO
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   DISABLE_POE_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  DISABLE_POE_CHECKING_CMD, POE_CHECKING_WAS_ENABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            sprintf(&pCraftPortDebugData[0], "POE checking was already DISABLED - no change made\r\n"); UART1_WRITE_PACKET_MACRO
        }
    }
}


// poeSetPortPowerUpDown
//
// Applies only to POE devices.
//
// This interface will write 2 bytes/16-bits over the POE interface. Power over any
// downstream port 1..8 is managed by controlling both the OPERATING MODE and the
// POWER ENABLE registers.
//
//
// OPERATING MODE REGISTER:
//
// From the TPS23882 data sheet page 49:
//
// - when a port is enabled and power is ON:   the port is to be placed in AUTO mode
// - when a port is disabled and power is OFF: the port is to be placed in MANUAL mode
//
// 16 bits:
//
//  <---             MSB for ports 4,3,2,1              --->|<---             LSB for ports 8,7,6,5              --->
//  [C4M1] [C4M0] [C3M1] [C3M0] [C2M1] [C2M0] [C1M1] [C1M0] | [C8M1] [C8M0] [C7M1] [C7M0] [C6M1] [C6M0] [C5M1] [C5M0]
//
// Any port in AUTOMATIC MODE: CxM1 CxM0 = 1 1 ... 0x11
// Any port in MANUAL    MODE: CxM1 CxM0 = 0 1 ... 0x01
//
//
// POWER ENABLE REGISTER:
//
// From the TPS23882 data sheet page 63:
//
// In this interface: - a port is turned "OFF" by: setting  the POFFn bit, clearing the PWONx bit
//                    - a port is turned "ON"  by: clearing the POFFx bit, setting  the PWONx bit
//
// 16 bits:
//
//  <---                  MSB for ports 4,3,2,1                 --->|<---                  LSB for ports 8,7,6,5                 --->
//  [POFF4] [POFF3] [POFF2] [POFF1] [PWON4] [PWON3] [PWON2] [PWON1] | [POFF8] [POFF7] [POFF6] [POFF5] [PWON8] [PWON7] [PWON6] [PWON5]
void poeSetPortPowerUpDown(U8 portIndex, U32 powerAction)
{
    U16    tempRegister;
    U16    portMask;

    if (powerAction == POE_PORT_POWER_UP) {
        // Power enable is achieved by: - OPERATING MODE register: set CxM1/CxM0 bits to AUTOMATIC for the port in question 
        //                              - POWER ENABLE register:   clear POFFx and set PWONx bits for the port in question.
        //
        // First: set the OPERATING MODE bits for this port to AUTOMATIC mode and write the register.
        tempRegister  = poeInfoCb.operatingModeRegister;
        portMask      = POE_OPERATING_MODE_AUTOMATIC_BY_PORT_INDEX_LOOKUP[portIndex];
        tempRegister |= portMask;   // <- this sets CxM1/CxM0 bits since auto mode = 0b11

        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PORT %u ENABLE, OPERATING MODE FROM 0x%04X -> 0x%04X ", (portIndex+1), poeInfoCb.operatingModeRegister, tempRegister);
                                                UART1_WRITE_PACKET_MACRO
                                             }

        poeInfoCb.operatingModeRegister = tempRegister;
        i2cWriteGenericData(POE_OPERATING_MODE_COMMAND, poeInfoCb.operatingModeRegister, I2C_DATA_TYPE_WORD);

        // Next: Generate the POWER ENABLE register to enable power on this port.
        // - the POFFx bit must be CLEAR for x = port 1..8
        // - the PWONx bit must be SET   for x = port 1..8
        // Start by CLEAR the POFFx bit.
        tempRegister  = poeInfoCb.powerEnableRegister;
        portMask      = POE_POWER_DISABLE_BY_PORT_INDEX_LOOKUP[portIndex];
        tempRegister &= (~portMask);
        // Now SET the PWONx bit.
        portMask      = POE_POWER_ENABLE_BY_PORT_INDEX_LOOKUP[portIndex];
        tempRegister |= portMask;

        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POWER ENABLE FROM 0x%04X -> 0x%04X\r\n", poeInfoCb.powerEnableRegister, tempRegister);
                                                UART1_WRITE_PACKET_MACRO
                                             }

        poeInfoCb.powerEnableRegister = tempRegister;
        i2cWriteGenericData(POE_POWER_ENABLE_COMMAND, poeInfoCb.powerEnableRegister, I2C_DATA_TYPE_WORD);

    } else /*(portCommand == PORT_COMMAND_POWER_DOWN)*/ {
        // Power disable is achieved by: - OPERATING MODE register: set CxM1/CxM0 bits to OFF for the port in question
        //                               - PORT ENABLE register:    set POFFx and CLEAR PWONx bits for the port in question.
        //
        // First: set the OPERATING MODE register for this port to OFF mode and write the register.
        tempRegister  = poeInfoCb.operatingModeRegister;
        portMask      = POE_OPERATING_MODE_AUTOMATIC_BY_PORT_INDEX_LOOKUP[portIndex];
        tempRegister &= (~portMask);   // <- this clears  CxM1/CxM0 bits since auto mode = 0b11

        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PORT %u DISABLE, OPERATING MODE FROM 0x%04X -> 0x%04X ", (portIndex+1), poeInfoCb.operatingModeRegister, tempRegister);
                                                UART1_WRITE_PACKET_MACRO
                                             }
        poeInfoCb.operatingModeRegister = tempRegister;
        i2cWriteGenericData(POE_OPERATING_MODE_COMMAND, poeInfoCb.operatingModeRegister, I2C_DATA_TYPE_WORD);

        // Next: Generate the POWER ENABLE register to disable power on this port.
        // - the POFFx bit must be SET
        // - the PWONx bit must be CLEAR
        // Start by setting the POFFx bit.
        tempRegister  = poeInfoCb.powerEnableRegister;
        portMask      = POE_POWER_DISABLE_BY_PORT_INDEX_LOOKUP[portIndex];
        tempRegister |= portMask;
        // Now clear the PWONx bit.
        portMask      = POE_POWER_ENABLE_BY_PORT_INDEX_LOOKUP[portIndex];
        tempRegister &= (~portMask);

        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POWER DISABLE FROM 0x%04X -> 0x%04X\r\n", poeInfoCb.powerEnableRegister, tempRegister);
                                                UART1_WRITE_PACKET_MACRO
                                                }

        poeInfoCb.powerEnableRegister = tempRegister;
        i2cWriteGenericData(POE_POWER_ENABLE_COMMAND, poeInfoCb.powerEnableRegister, I2C_DATA_TYPE_WORD);
    }

    return;
}


// initPoeControllerStateMachine
//
//
static void initPoeControllerStateMachine(void)
{
    poeInfoCb.controllerCheckingState = PCC_SM_IDLE;
    poeInfoCb.poeCheckingStartedTick  = g_ul_ms_ticks;
}


// checkPoeControllerSm
//
//
// Called from the main loop to periodically update/refresh the TPS POE device for 1 and only 1, of the following items:
//
// - POWER ENABLE: refresh the "power enable" register
// - TEMPERATURE or VOLTAGE: refresh either the POE temperature, or the POE controller input voltage.
// - PORT PAIR V/I: read out the voltage and current readings for a specified
//                  POE "port pair" [0]/[4], or [1]/[5], or [2]/[6] or [3]/[7].
//
// NOTE: the POWER ENABLE register refresh is a work-around for the following bug:
//
// -----------------------------------------------
// TPS23882 POE device bug work-around:
//
// - to remove power from a downstream ports, the port in question must be set to OFF MODE by writing to the OPERATING MODE register.
// - if a port is NOT powered down, then the port in question is set to AUTOMATIC MODE.
// - if a port has no device connected to the RJ45 jack, well, who cares.
// - B U G: if an empty RJ45 port connector, normally powered up, suddenly has a device connected to it,
//          power is NOT applied. The device will sit there, and never receive power.
// - SOLUTION: re-write the POWER ENABLE register. Then power will be applied as expected. Well, within 5 seconds, that is.
//
// - WORK_AROUND: since it cannot be made known in advance if and when a device might be connected to an empty RJ45 connected,
//                this function is called from the main loop, which determines if it is time to "refresh" the POWER ENABLE
//                register.
//
void checkPoeControllerSm(void)
{
    switch (poeInfoCb.controllerCheckingState) {

        case PCC_SM_START:
            if (poeInfoCb.checkingEnabled) {
                // Can now go to the idle state and do real work. This state machine never comes back here.
                sprintf(&pCraftPortDebugData[0], "POE: AMU now managed, POE checking activated\r\n"); UART1_WRITE_PACKET_MACRO
                poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_5_POWER; // GO STRAIGHT TO 5 - BYPASS PHASE 1/2/3/4 OK!!!!!
            }
            break;                


        // THIS SET OF STATES ARE SKIPPED OVER, FROM PCC_SM_PRELIM_PHASE_1 ...
        case PCC_SM_PRELIM_PHASE_1:                                                                                    // <- NOT USED ...
            // Some weird stuff with the TI POE controller. Of the  4 event registers that are tracked, the data sheet specifies
            // that, insofar as the DETECTION EVENT register is concerned (page 39): "These bits are cleared when channel-n is turned off".
            // So all 8 channels are turned OFF, then ON. According to the data sheet page 63: "Writing a �1� at POFFn and PWONn on same Channel
            // during the same write operation turns the Channel off". So 0xFFFF is written to the POWER ENABLE register to turn all ports OFF,
            // and thereafter, 0x0F0F is written to the POWER_ENABLE register. In order to correctly write to the POWER_ENABLE register, the
            // OPERATING_MODE register must be manipulated as well, as follows:
            //
            // 1: OPERATING_MODE = 0
            // 2: POWER_ENABLE   = FFFF
            // 3: OPERATING_MODE = FFFF
            // 4: POWER_ENABLE   = 0F0F
            //
            // RESULT: seemingly did nothing of value. Since the system overall, and the POE device in particular were always
            //         running correctly without any issues of note, the 4 states from PCC_SM_PRELIM_PHASE_1 .. PCC_SM_PRELIM_PHASE_4
            //         are now skipped over. Eventually, they will be removed, but could easily be re-instated in a future release,
            //         or in a test/debug environment.
            i2cWriteGenericData(POE_OPERATING_MODE_COMMAND, 0x0000, I2C_DATA_TYPE_WORD);
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_2;
            break;

        case  PCC_SM_PRELIM_PHASE_2:                                                                                   // <- NOT USED ...
            i2cWriteGenericData(POE_POWER_ENABLE_COMMAND,   0xFFFF, I2C_DATA_TYPE_WORD);
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_3;
            break;

            case  PCC_SM_PRELIM_PHASE_3:                                                                               // <- NOT USED ...
            i2cWriteGenericData(POE_OPERATING_MODE_COMMAND, 0xFFFF, I2C_DATA_TYPE_WORD);
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_4;
            break;

        case  PCC_SM_PRELIM_PHASE_4:                                                                                   // <- NOT USED ...
            i2cWriteGenericData(POE_POWER_ENABLE_COMMAND,   0x0F0F, I2C_DATA_TYPE_WORD);
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_5_POWER;
            break;
        // ... TO THIS STEP, PCC_SM_PRELIM_PHASE_4 ...



        // The 4 POE event registers are read using "read-only-and clear-on-read" commands, so that
        // the registers are cleared. An initial set of values extracted for subsequent validation.
        case PCC_SM_PRELIM_PHASE_5_POWER:
            i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_POWER_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD,          &poeInfoCb.powerEventRegisterValue);
            //sprintf(&pCraftPortDebugData[0], "STARTUP: POWER/%02X=0x%04X ", (U8)POE_POWER_EVENT_RO_CLEAR_COMMAND,     poeInfoCb.powerEventRegisterValue); UART1_WRITE_PACKET_MACRO       // <- COMMENT OUT/DELETE
            poeInfoCb.lastPowerChangeTick     = g_ul_ms_ticks;
            poeInfoCb.poeCheckingStartedTick  = g_ul_ms_ticks;
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_5_DETECTION;
            break;
        case PCC_SM_PRELIM_PHASE_5_DETECTION:
            i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_DETECTION_EVENT_RO_CLEAR_COMMAND, I2C_DATA_TYPE_WORD,          &poeInfoCb.detectionEventRegisterValue);
            //sprintf(&pCraftPortDebugData[0], "DETECTION/%02X=0x%04X ",      (U8)POE_DETECTION_EVENT_RO_CLEAR_COMMAND, poeInfoCb.detectionEventRegisterValue); UART1_WRITE_PACKET_MACRO   // <- COMMENT OUT/DELETE
            poeInfoCb.lastDetectionChangeTick = g_ul_ms_ticks;
            poeInfoCb.poeCheckingStartedTick  = g_ul_ms_ticks;
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_5_FAULT;
            break;
        case PCC_SM_PRELIM_PHASE_5_FAULT:
            i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_FAULT_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD,         &poeInfoCb.faultEventRegisterValue);
            //sprintf(&pCraftPortDebugData[0], "FAULT/%02X=0x%04X ",          (U8)POE_FAULT_EVENT_RO_CLEAR_COMMAND,     poeInfoCb.faultEventRegisterValue); UART1_WRITE_PACKET_MACRO       // <- COMMENT OUT/DELETE
            poeInfoCb.lastFaultChangeTick     = g_ul_ms_ticks;
            poeInfoCb.poeCheckingStartedTick  = g_ul_ms_ticks;
            poeInfoCb.controllerCheckingState = PCC_SM_PRELIM_PHASE_5_START;
            break;
        case PCC_SM_PRELIM_PHASE_5_START:
            i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_START_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD,         &poeInfoCb.startEventRegisterValue);
            //sprintf(&pCraftPortDebugData[0], "START/%02X=0x%04X\r\n",       (U8)POE_START_EVENT_RO_CLEAR_COMMAND,     poeInfoCb.startEventRegisterValue); UART1_WRITE_PACKET_MACRO       // COMMENT OUT/DELETE
            poeInfoCb.lastStartChangeTick     = g_ul_ms_ticks;
            poeInfoCb.poeCheckingStartedTick  = g_ul_ms_ticks;
            poeInfoCb.controllerCheckingState = PCC_SM_IDLE;
            break;

        // From this point onwards: the IDLE state is the beginning of the core of this state machine.
        // The state machine will not longer go to any of the previous state. Unless the system is reset.

        case PCC_SM_IDLE:
            if ((g_ul_ms_ticks - poeInfoCb.poeCheckingStartedTick) >= POE_CHECK_ALL_SETTLING_DOWN_TIMEOUT) {
                // System now settled down, go to the 1st real state.
                poeInfoCb.controllerCheckingState = PCC_SM_REWRITE_POWER_ENABLE;
            } else {} // Not settled down - wait some more.
            break;

        case PCC_SM_REWRITE_POWER_ENABLE:
            // Check if the power enable register needs to be re-written. It is a single action,
            // and this state machine can proceed to the next state.
            rewritePowerEnableRegister();
            poeInfoCb.controllerCheckingState = PCC_SM_READ_TEMPERATURE;
            break;

        case PCC_SM_READ_TEMPERATURE:
            // Check the POE device  temperature.
            checkPoeDeviceTemperature();
            poeInfoCb.controllerCheckingState = PCC_SM_READ_INPUT_VOLTAGE;
            break;

        case PCC_SM_READ_INPUT_VOLTAGE:
            // Check the POE device input voltage. It is a single action,
            // and this state machine can proceed to the next state.
            checkPoeDeviceVoltage();
            poeInfoCb.controllerCheckingState = PCC_SM_PORT_POWER;
            break;

        case PCC_SM_PORT_POWER:
            // Check the POE voltage/current for all 4 port-pairs and calculate port power.
            if (checkPoePortPower()) {
                // Completed - advance to the next state. On the other hand, there might have been a failure.
                poeInfoCb.controllerCheckingState = PCC_SM_PAC_SM;
            }
            break;

        case PCC_SM_PAC_SM:
            // Check the POE PAC ("Peak Average power Calculation") for all 4 port pairs.
            // As a state machine, remain here until it has gone to the idle state.
            if (pacStateMachine()) {
                // Completed - advance to the next state. On the other hand, there might have been a failure.
                poeInfoCb.controllerCheckingState = PCC_SM_AUTO_CLASS_CONFIG;
            }
            break;

        case PCC_SM_AUTO_CLASS_CONFIG:
            // Check the POE port auto class config registers for all 4 port-pairs. This function
            // is re-entered, so remain until it is completed.
            if (checkPoePortAutoClassConfig()) {
                // Completed - advance to the next state. On the other hand, there might have been a failure.
                poeInfoCb.controllerCheckingState = PCC_SM_AUTO_CLASS_STATUS;
            }
            break;

        case PCC_SM_AUTO_CLASS_STATUS:
            // Check the POE auto class status register for all 4 port-pairs. It is a single action,
            // and this state machine can proceed to the next state.
            checkAutoClassStatus();
            poeInfoCb.controllerCheckingState = PCC_SM_SUPPLY_FAULT_REGISTER;
            break;

        case PCC_SM_SUPPLY_FAULT_REGISTER:
            // Check the POE device's supply/fault register.
            checkSupplyFaultRegister();
            poeInfoCb.controllerCheckingState = PCC_SM_EVENT_REGISTERS;
            break;

        case PCC_SM_EVENT_REGISTERS:
            // Check 1 of the 4 event registers.
            checkEventRegisters();
            poeInfoCb.controllerCheckingState = PCC_SM_IDLE;
            break;

        default:
            // error ...
            incrementBadEventCounter(POE_CHECK_CONTROLLER_SM_ERROR);
            initPoeControllerStateMachine();
            break;
    } // switch ... 

    pokeWatchdog();

}


// rewritePowerEnableRegister
//
// Re-write the POWER ENABLE register with the last commanded value.
//
static void rewritePowerEnableRegister(void) {
    if ((g_ul_ms_ticks - poeInfoCb.lastCheckPowerEnableTick) > POE_CHECK_POWER_ENABLE_TIMEOUT) {
        poeInfoCb.lastCheckPowerEnableTick = g_ul_ms_ticks; // For the next time.
        if (i2cWriteGenericData(POE_POWER_ENABLE_COMMAND, poeInfoCb.powerEnableRegister, I2C_DATA_TYPE_WORD)) {
            pokeWatchdog();

            if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: POWER MODE RE-WRITTEN COMMAND=0x%lX REG=0x%04X tick=:%lu\r\n", POE_POWER_ENABLE_COMMAND, poeInfoCb.powerEnableRegister, g_ul_ms_ticks);
                                                   UART1_WRITE_PACKET_MACRO
                                                 }
        } else {} // Bad event noted, nothing to do.
    }
    return;
}


// checkPoeDeviceTemperature
//
// Read the POE controller temperature register (it's a 1-byte read),
// or read the POE controller input voltage register (it's a word/2-byte read).
//
//
static void checkPoeDeviceTemperature(void)
{
    if ((g_ul_ms_ticks - poeInfoCb.lastCheckTemperatureTick) > POE_CHECK_DEVICE_TEMPERATURE_TIMEOUT) {
        poeInfoCb.lastCheckTemperatureTick = g_ul_ms_ticks; // For the next time.
        if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_TEMPERATURE_COMMAND, I2C_DATA_TYPE_BYTE, &poeInfoCb.temperatureRegister)) {
            pokeWatchdog();

            if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: TEMPERATURE COMMAND=0x%lX REG=0x%04X tick=%lu\r\n", POE_TEMPERATURE_COMMAND, poeInfoCb.temperatureRegister, g_ul_ms_ticks);
                                                   UART1_WRITE_PACKET_MACRO
                                                 }
        } else {} // Bad event noted, nothing to do.
    }
    return;
}


// checkPoeDeviceVoltage
//
// Read the POE controller input voltage register (it's a 1-word/2-byte read).
//
// Note: when reading the POE_INPUT_VOLTAGE_COMMAND register, the 1st byte read out is the LSB byte,
//       the 2nd byte is the MSB, returned as the 1 word = [MSB][LSB] so no swapping is required.
//
static void checkPoeDeviceVoltage(void)
{
    U16   regValue;
    U16   maskedValue;

    if ((g_ul_ms_ticks - poeInfoCb.lastCheckVoltageTick) > POE_CHECK_DEVICE_VOLTAGE_TIMEOUT) {
        poeInfoCb.lastCheckVoltageTick = g_ul_ms_ticks; // For the next time.
        if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_INPUT_VOLTAGE_COMMAND, I2C_DATA_TYPE_WORD, &regValue)) {
            pokeWatchdog();
            maskedValue  = regValue & INPUT_VOLTAGE_WORD_MASK;
            poeInfoCb.inputVoltage = (maskedValue * INPUT_VOLTAGE_VSTEP_FACTOR) / 1000;

            if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: INPUT VOLTAGE COMMAND=0x%lX REG=0x%04X MASKED=0x%04X -> %2.1fv tick=%lu\r\n",
                                                                                    POE_INPUT_VOLTAGE_COMMAND, regValue, maskedValue, poeInfoCb.inputVoltage, g_ul_ms_ticks);
                                                   UART1_WRITE_PACKET_MACRO
                                                 }
        } else {} // Bad event noted, nothing to do.

    }
    return;
}


// poeCheckPortPowerChanges
//
// Go through the port power info block, and if there are any changes in power,
// write to the autonomous info block. Check to 2 decimal places.
//
// The autonomous event info string will look something like:
//
// {"EVT":{"EI":18,"POE PORT POWER":[{"PORT":"3","POWER":"1.02"}, ... ,{"PORT":"7","POWER":"2.96"}]}}
//
// If all 8 ports are POE and a change in power is to be reported for all 8 ports, up to about 226 chars
// will have been written, reasonably well within the 256-char limit for a single autonomous event.
static void poeCheckPortPowerChanges(void)
{
    U32  iggy;
    BOOL firstTime = TRUE;
    U32  charsWritten;

    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);

    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {

        // Comparing floats generates a "dangerous" compile warning, so they are compared compare as
        // a pair of longs after simple multiplication.
        if (!isTwoFloatsEqual(portPoeInfoDb[iggy].wattsPrevious, portPoeInfoDb[iggy].wattsPresent, 1)) {
            if (firstTime) {
                // Write the title for this jsonesque string and the 1st port with changed power.
                // On 1st time through: string length will be within max length.
                sprintf(pPoeJsonString, "{%s:{%s:%u,%s:[{%s:\"%lu\",%s:\"%2.1f\"}", EVT_STRING, EVENT_ID_STRING,  AEI_POE_PORT_POWER_CHANGE,
                                        POE_PORT_POWER_STRING,
                                        PORT_STRING,  (iggy+1),
                                        POWER_STRING, portPoeInfoDb[iggy].wattsPresent);
                firstTime = FALSE;
            } else {
                // Add the next port with changed power; don't forget the leading comma:
                memset(pMiniJsonString, 0, MINI_JSON_STRING_SIZE);
                charsWritten = sprintf(pMiniJsonString, ",{%s:\"%lu\",%s:\"%2.1f\"}", PORT_STRING, (iggy+1), POWER_STRING, portPoeInfoDb[iggy].wattsPresent);
                if ((charsWritten + strlen(pPoeJsonString)) > POE_JSON_SAFETY_MARGIN_CHARS) {
                    // Quite unexpected.
                    incrementBadEventCounter(POE_AUTO_STRING_EXCEEDS_SAFETY_MARGIN);
                    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE); // prevents writing the autonomous event below.
                    break;
                } else {
                    strcat(pPoeJsonString, pMiniJsonString);
                }
            }
        }

        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PORT %lu POWER %2.1f->%2.1f\r\n",
                                                                                (iggy+1), portPoeInfoDb[iggy].wattsPrevious, portPoeInfoDb[iggy].wattsPresent);
                                               UART1_WRITE_PACKET_MACRO
                                             }

        // Update the previous reading for the next time around.
        portPoeInfoDb[iggy].wattsPrevious = portPoeInfoDb[iggy].wattsPresent;
    } // for ...

    // If there was at least 1 POE port power change, make it available for retrieval by the Gateway.
    // Don't forget to close off the json-like string with 1 square bracket (close off the list) and 2 curly brackets.
    if (strlen(pPoeJsonString) != 0) {
        strcat(pPoeJsonString, "]}}");
        addJsonToAutonomnousString(pPoeJsonString);
    }

}


// checkPortPowerThresholds
//
// Go through the port power info block, and if any power measurements exceed the threshold for its class,
// write to the autonomous info block. The info string will look something like:
//
// {"EVT":{"EI":22,"POE POWER THRESHOLD EXCEEDED":[{"PORT":"3","AUTO CLASS":"CLASS 3","AUTO CLASS LIMIT":"15.0","POWER":"16.2"}, ...
//                                                 {"PORT":"5","AUTO CLASS":"CLASS 3","AUTO CLASS LIMIT":"15.0","POWER":"18.6"}]}}
//                                                 |<-----                       78 chars                               ----->|
//
static void  checkPortPowerThresholds(void)
{
    U32  iggy;
    U32  charsWritten;
    F32  powerClassLimit;
    BOOL firstTime = TRUE;

    // The POE json string has been sized 680 bytes, for the very unlikely event where all 8 ports are POE and all 8 ports
    // suddenly exceeded their power limit. The info for a single port is around 78 bytes, so if all 8 ports were to be written,
    // then 8 * 78 = 624 bytes are reuqired.

    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);

    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {

        // 1: Check if the device is POE.
        if (portPoeInfoDb[iggy].acConfigPrevious == POE_AUTO_CLASS_NOT_POE) {
            // This port is not POE/unequipped. Nothing to do.
        } else {
            // 2: It's POE - check the calculated power. Is the last calculated power greater than its class limit?
            // Note: code repetition could be avoided in the following if/else when constructing the autonomous event,
            //       but controlling when to add the comma-separator makes it more complicated than is merited.
            powerClassLimit = getPowerClassLimit(portPoeInfoDb[iggy].acConfigPrevious);
            if (portPoeInfoDb[iggy].wattsPresent > powerClassLimit) {
                if (firstTime) {
                    // Write the title for this jsonesque string and the 1st port with its calculated power.
                    sprintf(pPoeJsonString, "{%s:{%s:%u,%s:[{%s:\"%lu\",%s:%s,%s:\"%2.1f\",%s:\"%2.1f\"}",
                                            EVT_STRING, EVENT_ID_STRING,
                                            AEI_POE_PORT_POWER_THRESHOLD_EXCEEDED, POE_POWER_THRESHOLD_EXCEEDED_STRING,
                                            PORT_STRING,                 (iggy+1),
                                            POE_AUTO_CLASS_STRING,       getAutoClassString(portPoeInfoDb[iggy].acConfigPresent),
                                            POE_AUTO_CLASS_LIMIT_STRING, powerClassLimit,
                                            POWER_STRING,                portPoeInfoDb[iggy].wattsPresent);
                    firstTime = FALSE;
                } else {
                    // Add the next port with its calculated power with a leading comma separator.
                    // This string is about 78 bytes long.
                    memset(pMiniJsonString, 0, MINI_JSON_STRING_SIZE);
                    charsWritten = sprintf(pMiniJsonString, ",{%s:\"%lu\",%s:%s,%s:\"%2.1f\",%s:\"%2.1f\"}",
                                                            PORT_STRING,                 (iggy+1),
                                                            POE_AUTO_CLASS_STRING,       getAutoClassString(portPoeInfoDb[iggy].acConfigPresent),
                                                            POE_AUTO_CLASS_LIMIT_STRING, powerClassLimit,
                                                            POWER_STRING,                portPoeInfoDb[iggy].wattsPresent);
                    if ((charsWritten + strlen(pPoeJsonString)) > POE_JSON_SAFETY_MARGIN_CHARS) {
                        // Quite unexpected.
                        incrementBadEventCounter(POE_AUTO_STRING_EXCEEDS_SAFETY_MARGIN);
                        memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE); // prevents writing the autonomous event below.
                        break;
                    } else {
                        strcat(pPoeJsonString, pMiniJsonString);
                    }
                }
            }
        }
    } // for ...

    // If there was at least 1 POE port power change, make it available for retrieval by the Gateway.
    // Don't forget to close off the json-like string with 1 square bracket (close off he list) and 2 curly brackets.
    if (strlen(pPoeJsonString) != 0) {
        strcat(pPoeJsonString, "]}}");
        addJsonToAutonomnousString(pPoeJsonString);
    }
}


// checkPoePortPower
//
// Read the voltage and current for a port-pair and calculate each port's power.
// If the readings for all port-pairs are done, check for any changes in POE device
// power and report as an autonomous event. 
//
//
static BOOL checkPoePortPower(void)
{
    BOOL completed = TRUE;

    if ((g_ul_ms_ticks - poeInfoCb.checkPortPowerTick) > POE_CHECK_PORT_POWER_TIMEOUT) {
        if (poeInfoCb.nextRankPortPairViToRead == 0) {

            if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PORT POWER SM START tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

        }
        if (poeGetPowerReadings(poeInfoCb.nextRankPortPairViToRead)) {

            if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PORT POWER SM READ PAIR %lu tick=%lu\r\n", poeInfoCb.nextRankPortPairViToRead, g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

            poeInfoCb.nextRankPortPairViToRead += 1;
            if (poeInfoCb.nextRankPortPairViToRead >= (MAX_NUMBER_DOWNSTREAM_PORTS/2)) {
                // Voltage and current readings for all port pairs is complete, power calculated,
                // all data is stored in the info block. Check this block and report any power changes
                // in the autonomous info block for retrieval by the SBC/Gateway controller.
                poeCheckPortPowerChanges();

                // Check if power thresholds were exceeded.
                checkPortPowerThresholds();

                if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PORT POWER SM COMPLETE tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

                poeInfoCb.nextRankPortPairViToRead = 0;
                poeInfoCb.checkPortPowerTick       = g_ul_ms_ticks;
                completed                          = TRUE;
            } else {
                // Still 1 or more port pairs to check. Set the tick timer to check
                // immediately again upon returning to this function.
                poeInfoCb.checkPortPowerTick = 0;     // upon return to this function: get power readings for next port pair immediately
                completed                    = FALSE;
            }
        } else {
            //
            sprintf(&pCraftPortDebugData[0], "POE: PORT POWER SM FAIL PORR PAIR %lu tick=%lu\r\n", poeInfoCb.nextRankPortPairViToRead, g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO
            poeInfoCb.nextRankPortPairViToRead = 0;
            poeInfoCb.checkPortPowerTick       = g_ul_ms_ticks;
            completed                          = TRUE;
        }            
    }
    return completed;
}


// getPowerClassLimit
//
//
//
//
static F32 getPowerClassLimit(U8 autoClass)
{
    F32 thresholdLimit;

    // This wold be more elegant if done via a lookup table. But there's only to check.
    if      (autoClass == POE_AUTO_CLASS_1) { thresholdLimit = POE_AUTO_CLASS_1_MIN_THRESHOLD; } 
    else if (autoClass == POE_AUTO_CLASS_2) { thresholdLimit = POE_AUTO_CLASS_2_MIN_THRESHOLD; }
    else if (autoClass == POE_AUTO_CLASS_3) { thresholdLimit = POE_AUTO_CLASS_3_MIN_THRESHOLD; }
    else if (autoClass == POE_AUTO_CLASS_4) { thresholdLimit = POE_AUTO_CLASS_4_MIN_THRESHOLD; }
    else                                    { thresholdLimit = POE_AUTO_CLASS_NO_THRESHOLD;    }
    return thresholdLimit;
}


// setAutoClassConfigFastCheck
//
// When a port status changes (link UP -> DOWN or vice versa), reset the auto class config tick timer
// so that the state machine will quickly re-evaluate the auto class. Normally, this state machine is
// entered after a fairly long timer (about 2 minutes), but should be re-launched soon after detection
// of said status change. Set the tick timer to the usual timeout and add the extra that will launch
// the state machine asap.
//
void setAutoClassConfigFastCheck(void)
{
    poeInfoCb.checkAutoClassConfigTimeout = POE_CHECK_AC_CONFIG_FAST_TIMEOUT;
    poeInfoCb.lastCheckAutoClassConfigTick = g_ul_ms_ticks;
}


// checkPoePortAutoClassConfig
//
// Read the auto-class config register for a port-par. If the readings are complete
// for all port-pairs, check for changes.
//
static BOOL checkPoePortAutoClassConfig(void)
{
    BOOL completed = TRUE;

    if ((g_ul_ms_ticks - poeInfoCb.lastCheckAutoClassConfigTick) > poeInfoCb.checkAutoClassConfigTimeout) {
        if (poeInfoCb.poeDebugOutputEnabled) {
            if (poeInfoCb.nexRankPortPairAcToRead == 0) {
                sprintf(&pCraftPortDebugData[0], "POE: AC CONFIG CHECK START tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO
            }
        }                            

        if (isUpdateAutoClassConfigOnePair(poeInfoCb.nexRankPortPairAcToRead)) {
            poeInfoCb.nexRankPortPairAcToRead += 1;
            if (poeInfoCb.nexRankPortPairAcToRead >= (MAX_NUMBER_DOWNSTREAM_PORTS/2)) {
                // Police auto class config checking for all ports is now complete for this cycle complete.
                // Any AC config changes will be reported in the autonomous info block. Reset the tick timer
                // for the next cycle. Also reset the tick timeout in case it was reset to the fast timeout
                // due to port status change. 
                poeCheckAutoClassConfigChanges();
                poeInfoCb.nexRankPortPairAcToRead      = 0;
                poeInfoCb.lastCheckAutoClassConfigTick = g_ul_ms_ticks;
                poeInfoCb.checkAutoClassConfigTimeout  = POE_CHECK_AC_CONFIG_TIMEOUT;
                completed                              = TRUE;

                if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: AC CONFIG CHECK COMPLETE tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

            } else {
                // Still 1 or more port pairs to check. Set the tick timer to check
                // immediately again upon returning to this function.
                poeInfoCb.lastCheckAutoClassConfigTick = 0;     // return to this function asap.
                completed                              = FALSE;
            }
        } else {}            
    }
    return completed;
}


// pacStateMachine
//
// Reading of the PAC "Peak Average Calculated" power requires that:
//
// - a "start bit" be set in the PAC control register
// - read back the PAC control register until the bit is clear
// - read 1 port-pair until done
//
// The sequence of events, once started, takes on the order of 2.4seconds to complete.
//
// The state machine ensures that the minimum actions are executed so that it can
// return to main without hogging too much CPU time. Use the "poe_port_pac" CLI
// to verify the time to perform this action.
//
// NOTE: reading PAC registers is a 1-word read operation over I2C. The data is returned in [MSB][LSB] byte order.
//       The [LSB] contains port info 43/2/1 and the [MSB] contains port info 8/7/6/5. Which is opposite when
//       writing!!! So port data after read must be swapped.  
static BOOL pacStateMachine(void)
{
    U16  pacRegister;
    U16  readWord;
    U32  firstPortRank;
    U32  secondPortRank;
    U32  command;
    U32  iggy;
    BOOL completed = TRUE;

    switch (poeInfoCb.pacSmState) {
        case PAC_SM_IDLE:
            if ((g_ul_ms_ticks - poeInfoCb.lastCheckPacTick) > POE_CHECK_PAC_TIMEOUT) {
                // Set the bits in the control register, write to the control register.
                poeInfoCb.pacCommandRegisterValue = 0;
                for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {
                    if (isDevicePoe(portPoeInfoDb[iggy].acConfigPresent)) {
                        // This port has a POE device, set the bit if the link is UP..
                        if (isPortLinkStatusUp(iggy)) { poeInfoCb.pacCommandRegisterValue |= POE_PAC_CONTROL_BIT_BY_PORT_INDEX_LOOKUP[iggy];  } // set its bit in the register to write
                        else                          {                                                                                       } // port is down, the bit is NOT set
                    }
                } // for ...

                if (poeInfoCb.pacCommandRegisterValue == 0) {
                    // No POE devices, nothing to do.
                    poeInfoCb.lastCheckPacTick = g_ul_ms_ticks;
                    completed                  = TRUE;

                    if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM NO POE DEVICE, STAY IDLE tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

                } else {

                    if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM LAUNCH WITH PAC CMD VALUE 0x%04X tick=%lu\r\n", poeInfoCb.pacCommandRegisterValue, g_ul_ms_ticks);
                        UART1_WRITE_PACKET_MACRO
                    }

                    i2cWriteGenericData(POE_PAC_CONTROL_COMMAND, poeInfoCb.pacCommandRegisterValue, I2C_DATA_TYPE_WORD);
                    poeInfoCb.pacSmState       = PAC_SM_WAIT_CTRL_BITS_TO_CLEAR;
                    poeInfoCb.pacReadLoopCount = 0;
                }
            }
            break;

        case PAC_SM_WAIT_CTRL_BITS_TO_CLEAR:
            if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_PAC_CONTROL_COMMAND, I2C_DATA_TYPE_WORD, &readWord)) {
                pokeWatchdog();
                pacRegister = ((readWord << 8) & 0xFF00) | ((readWord >> 8) & 0x00FF); // swap bytes

                if (pacRegister == 0) {
                    // The control register bits have cleared. Ready to read PAC port-pair registers.
                    // Prepare to read them from this state machine asap, after returning to main.

                    if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM BITS CLEARED, START READING tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

                    poeInfoCb.pacSmState = PAC_SM_READ_PORT_PAIR_REGISTERS;
                } else {
                    // The control register bits have not cleared, so wait and try again. Once the PAC control register
                    // is written, it takes on average about 8 to 10 reads before the bits clear.
                    if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM BITS NOT CLEARED, RAW REG=0x%04X LOOP COUNT=%lu tick=%lu\r\n", readWord, poeInfoCb.pacReadLoopCount, g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }
                    poeInfoCb.pacReadLoopCount += 1;
                    if (poeInfoCb.pacReadLoopCount < POE_PAC_READ_LOOP_LIMIT) {
                        // Loop limit not reached, and control register bits have not cleared.
                        // Remain in this state by doing next to nothing.
                    } else {
                        // Loop limit reached. This is a low-runner condition. The bits expecting to clear are set only against ports with a link UP.
                        // But a link could go down during this period, typically about 1800msecs give or take. It is a small window of opportunity,
                        // not catastrophic, so re-checking if any port went DOWN is not worth the effort. Unless it's observed too often.

                        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM LIMIT REACHED WITH pacRegister=0x%04X tick=%lu\r\n", pacRegister, g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

                        incrementBadEventCounter(POE_PAC_LOOP_READY_LIMIT_REACHED);
                        poeInfoCb.pacSmState               = PAC_SM_IDLE;
                        poeInfoCb.nexRankPortPairPacToRead = 0;
                        poeInfoCb.lastCheckPacTick         = g_ul_ms_ticks;
                        completed                          = TRUE;
                    }
                }
            } else {
                // Bad event noted.
                poeInfoCb.pacSmState               = PAC_SM_IDLE;
                poeInfoCb.nexRankPortPairPacToRead = 0;
                poeInfoCb.lastCheckPacTick         = g_ul_ms_ticks;
                completed                          = TRUE;
            }                
            break;

        case PAC_SM_READ_PORT_PAIR_REGISTERS:
            // Read a PAC port pair register, write the raw register value to the appropriate port ranks.
            // The raw value for the 1st port is in the upper byte of the register, and the raw value for
            // the second port is the lower-order byte of the register.
            // NOTE: the pacRegister when read out will contain data in the following order:
            //       register 0x51 (ports 1,5): [MSB][LSB] = [port 5 PAC][port 1 PAC]
            //       register 0x52 (ports 2,6): [MSB][LSB] = [port 6 PAC][port 2 PAC]
            //       register 0x53 (ports 3,7): [MSB][LSB] = [port 7 PAC][port 3 PAC]
            //       register 0x54 (ports 4,8): [MSB][LSB] = [port 8 PAC][port 4 PAC]
            //       Hence the byte swapping when extracting the raw data.
            command        = POE_PAC_PORT_PAIR_INDEX_LOOKUP[poeInfoCb.nexRankPortPairPacToRead].pacCommand;
            if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, command, I2C_DATA_TYPE_WORD, &pacRegister)) {
                pokeWatchdog();
                firstPortRank  = POE_PAC_PORT_PAIR_INDEX_LOOKUP[poeInfoCb.nexRankPortPairPacToRead].firstPortRank;
                secondPortRank = POE_PAC_PORT_PAIR_INDEX_LOOKUP[poeInfoCb.nexRankPortPairPacToRead].secondPortRank;

                if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM PORT PAIR %lu (rank %lu/%lu) (command=0x%08lX) pacRegister=0x%04X\r\n",
                                                                                        poeInfoCb.nexRankPortPairPacToRead, firstPortRank, secondPortRank, command, pacRegister);
                                                       UART1_WRITE_PACKET_MACRO
                                                     }

                portPoeInfoDb[firstPortRank].pacRegRawValuePresent  = pacRegister & 0x00FF; // data for port rank 0, 1, 2 or 3 is in the lower-order byte
                portPoeInfoDb[secondPortRank].pacRegRawValuePresent = pacRegister >> 8;     // data for port rank 4, 5, 6 or 7 is in the upper-order byte

                // Prepare to move onto the next port pair.
                poeInfoCb.nexRankPortPairPacToRead += 1;
                if (poeInfoCb.nexRankPortPairPacToRead >= (MAX_NUMBER_DOWNSTREAM_PORTS/2)) {
                    // The PAC readings for all 4 port pairs have been read out.
                    // Report any PAC changes in the autonomous info block.
                    // Reset the timeout.
                    poeCheckPacChanges();
                    poeInfoCb.pacSmState               = PAC_SM_IDLE;
                    poeInfoCb.nexRankPortPairPacToRead = 0;
                    poeInfoCb.lastCheckPacTick         = g_ul_ms_ticks;
                    completed                          = TRUE;

                    if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: PAC SM COMPLETE tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

                } else {
                    // Still 1 or more port pairs to check. Set the tick timer to check
                    // immediately again upon returning to this section.
                    poeInfoCb.lastCheckPacTick = 0;     // return to this function asap.
                    completed                  = FALSE;
                }
            } else {
                // Bad event noted.
                poeInfoCb.pacSmState               = PAC_SM_IDLE;
                poeInfoCb.nexRankPortPairPacToRead = 0;
                poeInfoCb.lastCheckPacTick         = g_ul_ms_ticks;
                completed                          = TRUE;
            }
            break;

        default:
            // Coding error. Increment a counter.
            incrementBadEventCounter(POE_PAC_SM_CODING_ERROR);
            sprintf(&pCraftPortDebugData[0], "POE SM CODING ERROR: STATE=0x%lX\r\n", poeInfoCb.pacSmState); UART1_WRITE_PACKET_MACRO
            poeInfoCb.pacSmState = PAC_SM_IDLE;
            break;
    }
    return completed;
}


// checkAutoClassStatus
//
// Read the auto class status register and determine if any devices
// support auto class. The output is a json-like string of the form:
//
// {"EVT":{"EI":21,"POE AUTO CLASS STATUS":[{"PORT":"1","STATUS":"SUPPORTED"},{"PORT":"3","STATUS":"NOT SUPPORTED"},...,{"PORT":"7","STATUS":"NOT SUPPORTED"}]}}
//
//
static void checkAutoClassStatus(void)
{
    U32  iggy;
    BOOL firstTime = TRUE;
    U32  charsWritten;
    U16  acStatusRegister;
    U16  rawRegister;
    U16  currentPortAcStatus;
    U16  newPortAcStatus;
    U16  portMask;

    if ((g_ul_ms_ticks - poeInfoCb.lastCheckAutoClassStatusTick) > POE_CHECK_AC_STATUS_TIMEOUT) {
        // Read the auto class status register and check for changes. Swap the bytes in the 16-bit raw register.

        if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: AC STATUS CHECK tick=%lu\r\n", g_ul_ms_ticks); UART1_WRITE_PACKET_MACRO }

        if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_AUTO_CLASS_STATUS_COMMAND, I2C_DATA_TYPE_WORD, &rawRegister)) {
            pokeWatchdog();
            acStatusRegister = ((rawRegister & 0xFF00) >> 8) | ((rawRegister & 0x00FF) << 8);
            if (acStatusRegister == poeInfoCb.acStatusRegister) {
                // No change, nothing to do.
            } else {
                // A change has been detected in 1 or more ports. Detect and write as an autonomous event.

                memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);

                for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {
                    portMask            = POE_AUTO_CLASS_STATUS_BIT_BY_PORT_INDEX_LOOKUP[iggy];
                    currentPortAcStatus = poeInfoCb.acStatusRegister & portMask;
                    newPortAcStatus     = acStatusRegister & portMask;

                    if (currentPortAcStatus == newPortAcStatus) {
                        // There was no change for this port. Nothing to see here, keep moving.
                    } else {
                        if (firstTime) {
                            // Write the title for this jsonesque string and the 1st port with changed status.
                            sprintf(pPoeJsonString, "{%s:{%s:%u,%s:[{%s:\"%lu\",%s:%s}", EVT_STRING, EVENT_ID_STRING, AEI_POE_PORT_AC_STATUS_CHANGE,
                                                                                         POE_AUTO_CLASS_STATUS_STRING,
                                                                                         PORT_STRING,   (iggy+1),
                                                                                         STATUS_STRING, (newPortAcStatus == 0 ? NOT_SUPPORTED_STRING : SUPPORTED_STRING));
                            firstTime = FALSE;
                        } else {
                            // Add the next port with changed power; don't forget the leading comma:
                            memset(pMiniJsonString, 0, MINI_JSON_STRING_SIZE);
                            charsWritten = sprintf(pMiniJsonString, ",{%s:\"%lu\",%s:%s}", PORT_STRING, (iggy+1), STATUS_STRING, (newPortAcStatus == 0 ? NOT_SUPPORTED_STRING : SUPPORTED_STRING));
                            if ((charsWritten + strlen(pPoeJsonString)) > POE_JSON_SAFETY_MARGIN_CHARS) {
                                // Quite unexpected.
                                incrementBadEventCounter(POE_AUTO_STRING_EXCEEDS_SAFETY_MARGIN);
                                memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE); // prevents writing the autonomous event below.
                                break;
                            } else {
                                strcat(pPoeJsonString, pMiniJsonString);
                            }
                        }
                    }
                } // for ...

                // Update the status register in the POE info control block.
                 poeInfoCb.acStatusRegister = acStatusRegister;

                // If there was at least 1 POE port power change, make it available for retrieval by the Gateway.
                // Don't forget to close off the json-like string with 1 square bracket (close off the list) and 2 curly brackets.
                if (strlen(pPoeJsonString) != 0) {
                    strcat(pPoeJsonString, "]}}");
                    addJsonToAutonomnousString(pPoeJsonString);
                } else {
                    // Something wrong here. This section of code is only entered if a change was detected,
                    // yet the json string was of length 0? If it wasn't due to string size thresholds, then
                    // surely this is a coding error.
                    incrementBadEventCounter(POE_AC_STATUS_0_CHANGE_JSON_STRING);
                }
            }
            poeInfoCb.lastCheckAutoClassStatusTick = g_ul_ms_ticks;

        } else {} // Bad event noted.
    }
    return;
}


// checkSupplyFaultRegister
//
// Read and analyze the POE device's supply/fault register.
//
// If a supply/fault event is detected, generate the following autonomous event for the Gateway:
//
// {"EVT":{"EI":35,"POE SUPPLY FAULT EVENT":{"COMMAND REGISTER":"0x00000009","VALUE":"0xF3","INFO":"TSD VDUV VDWRN VPUV OSSE RAMFLT"}}}
//
static void checkSupplyFaultRegister(void)
{
    U16 poeData = 0xFF;
    if ((g_ul_ms_ticks - poeInfoCb.lastCheckSupplyStatusTick) > POE_CHECK_SUPPLY_STATUS_REG_TIMEOUT) {
        // Read the supply/fault register and check for faults.

        if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_SUPPLY_RO_CLEAR_COMMAND, I2C_DATA_TYPE_BYTE, &poeData)) {
            pokeWatchdog();
            if (poeData == 0) {
                // No supply or fault detected. Nothing to do.
                // sprintf(&pCraftPortDebugData[0], "*"); UART1_WRITE_PACKET_MACRO // <- used while developing to ensure the checking frequency.
            } else {
                // Determine which bits in the supply/fault register are set.
                memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);
                sprintf(pPoeJsonString, "{%s:{%s:%u,%s:{%s:\"0x%08lX\",%s:\"%02X\",%s:\"", EVT_STRING, EVENT_ID_STRING, AEI_POE_SUPPLY_FAULT_EVENT_DETECTED, POE_SUPPLY_FAULT_EVENT_STRING,
                                                                                          COMMAND_REGISTER_STRING, POE_SUPPLY_RO_CLEAR_COMMAND, VALUE_STRING, poeData, INFO_STRING);
                if ((poeData & POE_SUPPLY_FAULT_TSD_MASK)    != 0) { strcat(pPoeJsonString, POE_SUPPLY_FAULT_EVENT_TSD_STRING);    }
                if ((poeData & POE_SUPPLY_FAULT_VDUV_MASK)   != 0) { strcat(pPoeJsonString, POE_SUPPLY_FAULT_EVENT_VDUV_STRING);   }
                if ((poeData & POE_SUPPLY_FAULT_VDWRN_MASK)  != 0) { strcat(pPoeJsonString, POE_SUPPLY_FAULT_EVENT_VDWRN_STRING);  }
                if ((poeData & POE_SUPPLY_FAULT_VPUV_MASK)   != 0) { strcat(pPoeJsonString, POE_SUPPLY_FAULT_EVENT_VPUV_STRING);   }
                if ((poeData & POE_SUPPLY_FAULT_OSSE_MASK)   != 0) { strcat(pPoeJsonString, POE_SUPPLY_FAULT_EVENT_OSSE_STRING);   }
                if ((poeData & POE_SUPPLY_FAULT_RAMFLT_MASK) != 0) { strcat(pPoeJsonString, POE_SUPPLY_FAULT_EVENT_RAMFLT_STRING); }
                strcat(pPoeJsonString, "\"}}}");
                addJsonToAutonomnousString(pPoeJsonString);
            }
        } else {
            // A failure, which would have been noted.
        }

        // Set the tick for the next time around.
        poeInfoCb.lastCheckSupplyStatusTick = g_ul_ms_ticks;

    } else {} // Patience is a virtue ...

    return;
}


// checkEventRegisters
//
// When the tick times out, return and check one of the the port-based event register in the following sequence:
//
// - power event register
// - detection event register
// - fault event register
// - start/ilim event register
//
// If a change is detected, the following autonomous event is generated:
//
//
// {
//     "EVT": {
//         "EI": 36,
//         "POE PORT EVENT REGISTER CHANGE": {
//             "COMMAND REGISTER": "0x00000003",                                     <- or "0x00000005"      or "0x00000007"  or "0x00000009"
//             "EVENT NAME":"POWER EVENT",                                           <- or "DETECTION EVENT" or "FAULT EVENT" or "START EVENT"
//             "EVENT INFO":"POWER GOOD STATUS",                                     <- if START EVENT *ILIM BIT* FAULT
//             "EVENT INFO":"POWER ENABLE STATUS",                                   <- if START EVENT *START BIT* status change
//             "EVENT INFO":"DETECTION CHANGE OF CLASS",                             <- if DETECTION EVENT "change of class"
//             "EVENT INFO":"DETECTION CHANGE IN DETECTION",                         <- if DETECTION EVENT "change in detection"
//             "EVENT INFO":"FAULT DISCONNECT",                                      <- if FAULT DISCONNECT change
//             "EVENT INFO":"FAULT OVERLOAD",                                        <- if FAULT OVERLOAD change
//             "EVENT INFO":"START ILIM FAULT",                                      <- if START EVENT *ILIM BIT* FAULT
//             "EVENT INFO":"START CLASS DETECT",                                    <- if START EVENT *START BIT* status change
//             "FROM VALUE": "0x0F0F",
//             "TO VALUE": "0xFFFF",
//             "PORT LIST": "1,2,3,4,5,6,7,8",
//             "TICK INFO":"12345678 -> 23456789 = 11111111"
//         }
//     }
// }
//
//    TODO: the code in this function is very repetitious. Do something to streamline it, please! 
//
//
static void checkEventRegisters(void)
{
    U16  poeData;
    U32  currentTick;
    U32  elapsedTick;
    char *pEventInfoString;
    char pPortList[32]; // sized to hold all 8 ports if affected: "8 7 6 5 4 3 2 1 "

    if ((g_ul_ms_ticks - poeInfoCb.lastCheckEventRegistersTick) > POE_CHECK_EVENT_REGISTERS_TIMEOUT) {
        // Read and check the next event register for changes.
        switch (poeInfoCb.nextEventCommand) {
            case POE_POWER_EVENT_RO_CLEAR_COMMAND:
                i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_POWER_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD, &poeData);
                if (poeData != poeInfoCb.powerEventRegisterValue) {
                    currentTick = g_ul_ms_ticks;
                    elapsedTick = currentTick - poeInfoCb.lastPowerChangeTick;
                    memset(&pPortList[0], 0, 32);
                    pGetPortsListFromChangedData(poeData,  poeInfoCb.powerEventRegisterValue, &pPortList[0]);
                    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);
                    if      ((poeData & POE_POWER_GOOD_STATUS_MASK) != 0) { pEventInfoString = (char *)POWER_GOOD_STATUS_STRING;   }
                    else if ((poeData & POE_POWER_GOOD_STATUS_MASK) != 0) { pEventInfoString = (char *)POWER_ENABLE_STATUS_STRING; }
                    else                                                  { pEventInfoString = NULL;                               }
                    if (pEventInfoString != NULL) {
                        sprintf(pPoeJsonString, "{%s:{%s:%u,%s:{%s:\"0x%08lX\",%s:%s,%s:%s,%s:\"0x%04X\",%s:\"0x%04X\",%s:\"%s\",%s:\"%lu,%lu,%lu\"}}}",
                                                EVT_STRING, EVENT_ID_STRING, AEI_POE_PORT_EVENT_DETECTED, POE_PORT_EVENT_DETECTED_STRING,
                                                                             COMMAND_REGISTER_STRING,     POE_POWER_EVENT_RO_CLEAR_COMMAND,
                                                                             EVENT_NAME_STRING,           POWER_EVENT_STRING,
                                                                             EVENT_INFO_STRING,           pEventInfoString,
                                                                             FROM_VALUE_STRING,           poeInfoCb.powerEventRegisterValue,
                                                                             TO_VALUE_STRING,             poeData,
                                                                             PORT_LIST_STRING,            pPortList,
                                                                             TICK_INFO_STRING,            poeInfoCb.lastPowerChangeTick, currentTick, elapsedTick);
                        addJsonToAutonomnousString(pPoeJsonString);
                        //sprintf(&pCraftPortDebugData[0], "POWER:%04X->%04X ports=%s TICK:%lu->%lu->%lu\r\n",                                                                      // COMMENT OUT/DELETE
                        //                                  poeInfoCb.powerEventRegisterValue, poeData, pPortList,                                                                  // COMMENT OUT/DELETE
                        //                                  poeInfoCb.lastPowerChangeTick, currentTick, elapsedTick); UART1_WRITE_PACKET_MACRO                                      // COMMENT OUT/DELETE
                    }                        
                    poeInfoCb.powerEventRegisterValue = poeData;
                    poeInfoCb.lastPowerChangeTick     = currentTick;                
                }
                poeInfoCb.nextEventCommand = POE_DETECTION_EVENT_RO_CLEAR_COMMAND;
                break;

            case POE_DETECTION_EVENT_RO_CLEAR_COMMAND:
                i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_DETECTION_EVENT_RO_CLEAR_COMMAND, I2C_DATA_TYPE_WORD, &poeData);
                if (poeData != poeInfoCb.detectionEventRegisterValue) {
                    currentTick = g_ul_ms_ticks;
                    elapsedTick = currentTick - poeInfoCb.lastDetectionChangeTick;
                    memset(&pPortList[0], 0, 32);
                    pGetPortsListFromChangedData(poeData,  poeInfoCb.detectionEventRegisterValue, &pPortList[0]);
                    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);
                    if      ((poeData & POE_DETECTION_CHANGE_OF_CLASS_MASK)     != 0) { pEventInfoString = (char *)DETECTION_CHANGE_OF_CLASS_STRING;     }
                    else if ((poeData & POE_DETECTION_CHANGE_IN_DETECTION_MASK) != 0) { pEventInfoString = (char *)DETECTION_CHANGE_IN_DETECTION_STRING; }
                    else                                                              { pEventInfoString = NULL;                                         }
                    if (pEventInfoString != NULL) {
                        sprintf(pPoeJsonString, "{%s:{%s:%u,%s:{%s:\"0x%08lX\",%s:%s,%s:%s,%s:\"0x%04X\",%s:\"0x%04X\",%s:\"%s\",%s:\"%lu,%lu,%lu\"}}}",
                                                EVT_STRING, EVENT_ID_STRING, AEI_POE_PORT_EVENT_DETECTED, POE_PORT_EVENT_DETECTED_STRING,
                                                                             COMMAND_REGISTER_STRING,        POE_DETECTION_EVENT_RO_CLEAR_COMMAND,
                                                                             EVENT_NAME_STRING,              DETECTION_EVENT_STRING,
                                                                             EVENT_INFO_STRING,              pEventInfoString,
                                                                             FROM_VALUE_STRING,              poeInfoCb.detectionEventRegisterValue,
                                                                             TO_VALUE_STRING,                poeData,
                                                                             PORT_LIST_STRING,               pPortList,
                                                                             TICK_INFO_STRING,               poeInfoCb.lastDetectionChangeTick, currentTick, elapsedTick);
                        addJsonToAutonomnousString(pPoeJsonString);
                        //sprintf(&pCraftPortDebugData[0], "DETECTION:%04X->%04X ports=%s TICK:%lu->%lu->%lu\r\n",                                                                  // COMMENT OUT/DELETE
                        //                                 poeInfoCb.detectionEventRegisterValue, poeData, pPortList,                                                               // COMMENT OUT/DELETE
                        //                                 poeInfoCb.lastDetectionChangeTick, currentTick, elapsedTick); UART1_WRITE_PACKET_MACRO                                   // COMMENT OUT/DELETE
                    }                                                         
                    poeInfoCb.detectionEventRegisterValue = poeData;
                    poeInfoCb.lastDetectionChangeTick     = currentTick;
                }
                poeInfoCb.nextEventCommand = POE_FAULT_EVENT_RO_CLEAR_COMMAND;
                break;

            case POE_FAULT_EVENT_RO_CLEAR_COMMAND:
                i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_FAULT_EVENT_RO_CLEAR_COMMAND, I2C_DATA_TYPE_WORD, &poeData);
                if (poeData != poeInfoCb.faultEventRegisterValue) {
                    currentTick = g_ul_ms_ticks;
                    elapsedTick = currentTick - poeInfoCb.lastFaultChangeTick;
                    memset(&pPortList[0], 0, 32);
                    pGetPortsListFromChangedData(poeData,  poeInfoCb.faultEventRegisterValue, &pPortList[0]);
                    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);
                    if      ((poeData & POE_FAULT_DISCONNECT_MASK) != 0) { pEventInfoString = (char *)FAULT_DISCONNECT_STRING;     }
                    else if ((poeData & POE_FAULT_OVERLOAD_MASK)   != 0) { pEventInfoString = (char *)FAULT_OVERLOAD_STRING;       }
                    else                                                 { pEventInfoString = NULL;                                }
                    if (pEventInfoString != NULL) {
                        sprintf(pPoeJsonString, "{%s:{%s:%u,%s:{%s:\"0x%08lX\",%s:%s,%s:%s,%s:\"0x%04X\",%s:\"0x%04X\",%s:\"%s\",%s:\"%lu,%lu,%lu\"}}}",
                                                EVT_STRING, EVENT_ID_STRING, AEI_POE_PORT_EVENT_DETECTED, POE_PORT_EVENT_DETECTED_STRING,
                                                                             COMMAND_REGISTER_STRING,     POE_FAULT_EVENT_RO_CLEAR_COMMAND,
                                                                             EVENT_NAME_STRING,           FAULT_EVENT_STRING,
                                                                             EVENT_INFO_STRING,           pEventInfoString,
                                                                             FROM_VALUE_STRING,           poeInfoCb.faultEventRegisterValue,
                                                                             TO_VALUE_STRING,             poeData,
                                                                             PORT_LIST_STRING,            pPortList,
                                                                             TICK_INFO_STRING,            poeInfoCb.lastFaultChangeTick, currentTick, elapsedTick);
                        addJsonToAutonomnousString(pPoeJsonString);
                        //sprintf(&pCraftPortDebugData[0], "FAULT:%04X->%04X ports=%s TICK:%lu->%lu->%lu\r\n",                                                                      // COMMENT OUT/DELETE
                        //                                  poeInfoCb.faultEventRegisterValue, poeData, pPortList,                                                                  // COMMENT OUT/DELETE
                        //                                  poeInfoCb.lastFaultChangeTick, currentTick, elapsedTick); UART1_WRITE_PACKET_MACRO                                      // COMMENT OUT/DELETE
                    }                        
                    poeInfoCb.faultEventRegisterValue = poeData;
                    poeInfoCb.lastFaultChangeTick     = currentTick;
                }
                poeInfoCb.nextEventCommand = POE_START_EVENT_RO_CLEAR_COMMAND;
                break;

            case POE_START_EVENT_RO_CLEAR_COMMAND:
                i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_START_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD, &poeData);
                if (poeData != poeInfoCb.startEventRegisterValue) {
                    currentTick = g_ul_ms_ticks;
                    elapsedTick = currentTick - poeInfoCb.lastStartChangeTick;
                    memset(&pPortList[0], 0, 32);
                    pGetPortsListFromChangedData(poeData,  poeInfoCb.startEventRegisterValue, &pPortList[0]);
                    if      ((poeData & POE_START_ILIM_START_MASK) != 0) { pEventInfoString = (char *)START_CLASS_DETECT_STRING;   }
                    else if ((poeData & POE_START_ILIM_ILIM_MASK)  != 0) { pEventInfoString = (char *)START_ILIM_FAULT_STRING;     }
                    else                                                 { pEventInfoString = NULL;                                }
                    if (pEventInfoString != NULL) {
                        sprintf(pPoeJsonString, "{%s:{%s:%u,%s:{%s:\"0x%08lX\",%s:%s,%s:%s,%s:\"0x%04X\",%s:\"0x%04X\",%s:\"%s\",%s:\"%lu,%lu,%lu\"}}}",
                                                EVT_STRING, EVENT_ID_STRING, AEI_POE_PORT_EVENT_DETECTED, POE_PORT_EVENT_DETECTED_STRING,
                                                                             COMMAND_REGISTER_STRING,     POE_START_EVENT_RO_CLEAR_COMMAND,
                                                                             EVENT_NAME_STRING,           START_EVENT_STRING,
                                                                             EVENT_INFO_STRING,           pEventInfoString,
                                                                             FROM_VALUE_STRING,           poeInfoCb.startEventRegisterValue,
                                                                             TO_VALUE_STRING,             poeData,
                                                                             PORT_LIST_STRING,            pPortList,
                                                                             TICK_INFO_STRING,            poeInfoCb.lastStartChangeTick, currentTick, elapsedTick);
                        addJsonToAutonomnousString(pPoeJsonString);
                        //sprintf(&pCraftPortDebugData[0], "START:%04X->%04X ports=%s TICK:%lu->%lu->%lu\r\n",                                                                      // COMMENT OUT/DELETE
                        //                                 poeInfoCb.startEventRegisterValue, poeData, pPortList,                                                                   // COMMENT OUT/DELETE
                        //                                 poeInfoCb.lastStartChangeTick, currentTick, elapsedTick); UART1_WRITE_PACKET_MACRO                                       // COMMENT OUT/DELETE
                    }                                                         
                    poeInfoCb.startEventRegisterValue = poeData;
                    poeInfoCb.lastStartChangeTick     = currentTick;
                }

                // For the next time around: - set the event command to start from the top of this mini-state machine.
                //                           - set the tick for the next time around, to provide a small delay.
                poeInfoCb.nextEventCommand            = POE_POWER_EVENT_RO_CLEAR_COMMAND;
                poeInfoCb.lastCheckEventRegistersTick = g_ul_ms_ticks;
                break;
            default:
                // How did I get here?
                incrementBadEventCounter(CHECK_POE_PORT_EVENT_CODING_ERROR);
                poeInfoCb.lastCheckEventRegistersTick = g_ul_ms_ticks; // Set the tick for the next time around.
                break;
        }

    } else {} // Patience is a virtue ...

}


// pGetPortsListFromChangedData
//
// Based on changes in the bits between a pair of 16-bits "event" register values, get the list of affected ports.
// The string is more or less free-format. Each 16-bit register value contains the bits setting for some event
// register where: - upper nibble contains 2 bits for each of the 4 ports 8765 8765
//                 - lower nibble contains 2 bits for each of the 4 ports 4321 4311
//
//
static void pGetPortsListFromChangedData(U16 thisWord, U16 thatWord, char *pSomeString)
{
                                                                                                       // |<- this word  ->|   |<- that word ->|
    strcat(pSomeString, " ");                                                                          // 87658765 43214321    87658765 43214321 <- ports
    if ((thisWord & 0x8800) != (thatWord & 0x8800)) { strcat(pSomeString, "8 "); } // port 8 bit changed: 10001000 00000000 -> 10001000 00000000
    if ((thisWord & 0x4400) != (thatWord & 0x4400)) { strcat(pSomeString, "7 "); } // port 7 bit changed: 01000100 00000000 -> 01000100 00000000
    if ((thisWord & 0x2200) != (thatWord & 0x2200)) { strcat(pSomeString, "6 "); } // port 6 bit changed: 00100010 00000000 -> 00100000 00000000
    if ((thisWord & 0x1100) != (thatWord & 0x1100)) { strcat(pSomeString, "5 "); } // port 5 bit changed: 00010001 00000000 -> 00010000 00000000
    if ((thisWord & 0x0088) != (thatWord & 0x0088)) { strcat(pSomeString, "4 "); } // port 4 bit changed: 00000000 00001000 -> 00000000 00001000
    if ((thisWord & 0x0044) != (thatWord & 0x0044)) { strcat(pSomeString, "3 "); } // port 3 bit changed: 00000000 00000100 -> 00000000 00000100
    if ((thisWord & 0x0022) != (thatWord & 0x0022)) { strcat(pSomeString, "2 "); } // port 2 bit changed: 00000000 00000010 -> 00000000 00000010
    if ((thisWord & 0x0011) != (thatWord & 0x0011)) { strcat(pSomeString, "1 "); } // port 1 bit changed: 00000000 00000001 -> 00000000 00000001
}


// isDevicePoe
//
//
//
static BOOL isDevicePoe(U8 autoClass)
{
    if ((autoClass == POE_AUTO_CLASS_1)
     || (autoClass == POE_AUTO_CLASS_2)
     || (autoClass == POE_AUTO_CLASS_3)
     || (autoClass == POE_AUTO_CLASS_4))
        return TRUE;
    else return FALSE;
}


// poeCheckPacChanges
//
// Go through the port power info block, and if there are any changes in PAC,
// write to the autonomous info block. The info string will look something life:
//
// {"EVT":{"EI":19,"POE PORT PAC":[{"PORT":"3","PAC":"1.02"},{"PORT":"7","PAC":"2.96"}]}}
//
//
static void poeCheckPacChanges(void)
{
    U32  iggy;
    BOOL firstTime = TRUE;
    F32  calculatedPacAsWatts;
    U32  charsWritten;

    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);

    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {

        if (portPoeInfoDb[iggy].pacRegRawValuePrevious != portPoeInfoDb[iggy].pacRegRawValuePresent) {
            calculatedPacAsWatts = portPoeInfoDb[iggy].pacRegRawValuePresent * POE_PAC_STEP_FACTOR;

            if (poeInfoCb.poeDebugOutputEnabled) {
                sprintf(&pCraftPortDebugData[0], "POE: PAC SM PORT %lu PAC 0x%02X -> 0x%02X\r\n", (iggy+1), portPoeInfoDb[iggy].pacRegRawValuePrevious, portPoeInfoDb[iggy].pacRegRawValuePresent);
                UART1_WRITE_PACKET_MACRO
            }

            if (firstTime) {
                // Write the title for this jsonesque string and the 1st port with changed PAC.
                sprintf(pPoeJsonString, "{%s:{%s:%u,%s:[{%s:\"%lu\",%s:\"%2.1f\"}", EVT_STRING, EVENT_ID_STRING,  AEI_POE_PORT_PAC_CHANGE,
                                                                                    POE_PORT_PAC_STRING,
                                                                                    PORT_STRING, (iggy+1),
                                                                                    PAC_STRING,  calculatedPacAsWatts);
                firstTime = FALSE;
            } else {
                // Add the next port with changed PAC; don't forget the leading comma:
                memset(pMiniJsonString, 0, MINI_JSON_STRING_SIZE);
                charsWritten = sprintf(pMiniJsonString, ",{%s:\"%lu\",%s:\"%2.1f\"}", PORT_STRING, (iggy+1), PAC_STRING, calculatedPacAsWatts);
                if ((charsWritten + strlen(pPoeJsonString)) > POE_JSON_SAFETY_MARGIN_CHARS) {
                    // Quite unexpected.
                    incrementBadEventCounter(POE_AUTO_STRING_EXCEEDS_SAFETY_MARGIN);
                    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE); // prevents writing the autonomous event below. 
                    break;
                } else {
                    strcat(pPoeJsonString, pMiniJsonString);
                }                    
            }
            // Update the previous reading for the next time around.
            portPoeInfoDb[iggy].pacRegRawValuePrevious = portPoeInfoDb[iggy].pacRegRawValuePresent;
        } else {} // No PAC change for this port - nothing to do.
    } // for ...

    // If there was at least 1 POE port PAC change, make it available for retrieval by the Gateway.
    // Don't forget to close off the json-like string with 1 square bracket (close off the list) and 3 curly brackets.
    if (strlen(pPoeJsonString) != 0) {
        strcat(pPoeJsonString, "]}}");
        addJsonToAutonomnousString(pPoeJsonString);
    }
}


// poeCheckAutoClassConfigChanges
//
// Go through the port power info block, and if there are any changes in the auto-class config register,
// write to the autonomous info block. The info string will look something life:
//
// {"EVT":{"EI":19,"POE PORT AUTO CLASS":[{"PORT":"2","CLASS CONFIG":"CLASS 1"},{"PORT":"7","CLASS CONFIG":"CLASS 3"}]}}
//
static void poeCheckAutoClassConfigChanges(void)
{
    U32  iggy;
    BOOL firstTime = TRUE;
    U32  charsWritten;

    // 680 bytes is reserved for this event. The worse case scenario where data is written for
    // all 8 ports is 386 chars, so there's plenty of room.
    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE);

    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {
        if (portPoeInfoDb[iggy].acConfigPresent != portPoeInfoDb[iggy].acConfigPrevious) {
            if (firstTime) {
                // Write the title for this jsonesque string and the 1st port with changed power.
                // On 1st time through, write the overall title for this component. blah blah b;lah ...
                sprintf(pPoeJsonString, "{%s:{%s:%u,%s:[{%s:\"%lu\",%s:%s}", EVT_STRING, EVENT_ID_STRING,  AEI_POE_PORT_AUTO_CLASS_CONFIG_CHANGE,
                                                                             POE_PORT_AUTO_CLASS_STRING, PORT_STRING, (iggy+1),
                                                                             CLASS_CONFIG_STRING,        getAutoClassString(portPoeInfoDb[iggy].acConfigPresent));
                firstTime = FALSE;
            } else {
                // Add the next port with changed power; don't forget the leading comma:
                memset(pMiniJsonString, 0, MINI_JSON_STRING_SIZE);
                charsWritten = sprintf(pMiniJsonString, ",{%s:\"%lu\",%s:%s}", PORT_STRING, (iggy+1), CLASS_CONFIG_STRING, getAutoClassString(portPoeInfoDb[iggy].acConfigPresent));
                if ((charsWritten + strlen(pPoeJsonString)) > POE_JSON_SAFETY_MARGIN_CHARS) {
                    // Quite unexpected.
                    incrementBadEventCounter(POE_AUTO_STRING_EXCEEDS_SAFETY_MARGIN);
                    memset(&pPoeJsonString[0], 0, POE_SINGLE_JSON_EVENT_STRING_SIZE); // prevents writing the autonomous event below. 
                    break;
                } else {
                    strcat(pPoeJsonString, pMiniJsonString);
                }
            }

            if (poeInfoCb.poeDebugOutputEnabled) { sprintf(&pCraftPortDebugData[0], "POE: AC CONFIG CHANGE PORT %lu 0x%02X -> 0x%02X\r\n", (iggy + 1), portPoeInfoDb[iggy].acConfigPrevious, portPoeInfoDb[iggy].acConfigPresent);
                                                   UART1_WRITE_PACKET_MACRO
                                                 }

            // Update the previous reading for the next time around.
            portPoeInfoDb[iggy].acConfigPrevious = portPoeInfoDb[iggy].acConfigPresent;
        }
    }    

    // If there was at least 1 POE port power change, make it available for retrieval by the Gateway.
    // Don't forget to close off the json-like string with 1 square bracket (close off the list) and 2 curly brackets.
    if (strlen(pPoeJsonString) != 0) {
        strcat(pPoeJsonString, "]}}"); // Assume we are nowhere near the edge for this write.
        addJsonToAutonomnousString(pPoeJsonString);
    }

    return;

}


// primaryPortLinkStatusChange
//
// Called by the primary ethernet controller to inform the POE system component of a primary port link status change.
// Purpose: link status can go UP and DOWN during the time period where the POE component is checking for a potential
//          "link is down with POE power":
//
//          *** PROBABLY NO LONGER NEEDED ***
//
// This function is identical to the secondaryPortLinkStatusChange(xxx) function below. Each are called from the primary
// or secondary controller subsystem, each only knows about their own ports (or "port rank").
//
void primaryPortLinkStatusChange(U32 primaryPortRank, U32 portStatusHighLevel)
{
    // From the perspective of the primary ethernet controller, the port rank of interest is in the range of 0..3 (with
    // (rank 4 is the LAN port, ignored), so the actual "port rank" into the info data block is the port rank itself.
    // The primary ethernet controller has no notion of POE.
    if (primaryPortRank <= 3) {

        if (poeInfoCb.poeDebugOutputEnabled) {
            if (portPoeInfoDb[primaryPortRank].acConfigPresent == POE_AUTO_CLASS_NOT_POE) {
                sprintf(&pCraftPortDebugData[0], "POE: PORT %lu LINK %s IGNORED NON-POE %lu\r\n", (primaryPortRank + 1), (portStatusHighLevel == PORT_IS_UP_HIGH_LEVEL ? UP_STRING : DOWN_STRING), g_ul_ms_ticks);
                UART1_WRITE_PACKET_MACRO
            } else {
                sprintf(&pCraftPortDebugData[0], "POE: PORT %lu LINK %s IGNORED FOR POE %lu\r\n", (primaryPortRank + 1), (portStatusHighLevel == PORT_IS_UP_HIGH_LEVEL ? UP_STRING : DOWN_STRING), g_ul_ms_ticks);
                UART1_WRITE_PACKET_MACRO
            }
        }

        if (portStatusHighLevel == PORT_IS_DOWN_HIGH_LEVEL) {
            // The port is down, reset the check-PAC-port tick timer.
            poeInfoCb.lastCheckPacTick = g_ul_ms_ticks;
        }
    }
}


// secondaryPortLinkStatusChange
//
// See the description for primaryPortLinkStatusUp(xxx) above.
//
void secondaryPortLinkStatusChange(U32 secondaryPortRank, U32 portStatusHighLevel)
{
    // From the perspective of the secondary ethernet controller, the port rank of interest is in the range of 0..3,
    // so the actual "port rank" value into the info data block starts with port rank [4]. As with the primary controller,
    // the secondary ethernet controller has no notion of POE.
    if (secondaryPortRank <= 3) {

        if (poeInfoCb.poeDebugOutputEnabled) {
            if (portPoeInfoDb[secondaryPortRank + 4].acConfigPresent == POE_AUTO_CLASS_NOT_POE) {
                sprintf(&pCraftPortDebugData[0], "POE: PORT %lu LINK %s IGNORED NON-POE %lu\r\n", (secondaryPortRank + 5), (portStatusHighLevel == PORT_IS_UP_HIGH_LEVEL ? UP_STRING : DOWN_STRING), g_ul_ms_ticks);
                UART1_WRITE_PACKET_MACRO
            } else {
                sprintf(&pCraftPortDebugData[0], "POE: PORT %lu LINK %s IGNORED FOR NON-POE %lu\r\n", (secondaryPortRank + 5), (portStatusHighLevel == PORT_IS_UP_HIGH_LEVEL ? UP_STRING : DOWN_STRING), g_ul_ms_ticks);
                UART1_WRITE_PACKET_MACRO
            }
        }

        if (portStatusHighLevel == PORT_IS_DOWN_HIGH_LEVEL) {
            // The port is down, reset the check-PAC-port tick timer.
            poeInfoCb.lastCheckPacTick = g_ul_ms_ticks;
        }
    }
}


// displayPoeInfo
//
// An output component of hello processing. It contains the current power setting for all 8 downstream ports
// (UP or DOWN) and POE temperature info. Temperature might be negative.
//
// "POE INFO":{"POE POWER":[{"1": "UP","2": "UP", ... "8": "UP"}],"POE TEMPERATURE": "40.1"}
void displayPoeInfo (void)
{
    // Print the start title for this json string.
    printf("%s:{", POE_INFO_STRING);
    displayPoeHighLevelBaseInfo();
    printf("}");
}


// poeReadDeviceId
//
//
//
U8 poeReadDeviceId(void)
{
    U16  deviceId = 0xFFFF;
    if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_DEVICE_ID_COMMAND, I2C_DATA_TYPE_BYTE, &deviceId)) {
        pokeWatchdog();
        if (deviceId == TI_POE_CONTROLLER_DEV_ID_EXPECTED) {
            printf("- read complete, DEVICE ID = 0x%02X\r\n", deviceId);
        } else {
            printf("- read complete, DEVICE ID = 0x%02X ***invalid, expected 0x%02X***\r\n", deviceId, TI_POE_CONTROLLER_DEV_ID_EXPECTED);
            incrementBadEventCounter(UNEXPECTED_POE_DEVICE_ID);
        }  
    } else {
        printf("I FAILED TO I2C READ THE POE DEVICE ID");
    }        
    return deviceId;          
}


// poeReadManufacturerId
//
//
//
U8 poeReadManufacturerId(void)
{
    U16  manufactureId = 0xFFFF;
    if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_MFR_ID_COMMAND, I2C_DATA_TYPE_BYTE, &manufactureId)) {
        pokeWatchdog();
        if (manufactureId == TI_POE_CONTROLLER_MFR_ID_EXPECTED) {
            printf("- read complete, MANUFACTURER ID = 0x%02X\r\n", manufactureId);
        } else {
            printf("- read complete, MANUFACTURER ID = 0x%02X ***invalid, expected 0x%02X***\r\n", manufactureId, TI_POE_CONTROLLER_MFR_ID_EXPECTED);
            incrementBadEventCounter(UNEXPECTED_POE_MANUFACTURER_ID);
        }
    } else  {
        printf("I FAILED TO I2C READ THE POE MFR ID");
    }
    return manufactureId;
}


// poeHighLevelGeneralInfo
//
//
// {"CLI":{"COMMAND": "poe_info","RESPONSE": {"POWER":[{"1": "UP","2": "UP", ... "8": "UP"}],
//                                            "TEMPERATURE": "40.1",
//                                            "INPUT VOLTAGE":"48.321",
//                                            ... a buncha other items ...
//                                           }, "RESULT":"SUCCESS"}}
//
//
void poeHighLevelGeneralInfo(void)
{
    // Print the start title for this json string.
    printf("{%s:{%s:\"%s\",%s:{", CLI_STRING, COMMAND_STRING, POE_GENERAL_INFO_CMD, RESPONSE_STRING);

    // Print the power enable component and the POE temperature component.
    // This display encloses itself as a single json-like component.
    displayPoeHighLevelBaseInfo();
    printf("}");

    // Add the result portion and close off the json-like string.
    printf(",%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
}


// poeReadControllerInputVoltage
//
// Called by the user to display the TPS23882 controller input voltage.
//
//{"CLI":{"COMMAND": "poe_user_input_voltage","ARGUMENTS":"NONE","RESPONSE":{"ADDR":"2F","VALUE":"1234","CONTROLLER INPUT VOLTAGE":"54.1v (0x42586666)"},"RESULT":"SUCCESS"}}
//
// Input voltage (volts) is calculated from the formula: volts = (reg value & INPUT_VOLTAGE_WORD_MASK) * 3.662 /1000
void poeReadControllerInputVoltage(void)
{
    U16   regValue;
    U16   maskedValue;
    F32   volts;
    U32   voltsAsHex;


    // Read out the device input voltage as a 2-byte read.
    if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_INPUT_VOLTAGE_COMMAND, I2C_DATA_TYPE_WORD, &regValue)) {
        pokeWatchdog();
        // Print the start title for this json string.
        printf("{%s:{%s:\"%s\",%s:%s,%s:{", CLI_STRING, COMMAND_STRING, POE_CONTROLLER_INPUT_VOLTAGE_CMD, ARGUMENTS_STRING, NONE_STRING, RESPONSE_STRING);

        maskedValue   = regValue & INPUT_VOLTAGE_WORD_MASK;
        volts         = maskedValue * INPUT_VOLTAGE_VSTEP_FACTOR / 1000;
        voltsAsHex    = longFromFloat(volts);

        printf("%s:\"%02X\",%s:\"%04X\",%s:\"%3.1fv (0x%08lX)\"}",
               CLI_POE_REG_ADDR_STRING, (U8)POE_INPUT_VOLTAGE_COMMAND, CLI_VALUE_STRING, regValue, POE_CONTROLLER_INPUT_VOLTAGE_STRING, volts, voltsAsHex);

        // Close off the json-like string.
        printf(",%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
    } else {} // Bad event noted, nothing to do.        
}


// userDisplayPoePortPower
//
// Display power for all 8 POE ports based on register readings
// from a RAM table. This command is not intended to be used from the SBC/Gateway
// so the output is free-format, i.e., it is not json-like.
//
void userDisplayPoePortPower(void)
{
    U32 iggy;

    printf("DISPLAYING POWER FOR ALL PORTS:\r\n");
    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {
        printf("PORT %lu (raw volt=0x%04X raw amps=0x%04X): %2.1fV * %2.1fA = %2.1fW\tCLASS:%s\r\n",
               (iggy+1), portPoeInfoDb[iggy].voltageRegRawValue, portPoeInfoDb[iggy].amperageRegRawValue,
                         portPoeInfoDb[iggy].volts,              portPoeInfoDb[iggy].amps,
                         portPoeInfoDb[iggy].wattsPresent,
                         getAutoClassString(portPoeInfoDb[iggy].acConfigPresent));
    }
}


// userPoeReadPortEventRegister
//
// Read 1 of the 5 POE event registers. Or all of them, as the case may be. Return a json-friendly string.
// If an invalid argument was entered, return:
//
// {"CLI":{"COMMAND":"poe_event","ARGUMENTS":"xxxxx","CODE":99,"RESPONSE":"INVALID PARAMETERS","RESULT":"FAIL"}}     <- "event=" not found in the string
// {"CLI":{"COMMAND":"poe_event","ARGUMENTS":"xxxxx","CODE":99,"RESPONSE":"INVALID ARGUMENTS","RESULT":"FAIL"}}      <- if a valid event was not found
//
// If a valid event was specified:
//
// {"CLI":{"COMMAND": "poe_read_event_register","ARGUMENTS":"event=xxxxx","RESPONSE":{"COMMAND":"0x02","VALUE":"0x1234"},"RESULT":"SUCCESS"}}
void userPoeReadPortEventRegister(char *pUserInput)
{
    char  *pArguments;
    char   pEventString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    eventIndex = POE_EVENT_UNKNOWN_INDEX;
    U16    poeData;
    BOOL validEvent = TRUE;

    pArguments = &pUserInput[strlen(POE_READ_PORT_EVENT_REGISTER_CMD) + 1];
    memset(pEventString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_POE_EVENT_EVENT_STRING, pEventString, pArguments);

    if (strlen(pEventString) == 0) {
        pArguments = &pUserInput[strlen(POE_READ_PORT_EVENT_REGISTER_CMD) + 1];
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:99,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, POE_READ_PORT_EVENT_REGISTER_CMD, ARGUMENTS_STRING, pUserInput,
                                                                                               CODE_STRING, RESPONSE_STRING, INVALID_PARAMETERS_STRING, RESULT_STRING, FAIL_STRING);
    } else {
        // Validate the event.
        if      (strncmp(pEventString, CLI_ARG_POE_EVENT_POWER_STRING,     strlen(CLI_ARG_POE_EVENT_POWER_STRING))     == 0) { eventIndex = POE_EVENT_POWER_INDEX;     }
        else if (strncmp(pEventString, CLI_ARG_POE_EVENT_DETECTION_STRING, strlen(CLI_ARG_POE_EVENT_DETECTION_STRING)) == 0) { eventIndex = POE_EVENT_DETECTION_INDEX; } 
        else if (strncmp(pEventString, CLI_ARG_POE_EVENT_FAULT_STRING,     strlen(CLI_ARG_POE_EVENT_FAULT_STRING))     == 0) { eventIndex = POE_EVENT_FAULT_INDEX;     }
        else if (strncmp(pEventString, CLI_ARG_POE_EVENT_START_STRING,     strlen(CLI_ARG_POE_EVENT_START_STRING))     == 0) { eventIndex = POE_EVENT_START_INDEX;     }
        else if (strncmp(pEventString, CLI_ARG_POE_EVENT_ALL_STRING,       strlen(CLI_ARG_POE_EVENT_ALL_STRING))       == 0) { eventIndex = POE_EVENT_ALL_INDEX;       }
        else {
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:99,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, POE_READ_PORT_EVENT_REGISTER_CMD, ARGUMENTS_STRING, pUserInput,
                                                                                                   CODE_STRING, RESPONSE_STRING, INVALID_ARGUMENTS_STRING, RESULT_STRING, FAIL_STRING);
            validEvent = FALSE;
        }

        if (validEvent) {
            switch (eventIndex) {
                case POE_EVENT_POWER_INDEX:
                    if (isReadPoeEventRegister(POE_EVENT_POWER_INDEX,     &poeData)) { printf("POE EVENT POWER (0x%08lX) = 0x%04X\r\n",     POE_POWER_EVENT_RO_CLEAR_COMMAND,      poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    break;
                case POE_EVENT_DETECTION_INDEX:
                    if (isReadPoeEventRegister(POE_EVENT_DETECTION_INDEX, &poeData)) { printf("POE EVENT DETECTION (0x%08lX) = 0x%04X\r\n", POE_DETECTION_EVENT_RO_CLEAR_COMMAND,  poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    break;
                case POE_EVENT_FAULT_INDEX:
                    if (isReadPoeEventRegister(POE_EVENT_FAULT_INDEX,     &poeData)) { printf("POE EVENT FAULT (0x%08lX) = 0x%04X\r\n",     POE_FAULT_EVENT_RO_CLEAR_COMMAND,      poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    break;
                case POE_EVENT_START_INDEX:
                    if (isReadPoeEventRegister(POE_EVENT_START_INDEX,     &poeData)) { printf("POE EVENT START (0x%08lX) = 0x%04X\r\n",     POE_START_EVENT_RO_CLEAR_COMMAND,      poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    break;
                case POE_EVENT_ALL_INDEX:
                    printf("POE *READ ALL* EVENTS:\r\n");
                    if (isReadPoeEventRegister(POE_EVENT_POWER_INDEX,     &poeData)) { printf("POE EVENT POWER (0x%08lX) = 0x%04X\r\n",     POE_POWER_EVENT_RO_CLEAR_COMMAND,      poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    if (isReadPoeEventRegister(POE_EVENT_DETECTION_INDEX, &poeData)) { printf("POE EVENT DETECTION (0x%08lX) = 0x%04X\r\n", POE_DETECTION_EVENT_RO_CLEAR_COMMAND,  poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    if (isReadPoeEventRegister(POE_EVENT_FAULT_INDEX,     &poeData)) { printf("POE EVENT FAULT (0x%08lX) = 0x%04X\r\n",     POE_FAULT_EVENT_RO_CLEAR_COMMAND,      poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                    if (isReadPoeEventRegister(POE_EVENT_START_INDEX,     &poeData)) { printf("POE EVENT START (0x%08lX) = 0x%04X\r\n",     POE_START_EVENT_RO_CLEAR_COMMAND,      poeData); }
                    else                                                             { printf("I2C READ POE EVENT SUPPLY FAILED\r\n");                                                       }
                default:
                    // I just got hit between the eyes with a cosmic ray particle ..
                    break;
            } // switch ...
        }        
    }
}


// userPoeReadSupplyEventRegister
//
//
// {"CLI":{"COMMAND":"poe_read_supply_event_register","ARGUMENTS":"NONE","CODE":0,
// "RESPONSE":{"COMMAND REGISTER":"0x00000009","VALUE":"0x00","POE INFO":"NO-SUPPLY NO-FAULT"},"RESULT":"SUCCESS"}}
//
void userPoeReadSupplyEventRegister(void)
{
    U16 poeData;
    if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_SUPPLY_RO_CLEAR_COMMAND, I2C_DATA_TYPE_BYTE, &poeData)) {
        printf("{%s:{%s:\"%s\",%s:%s,%s:0,%s:{%s:\"0x%08lX\",%s:\"0x%02X\",%s:", CLI_STRING, COMMAND_STRING,  POE_READ_SUPPLY_EVENT_REGISTER_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING,
                                                                                            RESPONSE_STRING, COMMAND_REGISTER_STRING, POE_SUPPLY_RO_CLEAR_COMMAND, VALUE_STRING, poeData, POE_INFO_STRING);
        if (poeData == 0) {
            printf("%s},%s:%s}}\r\n", CLI_POE_NO_SUPPLY_NO_FAULTS_STRING, RESULT_STRING,   SUCCESS_STRING);
        } else {
            // 1 or more bits set: parse and identity the errors.
            printf("\""); // Start with a quote to contain the string-based info set.
            if ((poeData & POE_SUPPLY_FAULT_TSD_MASK)    != 0) { printf("%s", POE_SUPPLY_FAULT_EVENT_TSD_STRING);    }
            if ((poeData & POE_SUPPLY_FAULT_VDUV_MASK)   != 0) { printf("%s", POE_SUPPLY_FAULT_EVENT_VDUV_STRING);   }
            if ((poeData & POE_SUPPLY_FAULT_VDWRN_MASK)  != 0) { printf("%s", POE_SUPPLY_FAULT_EVENT_VDWRN_STRING);  }
            if ((poeData & POE_SUPPLY_FAULT_VPUV_MASK)   != 0) { printf("%s", POE_SUPPLY_FAULT_EVENT_VPUV_STRING);   }
            if ((poeData & POE_SUPPLY_FAULT_OSSE_MASK)   != 0) { printf("%s", POE_SUPPLY_FAULT_EVENT_OSSE_STRING);   }
            if ((poeData & POE_SUPPLY_FAULT_RAMFLT_MASK) != 0) { printf("%s", POE_SUPPLY_FAULT_EVENT_RAMFLT_STRING); }
            printf("\",%s:%s}}\r\n", RESULT_STRING,   SUCCESS_STRING); // leading quote to close off the above string-based info set.
        }            
    } else {
        printf("%s:{%s:\"%s\",%s:%s,%s:99,%s:{%s:%s},%s:%s}}\r\n",
               CLI_STRING, COMMAND_STRING,  POE_READ_SUPPLY_EVENT_REGISTER_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING,
                           RESPONSE_STRING, POE_INFO_STRING, CLI_POE_I2C_READ_FAILED_STRING,
                           RESULT_STRING,   FAIL_STRING);
    }
}


// isReadPoeEventRegister
//
//
//
//
static BOOL isReadPoeEventRegister(U32 eventIndex, U16 *pPoeData)
{
    BOOL readOk = FALSE;
    switch (eventIndex) {
        case POE_EVENT_POWER_INDEX:     readOk = i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_POWER_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD, pPoeData); break;
        case POE_EVENT_DETECTION_INDEX: readOk = i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_DETECTION_EVENT_RO_CLEAR_COMMAND,  I2C_DATA_TYPE_WORD, pPoeData); break;
        case POE_EVENT_FAULT_INDEX:     readOk = i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_FAULT_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD, pPoeData); break;
        case POE_EVENT_START_INDEX:     readOk = i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_START_EVENT_RO_CLEAR_COMMAND,      I2C_DATA_TYPE_WORD, pPoeData); break;
        case POE_EVENT_SUPPLY_INDEX:    readOk = i2cReadGenericData(I2C_DEVICE_TYPE_POE, POE_SUPPLY_RO_CLEAR_COMMAND,           I2C_DATA_TYPE_BYTE, pPoeData); break;
        default:                                                                                                                                               break;
    }
    return readOk;
}


// isUpdateAutoClassConfigOnePair
//
// Read the auto-class config register for a port-pair. The register value for the
// "upper" (or 1st port): 1,2,3,4 is in the [LSB] byte of the word, and the register
// value for the "lower" (or 2nd port): 5,6,7,8 is in the [MSB] byte of the word.  
//
//
static BOOL isUpdateAutoClassConfigOnePair(U32 portPairRank)
{
    U32  command;
    U16  acRegister;
    U16  thisPortRank;
    U8   autoClassConfig;
    BOOL readResult = TRUE;

    // Validate portPair?

    command = POE_AUTO_CLASS_CONFIG_PORT_PAIR_INDEX_LOOKUP[portPairRank].autoClassConfigCommand;
    if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, command, I2C_DATA_TYPE_WORD, & acRegister)) {
        pokeWatchdog();

        // Check the auto class config for the appropriate port based on the lower byte of the register read:
        autoClassConfig = acRegister & 0x00FF;
        thisPortRank    = POE_AUTO_CLASS_CONFIG_PORT_PAIR_INDEX_LOOKUP[portPairRank].firstPortRank;
        portPoeInfoDb[thisPortRank].acConfigPresent = autoClassConfig;

        // Now check the auto class config for the appropriate port based on the upper byte of the register read:
        autoClassConfig = (acRegister & 0xFF00) >> 8;
        thisPortRank    = POE_AUTO_CLASS_CONFIG_PORT_PAIR_INDEX_LOOKUP[portPairRank].secondPortRank;
        portPoeInfoDb[thisPortRank].acConfigPresent = autoClassConfig;
    } else {
        // Bad event noted. Nothing to do.
        readResult = FALSE;
    }        
    return readResult;
}


// getAutoClassString
//
//
//
//
char *getAutoClassString(U8 autoClass)
{
    char *pAutoClassString;
    if      (autoClass == POE_AUTO_CLASS_1)       { pAutoClassString = (char *)POE_AUTO_CLASS_1_STRING;       }
    else if (autoClass == POE_AUTO_CLASS_2)       { pAutoClassString = (char *)POE_AUTO_CLASS_2_STRING;       }
    else if (autoClass == POE_AUTO_CLASS_3)       { pAutoClassString = (char *)POE_AUTO_CLASS_3_STRING;       }
    else if (autoClass == POE_AUTO_CLASS_4)       { pAutoClassString = (char *)POE_AUTO_CLASS_4_STRING;       }
    else if (autoClass == POE_AUTO_CLASS_NOT_POE) { pAutoClassString = (char *)POE_AUTO_CLASS_NOT_POE_STRING; }
    else /* in case of something really bad */    { pAutoClassString = (char *)POE_AUTO_CLASS_UNKNOWN_STRING; }
    return pAutoClassString;
}


// poeGetPowerReadings
//
// Reading the voltage and current for a port pair is about 360msec.
// When v and i are read out, calculate power.
//
static BOOL poeGetPowerReadings(U32 portPairRank)
{
    U32  rawVoltsRegisterValue;
    U32  rawAmpsRegisterValue;
    U16  rawPortRegister;
    U16  regMasked;
    U32  command;
    U32  firstPortRank  = POE_VI_COMMANDS_BY_PORT_PAIR_INDEX_LOOKUP[portPairRank].firstPortRank;
    U32  secondPortRank = POE_VI_COMMANDS_BY_PORT_PAIR_INDEX_LOOKUP[portPairRank].secondPortRank;
    BOOL getPowerResult = FALSE;

    // VOLTAGE:
    command = POE_VI_COMMANDS_BY_PORT_PAIR_INDEX_LOOKUP[portPairRank].voltageCommand;
    if (i2cSuperReadGenericData(I2C_DEVICE_TYPE_POE, command, &rawVoltsRegisterValue)) {
        pokeWatchdog();

        // AMPERAGE:
        command = POE_VI_COMMANDS_BY_PORT_PAIR_INDEX_LOOKUP[portPairRank].currentCommand;
        if (i2cSuperReadGenericData(I2C_DEVICE_TYPE_POE, command, &rawAmpsRegisterValue)) {
            pokeWatchdog();
            getPowerResult = TRUE;

            // Voltage and current readings for this port pair successfully gotten. Calculate power for these 2 ports.

            // Voltage for the 1st port of the pair:
            rawPortRegister  = (U16)((rawVoltsRegisterValue & 0xFFFF0000) >> 16);                            // voltage 1st port
            portPoeInfoDb[firstPortRank].voltageRegRawValue = Swap16(rawPortRegister);                       //    "     "    "
            regMasked        = portPoeInfoDb[firstPortRank].voltageRegRawValue  & PORT_VOLTAGE_WORD_MASK;    //    "     "    "
            portPoeInfoDb[firstPortRank].volts = (F32)(regMasked * INPUT_VOLTAGE_VSTEP_FACTOR) / 1000;       //    "     "    "

            // Voltage for the 2nd port of the pair:
            rawPortRegister  = (U16)(rawVoltsRegisterValue & 0x0000FFFF);                                    // voltage 2nd port
            portPoeInfoDb[secondPortRank].voltageRegRawValue = Swap16(rawPortRegister);                      //    "     "    "
            regMasked        = portPoeInfoDb[secondPortRank].voltageRegRawValue  & PORT_VOLTAGE_WORD_MASK;   //    "     "    "
            portPoeInfoDb[secondPortRank].volts = (F32)(regMasked * INPUT_VOLTAGE_VSTEP_FACTOR) / 1000;      //    "     "    "

            // Amperage for the 1st port of the pair: 
            rawPortRegister  = (U16)((rawAmpsRegisterValue & 0xFFFF0000) >> 16);                             // current 1st port
            portPoeInfoDb[firstPortRank].amperageRegRawValue = Swap16(rawPortRegister);                      //    "     "    "
            regMasked        = portPoeInfoDb[firstPortRank].amperageRegRawValue & PORT_CURRENT_WORD_MASK;    //    "     "    "
            portPoeInfoDb[firstPortRank].amps = (F32)(regMasked * PORT_POWER_I_STEP_FACTOR) / 1000000;       //    "     "    "

            // Amperage for the 2nd port of the pair:
            rawPortRegister  = (U16)(rawAmpsRegisterValue & 0x0000FFFF);                                     // current 2nd port
            portPoeInfoDb[secondPortRank].amperageRegRawValue = Swap16(rawPortRegister);                     //    "     "    "
            regMasked        = portPoeInfoDb[secondPortRank].amperageRegRawValue  & PORT_CURRENT_WORD_MASK;  //    "     "    "
            portPoeInfoDb[secondPortRank].amps = (F32)(regMasked * PORT_POWER_I_STEP_FACTOR) / 1000000;      //    "     "    "

            // With voltage and amperage available for these 2 ports, calculate their power:
            portPoeInfoDb[firstPortRank].wattsPresent  = portPoeInfoDb[firstPortRank].volts  * portPoeInfoDb[firstPortRank].amps;  // power 1st port
            portPoeInfoDb[secondPortRank].wattsPresent = portPoeInfoDb[secondPortRank].volts * portPoeInfoDb[secondPortRank].amps; // power 2nd port

        } else {}
    } else {}

    return getPowerResult;

}


static BOOL isPortLinkStatusUp(U32 portIndex)
{
    U32  kszRank;
    BOOL linkIsUp = FALSE;
    kszRank = POE_PORT_RANK_TO_KSZ_PORT_RANK_LOOKUP[portIndex];
    if (portIndex <= MAX_PRIMARY_DOWNSTREAM_PORT_INDEX) { if (isCurrentPrimaryLinkStatusUp(kszRank))   { linkIsUp = TRUE; } } // Primary controller
    else                                                { if (isCurrentSecondaryLinkStatusUp(kszRank)) { linkIsUp = TRUE; } } // Secondary Controller
    return linkIsUp;
}


// poeDebugOutputOnOff
//
//
void  poeDebugOutputOnOff(BOOL onOrOff)
{
    if (onOrOff == TRUE) { poeInfoCb.poeDebugOutputEnabled = TRUE;  }
    else                 { poeInfoCb.poeDebugOutputEnabled = FALSE; }
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{\"poeDebugOutputEnabled\":%u},%s:%s}}\r\n",
            CLI_STRING, COMMAND_STRING,   (onOrOff == TRUE ? ENABLE_POE_DEBUG_OUTPUT_CMD : DISABLE_POE_DEBUG_OUTPUT_CMD),
                        ARGUMENTS_STRING, NONE_STRING,
                        CODE_STRING,      0,
                        RESPONSE_STRING,  poeInfoCb.poeDebugOutputEnabled,
                        RESULT_STRING,    SUCCESS_STRING);

    sprintf(&pCraftPortDebugData[0], "poeInfoCb.poeDebugOutputEnabled %s\r\n", (onOrOff == TRUE ? "TRUE" : "FALSE")); UART1_WRITE_PACKET_MACRO

}


// displayPoeHighLevelBaseInfo
//
// Display POE info BASIC (or core) info. The opening "POE INFO" with 1 curly bracket would already have been output,
// so only temperature and onward need be displayed.
//
// "TEMPERATURE":32,
// "CONTROLLER INPUT VOLTAGE":"7.7v",
// "PORT DATA":[{"PORT":1,"ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//              {"PORT":2:"ENABLE":"DOWN","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//              .
//              .
//              .
//              {"PORT":8,"ENABLE":"DOWN","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"}],
// "EVENT REGISTERS":{"POWER":"0xFFFF","DETECTION":"0xFFFF","FAULT":"0xFFFF","START":"0xFFFF","SUPPLY/FAULT":"0xFF"}
//
static void displayPoeHighLevelBaseInfo(void)
{
    U16  portMask;
    U16  bitMaskValue;
    S32  temperature;
    U32  iggy;
    F32  pacValue;
    BOOL firstTime = TRUE;

    // Calculate and display the POE device temperature in C (it is presented in the
    // same format as the general AMU board temperature). 
    temperature = POE_TEMPERATURE_CALCULATION(poeInfoCb.temperatureRegister); // float to signed integer
    printf("%s:%ld,%s:\"%3.1f\",", TEMPERATURE_STRING, temperature, POE_CONTROLLER_INPUT_VOLTAGE_STRING, poeInfoCb.inputVoltage);

    // Add POE port data for all 8 ports:
    printf("%s:[", PORT_DATA_STRING); // <- open the outter bracket for the PORT DATA list
    for (iggy = 0; iggy < MAX_NUMBER_DOWNSTREAM_PORTS; iggy++) {
        if (firstTime == TRUE) { firstTime = FALSE; }
        else                   { printf(",");       } // 2nd or subsequent, include a leading comma as list item separator
        portMask     = POE_POWER_ENABLE_BY_PORT_INDEX_LOOKUP[iggy];
        bitMaskValue = portMask & poeInfoCb.powerEnableRegister;
        pacValue     =  (portPoeInfoDb[iggy].pacRegRawValuePresent * POE_PAC_STEP_FACTOR);
        printf("{%s:\"%lu\",%s:%s,%s:\"%2.1f\",%s:\"%2.1f\",%s:%s,",  PORT_STRING,       (iggy+1),                                                   // port number
                                                                      ENABLE_STRING,     (bitMaskValue == 0 ? DOWN_STRING : UP_STRING),              // port enable
                                                                      POWER_STRING,      portPoeInfoDb[iggy].wattsPresent,                           // port power
                                                                      PAC_STRING,        pacValue,                                                   // port PAC
                                                                      AUTO_CLASS_STRING, getAutoClassString(portPoeInfoDb[iggy].acConfigPresent));   // port auto class
        portMask     = POE_AUTO_CLASS_STATUS_BIT_BY_PORT_INDEX_LOOKUP[iggy]; // variable re-used
        bitMaskValue = portMask & poeInfoCb.acStatusRegister;                // variable re-used
        printf("%s:%s}", AC_SUPPORT_STRING, (bitMaskValue == 0 ? NOT_SUPPORTED_STRING : SUPPORTED_STRING));                                          // port auto class supported
    }

    printf("],"); // <- close off the POE DATA list with 1 square bracket. Add a comma for the next component.

    // Add the set of "POE event registers".
    printf("%s:{%s:\"0x%04X\",%s:\"0x%04X\",%s:\"0x%04X\",%s:\"0x%04X\",%s:\"0x%02X\"}", // No extra curly here - let the caller complete the json-like string.
           EVENT_REGISTERS_STRING, POWER_STRING,        poeInfoCb.powerEventRegisterValue,
                                   DETECTION_STRING,    poeInfoCb.detectionEventRegisterValue,
                                   FAULT_STRING,        poeInfoCb.faultEventRegisterValue,
                                   START_STRING,        poeInfoCb.startEventRegisterValue,
                                   SUPPLY_FAULT_STRING, poeInfoCb.supplyStatusRegistervalue);

    return;
}


// displayPoeTemperature
//
// Display POE temperature as follows:
//
// {"POE TEMPERATURE":30}
//
void displayPoeTemperature(void)
{
    S32  temperature;
    temperature = POE_TEMPERATURE_CALCULATION(poeInfoCb.temperatureRegister);
    printf("%s:%ld}", POE_TEMPERATURE_STRING, temperature); // float to signed integer
}


// isPoePortEnabled
//
BOOL isPoePortEnabled(U32 portIndex)
{
    U16  portMask;
    U16  bitMaskValue;

    portMask     = POE_POWER_ENABLE_BY_PORT_INDEX_LOOKUP[portIndex];
    bitMaskValue = portMask & poeInfoCb.powerEnableRegister;
    if (bitMaskValue == 0) { return FALSE; }  // DOWN_STRING
    else                   { return TRUE;  }  // UP_STRING
}


// getPoePortPower
//
F32 getPoePortPower(U32 portIndex)
{
    return portPoeInfoDb[portIndex].wattsPresent;
}


// getPoePortPac
//
F32 getPoePortPac(U32 portIndex)
{
    return (portPoeInfoDb[portIndex].pacRegRawValuePresent * POE_PAC_STEP_FACTOR);
}


// getACSupportStringFromPort
//
char *getACSupportStringFromPort(U32 portIndex)
{
    return getAutoClassString(portPoeInfoDb[portIndex].acConfigPresent);
}


// poePortPowerUpDown
//
// Power UP or power DOWN a single POE port via the POE controller. The KSZ ethernet controllers are not involved.
//
// Command is of the form: poe_only_port_power_up port=<1..8>
//                     or: poe_only_port_power_down port=<1..8>
//
// Output is json-formatted, of the form:
//
// {"CLI":{"COMMAND":"poe_only_port_power_up","ARGUMENTS":"port=3","CODE":0,"RESPONSE":{"PORT":3,"CONFIG":"POWERED UP"},"RESULT":"SUCCESS"}}
//
// or:
//
// {"CLI":{"COMMAND":"poe_only_port_power_down","ARGUMENTS":"port=3","CODE":0,"RESPONSE":{"PORT":3,"CONFIG":"POWERED DOWN"},"RESULT":"SUCCESS"}}
//
void poePortPowerUpDown(char *pUserInput,  U32 portCommand)
{
    char *pArguments;
    char  pPortString[32];
    U32   portKey;

    if     (portCommand == PORT_COMMAND_POWER_UP)     { pArguments = &pUserInput[strlen(POE_ONLY_PORT_POWER_UP_CMD)   + 1]; }
    else /* portCommand == PORT_COMMAND_POWER_DOWN */ { pArguments = &pUserInput[strlen(POE_ONLY_PORT_POWER_DOWN_CMD) + 1]; }
    memset(pPortString, 0, 32);
    findComponentStringData(CLI_ARG_PORT_STRING, pPortString, pArguments);

    if ((strlen(pPortString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        portKey = atoi(pPortString);
        if ((portKey >= 1) && (portKey <= 8)) {
            // Pass a "port index" rather than a "port key".
            if (portCommand == PORT_COMMAND_POWER_UP) {
                poeSetPortPowerUpDown((portKey - 1), POE_PORT_POWER_UP);
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{%s:%lu,%s:%s},%s:%s}}\r\n",
                       CLI_STRING, COMMAND_STRING,   POE_ONLY_PORT_POWER_UP_CMD,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESPONSE_STRING,  PORT_STRING, portKey, CONFIG_STRING, PORT_CONFIG_POWERED_UP_STRING,
                                   RESULT_STRING,    SUCCESS_STRING);
            } else /*(portCommand == PORT_COMMAND_POWER_DOWN)*/ {
                poeSetPortPowerUpDown((portKey - 1), POE_PORT_POWER_DOWN);
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{%s:%lu,%s:%s},%s:%s}}\r\n",
                       CLI_STRING, COMMAND_STRING,   POE_ONLY_PORT_POWER_DOWN_CMD,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESPONSE_STRING,  PORT_STRING, portKey, CONFIG_STRING, PORT_CONFIG_POWERED_DOWN_STRING,
                                   RESULT_STRING, SUCCESS_STRING);
            }
        } else {
            displayInvalidParameters(pUserInput, NULL);
        }
    }
}


// poeReadByte
//
// Read a byte from the POE device, by "command register".
//
void poeReadByte(char *pUserInput)
{
    char *pArguments;
    char  pCommandString[32];
    U32   poeCommand;
    U16   poeData;

    pArguments = &pUserInput[strlen(POE_READ_BYTE_CMD) + 1];
    memset(pCommandString, 0, 32);
    findComponentStringData(CLI_ARG_COMMAND_STRING, pCommandString, pArguments);

    if ((strlen(pCommandString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        poeCommand = atoh(pCommandString);
        if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, poeCommand, I2C_DATA_TYPE_BYTE, &poeData)) {
            pokeWatchdog();
            printf("-> READ POE BYTE: 0x%08lx = 0x%02X/%u\r\n", poeCommand, poeData, poeData);
        } else {
            printf("I FAILED TO I2C BYTE READ\r\n");
        }        
    }
}


// poeReadWord
//
// Read a 16-bit WORD (2 bytes) from the POE device, by "command register".
// The 1st byte read out is considered LSB, the 2nd is MSB.
// The result will display the word with the bytes swapped.
//
void poeReadWord(char *pUserInput)
{
    char *pArguments;
    char  pCommandString[32];
    U32   poeCommand;
    U16   rawData;
    U16   poeData;

    pArguments = &pUserInput[strlen(POE_READ_WORD_CMD) + 1];
    memset(pCommandString, 0, 32);
    findComponentStringData(CLI_ARG_COMMAND_STRING, pCommandString, pArguments);

    if ((strlen(pCommandString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        poeCommand = atoh(pCommandString);
        if (i2cReadGenericData(I2C_DEVICE_TYPE_POE, poeCommand, I2C_DATA_TYPE_WORD, &rawData)) {
            pokeWatchdog();
            poeData    = ((rawData & 0xFF00) >> 8) | ((rawData & 0x00FF) << 8);
            printf("-> READ POE WORD 0x%08lx: raw=0x%04X SWAPPED: 0x%04X/%u\r\n", poeCommand, rawData, poeData, poeData);
        } else {
        printf("I FAILED TO I2C WORD READ\r\n");
        }
    }
}


// poeWriteByte
//
// Write 1 byte to the POE device, by "command register".
//
void poeWriteByte(char *pUserInput)
{
    char *pArguments;
    char  pCommandString[32];
    char  pValueString[32];
    U32   poeCommand;
    U8    poeData;

    pArguments = &pUserInput[strlen(POE_WRITE_BYTE_CMD) + 1];
    memset(pCommandString, 0, 32);
    memset(pValueString,   0, 32);
    findComponentStringData(CLI_ARG_COMMAND_STRING, pCommandString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValueString,   pArguments);

    if ((strlen(pCommandString) == 0) || (strlen(pValueString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        poeCommand = atoh(pCommandString);
        poeData    = atoh(pValueString);
        printf("-> WRITE POE device *byte*: 0x%08lx = 0x%02X\r\n", poeCommand, poeData);
        i2cWriteGenericData(poeCommand, poeData, I2C_DATA_TYPE_BYTE);
    }
}


// poeWriteWord
//
// Write a word (2 bytes) to the POE device, by "command register".
//
void poeWriteWord(char *pUserInput)
{
    char *pArguments;
    char  pCommandString[32];
    char  pValueString[32];
    U32   poeCommand;
    U16   poeData;

    pArguments = &pUserInput[strlen(POE_WRITE_WORD_CMD) + 1];
    memset(pCommandString, 0, 32);
    memset(pValueString,   0, 32);
    findComponentStringData(CLI_ARG_COMMAND_STRING, pCommandString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValueString,   pArguments);

    if ((strlen(pCommandString) == 0) || (strlen(pValueString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        poeCommand = atoh(pCommandString);
        poeData    = atoh(pValueString);
        printf("-> WRITE POE device *word*: 0x%08lx = 0x%04X\r\n", poeCommand, poeData);
        i2cWriteGenericData(poeCommand, poeData, I2C_DATA_TYPE_WORD);
    }
}


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
