/**
* \file
*
* \brief Apparent MAC Resolution Definitions.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _MAC_RESOLUTION_H_
#define _MAC_RESOLUTION_H_


// Define the states of the MAC resolution state machine:
#define MR_SM_IDLE                          1
#define MR_SM_LAUNCH                        2
#define MR_SM_READING_FROM_PRIMARY          3
#define MR_SM_PREPARE_SECONDARY_READ        4
#define MR_SM_READING_FROM_SECONDARY        5


// MAC resolution info control block.
typedef struct s_macResolutionInfoControlBlock
{
    U32  stateMachineState;
    U32  macListLength;           // 
    U32  macListRank;             // rank into the MAC list being resolved
    U32  secondaryEntries;        // number of entries to read from the secondary ALU
    U32  startTimestamp;          // tick when the state machine was 1st launched
    U32  endTimestamp;            // tick when state machine went idle
    U16  secondaryIndex;          // index of the secondary ALU to read
    U8   numberEntriesChanged;    // detects change during secondary sequential reading
    U8   pad;                     // just keep things neat and clean
} t_macResolutionInfoControlBlock;

// When reporting the "number of entries" results to the SBC:
#define NUMBER_OF_ENTRIES_NO_CHANGE         1
#define NUMBER_OF_ENTRIES_INCREASED         2
#define NUMBER_OF_ENTRIES_DECREASED         3

// When reading from the secondary controller, always read these many more entries
// than was indicated by number-of-entries from ALU row index 0:
#define EXTRA_SECONDARY_READS               2

#define CONTROLLER_ID_UNKNOWN               1
#define CONTROLLER_ID_PRIMARY               2
#define CONTROLLER_ID_SECONDARY             3

// Data descriptor for each entry of the MAC list from the SBC:
typedef struct s_macResolutionDataBlock
{
    U8    pMac[6];
    BOOL  secondAluReadRequired; // mac[6] + 1 byte BOOL + 1 byte U8 = 8 bytes for 32-bit alignment
    U8    pad2;                  // mac[6] + 2 more U8 = 8 * U8 for 32-bit alignment
    U32   controllerId;          //  (0=UNKNOWN, 1=PRIMARY, 2=SECONDARY)
    U32   aluIndex;              // from 0..4095 if PRIMARY, or 0..1023 if SECONARY
    U32   kszPort;               // (0,1,2,3,4,5,6 where: 1,2,3,4 = actual downstream port for either controller; 5 is the LAN port on the PRIMARY controller; 6 is the RGMII pathway between the 2 controllers; 0 = UNKNOWN)
    char *pPort;                 // (the downstream port from the user�s perspective: �1�, �2�, �3�, .. �8�, �LAN�, �UNKNOWN�)
} t_macResolutionDataBlock;

#define MAC_RESOLUTION_LIST_LENGTH_MAX      25


// External Function Prototypes

void  initMacResolutionData            (void);
void  getMacResolutionResult           (void);
void  resolveMacList                   (char *pUserInput);
BOOL  isMacResolutionSmIdle            (void);
void  checkMacResolutionSm             (void);


 
/*----------------------------------------------------------------------------*/
#endif   /* _MAC_RESOLUTION_H_ */
