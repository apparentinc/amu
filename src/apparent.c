/**
 *
 * Apparent utilities module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2021 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains utility support routines for the Apparent Gateway PCBA system.
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "pmc.h" // ???
#include "ASF/sam/utils/cmsis/same70/include/instance/pioc.h"
#include "ASF/sam/utils/cmsis/same70/include/instance/rstc.h"

#include "usart.h"
#include "ASF/sam/utils/cmsis/same70/include/component/twihs.h"
#include "ASF/sam/utils/cmsis/same70/include/component/rstc.h"
#include "status_codes.h"
#include "uart_serial.h"

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "board.h"

#include "apparent_strings.h"
#include "apparent.h"
#include "amu_common/flashinator.h"
#include "amu_common/rgbLedActivity.h"
#include "poe.h"
#include "i2c.h"
#include "primary_switch.h"
#include "secondary_switch.h"
#include "mac_resolution.h"
#include "alu_mac_info.h"


//
// Apparent note:
//
// The flash utility in the src/ASF/sam/flash_efc folder contains the 2 flash_efc.c and flash_efc.h files
// for the interface to the SAME70 "user signature" area of flash (Section "22.4.3.9 User Signature Area"
// in the SAME70 data sheet).
//
// Problem: after adding those 2 files to the project, the apparent.c file failed to locate the flash_efc.h
// header for inclusion in the build. The instructions to add this to the linker specified to:
//
// - open: Project -> AMU Properties -> ARM/GNU C Compiler -> Directories
// - click on "Add Files"
//
// But this simply did not work. So live with this funky relative path.

#include "ASF/sam/flash_efc/flash_efc.h"


// EXTERN DECLARATIONS:
extern U32  g_ul_ms_ticks;
extern BOOL g_b_led0_active;

// In main.c module:
extern void mdelay(uint32_t ul_dly_ticks);
extern void setButtonChangeTimestamp(void);

extern uint32_t _sfixed;

// To support DROPPED PACKET and/or DROPPED PACKET ACK in a debug test setting.
// Normally, the code that checks and acts upon these 2 variables is excluded
// from the compilation. Look for and search for the DEBUG_UPGRADE compile option.
extern BOOL debugDropTheNextIncomingDataPacket;


// CONSTANT DECLARATIONS:

// Calculate a 32-bit CRC over a range of memory. For use when committing the "user signature" to
// flash, and when committing sectors of memory to flash during a firmware (or bootloader) upgrade:
static const U32 CRC32_TABLE[] =
{
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f, 0xe963a535, 0x9e6495a3,
    0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988, 0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91,
    0x1db71064, 0x6ab020f2, 0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9, 0xfa0f3d63, 0x8d080df5,
    0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172, 0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,
    0x35b5a8fa, 0x42b2986c, 0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423, 0xcfba9599, 0xb8bda50f,
    0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924, 0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,
    0x76dc4190, 0x01db7106, 0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d, 0x91646c97, 0xe6635c01,
    0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e, 0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457,
    0x65b0d9c6, 0x12b7e950, 0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7, 0xa4d1c46d, 0xd3d6f4fb,
    0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0, 0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9,
    0x5005713c, 0x270241aa, 0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81, 0xb7bd5c3b, 0xc0ba6cad,
    0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a, 0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683,
    0xe3630b12, 0x94643b84, 0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb, 0x196c3671, 0x6e6b06e7,
    0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc, 0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5,
    0xd6d6a3e8, 0xa1d1937e, 0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55, 0x316e8eef, 0x4669be79,
    0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236, 0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f,
    0xc5ba3bbe, 0xb2bd0b28, 0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f, 0x72076785, 0x05005713,
    0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38, 0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,
    0x86d3d2d4, 0xf1d4e242, 0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69, 0x616bffd3, 0x166ccf45,
    0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2, 0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,
    0xaed16a4a, 0xd9d65adc, 0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693, 0x54de5729, 0x23d967bf,
    0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94, 0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};


// Port key lookup table: not to be confused with "portIndex" or "portRank".
// Used to determine if a specified port is PRIMARY or SECONARY when processing CLI user input.
// There are 5 PRIMARY ports with port keys:   [0]             = port "LAN"
//                                             [1],[2],[3],[4] = port 1,2,3,4
// There are 4 SECONDARY ports with port keys: [5],[6],[7],[8] = port 5,6,7,8


// A lookup table to get the "port index" from a "port key" based on port key 0..8 where 0 = LAN.
const U8 PORT_KEY_TO_PORT_INDEX_LOOKUP[] = { PORT_LAN_INDEX, // key [0]: upstream port LAN = port "5" w.r.t. primary controller:   (portRank [4] in primary control data tables by port)
                                             PORT_1_INDEX,   // key [1]: downstream port 1 = port "1" w.r.t. primary controller:       "     [0]  "    "      "     "    "   "   "   "
                                             PORT_2_INDEX,   // key [2]: downstream port 2 = port "2"   "       "        "             "     [1]  "    "      "     "    "   "   "   "
                                             PORT_3_INDEX,   // key [3]: downstream port 3 = port "3"   "       "        "             "     [2]  "    "      "     "    "   "   "   "
                                             PORT_4_INDEX,   // key [4]: downstream port 4 = port "4"   "       "        "             "     [3]  "    "      "     "    "   "   "   "
                                             PORT_5_INDEX,   // key [5]: downstream port 5 = port "1" w.r.t. secondary controller: (portRank [0] in secondary control data tables by port)
                                             PORT_6_INDEX,   // key [6]: downstream port 6 = port "2"   "       "        "             "     [1]  "    "      "     "    "   "   "   "
                                             PORT_7_INDEX,   // key [7]: downstream port 7 = port "3"   "       "        "             "     [2]  "    "      "     "    "   "   "   "
                                             PORT_8_INDEX    // key [8]: downstream port 8 = port "4"   "       "        "             "     [3]  "    "      "     "    "   "   "   "
                                           };


// GLOBAL DATA:

// -- Product:     ATIRA0000
// -- Serial:      XXXXX
// -- Model:       PCBA-1
// These 3 components are read out of the User Signature AM block:
static char pAmuProduct[AMU_COMPONENT_DATA_SIZE_MAX];
static char pAmuSerialNumber[AMU_COMPONENT_DATA_SIZE_MAX];
static char pAmuModel[AMU_COMPONENT_DATA_SIZE_MAX];

// The board's hardware version from the PCBA Option Jumpers.
static t_hardwareRevision pcbaHardwareRevision;

// The SAME70 "User Signature" internal flash memory, to be read out and stored
// in this RAM block. it must align to 32-bit word boundary.
static U32  pApparentUserSignature[FLASH_USER_SIG_SIZE_IN_LONGS];

static U32  userSignatureCalculatedCrc;
static U32  userSignatureExtractedCrc;

// Application/boot load flash addresses:
U32  bootloaderLoadFlashAddress;
U32  applicationLoadFlashAddress;

// Application/boot flash signatures:
U32  applicationFlashSignature;
U32  bootloaderFlashSignature;

t_jumpToBootOrResetControlBlock jumpToBootOrResetCb;

// If the AMU is managed by the SBC/Gateway, then proceed with ethernet and POE controller checking.
// Otherwise, the AMU is considered to be in an isolated state, so this checking is not done.
BOOL amuIsManaged;

// For the management of the 4 input and 4 output contact closures:
// - the output contact closures are configured by the Gateway as open/closed
// - the input contact closures are statuses that are checked from main
t_outputContactClosureInfo gOutputCcConfig[NUM_OUTPUT_CONTACT_CLOSURES];
t_inputContactClosureInfo  gInputCcStatus[NUM_INPUT_CONTACT_CLOSURES];
U32 nextInputCCRankToCheck;

// A lookup table to support initialization of input contact closure with the wired (i.e., connected) GPIO.
static const U32 GPIO_BY_INPUT_CC_LOOKUP [] = { CC_IN_1_GPIO, // [0]: input CC 1
                                                CC_IN_2_GPIO, // [1]: input CC 2
                                                CC_IN_3_GPIO, // [2]: input CC 3
                                                CC_IN_4_GPIO  // [3]: input CC 4
                                              };

// fan speed control block:
t_temperatureFanControlCb tempFanControlCb;

// Counters. 2 types:
//
// BAD EVENTS COUNTERS:
static U32 amuBadEventsCountersBlock[MAX_BAD_EVENTS_COUNTERS];

// Data to manage user input over UART0/SBCCOM interface.
static char pSbcComInputBuffer[SBCCOM_INPUT_BUFFER_LENGTH];
U32         sbcComInputBufferRank;     // rank into which the next input character is written
BOOL        sbcComInputReady;          // set by UART0_Handler() when input is ready for processing
BOOL        inputActivelyProcessed;    // indicates that the application is actively processing the input string
BOOL        sbcComEchoChar;            // interrupt routine: echo each character received if TRUE
static BOOL inputChecksumRequired;     // TRUE by default except when disabled for local testing
BOOL        i2cTwishOutputEnabled;     // set to TRUE for test

volatile U16  charsWrittenByInterrupt; // to support calculation of the checksum
volatile U32  amuOutputChecksum;       // calculated checksum

// Data to manage user input over UART1/CRAFT interface.
static char pCraftInputBuffer[CRAFT_INPUT_BUFFER_LENGTH];
U32         craftInputBufferRank;      // rank into which the next input character is written
BOOL        craftInputReady;           // set by UART1_Handler() when input is ready for processing
BOOL craftDebugOutputEnabled;          // set to TRUE for test

// The state machine that checks config/status registers of all ethernet ports:
static t_ethernetPortCheckingSmCb portCheckingSmCb;

// The SAME70 reset controller status register contains the last reset reason.
t_same70ResetInfoDataBlock same70ResetInfoDb;

// For autonomous event processing and reporting:

// I dunno how to declare the following constant string with the value of the
// AUTONOMOUS_EVENT_GENERIC_BAD_EVENT enumeration element (which is 24). Doing
// it this way is risky, however, this value is used in the SBC/Gateway, so it
// should NEVER change (only deprecated), otherwise it breaks the interface.
const char BAD_EVENT_AUTO_EVENT_STRING[] = ",{\"EVT\":{\"EI\":24}}";

// When the manager (i.e., the SBC/Gateway controller) makes a request to retrieve
// the autonomous event string, use this to close off the json-like string.
static const char AUTONOMOUS_EVENT_TERMINATION_STRING[] = "]}\r\n";

// Placeholder into which string-based autonomous event info is written. This string is returned
// to the SBC/Gateway when it makes the CLI request to retrieve the autonomous event info.
static char pAutonomousEventString[AUTONOMOUS_EVENTS_STRING_SIZE];

// To prepare a single json event for the general-purpose autonomous event string.
char pSingleJsonEventString[SINGLE_JSON_EVENT_STRING_SIZE];

// Add the BAD_EVENT_AUTO_EVENT_STRING to the autonomous event string once only. 
static BOOL badEventAddedToAutoString;

// For debug:
char pCraftPortDebugData[128];


// STATIC MODULE PROTOTYPES:

static void  displayControllerResetReason        (BOOL jsonYes);
static void  initApparentData                    (void);
static void  initAmuCounters                     (void);
static void  displayAndResetBadEventsCounters    (void);
static void  ethernetControllerReset             (char *pUserInput);
static void  readBothControllersMac              (void);
static void  configureStaticLanMac               (char *pUserInput);
static void  userGetStaticLanMacInfo             (void);
static void  displayStaticLanMacInfo             (void);
static void  processUart0InputString             (char *pUserInput);
static void  processUart1InputString             (char *pUserInput);
static BOOL  isInputChecksumValid                (char *pUserInput);
static BOOL  isEthernetCheckingEnabled           (void);
static void  setEthernetCheckingEnabled          (BOOL enable);
static void  displayApparentHelp                 (char *pUserInput);
static void  displayCliCommandList               (void);
static void  displayCliHelpCommandGroup1         (void);
static void  displayCliHelpCommandGroup2         (void);
static void  displayCliHelpCommandGroup3         (void);
static void  displayCliHelpCommandGroup4         (void);
static void  contactClosureOutputControl         (char *pUserInput, U32 action);
static void  initInputCCStatusData               (void);
static void  checkThisInputContactClosure        (U32 ccNumber);
static void  displayContactClosureInfo           (void);
static void  displayTemperature                  (void);
static void  updateTemperatureReading            (void);
static char *speedStringFromCurrentSpeed         (void);
static void  displayTempAndFanInfo               (void);
static void  tempFanInfo                         (void);
static void  fanInfo                             (char *pUserInput);
static void  fantasticControl                    (char *pUserInput);
static void  setFanControlSpeedSettings          (char *pUserInput);
static void  setNewFanSpeed                      (U32 u11FanSpeed);
static void  checkTemperatureForFanSpeed         (void);
static void  readU11FanInfoItem                  (const char *pSomeString, U32 readFanInfoItem);
static void  writeU11FanSpeed                    (U32 speed, BOOL jsonYes, char *pArguments);
static void  helloResponse                       (char *pUserInput);
static void  helloResponseFullData               (void);
static void  helloResponseTerseData              (void);
static void  displayFlashSignatures              (void);
static void  readOutProgramFlashSignatures       (void);
static void  getAutonomousEvents                 (void);
static void  executeNOP                          (void);
static void  disableWatchdog                     (void);
static void  executeJumpToBootloader             (void);
static void  executeSoftwareReset                (void);
static void  resetFunc                           (void);
static void  prepareSoftwareResetFunc            (void);
static void  displayFlashProgramSignatures       (void);

static U32   findEndOfInputRank                  (char * pUserInput);
static void  processUpgradeCommand               (char *pUserInput);

static void  updateUserSignatureFromFlash        (void);
static void  extractDataFromUserSignatureInRam   (void);
static void  readUserSignatureFlash              (void);
static void  readUserSignatureRam                (void);
static void  userSignatureWrite                  (char *pUserInput);
static void  userSignatureErase                  (void);
static void  userDisplayPortInfo                 (char *pUserInput);
static void  userGetContactClosureInfo           (void);
static void  enableEthernetControllerChecking    (BOOL enable);
//static void  getSwitchMibCounterCommand          (char *pUserInput);                        disabled
//static void  enableFlushFreezeAllPortsCommand    (void);                                    disabled
//static void  disableFlushFreezeAllPortsCommand   (void);                                    disabled
//static void  flushSwitchMibCountersCommand       (char *pUserInput);                        disabled
//static void  freezeSwitchMibCountersCommand      (char *pUserInput);                        disabled
//static void  unfreezeSwitchMibCountersCommand    (char *pUserInput);                        disabled
//static U16   portStringToPrimaryPortKey          (char *pPortNumberString);                 <- because mib disabled
//static U16   portStringToSecondaryPortKey        (char *pPortNumberString);                 <- because mib disabled
//static void  portKeyToPrimaryPortString          (U16 portKey, char *pPortString);          <- because mib disabled
static void  readUsart0ModeStatusCommand         (void);
static void  readUsart2ModeStatusCommand         (void);
static void  same70ReadRegisterCommand           (char *pUserInput);
static void  same70WriteRegisterCommand          (char *pUserInput);
static void  usartResetStatusCommand             (char *pUserInput);
static void  readPrimaryByteByAddress            (char *pUserInput);
static void  writePrimaryByteByAddress           (char *pUserInput);
static void  readPrimaryWordByAddress            (char *pUserInput);
static void  writePrimaryWordByAddress           (char *pUserInput);
static void  read5PortsByteByAddress             (char *pUserInput);
static void  write5PortsByteByAddress            (char *pUserInput);
static void  read5PortsWordByAddress             (char *pUserInput);
static void  write5PortsWordByAddress            (char *pUserInput);
static void  readSecondary4PortsByBaseAddress    (char *pUserInput);
static void  readSecondaryByteAddress            (char *pUserInput);
static void  writeSecondaryByteByAddress         (char *pUserInput);
static BOOL  isPortKeyInList                     (U8    portKey,         U8  *pPortKeyList);
static BOOL  isCcNumberInList                    (U32 ccNumber,          U32 *pCcNumberList);
static void  getCcNumberListFromCcNumberArg      (char *pUserInput, char *pCcNumberString, U32 *pCcNumberList);
static void  portPowerUpDown                     (char *pUserInput, U32 portCommand);
static void  portEthernetOnlyUpDown              (char *pUserInput, U32 portCommand);
static void  i2cDebugOutputOnOff                 (BOOL  enable);
static void  craftDebugOutputOnOff               (BOOL  enable);
static void  toggleChecksumRequired              (void);
static U32   readOneProgramFlashSignature        (U32 baseMemoryAddress);
static void  jumpToBootloader                    (void);
static void  programResponse                     (void);
static void  setFlashHoldingPattern              (char *pUserInput);
static void  readFromFlashMemoryIntoHolding      (char *pUserInput);
static void  writeFromHoldingIntoFlashMemory     (char *pUserInput);
static void  calculateAndPrintHoldingBlockCrc    (void);
static void  isCheckFlashErasedTestCommand       (char *pUserInput);
static void  readFlashCommand                    (char *pUserInput);
static void  writeFlashLongCommand               (char *pUserInput);
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
static BOOL  isFlashErasedMemoryRange            (U32 baseMemoryAddress, U32 numberOfInstructionWords);
static void  readFlashMemoryRange                (U32 baseMemoryAddress, U32 numberOfInstructionWords);
#endif

// MACRO FOR TESTING:
#define UART1_WRITE_PACKET_MACRO usart_serial_write_packet((usart_if)UART1, (uint8_t *)pCraftPortDebugData, strlen(pCraftPortDebugData));




/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond


// Exported/external functions from this module:


// getProcessorResetReason
//
// This function is identical on the bootloader program, and ought to be in a common module.
//
// Get the SAME70 reset reason.
void getProcessorResetReason(void)
{
    same70ResetInfoDb.status       =  RSTC->RSTC_SR;
    same70ResetInfoDb.statusMasked = same70ResetInfoDb.status & RSTC_SR_RSTTYP_Msk;

    if      ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_GENERAL_RST) == RSTC_SR_RSTTYP_GENERAL_RST) { same70ResetInfoDb.reason = RESET_REASON_GENERAL;  }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_BACKUP_RST)  == RSTC_SR_RSTTYP_BACKUP_RST)  { same70ResetInfoDb.reason = RESET_REASON_BACKUP;   }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_WDT_RST)     == RSTC_SR_RSTTYP_WDT_RST)     { same70ResetInfoDb.reason = RESET_REASON_WATCHDOG; }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_SOFT_RST)    == RSTC_SR_RSTTYP_SOFT_RST)    { same70ResetInfoDb.reason = RESET_REASON_SOFTWARE; }
    else if ((same70ResetInfoDb.statusMasked | RSTC_SR_RSTTYP_USER_RST)    == RSTC_SR_RSTTYP_USER_RST)    { same70ResetInfoDb.reason = RESET_REASON_NRST_LOW; }
    else                                                                                                  { same70ResetInfoDb.reason = RESET_REASON_UNKNOWN;  }

}


// displayControllerResetReason
//
// Called to display reset reason info, either as part of the response to the "banner" command
// (so free-format output), or as part of some other command where the output is json-structured.
// Out the following:
//
// - if free-format:     - SAME70 Reset Reason: SOFTWARE ("RESET INFO"=0x00010300 masked=0x00000300)
// - if json-structured: "SAME70 RESET REASON":"SOFTWARE","RESET INFO":"0x00010300 0x300"
//
// This function is identical on the bootloader program, and ought to be in a common module.
//
static void displayControllerResetReason(BOOL jsonYes)
{
    // How about a lookup table, huh?
    if (jsonYes) { printf("%s:", SAME70_RESET_REASON_STRING); } else { printf("-- SAME70 Reset Reason: ");         }
    if      (same70ResetInfoDb.reason == RESET_REASON_GENERAL)  { if (jsonYes) { printf("%s,", RESET_REASON_GENERAL_STRING);  } else { printf("%s ", RESET_REASON_GENERAL_STRING);  } }
    else if (same70ResetInfoDb.reason == RESET_REASON_BACKUP)   { if (jsonYes) { printf("%s,", RESET_REASON_BACKUP_STRING);   } else { printf("%s ", RESET_REASON_BACKUP_STRING);   } }
    else if (same70ResetInfoDb.reason == RESET_REASON_WATCHDOG) { if (jsonYes) { printf("%s,", RESET_REASON_WATCHDOG_STRING); } else { printf("%s ", RESET_REASON_WATCHDOG_STRING); } }
    else if (same70ResetInfoDb.reason == RESET_REASON_SOFTWARE) { if (jsonYes) { printf("%s,", RESET_REASON_SOFTWARE_STRING); } else { printf("%s ", RESET_REASON_SOFTWARE_STRING); } }
    else if (same70ResetInfoDb.reason == RESET_REASON_NRST_LOW) { if (jsonYes) { printf("%s,", RESET_REASON_NRST_LOW_STRING); } else { printf("%s ", RESET_REASON_NRST_LOW_STRING); } }
    else                                                        { if (jsonYes) { printf("%s,", RESET_REASON_UNKNOWN_STRING);  } else { printf("%s ", RESET_REASON_UNKNOWN_STRING);  } }

    if (jsonYes) { printf("%s:\"0x%08lX 0x%0lX\"",           RESET_INFO_STRING, same70ResetInfoDb.status, same70ResetInfoDb.statusMasked); }
    else         { printf("(%s=0x%08lX masked=0x%08lX)\r\n", RESET_INFO_STRING, same70ResetInfoDb.status, same70ResetInfoDb.statusMasked); }
}


// initApparentSubsystemData
//
// Called from main during system startup.
//
void initApparentSubsystemData(void)
{
    // Init data.
    initApparentData();
    initCraftInputBuffer();
    initSbcComInputBuffer();
    initAmuCounters();
    readOutProgramFlashSignatures();

    // Read out the User Signature from internal flash.
    updateUserSignatureFromFlash();

    // Extract all relevant info from the user signature.
    extractDataFromUserSignatureInRam();

    // For autonomous event processing and reporting:
    memset(&pAutonomousEventString[0], 0, AUTONOMOUS_EVENTS_STRING_SIZE);
    memset(&pSingleJsonEventString[0], 0, SINGLE_JSON_EVENT_STRING_SIZE);
    badEventAddedToAutoString = FALSE;

    // Where in flash memory this application is loaded:
    applicationLoadFlashAddress = (U32)((uint32_t *) & _sfixed) - IFLASH_START_ADDRESS;

    initRgbLedActivityData();

    i2cTwishOutputEnabled                    = FALSE; // for test/analysis
    craftDebugOutputEnabled                  = FALSE; // for test/analysis

    pcbaHardwareRevision.major               = 0;
    pcbaHardwareRevision.minor               = 0;

    // If the AMU is managed by the SBC/Gateway:
    amuIsManaged = FALSE;
}


// writeToSbcComInputBuffer
//
// Called by the UART0 interrupt handler to write a single character to the user input buffer.
// Check for buffer overflow by checking the input string length to the max buffer size. Also
// check against the safety margin: if above that limit, then either the UART0 input buffer
// needs to be increased, or the SBC controller needs to be more careful.
//
// Also check: if the input buffer is being actively processed by the application, increment a counter.
//             This is for observation purpose. If this event is detected, then changes are required,
//             such as disabling interrupts during input processing.
void writeToSbcComInputBuffer(char oneChar)
{
    if (inputActivelyProcessed) { incrementBadEventCounter(SBC_INPUT_WHILE_PROCESSING); }

    if (sbcComInputBufferRank < SBCCOM_INPUT_BUFFER_LENGTH) {
        pSbcComInputBuffer[sbcComInputBufferRank++] = oneChar;
        if (sbcComInputBufferRank > SBCCOM_INPUT_BUFFER_SAFETY_MARGIN) { incrementBadEventCounter(UART0_INPUT_BUFFER_ABOVE_SAFETY_MARGIN); }
    } else {
        // ICK! Buffer is FULL!!!
        incrementBadEventCounter(UART0_INPUT_BUFFER_OVERFLOW);
        initSbcComInputBuffer();
    }
}


// writeToCraftInputBuffer
//
// Called by the UART1 interrupt handler to write a single character to the craft debug buffer.
// Check for buffer overflow by checking the input string length to the max buffer size. Also
// check against the safety margin: if above that limit, then either the UART1 input buffer
// needs to be increased, or user input over this mostly debug interface needs to be more careful.
//
void writeToCraftInputBuffer(char oneChar)
{
    if (craftInputBufferRank < CRAFT_INPUT_BUFFER_LENGTH) {
        pCraftInputBuffer[craftInputBufferRank++] = oneChar;
        if (craftInputBufferRank > CRAFT_INPUT_BUFFER_SAFETY_MARGIN) { incrementBadEventCounter(UART1_INPUT_BUFFER_ABOVE_SAFETY_MARGIN); }
    } else {
        // ICK! Buffer is FULL!!!
        incrementBadEventCounter(UART1_INPUT_BUFFER_OVERFLOW);
        initCraftInputBuffer();
    }
}


void incrementBadEventCounter(U16 badEventIndex)
{
    if (badEventIndex < MAX_BAD_EVENTS_COUNTERS) { amuBadEventsCountersBlock[badEventIndex]++;              }
    else                                         { amuBadEventsCountersBlock[INVAL_COUNTER_TO_INCREMENT]++; }
    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u}}", EVT_STRING, EVENT_ID_STRING, AEI_GENERIC_BAD_EVENT);
    addJsonToAutonomnousString(pSingleJsonEventString);

    if (craftDebugOutputEnabled == TRUE) { sprintf(&pCraftPortDebugData[0], "BAD EVENT %u\r\n", badEventIndex); UART1_WRITE_PACKET_MACRO; }
}




/**
 * Apparent
 * User/debug input buffer (CRAFT/J50) initialization. Called by main on system startup,
 * when user input is ready for processing, and when a full buffer is detected.
 * It is the responsibility of the UART1 interrupt service routine to set "ready".
 */
void initCraftInputBuffer(void)
{
    memset((char *)&pCraftInputBuffer[0], 0, CRAFT_INPUT_BUFFER_LENGTH);
    craftInputBufferRank = 0;
    craftInputReady      = FALSE;
}


/**
 * Apparent
 * Application input buffer (SBCCOM/J21) initialization. Called by main on system startup,
 * when application input is ready for processing, and when a full buffer is detected.
 * It is the responsibility of the UART0 interrupt service routine to set "ready".
 */
void initSbcComInputBuffer(void)
{
    memset((char *)&pSbcComInputBuffer[0], 0, SBCCOM_INPUT_BUFFER_LENGTH);
    sbcComInputBufferRank   = 0;
    amuOutputChecksum       = 0;
    charsWrittenByInterrupt = 0;
    sbcComInputReady        = FALSE;
    inputActivelyProcessed  = FALSE;
}


/**
 * Apparent
 * Check if UART0 interrupt handler has set user input as ready to process.
 * If the ready bit is set, then interpret whatever is in the buffer.
 *
 * Currently, no requirements specified for what is expected on this port.
 * For test/feasibility: a few commands are parsed.
 */
void checkSbcComUserInput(void)
{
    if (sbcComInputReady == TRUE) {
        inputActivelyProcessed = TRUE;
        processUart0InputString(pSbcComInputBuffer);
        initSbcComInputBuffer();
        pokeWatchdog();
    }
}


/**
 * Apparent
 * Check if UART1 interrupt handler has set user input as ready to process.
 * If the ready bit is set, then interpret whatever is in the buffer.
 *
 * THIS FUNCTION: to be customized for development purpose.
 */
void checkCraftUserInput(void)
{
    if (craftInputReady == TRUE) {
        processUart1InputString(pCraftInputBuffer);
        initCraftInputBuffer();
        pokeWatchdog();
    }
}


// The static functions of this module:



/**
 * Apparent
 * Init data to support general AMU functionality and features.
 */
static void initApparentData(void)
{
    charsWrittenByInterrupt   = 0;
    amuOutputChecksum         = 0;
    sbcComEchoChar            = FALSE;
    inputChecksumRequired     = TRUE;

    // The ethernet port-checking state machine control block:
    portCheckingSmCb.lastCheckTick      = g_ul_ms_ticks;
    portCheckingSmCb.state              = ETH_SM_START;
    portCheckingSmCb.primaryNextIndex   = 0;
    portCheckingSmCb.secondaryNextIndex = 0;
    portCheckingSmCb.enabled            = FALSE; // wait for SBC/Gateway keep-alive

    // Will be updated when data is read out from the User Signature.
    strcpy(pAmuProduct,      UNKNOWN_STRING);
    strcpy(pAmuSerialNumber, UNKNOWN_STRING);
    strcpy(pAmuModel,        UNKNOWN_STRING);

    jumpToBootOrResetCb.jumpToBootTimestamp = 0;
    jumpToBootOrResetCb.resetTimestamp      = 0;
    jumpToBootOrResetCb.jumpToBootPlease    = FALSE;
    jumpToBootOrResetCb.resetPlease         = FALSE;
    jumpToBootOrResetCb.softwareResetPlease = FALSE;

}


/**
 * Apparent
 * AMU generic counters.
 */
static void initAmuCounters(void){
    U32 iggy;
    for (iggy = 0; iggy < MAX_BAD_EVENTS_COUNTERS; iggy++) {
        amuBadEventsCountersBlock[iggy] = 0;
    }
}


// displayInvalidParameters
//
// Display a json-like string with the invalid parameters indicator.
// Include the fail result, as well as an extra info if specified.
//
void displayInvalidParameters(char *pUserInput, char *pExtraInfo)
{
    printf("{%s:{%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s,%s:", CLI_STRING, COMMAND_STRING,  pUserInput, 
                                                                   CODE_STRING,     0,
                                                                   RESULT_STRING,   FAIL_STRING,
                                                                   RESPONSE_STRING, REASON_STRING, INVALID_PARAMETERS_STRING, INFO_STRING);
    if (pExtraInfo == NULL) { printf("%s}}}\r\n", NONE_STRING); }
    else                    { printf("%s}}}\r\n", pExtraInfo);  }

    return;
}


// displayAndResetBadEventsCounters//
//
// Display the counters from the "bad events" counter data block. Only include non-zero counts.
//
// {"CLI":{"COMMAND":"counters","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"AMU COUNTERS":[{"RANK":1,"COUNT":111},{"RANK":2,"COUNT":222}, ... ,{"RANK":10,"COUNT":333}]},"RESULT":"SUCCESS"}}
//
//

static void displayAndResetBadEventsCounters(void)
{
    U32 iggy;
    U32 firstTime = TRUE;

    // The preamble. Counters are returned in a list, so use a square bracket to open the list.
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:[", CLI_STRING, COMMAND_STRING,   COUNTERS_CMD, 
                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                    CODE_STRING,      0,
                                                                    RESULT_STRING,    SUCCESS_STRING,
                                                                    RESPONSE_STRING,  AMU_COUNTERS_STRING);

    // Now the counters data.
    for (iggy = 0; iggy < MAX_BAD_EVENTS_COUNTERS; iggy++) {
        if (amuBadEventsCountersBlock[iggy] != 0) { if (firstTime) { printf("{%s:%lu,%s:%lu}",  RANK_STRING, iggy, COUNT_STRING, amuBadEventsCountersBlock[iggy]); firstTime = FALSE; }
                                                    else           { printf(",{%s:%lu,%s:%lu}", RANK_STRING, iggy, COUNT_STRING, amuBadEventsCountersBlock[iggy]);                    }
                                                  }                                                  
    }
    // Terminate: - the list with ]
    //            - "RESPONSE" with }
    //            - close off the entire json-like string with }}
    printf("]}}}\r\n");
    initAmuCounters(); // Reset the counters.
    
}


// readUserSignatureFlash
//
// Read and display the user signature data from flash by byte.
// For test/debug, the output is json-formatted.
//
//
// {"CLI":{"COMMAND":"r_us_ram","ARGUMENTS":"NONE","CODE":0,
//         "RESPONSE":{"DATA 0":"D1 5F 76 98 6D 6F 64 65 6C 3D 50 43 42 41 2D 31",
//                     "DATA 1":"20 70 72 6F 64 75 63 74 3D 41 54 49 52 41 30 30",
//                     "DATA 2":"30 30 20 73 6E 3D 41 45 31 32 30 37 32 30 2D 30",
//                     .
//                     .
//                     .
//                     "DATA 31":"00 00 00 00 00 00 00 00 00 00 00 00 9C BF 3D 28"},
//         "RESULT":"SUCCESS"}}
//
static void readUserSignatureFlash(void)
{
    U16   iggy;
    U16   pop;
    U16   flashResult;
    U32   pUserSignature[128];
    U8   *pSigData;

    flashResult = flash_read_user_signature(&pUserSignature[0], 128);
    if (flashResult == FLASH_RC_OK) {
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{", CLI_STRING, COMMAND_STRING, USER_SIGNATURE_READ_FLASH_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESPONSE_STRING);
        // Display 16 bytes per line. There are 32 lines, add a trailing comma for all except the last line (controlled by an ugly if statement).
        pSigData = (U8 *)&pUserSignature[0];
        for (iggy = 0, pop=0; iggy < 512; iggy=iggy+16, pop++) {
            if (pop < 31) {
                printf("\"DATA %d\":\"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\",",  // <- trailing comma
                       pop,
                       pSigData[iggy+0], pSigData[iggy+1], pSigData[iggy+2],   pSigData[iggy+3],   pSigData[iggy+4],   pSigData[iggy+5],   pSigData[iggy+6],   pSigData[iggy+7],
                       pSigData[iggy+8], pSigData[iggy+9], pSigData[iggy+10],  pSigData[iggy+11],  pSigData[iggy+12],  pSigData[iggy+13],  pSigData[iggy+14],  pSigData[iggy+15] );
            } else {
                printf("\"DATA %d\":\"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\"",   // <- no trailing comma
                       pop,
                       pSigData[iggy+0], pSigData[iggy+1], pSigData[iggy+2],   pSigData[iggy+3],   pSigData[iggy+4],   pSigData[iggy+5],   pSigData[iggy+6],   pSigData[iggy+7],
                       pSigData[iggy+8], pSigData[iggy+9], pSigData[iggy+10],  pSigData[iggy+11],  pSigData[iggy+12],  pSigData[iggy+13],  pSigData[iggy+14],  pSigData[iggy+15] );
            }
        } // for ...
        printf("},%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
    } else {
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:\"CODE 0x%X\"},%s:%s", CLI_STRING,
                                                                         COMMAND_STRING,   USER_SIGNATURE_READ_FLASH_CMD,
                                                                         ARGUMENTS_STRING, NONE_STRING,
                                                                         CODE_STRING,      0,
                                                                         RESPONSE_STRING,  FLASH_READ_FAIL_STRING, flashResult,
                                                                         RESULT_STRING,    FAIL_STRING);
    }
}


// readUserSignatureRam
//
// Read the raw user signature data from flash by byte.
// For test/debug, the output is json-formatted.
//
// {"CLI":{"COMMAND":"r_us_ram","ARGUMENTS":"NONE","CODE":0,
//         "RESPONSE":{"DATA 0":"D1 5F 76 98 6D 6F 64 65 6C 3D 50 43 42 41 2D 31",
//                     "DATA 1":"20 70 72 6F 64 75 63 74 3D 41 54 49 52 41 30 30",
//                     "DATA 2":"30 30 20 73 6E 3D 41 45 31 32 30 37 32 30 2D 30",
//                     .
//                     .
//                     .
//                     "DATA 31":"00 00 00 00 00 00 00 00 00 00 00 00 9C BF 3D 28"},
//         "RESULT":"SUCCESS"}}
//
static void readUserSignatureRam(void)
{
    U16   iggy;
    U16   pop;
    U8   *pSigData;

    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{", CLI_STRING, COMMAND_STRING, USER_SIGNATURE_READ_FLASH_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESPONSE_STRING);
    // Display 16 bytes per line. There are 32 lines, add a trailing comma for all except the last line (controlled by an ugly if statement).
    pSigData = (U8 *)&pApparentUserSignature[0];
    for (iggy = 0, pop=0; iggy < 512; iggy=iggy+16, pop++) {
        if (pop < 31) {
            printf("\"DATA %d\":\"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\",",  // <- trailing comma
            pop,
            pSigData[iggy+0], pSigData[iggy+1], pSigData[iggy+2],   pSigData[iggy+3],   pSigData[iggy+4],   pSigData[iggy+5],   pSigData[iggy+6],   pSigData[iggy+7],
            pSigData[iggy+8], pSigData[iggy+9], pSigData[iggy+10],  pSigData[iggy+11],  pSigData[iggy+12],  pSigData[iggy+13],  pSigData[iggy+14],  pSigData[iggy+15] );
        } else {
            printf("\"DATA %d\":\"%02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\"",   // <- no trailing comma
                   pop,
                   pSigData[iggy+0], pSigData[iggy+1], pSigData[iggy+2],   pSigData[iggy+3],   pSigData[iggy+4],   pSigData[iggy+5],   pSigData[iggy+6],   pSigData[iggy+7],
                   pSigData[iggy+8], pSigData[iggy+9], pSigData[iggy+10],  pSigData[iggy+11],  pSigData[iggy+12],  pSigData[iggy+13],  pSigData[iggy+14],  pSigData[iggy+15] );
        }
    } // for ...
    printf("},%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
}


// updateUserSignatureFromFlash
//
// Read the user signature flash memory into the global RAM memory.
// Called during system startup only.
//
static void updateUserSignatureFromFlash(void)
{
    U16  flashResult;
    t_apparentUserSignature *pAppUserSig;

    memset(&pApparentUserSignature[0], 0xFF, FLASH_USER_SIG_SIZE_IN_BYTES);
    flashResult = flash_read_user_signature((uint32_t *)&pApparentUserSignature[0], FLASH_USER_SIG_SIZE_IN_LONGS);
    if (flashResult == FLASH_RC_OK) {
        pAppUserSig = (t_apparentUserSignature *)(&pApparentUserSignature[0]);
        if (pAppUserSig->signatureValidation == APPARENT_USER_SIG_VALIDATED) {
            // Validate what was read out over those bytes.
            userSignatureCalculatedCrc = calcCrc32Checksum(CRC_32BIT_SEED, (U8 *)&pApparentUserSignature[0], USER_SIGNATURE_CRC_SIZE); // Calculate over 512 - 4 = 508 bytes
            userSignatureExtractedCrc  = pAppUserSig->calculatedCrc;                                                                   // CRC is in the last 32-bit LONG

            if (userSignatureCalculatedCrc == userSignatureExtractedCrc) {
                // The User Signature is valid; data will be extracted and displayed in the banner. Nothing more need be said here.
            } else {
                memset(&pApparentUserSignature[0], 0xFF, FLASH_USER_SIG_SIZE_IN_BYTES);
            }
        } else {
            // User signature has not been written. The banner that will eventually be displayed
            // will indicate "UNKNOWN" for the components that are normally stored in this block.
        }
    } else {
        memset(&pApparentUserSignature[0], 0xFF, FLASH_USER_SIG_SIZE_IN_BYTES);
        incrementBadEventCounter(FAILED_FLASH_USER_SIGNATURE_READ);
    }
}


// extractDataFromUserSignatureInRam
//
// Extract data from the UserSignature in RAM. The USER SIGNAURE from flash would have
// been written to a RAM zone, so extract data from that RAM. The following data is
// extracted: "model", "product", "sn".
//
static void extractDataFromUserSignatureInRam(void)
{
    t_apparentUserSignature *pApparentSignature;
    char *pString;

    pApparentSignature = (t_apparentUserSignature *)(&pApparentUserSignature[0]);
    if (pApparentSignature->signatureValidation == APPARENT_USER_SIG_VALIDATED) {
        pString = (char *)(&pApparentSignature->apparentData[0]);
        // Parse the user signature and locate "sn", "model", "product".
        memset(pAmuProduct,      0, AMU_COMPONENT_DATA_SIZE_MAX);
        memset(pAmuSerialNumber, 0, AMU_COMPONENT_DATA_SIZE_MAX);
        memset(pAmuModel,        0, AMU_COMPONENT_DATA_SIZE_MAX);

        findComponentStringData(CLI_ARG_PRODUCT_STRING,       pAmuProduct,      pString);
        findComponentStringData(CLI_ARG_SERIAL_NUMBER_STRING, pAmuSerialNumber, pString);
        findComponentStringData(CLI_ARG_MODEL_STRING,         pAmuModel,        pString);
         if ((strlen(pAmuProduct) == 0) || (strlen(pAmuSerialNumber) == 0) || (strlen(pAmuModel) == 0)) {
             // Something awry with the data in the User Signature.
             strcpy(pAmuProduct,      UNKNOWN_STRING);
             strcpy(pAmuSerialNumber, UNKNOWN_STRING);
             strcpy(pAmuModel,        UNKNOWN_STRING);
         } else {
             // No news is good news. The data will be displayed in the banner.
         }
    }
}


// userSignatureWrite
//
// Update the user signature in flash. That area of memory must be erased. The CLI is of the form:
//
// us_write sn=AE123456-0000-000 product=ATIRA0000 model=PCBA-1



//    printf("%25s - write user signature: %s %s=%s %s=%s %s=%s\r\n",                                /*  33 */ USER_SIGNATURE_WRITE_CMD, USER_SIGNATURE_WRITE_CMD, CLI_ARG_SERIAL_NUMBER_STRING, AMU_SERIAL_NUMBER_DEFAULT,
//    CLI_ARG_PRODUCT_STRING,       AMU_ATIRA_PRODUCT_DEFAULT,
//    CLI_ARG_MODEL_STRING,         AMU_MODEL_DEFAULT);




//
// Both the serial number and product are free-format, unverified. Write once, cannot be re-written unless erased.
// The standard firmware release does not allow for the USER SIGNTURE to be erased - a specially compiled temporary
// release must be used in order to do so.
//
// The output if json-formatted, and for a successful update is of the form:
// 
// {"CLI":{"COMMAND":"us_write","ARGUMENTS":"sn=AE120720-0004-008 product=ATIRA0000 model=PCBA-1", "CODE":0,
//         "RESPONSE":{"UPDATE USER SIGNATURE":"USER SIGNATURE UPDATED",
//                     "CALCULATED CRC":"0x69FABC37",
//                     "NUMBER OF BYTES":508},
//         "RESULT":"SUCCESS"}}
//
// Currently, only the serial number and product are stored in the User Signature.
static void userSignatureWrite(char *pUserInput)
{
    char  *pArguments;
    U32    flashResult;
    U32    endOfInputRank;

    char pSnArgument[AMU_COMPONENT_DATA_SIZE_MAX];
    char pProductArgument[AMU_COMPONENT_DATA_SIZE_MAX];
    char pModelArgument[AMU_COMPONENT_DATA_SIZE_MAX];

    t_apparentUserSignature *pSignature;

    pSignature = (t_apparentUserSignature *)(&pApparentUserSignature[0]);
    pArguments = &pUserInput[strlen(USER_SIGNATURE_WRITE_CMD) + 1];

    if (pSignature->signatureValidation == APPARENT_USER_SIG_VALIDATED) {
        // Must first erase the user signature.
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   USER_SIGNATURE_WRITE_CMD,
			                                                                        ARGUMENTS_STRING, pArguments,
																					CODE_STRING,      0,
																					RESPONSE_STRING,  UPDATE_USER_SIGNATURE_STRING, USER_SIGNATURE_MUST_BE_ERASED_STRING,
																					RESULT_STRING,    FAIL_STRING);
    } else {
        // Set the pointer to the start of the arguments, and extract data.
        memset(pSnArgument,      0, AMU_COMPONENT_DATA_SIZE_MAX);
        memset(pProductArgument, 0, AMU_COMPONENT_DATA_SIZE_MAX);
        memset(pModelArgument,   0, AMU_COMPONENT_DATA_SIZE_MAX);
        findComponentStringData(CLI_ARG_SERIAL_NUMBER_STRING, pSnArgument,      pArguments);
        findComponentStringData(CLI_ARG_PRODUCT_STRING,       pProductArgument, pArguments);
        findComponentStringData(CLI_ARG_MODEL_STRING,         pModelArgument,   pArguments);

        if ((strlen(pSnArgument) == 0) || (strlen(pProductArgument) == 0) || (strlen(pModelArgument) == 0)) {
            // All 3 must have been specified.
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   USER_SIGNATURE_WRITE_CMD,
                                                                                        ARGUMENTS_STRING, pArguments,
                                                                                        CODE_STRING,      1,
                                                                                        RESPONSE_STRING,  UPDATE_USER_SIGNATURE_STRING, INVALID_PARAMETERS_STRING,
                                                                                        RESULT_STRING,    FAIL_STRING);
        } else {
            // Update the local values, and update the user signature in both RAM and FLASH.
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,", CLI_STRING, COMMAND_STRING,   USER_SIGNATURE_WRITE_CMD,
                                                                  ARGUMENTS_STRING, pArguments,
                                                                  CODE_STRING,      0);

            memset(&pApparentUserSignature[0], 0xFF, FLASH_USER_SIG_SIZE_IN_BYTES);

            // Write the data to the RAM zone. Even though it's only 1 component.
            // Note: when preparing the nextWriteRank and the pointer to the next memory location
            //       for the next TLV, care must be taken to respect memory alignment (i.e.,
            //       don't write at an odd address).

            // 1: Write the apparent signature validation to indicate the User Sig has been updated.
            pSignature->signatureValidation = APPARENT_USER_SIG_VALIDATED;

            // 2: write the string as it was input by the user into the RAM zone. Since this user input
            //    contains the END-OF-INPUT termination component, terminate the argument string with a
            //    NULL CHAR.
            endOfInputRank   = findEndOfInputRank(pUserInput);
            pUserInput[endOfInputRank] = 0x00; 
            strcpy(pSignature->apparentData, pArguments);

            // 3: 32-bit CRC checksum. Calculated over the 508 bytes of the RAM block
            // (got to leave 4 bytes at the very end to write the CRC itself).
            pSignature->calculatedCrc = calcCrc32Checksum(CRC_32BIT_SEED, (U8 *)&pApparentUserSignature[0], USER_SIGNATURE_CRC_SIZE);

            // 4: 2 part process: erase flash, then write.
            flashResult = flash_erase_user_signature();
            if (flashResult == 0) {
                // Now the write. If good, update the local components pointers into the user signature. 
                flashResult = flash_write_user_signature(&pApparentUserSignature[0], FLASH_USER_SIG_SIZE_IN_LONGS);
                if (flashResult == FLASH_RC_OK) {
                    printf("%s:{%s:%s,%s:\"0x%08lX\",%s:%u},%s:%s}}\r\n", RESPONSE_STRING, UPDATE_USER_SIGNATURE_STRING, USER_SIGNATURE_UPDATED_STRING,
                                                                                           CALCULATED_CRC_STRING, pSignature->calculatedCrc,
                                                                                           NUMBER_OF_BYTES_STRING, USER_SIGNATURE_CRC_SIZE,
                                                                                           RESULT_STRING, SUCCESS_STRING);
                    extractDataFromUserSignatureInRam();
                } else {
                    printf("%s:{%s:%s},%s:%s}}\r\n", RESPONSE_STRING, UPDATE_USER_SIGNATURE_STRING, USER_SIGNATURE_WRITE_FAILED_STRING, RESULT_STRING, FAIL_STRING);
                    incrementBadEventCounter(FAILED_FLASH_USER_SIGNATURE_WRITE);
                    }
            } else {
                printf("%s},%s:%s}}\r\n", USER_SIGNATURE_ERASE_FAILED_STRING, RESULT_STRING, FAIL_STRING);
                incrementBadEventCounter(FAILED_FLASH_USER_SIGNATURE_ERASE);
            }
        }
    }
}


// userSignatureErase
//
//
// Output is json-formatted:
//
// {"CLI":{"COMMAND":"us_erase","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"ERASE USER SIGNATURE":"USER SIGNATURE ERASED - RESET REQUIRED"},"RESULT":"SUCCESS"}}
// {"CLI":{"COMMAND":"us_erase","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"ERASE USER SIGNATURE":"USER SIGNATURE ERASE *FAILED*"},"RESULT":"FAIL"}}
// {"CLI":{"COMMAND":"us_erase","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"ERASE USER SIGNATURE":"USER SIGNATURE ERASE *DISABLED*"},"RESULT":"FAIL"}}
//
static void userSignatureErase(void)
{
    // The preamble output:
	printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:", CLI_STRING, COMMAND_STRING,   USER_SIGNATURE_ERASE_CMD,
		                                                     ARGUMENTS_STRING, NONE_STRING,
                                                             CODE_STRING,      0,
                                                             RESPONSE_STRING,  ERASE_USER_SIGNATURE_STRING);

#if USER_SIGNATURE_REFLASH == TRUE
    memset((U8 *)(&pApparentUserSignature[0]), 0xFF, FLASH_USER_SIG_SIZE_IN_BYTES);
    strcpy(pAmuProduct,      UNKNOWN_STRING);
    strcpy(pAmuSerialNumber, UNKNOWN_STRING);
    strcpy(pAmuModel,        UNKNOWN_STRING);
    if (flash_erase_user_signature() == 0) { printf("%s},%s:%s}}\r\n", USER_SIGNATURE_ERASED_STRING,       RESULT_STRING, SUCCESS_STRING); }
    else                                   { printf("%s},%s:%s}}\r\n", USER_SIGNATURE_ERASE_FAILED_STRING, RESULT_STRING, FAIL_STRING);    }
#else
    printf("%s},%s:%s}}\r\n", USER_SIGNATURE_ERASE_DISABLED_STRING, RESULT_STRING, FAIL_STRING);
#endif
}


// ethernetControllerReset
//
//
//
static void ethernetControllerReset (char *pUserInput)
{
    char *pArguments;
    char  pController[AMU_COMPONENT_DATA_SIZE_MAX];
    
    // Set the pointer to the start of the arguments, and extract the action.
    pArguments = &pUserInput[strlen(ETHERNET_CONTROLLER_RESET_CMD) + 1];
    if (pArguments != NULL) {
        memset(pController, 0, AMU_COMPONENT_DATA_SIZE_MAX);
        findComponentStringData(CLI_ARG_CONTROLLER_STRING, pController, pArguments);

        if (strlen(pController) > 0) {
            if (strncmp(pController, CLI_ARG_PRIMARY_STRING, strlen(CLI_ARG_PRIMARY_STRING)) == 0) {
                primaryControllerReset(); // PRIMARY ONLY
            } else if (strncmp(pController, CLI_ARG_SECONDARY_STRING, strlen(CLI_ARG_SECONDARY_STRING)) == 0) {
                secondaryControllerReset(); // SECONDARY ONLY
            } else if (strncmp(pController, CLI_ARG_BOTH_STRING, strlen(CLI_ARG_BOTH_STRING)) == 0) {
                // BOTH PRIMARY AND SECONDARY:
                primaryControllerReset();
                secondaryControllerReset();
            } else { displayInvalidParameters(pUserInput, NULL); }
        } else { displayInvalidParameters(pUserInput, NULL); }
    } else { displayInvalidParameters(pUserInput, NULL); }
}


// readBothControllersMac
//
// {"CLI":{"COMMAND":"r_both_ksz_mac","ARGUMENTS":"NONE","CODE":0,"RESULT":"SUCCESS","RESPONSE":{"PRIMARY MAC":"00:10:A1:FF:FF:FF","SECONDARY MAC":"00:10:A1:FF:FF:FF"}}}
//
static void readBothControllersMac(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{", CLI_STRING, COMMAND_STRING, READ_BOTH_KSZ_MAC_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESULT_STRING, SUCCESS_STRING, RESPONSE_STRING);
    readPrimaryMac();
    printf(",");
    readSecondaryMac(); 
    printf("}}}\r\n");
}


//  configureStaticLanMac
//
// Configures the given MAC (presumably the Gateway's LAN MAC) as static in the ALU tables:
//
// - on the primary controller   as static against the SBC/LAN port
// - on the secondary controller as static against the RGMII port
//
// The json-friendly response if of the form:
//
// {"CLI":{"COMMAND":"config_static_lan_mac","ARGUMENTS":"mac=00:00:00:00:00:00","CODE":0,"RESULT":"SUCCESS",
//         "RESPONSE":{"INFO":{"STATIC LAN MAC CONFIG":"MAC":"AC:1F:6B:45:D5:31"}}}}
// 
static void configureStaticLanMac(char *pUserInput)
{
    char  *pArguments;
    char   pMacAddressString[32];
    U8     pMac[6];
    BOOL   configResult;

    pArguments = &pUserInput[strlen(CONFIG_STATIC_LAN_MAC_CMD) + 1];
    memset(pMacAddressString,  0, 32);
    findComponentStringData(CLI_ARG_MAC_STRING,  pMacAddressString,  pArguments);

    if (strlen(pMacAddressString) == 0) {
        printf("{%s:{%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, ALU_MAC_RESO_PARAM_CMD, pUserInput,
                           CODE_STRING,            0,
                           RESULT_STRING,          FAIL_STRING,
                           RESPONSE_STRING,        INFO_STRING, INVALID_ARGUMENTS_STRING);
    } else {
        // A MAC string must be exactly these many characters, including the colons.
        if (strlen(pMacAddressString) == MAX_CHARS_IN_MAC_STRING) {
            if (isConvertMacString(pMacAddressString, pMac)) {

                configResult = isConfigPrimaryStaticAluMac(&pMac[0]);
                if (configResult == TRUE) {

                    configResult = isConfigSecondaryStaticAluMac(&pMac[0]);

                    if (configResult == TRUE) {

                        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:{%s:\"%02X:%02X:%02X:%02X:%02X:%02X:\"}}}}\r\n",
                               CLI_STRING, COMMAND_STRING,            CONFIG_STATIC_LAN_MAC_CMD,
                                           ARGUMENTS_STRING,          pArguments,
                                           CODE_STRING,               0,
                                           RESULT_STRING,             SUCCESS_STRING,
                                           RESPONSE_STRING,           INFO_STRING, STATIC_LAN_MAC_CONFIG_STRING,
                                                                                   pMac[0], pMac[1], pMac[2], pMac[3], pMac[4], pMac[5]);

                    } else {
                        // Configuration failed with the secondary controller.
                        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                               CLI_STRING, COMMAND_STRING,            CONFIG_STATIC_LAN_MAC_CMD,
                                           ARGUMENTS_STRING,          pArguments,
                                           CODE_STRING,               0,
                                           RESULT_STRING,             FAIL_STRING,
                                           RESPONSE_STRING,           INFO_STRING, STATIC_LAN_MAC_CONFIG_SECONDARY_FAILED_STRING);
                    }                    

                } else {
                    // Configuration failed with the primary controller.
                    printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                           CLI_STRING, COMMAND_STRING,            CONFIG_STATIC_LAN_MAC_CMD,
                                       ARGUMENTS_STRING,          pArguments,
                                       CODE_STRING,               0,
                                       RESULT_STRING,             FAIL_STRING,
                                       RESPONSE_STRING,           INFO_STRING, STATIC_LAN_MAC_CONFIG_PRIMARY_FAILED_STRING);
                }

            } else {
                // This failure and the one below make no distinction for the actual cause.
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                       CLI_STRING, COMMAND_STRING,            CONFIG_STATIC_LAN_MAC_CMD,
                                   ARGUMENTS_STRING,          pArguments,
                                   CODE_STRING,               0,
                                   RESULT_STRING,             FAIL_STRING,
                                   RESPONSE_STRING,           INFO_STRING, INVALID_MAC_STRING);
            }
        } else {
            // This failure and the one above make no distinction for the actual cause.
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                   CLI_STRING, COMMAND_STRING,            CONFIG_STATIC_LAN_MAC_CMD,
                               ARGUMENTS_STRING,          pArguments,
                               CODE_STRING,               0,
                               RESULT_STRING,             FAIL_STRING,
                               RESPONSE_STRING,           INFO_STRING, INVALID_MAC_STRING);
        }
    }
}


// userGetStaticLanMacInfo
//
// The read is hard-coded to retrieve entry[0] from both controllers.
//
// If the read from both controller is successful, return :
//
// {"CLI":{"COMMAND":"get_static_lan_mac_info","ARGUMENTS":"NONE","CODE":0,"RESULT":"SUCCESS",
//         "RESPONSE":{"STATIC_LAN_MAC_INFO":{"PRIMARY":{"PORT FORWARD":"0x10","PORT":"LAN","MAC":"AC:1F:6B:45:D5:31"},
//                                            "SECONDARY":{"PORT FORWARD":"0x10","PORT":"RGMII","MAC":"AC:1F:6B:45:D5:31"}}}}}
//
//
static void userGetStaticLanMacInfo(void)
{
    // The preamble:
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{", CLI_STRING, COMMAND_STRING,   GET_STATIC_LAN_MAC_INFO_CMD,
                                                                ARGUMENTS_STRING, NONE_STRING,
                                                                CODE_STRING,      0,
                                                                RESULT_STRING,    SUCCESS_STRING,
                                                                RESPONSE_STRING);
    // The LAN MAC info:
    displayStaticLanMacInfo();

    // The end. Close off the string to make it json friendly.
    printf("}}}\r\n");

    return;
}


// displayStaticLanMacInfo
//
// Display the LAN MAC info only. This function is used by hello response processing,
// as well as by the get-lan-mac-info command. Output is of the form:
//
// "STATIC LAN MAC INFO":{"PRIMARY":{"PORT FORWARD":"0x10","PORT":"LAN","MAC":"AC:1F:6B:45:D5:31"},
//                        "SECONDARY":{"PORT FORWARD":"0x10","PORT":"RGMII","MAC":"AC:1F:6B:45:D5:31"}
//                       }
//
static void displayStaticLanMacInfo(void)
{
    BOOL  readResult;
    U8    primaryPortForward = 0xFF;
    U8    pPrimaryMac[6];
    U8    secondaryPortForward = 0xFF;
    U8    pSecondaryMac[6];
    char *pPrimaryPort;
    char *pSecondaryPortString;

    // Read from the primary controller.
    readResult = isReadPrimaryStaticInfo(0, &primaryPortForward, &pPrimaryMac[0]);

    if (readResult == TRUE) {
    
        // Read from the secondary controller.
        readResult = isReadSecondaryStaticInfo(0, &secondaryPortForward, &pSecondaryMac[0]);

        if (readResult == TRUE) {

            // Both reads succeeded. Display the primary controller data.
            pPrimaryPort = getPrimaryPortStringFromPortForward(primaryPortForward);
            printf("%s:{%s:{%s:\"0x%02X\",%s:\"%s\",%s:\"%02X:%02X:%02X:%02X:%02X:%02X\"},",
                   STATIC_LAN_MAC_INFO_STRING, PRIMARY_STRING, PORT_FORWARD_STRING, primaryPortForward,
                                                               PORT_STRING, pPrimaryPort,
                                                               MAC_STRING,  pPrimaryMac[0],   pPrimaryMac[1],   pPrimaryMac[2],
                                                                            pPrimaryMac[3],   pPrimaryMac[4],   pPrimaryMac[5]);

            // Display the secondary controller data. The string is terminated with a second } which
            // closes off the starting { from the print just above.
            pSecondaryPortString = getSecondaryPortStringFromStatic(secondaryPortForward);
            printf("%s:{%s:\"0x%02X\",%s:\"%s\",%s:\"%02X:%02X:%02X:%02X:%02X:%02X\"}}",
                   SECONDARY_STRING, PORT_FORWARD_STRING, secondaryPortForward,
                                     PORT_STRING,         pSecondaryPortString,
                                     MAC_STRING,          pSecondaryMac[0], pSecondaryMac[1], pSecondaryMac[2],
                                                          pSecondaryMac[3], pSecondaryMac[4], pSecondaryMac[5]);

        } else { printf("%s:{%s:%s}", STATIC_LAN_MAC_INFO_STRING, INFO_STRING, READ_FROM_SECONDARY_FAILED_STRING); }

    } else { printf("%s:{%s:%s}", STATIC_LAN_MAC_INFO_STRING, INFO_STRING, READ_FROM_PRIMARY_FAILED_STRING); }
}


// isCcNumberInList
//
//
//
static BOOL isCcNumberInList(U32 ccNumber, U32 *pCcNumberList)
{
    BOOL ccInList = FALSE;
    U32 iggy;
    for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) {
        if (ccNumber == pCcNumberList[iggy]) { ccInList = TRUE; break; }
    }
    return ccInList;
}


// Parse a string containing a comma-separated list of contact closure numbers, and extract the
// list of those numbers. The string specification normally something like:
//
// 1,2,3,4
//
// Rules: - no duplicates
//        - must be valid contact closure numbers 1..4
//        - cannot have more than 4 total
// Contact closure numbers can be in any order.
// If any rules are violated, the list is wiped.
//
static void  getCcNumberListFromCcNumberArg (char *pUserInput, char *pCcNumberArgument, U32 *pCcNumberList)
{
    U32  iggy;
    BOOL keepSearching = TRUE;
    char pCcNumberString[2];
    U16  charRank = 0;
    char thisChar[2];
    U32  portStringLength = strlen(pCcNumberArgument);
    U32  ccNumber;
    U16  ccIndex = 0;
    BOOL badUserInput = FALSE;

    for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) { pCcNumberList[iggy] = 0; }
    memset(&pCcNumberString[0], 0, 2);
    thisChar[1] = 0;

    while (keepSearching) {
        if (charRank == portStringLength) {
            // Found the last one. End of the line. I know: bad pun.
            if (strlen(pCcNumberString) == 1) {
                ccNumber = atoi(pCcNumberString);
                if ((ccNumber >= 1) && (ccNumber <= 4)) {
                    if (isCcNumberInList(ccNumber, pCcNumberList) == FALSE) { pCcNumberList[ccIndex++] = ccNumber; }
                    else                                    /* DUPLICATE */ { badUserInput = TRUE;                 }
                } else {
                    // Invalid port.
                    badUserInput = TRUE;
                }
            } else {
                // String was incorrectly terminated.
                badUserInput = TRUE;
            }
        } else {
            thisChar[0] = pCcNumberArgument[charRank++];
            // Check this 1 char - is it a comma?
            if (thisChar[0] == ',') {
                // Found the comma, so evaluate the "port as string" collected up to now.
                ccNumber = atoi(pCcNumberString);
                if ((ccNumber >= 1) && (ccNumber <= 4)) {                                                          /*| <---  for next time  ---> |*/
                    if (isCcNumberInList(ccNumber, pCcNumberList) == FALSE) { pCcNumberList[ccIndex++] = ccNumber; memset(&pCcNumberString[0], 0, 2); }
                    else                                    /* DUPLICATE */ { badUserInput = TRUE;                                                    }
                } else {
                    // Invalid port.
                    badUserInput = TRUE;
                }
            } else {
                // Add it if not at the limit.
                if (strlen(pCcNumberString) == 0) { pCcNumberString[0] = thisChar[0]; }
                else          /* port too long */ { badUserInput = TRUE;              }
            }
        }
        if (badUserInput) { break; }
    } // while ...

    if (badUserInput == TRUE) { displayInvalidParameters(pUserInput, NULL);
                                memset(&pCcNumberList[0], 0xFFFFFFFF, NUM_OUTPUT_CONTACT_CLOSURES); } // I told you this would happen.
}


static void contactClosureOutputControl (char *pUserInput, U32 action)
{
    U32    iggy;
    char  *pArguments;
    char  *pActionString;
    char   pCcString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    pCcNumberList[NUM_OUTPUT_CONTACT_CLOSURES];
    U32    ccRank     = 0;                         // avoid pesky compile warning
    U32    numberOfCc = 0;                         //   "     "      "       "
    BOOL   goodInput  = TRUE;                      //   "     "      "       "

    // The processor for the CLI to OPEN or CLOSE an output contact closure.
    // The command takes on the form: cc_output_open cc=<1..4>
    // or:                            cc_output_close cc=<1..4>
    // Give the user the usual silent treatment upon bad input.

    memset(pCcString, 0, AMU_COMPONENT_DATA_SIZE_MAX);

    // Point to the argument section that follows the command, and extract the list of output contact closure numbers.
    if   (action == OUTPUT_CC_OPEN)  { pArguments = &pUserInput[strlen(CONTACT_CLOSURE_OUTPUT_OPEN_CMD)  + 1]; pActionString = (char *)&CONTACT_CLOSURE_OUTPUT_OPEN_CMD;  }
    else                             { pArguments = &pUserInput[strlen(CONTACT_CLOSURE_OUTPUT_CLOSE_CMD) + 1]; pActionString = (char *)&CONTACT_CLOSURE_OUTPUT_CLOSE_CMD; }
    findComponentStringData(CLI_ARG_CC_STRING,     pCcString,     pArguments);

    if (strlen(pCcString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
        goodInput = FALSE;
    } else {
        // Extract the list of contact closure numbers from the presumed comma-separated list.
        // If there is only 1 contact, there is no comma. For 2 or more contact numbers, they
        // are comma-separated and can be in any order.
        getCcNumberListFromCcNumberArg(pUserInput, pCcString, pCcNumberList);

        // for loop maybe?
        if (pCcNumberList[0] == 0xFFFFFFFF) {
            // If the list is bad, then the [0] element contains the indicator.
            displayInvalidParameters(pUserInput, NULL);
            goodInput = FALSE;
        } else {
            // Valid list. How many contact closures were specified?
            // Required below in order to correctly terminate the json-like list.
            for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) {
                if (pCcNumberList[iggy] != 0) { numberOfCc++; }
            }
        }
    }

    if (goodInput) {
        // For the usual giggles, output a string as a handsome json string, something like:
        // {"CLI":{"COMMAND":"cc_output","ARGUMENTS":"action=open cc=1,2","CODE":0,"RESULT":[{"CC #1 (GPIO 64)":"NOW CLOSED"},{"CC #2 (GPIO 65)":"WAS ALREADY CLOSED"}]}}
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:[", CLI_STRING, COMMAND_STRING, pActionString, ARGUMENTS_STRING, pArguments, CODE_STRING, 0, RESULT_STRING);
        for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) {
            if (pCcNumberList[iggy] != 0) {
                ccRank = pCcNumberList[iggy] - 1;
                if (gOutputCcConfig[ccRank].config == action) {
                    if (action == OUTPUT_CC_OPEN) { printf("{\"CC %ld\":%s} ", pCcNumberList[iggy], WAS_ALREADY_OPEN_STRING);  }
                    else                          { printf("{\"CC %ld\":%s} ", pCcNumberList[iggy], WAS_ALREADY_CLOSE_STRING); }
                }
                else {
                    gOutputCcConfig[ccRank].config = action;
                    if (action == OUTPUT_CC_OPEN) { ioport_set_pin_level(gOutputCcConfig[ccRank].gpio, IOPORT_PIN_LEVEL_LOW);
                                                    printf("{\"CC #%ld (GPIO %ld)\":%s} ",  pCcNumberList[iggy], gOutputCcConfig[ccRank].gpio, NOW_OPEN_STRING);
                                                  }
                    else                          { ioport_set_pin_level(gOutputCcConfig[ccRank].gpio, IOPORT_PIN_LEVEL_HIGH);
                                                    printf("{\"CC #%ld (GPIO %ld)\":%s} ",  pCcNumberList[iggy], gOutputCcConfig[ccRank].gpio, NOW_CLOSE_STRING);
                                                  }
                }
                // is a comma required?
                if ((iggy+1) == numberOfCc) {              } // last item, no comma, nothing to do
                else                        { printf(","); } // trailing comma for the next element
            } else { /* Nothing specified by the user */ }
        }
        printf("]}}\r\n"); // terminate the json. The leading ] closes off the RESULT list.
    }

    return;
}



// checkInputContactClosures
//
//
//
void checkInputContactClosures(void)
{
    checkThisInputContactClosure(nextInputCCRankToCheck);
    nextInputCCRankToCheck++;
    if (nextInputCCRankToCheck >= NUM_INPUT_CONTACT_CLOSURES) { nextInputCCRankToCheck = 0; }

    pokeWatchdog();

}


// checkThisInputContactClosure
//
// Detect a change in input contact closure status. If detected, write to the autonomous event block.
// Each input contact status change is considered to be a single autonomous event.
//
//
static void checkThisInputContactClosure(U32 ccNumber)
{
    U32  pinLevel;

    switch (gInputCcStatus[ccNumber].smState) {
            
        case INPUT_CC_CHECK_STATUS_SM_IDLE:
            if ((g_ul_ms_ticks - gInputCcStatus[ccNumber].lastTickCheck) >= CONTACT_CLOSURE_CHECK_FREQUENCY) {
                gInputCcStatus[ccNumber].smState = INPUT_CC_CHECK_STATUS_SM_CHECK;
            }                    
            break;

        case INPUT_CC_CHECK_STATUS_SM_CHECK:
            pinLevel = ioport_get_pin_level(gInputCcStatus[ccNumber].gpio);
            if (pinLevel == gInputCcStatus[ccNumber].status) {
                // No change. Go back to idle.
                gInputCcStatus[ccNumber].smState = INPUT_CC_CHECK_STATUS_SM_IDLE;
            } else {                    
                // Pin level changed. Go to the next state which will check for further changes for
                // the duration of the debounce period, to let things settle down. Don't change the
                // status just yet.
                gInputCcStatus[ccNumber].lastTickCheck = g_ul_ms_ticks;
                gInputCcStatus[ccNumber].smState       = INPUT_CC_CHECK_STATUS_SM_DEBOUNCE;

                if (craftDebugOutputEnabled == TRUE) {
                    sprintf(&pCraftPortDebugData[0], "I-CC %lu CHANGE DETECTED = %s\r\n", (ccNumber + 1), (pinLevel == 0 ? CC_OPEN_STRING : CC_CLOSE_STRING));
                    UART1_WRITE_PACKET_MACRO
                }

            }
            break;

        case INPUT_CC_CHECK_STATUS_SM_DEBOUNCE:
            if ((g_ul_ms_ticks - gInputCcStatus[ccNumber].lastTickCheck) >= CONTACT_CLOSURE_DEBOUNCE_TIMEOUT) {
                // It has remained either OPEN or CLOSE for the minimum debounce period.
                // Get the latest pin level in case it changed during this period.
                pinLevel = ioport_get_pin_level(gInputCcStatus[ccNumber].gpio);
                if (pinLevel == gInputCcStatus[ccNumber].status) {
                    // No change. Will be going back to idle.

                    if (craftDebugOutputEnabled == TRUE) {
                        sprintf(&pCraftPortDebugData[0], "I-CC %lu NO CHANGE AFTER DEBOUNCE = %s\r\n", (ccNumber + 1), (pinLevel == 0 ? CC_OPEN_STRING : CC_CLOSE_STRING));
                        UART1_WRITE_PACKET_MACRO
                    }

                } else {
                    // Build this JSON-like string component as follows:
                    //
                    // {"EVT":{"CC IN STATUS":{"CC_NUM":1,"STATUS":"OPEN"},"EI":2}}
                    // The contact closure number (1..4) is simply the for loop "index + 1".
                    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:{%s:\"%ld\",%s:%s}}}",
                                                        EVT_STRING,
                                                        EVENT_ID_STRING,     AEI_CC_INPUT_STATUS_CHANGE,
                                                        CC_IN_STATUS_STRING, CC_NUMBER_STRING, (ccNumber+1), CC_STATUS_STRING, (pinLevel == 0 ? CC_OPEN_STRING : CC_CLOSE_STRING));
                    addJsonToAutonomnousString(pSingleJsonEventString);
                    gInputCcStatus[ccNumber].status = pinLevel;

                    if (craftDebugOutputEnabled == TRUE) {
                        sprintf(&pCraftPortDebugData[0], "I-CC %lu CHANGED AFTER DEBOUNCE = %s\r\n", (ccNumber + 1), (pinLevel == 0 ? CC_OPEN_STRING : CC_CLOSE_STRING));
                        UART1_WRITE_PACKET_MACRO
                    }
                }
                gInputCcStatus[ccNumber].lastTickCheck = g_ul_ms_ticks;
                gInputCcStatus[ccNumber].smState       = INPUT_CC_CHECK_STATUS_SM_IDLE;
            }
            break;

        default:
            // Coding error. Increment a counter.
            sprintf(&pCraftPortDebugData[0], "INPUT CC ERROR: CC 0x%lX STATE=0x%lX\r\n", ccNumber, gInputCcStatus[ccNumber].smState); UART1_WRITE_PACKET_MACRO
            incrementBadEventCounter(CC_INPUT_STATUS_CHECK_CODING_ERROR);
            initInputCCStatusData();
            break;
    }

    return;
}


// displayContactClosureInfo
//
// Display contact closure info. Called as part of HELLO output, or called by userGetContactClosureInfo() if user wants only cc status.
//


static void displayContactClosureInfo(void)
{
    U32 iggy;

    // Can be called via cli, or as part of the keep-alive/HELLO output processing.

    // Report the output contact closure configurations for all 4 output contacts.
    // The output is a json-like thingy of the form:
    //  "CC_INFO":{"OUTPUT":[{"CC_NUM":"1","CONFIG":"CLOSE"},{"CC_NUM":"2","CONFIG":"OPEN"},{"CC_NUM":"3","CONFIG":"CLOSE"},{"CC_NUM":"4","CONFIG":"OPEN"}]}
    printf("%s:{%s:[", CC_INFO_STRING, CC_OUTPUT_STRING);
    for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) {
        printf("{%s:\"%ld\",%s:%s}", CC_NUMBER_STRING, iggy+1, CONFIG_STRING, gOutputCcConfig[iggy].config == OUTPUT_CC_OPEN ? CC_OPEN_STRING : CC_CLOSE_STRING);
        if ((iggy+1) == NUM_OUTPUT_CONTACT_CLOSURES) {              } // last item, no comma, nothing to do
        else                                         { printf(","); } // trailing comma for the next element

    }
    printf("],"); // Terminate the "output" portion. The "input" section follows, so include a trailing comma.

    // Report the input contact closure statuses for all 4 input contacts.
    // The output is similar to output contacts, of the form:
    // {"INPUT":[{"CC_NUM":"1","STATUS":"CLOSE"},{"CC_NUM":"2","STATUS":"OPEN"},{"CC_NUM":"3","STATUS":"CLOSE"},{"CC_NUM":"4","STATUS":"OPEN"}]}
    printf("%s:[", CC_INPUT_STRING);
    for (iggy = 0; iggy < NUM_INPUT_CONTACT_CLOSURES; iggy++) {
        printf("{%s:\"%ld\",%s:%s}", CC_NUMBER_STRING, iggy+1, STATUS_STRING, gInputCcStatus[iggy].status == INPUT_CC_OPEN ? CC_OPEN_STRING : CC_CLOSE_STRING);
        if ((iggy+1) == NUM_INPUT_CONTACT_CLOSURES) {              } // last item, no comma, nothing to do
        else                                        { printf(","); } // trailing comma for the next element

    }
    
    // Terminate the "input" portion; terminate the json-like thingy. No trailing comma here,
    // as this function is called as part of a larger json-building effort.
    printf("]}");

    return;
}


// helloResponse
//
// Check input argument:
//
// Could be just a plain "hello", or "hello terse":
//
// - if "terse": then return the terse-form of the hello response,
// -             else return the full-blown response
//
static void helloResponse(char *pUserInput)
{
    char  *pArguments;
    pArguments = &pUserInput[strlen(KEEP_ALIVE_HELLO_CMD) + 1];
    if (strncmp(pArguments, CLI_ARG_TERSE_STRING, strlen(CLI_ARG_TERSE_STRING)) == 0) { helloResponseTerseData(); }
    else                                                                              { helloResponseFullData();  }
}


// helloResponseFullData
//
// Reply is of the form:
//
// {"CLI":{"COMMAND":"hello",
//         "COMMAND ARG":"NONE",
//         "CODE":0,
//         "RESPONSE":{"PROGRAM":"APPLICATION",
//                     "RELEASE":"67",
//                     "PRODUCT" :"ATIRA00000",
//                     "SERIAL NUMBER:"AE120720-0004-008",
//                     "HW REVISION":"rev 1.0",
//                     "UPTIME (SECS)" :494527,
//                     "SAME70 RESET REASON":"SOFTWARE",
//                     "RESET INFO":"0x00010300 0x300"
//                     "FLASH SIGNATURES":{"APP":"0xE0B2B315","BOOT 1":"0xFFFFFFFF","BOOT 0":"0xFFFFFFFF"},

//                     "STATIC LAN MAC INFO":{"PRIMARY":{"PORT FORWARD":"0x10","PORT":"LAN","MAC":"AC:1F:6B:45:D5:31"},
//                                            "SECONDARY":{"PORT FORWARD":"0x10","PORT":"RGMII","MAC":"AC:1F:6B:45:D5:31"}},

//                     "PRIMARY GLOBAL REGISTERS":{"GLOBAL REGISTER 0x00":"0x00","GLOBAL REGISTER 0x01":"0x01",..."GLOBAL REGISTER 0x0F":"0x0F"},
//                     "PRIMARY CONTROL REGISTERS":{"PORT 1":{"CONTROL":"0x2100","CONFIG":"POWERED UP"},..."PORT LAN":{"CONTROL":"0x2100","CONFIG":"ENABLED"}},
//                     "SECONDARY GLOBAL REGISTERS":{"GLOBAL REGISTER 0x00":"0x00", "GLOBAL REGISTER 0x01":"0x01" ... , "GLOBAL REGISTER 0x0F":"0x0F"},
//                     "SECONDARY CONTROL REGISTERS":{"CONTROL_02":{"PORT 5":{"CONTROL":"0x22"},"PORT 6":{"CONTROL":"0x22"},"PORT 7":{"CONTROL":"0x22"},"PORT 8":{"CONTROL":"0x22"}},
//                                                    "CONTROL_09":{"PORT 5":{"CONTROL":"0x11"},"PORT 6":{"CONTROL":"0x11"},"PORT 7":{"CONTROL":"0x11"},"PORT 8":{"CONTROL":"0x11"}},
//                                                    "CONTROL_10":{"PORT 5":{"CONTROL":"0x06","CONFIG":"POWERED UP"},"PORT 6":{"CONTROL":"0x06","CONFIG":"POWERED DOWN"},
//                                                                  "PORT 7":{"CONTROL":"0x06","CONFIG":"POWERED UP"},"PORT 8":{"CONTROL":"0x06","CONFIG":"POWERED UP"}},
//                                                    "CONTROL_11":{"PORT 5":{"CONTROL":"0x33"},"PORT 6":{"CONTROL":"0x33"},"PORT 7":{"CONTROL":"0x33"},"PORT 8":{"CONTROL":"0x33"}}},
//                     "SECONDARY STATUS REGISTERS":{"PORT 5":{"STATUS":"0x66","LINK":"UP"},"PORT 6":{"STATUS":"0x66","LINK":"UP"},"PORT 7":{"STATUS":"0x77","LINK":"DOWN"},"PORT 8":{"STATUS":"0x77","LINK":"DOWN"}},
//                     "CC_INFO":{"OUTPUT":[{"CC_NUM":"1","CONFIG":"OPEN"},{"CC_NUM":"2","CONFIG":"OPEN"},{"CC_NUM":"3","CONFIG":"OPEN"},{"CC_NUM":"4","CONFIG":"OPEN"}],
//                                "INPUT":[{"CC_NUM":"1","STATUS":"OPEN"},{"CC_NUM":"2","STATUS":"OPEN"},{"CC_NUM":"3","STATUS":"OPEN"},{"CC_NUM":"4","STATUS":"OPEN"}]},
//                     "TEMP/FAN INFO":{"TEMPERATURE":"32.1"},"CURRENT FAN SPEED":"OFF","TEMPERATURE SPEED SETTINGS":{"LOW":35,"MAX":45}},
//                     "POE INFO":{"POE POWER":[{"1":"UP","2":"UP","3":"UP","4":"UP","5":"UP","6":"UP","7":"UP","8":"UP"}],"TEMPERATURE":"38C/100F"}}}}
//                     "POE INFO":{"POE TEMPERATURE":"32.1",
//                                 "CONTROLLER INPUT VOLTAGE":"55.1",
//                                 "PORT DATA":[{"PORT":"1","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"2","ENABLE":"UP","POWER":"1.02","PAC":"0.00","AUTO CLASS":"CLASS 3","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"3","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"4","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"5","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"6","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"7","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"},
//                                              {"PORT":"8","ENABLE":"UP","POWER":"0.00","PAC":"0.00","AUTO CLASS":"CLASS NOT POE","AC SUPPORT":"NOT SUPPORTED"}]
//                                 "EVENT REGISTERS":{"POWER":"0xFFFF","DETECTION":"0xFFFF","FAULT":"0xFFFF","START":"0xFFFF","SUPPLY/FAULT":"0xFF"}}}}
//
//
static void helloResponseFullData(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{", CLI_STRING, COMMAND_STRING, KEEP_ALIVE_HELLO_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESPONSE_STRING);

    // 1: Display: running application, release, product, serial number, model, hw revision, uptime. 
    printf("%s:%s,%s:\"%s\",%s:\"%s\",%s:\"%s\",%s:\"%s\",%s:\"%X.%X\",%s:%lu,", PROGRAM_STRING,           APPLICATION_STRING,
                                                                       RELEASE_STRING,           APPARENT_RELEASE_STRING,
                                                                       PRODUCT_STRING,           pAmuProduct,
                                                                       SERIAL_NUMBER_STRING,     pAmuSerialNumber,
                                                                       MODEL_STRING,             pAmuModel,
                                                                       HARDWARE_REVISION_STRING, pcbaHardwareRevision.major, pcbaHardwareRevision.minor,
                                                                       UPTIME_STRING,            g_ul_ms_ticks/1000);
     // 2: Reset reason.
     displayControllerResetReason(JSON_OUTPUT); printf(",");

     // 3: Read the current flash signatures.
     displayFlashSignatures(); printf(",");

    //  4: display the static LAN MAC info from both controllers.
    displayStaticLanMacInfo(); printf(",");

    //  5: The global register values 0000/0001/0002/0003/0103/0300 of the primary controller:
    readPrimaryGlobalRegisters(); printf(",");

    //  6: The set of control and status registers for the primary controller:
    readPrimaryPortControlAndStatusInfo(); printf(",");

    //  7: The global registers of the secondary controller:
    readCoreSecondaryGlobalRegisters(); printf(",");

    //  8:. The set of control and status registers for the secondary controller:
    readSecondaryPortControlAndStatusInfo(); printf(",");

    //  9: The set of output/input contact closure data.
    displayContactClosureInfo(); printf(",");

    // 10: Temperature and fan control info:
    displayTempAndFanInfo(); printf(",");

    // 11: Display POE controller info (power and temperature registers).
    displayPoeInfo();

    printf("}}}\r\n"); // Always do this before going home.

    return;
}


// helloResponseTerseData
//
// Reply is of the form:
//
// {"CLI":{"COMMAND":"hello",
//         "COMMAND ARG":"NONE",
//         "CODE":0,
//         "RESPONSE":{"UPTIME (SECS)" :494527,
//                     "TEMPERATURE":"32.1",
//                     "CURRENT FAN SPEED":"OFF",
//                     "POE TEMPERATURE":"23.1"}}}}
//
static void helloResponseTerseData(void)
{
    printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{", CLI_STRING, COMMAND_STRING, KEEP_ALIVE_HELLO_CMD, ARGUMENTS_STRING, CLI_ARG_TERSE_STRING, CODE_STRING, 0, RESPONSE_STRING);

    // Uptime:
    printf("%s:%lu,", UPTIME_STRING, g_ul_ms_ticks/1000);

    // Temperature:
    printf("%s:\"%ld\",", TEMPERATURE_STRING, tempFanControlCb.currentTempCelsius);

    // Fan speed:
    printf("%s:%s,", CURRENT_FAN_SPEED_STRING,   speedStringFromCurrentSpeed());

    // POE temperature:
    displayPoeTemperature();

    // Close the json string:
    printf("}}\r\n");

    return;
}


// displayFlashSignatures
//
//
//  "FLASH SIGNATURES":{"APPLICATION":"0x12345678","BOOTLOADER":"0x87654321"}
//
static void displayFlashSignatures(void)
{
    printf("%s:{%s:\"0x%08lX\",%s:\"0x%08lX\"}", FLASH_SIGNATURES_STRING, APPLICATION_STRING,  applicationFlashSignature, BOOTLOADER_STRING, bootloaderFlashSignature);
    return;
}


// getAutonomousEvents
//
// The gateway/iGos will periodically poll the AMU for autonomous events detected by the AMU.
// The poling is performed by CLI, this function is called to return the autonomous events.
// The list of events is contained in a json-like string.
//
// {"CLI":{"COMMAND":"get_auto_events","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"AUTONOMOUS EVENTS LIST":[]},"RESULT":"SUCCESS"}}
//
static void getAutonomousEvents(void)
{
    if (strlen(pAutonomousEventString) == 0) {
        // There are no outstanding autonomous events to report. Print an empty list.
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:[]},%s:%s}}\r\n",
               CLI_STRING, COMMAND_STRING,   GET_AUTONOMOUS_EVENTS_CMD,
                           ARGUMENTS_STRING, NONE_STRING,
                           CODE_STRING,      0,
                           RESPONSE_STRING,  AUTONOMOUS_EVENTS_LIST_STRING,
                           RESULT_STRING,    SUCCESS_STRING);
    } else {
        // Report the autonomous event string, which has been formatted to be JSON-like. The end of the
        // JSON-like string was not known while building up the string, so now is the time to "terminate"
        // the JSON-like string with a square bracket to close off the inner list, and a curly bracket to
        // close off the entire "object".
        //
        // Once the string is printed to standard output, the string is cleared.
        // Print the preamble. The events are from a list, so open with a square bracket.
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:",
               CLI_STRING, COMMAND_STRING,   GET_AUTONOMOUS_EVENTS_CMD,
                           ARGUMENTS_STRING, NONE_STRING,
                           CODE_STRING,      0,
                           RESPONSE_STRING);

        // Add the list of events and terminate the json-like string.
        printf("%s]},%s:%s}}\r\n", pAutonomousEventString, RESULT_STRING, SUCCESS_STRING);
        memset(&pAutonomousEventString[0], 0, AUTONOMOUS_EVENTS_STRING_SIZE);
    }

    // The event string has been reported; subsequent "bad events" can now be added.
    badEventAddedToAutoString = FALSE;

    if (!amuIsManaged) {
        // Ethernet port and POE checking can commence.
        amuIsManaged = TRUE;
        sprintf(&pCraftPortDebugData[0], "AMU NOW UNDER SBC/GATEWAY MANAGEMENT\r\n"); UART1_WRITE_PACKET_MACRO

        setEthernetCheckingEnabled(TRUE);
        setPoeEnabled(TRUE);
    }

    return;
}


static void processUart0InputString(char *pUserInput)
{
    // This "parser" is bare-bones, un-glamorous, not even pretty.
    //
    // One day, this function will grow into something we can all be proud of. Depending on compile options,
    // user input may or may not contain the end-of-input trailing signature with checksum. If disabled, the
    // presence of the signature has no effect on input analysis.

    // BUG NOTICE (not worth fixing until a proper parser is implemented (if ever):
    //
    // Issue: the user can enter a command such as "bannerrrr" and the command is validated correctly as the
    // "banner" command, since the 1st set of characters is a match for one of the recognized commands.

    if (strlen(pUserInput) > 0) {
        if (isInputChecksumValid(pUserInput)) {
            // The input checksum was OK. Process the user input.
            printf("\r\n"); // forces all subsequent output to start on a new line.

            // CLI Group 1: command from the SBC/Gateway controller:
            if      (strncmp(pUserInput, GET_AUTONOMOUS_EVENTS_CMD,             strlen(GET_AUTONOMOUS_EVENTS_CMD)) == 0)             { getAutonomousEvents();                                                            displayEndOfOutput(); } //   0
            else if (strncmp(pUserInput, KEEP_ALIVE_HELLO_CMD,                  strlen(KEEP_ALIVE_HELLO_CMD)) == 0)                  { helloResponse(pUserInput);                                                        displayEndOfOutput(); } //   1
            else if (strncmp(pUserInput, COUNTERS_CMD,                          strlen(COUNTERS_CMD)) == 0)                          { displayAndResetBadEventsCounters();                                               displayEndOfOutput(); } //   2
            else if (strncmp(pUserInput, UPGRADE_CMD,                           strlen(UPGRADE_CMD)) == 0)                           { processUpgradeCommand(pUserInput);                                                displayEndOfOutput(); } //   3
            else if (strncmp(pUserInput, RESET_CMD,                             strlen(RESET_CMD)) == 0)                             { resetFunc();                                                                      displayEndOfOutput(); } //   4
            else if (strncmp(pUserInput, JUMP_TO_BOOT_CMD,                      strlen(JUMP_TO_BOOT_CMD)) == 0)                      { jumpToBootloader();                                                               displayEndOfOutput(); } //   5
            else if (strncmp(pUserInput, PROGRAM_CMD,                           strlen(PROGRAM_CMD)) == 0)                           { programResponse();                                                                displayEndOfOutput(); } //   6
            else if (strncmp(pUserInput, PROGRAM_SIGNATURES_CMD,                strlen(PROGRAM_SIGNATURES_CMD)) == 0)                { displayFlashProgramSignatures();                                                  displayEndOfOutput(); } //   7
            else if (strncmp(pUserInput, CONTACT_CLOSURE_OUTPUT_CLOSE_CMD,      strlen(CONTACT_CLOSURE_OUTPUT_CLOSE_CMD)) == 0)      { contactClosureOutputControl(pUserInput, OUTPUT_CC_CLOSE);                         displayEndOfOutput(); } //   8
            else if (strncmp(pUserInput, CONTACT_CLOSURE_OUTPUT_OPEN_CMD,       strlen(CONTACT_CLOSURE_OUTPUT_OPEN_CMD)) == 0)       { contactClosureOutputControl(pUserInput, OUTPUT_CC_OPEN);                          displayEndOfOutput(); } //   9
            else if (strncmp(pUserInput, PORT_POWER_UP_CMD,                     strlen(PORT_POWER_UP_CMD)) == 0)                     { portPowerUpDown(pUserInput,        PORT_COMMAND_POWER_UP);                        displayEndOfOutput(); } //  10
            else if (strncmp(pUserInput, PORT_POWER_DOWN_CMD,                   strlen(PORT_POWER_DOWN_CMD)) == 0)                   { portPowerUpDown(pUserInput,        PORT_COMMAND_POWER_DOWN);                      displayEndOfOutput(); } //  11
            else if (strncmp(pUserInput, POE_ONLY_PORT_POWER_UP_CMD,            strlen(POE_ONLY_PORT_POWER_UP_CMD)) == 0)            { poePortPowerUpDown(pUserInput,     PORT_COMMAND_POWER_UP);                        displayEndOfOutput(); } //  12
            else if (strncmp(pUserInput, POE_ONLY_PORT_POWER_DOWN_CMD,          strlen(POE_ONLY_PORT_POWER_DOWN_CMD)) == 0)          { poePortPowerUpDown(pUserInput,     PORT_COMMAND_POWER_DOWN);                      displayEndOfOutput(); } //  13
            else if (strncmp(pUserInput, SET_FAN_SPEED_SETTINGS_CMD,            strlen(SET_FAN_SPEED_SETTINGS_CMD)) == 0)            { setFanControlSpeedSettings(pUserInput);                                           displayEndOfOutput(); } //  14
            else if (strncmp(pUserInput, RESOLVE_MAC_LIST_CMD,                  strlen(RESOLVE_MAC_LIST_CMD)) == 0)                  { resolveMacList(pUserInput);                                                       displayEndOfOutput(); } //  15
            else if (strncmp(pUserInput, GET_MAC_RESOLUTION_RESULT_CMD,         strlen(GET_MAC_RESOLUTION_RESULT_CMD)) == 0)         { getMacResolutionResult();                                                         displayEndOfOutput(); } //  16
            else if (strncmp(pUserInput, LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,     strlen(LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD)) == 0)     { launchAluInfoByPortIndex(pUserInput);                                             displayEndOfOutput(); } //  17
            else if (strncmp(pUserInput, GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD, strlen(GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD)) == 0) { getAluInfoByPortIndexResult();                                                    displayEndOfOutput(); } //  18
            else if (strncmp(pUserInput, CANCEL_GET_ALU_MAC_RESULT_CMD,         strlen(CANCEL_GET_ALU_MAC_RESULT_CMD)) == 0)         { cancelGetAluMacResult();                                                          displayEndOfOutput(); } //  19
            else if (strncmp(pUserInput, DISPLAY_BANNER_CMD,                    strlen(DISPLAY_BANNER_CMD)) == 0)                    { displayApparentBanner();                                                          displayEndOfOutput(); } //  20
            else if (strncmp(pUserInput, CONFIG_STATIC_LAN_MAC_CMD,             strlen(CONFIG_STATIC_LAN_MAC_CMD)) == 0)             { configureStaticLanMac(pUserInput);                                                displayEndOfOutput(); } //  21
            else if (strncmp(pUserInput, GET_STATIC_LAN_MAC_INFO_CMD,           strlen(GET_STATIC_LAN_MAC_INFO_CMD)) == 0)           { userGetStaticLanMacInfo();                                                        displayEndOfOutput(); } //  22

            // CLI Group 2:
            else if (strncmp(pUserInput, ETH_ONLY_PORT_ENABLE_CMD,              strlen(ETH_ONLY_PORT_ENABLE_CMD)) == 0)              { portEthernetOnlyUpDown(pUserInput, PORT_COMMAND_ETH_ONLY_UP);                     displayEndOfOutput(); } //  23
            else if (strncmp(pUserInput, ETH_ONLY_PORT_DISABLE_CMD,             strlen(ETH_ONLY_PORT_DISABLE_CMD)) == 0)             { portEthernetOnlyUpDown(pUserInput, PORT_COMMAND_ETH_ONLY_DOWN);                   displayEndOfOutput(); } //  24
            else if (strncmp(pUserInput, READ_ALU_PRI_BY_INDEX_CMD,             strlen(READ_ALU_PRI_BY_INDEX_CMD)) == 0)             { userReadAluPrimaryByIndex(pUserInput);                                            displayEndOfOutput(); } //  25
            else if (strncmp(pUserInput, READ_ALU_PRI_RANGE_CMD,                strlen(READ_ALU_PRI_RANGE_CMD)) == 0)                { userReadAluPrimaryRange(pUserInput);                                              displayEndOfOutput(); } //  26
            else if (strncmp(pUserInput, READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD,    strlen(READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD)) == 0)    { userReadAluPrimaryWithMacAsIndex(pUserInput);                                     displayEndOfOutput(); } //  27
            else if (strncmp(pUserInput, READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD,     strlen(READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD)) == 0)     { userReadAluSecondaryDynamicByIndex(pUserInput);                                   displayEndOfOutput(); } //  28
            else if (strncmp(pUserInput, READ_ALU_SEC_RANGE_CMD,                strlen(READ_ALU_SEC_RANGE_CMD)) == 0)                { readAluSecondaryRange(pUserInput);                                                displayEndOfOutput(); } //  29
            else if (strncmp(pUserInput, ALU_MAC_RESO_PARAM_CMD,                strlen(ALU_MAC_RESO_PARAM_CMD)) == 0)                { macResolutionParamUpdate(pUserInput);                                             displayEndOfOutput(); } //  30
            else if (strncmp(pUserInput, USER_SIGNATURE_READ_FLASH_CMD,         strlen(USER_SIGNATURE_READ_FLASH_CMD)) == 0)         { readUserSignatureFlash();                                                         displayEndOfOutput(); } //  31
            else if (strncmp(pUserInput, USER_SIGNATURE_READ_RAM_CMD,           strlen(USER_SIGNATURE_READ_RAM_CMD)) == 0)           { readUserSignatureRam();                                                           displayEndOfOutput(); } //  32
            else if (strncmp(pUserInput, USER_SIGNATURE_WRITE_CMD,              strlen(USER_SIGNATURE_WRITE_CMD)) == 0)              { userSignatureWrite(pUserInput);                                                   displayEndOfOutput(); } //  33
            else if (strncmp(pUserInput, USER_SIGNATURE_ERASE_CMD,              strlen(USER_SIGNATURE_ERASE_CMD)) == 0)              { userSignatureErase();                                                             displayEndOfOutput(); } //  34
            else if (strncmp(pUserInput, READ_BOTH_KSZ_MAC_CMD,                 strlen(READ_BOTH_KSZ_MAC_CMD)) == 0)                 { readBothControllersMac();                                                         displayEndOfOutput(); } //  35
            else if (strncmp(pUserInput, SET_LOCAL_TESTING_CMD,                 strlen(SET_LOCAL_TESTING_CMD)) == 0)                 {                                                                                                         } //  36
            else if (strncmp(pUserInput, POE_GENERAL_INFO_CMD,                  strlen(POE_GENERAL_INFO_CMD)) == 0)                  { poeHighLevelGeneralInfo();                                                        displayEndOfOutput(); } //  37
            else if (strncmp(pUserInput, POE_CONTROLLER_INPUT_VOLTAGE_CMD,      strlen(POE_CONTROLLER_INPUT_VOLTAGE_CMD)) == 0)      { poeReadControllerInputVoltage();                                                  displayEndOfOutput(); } //  38
            else if (strncmp(pUserInput, ETHERNET_CONTROLLER_RESET_CMD,         strlen(ETHERNET_CONTROLLER_RESET_CMD)) == 0)         { ethernetControllerReset(pUserInput);                                              displayEndOfOutput(); } //  39
            else if (strncmp(pUserInput, TEMPERATURE_CMD,                       strlen(TEMPERATURE_CMD)) == 0)                       { displayTemperature();                                                             displayEndOfOutput(); } //  40
            else if (strncmp(pUserInput, TEMP_FAN_INFO_CMD,                     strlen(TEMP_FAN_INFO_CMD)) == 0)                     { tempFanInfo();                                                                    displayEndOfOutput(); } //  41
            else if (strncmp(pUserInput, FAN_INFO_CMD,                          strlen(FAN_INFO_CMD)) == 0)                          { fanInfo(pUserInput);                                                              displayEndOfOutput(); } //  42
            else if (strncmp(pUserInput, FAN_TASTIC_CMD,                        strlen(FAN_TASTIC_CMD)) == 0)                        { fantasticControl(pUserInput);                                                     displayEndOfOutput(); } //  43
            else if (strncmp(pUserInput, READ_PCBA_JUMPERS_CMD,                 strlen(READ_PCBA_JUMPERS_CMD)) == 0)                 { readPcbaJumpers(JSON_OUTPUT);                                                     displayEndOfOutput(); } //  44
            else if (strncmp(pUserInput, DISPLAY_HELP_CMD,                      strlen(DISPLAY_HELP_CMD)) == 0)                      { displayApparentHelp(pUserInput);                                                  displayEndOfOutput(); } //  45
            else if (strncmp(pUserInput, LAN_PORT_ENABLE_CMD,                   strlen(LAN_PORT_ENABLE_CMD)) == 0)                   { userLanPortUpDown(PORT_COMMAND_ETH_ONLY_UP);                                      displayEndOfOutput(); } //  46
            else if (strncmp(pUserInput, LAN_PORT_DISABLE_CMD,                  strlen(LAN_PORT_DISABLE_CMD)) == 0)                  { userLanPortUpDown(PORT_COMMAND_ETH_ONLY_DOWN);                                    displayEndOfOutput(); } //  47
            else if (strncmp(pUserInput, DISPLAY_POE_PORT_POWER_CMD,            strlen(DISPLAY_POE_PORT_POWER_CMD)) == 0)            { userDisplayPoePortPower();                                                        displayEndOfOutput(); } //  48
            else if (strncmp(pUserInput, DISPLAY_PORT_INFO_CMD,                 strlen(DISPLAY_PORT_INFO_CMD)) == 0)                 { userDisplayPortInfo(pUserInput);                                                  displayEndOfOutput(); } //  49
            else if (strncmp(pUserInput, GET_CC_INFO_CMD,                       strlen(GET_CC_INFO_CMD)) == 0)                       { userGetContactClosureInfo();                                                      displayEndOfOutput(); } //  50
            else if (strncmp(pUserInput, ENABLE_SWITCH_CHECKING_CMD,            strlen(ENABLE_SWITCH_CHECKING_CMD)) == 0)            { enableEthernetControllerChecking(TRUE);                                           displayEndOfOutput(); } //  51
            else if (strncmp(pUserInput, DISABLE_SWITCH_CHECKING_CMD,           strlen(DISABLE_SWITCH_CHECKING_CMD)) == 0)           { enableEthernetControllerChecking(FALSE);                                          displayEndOfOutput(); } //  52
            else if (strncmp(pUserInput, ENABLE_POE_CHECKING_CMD,               strlen(ENABLE_POE_CHECKING_CMD)) == 0)               { enablePoeCheckingCmd(TRUE);                                                       displayEndOfOutput(); } //  53
            else if (strncmp(pUserInput, DISABLE_POE_CHECKING_CMD,              strlen(DISABLE_POE_CHECKING_CMD)) == 0)              { enablePoeCheckingCmd(FALSE);                                                      displayEndOfOutput(); } //  54
            else if (strncmp(pUserInput, POE_READ_PORT_EVENT_REGISTER_CMD,      strlen(POE_READ_PORT_EVENT_REGISTER_CMD)) == 0)      { userPoeReadPortEventRegister(pUserInput);                                         displayEndOfOutput(); } //  55
            else if (strncmp(pUserInput, POE_READ_SUPPLY_EVENT_REGISTER_CMD,    strlen(POE_READ_SUPPLY_EVENT_REGISTER_CMD)) == 0)    { userPoeReadSupplyEventRegister();                                                 displayEndOfOutput(); } //  56

            // CLI Group 3:
            else if (strncmp(pUserInput, POE_DEVICE_ID_CMD,                     strlen(POE_DEVICE_ID_CMD)) == 0)                     { poeReadDeviceId();                                                                displayEndOfOutput(); } //  57
            else if (strncmp(pUserInput, POE_MFR_ID_CMD,                        strlen(POE_MFR_ID_CMD)) == 0)                        { poeReadManufacturerId();                                                          displayEndOfOutput(); } //  58
            else if (strncmp(pUserInput, POE_READ_BYTE_CMD,                     strlen(POE_READ_BYTE_CMD)) == 0)                     { poeReadByte(pUserInput);                                                          displayEndOfOutput(); } //  59
            else if (strncmp(pUserInput, POE_READ_WORD_CMD,                     strlen(POE_READ_WORD_CMD)) == 0)                     { poeReadWord(pUserInput);                                                          displayEndOfOutput(); } //  60
            else if (strncmp(pUserInput, POE_WRITE_BYTE_CMD,                    strlen(POE_WRITE_BYTE_CMD)) == 0)                    { poeWriteByte(pUserInput);                                                         displayEndOfOutput(); } //  61
            else if (strncmp(pUserInput, POE_WRITE_WORD_CMD,                    strlen(POE_WRITE_WORD_CMD)) == 0)                    { poeWriteWord(pUserInput);                                                         displayEndOfOutput(); } //  62
          //else if (strncmp(pUserInput, GET_SWITCH_MIB_COUNTER_CMD,            strlen(GET_SWITCH_MIB_COUNTER_CMD)) == 0)            { getSwitchMibCounterCommand(pUserInput);                                           displayEndOfOutput(); } //  63  // disabled
          //else if (strncmp(pUserInput, ENABLE_MIB_FLUSH_FREEZE_CMD,           strlen(ENABLE_MIB_FLUSH_FREEZE_CMD)) == 0)           { enableFlushFreezeAllPortsCommand();                                               displayEndOfOutput(); } //  64  // disabled
          //else if (strncmp(pUserInput, DISABLE_MIB_FLUSH_FREEZE_CMD,          strlen(DISABLE_MIB_FLUSH_FREEZE_CMD)) == 0)          { disableFlushFreezeAllPortsCommand();                                              displayEndOfOutput(); } //  65  // disabled
          //else if (strncmp(pUserInput, FLUSH_MIB_COUNTERS_CMD,                strlen(FLUSH_MIB_COUNTERS_CMD)) == 0)                { flushSwitchMibCountersCommand(pUserInput);                                        displayEndOfOutput(); } //  66  // disabled
          //else if (strncmp(pUserInput, FREEZE_MIB_COUNTERS_CMD,               strlen(FREEZE_MIB_COUNTERS_CMD)) == 0)               { freezeSwitchMibCountersCommand(pUserInput);                                       displayEndOfOutput(); } //  67  // disabled
          //else if (strncmp(pUserInput, UNFREEZE_MIB_COUNTERS_CMD,             strlen(UNFREEZE_MIB_COUNTERS_CMD)) == 0)             { unfreezeSwitchMibCountersCommand(pUserInput);                                     displayEndOfOutput(); } //  68  // disabled
            else if (strncmp(pUserInput, SAME70_READ_REG_CMD,                   strlen(SAME70_READ_REG_CMD)) == 0)                   { same70ReadRegisterCommand(pUserInput);                                            displayEndOfOutput(); } //  69
            else if (strncmp(pUserInput, SAME70_WRITE_REG_CMD,                  strlen(SAME70_WRITE_REG_CMD)) == 0)                  { same70WriteRegisterCommand(pUserInput);                                           displayEndOfOutput(); } //  70
            else if (strncmp(pUserInput, READ_5PORTS_BYTE_BY_ADDRESS_CMD,       strlen(READ_5PORTS_BYTE_BY_ADDRESS_CMD)) == 0)       { read5PortsByteByAddress(pUserInput);                                              displayEndOfOutput(); } //  71
            else if (strncmp(pUserInput, WRITE_5PORTS_BYTE_BY_ADDRESS_CMD,      strlen(WRITE_5PORTS_BYTE_BY_ADDRESS_CMD)) == 0)      { write5PortsByteByAddress(pUserInput);                                             displayEndOfOutput(); } //  72
            else if (strncmp(pUserInput, READ_5PORTS_WORD_BY_ADDRESS_CMD,       strlen(READ_5PORTS_WORD_BY_ADDRESS_CMD)) == 0)       { read5PortsWordByAddress(pUserInput);                                              displayEndOfOutput(); } //  73
            else if (strncmp(pUserInput, WRITE_5PORTS_WORD_BY_ADDRESS_CMD,      strlen(WRITE_5PORTS_WORD_BY_ADDRESS_CMD)) == 0)      { write5PortsWordByAddress(pUserInput);                                             displayEndOfOutput(); } //  74
            else if (strncmp(pUserInput, ENABLE_25MHZ_CLOCK_OUTPUT_CMD,         strlen(ENABLE_25MHZ_CLOCK_OUTPUT_CMD)) == 0)         { enable25MhzClockOutput(ENABLE_DISABLE_IS_BY_COMMAND);                             displayEndOfOutput(); } //  75
            else if (strncmp(pUserInput, DISABLE_25MHZ_CLOCK_OUTPUT_CMD,        strlen(DISABLE_25MHZ_CLOCK_OUTPUT_CMD)) == 0)        { disable25MhzClockOutput(ENABLE_DISABLE_IS_BY_COMMAND);                            displayEndOfOutput(); } //  76
            else if (strncmp(pUserInput, READ_PRI_BYTE_BY_ADDRESS_CMD,          strlen(READ_PRI_BYTE_BY_ADDRESS_CMD)) == 0)          { readPrimaryByteByAddress(pUserInput) ;                                            displayEndOfOutput(); } //  77
            else if (strncmp(pUserInput, WRITE_PRI_BYTE_BY_ADDRESS_CMD,         strlen(WRITE_PRI_BYTE_BY_ADDRESS_CMD)) == 0)         { writePrimaryByteByAddress(pUserInput);                                            displayEndOfOutput(); } //  78
            else if (strncmp(pUserInput, READ_PRI_WORD_BY_ADDRESS_CMD,          strlen(READ_PRI_WORD_BY_ADDRESS_CMD)) == 0)          { readPrimaryWordByAddress(pUserInput);                                             displayEndOfOutput(); } //  79
            else if (strncmp(pUserInput, WRITE_PRI_WORD_BY_ADDRESS_CMD,         strlen(WRITE_PRI_WORD_BY_ADDRESS_CMD)) == 0)         { writePrimaryWordByAddress(pUserInput);                                            displayEndOfOutput(); } //  80
            else if (strncmp(pUserInput, READ_SEC_BYTE_ADDRESS_CMD,             strlen(READ_SEC_BYTE_ADDRESS_CMD)) == 0)             { readSecondaryByteAddress(pUserInput);                                             displayEndOfOutput(); } //  81
            else if (strncmp(pUserInput, READ_SEC_4PORTS_BY_BASE_ADDR_CMD,      strlen(READ_SEC_4PORTS_BY_BASE_ADDR_CMD)) == 0)      { readSecondary4PortsByBaseAddress(pUserInput);                                     displayEndOfOutput(); } //  82
            else if (strncmp(pUserInput, WRITE_SEC_BYTE_BY_ADDRESS_CMD,         strlen(WRITE_SEC_BYTE_BY_ADDRESS_CMD)) == 0)         { writeSecondaryByteByAddress(pUserInput);                                          displayEndOfOutput(); } //  83
            else if (strncmp(pUserInput, UART0_READ_MODE_STATUS_CMD,            strlen(UART0_READ_MODE_STATUS_CMD)) == 0)            { readUsart0ModeStatusCommand();                                                    displayEndOfOutput(); } //  84
            else if (strncmp(pUserInput, USART2_READ_MODE_STATUS_CMD,           strlen(USART2_READ_MODE_STATUS_CMD)) == 0)           { readUsart2ModeStatusCommand();                                                    displayEndOfOutput(); } //  85
            else if (strncmp(pUserInput, USART_RESET_STATUS_CMD,                strlen(USART_RESET_STATUS_CMD)) == 0)                { usartResetStatusCommand(pUserInput);                                              displayEndOfOutput(); } //  86
            else if (strncmp(pUserInput, RGB_LED_RED_MEDIUM_CMD,                strlen(RGB_LED_RED_MEDIUM_CMD)) == 0)                { userSetRgbLed(RGB_LED_RED_MEDIUM_CMD,     RGB_LED_RED_GPIO,   RGB_LED_MEDIUM);    displayEndOfOutput(); } //  87
            else if (strncmp(pUserInput, RGB_LED_RED_OFF_CMD,                   strlen(RGB_LED_RED_OFF_CMD)) == 0)                   { userSetRgbLed(RGB_LED_RED_OFF_CMD,        RGB_LED_RED_GPIO,   RGB_LED_OFF);       displayEndOfOutput(); } //  88
            else if (strncmp(pUserInput, RGB_LED_RED_BRILLIANT_CMD,             strlen(RGB_LED_RED_BRILLIANT_CMD)) == 0)             { userSetRgbLed(RGB_LED_RED_BRILLIANT_CMD,  RGB_LED_RED_GPIO,   RGB_LED_BRILLIANT); displayEndOfOutput(); } //  89
            else if (strncmp(pUserInput, RGB_LED_GREEN_MEDIUM_CMD,              strlen(RGB_LED_GREEN_MEDIUM_CMD)) == 0)              { userSetRgbLed(RGB_LED_GREEN_MEDIUM_CMD,   RGB_LED_GREEN_GPIO, RGB_LED_MEDIUM);    displayEndOfOutput(); } //  90
            else if (strncmp(pUserInput, RGB_LED_GREEN_OFF_CMD,                 strlen(RGB_LED_GREEN_OFF_CMD)) == 0)                 { userSetRgbLed(RGB_LED_GREEN_OFF_CMD,      RGB_LED_GREEN_GPIO, RGB_LED_OFF);       displayEndOfOutput(); } //  91
            else if (strncmp(pUserInput, RGB_LED_GREEN_BRILLIANT_CMD,           strlen(RGB_LED_GREEN_BRILLIANT_CMD)) == 0)           { userSetRgbLed(RGB_LED_GREEN_BRILLIANT_CMD,RGB_LED_GREEN_GPIO, RGB_LED_BRILLIANT); displayEndOfOutput(); } //  82
            else if (strncmp(pUserInput, RGB_LED_BLUE_MEDIUM_CMD,               strlen(RGB_LED_BLUE_MEDIUM_CMD)) == 0)               { userSetRgbLed(RGB_LED_BLUE_MEDIUM_CMD,    RGB_LED_BLUE_GPIO,  RGB_LED_MEDIUM);    displayEndOfOutput(); } //  93
            else if (strncmp(pUserInput, RGB_LED_BLUE_OFF_CMD,                  strlen(RGB_LED_BLUE_OFF_CMD)) == 0)                  { userSetRgbLed(RGB_LED_BLUE_OFF_CMD,       RGB_LED_BLUE_GPIO,  RGB_LED_OFF);       displayEndOfOutput(); } //  94
            else if (strncmp(pUserInput, RGB_LED_BLUE_BRILLIANT_CMD,            strlen(RGB_LED_BLUE_BRILLIANT_CMD)) == 0)            { userSetRgbLed(RGB_LED_BLUE_BRILLIANT_CMD, RGB_LED_BLUE_GPIO,  RGB_LED_BRILLIANT); displayEndOfOutput(); } //  95
            else if (strncmp(pUserInput, RGB_LED_ENABLE_CMD,                    strlen(RGB_LED_ENABLE_CMD)) == 0)                    { rgbLedFlashEnable(pUserInput, TRUE);                                              displayEndOfOutput(); } //  96
            else if (strncmp(pUserInput, RGB_LED_DISABLE_CMD,                   strlen(RGB_LED_DISABLE_CMD)) == 0)                   { rgbLedFlashEnable(pUserInput, FALSE);                                             displayEndOfOutput(); } //  97

            // CLI Group 3:
            else if (strncmp(pUserInput, ENABLE_CRAFT_DEBUG_OUTPUT_CMD,         strlen(ENABLE_CRAFT_DEBUG_OUTPUT_CMD)) == 0)         { craftDebugOutputOnOff(TRUE);                                                      displayEndOfOutput(); } //  98
            else if (strncmp(pUserInput, DISABLE_CRAFT_DEBUG_OUTPUT_CMD,        strlen(DISABLE_CRAFT_DEBUG_OUTPUT_CMD)) == 0)        { craftDebugOutputOnOff(FALSE);                                                     displayEndOfOutput(); } //  99
            else if (strncmp(pUserInput, ENABLE_POE_DEBUG_OUTPUT_CMD,           strlen(ENABLE_POE_DEBUG_OUTPUT_CMD)) == 0)           { poeDebugOutputOnOff(TRUE);                                                        displayEndOfOutput(); } // 100
            else if (strncmp(pUserInput, DISABLE_POE_DEBUG_OUTPUT_CMD,          strlen(DISABLE_POE_DEBUG_OUTPUT_CMD)) == 0)          { poeDebugOutputOnOff(FALSE);                                                       displayEndOfOutput(); } // 101
            else if (strncmp(pUserInput, ENABLE_I2C_FULL_OUTPUT_CMD,            strlen(ENABLE_I2C_FULL_OUTPUT_CMD)) == 0)            { i2cDebugOutputOnOff(TRUE);                                                        displayEndOfOutput(); } // 102
            else if (strncmp(pUserInput, DISABLE_I2C_FULL_OUTPUT_CMD,           strlen(DISABLE_I2C_FULL_OUTPUT_CMD)) == 0)           { i2cDebugOutputOnOff(FALSE);                                                       displayEndOfOutput(); } // 103
            else if (strncmp(pUserInput, ENABLE_UPGRADE_INFO_CMD,               strlen(ENABLE_UPGRADE_INFO_CMD)) == 0)               { upgradeInfoOutputOnOff(TRUE);                                                     displayEndOfOutput(); } // 104
            else if (strncmp(pUserInput, DISABLE_UPGRADE_INFO_CMD,              strlen(DISABLE_UPGRADE_INFO_CMD)) == 0)              { upgradeInfoOutputOnOff(FALSE);                                                    displayEndOfOutput(); } // 105
            else if (strncmp(pUserInput, ENABLE_UPGRADE_HEX_INFO_CMD,           strlen(ENABLE_UPGRADE_HEX_INFO_CMD)) == 0)           { upgradeHexInfoOutputOnOff(TRUE);                                                  displayEndOfOutput(); } // 106
            else if (strncmp(pUserInput, DISABLE_UPGRADE_HEX_INFO_CMD,          strlen(DISABLE_UPGRADE_HEX_INFO_CMD)) == 0)          { upgradeHexInfoOutputOnOff(FALSE);                                                 displayEndOfOutput(); } // 107
            else if (strncmp(pUserInput, DROP_UPGRADE_DATA_PACKET_CMD,          strlen(DROP_UPGRADE_DATA_PACKET_CMD)) == 0)          { dropUpgradeSomething(PLEASE_DROP_THE_NEXT_DATA_PACKET);                           displayEndOfOutput(); } // 108
            else if (strncmp(pUserInput, DROP_UPGRADE_PACKET_ACK_CMD,           strlen(DROP_UPGRADE_PACKET_ACK_CMD)) == 0)           { dropUpgradeSomething(PLEASE_DROP_THE_NEXT_PACKET_ACK);                            displayEndOfOutput(); } // 109
            else if (strncmp(pUserInput, PRINT_FH_CMD,                          strlen(PRINT_FH_CMD)) == 0)                          { printFlashHoldingBuffer();                                                        displayEndOfOutput(); } // 110
            else if (strncmp(pUserInput, SET_FH_PATTERN_CMD,                    strlen(SET_FH_PATTERN_CMD)) == 0)                    { setFlashHoldingPattern(pUserInput);                                               displayEndOfOutput(); } // 111
            else if (strncmp(pUserInput, READ_FLASH_BLOCK_CMD,                  strlen(READ_FLASH_BLOCK_CMD)) == 0)                  { readFromFlashMemoryIntoHolding(pUserInput);                                       displayEndOfOutput(); } // 112
            else if (strncmp(pUserInput, WRITE_FLASH_BLOCK_CMD,                 strlen(WRITE_FLASH_BLOCK_CMD)) == 0)                 { writeFromHoldingIntoFlashMemory(pUserInput);                                      displayEndOfOutput(); } // 113
            else if (strncmp(pUserInput, CRC_FLASH_BLOCK_CMD,                   strlen(CRC_FLASH_BLOCK_CMD)) == 0)                   { calculateAndPrintHoldingBlockCrc();                                               displayEndOfOutput(); } // 114
            else if (strncmp(pUserInput, IS_FLASH_RANGE_ERASED_CMD,             strlen(IS_FLASH_RANGE_ERASED_CMD)) == 0)             { isCheckFlashErasedTestCommand(pUserInput);                                        displayEndOfOutput(); } // 115
            else if (strncmp(pUserInput, READ_FLASH_LONG_CMD,                   strlen(READ_FLASH_LONG_CMD)) == 0)                   { readFlashCommand(pUserInput);                                                     displayEndOfOutput(); } // 116
            else if (strncmp(pUserInput, WRITE_FLASH_LONG_CMD,                  strlen(WRITE_FLASH_LONG_CMD)) == 0)                  { writeFlashLongCommand(pUserInput);                                                displayEndOfOutput(); } // 117
            else if (strncmp(pUserInput, SOFTWARE_RESET_CMD,                    strlen(SOFTWARE_RESET_CMD)) == 0)                    { prepareSoftwareResetFunc();                                                       displayEndOfOutput(); } // 118
            else if (strncmp(pUserInput, NOP_CMD,                               strlen(NOP_CMD)) == 0)                               { executeNOP();                                                                     displayEndOfOutput(); } // 119
            else if (strncmp(pUserInput, WATCHDOG_DISABLE_TEST_CMD,             strlen(WATCHDOG_DISABLE_TEST_CMD)) == 0)             { disableWatchdog();                                                                displayEndOfOutput(); } // 120

            else {
                printf("{%s:{%s:\"%s\",%s:%s,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,  pUserInput,             RESULT_STRING, FAIL_STRING,
                                                                             RESPONSE_STRING, INVALID_COMMAND_STRING, INFO_STRING,   NONE_STRING);
                displayEndOfOutput();
            }
        } else {
            // Checksum did not pass muster. COUNTER???
        }
    } else {
        // Empty buffer. Normally don't come here unless the user entered a plain <CR> that is echo'd locally.
        printf("\r\n");
    }
}










// processUart1InputString
//
// Some input from the CRAFT/Debug port. Ordinarily, it would be beneficial to parse, process and execute.
// But no requirement has been established to do so, nor is it needed - so far.
//
static void processUart1InputString(char *pUserInput)
{
    if (strlen(pUserInput) > 0) {
        usart_serial_write_packet((usart_if)UART1, (uint8_t *)CRAFT_STRING_TO_PROCESS_STRING, strlen(CRAFT_STRING_TO_PROCESS_STRING));
        usart_serial_write_packet((usart_if)UART1, (uint8_t *)pUserInput,                     strlen(pUserInput));
        usart_serial_write_packet((usart_if)UART1, (uint8_t *)"\r\n",                         2);
    } else {
        // Empty buffer. Normally don't come here unless the user entered a plain <CR> that is echo'd locally.
        printf("\r\n");
    }
}


// readPcbaJumpers
//
// Read the GPIO pins assigned to the PCBA Option Jumpers to determine the hardware revision. 
//
// The current interpretation of the set of 8 PCBA Option Jumpers tied to GPIO is as follows:
//
// - the 4 HW 7/6/5/4 "hw option"   pins form the minor component of the hardware revision
// - the 4 HW 3/2/1/0 "hw revision" pins form the major component of the hardware revision
//
// such that the resulting HW REVISION is: major dot minor
//
// If output is called for, output something like:
//
// {"CLI":{"COMMAND":"read_pcba_jumpers","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"HW_7":"0x00000000","HW_6":"0x00000000",
//                                                                                 "HW_5":"0x00000000","HW_4":"0x00000000",
//                                                                                 "HW_3":"0x00000000","HW_2":"0x00000000",
//                                                                                 "HW_1":"0x00000000","HW_0":"0x00000001"},"RESULT":"SUCCESS"}}
void readPcbaJumpers(BOOL outputJson)
{
    U32 pinLevel;
    U8  my4Bits = 0;

    if (outputJson) { printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{", CLI_STRING, COMMAND_STRING, READ_PCBA_JUMPERS_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESPONSE_STRING); }

    // Read the 4 bits that form the minor component:
    pinLevel = ioport_get_pin_level(HW_7_OPTION_BIT_3);        my4Bits |= (pinLevel << 3);  if (outputJson) { printf("\"HW_7\":\"0x%08lX\",", pinLevel); }
    pinLevel = ioport_get_pin_level(HW_6_OPTION_BIT_2);        my4Bits |= (pinLevel << 2);  if (outputJson) { printf("\"HW_6\":\"0x%08lX\",", pinLevel); }
    pinLevel = ioport_get_pin_level(HW_5_OPTION_BIT_1);        my4Bits |= (pinLevel << 1);  if (outputJson) { printf("\"HW_5\":\"0x%08lX\",", pinLevel); }
    pinLevel = ioport_get_pin_level(HW_4_OPTION_BIT_0);        my4Bits |= (pinLevel << 0);  if (outputJson) { printf("\"HW_4\":\"0x%08lX\",", pinLevel); }
    pcbaHardwareRevision.minor = my4Bits;
    // Read the 4 bits that form the major component:
    my4Bits = 0;
    pinLevel = ioport_get_pin_level(HW_3_PCBA_REVISION_BIT_3); my4Bits |= (pinLevel << 3);  if (outputJson) { printf("\"HW_3\":\"0x%08lX\",", pinLevel); }
    pinLevel = ioport_get_pin_level(HW_2_PCBA_REVISION_BIT_2); my4Bits |= (pinLevel << 2);  if (outputJson) { printf("\"HW_2\":\"0x%08lX\",", pinLevel); }
    pinLevel = ioport_get_pin_level(HW_1_PCBA_REVISION_BIT_1); my4Bits |= (pinLevel << 1);  if (outputJson) { printf("\"HW_1\":\"0x%08lX\",", pinLevel); }
    pinLevel = ioport_get_pin_level(HW_0_PCBA_REVISION_BIT_0); my4Bits |= (pinLevel << 0);  if (outputJson) { printf("\"HW_0\":\"0x%08lX\"},%s:%s}}\r\n", pinLevel, RESULT_STRING, SUCCESS_STRING); }
    pcbaHardwareRevision.major = my4Bits;
}



// displayApparentBanner
//
// Display the apparent banner. Since it is not called from the Gateway daemon,
// and because it's called as a plain command from igos: it is not json-like.
//
void displayApparentBanner(void)
{
    // Output the system header.
    printf(APPARENT_SYSTEM_HEADER);

    printf("-- Product:     %s\r\n-- Serial:      %s\r\n-- Model:       %s\r\n-- HW Revision: %X.%X\r\n-- UPTIME/SECS: %lu\r\n",
           pAmuProduct, pAmuSerialNumber, pAmuModel, pcbaHardwareRevision.major, pcbaHardwareRevision.minor, g_ul_ms_ticks/1000);

    // Output the application's version.
    printf("%s%s\r\n", APPARENT_RELEASE_TITLE_STRING, APPARENT_RELEASE_STRING);

    // Show the address in flash where the application is loaded.
    printf("-- Application is in flash at 0x%08lX\r\n", applicationLoadFlashAddress);

    displayControllerResetReason(NO_JSON_OUTPUT);

    // Let the user know that help is available:
    // Actually, not a good idea, it is too risky for an overly curious miscreant.
}


/**
 * Apparent
 * Display the AMU help. Command is of the form:
 *
 * heeeeelp group=<1|2|3|4|list>
 *
 */
static void displayApparentHelp(char *pUserInput)
{
    char  *pArguments;
    char   pGroupString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    cliGroup;

    pArguments = &pUserInput[strlen(DISPLAY_HELP_CMD) + 1];
    memset(pGroupString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_GROUP_STRING, pGroupString, pArguments);

    if (strlen(pGroupString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {

        // Did the user simply requested a list of commands?
        if (strncmp(pGroupString, CLI_ARG_LIST_STRING, strlen(CLI_ARG_LIST_STRING)) == 0) {
            displayCliCommandList();
        } else {
            // Must be a request for help for one of the 4 CLI command groups.
            cliGroup = atoi(&pGroupString[0]);
            switch (cliGroup) {
                case 1:  displayCliHelpCommandGroup1();              break; // Group 1:
    	        case 2:  displayCliHelpCommandGroup2();              break; // Group 2:
        	    case 3:  displayCliHelpCommandGroup3();              break; // Group 3:
                case 4:  displayCliHelpCommandGroup4();              break; // Group 4:
        	    default: displayInvalidParameters(pUserInput, NULL); break;
            } // switch ...
        }
    }
}


// displayCliCommandList
//
//
//
static void displayCliCommandList(void)
{
    printf("CLI COMMAND LIST:\r\n");

    printf("GROUP 1 COMMANDS:\r\n");
    printf("%s\r\n", GET_AUTONOMOUS_EVENTS_CMD);                //   0
    printf("%s\r\n", KEEP_ALIVE_HELLO_CMD);                     //   1
    printf("%s\r\n", COUNTERS_CMD);                             //   2
    printf("%s\r\n", UPGRADE_CMD);                              //   3
    printf("%s\r\n", RESET_CMD);                                //   4
    printf("%s\r\n", JUMP_TO_BOOT_CMD);                         //   5
    printf("%s\r\n", PROGRAM_CMD);                              //   6
    printf("%s\r\n", PROGRAM_SIGNATURES_CMD);                   //   7
    printf("%s\r\n", CONTACT_CLOSURE_OUTPUT_CLOSE_CMD);         //   8
    printf("%s\r\n", CONTACT_CLOSURE_OUTPUT_OPEN_CMD);          //   9
    printf("%s\r\n", PORT_POWER_UP_CMD);                        //  10
    printf("%s\r\n", PORT_POWER_DOWN_CMD);                      //  11
    printf("%s\r\n", POE_ONLY_PORT_POWER_UP_CMD);               //  12
    printf("%s\r\n", POE_ONLY_PORT_POWER_DOWN_CMD);             //  13
    printf("%s\r\n", SET_FAN_SPEED_SETTINGS_CMD);               //  14
    printf("%s\r\n", RESOLVE_MAC_LIST_CMD);                     //  15
    printf("%s\r\n", GET_MAC_RESOLUTION_RESULT_CMD);            //  16
    printf("%s\r\n", LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD);        //  17
    printf("%s\r\n", GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD);    //  18
    printf("%s\r\n", CANCEL_GET_ALU_MAC_RESULT_CMD);            //  19
    printf("%s\r\n", CONFIG_STATIC_LAN_MAC_CMD);                //  20
    printf("%s\r\n", GET_STATIC_LAN_MAC_INFO_CMD);              //  21

    printf("GROUP 2 COMMANDS:\r\n");
    printf("%s\r\n", DISPLAY_BANNER_CMD);                       //  22
    printf("%s\r\n", ETH_ONLY_PORT_ENABLE_CMD);                 //  23
    printf("%s\r\n", ETH_ONLY_PORT_DISABLE_CMD);                //  24
    printf("%s\r\n", READ_ALU_PRI_BY_INDEX_CMD);                //  25
    printf("%s\r\n", READ_ALU_PRI_RANGE_CMD);                   //  26
    printf("%s\r\n", READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD);       //  27
    printf("%s\r\n", READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD);        //  28
    printf("%s\r\n", READ_ALU_SEC_RANGE_CMD);                   //  29
    printf("%s\r\n", ALU_MAC_RESO_PARAM_CMD);                   //  30
    printf("%s\r\n", USER_SIGNATURE_READ_FLASH_CMD);            //  31
    printf("%s\r\n", USER_SIGNATURE_READ_RAM_CMD);              //  32
    printf("%s\r\n", USER_SIGNATURE_WRITE_CMD);                 //  33
    printf("%s\r\n", USER_SIGNATURE_ERASE_CMD);                 //  34
    printf("%s\r\n", READ_BOTH_KSZ_MAC_CMD);                    //  35
    printf("%s\r\n", SET_LOCAL_TESTING_CMD);                    //  36
    printf("%s\r\n", POE_GENERAL_INFO_CMD);                     //  37
    printf("%s\r\n", POE_CONTROLLER_INPUT_VOLTAGE_CMD);         //  38
    printf("%s\r\n", ETHERNET_CONTROLLER_RESET_CMD);            //  39
    printf("%s\r\n", TEMPERATURE_CMD);                          //  40
    printf("%s\r\n", TEMP_FAN_INFO_CMD);                        //  41
    printf("%s\r\n", FAN_INFO_CMD);                             //  42
    printf("%s\r\n", FAN_TASTIC_CMD);                           //  43
    printf("%s\r\n", READ_PCBA_JUMPERS_CMD);                    //  44
    printf("%s\r\n", DISPLAY_HELP_CMD);                         //  45
    printf("%s\r\n", LAN_PORT_ENABLE_CMD);                      //  46
    printf("%s\r\n", LAN_PORT_DISABLE_CMD);                     //  47
    printf("%s\r\n", DISPLAY_POE_PORT_POWER_CMD);               //  48
    printf("%s\r\n", DISPLAY_PORT_INFO_CMD);                    //  49
    printf("%s\r\n", GET_CC_INFO_CMD);                          //  50
    printf("%s\r\n", ENABLE_SWITCH_CHECKING_CMD);               //  51
    printf("%s\r\n", DISABLE_SWITCH_CHECKING_CMD);              //  52
    printf("%s\r\n", ENABLE_POE_CHECKING_CMD);                  //  53
    printf("%s\r\n", DISABLE_POE_CHECKING_CMD);                 //  54
    printf("%s\r\n", POE_READ_PORT_EVENT_REGISTER_CMD);         //  55
    printf("%s\r\n", POE_READ_SUPPLY_EVENT_REGISTER_CMD);       //  56

    printf("GROUP 3 COMMANDS:\r\n");
    printf("%s\r\n", POE_DEVICE_ID_CMD);                        //  57
    printf("%s\r\n", POE_MFR_ID_CMD);                           //  58
    printf("%s\r\n", POE_READ_BYTE_CMD);                        //  59
    printf("%s\r\n", POE_READ_WORD_CMD);                        //  60
    printf("%s\r\n", POE_WRITE_BYTE_CMD);                       //  61
    printf("%s\r\n", POE_WRITE_WORD_CMD);                       //  62
    printf("%s\r\n", GET_SWITCH_MIB_COUNTER_CMD);               //  63
    printf("%s\r\n", ENABLE_MIB_FLUSH_FREEZE_CMD);              //  64
    printf("%s\r\n", DISABLE_MIB_FLUSH_FREEZE_CMD);             //  65
    printf("%s\r\n", FLUSH_MIB_COUNTERS_CMD);                   //  66
    printf("%s\r\n", FREEZE_MIB_COUNTERS_CMD);                  //  67
    printf("%s\r\n", UNFREEZE_MIB_COUNTERS_CMD);                //  68
    printf("%s\r\n", SAME70_READ_REG_CMD);                      //  69
    printf("%s\r\n", SAME70_WRITE_REG_CMD);                     //  70
    printf("%s\r\n", READ_5PORTS_BYTE_BY_ADDRESS_CMD);          //  71
    printf("%s\r\n", WRITE_5PORTS_BYTE_BY_ADDRESS_CMD);         //  72
    printf("%s\r\n", READ_5PORTS_WORD_BY_ADDRESS_CMD);          //  73
    printf("%s\r\n", WRITE_5PORTS_WORD_BY_ADDRESS_CMD);         //  74
    printf("%s\r\n", ENABLE_25MHZ_CLOCK_OUTPUT_CMD);            //  75
    printf("%s\r\n", DISABLE_25MHZ_CLOCK_OUTPUT_CMD);           //  76
    printf("%s\r\n", READ_PRI_BYTE_BY_ADDRESS_CMD);             //  77
    printf("%s\r\n", WRITE_PRI_BYTE_BY_ADDRESS_CMD);            //  78
    printf("%s\r\n", READ_PRI_WORD_BY_ADDRESS_CMD);             //  79
    printf("%s\r\n", WRITE_PRI_WORD_BY_ADDRESS_CMD);            //  80
    printf("%s\r\n", READ_SEC_BYTE_ADDRESS_CMD);                //  81
    printf("%s\r\n", READ_SEC_4PORTS_BY_BASE_ADDR_CMD);         //  82
    printf("%s\r\n", WRITE_SEC_BYTE_BY_ADDRESS_CMD);            //  83
    printf("%s\r\n", UART0_READ_MODE_STATUS_CMD);               //  84
    printf("%s\r\n", USART2_READ_MODE_STATUS_CMD);              //  85
    printf("%s\r\n", USART_RESET_STATUS_CMD);                   //  86
    printf("%s\r\n", RGB_LED_RED_MEDIUM_CMD);                   //  87
    printf("%s\r\n", RGB_LED_RED_OFF_CMD);                      //  88
    printf("%s\r\n", RGB_LED_RED_BRILLIANT_CMD);                //  89
    printf("%s\r\n", RGB_LED_GREEN_MEDIUM_CMD);                 //  90
    printf("%s\r\n", RGB_LED_GREEN_OFF_CMD);                    //  91
    printf("%s\r\n", RGB_LED_GREEN_BRILLIANT_CMD);              //  92
    printf("%s\r\n", RGB_LED_BLUE_MEDIUM_CMD);                  //  93
    printf("%s\r\n", RGB_LED_BLUE_OFF_CMD);                     //  94
    printf("%s\r\n", RGB_LED_BLUE_BRILLIANT_CMD);               //  95
    printf("%s\r\n", RGB_LED_ENABLE_CMD);                       //  96
    printf("%s\r\n", RGB_LED_DISABLE_CMD);                      //  97

    printf("GROUP 4 COMMANDS:\r\n");
    printf("%s\r\n", ENABLE_CRAFT_DEBUG_OUTPUT_CMD);            //  98
    printf("%s\r\n", DISABLE_CRAFT_DEBUG_OUTPUT_CMD);           //  99
    printf("%s\r\n", ENABLE_POE_DEBUG_OUTPUT_CMD);              // 100
    printf("%s\r\n", DISABLE_POE_DEBUG_OUTPUT_CMD);             // 101
    printf("%s\r\n", ENABLE_I2C_FULL_OUTPUT_CMD);               // 102
    printf("%s\r\n", DISABLE_I2C_FULL_OUTPUT_CMD);              // 103
    printf("%s\r\n", ENABLE_UPGRADE_INFO_CMD);                  // 104
    printf("%s\r\n", DISABLE_UPGRADE_INFO_CMD);                 // 105
    printf("%s\r\n", ENABLE_UPGRADE_HEX_INFO_CMD);              // 106
    printf("%s\r\n", DISABLE_UPGRADE_HEX_INFO_CMD);             // 107
    printf("%s\r\n", DROP_UPGRADE_DATA_PACKET_CMD);             // 108
    printf("%s\r\n", DROP_UPGRADE_PACKET_ACK_CMD);              // 109
    printf("%s\r\n", PRINT_FH_CMD);                             // 110
    printf("%s\r\n", SET_FH_PATTERN_CMD);                       // 111
    printf("%s\r\n", CRC_FLASH_BLOCK_CMD);                      // 112
    printf("%s\r\n", IS_FLASH_RANGE_ERASED_CMD);                // 113
    printf("%s\r\n", READ_FLASH_LONG_CMD);                      // 114
    printf("%s\r\n", WRITE_FLASH_LONG_CMD);                     // 115
    printf("%s\r\n", READ_FLASH_BLOCK_CMD);                     // 116
    printf("%s\r\n", WRITE_FLASH_BLOCK_CMD);                    // 117
    printf("%s\r\n", SOFTWARE_RESET_CMD);                       // 118
    printf("%s\r\n", NOP_CMD);                                  // 119
    printf("%s\r\n", WATCHDOG_DISABLE_TEST_CMD);                // 120

}


// displayCliHelpCommandGroup1
//
//
//
static void displayCliHelpCommandGroup1(void) {
    printf("CLI COMMAND GROUP 1 HELP:\r\n");
    printf("%25s - get the autonomous events\r\n",                                                 /*   0 */ GET_AUTONOMOUS_EVENTS_CMD);
    printf("%25s - hello keep-alive <terse=yes> is optional\r\n",                                  /*   1 */ KEEP_ALIVE_HELLO_CMD);
    printf("%25s - display and reset the Apparent AMU counters\r\n",                               /*   2 */ COUNTERS_CMD);
    printf("%25s - boot loader upgrade: %s %s=<%s|%s|%s|%s|%s> %s=<hex file string data>\r\n",     /*   3 */ UPGRADE_CMD,                   UPGRADE_CMD,                   CLI_ARG_COMMAND_STRING,
                                                                                                             CLI_ARG_UPGRADE_START_STRING,  CLI_ARG_UPGRADE_DATA_STRING,   CLI_ARG_UPGRADE_END_STRING,
                                                                                                             CLI_ARG_UPGRADE_COMMIT_STRING, CLI_ARG_UPGRADE_CANCEL_STRING, CLI_ARG_UPGRADE_DATA_STRING);
    printf("%25s - AMU/SAME70 reset\r\n",                                                          /*   4 */ RESET_CMD);
    printf("%25s - jump to bootloader\r\n",                                                        /*   5 */ JUMP_TO_BOOT_CMD);
    printf("%25s - display the type of running program (application or bootloader)\r\n",           /*   6 */ PROGRAM_CMD);
    printf("%25s - display program flash signatures\r\n",                                          /*   7 */ PROGRAM_SIGNATURES_CMD);
    printf("%25s - output contact closure CLOSE: %s %s=<comma-separated list 1..4>\r\n",           /*   8 */ CONTACT_CLOSURE_OUTPUT_CLOSE_CMD,   CONTACT_CLOSURE_OUTPUT_CLOSE_CMD,   CLI_ARG_CC_STRING);
    printf("%25s - output contact closure OPEN:  %s %s=<comma-separated list 1..4>\r\n",           /*   9 */ CONTACT_CLOSURE_OUTPUT_OPEN_CMD,    CONTACT_CLOSURE_OUTPUT_OPEN_CMD,    CLI_ARG_CC_STRING);
    printf("%25s - port power up:    %s %s=<comma-separated list 1..8 NO LAN>\r\n",                /*  10 */ PORT_POWER_UP_CMD,                  PORT_POWER_UP_CMD,                  CLI_ARG_PORT_STRING);
    printf("%25s - port power down:  %s %s=<comma-separated list 1..8 NO LAN>\r\n",                /*  11 */ PORT_POWER_DOWN_CMD,                PORT_POWER_DOWN_CMD,                CLI_ARG_PORT_STRING);
    printf("%25s - poe controller only port power up:   %s %s=<1..8>\r\n",                         /*  12 */ POE_ONLY_PORT_POWER_UP_CMD,         POE_ONLY_PORT_POWER_UP_CMD,         CLI_ARG_PORT_STRING);
    printf("%25s - poe controller only port power down: %s %s=<1..8>\r\n",                         /*  13 */ POE_ONLY_PORT_POWER_DOWN_CMD,       POE_ONLY_PORT_POWER_DOWN_CMD,       CLI_ARG_PORT_STRING);
    printf("%25s - set fan speed settings: %s %s=<0..100 decimal> %s=<0..100 decimal>\r\n",        /*  14 */ SET_FAN_SPEED_SETTINGS_CMD,         SET_FAN_SPEED_SETTINGS_CMD,         CLI_ARG_LOW_STRING,
                                                                                                             CLI_ARG_MAX_STRING);
    printf("%25s - resolve list of MACs: %s %s=<comma-separated list of MACs (max length=%u>\r\n", /*  15 */ RESOLVE_MAC_LIST_CMD,               RESOLVE_MAC_LIST_CMD,               CLI_ARG_MAC_LIST_STRING,
                                                                                                             MAC_RESOLUTION_LIST_LENGTH_MAX);
    printf("%25s - get the current MAC resolution results\r\n",                                    /*  16 */ GET_MAC_RESOLUTION_RESULT_CMD);
    printf("%25s - return 1st KSZ ALU MAC from port and index: %s %s=<1..8> %s=<0..4095>\r\n",     /*  17 */ LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,  LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,  CLI_ARG_PORT_STRING,
                                                                                                             CLI_ARG_ALU_START_INDEX_STRING);
    printf("%25s - return the result of the GET ALU INFO process\r\n",                             /*  18 */ GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD);
    printf("%25s - cancel an active get-alu-mac-info process\r\n",                                 /*  19 */ CANCEL_GET_ALU_MAC_RESULT_CMD);
    printf("%25s - configure the gateway LAN MAC as static: %s %s=<some mac>\r\n",                 /*  20 */ CONFIG_STATIC_LAN_MAC_CMD,          CONFIG_STATIC_LAN_MAC_CMD,          CLI_ARG_MAC_STRING);
    printf("%25s - get the static LAN MAC info\r\n",                                               /*  21 */ GET_STATIC_LAN_MAC_INFO_CMD);
    return;
}


// displayCliHelpCommandGroup2
//
//
//
static void displayCliHelpCommandGroup2(void) {
    printf("CLI COMMAND GROUP 2 HELP:\r\n");
    printf("%25s - display the Apparent banner\r\n",                                               /*  22 */ DISPLAY_BANNER_CMD);
    printf("%25s - enable ethernet only service (poe untouched):  %s %s=<1..8>\r\n",               /*  23 */ ETH_ONLY_PORT_ENABLE_CMD,           ETH_ONLY_PORT_ENABLE_CMD,           CLI_ARG_PORT_STRING);
    printf("%25s - disable ethernet only service (poe untouched): %s %s=<1..8>\r\n",               /*  24 */ ETH_ONLY_PORT_DISABLE_CMD,          ETH_ONLY_PORT_DISABLE_CMD,          CLI_ARG_PORT_STRING);
    printf("%25s - read primary ALU by index: %s %s=<1..4096>\r\n",                                /*  25 */ READ_ALU_PRI_BY_INDEX_CMD,          READ_ALU_PRI_BY_INDEX_CMD,          CLI_ARG_INDEX_STRING);
    printf("%25s - read primary ALU by range: %s %s=<1..4096> %s=<1..4096>\r\n",                   /*  26 */ READ_ALU_PRI_RANGE_CMD,             READ_ALU_PRI_RANGE_CMD,             CLI_ARG_START_STRING,
                                                                                                             CLI_ARG_END_STRING);
    printf("%25s - read primary ALU with MAC-AS-INDEX: %s %s=11:22:33:44:55:66\r\n",               /*  27 */ READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD, READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD, CLI_ARG_MAC_STRING);
    printf("%25s - read secondary ALU DYNAMIC by index: %s %s=<0..1023>\r\n",                      /*  28 */ READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD,  READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD,  CLI_ARG_INDEX_STRING);
    printf("%25s - read secondary ALU by range: %s %s=<0..1024> %s=<1..1024>\r\n",                 /*  29 */ READ_ALU_SEC_RANGE_CMD,             READ_ALU_SEC_RANGE_CMD,             CLI_ARG_START_STRING,
                                                                                                             CLI_ARG_END_STRING);
    printf("%25s - mac resolution param update: %s %s=<1..4095> %s=<%s|%s|%s|%s> %s=<100>\r\n",    /*  30 */ ALU_MAC_RESO_PARAM_CMD,             ALU_MAC_RESO_PARAM_CMD,             CLI_ARG_LOOP_LIMIT_STRING,
                                                                                                             CLI_ARG_ALU_ACTION_STRING,          CLI_ARG_NO_OP_STRING,               CLI_ARG_WRITE_STRING,
                                                                                                             CLI_ARG_READ_STRING,                CLI_ARG_SEARCH_STRING,
                                                                                                             CLI_ARG_WAIT_ACTION_STRING);
    printf("%25s - read display user signature in FLASH\r\n",                                      /*  31 */ USER_SIGNATURE_READ_FLASH_CMD);
    printf("%25s - read display user signature in RAM\r\n",                                        /*  32 */ USER_SIGNATURE_READ_RAM_CMD);
    printf("%25s - write user signature: %s %s=%s %s=%s %s=%s\r\n",                                /*  33 */ USER_SIGNATURE_WRITE_CMD,  USER_SIGNATURE_WRITE_CMD, CLI_ARG_SERIAL_NUMBER_STRING,
                                                                                                             AMU_SERIAL_NUMBER_DEFAULT, CLI_ARG_PRODUCT_STRING,   AMU_ATIRA_PRODUCT_DEFAULT,
                                                                                                             CLI_ARG_MODEL_STRING,      AMU_MODEL_DEFAULT);
    printf("%25s - erase RAM and FLASH signature\r\n",                                             /*  34 */ USER_SIGNATURE_ERASE_CMD);
    printf("%25s - read MAC address for both PRIMARY and secondary KSZ ethernet controllers\r\n",  /*  35 */ READ_BOTH_KSZ_MAC_CMD);
    /*  36    SET_LOCAL_TESTING*/
    printf("%25s - display POE high-level info (json format)\r\n",                                 /*  37 */ POE_GENERAL_INFO_CMD);
    printf("%25s - user read POE controller input voltage\r\n",                                    /*  38 */ POE_CONTROLLER_INPUT_VOLTAGE_CMD);
    printf("%25s - reset ethernet controller: %s %s=<%s|%s|%s>\r\n",                               /*  39 */ ETHERNET_CONTROLLER_RESET_CMD, ETHERNET_CONTROLLER_RESET_CMD, CLI_ARG_CONTROLLER_STRING,
                                                                                                             CLI_ARG_PRIMARY_STRING, CLI_ARG_SECONDARY_STRING, CLI_ARG_BOTH_STRING);
    printf("%25s - display current temperature reading\r\n",                                       /*  40 */ TEMPERATURE_CMD);
    printf("%25s - display temperature and fin info\r\n",                                          /*  41 */ TEMP_FAN_INFO_CMD);
    printf("%25s - fan info read. Example: %s <%s|%s|%s|%s>\r\n",                                  /*  42 */ FAN_INFO_CMD, FAN_INFO_CMD,        CLI_ARG_READ_FAN_MFR_ID_STRING, CLI_ARG_READ_FAN_VERSION_ID_STRING,
                                                                                                             CLI_ARG_READ_FAN_STATUS_ID_STRING, CLI_ARG_READ_FAN_SPEED_STRING);
    printf("%25s - set fan speed to %s|%s|%s: %s %s=%s %s=<%s|%s|%s>\r\n",                         /*  43 */ FAN_TASTIC_CMD,                 CLI_ARG_FAN_SPEED_OFF_STRING, CLI_ARG_FAN_SPEED_LOW_STRING,
                                                                                                             CLI_ARG_FAN_SPEED_MAX_STRING,   FAN_TASTIC_CMD,               CLI_ARG_ACTION_STRING,
                                                                                                             CLI_ARG_WRITE_FAN_SPEED_STRING, CLI_ARG_SPEED_STRING,         CLI_ARG_FAN_SPEED_OFF_STRING,
                                                                                                             CLI_ARG_FAN_SPEED_LOW_STRING,   CLI_ARG_FAN_SPEED_MAX_STRING);
    printf("%25s - read the PCBA option jumpers\r\n",                                              /*  44 */ READ_PCBA_JUMPERS_CMD);
    printf("%25s - display the Apparent help\r\n",                                                 /*  45 */ DISPLAY_HELP_CMD);
    printf("%25s - enable ethernet service on the LAN port\r\n",                                   /*  46 */ LAN_PORT_ENABLE_CMD);
    printf("%25s - disable ethernet service on the LAN port\r\n",                                  /*  47 */ LAN_PORT_DISABLE_CMD);
    printf("%25s - display poe port power\r\n",                                                    /*  48 */ DISPLAY_POE_PORT_POWER_CMD);
    printf("%25s - display port info: %s %s=<1..8|LAN>\r\n",                                       /*  49 */ DISPLAY_PORT_INFO_CMD, DISPLAY_PORT_INFO_CMD, CLI_ARG_PORT_STRING);
    printf("%25s - get and display input and output contact closure info\r\n",                     /*  50 */ GET_CC_INFO_CMD);
    printf("%25s - ENABLE primary/secondary ethernet controller checking\r\n",                     /*  51 */ ENABLE_SWITCH_CHECKING_CMD);
    printf("%25s - DISABLE primary/secondary ethernet controller checking\r\n",                    /*  52 */ DISABLE_SWITCH_CHECKING_CMD);
    printf("%25s - ENABLE POE checking\r\n",                                                       /*  53 */ ENABLE_POE_CHECKING_CMD);
    printf("%25s - DISABLE POE checking\r\n",                                                      /*  54 */ DISABLE_POE_CHECKING_CMD);
    printf("%25s - POE read port event register: %s %s=<%s|%s|%s|%s|%s|%s>\r\n",                   /*  55 */ POE_READ_PORT_EVENT_REGISTER_CMD, POE_READ_PORT_EVENT_REGISTER_CMD,   CLI_ARG_POE_EVENT_EVENT_STRING,
                                                                                                             CLI_ARG_POE_EVENT_POWER_STRING,   CLI_ARG_POE_EVENT_DETECTION_STRING, CLI_ARG_POE_EVENT_FAULT_STRING,
                                                                                                             CLI_ARG_POE_EVENT_START_STRING,   CLI_ARG_POE_EVENT_SUPPLY_STRING,    CLI_ARG_POE_EVENT_ALL_STRING);
    printf("%25s - POE read supply/fault event register\r\n",                                      /*  56 */ POE_READ_SUPPLY_EVENT_REGISTER_CMD);
    return;
}


// displayCliHelpCommandGroup3
//
//
//
static void displayCliHelpCommandGroup3(void) {
    printf("CLI COMMAND GROUP 3 HELP:\r\n");
    printf("%25s - user read device ID from POE device (expected value: %02X)\r\n",                /*  57 */ POE_DEVICE_ID_CMD, TI_POE_CONTROLLER_DEV_ID_EXPECTED);
    printf("%25s - user read manufacturer ID from POE device (Expected value: %02X)\r\n",          /*  58 */ POE_MFR_ID_CMD,    TI_POE_CONTROLLER_MFR_ID_EXPECTED);
    printf("%25s - read any POE byte by command: %s %s=<HEX>\r\n",                                 /*  59 */ POE_READ_BYTE_CMD,  POE_READ_BYTE_CMD,  CLI_ARG_COMMAND_STRING);
    printf("%25s - read any POE word by command: %s %s=<HEX>\r\n",                                 /*  60 */ POE_READ_WORD_CMD,  POE_READ_WORD_CMD,  CLI_ARG_COMMAND_STRING);
    printf("%25s - write any POE byte by command: %s %s=<HEX> %s=<HEX>\r\n",                       /*  61 */ POE_WRITE_BYTE_CMD, POE_WRITE_BYTE_CMD, CLI_ARG_COMMAND_STRING, CLI_ARG_VALUE_STRING);
    printf("%25s - write any POE word by command: %s %s=<HEX> %s=<HEX>\r\n",                       /*  62 */ POE_WRITE_WORD_CMD, POE_WRITE_WORD_CMD, CLI_ARG_COMMAND_STRING, CLI_ARG_VALUE_STRING);
    printf("%25s - get a switch mib counter: %s %s=<1..8|LAN> %s=<hex MIB INDEX>\r\n",             /*  63 */ GET_SWITCH_MIB_COUNTER_CMD,   GET_SWITCH_MIB_COUNTER_CMD,   CLI_ARG_PORT_NUMBER_SWITCH_MIB_COUNTER_STRING,
                                                                                                             CLI_ARG_MIB_INDEX_SWITCH_MIB_COUNTER_STRING);
    printf("%25s - ENABLE MIB flush/freeze on all ports\r\n",                                      /*  64 */ ENABLE_MIB_FLUSH_FREEZE_CMD);
    printf("%25s - DISABLE MIB flush/freeze on all ports\r\n",                                     /*  65 */ DISABLE_MIB_FLUSH_FREEZE_CMD);
    printf("%25s - FLUSH all mib counters\r\n",                                                    /*  66 */ FLUSH_MIB_COUNTERS_CMD);
    printf("%25s - FREEZE all mib counters\r\n",                                                   /*  67 */ FREEZE_MIB_COUNTERS_CMD);
    printf("%25s - UN-FREEZE all mib counters\r\n",                                                /*  68 */ UNFREEZE_MIB_COUNTERS_CMD);
    printf("%25s - read SAME70 register address:           %s %s=<HEX>\r\n",                       /*  69 */ SAME70_READ_REG_CMD,              SAME70_READ_REG_CMD,              CLI_ARG_ADDR_STRING);
    printf("%25s - write to any SAME70 register:           %s %s=<HEX> %s=<HEX>\r\n",              /*  70 */ SAME70_WRITE_REG_CMD,             SAME70_WRITE_REG_CMD,             CLI_ARG_ADDR_STRING,
                                                                                                             CLI_ARG_VALUE_STRING);
    printf("%25s - read 1 BYTE ports 1..5 base address 0xNxxx: %s %s=<HEX>\r\n",                   /*  71 */ READ_5PORTS_BYTE_BY_ADDRESS_CMD,  READ_5PORTS_BYTE_BY_ADDRESS_CMD,  CLI_ARG_ADDR_STRING);
    printf("%25s - write 1 BYTE ports 1..5 base address 0xNxxx:%s %s=<HEX> %s=<HEX>\r\n",          /*  72 */ WRITE_5PORTS_BYTE_BY_ADDRESS_CMD, WRITE_5PORTS_BYTE_BY_ADDRESS_CMD, CLI_ARG_ADDR_STRING,
                                                                                                             CLI_ARG_VALUE_STRING);
    printf("%25s - read 1 WORD/16bits ports 1..5 base address 0xNxxx: %s %s=<HEX>\r\n",            /*  73 */ READ_5PORTS_WORD_BY_ADDRESS_CMD,  READ_5PORTS_WORD_BY_ADDRESS_CMD,  CLI_ARG_ADDR_STRING);
    printf("%25s - write 1 WORD/16bits ports 1..5 base address 0xNxxx: %s %s=<HEX> %s=<HEX>\r\n",  /*  74 */ WRITE_5PORTS_WORD_BY_ADDRESS_CMD, WRITE_5PORTS_WORD_BY_ADDRESS_CMD, CLI_ARG_ADDR_STRING,
                                                                                                             CLI_ARG_VALUE_STRING);
    printf("%25s - ENABLE the PRIMARY switch 25Mhz clock output\r\n",                              /*  75 */ ENABLE_25MHZ_CLOCK_OUTPUT_CMD);
    printf("%25s - DISABLE the PRIMARY switch 25Mhz clock output\r\n",                             /*  76 */ DISABLE_25MHZ_CLOCK_OUTPUT_CMD);
    printf("%25s - read byte PRIMARY controller by address:          %s %s=<HEX>\r\n",             /*  77 */ READ_PRI_BYTE_BY_ADDRESS_CMD,     READ_PRI_BYTE_BY_ADDRESS_CMD,     CLI_ARG_ADDR_STRING);
    printf("%25s - write byte PRIMARY controller by address:         %s %s=<HEX> %s=Y\r\n",        /*  78 */ WRITE_PRI_BYTE_BY_ADDRESS_CMD,    WRITE_PRI_BYTE_BY_ADDRESS_CMD,    CLI_ARG_ADDR_STRING,
                                                                                                             CLI_ARG_VALUE_STRING);
    printf("%25s - read WORD/16bit PRIMARY controller by address:    %s %s=<HEX>\r\n",             /*  79 */ READ_PRI_WORD_BY_ADDRESS_CMD,     READ_PRI_WORD_BY_ADDRESS_CMD,     CLI_ARG_ADDR_STRING);
    printf("%25s - write WORD/16bit PRIMARY controller by address:   %s %s=<HEX> %s=Y\r\n",        /*  80 */ WRITE_PRI_WORD_BY_ADDRESS_CMD,    WRITE_PRI_WORD_BY_ADDRESS_CMD,    CLI_ARG_ADDR_STRING,
                                                                                                             CLI_ARG_VALUE_STRING);
    printf("%25s - read byte SECONDARY ports by address:             %s %s=<HEX>\r\n",             /*  81 */ READ_SEC_BYTE_ADDRESS_CMD,        READ_SEC_BYTE_ADDRESS_CMD,        CLI_ARG_ADDR_STRING);
    printf("%25s - read byte SECONDARY ports 1..4 by base address:   %s %s=<HEX>\r\n",             /*  82 */ READ_SEC_4PORTS_BY_BASE_ADDR_CMD, READ_SEC_4PORTS_BY_BASE_ADDR_CMD, CLI_ARG_ADDR_STRING);
    printf("%25s - write byte SECONDARY by address:                  %s %s=<HEX> %s=Y\r\n",        /*  83 */ WRITE_SEC_BYTE_BY_ADDRESS_CMD,    WRITE_SEC_BYTE_BY_ADDRESS_CMD,    CLI_ARG_ADDR_STRING,
                                                                                                             CLI_ARG_VALUE_STRING);
    printf("%25s - read USART0 mode and status registers\r\n",                                     /*  84 */ UART0_READ_MODE_STATUS_CMD);
    printf("%25s - read USART2 mode and status registers\r\n",                                     /*  85 */ USART2_READ_MODE_STATUS_CMD);
    printf("%25s - reset USART0 or USART2 status register: %s %s=<USART0|USART2>\r\n",             /*  86 */ USART_RESET_STATUS_CMD,           USART_RESET_STATUS_CMD,           CLI_ARG_UART_STRING);
    printf("%25s - set RGB LED RED   color to medium\r\n",                                         /*  87 */ RGB_LED_RED_MEDIUM_CMD);
    printf("%25s - set RGB LED RED   color to off\r\n",                                            /*  88 */ RGB_LED_RED_OFF_CMD);
    printf("%25s - set RGB LED RED   color to brilliant\r\n",                                      /*  89 */ RGB_LED_RED_BRILLIANT_CMD);
    printf("%25s - set RGB LED GREEN color to medium\r\n",                                         /*  90 */ RGB_LED_GREEN_MEDIUM_CMD);
    printf("%25s - set RGB LED GREEN color to off\r\n",                                            /*  91 */ RGB_LED_GREEN_OFF_CMD);
    printf("%25s - set RGB LED GREEN color to brilliant\r\n",                                      /*  92 */ RGB_LED_GREEN_BRILLIANT_CMD);
    printf("%25s - set RGB LED BLUE  color to medium\r\n",                                         /*  93 */ RGB_LED_BLUE_MEDIUM_CMD);
    printf("%25s - set RGB LED BLUE  color to off\r\n",                                            /*  94 */ RGB_LED_BLUE_OFF_CMD);
    printf("%25s - set RGB LED BLUE  color to brilliant\r\n",                                      /*  95 */ RGB_LED_BLUE_BRILLIANT_CMD);
    printf("%25s - enable flashing of RGB LEDs: %s %s=<%s|%s|%s|%s> %s=<msecs> %s=<msecs>\r\n",    /*  96 */ RGB_LED_ENABLE_CMD,         RGB_LED_ENABLE_CMD,       CLI_ARG_RGB_COLOR_STRING, CLI_ARG_BLUE_COLOR_STRING,
                                                                                                             CLI_ARG_GREEN_COLOR_STRING, CLI_ARG_RED_COLOR_STRING, CLI_ARG_ALL_COLOR_STRING, CLI_ARG_ON_PERIOD_STRING,
                                                                                                             CLI_ARG_OFF_PERIOD_STRING);
    printf("%25s - disable the flashing of BLUE/GREEN/RED/ALL RGB LEDs\r\n",                       /*  97 */ RGB_LED_DISABLE_CMD);

    return;
}


// displayCliHelpCommandGroup4
//
//
//
static void displayCliHelpCommandGroup4(void) {
    printf("CLI COMMAND GROUP 4 HELP:\r\n");
    printf("%25s - enable craft debug boolean\r\n",                                                /*  98 */ ENABLE_CRAFT_DEBUG_OUTPUT_CMD);
    printf("%25s - disable craft debug boolean\r\n",                                               /*  99 */ DISABLE_CRAFT_DEBUG_OUTPUT_CMD);
    printf("%25s - enable POE debug boolean\r\n",                                                  /* 100 */ ENABLE_POE_DEBUG_OUTPUT_CMD);
    printf("%25s - disable POE debug boolean\r\n",                                                 /* 101 */ DISABLE_POE_DEBUG_OUTPUT_CMD);
    printf("%25s - enable I2C debug boolean\r\n",                                                  /* 102 */ ENABLE_I2C_FULL_OUTPUT_CMD);
    printf("%25s - disable I2C debug boolean\r\n",                                                 /* 103 */ DISABLE_I2C_FULL_OUTPUT_CMD);
    printf("%25s - enable upgrade info debug boolean\r\n",                                         /* 104 */ ENABLE_UPGRADE_INFO_CMD);
    printf("%25s - disable upgrade info debug boolean\r\n",                                        /* 105 */ DISABLE_UPGRADE_INFO_CMD);
    printf("%25s - enable upgrade HEX info debug boolean\r\n",                                     /* 106 */ ENABLE_UPGRADE_HEX_INFO_CMD);
    printf("%25s - disable upgrade HEX info debug boolean\r\n",                                    /* 107 */ DISABLE_UPGRADE_HEX_INFO_CMD);
    printf("%25s - upgrade testing: drop the next incoming data packet\r\n",                       /* 108 */ DROP_UPGRADE_DATA_PACKET_CMD);
    printf("%25s - upgrade testing: drop the next outgoing ACK packet\r\n",                        /* 109 */ DROP_UPGRADE_PACKET_ACK_CMD);
    printf("%25s - print the flash holding buffer\r\n",                                            /* 110 */ PRINT_FH_CMD);
    printf("%25s - write a pattern to flash holding buffer:   %s %s=12345678\r\n",                 /* 111 */ SET_FH_PATTERN_CMD,    SET_FH_PATTERN_CMD,    CLI_ARG_LONG_STRING);
    printf("%25s - read 1 block from flash to holding buffer: %s %s=40000\r\n",                    /* 112 */ READ_FLASH_BLOCK_CMD,  READ_FLASH_BLOCK_CMD,  CLI_ARG_ADDR_STRING);
    printf("%25s - write 1 block of holding buffer to flash:  %s %s=40000\r\n",                    /* 113 */ WRITE_FLASH_BLOCK_CMD, WRITE_FLASH_BLOCK_CMD, CLI_ARG_ADDR_STRING);
    printf("%25s - calculate the CRC over the flash holding block\r\n",                            /* 114 */ CRC_FLASH_BLOCK_CMD);
    printf("%25s - test/check if flash memory range is erased: %s %s=20000 %s=10\r\n",             /* 115 */ IS_FLASH_RANGE_ERASED_CMD, IS_FLASH_RANGE_ERASED_CMD, CLI_ARG_ADDR_STRING, CLI_ARG_LENGTH_STRING);
    printf("%25s - test/read up to 32 longs from flash: %s %s=20000 %s=10\r\n",                    /* 116 */ READ_FLASH_LONG_CMD,       READ_FLASH_LONG_CMD,       CLI_ARG_ADDR_STRING, CLI_ARG_LENGTH_STRING);
    printf("%25s - test/write 1 long to flash: %s %s=20000 %s=E0B2B315\r\n",                       /* 117 */ WRITE_FLASH_LONG_CMD,      WRITE_FLASH_LONG_CMD,      CLI_ARG_ADDR_STRING, CLI_ARG_LONG_STRING);
    printf("%25s - initiate software reset\r\n",                                                   /* 118 */ SOFTWARE_RESET_CMD);
    printf("%25s - for test: infinite nop ... watchdog\r\n",                                       /* 119 */ NOP_CMD);
    printf("%25s - for test: disable the watchdog\r\n",                                            /* 120 */ WATCHDOG_DISABLE_TEST_CMD);
    return;
}


/**
 * Apparent
 * displayEndOfOutput: for the SBC/Gateway to detect end-of-output. This function appends the
 *                     checksum and number of bytes over which the checksum is calculated.
 *
 */
void displayEndOfOutput(void)
{
    if (inputChecksumRequired) { printf("%s 0x%08lX %d\r\n", END_OF_OUTPUT_SIGNATURE_STRING, amuOutputChecksum, charsWrittenByInterrupt); }
    charsWrittenByInterrupt = 0;
    amuOutputChecksum       = 0;
}


/**
 * Apparent: isInputChecksumValid
 *
 * User input at the SBC-COM interface must have a trailing checksum signature consisting of a
 * "END-OF-INPUT" set of characters, following by the simple checksum value of the user's input
 * (0x12345678 format) and by the number of characters (decimal) over which the checksum is to be
 * evaluated.
 *
 * Example of what a command might look like:
 *
 * beamup scotty other stuff END-OF-INPUT 0x000009CA 25
 *                           ^                        ^
 *                           | <----  signature  ---->|
 *
 *
 */
static BOOL isInputChecksumValid(char *pUserInput) {
    BOOL  checksumValid = FALSE;
    char *pSignature;
    char  pChecksumString[10];
    char *pNumChecksumDigits;
    U16   numChecksumDigits;
    U32   extractedCheckSum;
    U32   calculatedChecksum;
    U32   iggy;
    U32   checksumRank;

    // Secret back door to get in the front door. For localized testing.
    if (strncmp(pUserInput, SET_LOCAL_TESTING_CMD, strlen(SET_LOCAL_TESTING_CMD)) == 0) { toggleChecksumRequired(); return TRUE; }

    // Otherwise, input checksum needs to be checked.
    if (inputChecksumRequired) {
        pSignature = pApparent_strstr(pUserInput, (char *)&END_OF_INPUT_SIGNATURE_STRING);

        if (pSignature == NULL) {
            // Signature is missing altogether.
            incrementBadEventCounter(MISSING_END_OF_INPUT_SIGNATURE);
        } else {
            // pSignature now points to the 1st character after END-OF-INPUT which typically is a "blank".
            //
            // Parse the remaining section of the command for the checksum and number of chars.
            // Apply the following criteria:
            //   - [0,1] characters must be 0x
            //   - [2,3,4,5,6,7,8,9] are expected to be HEX digits
            //   - [10] must be a space
            //   - [11,12] is an integer: number of characters in the user-portion of the string, so exclude the end-of-input signature. 
            // beyond the END-OF-INPUT signature: 0x000009CA 25
            //                                    01234567890123
            // Start by evaluating the rank of the presumed "0" character of the checksum in pSignature:
            checksumRank = strlen(END_OF_INPUT_SIGNATURE_STRING) + 1; // checksumRank now indexes the "0" of the checksum string
            if ((char)pSignature[checksumRank]      == '0' &&
                (char)pSignature[checksumRank + 1]  == 'x' &&
                (char)pSignature[checksumRank + 10] == ' ') {
                // The [0]/[1] chars are "0" and "x" and the [10] char is a space, so far so good.
                // Extract the digits that form the actual checksum. Advance the pointer.
                // If the user passed garbage, the checksum will not compute, the command
                // is dropped.
                memset(&pChecksumString[0], 0, 10);                            // clear the checksum-only string
                memcpy(&pChecksumString[0], &pSignature[checksumRank + 2], 8); // copy the hex digits only 
                extractedCheckSum  = strtol(pChecksumString, NULL, 16);
                pNumChecksumDigits = &pSignature[checksumRank + 11];
                numChecksumDigits  = atoi(pNumChecksumDigits);

                // Now calculate the checksum from the user's input over the prescribed number of characters.
                calculatedChecksum = 0;
                for (iggy = 0; iggy < numChecksumDigits; iggy++) {
                    calculatedChecksum += pUserInput[iggy];
                }
                if (calculatedChecksum == extractedCheckSum) {
                    checksumValid = TRUE;
                } else {
                    incrementBadEventCounter(INPUT_CHECKSUM_GENERAL_ERROR);
                }
            } else {
                incrementBadEventCounter(INVALID_END_OF_INPUT_FORMAT);
            }
        }
    } else {
        // Input checksum not required. Typically the case when the CLI input interface is a user directly
        // communicating to the AMU over, say a teraterm connection. If the connection to the SBC/Gateway
        // controller is re-established, the interface remains broken until the command is re-entered which
        // must be entered but without the END_OF_INPUT tag - and that's not going to happen. Or: look for
        // the END-OF_INPUT tag right here and now: if it is detected, then restore this setting to its default.
        pSignature = pApparent_strstr(pUserInput, (char *)&END_OF_INPUT_SIGNATURE_STRING);
        if (pSignature == NULL) { /* nothing to do */                           }
        else                    { /* restore:      */ toggleChecksumRequired(); }
        checksumValid = TRUE;
    }
    return checksumValid;
}


//
// configureEthernetSubsystem
//
// There are 2 ethernet controllers: - a primary controller supporting the SBC-LAN and downstream ports 1,2,3,4,
//                                   - and a secondary controller supporting downstream ports 5,6,7,8.
//
// Each are treated as 2 different beasts: they have different read-write interfaces, different register sets
// for similar data. Each has its own control data block to be managed, and each must be configured separately.
//
void configureEthernetSubsystem(void)
{
    mdelay(1000); // delay to allow the 2 ethernet chip controllers to come up.

    // Initialize the data for the primary switch controller, and configure it.
    initPrimarySwitchData();
    configurePrimarySwitchController();

    // Initialize the data for the secondary switch controller, and configure it.
    initSecondarySwitchData();
    configureSecondarySwitchController();

}


static BOOL isEthernetCheckingEnabled(void)
{
    if (portCheckingSmCb.enabled) return TRUE;
    else                          return FALSE;
}


static void setEthernetCheckingEnabled(BOOL enable)
{
    portCheckingSmCb.enabled = enable;
}


/**
 * checkEthernetPortsSm
 *
 * A mini state machine that determines the state of its universe. As with the check-all-ports-in-1-shot function
 * above, first determine if the controlling timeout has expired before moving out of the idle state. Once the SM
 * leaves the IDLE state, then:
 *
 * - check 1 port of the primary controller
 * - sets up the next port of the primary controller before returning to main
 * - when all ports of the primary controller have been checked, set the SM to check the ports on the secondary controller
 * - do likewise checking 1 port of the secondary controller before returning to main
 * - when all ports of the secondary controller have been checked, set the SM to check the overall ethernet service1
 * - check the overall ethernet service bit
 * - then return to IDLE
 *
 */
void checkEthernetPortsSm(void)
{
    switch (portCheckingSmCb.state) {
        case ETH_SM_START:
            if (portCheckingSmCb.enabled) {
                // Can now go to the idle state and do real work.
                sprintf(&pCraftPortDebugData[0], "ETH: AMU now managed, ethernet checking enabled\r\n"); UART1_WRITE_PACKET_MACRO
                portCheckingSmCb.state = ETH_SM_IDLE;
            } else {} // not managed - staying this state until managed.
            break;

        case ETH_SM_IDLE:
            if ((g_ul_ms_ticks - portCheckingSmCb.lastCheckTick) > CHECK_ETHERNET_PORTS_TIMEOUT) {
                // Get out of bed, get to work. Prepare to check the primary controller ports.
                portCheckingSmCb.state              = ETH_SM_PRIMARY_CONTROL_REGISTER;
                portCheckingSmCb.primaryNextIndex   = 0;
            }
            break;
        case ETH_SM_PRIMARY_CONTROL_REGISTER:
            // Check the control register of a single port on the primary controller, and setup to check the next port.
            checkPrimaryEthernetPortControlRegister(portCheckingSmCb.primaryNextIndex);
            portCheckingSmCb.primaryNextIndex++;
            if (portCheckingSmCb.primaryNextIndex > PRIMARY_PORT_INDEX_MAX) {
                // Done with the control registers of the primary controller. Prepare to check the status registers of the primary controller.
                portCheckingSmCb.primaryNextIndex   = 0;
                portCheckingSmCb.state              = ETH_SM_PRIMARY_STATUS_REGISTER;
            }
            break;
        case ETH_SM_PRIMARY_STATUS_REGISTER:
            // Check the status register of a single port on the primary controller, and setup to check the next port.
            checkPrimaryEthernetPortStatusRegister(portCheckingSmCb.primaryNextIndex);
            portCheckingSmCb.primaryNextIndex++;
            if (portCheckingSmCb.primaryNextIndex > PRIMARY_PORT_INDEX_MAX) {
                // Done with the control registers of the primary controller. Prepare to check the status registers of the primary controller.
                portCheckingSmCb.secondaryNextIndex = 0;
                portCheckingSmCb.state              = ETH_SM_SECONDARY_CONTROL_REGISTER_02;
            }
            break;
        case ETH_SM_SECONDARY_CONTROL_REGISTER_02:
            // Check a single port on the secondary controller, and setup to check the next port.
            checkSecondaryControlReg_02(portCheckingSmCb.secondaryNextIndex);
            portCheckingSmCb.secondaryNextIndex++;
            if (portCheckingSmCb.secondaryNextIndex > SECONDARY_PORT_INDEX_MAX) {
                // Done with the control registers "02" of the secondary controller. Prepare to check control register "09" of the secondary controller.
                portCheckingSmCb.secondaryNextIndex = 0;
                portCheckingSmCb.state              = ETH_SM_SECONDARY_CONTROL_REGISTER_09;
            }
            break;
        case ETH_SM_SECONDARY_CONTROL_REGISTER_09:
            // Check a single port on the secondary controller, and setup to check the next port.
            checkSecondaryControlReg_09(portCheckingSmCb.secondaryNextIndex);
            portCheckingSmCb.secondaryNextIndex++;
            if (portCheckingSmCb.secondaryNextIndex > SECONDARY_PORT_INDEX_MAX) {
                // Done with the control registers "09" of the secondary controller. Prepare to check control register "10" of the secondary controller.
                portCheckingSmCb.secondaryNextIndex = 0;
                portCheckingSmCb.state              = ETH_SM_SECONDARY_CONTROL_REGISTER_10;
            }
            break;
        case ETH_SM_SECONDARY_CONTROL_REGISTER_10:
            // Check a single port on the secondary controller, and setup to check the next port.
            checkSecondaryControlReg_10(portCheckingSmCb.secondaryNextIndex);
            portCheckingSmCb.secondaryNextIndex++;
            if (portCheckingSmCb.secondaryNextIndex > SECONDARY_PORT_INDEX_MAX) {
                // Done with the control registers "10" of the secondary controller. Prepare to check control register "11" of the secondary controller.
                portCheckingSmCb.secondaryNextIndex = 0;
                portCheckingSmCb.state              = ETH_SM_SECONDARY_CONTROL_REGISTER_11;
            }
            break;
        case ETH_SM_SECONDARY_CONTROL_REGISTER_11:
            // Check a single port on the secondary controller, and setup to check the next port.
            checkSecondaryControlReg_11(portCheckingSmCb.secondaryNextIndex);
            portCheckingSmCb.secondaryNextIndex++;
            if (portCheckingSmCb.secondaryNextIndex > SECONDARY_PORT_INDEX_MAX) {
                 // Done with the control registers "11" of the secondary controller. Prepare to check the status registers of the secondary controller.
                portCheckingSmCb.secondaryNextIndex = 0;
                portCheckingSmCb.state              = ETH_SM_SECONDARY_STATUS_REGISTER;
            }
            break;
        case ETH_SM_SECONDARY_STATUS_REGISTER:
            // Check 1 port on the secondary controller, and setup to check the next port.
            checkSecondaryStatusRegisters(portCheckingSmCb.secondaryNextIndex);
            portCheckingSmCb.secondaryNextIndex++;
            if (portCheckingSmCb.secondaryNextIndex > SECONDARY_PORT_INDEX_MAX) {
                // Done with the secondary controller ports. Prepare to check the overall ethernet service bit.
                portCheckingSmCb.state              = ETH_SM_PRIMARY_ETH_SERVICE;
            }
            break;
        case ETH_SM_PRIMARY_ETH_SERVICE:
            // Check the overall ethernet service bit of the primary controller.
            checkPrimaryEthernetService();
            portCheckingSmCb.state                  = ETH_SM_SECONDARY_ETH_SERVICE;
            break;
        case ETH_SM_SECONDARY_ETH_SERVICE:
            // Check the overall ethernet service bit of the secondary controller, and prepare to return to IDLE.
            checkSecondaryEthernetService();
            portCheckingSmCb.state                  = ETH_SM_IDLE;
            portCheckingSmCb.primaryNextIndex       = 0;
            portCheckingSmCb.secondaryNextIndex     = 0;
            portCheckingSmCb.lastCheckTick          = g_ul_ms_ticks; // For the next time in this state.
            break;
        default:
            // Coding error. Increment a counter.
            sprintf(&pCraftPortDebugData[0], "PORT CHECK SM ERROR: STATE=0x%lX\r\n", portCheckingSmCb.state); UART1_WRITE_PACKET_MACRO
            incrementBadEventCounter(CHECK_ETHERNET_PORTS_SM_CODING_ERROR);
            portCheckingSmCb.state = ETH_SM_IDLE;
            break;
    }

    pokeWatchdog();

}


// findComponentStringData
//
// A service routine to support CLI. Given a user input argument string, this routine will:
//
// - search for the occurrence of the specified component string
// - if the component string is found, write what follows it to the output string
//
// Example:
//
// - given a user input argument string, say:    sn=456789
//
// - make the function call:     findComponentStringData(CLI_ARG_SERIAL_NUMBER_STRING, pOutputString, pArguments);
//
// - where CLI_ARG_SERIAL_NUMBER_STRING is defined as:    const char CLI_ARG_SERIAL_NUMBER_STRING[] = "sn";
//
// - then: - the string data for component "sn" is found
//         - ensure the = follows the string "sn"
//         - find the rest of the string that follows it up to the next blank space or NULL CHAR
//         - the string   randy_take_1   is found and written to pOutputString
//
// The pOutputString parameter that is passed is expected to have been pre-initialized by the user with NULL CHAR.
// If the component is not found, this function simply returns; it is up to the user to check if pOutputString
// string length is zero (component not found) or greater than 0/zero.
//
void findComponentStringData(const char *pComponent, char *pOutputString, char *pArguments)
{
    U16   argumentLength;
    U16   componentLength;
    U16   iggy;
    U16   pop;
    char *pValueLocateString;
    char  someChar;
    BOOL  endOfValueFound;

    componentLength = strlen(pComponent);
    argumentLength  = strlen(pArguments);

    for (iggy = 0; iggy < argumentLength; iggy++) {

        if (iggy == (argumentLength + 1)) { /* printf("GONE TOO FAR\r\n"); */ break; }  // <- gone too far without finding the component

        if (strncmp(pComponent, &pArguments[iggy], componentLength) == 0) {
            // Found the desired argument. Locate the = character, and locate the string value of this argument.
            pValueLocateString = &pArguments[iggy + componentLength];
            // This char has to be the = character.
            if (pValueLocateString[0] == '=') {
                // Extract the remaining set of characters up to the next empty space or end of string.
                // Point to the next character after the = character, then locate the
                // rank of the next space or end-of-string.
                pValueLocateString++;
                pop = 0;
                endOfValueFound = FALSE;
                while (!endOfValueFound) {
                    someChar = pValueLocateString[pop++];
                    if (someChar == ' ' || someChar == 0) {
                        // Found it.
                        endOfValueFound = TRUE;
                    } else {
                        if (pop >= argumentLength) {
                            // Went too far.
                            pop = 0;
                            endOfValueFound = TRUE;
                        } else {
                            // A character that will be copied. Is the argument itself too long?
                            if (pop >= AMU_COMPONENT_DATA_SIZE_MAX) {
                                // The component is too long.
                                pop = 0;
                                endOfValueFound = TRUE;
                            }
                        }
                    }
                } // while ...
                if (pop > 0) {
                    // Copy over the value string.
                    memcpy(pOutputString, pValueLocateString, pop-1);
                } else {
                    // Nothing of value found, nothing written to the output buffer.
                }
            } else {
                // The = character was expected, not found. Invalid user input format.
                break;
            }
            break; // Out of for loop
        } else {
            // Not a string match - keep searching.
        }
    } // for ...
        
}


static void readPrimaryByteByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    someAddress;
    U8     someByte;

    pArguments = &pUserInput[strlen(READ_PRI_BYTE_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &someAddress)) {
            someByte = readByteFromPrimaryByAddress(someAddress);
            printf("READ FROM PRIMARY ADDRESS 0x%04lX = 0x%02X\r\n", someAddress, someByte);
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void writePrimaryByteByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pValueString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    someAddress;
    U8     someByte;

    pArguments = &pUserInput[strlen(WRITE_PRI_BYTE_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pValueString,   0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValueString,   pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else if (strlen(pValueString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &someAddress)) {
            someByte = atoh(pValueString);
            printf("WILL WRITE 0x%02X TO PRIMARY SWITCH ADDRESS 0x%08lX\r\n", someByte, someAddress);
            writeByteToPrimaryByAddress(someAddress, someByte);
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void readPrimaryWordByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    someAddress;
    U8     pSomeByte[2];
    U16    someWord;

    pArguments = &pUserInput[strlen(READ_PRI_WORD_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &someAddress)) {
            pSomeByte[0] = readByteFromPrimaryByAddress(someAddress);
            pSomeByte[1] = readByteFromPrimaryByAddress(someAddress+1);
            someWord     = (pSomeByte[0] << 8) + pSomeByte[1];
            printf("READ FROM PRIMARY ADDRESS 0x%04lX = 0x%04X\r\n", someAddress, someWord);
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void writePrimaryWordByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pValueString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    someAddress;
    U16    someWord;

    pArguments = &pUserInput[strlen(WRITE_PRI_WORD_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pValueString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValueString,   pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else if (strlen(pValueString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &someAddress)) {
            someWord = atoh(pValueString);
            printf("WILL WRITE 0x%04X TO PRIMARY SWITCH ADDRESS 0x%08lX\r\n", someWord, someAddress);
            writeWordToPrimaryByAddress(someAddress, someWord);
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void read5PortsByteByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    baseAddress;
    U32    readAddress;
    U8     someByte;
    U16    iggy;

    pArguments = &pUserInput[strlen(READ_5PORTS_BYTE_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &baseAddress)) {
            printf("READ 1 BYTE/5PORTS BASE ADDR 0xN%03lX:\r\n", baseAddress);
            for (iggy = 1; iggy <= NUMBER_PRIMARY_SWITCH_PORTS; iggy++) {
                readAddress = (0x1000 * iggy) + baseAddress; someByte = readByteFromPrimaryByAddress(readAddress); printf("REG ADDR 0x%04lX = 0x%02X\r\n", readAddress, someByte);
            }
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void write5PortsByteByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pValueString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    baseAddress;
    U32    writeAddress;
    U8     someByte;
    U16    iggy;

    pArguments = &pUserInput[strlen(WRITE_5PORTS_BYTE_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pValueString,   0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValueString,   pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else if (strlen(pValueString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &baseAddress)) {
            someByte = atoh(pValueString);
            printf("WRITE 1 BYTE/5PORTS 0x%02X BASE ADDR 0xN%03lX:", someByte, baseAddress);
            for (iggy = 1; iggy <= NUMBER_PRIMARY_SWITCH_PORTS; iggy++) {
                writeAddress = (0x1000 * iggy) + baseAddress;
                writeByteToPrimaryByAddress(writeAddress, someByte);
                printf(" 0x%04lX", writeAddress);
            }
            printf("\r\n");
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void read5PortsWordByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    baseAddress;
    U32    readAddress;
    U8     byte1, byte2;
    U16    someWord;
    U16    iggy;

    pArguments = &pUserInput[strlen(READ_5PORTS_WORD_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &baseAddress)) {
            printf("READ 1 WORD/5PORTS BASE ADDR 0xN%03lX:\r\n", baseAddress);
            for (iggy = 1; iggy <= NUMBER_PRIMARY_SWITCH_PORTS; iggy++) {
                readAddress = (0x1000 * iggy) + baseAddress;
                byte1    = readByteFromPrimaryByAddress(readAddress);
                byte2    = readByteFromPrimaryByAddress(readAddress+1);
                someWord = (byte1 << 8) + byte2;
                printf("REG ADDR 0x%04lX/%04lX = %04X\r\n", readAddress, readAddress+1, someWord);
            }
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void write5PortsWordByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pValueString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    baseAddress;
    U32    writeAddress;
    U16    someWord;
    U16    iggy;

    pArguments = &pUserInput[strlen(WRITE_5PORTS_WORD_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pValueString,   0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValueString,   pArguments);

    if (strlen(pAddressString) == 0) {
        printf("address not found\r\n");
    } else if (strlen(pValueString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (newATOH(pAddressString, &baseAddress)) {
            someWord = atoh(pValueString);
            printf("WRITE 1 WORD/5PORTS 0x%04X BASE ADDR 0xN%03lX:", someWord, baseAddress);
            for (iggy = 1; iggy <= 5; iggy++) {
                writeAddress = (0x1000 * iggy) + baseAddress;
                writeWordToPrimaryByAddress(writeAddress, someWord);
                printf(" 0x%04lX", writeAddress);
            }
            printf("\r\n");
        } else {
            printf("***%s DID NOT CONVERT TO HEX\r\n", pAddressString);
        }
    }
}


static void readSecondaryByteAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U8     address;
    U8     oneByte;

    pArguments = &pUserInput[strlen(READ_SEC_BYTE_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        address = atoh(pAddressString);
        oneByte = readFromSecondarySwitchByAddress(address);
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:\"0x%02X\",%s:\"0x%02X\"}}}\r\n", CLI_STRING, COMMAND_STRING,   READ_SEC_BYTE_ADDRESS_CMD,
                                                                                                          ARGUMENTS_STRING, pArguments,
                                                                                                          CODE_STRING,      0,
                                                                                                          RESULT_STRING,    SUCCESS_STRING,
                                                                                                          RESPONSE_STRING,  ADDRESS_STRING, address,
                                                                                                                            VALUE_STRING,   oneByte);
    }
}


// writeSecondaryByteByAddress
//
//
//
//
static void writeSecondaryByteByAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pValueString[AMU_COMPONENT_DATA_SIZE_MAX];
    U8     address;
    U8     someByte;

    pArguments = &pUserInput[strlen(WRITE_SEC_BYTE_BY_ADDRESS_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pValueString,   0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING,  pAddressString, pArguments);
    findComponentStringData(CLI_ARG_VALUE_STRING, pValueString,   pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else if (strlen(pValueString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        address  = atoh(pAddressString);
        someByte = atoh(pValueString);
        writeToSecondaryByAddress(address, someByte);
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:\"0x%02X\",%s:\"0x%02X\"}}}\r\n", CLI_STRING, COMMAND_STRING,   WRITE_SEC_BYTE_BY_ADDRESS_CMD,
                                                                                                          ARGUMENTS_STRING, pArguments,
                                                                                                          CODE_STRING,      0,
                                                                                                          RESULT_STRING,    SUCCESS_STRING,
                                                                                                          RESPONSE_STRING,  ADDRESS_STRING, address,
                                                                                                                            VALUE_STRING,   someByte);
    }
}


// getPortKeyFromPortArg
//
// Check that the "port string" specifies a port in the "1..8" range or "LAN" (if LAN allowed).
//
U8 getPortKeyFromPortArg(char *pPortString, BOOL lanAllowed)
{
    U8 portKey = 0xFF;

    if (strncmp(pPortString, PORT_LAN_STRING, 3) == 0) {
        // The string contains 3 chars. Is it "LAN"?
        if (strncmp(pPortString, PORT_LAN_STRING, 3) == 0) { if (lanAllowed) { portKey = PORT_KEY_PORT_LAN; } }
    } else {
        // Any other valid port must be 1 char long, from "1" to "8"..
        if (strlen(pPortString) == 1) {
            if      (strncmp(pPortString, PORT_1_STRING,   1) == 0) { portKey = PORT_KEY_PORT_1; }
            else if (strncmp(pPortString, PORT_2_STRING,   1) == 0) { portKey = PORT_KEY_PORT_2; }
            else if (strncmp(pPortString, PORT_3_STRING,   1) == 0) { portKey = PORT_KEY_PORT_3; }
            else if (strncmp(pPortString, PORT_4_STRING,   1) == 0) { portKey = PORT_KEY_PORT_4; }
            else if (strncmp(pPortString, PORT_5_STRING,   1) == 0) { portKey = PORT_KEY_PORT_5; }
            else if (strncmp(pPortString, PORT_6_STRING,   1) == 0) { portKey = PORT_KEY_PORT_6; }
            else if (strncmp(pPortString, PORT_7_STRING,   1) == 0) { portKey = PORT_KEY_PORT_7; }
            else if (strncmp(pPortString, PORT_8_STRING,   1) == 0) { portKey = PORT_KEY_PORT_8; }
        }
    }
    return portKey;
}


static BOOL isPortKeyInList(U8 portKey, U8 *pPortKeyList)
{
    BOOL keyInList = FALSE;
    U16 iggy;
    for (iggy = 0; iggy < MAX_NUMBER_APPARENT_PORTS; iggy++) {
        if (portKey == pPortKeyList[iggy]) { keyInList = TRUE; break; }
    }
    return keyInList;
}


// getPortKeyListFromPortArg
//
// Parse a string containing a comma-separated list of ports, and extract the
// list of "port keys", where a port key is an internal number 0..8 for all
// 9 ports supported by both ethernet controllers. The string specification
// normally looks like:
//
// 1,2,LAN,6,7
//
// Rules: - no duplicates
//        - must be valid ports 1..8 or "LAN"
//        - cannot have more than 8 ports total
// Ports can be in any order.
// If any rules are violated, the port key list is wiped.
//
void getPortKeyListFromPortArg(char *pPortString, U8 *pPortKeyList)
{
    U16  iggy;
    BOOL keepSearching = TRUE;
    char pPortValue[3]; // 3 chars max to hold "LAN" when specified
    U16  charRank = 0;
    char thisChar[2];
    U16  portStringLength = strlen(pPortString);
    U32  portKey;
    U16  portKeyIndex = 0;
    BOOL badUserInput = FALSE;

    for (iggy = 0; iggy < MAX_NUMBER_APPARENT_PORTS; iggy++) { pPortKeyList[iggy] = 0xFF; }
    memset(&pPortValue[0], 0, 3);
    thisChar[1] = 0;

    while (keepSearching) {
        if (charRank == portStringLength) {
            // Found the last one. End of the line. I know: bad pun.
            if (strlen(pPortValue) > 0) {
                portKey = getPortKeyFromPortArg(pPortValue, LAN_NOK);
                if (portKey != 0xFF) {
                    if (isPortKeyInList(portKey, pPortKeyList) == FALSE) { pPortKeyList[portKeyIndex++] = portKey; }
                    else                                 /* DUPLICATE */ { badUserInput = TRUE;                    }
                } else {                
                    // Invalid port.
                    badUserInput = TRUE;
                }
            } else {
                // String was incorrectly terminated.
                badUserInput = TRUE;
            }
            break;
        } else {
            thisChar[0] = pPortString[charRank++];
            // Check this 1 char - is it a comma?
            if (thisChar[0] == ',') {
                // Found the comma, so evaluate the "port as string" collected up to now.
                portKey = getPortKeyFromPortArg(pPortValue, LAN_NOK);
                if (portKey != 0xFF) {                                                                          /*| <---  for next time  ---> |*/
                    if (isPortKeyInList(portKey, pPortKeyList) == FALSE) { pPortKeyList[portKeyIndex++] = portKey; memset(&pPortValue[0], 0, 3); }
                    else                                 /* DUPLICATE */ { badUserInput = TRUE; break; }
                } else {
                    // Invalid port.
                    badUserInput = TRUE;
                    break;
                }
            } else {
                // Add it if not at the limit.
                if (strlen(pPortValue) < 3) { strcat(pPortValue, &thisChar[0]); }
                else    /* port too long */ { badUserInput = TRUE; break;       }
            }
        }
    } // while ...

    if (badUserInput == TRUE) { memset(&pPortKeyList[0], 0xFF, 9); } // I told you this would happen.
}


BOOL isLanPortKeyPresent(U8  *pPortKeyList)
{
    BOOL lanPresent = FALSE;
    U32 iggy;
    for (iggy = 0; iggy < MAX_NUMBER_APPARENT_PORTS; iggy++) {
        if (pPortKeyList[iggy] == 0) {
            // Not allowed.
            lanPresent = TRUE;
            break;
        }
    }
    return lanPresent;
}


static void readSecondary4PortsByBaseAddress(char *pUserInput)
{
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U8     baseAddress;
    U8     readAddress;
    U8     oneByte;
    U16    iggy;

    pArguments = &pUserInput[strlen(READ_SEC_4PORTS_BY_BASE_ADDR_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if (strlen(pAddressString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        baseAddress = atoh(pAddressString);
        displayInvalidParameters(pUserInput, NULL);
        for (iggy = 1; iggy <= NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
            readAddress = (0x10 * iggy) + baseAddress;
            oneByte = readFromSecondarySwitchByAddress(readAddress);
            printf("REG ADDR 0x%02X = %02X\r\n", readAddress, oneByte);
        }
    }
}



static void  usartResetStatusCommand (char *pUserInput)
{
    char  *pArguments;
    char   pUsartString[AMU_COMPONENT_DATA_SIZE_MAX];

    pArguments = &pUserInput[strlen(USART_RESET_STATUS_CMD) + 1];
    memset(pUsartString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_UART_STRING, pUsartString, pArguments);

    if (strlen(pUsartString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if (strncmp(pUsartString, "USART0", strlen(pUsartString)) == 0) {
            printf("reset status for USART0\r\n");
            usart_reset_status(USART0);
        } else if (strncmp(pUsartString, "USART2", strlen(pUsartString)) == 0) {
            printf("reset status for USART2\r\n");
            usart_reset_status(USART2);
        } else {
            displayInvalidParameters(pUserInput, NULL);
        }
    }
}


#if 0 // Because the MIB stuff is disabled

static U16 portStringToPrimaryPortKey(char *pPortNumberString)
{
    U16 primaryPortKey;
    primaryPortKey = 0;
    if      (strncmp(pPortNumberString, PORT_1_STRING,   1) == 0) { primaryPortKey = 1; }
    else if (strncmp(pPortNumberString, PORT_2_STRING,   1) == 0) { primaryPortKey = 2; }
    else if (strncmp(pPortNumberString, PORT_3_STRING,   1) == 0) { primaryPortKey = 3; }
    else if (strncmp(pPortNumberString, PORT_4_STRING,   1) == 0) { primaryPortKey = 4; }
    else if (strncmp(pPortNumberString, PORT_LAN_STRING, 3) == 0) { primaryPortKey = 5; }
    return primaryPortKey;
}


static U16 portStringToSecondaryPortKey(char *pPortNumberString)
{
    U16 secondaryPortKey;
    secondaryPortKey = 0;
    if      (strncmp(pPortNumberString, PORT_5_STRING,   1) == 0) { secondaryPortKey = 1; }
    else if (strncmp(pPortNumberString, PORT_6_STRING,   1) == 0) { secondaryPortKey = 2; }
    else if (strncmp(pPortNumberString, PORT_7_STRING,   1) == 0) { secondaryPortKey = 3; }
    else if (strncmp(pPortNumberString, PORT_8_STRING,   1) == 0) { secondaryPortKey = 4; }
    return secondaryPortKey;
}


static void portKeyToPrimaryPortString(U16 portKey, char *pPortString)
{
    if      (portKey == 1) { strcpy(pPortString, PORT_1_STRING);   }
    else if (portKey == 2) { strcpy(pPortString, PORT_2_STRING);   }
    else if (portKey == 3) { strcpy(pPortString, PORT_3_STRING);   }
    else if (portKey == 4) { strcpy(pPortString, PORT_4_STRING);   }
    else if (portKey == 5) { strcpy(pPortString, PORT_LAN_STRING); }
    return;
}
#endif // MIB


// userDisplayPortInfo
//
//
//
//
static void userDisplayPortInfo(char *pUserInput)
{
    char  *pArguments;
    char   pPortString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    portKey;
    U32    portIndex;
    U16    statusRegister;
    U16    controlRegister;
    BOOL   poeEnabled;
    F32    poePower;
    F32    poePac;
    char  *pAcSupport;

    pArguments = &pUserInput[strlen(DISPLAY_PORT_INFO_CMD) + 1];
    memset(pPortString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_PORT_STRING, pPortString, pArguments);

    if (strlen(pPortString) == 0) {
        printf("INVALID ARGUMENT\r\n");
    } else {
        // Get the port "key", a value from 0..8 where 0 = LAN.
        portKey = getPortKeyFromPortArg(pPortString, LAN_OK);
        if (portKey != 0xFF) {
            // From the port "key", determine if it is a port on the primary or secondary controller.
            // Evaluate the port "index" to access the PRIMARY or SEWCOMNDARY controller tables.
            portIndex = PORT_KEY_TO_PORT_INDEX_LOOKUP[portKey];
            if (portKey <= 4) {
                // PRIMARY controller port:
                statusRegister  = getPrimaryStatusByPortIndex(portIndex);
                controlRegister = getPrimaryControlByPortIndex(portIndex);

                printf("PORT %s (key:%ld/index:%ld): LINK=%s CONFIG=%s (status=0x%04X control:0x%04X)",
                       pPortString, portKey, portIndex,
                       (isPrimaryLinkStatusLinkUp(statusRegister)       == TRUE ? "UP"      : "DOWN"),
                       (isPrimaryLinkControlPoweredUp(controlRegister)  == TRUE ? "ENABLED" : "DISABLED"),
                       statusRegister, controlRegister);
                if (portKey == 0) {
                    // LAN ONLY, no POE info.
                    printf("\r\n");
                } else {
                    // It is 1 one of the POE ports. Append POE info. Access to the POE tables is 0..7 based on the "port key less 1":
                    poeEnabled = isPoePortEnabled(portKey - 1);
                    poePower   = getPoePortPower(portKey - 1);
                    poePac     = getPoePortPac(portKey - 1);
                    pAcSupport = getACSupportStringFromPort(portKey - 1);
                    printf(" POE:%s %2.2fW PAC:%2.2fW AC:%s\r\n", (poeEnabled == TRUE ? "POWERED UP" : "POWERED DOWN"), poePower, poePac, pAcSupport);
                }
            } else {
                // SECONDARY controller port:
                statusRegister  = getSecondaryStatusByPortIndex(portIndex);
                controlRegister = getSecondaryControl10ByPortIndex(portIndex);
                poeEnabled      = isPoePortEnabled(portKey - 1);
                poePower        = getPoePortPower(portKey - 1);
                poePac          = getPoePortPac(portKey - 1);
                pAcSupport      = getACSupportStringFromPort(portKey - 1);
                printf("PORT %s (key:%ld/index:%ld): LINK=%s CONFIG=%s (status=0x%04X control=0x%04X) POE:%s %2.2fW PAC:%2.2fW AC:%s\r\n",
                       pPortString, portKey, portIndex,
                       (isSecondaryLinkStatusLinkUp(statusRegister)         == TRUE ? "UP"      : "DOWN"),
                       (isSecondaryLinkControl10PoweredUp(controlRegister)  == TRUE ? "ENABLED" : "DISABLED"),
                       statusRegister,
                       controlRegister,
                       (poeEnabled == TRUE ? "POWERED UP" : "POWERED DOWN"),
                       poePower,
                       poePac,
                       pAcSupport);
            }
        } else {
            printf("*invalid* port specified\r\n");
        }
    }
}


// userGetContactClosureInfo
//
// Called as a user-friendly CLI command, non-json formatted.
static void userGetContactClosureInfo(void)
{
    U32 iggy;

    // Display the output contact closure configurations for all 4 output contacts.
    printf("OUTPUT CONTACT CLOSURE CONFIGURATIONS:\r\n");
    for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) {
        printf("CC%ld: %s\r\n", (iggy+1), gOutputCcConfig[iggy].config == OUTPUT_CC_OPEN ? CC_OPEN_STRING : CC_CLOSE_STRING);
    }

    // Display the input contact closure statuses for all 4 input contacts.
    printf("INPUT CONTACT CLOSURE STATUSES:\r\n");
    for (iggy = 0; iggy < NUM_INPUT_CONTACT_CLOSURES; iggy++) {
        printf("CC%ld: %s\r\n", (iggy+1), ioport_get_pin_level(gInputCcStatus[iggy].gpio) == INPUT_CC_OPEN ? CC_OPEN_STRING : CC_CLOSE_STRING);
    }
}


// enableEthernetControllerChecking//
//
//
// {"CLI":{"COMMAND":"enable_switch_checking","COMMAND ARG":"NONE","CODE":1,"RESPONSE":{"enable_switch_checking":"ethernet controller checking is ENABLED"},"RESULT":"SUCCESS"}}
// {"CLI":{"COMMAND":"disable_switch_checking","COMMAND ARG":"NONE","CODE":1,"RESPONSE":{"disable_switch_checking":"ethernet controller checking is *DISABLED*"},"RESULT":"SUCCESS"}}
static void  enableEthernetControllerChecking(BOOL enable)
{
    if (enable == TRUE) {
        // Only make the change if it is not enabled.
        if (!isEthernetCheckingEnabled()) {
            setEthernetCheckingEnabled(TRUE);
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   ENABLE_SWITCH_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  ENABLE_SWITCH_CHECKING_CMD, ETH_CONTROLLER_CHECKING_ENABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            portCheckingSmCb.state = ETH_SM_START;
            sprintf(&pCraftPortDebugData[0], "ethernet controller checking is enabled\r\n"); UART1_WRITE_PACKET_MACRO
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   ENABLE_SWITCH_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  ENABLE_SWITCH_CHECKING_CMD, ETH_CONTROLLER_CHECKING_WAS_ENABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            sprintf(&pCraftPortDebugData[0], "ethernet controller checking was already enabled - no change made\r\n"); UART1_WRITE_PACKET_MACRO
        }
    } else {
        // Only make the change if it is enabled.
        if (isEthernetCheckingEnabled()) {
            setEthernetCheckingEnabled(FALSE);
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   DISABLE_SWITCH_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  DISABLE_SWITCH_CHECKING_CMD, ETH_CONTROLLER_CHECKING_DISABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            portCheckingSmCb.state = ETH_SM_START;
            sprintf(&pCraftPortDebugData[0], "ethernet controller checking is DISABLED\r\n"); UART1_WRITE_PACKET_MACRO
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,   ENABLE_SWITCH_CHECKING_CMD,
                                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                                    CODE_STRING,      0,
                                                                                    RESPONSE_STRING,  DISABLE_SWITCH_CHECKING_CMD, ETH_CONTROLLER_CHECKING_WAS_DISABLED_STRING,
                                                                                    RESULT_STRING,    SUCCESS_STRING);
            sprintf(&pCraftPortDebugData[0], "ethernet controller checking was already DISABLED - no change made\r\n"); UART1_WRITE_PACKET_MACRO
        }
    }
}





// NOTE: All KSZ "MIB" counters processing disabled until a firm set of requirements is defined.


#if 0 // DISABLED

static void getSwitchMibCounterCommand(char *pUserInput)
{
    char *pArguments;
    char  pPortNumberString[AMU_COMPONENT_DATA_SIZE_MAX];
    char  pMibIndexString[AMU_COMPONENT_DATA_SIZE_MAX];
    U16   primaryPortKey;
    U16   secondayPortKey;
    U32   mibIndex;

    // User input should be something like:
    //
    // get_mib_counter 2 80
    //
    // where: - the 1st parameter is the port: 1, 2, 3, 4 or LAN (primary switch only), 5, 6, 7, 8 (secondary switch)
    //        - the 2nd is the hex MIB index: 01 .. 1F for all ports + 80, 81,82, 83 for primary ports only.

    pArguments = &pUserInput[strlen(GET_SWITCH_MIB_COUNTER_CMD) + 1];
    memset(pPortNumberString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pMibIndexString,   0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_PORT_NUMBER_SWITCH_MIB_COUNTER_STRING, pPortNumberString, pArguments);
    findComponentStringData(CLI_ARG_MIB_INDEX_SWITCH_MIB_COUNTER_STRING,   pMibIndexString,   pArguments);

    if ((strlen(pPortNumberString) == 0) || (strlen(pMibIndexString) == 0)) {
        // {"MIB COUNTER":{"PORT":6,"MIB INDEX":"88","MIB RESULT":"FAILED","REASON":"MISSING ARGUMENT"}}
        printf("{%s:{%s:%s,%s:\"%02lX\",%s:%s,%s:%s}}\r\n", MIB_COUNTER_STRING, PORT_STRING,              pPortNumberString, MIB_INDEX_STRING, mibIndex,
                                                            MIB_RESULT_STRING,  MIB_RESULT_FAILED_STRING, REASON_STRING,     MIB_RESULT_MISSING_ARG_REASON_STRING);
    } else {
        // Validate the port number. It is: 1, 2, 3, 4, LAN, 5, 6, 7, 8.
        primaryPortKey = portStringToPrimaryPortKey(pPortNumberString); // I think there are other (better) means to obtain this
        if (primaryPortKey != 0) {
            // Port good. Validate mib index.
            if (newATOH(pMibIndexString, &mibIndex)) {
                // it looks like a hex. Is it valid? Let the primary switch subsystem decide.
                getPrimarySwitchMibCounter(primaryPortKey, (U8) mibIndex, pPortNumberString);
            } else {
                printf("{%s:{%s:%s,%s:\"%02lX\",%s:%s,%s:%s}}\r\n", MIB_COUNTER_STRING, PORT_STRING,              pPortNumberString, MIB_INDEX_STRING, mibIndex,
                                                                    MIB_RESULT_STRING,  MIB_RESULT_FAILED_STRING, REASON_STRING,     MIB_RESULT_INVAL_MIB_INDEX_REASON_STRING);
                incrementBadEventCounter(INVALID_MIB_AS_HEX_IN_GET_MIB_COUNTER);
            }
        } else {
            // Not a primary switch port number. Maybe the secondary switch?
            secondayPortKey = portStringToSecondaryPortKey(pPortNumberString); // I think there are other (better) means to obtain this
            if (secondayPortKey != 0) {
                // Port good. Validate mib index. treat is ass a 2-character byte.
                if (newATOH(pMibIndexString, &mibIndex)) {
                    // It looks like a hex. Is it valid? Let the secondary switch subsystem decide.
                    // !!!!!!!!!!!!!!!!! getSecondarySwitchMibCounter(secondayPortKey, (U8) mibIndex);
                    //
                    // Not ready - yet. For now, return a JSON-like string as follows:
                    //
                    // {"MIB COUNTER":{"PORT":"4","MIB INDEX":"80","MIB RESULT":"FAILED","REASON":"NOT READY FOR SECONDARY SWITCH"}}
                    printf("{%s:{%s:%s,%s:\"%02lX\",%s:%s,%s:%s}}\r\n", MIB_COUNTER_STRING, PORT_STRING,              pPortNumberString, MIB_INDEX_STRING, mibIndex,
                                                                        MIB_RESULT_STRING,  MIB_RESULT_FAILED_STRING, REASON_STRING,     MIB_RESULT_NOT_READY_REASON_STRING);
                } else {
                    printf("{ %s:{%s:%s,%s:\"%02lX\",%s:%s,%s:%s}}\r\n", MIB_COUNTER_STRING, PORT_STRING,              pPortNumberString, MIB_INDEX_STRING, mibIndex,
                                                                         MIB_RESULT_STRING,  MIB_RESULT_FAILED_STRING, REASON_STRING,     MIB_RESULT_INVAL_MIB_INDEX_REASON_STRING);
                    incrementBadEventCounter(INVALID_MIB_AS_HEX_IN_GET_MIB_COUNTER);
                }
            } else {
                printf("{%s:{%s:%s,%s:\"%02lX\",%s:%s,%s:%s}}\r\n", MIB_COUNTER_STRING, PORT_STRING,              pPortNumberString, MIB_INDEX_STRING, mibIndex,
                                                                    MIB_RESULT_STRING,  MIB_RESULT_FAILED_STRING, REASON_STRING,     MIB_RESULT_INVAL_PORT_REASON_STRING);
                incrementBadEventCounter(INVALID_PORT_IN_GET_MIB_COUNTER);
            }
        }
    }
}


// TODO: MERGE THE FOLLOWING 2 FUNCTIONS INTO 1 WITH PARAMETER INDICATING ENABLE/DISABLE. PLEASE.
//       Since it has to do with the KSZ MIBs which is implemented mostly only for the primary
//       controller, not of high importance. 

static void enableFlushFreezeAllPortsCommand(void)
{
    U16   iggy;
    char  pPortNumberString[32];

    // The output will look something like this:
    //
    // {"ENABLE FLUSH FREEZE":{"PRIMARY SWITCH LIST":[{"port 1":"DONE","ADDRESS":"0x1500"}, ... {"PORT LAN":"DONE","ADDRESS":"0x5500"}],"SECONDARY SWITCH LIST":"NOT READY YET"}}

    printf("{ %s:{%s:[", ENABLE_FLUSH_FREEZE_STRING, PRIMARY_SWITCH_LIST_STRING); // square bracket - contents is a list.

    // Primary switch controller:
    for (iggy = PRIMARY_SWITCH_PORT_NUMBER_MIN; iggy <= PRIMARY_SWITCH_PORT_NUMBER_MAX; iggy++) {
        portKeyToPrimaryPortString(iggy, pPortNumberString); // I think there are other (better) means to obtain this
        enableFlushFreezeByPortKey(iggy, pPortNumberString);
        if (iggy < 5) { printf(", "); }
    } // for ...

    // Since the system is not ready for the secondary switch ...
    printf("],%s:[%s]}}\r\n", SECONDARY_SWITCH_LIST_STRING, MIB_RESULT_NOT_READY_REASON_STRING); // close off the JSON-like string.
}


static void disableFlushFreezeAllPortsCommand(void)
{
    U16   iggy;
    char  pPortNumberString[32];

    // The output will look something like this:
    //
    // { "DISABLE FLUSH FREEZE":{"PRIMARY SWITCH LIST":[{"port 1":"DONE","ADDRESS":"0x1500"}, ... {"PORT LAN":"DONE","ADDRESS":"0x5500"}],"SECONDARY SWITCH LIST":"NOT READY YET"}}

    printf("{%s:{ %s:[", DISABLE_FLUSH_FREEZE_STRING, PRIMARY_SWITCH_LIST_STRING); // square bracket - contents is a list.

    // Primary switch controller:
    for (iggy = PRIMARY_SWITCH_PORT_NUMBER_MIN; iggy <= PRIMARY_SWITCH_PORT_NUMBER_MAX; iggy++) {
        portKeyToPrimaryPortString(iggy, pPortNumberString); // I think there are other (better) means to obtain this
        disableFlushFreezeByPortKey(iggy, pPortNumberString);
        if (iggy < 5) { printf(", "); }
    } // for ...

    // Since the system is not ready for the secondary switch ...
    printf("],%s:[%s]}}\r\n", SECONDARY_SWITCH_LIST_STRING, MIB_RESULT_NOT_READY_REASON_STRING); // close off the JSON-like string.
}


static void flushSwitchMibCountersCommand(char *pUserInput)
{
    flushPrimarySwitchMibCounters();
    printf("MIB COUNTERS OPERATION - FLUSHED\r\n"); // json-like?
}


static void freezeSwitchMibCountersCommand(char *pUserInput)
{
    freezePrimarySwitchMibCounters();
    printf("MIB COUNTERS OPERATION - FROZEN\r\n"); // json-like?
}


static void unfreezeSwitchMibCountersCommand(char *pUserInput)
{

    unfreezePrimarySwitchMibCounters();
    printf("MIB COUNTERS OPERATION - NORMAL\r\n"); // json-like?
}
#endif // MIB


// END OF DISABLED "MIB" STUFF


static void readUsart0ModeStatusCommand(void)
{
    printf("USART0 STATUS=0x%08lX  MODE=0x%08lx\r\n", usart_get_status(USART0), usart_get_mode(USART0));
}


static void readUsart2ModeStatusCommand(void)
{
    printf("USART2 STATUS=0x%08lX  MODE=0x%08lx\r\n", usart_get_status(USART2), usart_get_mode(USART2));
}


static void same70ReadRegisterCommand(char *pUserInput)
{
    char *pArguments;
    char  pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32   registerAddress;
    U32   registerValue;
    U32  *pAddress;

    pArguments = &pUserInput[strlen(SAME70_READ_REG_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if ((strlen(pAddressString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        registerAddress = atoh(pAddressString);
        pAddress        = (U32 *)registerAddress;
        registerValue   = *pAddress;
        printf("     READING FROM %p ... value=0x%08lX\r\n", pAddress, registerValue); // json-like?
    }
}


static void same70WriteRegisterCommand(char *pUserInput)
{
    char *pArguments;
    char  pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char  pValue[AMU_COMPONENT_DATA_SIZE_MAX];
    U32   registerAddress;
    U32   registerValue;
    U32  *pAddress;
    
    pArguments = &pUserInput[strlen(SAME70_WRITE_REG_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pValue,         0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_VALUE_STRING,   pValue,         pArguments);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pValue) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        registerAddress = atoh(pAddressString);
        pAddress        = (U32 *)registerAddress;
        registerValue   = atoh(pValue);
        printf("     WRITING 0x%lX TO %p\r\n", registerValue, pAddress);  // json-like?
        *pAddress = registerValue;
    }
}


char * pGimmePrimaryPortString(U16 primaryPortIndex)
{
    char *pSomeString;
    if      (primaryPortIndex == 0) { pSomeString = (char *)PORT_1_STRING; }
    else if (primaryPortIndex == 1) { pSomeString = (char *)PORT_2_STRING; }
    else if (primaryPortIndex == 2) { pSomeString = (char *)PORT_3_STRING; }
    else if (primaryPortIndex == 3) { pSomeString = (char *)PORT_4_STRING; }
    else if (primaryPortIndex == 4) { pSomeString = (char *)PORT_LAN_STRING; }
    else                            { pSomeString = (char *)PORT_UNKNOWN_STRING; }
    return pSomeString;
}


char * pGimmeSecondaryPortString(U16 secondaryPortIndex)
{
    char *pSomeString;
    if      (secondaryPortIndex == 0) { pSomeString = (char *)PORT_5_STRING; }
    else if (secondaryPortIndex == 1) { pSomeString = (char *)PORT_6_STRING; }
    else if (secondaryPortIndex == 2) { pSomeString = (char *)PORT_7_STRING; }
    else if (secondaryPortIndex == 3) { pSomeString = (char *)PORT_8_STRING; }
    else                              { pSomeString = (char *)PORT_UNKNOWN_STRING; }
    return pSomeString;
}


// writeAutonomousAmuStartupNotice
//
// Called from main during system startup. It writes the AMU startup notice to the
// autonomous event data block. This block is polled by the SBC every few seconds,
// so that it can detect AMU startup in a timely manner.
void writeAutonomousAmuStartupNotice(void)
{
    // For the benefit of the SBC/gateway, write a json-like string into the autonomous data block.
    // It will look something like this:
    //
    // {"EVT":{EI":1,"TICK":12345678,"INFO":"AMU STARTUP","WATCHDOG":"WATCHDOG ENABLED"}}
#ifdef CONF_BOARD_KEEP_WATCHDOG_AT_INIT
    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%ld,%s:%s,%s:%s}}", EVT_STRING,
                                                                           EVENT_ID_STRING, AEI_AMU_STARTUP,
                                                                           TICK_STRING,     g_ul_ms_ticks,
                                                                           INFO_STRING,     AMU_STARTUP_STRING,
                                                                           WATCHDOG_STRING, WATCHDOG_ENABLED_STRING);
#else
    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%ld,%s:%s,%s:%s}}", EVT_STRING,
                                                                           EVENT_ID_STRING, AEI_AMU_STARTUP,
                                                                           TICK_STRING,     g_ul_ms_ticks,
                                                                           INFO_STRING,     AMU_STARTUP_STRING,
                                                                           WATCHDOG_STRING, WATCHDOG_DISABLED_STRING);
#endif
    addJsonToAutonomnousString(pSingleJsonEventString);
}


// addJsonToAutonomnousString
//
//
//
void addJsonToAutonomnousString(char *pJson)
{
    // Each JSON-like event string added is of the form:
    //
    // {"EVT":{EI":1,xxxxx_data_xxxxx}}
    //

    if (strlen(pAutonomousEventString) == 0) {
        // Build the JSON from scratch. There is always room for a single event.
        // Write the event as JSON-like and return. Don't include a trailing comma
        // yet - there may be more events to add to the string.
        sprintf(pAutonomousEventString, "{%s:[%s", AUTONOMOUS_EVENTS_LIST_STRING, pJson);
    } else {
        // There are 1 or more events already present in the autonomous event string. Append the JSON.
        // Check that there is ALWAYS room for the AUTONOMOUS_EVENT_GENERIC_BAD_EVENT itself, which is
        // the autonomous event to add when there is no room for other autonomous events.
        if ((strlen(pAutonomousEventString) + strlen(pJson) + strlen(COMMA_ONLY_STRING) + strlen(BAD_EVENT_AUTO_EVENT_STRING) + strlen(AUTONOMOUS_EVENT_TERMINATION_STRING) ) < AUTONOMOUS_EVENTS_STRING_SIZE) {
            // There's room to append the JSON string to the autonomous string. Pre-append the comma.
            strcat(pAutonomousEventString, COMMA_ONLY_STRING);
            strcat(pAutonomousEventString, pJson);

            // How close to the limit?
            if (strlen(pAutonomousEventString) >= AUTONOMOUS_EVENTS_SAFETY_MARGIN) { incrementBadEventCounter(AUTO_STRING_EXCEEDS_SAFETY_MARGIN); }

        } else {
            // String limit reached, event dropped. Either increment the size of the string, and/or the SBC/Gateway
            // should scan and get the autonomous events list more frequently. Although this normally does occurs if
            // the SBC/Gateway or USB link is down. The BAD EVENT string should be added at least once, for which
            // there is always room.
            if (badEventAddedToAutoString == FALSE) { 
                // Add the BAD EVENT string once only.
                strcat(pAutonomousEventString, BAD_EVENT_AUTO_EVENT_STRING); // already includes a leading comma
                badEventAddedToAutoString = TRUE;
                if (craftDebugOutputEnabled == TRUE) { sprintf(&pCraftPortDebugData[0], "BAD EVENT AUTO EVENT ADDED\r\n"); UART1_WRITE_PACKET_MACRO }
            }
            amuBadEventsCountersBlock[AUTONOMOUS_EVENT_STRING_LIMIT_REACHED]++;
            if (craftDebugOutputEnabled == TRUE) { sprintf(&pCraftPortDebugData[0], "INC COUNT: AUTO EVENT LIMIT REACHED\r\n"); UART1_WRITE_PACKET_MACRO }
        }
    }
}


void  configureContactClosures   (void)
{
    U32 iggy;

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    // Called during system initialization.
    printf("-- Configuring output contact closures 1..4 (PIN: LOW/OPEN)\r\n");
    printf("-- Set input contact closures statuses 1..4 (OPEN)\r\n");
    #endif

    // The default config is OPEN, so the pin levels are set as such. As this is called during
    // system startup, the config values will be reported to the SBC/Gateway as an autonomous
    // "SYSTEM STARTUP" event, which will thereafter check the actual configurations from its
    // database and send the appropriate CLI update command.
    gOutputCcConfig[0].gpio   = CC_OUT_1_GPIO;
    gOutputCcConfig[1].gpio   = CC_OUT_2_GPIO;
    gOutputCcConfig[2].gpio   = CC_OUT_3_GPIO;
    gOutputCcConfig[3].gpio   = CC_OUT_4_GPIO;
    for (iggy = 0; iggy < NUM_OUTPUT_CONTACT_CLOSURES; iggy++) {
        gOutputCcConfig[iggy].config = OUTPUT_CC_OPEN;
        ioport_set_pin_level(gOutputCcConfig[iggy].gpio, IOPORT_PIN_LEVEL_LOW);
    }

    initInputCCStatusData();

}

// initInputCCStatusData
//
//
//
static void initInputCCStatusData(void)
{
    U32 iggy;
    // The actual checking of input contact closure statuses is controlled from the main loop.
    for (iggy = 0; iggy < NUM_INPUT_CONTACT_CLOSURES; iggy++) {
         gInputCcStatus[iggy].status        = INPUT_CC_OPEN;
         gInputCcStatus[iggy].smState       = INPUT_CC_CHECK_STATUS_SM_IDLE;
         gInputCcStatus[iggy].lastTickCheck = g_ul_ms_ticks;
         gInputCcStatus[iggy].gpio          = GPIO_BY_INPUT_CC_LOOKUP[iggy];
    }
    nextInputCCRankToCheck  = 0;
}


// configureTempAndFanControl
//
// Called from main during system restart, to configure I2C over the SAME70 TWISH0 interface.
// This enables access to all functions of the PCT2075D temperature sensor device and to the
// TC664 PWM Fan controller devices.
//
//
// Configuring the I2C interface over the SAME70 TWISH0 requires that:
//
// - configure CCFG_SYSIO to select PB4,PB5 function
// - enable Peripheral Clocks for TWIHS0
// - configure PA3,PA4 for TWIHS0 Peripheral A Function
// - set TWIHS0 SCLK Frequency to 99kHz
// - enable TWIHS0 Master Mode and disable all other modes
// - set FIFODIS, ACMDIS, PECDIS, SMBDIS, HSDIS, SVDIS, MSEN
//
void configureTempAndFanControl(void)
{
    U32 regValue;

    // Called during system initialization.

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("-- Configuring I2C/TWIHS0 temperature/fan control interface (PIOA)\r\n");
    #endif
    tempFanControlCb.lowSpeedTemperatureSetting = FAN_SPEED_LOW_TEMPERTURE_DEFAULT;
    tempFanControlCb.maxSpeedTemperatureSetting = FAN_SPEED_MAX_TEMPERTURE_DEFAULT;

    // Based on hardware test script.

    // 1: Enable Peripheral Clocks for TWIHS0 by writing 0x00180000 to REG_PMC_PCER0
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Enable PMC_PCER0 for TWIHS0:      write_reg addr=%08lX value=%08X\r\n",  (uint32_t)&REG_PMC_PCER0, TWISH0_CLOCK_ENABLE_VALUE);
    #endif
    writeSame70Register((uint32_t)&REG_PMC_PCER0, TWISH0_CLOCK_ENABLE_VALUE);

    // 2: Configure PA3 (PIO_PDR_P3) and PA4 (PIO_PDR_P4) for TWIHS0 Peripheral A Function
    regValue = PIO_PDR_P3 + PIO_PDR_P4; // 0x18
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Configure PA3,PA4 for TWIHS0:    write_reg addr=%08lX value=%08lX\r\n", (uint32_t)&REG_PIOA_PDR, regValue);
    #endif
    writeSame70Register((uint32_t)&REG_PIOA_PDR, regValue);
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - PIOA_PSR:                        read_reg  addr=%08lX = 0x%08lX\r\n",    (uint32_t)&REG_PIOA_PSR,    readSame70Register((uint32_t)&REG_PIOA_PSR));
    printf("  - PIOA_ABCDSR:                     read_reg  addr=%08lX = 0x%08lX\r\n",    (uint32_t)&REG_PIOA_ABCDSR, readSame70Register((uint32_t)&REG_PIOA_ABCDSR));
    #endif

    // 3: Set TWIHS0 SCLK Frequency (100 kHz max)
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Set TWIHS0 SCK to 99 kHz:        write_reg addr=%08lX value=%08X\r\n",  TEMP_FAN_REG_TWIHS_CWGR, TWIHS0_SCLK_FREQUENCY_VALUE);
    #endif
    writeSame70Register(TEMP_FAN_REG_TWIHS_CWGR, TWIHS0_SCLK_FREQUENCY_VALUE);
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - TWIHS0_CWGR:                     read_reag addr=%08lX = %08lX\r\n",     TEMP_FAN_REG_TWIHS_CWGR, readSame70Register(TEMP_FAN_REG_TWIHS_CWGR));
    #endif

    // 4: Enable TWIHS0 Master Mode and disable all other modes
    //    Set FIFODIS, ACMDIS, PECDIS, SMBDIS, HSDIS, SVDIS, MSEN
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - Enable TWIHS0 Master Mode:       write_reg addr=%08lX value=%08X\r\n",  TEMP_FAN_REG_TWIHS_CR, TWIHS0_ENABLE_MM_DISABLE_OTHERS);
    #endif
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, TWIHS0_ENABLE_MM_DISABLE_OTHERS);
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - TWIHS0_SR:                       read_reg  addr=%08lX = 0x%08lX\r\n",    TEMP_FAN_REG_TWIHS_STATUS, readSame70Register(TEMP_FAN_REG_TWIHS_STATUS));
    #endif

    // 5: Set the fan speed to max.
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - fan speed set to MAX on startup\r\n");
    #endif
    writeU11FanSpeed(U11_FAN_SPEED_SETTING_MAX, NO_JSON_OUTPUT, NULL);
    tempFanControlCb.currentU11FanSpeed = U11_FAN_SPEED_SETTING_MAX;
    tempFanControlCb.lastCheckTick      = g_ul_ms_ticks;
    tempFanControlCb.deviceToCheck      = DEVICE_TO_CHECK_TEMPERATURE;

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - I2C/TWIHS0 TEMP/FAN config complete\r\n");
    #endif

}


static char *speedStringFromCurrentSpeed(void)
{
    if      (tempFanControlCb.currentU11FanSpeed == U11_FAN_SPEED_SETTING_MAX)  { return (char *)&FAN_SPEED_MAX_STRING; }
    else if (tempFanControlCb.currentU11FanSpeed == U11_FAN_SPEED_SETTING_LOW)  { return (char *)&FAN_SPEED_LOW_STRING; }
    else   /*tempFanControlCb.currentU11FanSpeed == U11_FAN_SPEED_SETTING_OFF*/ { return (char *)&FAN_SPEED_OFF_STRING; }
}


// displayTempAndFanInfo
//
// An output component of hello processing. It contains the temperature and fan control settings info.
// The data is in the temperature/fan ram control block based on periodic call from the main loop. 
// So the data written to output is from the ram control block. Output a json-like string of the form:
//
// "TEMP/FAN INFO":{"TEMPERATURE":"23.1", "CURRENT FAN SPEED":"OFF|LOW|MAX", "TEMPERATURE SPEED SETTINGS":{"LOW":25,"MAX":40}}
//
static void displayTempAndFanInfo(void)
{
    printf("%s:{%s:\"%ld\",%s:%s,%s:{%s:%lu,%s:%lu}}",
           TEMP_FAN_INFO_STRING,
           TEMPERATURE_STRING,         tempFanControlCb.currentTempCelsius,
           CURRENT_FAN_SPEED_STRING,   speedStringFromCurrentSpeed(),
           TEMP_SPEED_SETTINGS_STRING, FAN_SPEED_LOW_STRING, (U32)tempFanControlCb.lowSpeedTemperatureSetting, FAN_SPEED_MAX_STRING, (U32)tempFanControlCb.maxSpeedTemperatureSetting);
}


// tempFanInfo
//
//
//
static void tempFanInfo(void)
{
    displayTempAndFanInfo(); printf("\r\n");
}


// fanInfo
//
// More to come, in a future near you ...
//
//
// Fan control is via I2C access to/from the sensor via the SAME70 TWISH0 component. As with temperature reading,
// there is no TWISH0 (or TWISH1/2) module in the system, so access to the fan control device is via direct reading/writing
// from/to the appropriate SAME70 register addresses.
//
// The cli takes the form: fan_info <mfr_id|ver_id|status|speed>
//
// where speed applies to the write-speed action only.
static void fanInfo(char *pUserInput)
{
    char *pArguments;
    char  pActionString[AMU_COMPONENT_DATA_SIZE_MAX];

    pArguments = &pUserInput[strlen(FAN_INFO_CMD) + 1];
    memset(pActionString,     0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ACTION_STRING, pActionString, pArguments);

    if ((strlen(pActionString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        if      (strncmp(pActionString, CLI_ARG_READ_FAN_MFR_ID_STRING,     strlen(CLI_ARG_READ_FAN_MFR_ID_STRING)) == 0)     { readU11FanInfoItem(CLI_ARG_READ_FAN_MFR_ID_STRING,     U11_FAN_MFR_ID_READ_COMMAND);      }
        else if (strncmp(pActionString, CLI_ARG_READ_FAN_VERSION_ID_STRING, strlen(CLI_ARG_READ_FAN_VERSION_ID_STRING)) == 0) { readU11FanInfoItem(CLI_ARG_READ_FAN_VERSION_ID_STRING, U11_FAN_VERSION_ID_READ_COMMAND);  }
        else if (strncmp(pActionString, CLI_ARG_READ_FAN_STATUS_ID_STRING,  strlen(CLI_ARG_READ_FAN_STATUS_ID_STRING)) == 0)  { readU11FanInfoItem(CLI_ARG_READ_FAN_STATUS_ID_STRING,  U11_FAN_STATUS_READ_COMMAND);      }
        else if (strncmp(pActionString, CLI_ARG_READ_FAN_SPEED_STRING,      strlen(CLI_ARG_READ_FAN_SPEED_STRING)) == 0)      { readU11FanInfoItem(CLI_ARG_READ_FAN_SPEED_STRING,      U11_FAN_CONFIG_READ_COMMAND);      }
        else                                                                                                                  { displayInvalidParameters(pUserInput, NULL);                                               }
    }
}


// fantasticControl
//
// More to come, in a future near you ...
//
//
// Fan control is via I2C access to/from the sensor via the SAME70 TWISH0 component. As with temperature reading,
// there is no TWISH0 (or TWISH1/2) module in the system, so access to the fan control device is via direct reading/writing
// from/to the appropriate SAME70 register addresses.
//
// The cli takes the form: fan_tastic action=<some action string> speed=<off|low|max>
//                         fan_tastic action=w_speed speed=<off|low|max>
//
// where speed applies to the write-speed action only.
static void fantasticControl(char *pUserInput)
{
    char *pArguments;
    char *pNextArguments;
    char  pActionString[AMU_COMPONENT_DATA_SIZE_MAX];
    char  pSpeedString[AMU_COMPONENT_DATA_SIZE_MAX];

    pArguments = &pUserInput[strlen(FAN_TASTIC_CMD) + 1];
    memset(pActionString,     0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pSpeedString,      0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ACTION_STRING, pActionString, pArguments);

    if ((strlen(pActionString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {

        if (strncmp(pActionString, CLI_ARG_WRITE_FAN_SPEED_STRING, strlen(CLI_ARG_WRITE_FAN_SPEED_STRING)) == 0) {
            // To set the fan speed, extract the value. Re-adjust the pointer to the argument portion.
            pNextArguments = pArguments + strlen(CLI_ARG_ACTION_STRING) + 1 + strlen(CLI_ARG_WRITE_FAN_SPEED_STRING); // at a minimum: sizeof "action" + 1 for the = + sizeof "w_speed"
            findComponentStringData(CLI_ARG_SPEED_STRING, pSpeedString, pNextArguments);
            if ((strlen(pSpeedString) == 0)) { displayInvalidParameters(pUserInput, NULL); }
            else {
                // The speed portion was found.
                if      (strncmp(pSpeedString, CLI_ARG_FAN_SPEED_OFF_STRING, strlen(CLI_ARG_FAN_SPEED_OFF_STRING)) == 0)  { writeU11FanSpeed(U11_FAN_SPEED_SETTING_OFF, JSON_OUTPUT, pArguments); }
                else if (strncmp(pSpeedString, CLI_ARG_FAN_SPEED_LOW_STRING, strlen(CLI_ARG_FAN_SPEED_LOW_STRING)) == 0)  { writeU11FanSpeed(U11_FAN_SPEED_SETTING_LOW, JSON_OUTPUT, pArguments); }
                else if (strncmp(pSpeedString, CLI_ARG_FAN_SPEED_MAX_STRING, strlen(CLI_ARG_FAN_SPEED_MAX_STRING)) == 0)  { writeU11FanSpeed(U11_FAN_SPEED_SETTING_MAX, JSON_OUTPUT, pArguments); }
                else                                                                                                      { displayInvalidParameters(pUserInput, NULL);                           }
            }
        } else { displayInvalidParameters(pUserInput, NULL); }
    }
}


// readU11FanInfoItem
//
//
//
static void readU11FanInfoItem(const char *pSomeString, U32 readFanInfoItem)
{
    U32 twihs0RcvHolding;
    U32 twihs0Status;
    U8  dataItem;

    // The TWIHS0 clock is setup for both temperature and fan control during system initialization, no setup required here.
    // Proceed with accessing the U11 fan control interface, which is over TWISH0 using I2C address 0011011 (binary).
    // Processing is based on the SAME70 data sheet, Figure 43-22 "TWIHS Read Operation with Single Data Byte and Internal Address".

    if (i2cTwishOutputEnabled) { printf("-> reading U11 fan info item %s\r\n", pSomeString); }

    // 1: Set the clock in CWGR. This has already been done upon system startup.

    // 2: Set the control register: CR = MSEN + SVDIS
    if (i2cTwishOutputEnabled) { printf("- set CR TO MSEN+SVDIS:       write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS)); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS));

    // 3: Set the master mode register: - set internal address size
    //                                  - set device address
    //                                  - MREAD bit = 1 for this read operation
    if (i2cTwishOutputEnabled) { printf("- set MMR:                    write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_MMR, U11_FAN_MMR_READ_SETUP); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_MMR, U11_FAN_MMR_READ_SETUP);

    // 4. Set the internal address in the IADR register.
    if (i2cTwishOutputEnabled) { printf("- set IADR:                   write_reg addr=%08lX value=%08lX\r\n", TEMP_FAN_REG_TWIHS_IADR, readFanInfoItem); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_IADR, readFanInfoItem);


    // --------------------------------------------------------------------------------------------
    // The TWIHS description in the data sheet does not show this extra read of the RHR and SR
    // registers. Without it, temperature readings fail - invalid data is returned.
    // Read TWIHS0 Status: if RXRDY bit is set, read TWIHS_RHR to clear it
    twihs0Status = readSame70Register(TEMP_FAN_REG_TWIHS_STATUS);
    if ((twihs0Status & TWIHS_SR_RXRDY) == TWIHS_SR_RXRDY) {
        twihs0RcvHolding = readSame70Register(TEMP_FAN_REG_TWIHS_RHOLD);
        twihs0Status     = readSame70Register(TEMP_FAN_REG_TWIHS_STATUS);
        if (i2cTwishOutputEnabled) { printf("- re-read ST/RHOLD regs:      read_reg addr=0x%08lX value = 0x%08lX     read_reg addr=0x%08lX value = 0x%08lX\r\n",
                                            TEMP_FAN_REG_TWIHS_RHOLD, twihs0RcvHolding, TEMP_FAN_REG_TWIHS_STATUS, twihs0Status); }
    }
    // --------------------------------------------------------------------------------------------


    // 5: start the transfer by loading START/STOP in the control register.
    if (i2cTwishOutputEnabled) { printf("- set CR with START/STOP:     write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_START | TWIHS_CR_STOP)); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_START | TWIHS_CR_STOP));

    // 6: Read status until RXRDY is clear.
    if (readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_RXRDY)) {

        // 7: Read the receive holding register.
        twihs0RcvHolding = readSame70Register(TEMP_FAN_REG_TWIHS_RHOLD);
        if (i2cTwishOutputEnabled) { printf("- read RHOLD:                 read_reg addr=%08lX     data = 0x%08lX\r\n", TEMP_FAN_REG_TWIHS_RHOLD, twihs0RcvHolding); }
    
        dataItem = twihs0RcvHolding;

        // 8: Check that TXCOMP bit is set in status register.
        if (readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_TXCOMP)) {

            if (i2cTwishOutputEnabled) { printf("- reading complete, %s = 0x%02X\r\n", pSomeString, dataItem); }

            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%s,%s:{\"0x%08lX\":\"0x%02X\"}}}\r\n",
                   CLI_STRING, COMMAND_STRING, FAN_TASTIC_CMD, ARGUMENTS_STRING, pSomeString, RESULT_STRING, SUCCESS_STRING, RESPONSE_STRING, readFanInfoItem, dataItem);
        } else {
            // bad event counter already incremented.
            sprintf(&pCraftPortDebugData[0], "I2C RUR #1\r\n"); UART1_WRITE_PACKET_MACRO
        }                               
    } else {
        // bad event counter already incremented.
        sprintf(&pCraftPortDebugData[0], "I2C RUR #2\r\n"); UART1_WRITE_PACKET_MACRO 
    }    

}


static void writeU11FanSpeed(U32 speed, BOOL jsonYes, char *pArguments)
{
    const char *pFanSpeed;
    // As per "Figure 43-15. TWIHS Write Operation with Single Data Byte and Internal Address" of the SAME70 data sheet.

    if (i2cTwishOutputEnabled) { printf("-> writing U11 speed command = 0x%08lX\r\n", speed); }

    // 1: Set the control register: CR = MSEN + SVDIS
    if (i2cTwishOutputEnabled) { printf("- set CR (MSEN+SVDIS):        write_reg addr=%08lX value=%08X\r\n",  TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS)); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS));

    // 2: Set the master mode register: - set internal address size
    //                                  - set device address
    //                                  - MREAD bit = 0 for this write operation
    if (i2cTwishOutputEnabled) { printf("- set MMR:                    write_reg addr=%08lX value=%08X\r\n",  TEMP_FAN_REG_TWIHS_MMR, TWIHS0_MMR_FAN_WRITE_SETUP); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_MMR, TWIHS0_MMR_FAN_WRITE_SETUP);

    // 3. Set the internal address in the IADR register.
    if (i2cTwishOutputEnabled) { printf("- set IADR:                   write_reg addr=%08lX value=%08lX\r\n", TEMP_FAN_REG_TWIHS_IADR, U11_FAN_CONFIG_WRITE_COMMAND); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_IADR, U11_FAN_CONFIG_WRITE_COMMAND);

    // 4: Load the transmit holding value in the THR register. And update the control block.
    if (i2cTwishOutputEnabled) { printf("- set THR:                    write_reg addr=%08lX value=%08lX\r\n", TEMP_FAN_REG_TWIHS_THOLD, speed); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_THOLD, speed);
    tempFanControlCb.currentU11FanSpeed = speed;

    // 5. Write STOP to the CR register.
    if (i2cTwishOutputEnabled) { printf("- set CR to stop:             write_reg addr=%08lX value=%08X\r\n",  TEMP_FAN_REG_TWIHS_CR, TWIHS_CR_STOP); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, TWIHS_CR_STOP);

    // 6: Read status until TXRDY is clear.
    if (readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_TXRDY)) {

        // 7: Check that TXCOMP bit is clear in status register.
        if (readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_TXCOMP)) {

            if (jsonYes) {
                if (pArguments != NULL) {
                    if       (speed == U11_FAN_SPEED_SETTING_OFF) { pFanSpeed = &FAN_SPEED_OFF_STRING[0]; }
                    else if  (speed == U11_FAN_SPEED_SETTING_LOW) { pFanSpeed = &FAN_SPEED_LOW_STRING[0]; }
                    else                                          { pFanSpeed = &FAN_SPEED_MAX_STRING[0]; }
                    printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{%s:%s},%s:%s}}\r\n",
                           CLI_STRING, COMMAND_STRING,   FAN_TASTIC_CMD,
                                       ARGUMENTS_STRING, pArguments,
                                       CODE_STRING,      0,
                                       RESPONSE_STRING, SPEED_STRING, pFanSpeed,
                                       RESULT_STRING, SUCCESS_STRING);
                } else{
                    // This should not have happened.
                    incrementBadEventCounter(WRITE_U11_FAN_SPEED_CODING_ERROR);
                }                           
            }
        } else {
             // bad event counter already incremented.
             sprintf(&pCraftPortDebugData[0], "I2C RUR #3\r\n"); UART1_WRITE_PACKET_MACRO
        }            
    } else {
        // bad event counter already incremented.
        sprintf(&pCraftPortDebugData[0], "I2C RUR #4\r\n"); UART1_WRITE_PACKET_MACRO
    }
}


// setFanControlSpeedSettings
//
// Fan control speed settings configuration. It will change the LOW and MAX speed fan settings for 2 temperatures:
//
// - below a LOW temperature setting:  the fan will be set to OFF
// - above a MAX temperature setting: the fan will be set to MAX speed
// - in between, the fan will be set to LOW speed
//
// The cli takes the form: fan_speed_settings low=XX max=YY
//
// where XX and YY are temperature settings in Celsius (decimal), and XX must be greater than YY.
//
// The response is a fully-formatted CLI response of the form:
//
// {"CLI":{"COMMAND":"fan_speed_settings","ARGUMENTS":"low=35 max45","CODE":0,
//                                        "RESPONSE":{"TEMPERATURE SPEED SETTINGS":{"LOW TEMP SPEED SETTING":35,"MAX TEMP SPEED SETTING":45}},
//                                        "RESULT":"SUCCESS"}}
//
static void setFanControlSpeedSettings(char *pUserInput)
{
    char *pArguments;
    char  pLowString[AMU_COMPONENT_DATA_SIZE_MAX];
    char  pMaxString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32   lowTemp;
    U32   maxTemp;

    pArguments = &pUserInput[strlen(SET_FAN_SPEED_SETTINGS_CMD) + 1];
    memset(pLowString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pMaxString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_LOW_STRING, pLowString, pArguments);
    findComponentStringData(CLI_ARG_MAX_STRING, pMaxString, pArguments);

    if ((strlen(pLowString) == 0) || (strlen(pMaxString) == 0)) {
        displayInvalidParameters(pUserInput, NULL);
    } else {
        lowTemp = atoi(pLowString);
        maxTemp = atoi(pMaxString);
        if (maxTemp <= lowTemp) { displayInvalidParameters(pUserInput, NULL); }
        else {
            tempFanControlCb.lowSpeedTemperatureSetting = (S32)lowTemp;
            tempFanControlCb.maxSpeedTemperatureSetting = (S32)maxTemp;
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:{%s:{%s:%ld,%s:%ld}},%s:%s}}\r\n",
                    CLI_STRING, COMMAND_STRING,   SET_FAN_SPEED_SETTINGS_CMD,
                                ARGUMENTS_STRING, pArguments,
                                CODE_STRING,      0,
                                RESPONSE_STRING,  TEMP_SPEED_SETTINGS_STRING, LOW_TEMP_SPEED_SETTING_STRING, lowTemp, MAX_TEMP_SPEED_SETTING_STRING, maxTemp,
                                RESULT_STRING,    SUCCESS_STRING);
        }
    }
}


// setNewFanSpeed
//
// Called by checkTemperatureForFanSpeed which is called from the main loop. No console output.
static void setNewFanSpeed(U32 u11FanSpeed)
{
    if (tempFanControlCb.currentU11FanSpeed != u11FanSpeed) {
        tempFanControlCb.currentU11FanSpeed = u11FanSpeed;
        writeU11FanSpeed(u11FanSpeed, NO_JSON_OUTPUT, NULL);

        // Add an autonomous event of the form: {"EVT":{"EI":34,"FAN SPEED CHANGE":"MAX"}}
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%s}}", EVT_STRING, EVENT_ID_STRING, AEI_FAN_SPEED_CHANGE, FAN_SPEED_CHANGE_STRING, speedStringFromCurrentSpeed());
        addJsonToAutonomnousString(pSingleJsonEventString);
     }
}


// checkTemperatureForFanSpeed
//
// Called from the main loop to check if the fan speed should be changed based on current temperature reading.
static void checkTemperatureForFanSpeed(void)
{
    S32 degrees;
    degrees = tempFanControlCb.currentTempCelsius;
    if (degrees >= 0) {
        if      (degrees <= tempFanControlCb.lowSpeedTemperatureSetting) { setNewFanSpeed(U11_FAN_SPEED_SETTING_OFF); }
        else if (degrees >= tempFanControlCb.maxSpeedTemperatureSetting) { setNewFanSpeed(U11_FAN_SPEED_SETTING_MAX); }
        else    /* between low/max so fan speed set to low */            { setNewFanSpeed(U11_FAN_SPEED_SETTING_LOW); }
    }
}


// displayTemperature
//
// Display the current temperature. Output is json-friendly, of the form:
//
// {"TEMPERATURE":"26","RAW REGISTER":"0x1234"}
    //
static void displayTemperature(void)
{
    printf("{%s:\"%ld\",%s:\"0x%04X\"}\r\n", TEMPERATURE_STRING,  tempFanControlCb.currentTempCelsius,
                                             RAW_REGISTER_STRING, tempFanControlCb.rawRegister);
}


// updateTemperatureReading
//
// Called from the main loop to update the temperature reading.
//
// Temperature reading is via I2C access to/from the sensor via the SAME70 TWISH0 component. There is no TWISH0
// (or TWISH1) module in the system, so access to the temperature device is via direct reading/writing to the
// appropriate SAME70 register addresses using standard I2C messaging protocol.
//
// Note: normally, processing to retrieve the temperature from the sensor would be based on the SAME70 data sheet,
//       Figure 43-23 "TWIHS Read Operation with Multiple Data Bytes with or without Internal Address". But a
//       consistent temperature reading was not extracted from the sensor device. The algorithm as implemented
//       below is based on the hardware script.
//
// Temperature is reported as a whole unsigned integer in a json-friendly string.
//
// As per the PCT2075DP data sheet (http://www.nxp.com/docs/en/data-sheet/PCT2075.pdf) page 11/38:
//
// 1. If the Temp data MSByte bit D10 = 0, then the temperature is positive and Temp value (�C) = +(Temp data) � 0.125 �C.
// 2. If the Temp data MSByte bit D10 = 1, then the temperature is negative and Temp value (�C) = -(two�s complement of Temp data) � 0.125 �C.
//
// NOTE: the data sheet forgot to mention that the concatenated register is shifted before multiplying by 0.125 in 1/2 above.
static void updateTemperatureReading(void)
{
    S32  wholeDegrees;
    F32  calculatedTemperature;
    U32  twihs0Status;
    U32  twihs0RcvHolding;
    U16  tempMsbByte;
    U16  tempLsbByte;
    U16  temporaryRegister1 = 0;
    U16  temporaryRegister2 = 0;
    U16  temporaryRegister3 = 0;

    // Set the control register: CR = MSEN + SVDIS
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- CR TO MSEN+SVDIS: write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS)); UART1_WRITE_PACKET_MACRO  }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS));

    // Set the master mode register.
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- MMR:              write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_MMR, TWIHS0_MMR_READ_TEMPERATURE_SETUP); UART1_WRITE_PACKET_MACRO  }
    writeSame70Register(TEMP_FAN_REG_TWIHS_MMR, TWIHS0_MMR_READ_TEMPERATURE_SETUP);

    // Clear the internal address in the IADR register.
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- IADR:             write_reg addr=%08lX value=0\r\n", TEMP_FAN_REG_TWIHS_IADR); UART1_WRITE_PACKET_MACRO  }
    writeSame70Register(TEMP_FAN_REG_TWIHS_IADR, 0x00);

    // --------------------------------------------------------------------------------------------
    // The TWIHS description in the data sheet does not show this extra read of the RHR and SR
    // registers. Without it, temperature readings fail - invalid data is returned.
    // Read TWIHS0 Status: if RXRDY bit is set, read TWIHS_RHR to clear it
    twihs0Status = readSame70Register(TEMP_FAN_REG_TWIHS_STATUS);
    if ((twihs0Status & TWIHS_SR_RXRDY) == TWIHS_SR_RXRDY) {
        twihs0RcvHolding = readSame70Register(TEMP_FAN_REG_TWIHS_RHOLD);
        twihs0Status     = readSame70Register(TEMP_FAN_REG_TWIHS_STATUS);
        if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- re-read ST/RHOLD regs: addr=0x%08lX value=0x%08lX    addr=0x%08lX value 0x%08lX\r\n",
                                                                      TEMP_FAN_REG_TWIHS_RHOLD, twihs0RcvHolding, TEMP_FAN_REG_TWIHS_STATUS, twihs0Status);
                                     UART1_WRITE_PACKET_MACRO
                                   }

    } else {
        if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- read status not required = 0x%08lX\r\n", twihs0Status); UART1_WRITE_PACKET_MACRO }
    }
    // --------------------------------------------------------------------------------------------


    // Initiate the transfer by writing START to the control register.
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- START:            write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_CR, TWIHS_CR_START); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, TWIHS_CR_START);

    // Read TWIHS0 Status until RXRDY bit is set.
    if (!readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_RXRDY)) { sprintf(&pCraftPortDebugData[0], "I2C RUR #5\r\n"); UART1_WRITE_PACKET_MACRO return; } // I don't like returning in the middle like this ...

    // Read temperature most-significant data byte from the holding register.
    twihs0RcvHolding = readSame70Register(TEMP_FAN_REG_TWIHS_RHOLD);
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- RHOLD 1st:        read_reg addr=%08lX data=0x%08lX\r\n", twihs0RcvHolding, twihs0RcvHolding); }

    tempMsbByte = twihs0RcvHolding;

    // Initiate TWIHS0 Stop by writing to the control register.
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- STOP:             write_reg addr=%08lX value=%08X\r\n", TEMP_FAN_REG_TWIHS_CR, TWIHS_CR_STOP); }
    writeSame70Register(TEMP_FAN_REG_TWIHS_CR, TWIHS_CR_STOP);

    // Read TWIHS0 Status Register and check TWIHS_SR.RXRDY is set.
    if (!readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_RXRDY)) { sprintf(&pCraftPortDebugData[0], "I2C RUR #6\r\n"); UART1_WRITE_PACKET_MACRO return; } // I don't like returning in the middle like this ...

    // Read temperature least-significant data byte:
    twihs0RcvHolding = readSame70Register(TEMP_FAN_REG_TWIHS_RHOLD);
    tempLsbByte = twihs0RcvHolding;
    if (i2cTwishOutputEnabled) { sprintf(&pCraftPortDebugData[0], "- RHOLD 2nd: addr=%08lX data=0x%08lX\r\n", TEMP_FAN_REG_TWIHS_RHOLD, twihs0RcvHolding); }

    // Read TWIHS0 Status until TXCOMP is SET
    if (!readI2cStatusUntilReady(TEMP_FAN_REG_TWIHS_STATUS, TWIHS_SR_TXCOMP)) { sprintf(&pCraftPortDebugData[0], "I2C RUR #7\r\n"); UART1_WRITE_PACKET_MACRO return; } // I don't like returning in the middle like this ...

    // End of read transaction

    // Calculate and display the temperature in C. Construct the 16-bit value,
    // determine if temperature is above or below zero C and calculate.
    tempFanControlCb.rawRegister = (tempMsbByte << 8) + tempLsbByte;
    if ((tempFanControlCb.rawRegister & RAW_TEMPERATURE_DATA_NEGATIVE_BIT) == 0) {
        // Temperature is 0C or above - it's a straightforward calculation.
        temporaryRegister1    = tempFanControlCb.rawRegister >> RAW_TEMPERATURE_DATA_REGISTER_SHIFT; // shift
        calculatedTemperature = temporaryRegister1 * RAW_TEMPERATURE_DATA_FACTOR;                    // F32 calculation: multiply by factor
        wholeDegrees          = (calculatedTemperature + 0.5);                                       // add 0.5 for rounding up
    } else {
        // Below freezing. Well, freezing for some. Polar bears and baby seals enjoy this.
        temporaryRegister1    = ~tempFanControlCb.rawRegister;                                       // 2's compliment
        temporaryRegister2    = temporaryRegister1  + 1;                                             // add 1 (part of 2's compliment)
        temporaryRegister3    = temporaryRegister2 >> RAW_TEMPERATURE_DATA_REGISTER_SHIFT;           // shift
        calculatedTemperature = temporaryRegister3 * RAW_TEMPERATURE_DATA_FACTOR;                    // F32 calculation: multiply by factor
        wholeDegrees          = -(calculatedTemperature + 0.5);                                      // add 0.5 for rounding up, Set to negative degrees
    }

    if (wholeDegrees != tempFanControlCb.currentTempCelsius) {
        // Add an autonomous event of the form: {"EVT":{"EI":33,"TEMPERATURE CHANGE":10,"RAW REGISTER":"1A2C"}}
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%ld,%s:\"%04X\"}}", EVT_STRING, EVENT_ID_STRING, AEI_TEMPERATURE_CHANGE, TEMPERATURE_CHANGE_STRING, wholeDegrees, RAW_REGISTER_STRING, tempFanControlCb.rawRegister);
        addJsonToAutonomnousString(pSingleJsonEventString);
        tempFanControlCb.currentTempCelsius = wholeDegrees;
    }

    if (i2cTwishOutputEnabled == TRUE) { sprintf(&pCraftPortDebugData[0], "TEMPERATURE=%ld->%ld RAW:0x%04X\r\n", tempFanControlCb.currentTempCelsius, wholeDegrees, tempFanControlCb.rawRegister); UART1_WRITE_PACKET_MACRO }

    return;
}


void checkFanSpeedOrTemperature (void)
{
    if ((g_ul_ms_ticks - tempFanControlCb.lastCheckTick) >= FAN_SPEED_CHECK_TIMEOUT) {
        // It is time to check  ...
        if (tempFanControlCb.deviceToCheck == DEVICE_TO_CHECK_TEMPERATURE) {
            updateTemperatureReading();
            tempFanControlCb.lastCheckTick = g_ul_ms_ticks;
            tempFanControlCb.deviceToCheck = DEVICE_TO_CHECK_FAN;
        } else {
            // Check the temperature to determine the fan speed.
            checkTemperatureForFanSpeed();            
            tempFanControlCb.lastCheckTick = g_ul_ms_ticks;
            tempFanControlCb.deviceToCheck = DEVICE_TO_CHECK_TEMPERATURE;
        }

        pokeWatchdog();

    }
}


// portPowerUpDown
//
// Parse the argument list for a comma-separated list of ports 1..8 EXCLUDING the LAN. Order not enforced. Duplicates not allowed.
// Then "power UP" or "power DOWN" the list of ports by configuring the "Power Down" bit in the appropriate CONTROL register of
// either or both controllers. In the case of the POWER UP or DOWN commands, the value of the appropriate control register is
// included in the response.
//
// print something like:
//
// {"CLI":{"COMMAND":"poe_port_power_up","COMMAND ARG":"port=3","CODE":0,"RESPONSE":{
// "PORT CONFIG":[{"PORT":1,"COMMAND":"POWER_DOWN","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"}]}}}   <- if port DOWN
// "PORT CONFIG":[{"PORT":2,"COMMAND":"POWER_UP","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"}]}}}     <- if port UP
//
// For multiple ports:
//
// {"CLI":{"COMMAND":"poe_port_power_up","COMMAND ARG":"port=3","CODE":0,"RESPONSE":{
// "PORT CONFIG":[{"PORT":1,"COMMAND":"POWER_UP","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"},
//                {"PORT":5,"COMMAND":"POWER_UP","REG_ADDR":"0x20","CONTROL_BEFORE":"0x30","CONTROL_AFTER":"0x31"},
//                .
//                .
//                .
//                {"PORT":8,"COMMAND":"POWER_UP","REG_ADDR":"0x20","CONTROL_BEFORE":"0x30","CONTROL_AFTER":"0x31"}]}}}
//
static void portPowerUpDown(char *pUserInput, U32 portCommand)
{
    U16    iggy;
    char  *pArguments;
    char   pPortString[AMU_COMPONENT_DATA_SIZE_MAX];
    U16    portKey;      // evaluates to 1..8 if valid
    U16    kszPortIndex; // evaluates to 0..3 for both primary and secondary controllers
    U16    poePortIndex; // evaluates to 0..7 for the POE controller
    U8     pPortKeyList[MAX_NUMBER_APPARENT_PORTS];
    BOOL   firstTime = TRUE;

    if      (portCommand == PORT_COMMAND_POWER_UP)      { pArguments = &pUserInput[strlen(PORT_POWER_UP_CMD)    + 1]; }
    else  /*(portCommand == PORT_COMMAND_POWER_DOWN) */ { pArguments = &pUserInput[strlen(PORT_POWER_DOWN_CMD)  + 1]; }
    memset(pPortString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_PORT_STRING, pPortString, pArguments);

    if (strlen(pPortString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {

        // Get the "port key" list, verify that the LAN was not specified.
        getPortKeyListFromPortArg(pPortString, pPortKeyList);
        if (isLanPortKeyPresent(pPortKeyList)) { displayInvalidParameters(pUserInput, NULL); }
        else {
            // The LAN port was not specified, proceed.
            // Open the json-like string as the start of a list of port changes.
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:[", CLI_STRING, COMMAND_STRING, (portCommand == PORT_COMMAND_POWER_UP ? PORT_POWER_UP_CMD : PORT_POWER_DOWN_CMD), ARGUMENTS_STRING, pArguments, CODE_STRING, 0, RESPONSE_STRING);
            for (iggy = 0; iggy < MAX_NUMBER_APPARENT_PORTS; iggy++) {
                if (pPortKeyList[iggy] != 0xFF) {
                    portKey      = pPortKeyList[iggy];
                    kszPortIndex = PORT_KEY_TO_PORT_INDEX_LOOKUP[portKey];
                    poePortIndex = portKey - 1;

                    if (firstTime) { firstTime = FALSE; }
                    else           { printf(",");       } // Not the 1st time. A comma needs to be added to separate the list for the following next item.

                    // Is portKey (1..8) that of the PRIMARY or SECONARY controller?
                    if (portKey <= PRIMARY_DOWNSTREAM_PORT_KEY_MAX) {
                        // Primary port:
                        if    (portCommand == PORT_COMMAND_POWER_UP)      { configurePrimaryPortPowerBit(kszPortIndex,   portKey, TRUE);  poeSetPortPowerUpDown(poePortIndex, POE_PORT_POWER_UP);   }
                        else /*(portCommand == PORT_COMMAND_POWER_DOWN)*/ { configurePrimaryPortPowerBit(kszPortIndex,   portKey, FALSE); poeSetPortPowerUpDown(poePortIndex, POE_PORT_POWER_DOWN); }
                    } else {
                        // Secondary port:
                        if     (portCommand == PORT_COMMAND_POWER_UP)     { configureSecondaryPortPowerBit(kszPortIndex, portKey, TRUE);  poeSetPortPowerUpDown(poePortIndex, POE_PORT_POWER_UP);   }
                        else /*(portCommand == PORT_COMMAND_POWER_DOWN)*/ { configureSecondaryPortPowerBit(kszPortIndex, portKey, FALSE); poeSetPortPowerUpDown(poePortIndex, POE_PORT_POWER_DOWN); }
                    }
                } else {
                    // Either end of the line, or invalid input entered by the user.
                }
            } // for ...
            printf("]}}\r\n"); // close the json-like string: add ] to close the list, add } to close the RESPONSE and 1 more to close off the entire object.
        }
    }
}
                        

// portEthernetOnlyUpDown
//
// Parse the argument list for a comma-separated list of ports 1..8 excluding the LAN. Order not enforced. Duplicates not allowed.
// Then "power UP" or "power DOWN" the list of ports by configuring the "Power Down" bit in the appropriate CONTROL register of
// either or both controllers. In the case of the POWER UP or DOWN commands, the value of the appropriate control register is
// included in the response.
//
// The POE controller remains unaffected, so a POE device is expected to remain powered, but no ethernet service is provided
// if the command is "ethernet disable".
//
// print something like:
//
// {"CLI":{"COMMAND":"eth_only_port_enable","COMMAND ARG":"port=3","CODE":0,"RESPONSE":
// [{"PORT":1,"COMMAND":"POWER_DOWN","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"}]}}}   <- if port DOWN
// "PORT":[{"PORT":2,"COMMAND":"POWER_UP","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"}]}}}     <- if port UP
//
//
// {"CLI":{"COMMAND":"eth_only_port_enable","COMMAND ARG":"port=1","CODE":0,"RESPONSE":[{"PORT":1,"COMMAND":"POWER DOWN","REG ADDR":"0x1100","CONTROL BEFORE":"0x2900","CONTROL AFTER":"0x2900"}]}}
//
//
// For multiple ports:
//
// {"CLI":{"COMMAND":"port_eth_only_enable","COMMAND ARG":"port=3","CODE":0,"RESPONSE":{
// "PORT CONFIG":[{"PORT":1,"COMMAND":"POWER_UP","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"},
//                {"PORT":5,"COMMAND":"POWER_UP","REG_ADDR":"0x20","CONTROL_BEFORE":"0x30","CONTROL_AFTER":"0x31"},
//                .
//                .
//                .
//                {"PORT":8,"COMMAND":"POWER_UP","REG_ADDR":"0x20","CONTROL_BEFORE":"0x30","CONTROL_AFTER":"0x31"}]}}}
//
static void portEthernetOnlyUpDown(char *pUserInput, U32 portCommand)
{
    U16    iggy;
    char  *pArguments;
    char   pPortString[AMU_COMPONENT_DATA_SIZE_MAX];
    U16    portKey;      // evaluates to 1..8 if valid
    U16    kszPortIndex; // evaluates to 0..3 for both primary and secondary controllers
    U8     pPortKeyList[MAX_NUMBER_APPARENT_PORTS];
    BOOL   firstTime = TRUE;

    if      (portCommand == PORT_COMMAND_ETH_ONLY_UP)      { pArguments = &pUserInput[strlen(ETH_ONLY_PORT_ENABLE_CMD)   + 1]; }
    else  /*(portCommand == PORT_COMMAND_ETH_ONLY_DOWN) */ { pArguments = &pUserInput[strlen(ETH_ONLY_PORT_DISABLE_CMD)  + 1]; }
    memset(pPortString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_PORT_STRING, pPortString, pArguments);

    if (strlen(pPortString) == 0) {
        displayInvalidParameters(pUserInput, NULL);
    } else {

        // Get the port list. The LAN wold have been excluded.
        // Open the json-like string as the start of a list of port changes.
        getPortKeyListFromPortArg(pPortString, pPortKeyList);
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:[", CLI_STRING, COMMAND_STRING, (portCommand == PORT_COMMAND_ETH_ONLY_UP ? ETH_ONLY_PORT_ENABLE_CMD : ETH_ONLY_PORT_DISABLE_CMD),
                                                                 ARGUMENTS_STRING, pArguments, CODE_STRING, 0, RESPONSE_STRING);
        for (iggy = 0; iggy < MAX_NUMBER_APPARENT_PORTS; iggy++) {
            if (pPortKeyList[iggy] != 0xFF) {
                portKey      = pPortKeyList[iggy];                     // <- on the basis of the port key 1..8
                kszPortIndex = PORT_KEY_TO_PORT_INDEX_LOOKUP[portKey]; // <- get the KSZ port index: 0..3 for both controllers.

                if (firstTime) { firstTime = FALSE; }
                else           { printf(",");       } // Not the 1st time. A comma needs to be added to separate the list for the following next item.

                // Is portKey (1..8) that of the PRIMARY or SECONARY controller?
                if (portKey <= PRIMARY_DOWNSTREAM_PORT_KEY_MAX) {
                    // Primary port:
                    if     (portCommand == PORT_COMMAND_ETH_ONLY_UP)     { configurePrimaryPortPowerBit(kszPortIndex,   portKey, TRUE);  }
                    else /*(portCommand == PORT_COMMAND_ETH_ONLY_DOWN)*/ { configurePrimaryPortPowerBit(kszPortIndex,   portKey, FALSE); }
                } else {
                    // Secondary port:
                    if     (portCommand == PORT_COMMAND_ETH_ONLY_UP)     { configureSecondaryPortPowerBit(kszPortIndex, portKey, TRUE);  }
                    else /*(portCommand == PORT_COMMAND_ETH_ONLY_DOWN)*/ { configureSecondaryPortPowerBit(kszPortIndex, portKey, FALSE); }
                }
            } else {
                // Either end of the line, or invalid input entered by the user.
            }
        } // for ...
        printf("]}}\r\n"); // close the json-like string: add ] to close the list, add } to close the RESPONSE and 1 more to close off the entire object.
    }
}


// i2cDebugOutputOnOff
//
// For debug/test. When enabled, it will print all register activity over I2C
// to the craft/debug port.
//
static void i2cDebugOutputOnOff(BOOL enable)
{
    if (enable == TRUE) { i2cTwishOutputEnabled = TRUE;  }
    else                { i2cTwishOutputEnabled = FALSE; }
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{\"i2cTwishOutputEnabled\":%u},%s:%s}}\r\n",
            CLI_STRING, COMMAND_STRING,   (enable == TRUE ? ENABLE_I2C_FULL_OUTPUT_CMD : DISABLE_I2C_FULL_OUTPUT_CMD),
                        ARGUMENTS_STRING, NONE_STRING,
                        CODE_STRING,      0,
                        RESPONSE_STRING,  i2cTwishOutputEnabled,
                        RESULT_STRING,    SUCCESS_STRING);
}


// craftDebugOutputOnOff
//
// For debug/test. When enabled, will output text to the CRAFT port. Response if of the form:
//
// {"CLI":{"COMMAND":"toggle_craft_debug_output","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"craftDebugOutputEnabled:1|0},"RESULT":"SUCCESS"}}
//
static void craftDebugOutputOnOff(BOOL enable)
{
    if (enable == TRUE) { craftDebugOutputEnabled = TRUE;  }
    else                { craftDebugOutputEnabled = FALSE; }
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{\"craftDebugOutputEnabled\":%u},%s:%s}}\r\n",
           CLI_STRING, COMMAND_STRING,   (enable == TRUE ? ENABLE_CRAFT_DEBUG_OUTPUT_CMD : DISABLE_CRAFT_DEBUG_OUTPUT_CMD),
                       ARGUMENTS_STRING, NONE_STRING,
                       CODE_STRING,      0,
                       RESPONSE_STRING,  craftDebugOutputEnabled,
                       RESULT_STRING,    SUCCESS_STRING);

    sprintf(&pCraftPortDebugData[0], "craftDebugOutputEnabled %s\r\n", (enable == TRUE ? "TRUE" : "FALSE")); UART1_WRITE_PACKET_MACRO

}


// toggleChecksumRequired
//
static void toggleChecksumRequired(void)
{
    inputChecksumRequired ^= TRUE;
    sbcComEchoChar        ^= TRUE;
}


BOOL isCraftDebugEnabled(void)
{
    if (craftDebugOutputEnabled == TRUE) { return TRUE;  }
    else                                 { return FALSE; }
}


// read a U32 quantity from a SAME70 register by address.
// Wait a bit before returning to ensure register interface
// readiness for the next read/write operation.
U32 readSame70Register(U32 registerAddress)
{
    U32 registerValue;
    U32 *pAddress;
    pAddress      = (U32 *)registerAddress;
    registerValue = *pAddress;
    mdelay(SAME70_REG_DELAY);
    return registerValue;
}


// Write a U32 quantity to a SAME70 register by address.
// Wait a bit before returning to ensure register interface
// readiness for the next read/write operation.
void writeSame70Register(U32 registerAddress, U32 registerValue)
{
    U32 *pAddress;
    pAddress = (U32 *)registerAddress;
    *pAddress = registerValue;
    mdelay(SAME70_REG_DELAY);
    return;
}


// programResponse
//
// print a json-like string of the form:
//
// {"CLI":{"COMMAND":"program","ARGUMENTS":"NONE","CODE":0,"RESPONSE":{"PROGRAM":"APPLICATION",
//                                                                     "FLASH LOAD ADDRESS":"0x00040000",
//                                                                     "FLASH SIGNATURES": {"APPLICATION": "0xFFFFFFFF", "BOOTLOADER": "0x3810910F"},
//                                                                     "UPGRADE STATE MACHINE"': 'IDLE',
//                                                                     "UPGRADE PACKET SIZE":512,
//                                                                     "UPTIME": 612149,
//                                                                     "RELEASE":"84"
//                                                                     "SAME70 RESET REASON":"SOFTWARE",
//                                                                     "RESET INFO":"0x00010300 0x300"
//                                                                    },"RESULT":"SUCCESS"}}
static void programResponse(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s,%s:\"0x%08lX\",%s:{%s:\"0x%08lX\",%s:\"0x%08lX\"},",
           CLI_STRING, COMMAND_STRING,   PROGRAM_CMD,
                       ARGUMENTS_STRING, NONE_STRING,
                       CODE_STRING,      0,
                       RESPONSE_STRING,  PROGRAM_STRING,            APPLICATION_STRING,
                                         FLASH_LOAD_ADDRESS_STRING, applicationLoadFlashAddress,
                                         FLASH_SIGNATURES_STRING, APPLICATION_STRING,  applicationFlashSignature, BOOTLOADER_STRING, bootloaderFlashSignature);
    displayUpgradeStateMachineInfo();
    printf(",%s:%u,%s:%lu,%s:\"%s\"", UPGRADE_PACKET_SIZE_STRING, UPGRADE_PACKET_DATA_SIZE_MAX, UPTIME_STRING, g_ul_ms_ticks/1000, RELEASE_STRING, APPARENT_RELEASE_STRING);
    printf(",");
    displayControllerResetReason(JSON_OUTPUT);
    printf("},%s:%s}}\r\n", RESULT_STRING,    SUCCESS_STRING);
}


// jumpToBootloader
//
// Will jump to bootloader if its signature is valid,
// else no jump - period.
//
// The output is json-friendly.
static void jumpToBootloader(void)
{
    if (bootloaderFlashSignature == BOOTLOADER_SIGNATURE_VALUE) {
        // Prepare the jump.
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s,%s:\"0x%08lX\"},%s:%s}}\r\n", CLI_STRING,
                                                                                   COMMAND_STRING,   JUMP_TO_BOOT_CMD,
                                                                                   ARGUMENTS_STRING, NONE_STRING,
                                                                                   CODE_STRING,      0,
                                                                                   RESPONSE_STRING,  JUMP_TO_BOOT_STRING, JUMP_TO_BOOT_IN_5_SECONDS_STRING, FLASH_LOAD_ADDRESS_STRING, BOOTLOADER_FLASH_ADDRESS,
                                                                                   RESULT_STRING,    SUCCESS_STRING);
        jumpToBootOrResetCb.jumpToBootPlease    = TRUE;
        jumpToBootOrResetCb.jumpToBootTimestamp = g_ul_ms_ticks;

        // Set the fan speed to max.
        writeU11FanSpeed(U11_FAN_SPEED_SETTING_MAX, NO_JSON_OUTPUT, NULL);



    } else {
        // No jump.
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING,
                                                                    COMMAND_STRING,   JUMP_TO_BOOT_CMD,
                                                                    ARGUMENTS_STRING, NONE_STRING,
                                                                    CODE_STRING,      1,
                                                                    RESPONSE_STRING,  JUMP_TO_BOOT_STRING, CLI_FLASH_FAIL_NO_BOOT_SIG_STRING,
                                                                    RESULT_STRING,    FAIL_STRING);
    }
}


// checkJumpToBootOrReset
//
//
void checkJumpToBootOrReset(void)
{
    if      (jumpToBootOrResetCb.resetPlease)         { if ((g_ul_ms_ticks - jumpToBootOrResetCb.resetTimestamp)      >= 5000) { __NVIC_SystemReset();      } }
    else if (jumpToBootOrResetCb.softwareResetPlease) { if ((g_ul_ms_ticks - jumpToBootOrResetCb.resetTimestamp)      >= 5000) { executeSoftwareReset();    } }
    else if (jumpToBootOrResetCb.jumpToBootPlease)    { if ((g_ul_ms_ticks - jumpToBootOrResetCb.jumpToBootTimestamp) >= 5000) { executeJumpToBootloader(); } }
}


// executeJumpToBootloader
//
//
//
//
static void executeJumpToBootloader(void)
{
    U32 startAddressResetVector;
    void (*codeEntryVector)(void); // pointer to the bootloader section

    // Disable the watchdog and cache.
    WDT->WDT_MR = WDT_MR_WDDIS;
    SCB_DisableICache();
    SCB_DisableDCache();

    // Set the reset vector for the jump.
    startAddressResetVector = BOOTLOADER_FLASH_ADDRESS + (U32)0x00000004;

    __DSB();                                                                 // data synchronization barrier
    __ISB();                                                                 // instruction synchronization barrier
    SCB->VTOR = ((uint32_t) BOOTLOADER_FLASH_ADDRESS & SCB_VTOR_TBLOFF_Msk); // Re-base the vector table base address
    __set_MSP(*(uint32_t *) BOOTLOADER_FLASH_ADDRESS);                       // Re-base the Stack Pointer
    __DSB();                                                                 // data synchronization barrier
    __ISB();                                                                 // instruction synchronization barrier

    // Load the reset handler address of the application and call it.
    codeEntryVector =  (void (*)(void))(unsigned *)(*(unsigned *)(startAddressResetVector));
    codeEntryVector(); // <- this is it: this call will jump to the bootloader's Reset Handler!!!
}


// executeSoftwareReset
//
//
//
//
static void executeSoftwareReset(void)
{
printf("software reset via RSTC ...not implemented\r\n");
}


// resetFunc
static void resetFunc(void)
{
    // Prepare for the hardware reset:
    // {"CLI":{"COMMAND":"reset","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"RESET":"RESET IN 10 SECONDS"},"RESULT":"SUCCESS"}}
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING,
                                                                COMMAND_STRING,   RESET_CMD,
                                                                ARGUMENTS_STRING, NONE_STRING,
                                                                CODE_STRING,      0,
                                                                RESPONSE_STRING,  RESET_STRING, RESET_IN_5_SECONDS_STRING,
                                                                RESULT_STRING,    SUCCESS_STRING);
    jumpToBootOrResetCb.resetPlease    = TRUE;
    jumpToBootOrResetCb.resetTimestamp = g_ul_ms_ticks;

    // Set the fan speed to max.
    writeU11FanSpeed(U11_FAN_SPEED_SETTING_MAX, NO_JSON_OUTPUT, NULL);
}


// prepareSoftwareResetFunc
static void prepareSoftwareResetFunc(void)
{
    // Prepare for the user-initiated software reset:
    // {"CLI":{"COMMAND":"software_reset","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"RESET":"RESET IN 10 SECONDS"},"RESULT":"SUCCESS"}}
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{%s:%s},%s:%s}}\r\n", CLI_STRING,
                                                                COMMAND_STRING,   SOFTWARE_RESET_CMD,
                                                                ARGUMENTS_STRING, NONE_STRING,
                                                                CODE_STRING,      0,
                                                                RESPONSE_STRING,  RESET_STRING, RESET_IN_5_SECONDS_STRING,
                                                                RESULT_STRING,    SUCCESS_STRING);
    jumpToBootOrResetCb.softwareResetPlease = TRUE;
    jumpToBootOrResetCb.resetTimestamp      = g_ul_ms_ticks;
}


// displayFlashProgramSignatures
//
//
// {"CLI":{"COMMAND":"display_program_signatgures","COMMAND ARG":"NONE","CODE":0,"RESPONSE":{"FLASH SIGNATURES":{"APPLICATION":"0x12345678","BOOTLOADER":"0x87654321"}},"RESULT":"SUCCESS"}}
//
static void displayFlashProgramSignatures(void)
{
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:{", CLI_STRING, COMMAND_STRING, PROGRAM_SIGNATURES_CMD, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, 0, RESPONSE_STRING);
    displayFlashSignatures();
    printf("},%s:%s}}\r\n", RESULT_STRING, SUCCESS_STRING);
}


// findEndOfInputRank
//
// Find the rank into the string where " END-OF-INPUT ..." signature is located.
//
static U32 findEndOfInputRank(char * pUserInput)
{
    U32  iggy;
    U32  signatureLength;
    U32  endOfInputRank = 0;
    U32  maxLengthToSearch;

    signatureLength   = strlen(END_OF_INPUT_SIGNATURE_STRING);
    maxLengthToSearch = strlen(pUserInput) - signatureLength;

    for (iggy = 0; iggy < maxLengthToSearch; iggy++) {
        if (strncmp(&pUserInput[iggy], END_OF_INPUT_SIGNATURE_STRING, signatureLength) == 0) {
            // Found the rank of the end-of-input signature. The signature is preceded by a white space,
            // it is to be excluded, so the value of 1 is subtracted from the rank.
            endOfInputRank = iggy - 1;
            break;
        } else {
            // Not found - keep searching.
        }
    }
    return endOfInputRank;
}


// processUpgradeCommand
//
// Used to upgrade the bootloader. If the user inadvertently sends a HEX file compiled for the application itself,
// the segment address specification would be within the application's own flash zone, in which case, the packet
// is rejected.
//
// The upgrade command is of the form:
//
// upgrade command=<start|data|end|cancel> data=<hex file text data>
//
// The response is json-like:
//
// {"CLI":{"COMMAND":"upgrade","ARGUMENTS":"if any","CODE":0,"RESPONSE":{"somewhat free-form"},"RESULT":"SUCCESS"}}
//
static void processUpgradeCommand(char *pUserInput)
{
    char  *pArguments;
    U32    argumentLength;
    char   pCommandString[32];
    U32    upgradeCommand = UPGRADE_COMMAND_INVALID;
    U32    iggy;
    U32    dataEqualLength = strlen(CLI_ARG_DATA_EQUAL_STRING);
    U32    endOfInputRank;
    U32    startOfInputRank = 0;

    pArguments = &pUserInput[strlen(UPGRADE_CMD) + 1];
    memset(pCommandString,   0, 32);
    
    findComponentStringData(CLI_ARG_COMMAND_STRING,   pCommandString,   pArguments);
    if (strlen(pCommandString) == 0) {
        printf("{%s:{%s:\"%s\",%s:%s;%s:99,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_COMMAND_EQUAL_NOT_FOUND_STRING, RESULT_STRING, FAIL_STRING);
    } else {
        // Check the actual command.
        if (strncmp(pCommandString, CLI_ARG_UPGRADE_START_STRING, strlen(CLI_ARG_UPGRADE_START_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_START;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_DATA_STRING, strlen(CLI_ARG_UPGRADE_DATA_STRING)) == 0) {
            // A data packet. Evaluate the pointer to the very start of the actual text-based data.
            // Start by parsing the packet for "data=" in the user input.


            // It is less than desirable to keep this bit of test code permanently in the build.
			// Nevertheless, this capability is enabled, so the DEBUG_UPGRADE option is removed.
			// To include it as a compile option:
            //    - Project -> AMU Properties
            //    - Toolchain -> ARM/GNU C Compiler -> Symbols
            //    - click the green + button and add DEBUG_UPGRADE  in the pop-up box
            //    - click OK
            //    - SAVE the "AMU" properties
            //    - and Bob's your uncle

            if (debugDropTheNextIncomingDataPacket) { debugDropTheNextIncomingDataPacket = FALSE; return; }

           argumentLength = strlen(pArguments);
            for (iggy = 0; iggy < argumentLength; iggy++) {
                if (iggy == (argumentLength + 1)) {
                    // Gone too far without finding the component
                    printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_DATA_FIELD_TOO_LONG_STRING, RESULT_STRING, FAIL_STRING);
                    break;
                }
                if (strncmp(CLI_ARG_DATA_EQUAL_STRING, &pArguments[iggy], dataEqualLength) == 0) {
                    // Found the data= component in the user input string. Copy the string to the xfer packet data string,
                    // omitting any trailing END-OF-INPUT component. Find the rank of the " END-OF-INPUT 0x00000852 21"
                    // string.
                    endOfInputRank   = findEndOfInputRank(pUserInput);
                    if (endOfInputRank == 0) {
                        endOfInputRank = strlen(pUserInput); // the END-OF-INPUT signature was not included - which is accepted. BUT I DON'T RECALL WHY!!!
                    }
                    startOfInputRank = strlen(UPGRADE_CMD) + 1 + iggy + dataEqualLength;
                    upgradeCommand = UPGRADE_COMMAND_DATA;
                    // The standard SBC/COMM input buffer is no longer require, and will now be treated as a single
                    // packet with a variable number of HEX records. The last character of this packet must be set
                    // to the NULL CHR, else the packet will be rejected.
                    pUserInput[endOfInputRank] = 0;
                    break;
                }
            } // for ...
            if (startOfInputRank == 0x00) {
                printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_DATA_EQUAL_NOT_FOUND_STRING, RESULT_STRING, FAIL_STRING);
            }
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_END_STRING,    strlen(CLI_ARG_UPGRADE_END_STRING))    == 0) {
            upgradeCommand = UPGRADE_COMMAND_END;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_COMMIT_STRING, strlen(CLI_ARG_UPGRADE_COMMIT_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_COMMIT;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_CANCEL_STRING, strlen(CLI_ARG_UPGRADE_CANCEL_STRING)) == 0) {
            upgradeCommand = UPGRADE_COMMAND_CANCEL;
        } else if (strncmp(pCommandString, CLI_ARG_UPGRADE_STATS_STRING,  strlen(CLI_ARG_UPGRADE_STATS_STRING))  == 0) {
            upgradeCommand = UPGRADE_COMMAND_STATS;
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:1,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING, pUserInput, ARGUMENTS_STRING, NONE_STRING, CODE_STRING, RESPONSE_STRING, CLI_INVALID_UPGRADE_COMMAND_STRING, RESULT_STRING, FAIL_STRING);
        }
    }

    if (upgradeCommand == UPGRADE_COMMAND_STATS) {
        displayUpgradeStats();
    } else if (upgradeCommand != UPGRADE_COMMAND_INVALID) {
        // Send this packet to the flashinator state machine for processing.
        upgradeStateMachine(upgradeCommand, &pUserInput[startOfInputRank]);
    }

    return;
}


static void executeNOP(void)
{

    printf("DO NOP\r\n");
    while(1) { __ASM volatile ("nop"); }    //__NOP;
    printf("you should never see this ...\r\n");
}


// disableWatchdog
//
// SHOULD REMOVE THIS: IT DOESN'T WORK, EVEN FOR TESTING.
static void disableWatchdog(void)
{
    WDT->WDT_MR = WDT_MR_WDDIS;
    printf("Watchdog: will no longer BARK ...\r\n");
}


// readOneProgramFlashSignature
//
//
//
static U32 readOneProgramFlashSignature(U32 baseMemoryAddress)
{
    U32  instructionWord = 0xFFFFFFFF;
    U32 *instructionWordPtr;
    U32  flashResult;

    flashResult = flash_set_wait_state(baseMemoryAddress, 6);
    if (flashResult != FLASH_RC_OK) { printf("READ 1 FLASH PROGRAM SIGNATURE FROM 0x%08lX FAIL - flashResult=0x%08lX\r\n", baseMemoryAddress, flashResult); }

    // Point to the specified flash memory base address to start blank-checking from.
    // Pointer is of type 32-bit (Instruction Words).
    instructionWordPtr = (uint32_t *) baseMemoryAddress;

    // Read the current instruction word (IW)
    cpu_irq_disable();
    SCB_DisableICache();
    SCB_DisableDCache();
    instructionWord = *instructionWordPtr;
    SCB_EnableDCache();
    SCB_EnableICache();
    cpu_irq_enable();

    return instructionWord;
}


// readOutProgramFlashSignatures
//
//
static void readOutProgramFlashSignatures(void)
{
    applicationFlashSignature = readOneProgramFlashSignature(APPLICATION_SIGNATURE_ADDRESS);
    bootloaderFlashSignature  = readOneProgramFlashSignature(BOOTLOADER_SIGNATURE_ADDRESS);
}


// setFlashHoldingPattern
//
//
//
//
static void setFlashHoldingPattern(char *pUserInput)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    char  *pArguments;
    char   pLongString[32];
    U32    someLong;

    pArguments = &pUserInput[strlen(SET_FH_PATTERN_CMD) + 1];
    memset(pLongString, 0, 32);
    findComponentStringData(CLI_ARG_LONG_STRING, pLongString, pArguments);
    if (newATOH(pLongString, &someLong)) {
        printf("-> set pattern in flash holding with 0x%08lX\r\n", someLong);
        writeFlashHoldingPattern(someLong);
    } else {
        printf("... BUT LONG %s DID NOT CONVERT TO HEX\r\n", pLongString);
    }
#else
    printf("NOT AVAILABLE\r\n");  
#endif

}


// readFromFlashMemoryIntoHolding
//
// Read some flash memory into the holding block.
//
//
static void readFromFlashMemoryIntoHolding(char *pUserInput)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    char  *pArguments;
    char   pAddressString[32];
    U32    flashAddress;
    BOOL   readResult;

    pArguments = &pUserInput[strlen(READ_FLASH_BLOCK_CMD) + 1];
    memset(pAddressString, 0, 32);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    if (newATOH(pAddressString, &flashAddress)) {
        printf("-> read 1 block of flash from address 0x%08lX to the holding block ... ", flashAddress);
        readResult = readFlashToHoldingBlock(flashAddress);
        if (readResult) { printf("DONE\r\n");     }
        else            { printf("*FAILED*\r\n"); }
    } else {
        printf("... BUT ADDRESS %s DID NOT CONVERT TO HEX\r\n", pAddressString);
    }
#else
    printf("NOT AVAILABLE\r\n");
#endif

}


// writeHoldingBlockToFlash
//
// The contents of the flash holding buffer will be written to flash at the specified flash address.
// The write will fail if the address is not within bounds.
//
static void writeFromHoldingIntoFlashMemory(char *pUserInput)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    char  *pArguments;
    char   pAddressString[32];
    U32    flashAddress;
    BOOL   writeResult;

    pArguments = &pUserInput[strlen(WRITE_FLASH_BLOCK_CMD) + 1];
    memset(pAddressString, 0, 32);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    if (newATOH(pAddressString, &flashAddress)) {
        printf("-> write holding buffer to flash address  0x%08lX ... ", flashAddress);
        writeResult = writeHoldingBlockToFlash(flashAddress);
        if (writeResult) { printf("DONE\r\n");     }
        else             { printf("*FAILED*\r\n"); }
    } else {
        printf("... BUT %s DID NOT CONVERT TO HEX\r\n", pAddressString);
    }
#else
    printf("NOT AVAILABLE\r\n");
#endif

}


// calculateHoldingBlockCrc
//
//
static void calculateAndPrintHoldingBlockCrc(void)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    U32 crc;
    crc = calculateHoldingBlockCrc();
    printf("calculated holding block CRC=0x%08lX\r\n", crc);
#else
    printf("NOT AVAILABLE\r\n");
#endif

}


// isCheckFlashErasedTestCommand
//
// Command takes the following form:
//
// read_flash addr=xxxx len=xxxx
//
//
//
static void isCheckFlashErasedTestCommand(char *pUserInput)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    char  *pArguments;
    char   pAddressString[32];
    char   pLengthString[32];
    U32    flashAddress;
    U32    flashLength;

    pArguments = &pUserInput[strlen(IS_FLASH_RANGE_ERASED_CMD) + 1];
    memset(pAddressString, 0, 32);
    memset(pLengthString,  0, 32);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_LENGTH_STRING,  pLengthString,  pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pLengthString) == 0)) {
        printf("addr and/or len not found\r\n");
    } else {
        flashAddress = atoh(pAddressString);
        flashLength  = atoh(pLengthString);
        printf("CHECK %lu LONGS OF FLASH FROM 0x%08lX: ", flashLength, flashAddress);
        if (isFlashErasedMemoryRange(flashAddress, flashLength)) { printf("FLASH IS ERASED\r\n");       }
        else                                                     { printf("FLASH IS *NOT* ERASED\r\n"); }
    }
#else
    printf("NOT AVAILABLE\r\n");
#endif

}


// readFlashCommand
//
// Command takes the following form:
//
// read_flash addr=xxxx len=xxxx
//
//
//
static void readFlashCommand(char *pUserInput)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pLengthString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    flashAddress;
    U32    flashLength;

    pArguments = &pUserInput[strlen(READ_FLASH_LONG_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pLengthString,  0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_LENGTH_STRING,  pLengthString,  pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pLengthString) == 0)) {
        printf("addr and/or len not found\r\n");
    } else {
        flashAddress = atoh(pAddressString);
        flashLength  = atoh(pLengthString);
        printf("WILL READ %lu BYTES OF FLASH FROM 0x%08lX \r\n", flashLength, flashAddress);
        readFlashMemoryRange(flashAddress, flashLength);
    }
#else
    printf("NOT AVAILABLE\r\n");
#endif

}


// writeFlashLongCommand
////
//
// - flash_init
// - flash_unlock
// - flash_erase_sector
// - flash_write
// - flash_lock
//
//
// Command takes the following form:
//
// write_flash addr=xxxx long=xxxx
//
//
//
static void writeFlashLongCommand(char *pUserInput)
{
#if INCLUDE_CLI_FLASH_COMMANDS == TRUE
    char  *pArguments;
    char   pAddressString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pLongString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    flashAddress;
    U32    flashAddressActualStart;
    U32    flashAddressActualEnd;
    U32    flashLong;
    U32    flashResult;

    pArguments = &pUserInput[strlen(WRITE_FLASH_LONG_CMD) + 1];
    memset(pAddressString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
    memset(pLongString,    0, AMU_COMPONENT_DATA_SIZE_MAX);
    findComponentStringData(CLI_ARG_ADDR_STRING, pAddressString, pArguments);
    findComponentStringData(CLI_ARG_LONG_STRING,    pLongString,    pArguments);

    if ((strlen(pAddressString) == 0) || (strlen(pLongString) == 0)) {
        printf("addr and/or long not found\r\n");
    } else {
        flashAddress = atoh(pAddressString) + IFLASH_ADDR;
        flashLong    = atoh(pLongString);
        flashAddressActualStart = 0;
        flashAddressActualEnd   = 0;
        printf("- write 0x%08lX to flash address 0x%08lX \r\n", flashLong, flashAddress);
        flash_init(FLASH_ACCESS_MODE_128, 6);

        flashResult = flash_unlock(flashAddress, (flashAddress + IFLASH_PAGE_SIZE), &flashAddressActualStart, &flashAddressActualEnd);
        if (flashResult != FLASH_RC_OK) { printf("**UNLOCK [512] FAILED: RC=0x%lX\r\n", flashResult); return;                                                                   }
        else                            { printf("- unlock OK: flashAddressActualStart=0x%lX flashAddressActualEnd=0x%lX\r\n", flashAddressActualStart, flashAddressActualEnd); }

        flashResult = flash_erase_page(flashAddress, IFLASH_ERASE_PAGES_16);
        if (flashResult != FLASH_RC_OK) { printf("***ERASE PAGE FAILED: RC=0x%lX***\r\n", flashResult); return;   }
        else                            { printf("- erase page: OK\r\n");                                         }

        flashResult = flash_write(flashAddress, &flashLong, sizeof(U32), FALSE);
        if (flashResult != FLASH_RC_OK) { printf("***FLASH WRITE FAILED: RC=0x%lX***\r\n", flashResult); return;  }
        else                            { printf("- flash write: OK\r\n");                                        }

        flashResult = flash_lock(flashAddress, (flashAddress + IFLASH_PAGE_SIZE), NULL, NULL);
        if (flashResult != FLASH_RC_OK) { printf("***LOCK FAILED: RC=0x%lX***\r\n", flashResult); return;         }
        else                            { printf("- flash lock: OK\r\n");                                         }

        // Check the security bit: - retval 1 If the security bit is currently set.
        //                         - retval 0 If the security bit is currently cleared.
        //                         - otherwise returns an error code.
        flashResult = flash_is_security_bit_enabled();
        if (flashResult == FLASH_RC_OK) {
            printf("- security bit *must* be enabled\r\n");
            flashResult = flash_enable_security_bit();
            if (flashResult != FLASH_RC_OK) { printf("***ENABLE SECURITY FAILED: RC=0x%lX***\r\n", flashResult); }
            else                            { printf("- enable security: OK\r\n");                               }
        } else { printf("- security bit was enabled\r\n"); }
        printf("- flash write: DONE\r\n");
    }
#else
    printf("NOT AVAILABLE\r\n");
#endif

}


#if INCLUDE_CLI_FLASH_COMMANDS == TRUE

// Include the supporting functions for the CLI-based flSH COMMANDS:


// isFlashErasedMemoryRange
//
// Checks a range of FLASH memory: if any DWORD is not 0xFFFFFFFF then the memory range
//                                 is considered NOT ERASED.
//
// This piece of code taken from some person going by the name of Ken Pergola/Microchip forum.
//
static BOOL isFlashErasedMemoryRange(U32 baseMemoryAddress, U32 numberOfInstructionWords)
{
    BOOL isErased = TRUE;
    U32  instructionWord;
    U32 *instructionWordPtr;

    // Point to the specified flash memory base address to start blank-checking from.
    // Pointer is of type 32-bit (Instruction Words).
    instructionWordPtr = (U32 *)baseMemoryAddress;

    // Range-check argument
    if (numberOfInstructionWords == 0) {
        isErased = FALSE;
    } else {
        // Blank check the number of instruction words (IW) specified
        // (Bail out early on first non-blank instruction word found.)
        while (numberOfInstructionWords-- != 0) {
            // Read the current instruction word (IW)
            instructionWord = *instructionWordPtr++;

            // Is the current instruction word read not erased?
            if (instructionWord != 0xFFFFFFFF) {
                // It is not erased -- bail out
                isErased = FALSE;
                break;
            }
        }
    }
    return isErased;
}


// From Ken Pergola/Microchip forum.
static void readFlashMemoryRange(U32 baseMemoryAddress, U32 numberOfInstructionWords)
{
    U32  flashResult;
    U32  instructionWord;
    U32 *instructionWordPtr;

    // Point to the specified flash memory base address to start blank-checking from.
    // Pointer is of type 32-bit (Instruction Words).
    instructionWordPtr = (uint32_t *) baseMemoryAddress;

    flashResult = flash_set_wait_state(baseMemoryAddress, 6);
    if (flashResult != FLASH_RC_OK) { printf("YAYAYA\r\n"); }

    // Range-check argument
    if (numberOfInstructionWords > 0) {
        while (numberOfInstructionWords-- != 0) {
            // Read the current instruction word (IW)
            instructionWord = 0xFFFFFFFF;
            cpu_irq_disable();
            SCB_DisableICache();
            SCB_DisableDCache();
            instructionWord = *instructionWordPtr++;
            SCB_EnableDCache();
            SCB_EnableICache();
            cpu_irq_enable();
            printf("%08lX ", instructionWord);
        }
        printf("\r\n");
    }
    return;
}


#endif // INCLUDE_CLI_FLASH_COMMANDS


// pokeWatchdog
//
// Restart the SAME70 watchdog timer every so often. Called from main, but also any "controlled" loop where protection
// against a barking watchdog is required. How often? Well, at least more often than its max period of 16 seconds.
// Originally, the watchdog was restarted once every 5 seconds based on a local tick timer. But as checking the timer
// was as onerous as restarting the watchdog anyway, the timer was removed altogether.
//
// During chamber testing: at low temperature (in the -20C range), a WATCHDOG reset was observed. The system would not
// run for more than a few minutes before the SAME70 would reset, reason: WATCHDOG. But only at low temperature during
// chamber testing. Never seen at room temperature. Code inspection, extra loop (for, while) protection added, the low
// temperature WATCHDOG persisted.
//
// What todo, what to do ...
//
// The SAME70 errata data sheet was unearthed, and of the several bugs/workarounds that were described, the most
// applicable bug was that on page 21, section 2.22.1"Watchdog Reset":
//
// 
//     Problem description: "With External Reset Length set to 0 (MR.ERSTL= 0) in the Reset Controller Mode register,
//                          a Watchdog Reset may cause an Infinite Reset loop."
//
//     Recommended workaround: "To ensure a correct Watchdog Reset of the system, the ERSTL field in the
//                             Reset Controller Mode register must be set to a non-zero value ( MR.ERSTL >= 1)."
//
//
// The Apparent application fix to prevent WATCHDOG reset at low temperature consists of writing 1 to the aforementioned
// ERSTL register in the reset controller's "mode" register. However, the statement:
//
//  RSTC->RSTC_MR = RSTC_MR_ERSTL(1);
//
// ... had no effect on system behavior. It has been removed.
//
//
void pokeWatchdog(void)
{
    WDT->WDT_CR   = WATCHDOG_CONTROL_RESTART;
    // RSTC->RSTC_MR = RSTC_MR_ERSTL(1); <- did not change system behavior; did not prevent WATCHDOG at low temperaure.
}




//
//
//  U T I L I T Y      F U N C T I O N S
//
//




// pApparent_strstr
//
//
//
char *pApparent_strstr(char *pSomeString, char *pSearchPart)
{
    char *pLocatedString = NULL;
    U32 iggy;
    U32 searchPartLength = strlen(pSearchPart);

    for (iggy = 0; iggy < SBCCOM_INPUT_BUFFER_LENGTH; iggy++) {
        //
        if (strncmp(&pSomeString[iggy], pSearchPart, searchPartLength) == 0) {
            // Found it.
            pLocatedString = &pSomeString[iggy];
            break;
        }
    }
    return pLocatedString;
}


// Imported from "A Collection of 32-bit CRC Tables and Algorithms" at:
// http://www.mrob.com/pub/comp/crc-all.html
U32 calcCrc32Checksum(U32 crc, U8 *buf, U16 len)
{
    U8 *end;
    crc = ~crc;
    for (end = buf + len; buf < end; ++buf) {
        crc = CRC32_TABLE[(crc ^ *buf) & 0xff] ^ (crc >> 8);
    }
    return ~crc;
}


// isConvertMacString
//
//
//
BOOL isConvertMacString(char *pStringAsMac, U8 *pMac)
{
    U32  iggy;
    U32  pop;
    char pMacChars[13];
    char pTwoBytes[3];
    BOOL converted = TRUE;

    if (strlen(pStringAsMac) == MAX_CHARS_IN_MAC_STRING) {
        if ((pStringAsMac[2] == ':') && (pStringAsMac[5] == ':') && (pStringAsMac[8] == ':') && (pStringAsMac[11] == ':') && (pStringAsMac[14] == ':')) {
            // Hard-coded manual move of the actual mac "digits" only.
            pMacChars[0]  = pStringAsMac[0];
            pMacChars[1]  = pStringAsMac[1];
            pMacChars[2]  = pStringAsMac[3];
            pMacChars[3]  = pStringAsMac[4];
            pMacChars[4]  = pStringAsMac[6];
            pMacChars[5]  = pStringAsMac[7];
            pMacChars[6]  = pStringAsMac[9];
            pMacChars[7]  = pStringAsMac[10];
            pMacChars[8]  = pStringAsMac[12];
            pMacChars[9]  = pStringAsMac[13];
            pMacChars[10] = pStringAsMac[15];
            pMacChars[11] = pStringAsMac[16];
            pMacChars[12] = 0; // Null char
            if (isStringHexDigit(&pMacChars[0])) {
                // So far so good. Convert characters in pairs.
                pTwoBytes[2] = 0; // null-char
                pop          = 0;
                for (iggy = 0; iggy < 12; iggy+=2) {
                    pTwoBytes[0] = pMacChars[iggy];
                    pTwoBytes[1] = pMacChars[iggy+1];
                    pMac[pop++] = atoh(pTwoBytes);
                }
            } else {
                converted = FALSE;
            }
        } else {
            converted = FALSE;
        }
    } else {
        converted = FALSE;
    }
    return converted;
}


// isMacNull
//
// Check the 6 bytes of the MAC: if they are all 0x00, the MAC is NULL.
//
BOOL isMacNull(U8 *pMac)
{
    if ((pMac[0] == 0x00) && (pMac[1] == 0x00) && (pMac[2] == 0x00)
    && (pMac[3] == 0x00) && (pMac[4] == 0x00) && (pMac[5] == 0x00)) { return TRUE;  }
    else                                                             { return FALSE; }
}


// isTwoFloatsEqual
//
// Compares 2 floats for equality, to the specified number of decimal places with round-up.
// Use this function to avoid a compiler warning when directly comparing 2 floats.
//
BOOL isTwoFloatsEqual(F32 thisFloat, F32 thatFloat, U32 numberDecimals)
{
    BOOL iAmEqualToYou = FALSE;
    U32  thisFirstFloatAsLong;
    U32  thatOtherFloatAsLong;

    if (numberDecimals == 1) {
        // Compare 2 floats to 1 decimal place.
        thisFirstFloatAsLong = (U32)( (thisFloat + 0.05) * 10);
        thatOtherFloatAsLong = (U32)( (thatFloat + 0.05) * 10);
        if (thisFirstFloatAsLong == thatOtherFloatAsLong) { iAmEqualToYou = TRUE; }

    } else {
        // For all others, compare 2 floats to 2 decimal places.
        thisFirstFloatAsLong = (U32)( (thisFloat + 0.005) * 100);
        thatOtherFloatAsLong = (U32)( (thatFloat + 0.005) * 100);
        if (thisFirstFloatAsLong == thatOtherFloatAsLong) { iAmEqualToYou = TRUE; }
    }            
    return iAmEqualToYou;
}


// isStringHexDigit
//
// Return TRUE if all chars are hex digits from 0..9, a..f or A..F.
//
BOOL isStringHexDigit(char *pString)
{
    BOOL hexResult = TRUE;
    U32  length;
    U32  iggy;
    char oneChar;

    length = strlen(pString);
    if (length > 0) {
        for (iggy = 0; iggy < length; iggy++) {
            oneChar = pString[iggy];
            // I know, magic numbers, so sue me ...
            // 0..9 = 48..57  decimal
            // a..f = 97..102 decimal
            // A..F = 65..70  decimal
            if ( ((oneChar >= 48) && (oneChar <= 57))
              || ((oneChar >= 97) && (oneChar <= 102))
              || ((oneChar >= 65) && (oneChar <= 70)) ) { /* It's a good hex, keep checking. */      }
            else                                        { hexResult = FALSE; break; /* Bad. Bail. */ }
        }
    } else { hexResult = FALSE; }
    return hexResult;
}


// isStringDecimalDigit
//
// Return TRUE if all chars are digits from 0..9.
//
BOOL isStringDecimalDigit(char *pString)
{
    BOOL digitResult = TRUE;
    U32  length;
    U32  iggy;
    char oneChar;

    length = strlen(pString);
    if (length > 0) {
        for (iggy = 0; iggy < length; iggy++) {
            oneChar = pString[iggy];
            // I know, magic numbers, so sue me ...
            if ((oneChar < 48) || (oneChar > 57)) { digitResult = FALSE; break; }
        } 
    } else { digitResult = FALSE; }
    return digitResult;
}


// longFromFloat
//
// Take a 32-bit float and return it untouched, but as a U32.
// helps avoid pesky compiler warnings.
//
U32 longFromFloat(F32 thisFloat)
{
    U8 *pByte;
    U32 thisLong;
    pByte    = (U8 *)&thisFloat;
    thisLong = (pByte[3] << 24) | (pByte[2] << 16) | (pByte[1] << 8) | (pByte[0]);
    return thisLong;
}


// Direct imported from the SG424 universe.
// Modified to return a BOOL; value written to pointer.
BOOL newATOH(const char *String, U32 *pSomeLong)
{
    BOOL returnValue = TRUE;
    U32  value = 0;
    int  digit;
    char c;
    while ((c = *String++) != '\0') {
        if (c >= '0' && c <= '9')
            digit = (unsigned) (c - '0');
        else if (c >= 'a' && c <= 'f')
            digit = (unsigned) (c - 'a') + 10;
        else if (c >= 'A' && c <= 'F')
            digit = (unsigned) (c - 'A') + 10;
        else
        {
            value=0;
            returnValue = FALSE;
            break;
        }
        value = (value << 4) + digit;
    } // while ...
    // Now write the value.
    *pSomeLong = value;
    return returnValue;
}


// Direct imported from the SG424 universe.
// Assumes that the string contains good stuff.
U32 atoh(const char *String)
{
    U32 value = 0;
    int digit;
    char oneChar;
    while ((oneChar = *String++) != '\0') {
        if (oneChar >= '0' && oneChar <= '9')
            digit = (unsigned) (oneChar - '0');
        else if (oneChar >= 'a' && oneChar <= 'f')
            digit = (unsigned) (oneChar - 'a') + 10;
        else if (oneChar >= 'A' && oneChar <= 'F')
            digit = (unsigned) (oneChar - 'A') + 10;
        else
        {
            printf("BAD string=%s\r\n", String);
            value = 0;
            break;
        }
        value = (value << 4) + digit;
    }
    return value;
}


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
