/**
 *
 * Apparent I2C utilities module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2021 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains utility support routines for the Apparent Gateway PCBA system.
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

//#include "pmc.h" // ???
//#include "ASF/sam/utils/cmsis/same70/include/instance/pioc.h"

#include "usart.h"
#include "ASF/sam/utils/cmsis/same70/include/component/twihs.h"
#include "status_codes.h"
#include "uart_serial.h"

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "board.h"

#include "apparent.h"
#include "poe.h"
#include "i2c.h"


// External function declarations:
extern void mdelay       (uint32_t ul_dly_ticks);


static const t_i2cRegisterMapElement I2C_REGISTER_MAP_LOOKUP [] = { /* [0] POE DEVICE ADDRESSES:*/ { POE_REG_TWIHS_CR,          // control
                                                                                                     POE_REG_TWIHS_MMR,         // master mode
                                                                                                     POE_MMR_READ_SETUP,        // master mode setup
                                                                                                     POE_REG_TWIHS_IADR,        // internal address
                                                                                                     POE_REG_TWIHS_STATUS,      // status
                                                                                                     POE_REG_TWIHS_RHOLD},      // receive holding
                                                                    /* [1] FAN DEVICE ADDRESSES:*/ { TEMP_FAN_REG_TWIHS_CR,     // control
                                                                                                     TEMP_FAN_REG_TWIHS_MMR,    // master mode
                                                                                                     U11_FAN_MMR_READ_SETUP,    // master mode setup
                                                                                                     TEMP_FAN_REG_TWIHS_IADR,   // internal address
                                                                                                     TEMP_FAN_REG_TWIHS_STATUS, // status
                                                                                                     TEMP_FAN_REG_TWIHS_RHOLD}  // receive holding
                                                                  };

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond


// Exported/external functions from this module:


// readI2cStatusUntilReady
//
// A generic I2C/TWIHS facility for any I2C device to read a specified status register
// until the bit selector is set. The bit selector is typically one of:
//
// - TWIHS_SR_TXRDY  = transmit ready
// - TWIHS_SR_RXRDY  = receive ready
// - TWIHS_SR_TXCOMP = transmit complete
//
// but any defined status bit can be passed. If the I2C interface is somehow mangled,
// the loop is unceremoniously but eventually exited.
BOOL readI2cStatusUntilReady(U32 statusAddress, U32 bitSelector)
{
    BOOL readSuccess  = FALSE;
    BOOL keepChecking = TRUE;
    U32  loopCount    = 0;
    U32  status;
    while (keepChecking) {
        status = readSame70Register(statusAddress);
        if ((status & bitSelector) == bitSelector) {
            keepChecking = FALSE;
            readSuccess  = TRUE;
            break; // superfluous but can't hurt.
        }
        // else keep reading.
        mdelay(5);
        pokeWatchdog();
        if (loopCount++ > I2C_STATUS_READ_LOOP_LIMIT_PROTECTION) { incrementBadEventCounter(I2C_LOOP_STATUS_READ_LIMIT_REACHED); readSuccess = FALSE; keepChecking = FALSE; break; }
    } // while ...

    return readSuccess;

}


// i2cReadGenericData
//
// Read 1 or 2 bytes of data over I2C.
//
//
//
//
//
//
BOOL i2cReadGenericData(U32 deviceType, U32 command, U32 dataType, U16 *pData)
{
    U32  holdingValue; // receive holding register value
    U32  statusValue;
    U32  control     = I2C_REGISTER_MAP_LOOKUP[deviceType].control;
    U32  masterMode  = I2C_REGISTER_MAP_LOOKUP[deviceType].masterMode;
    U32  mmrSetup    = I2C_REGISTER_MAP_LOOKUP[deviceType].masterModeSetup;
    U32  iadr        = I2C_REGISTER_MAP_LOOKUP[deviceType].iadr;
    U32  status      = I2C_REGISTER_MAP_LOOKUP[deviceType].status;
    U32  rcvHolding  = I2C_REGISTER_MAP_LOOKUP[deviceType].rcvHolding;
    U8   byteMsb;
    U8   byteLsb;
    BOOL readResult = TRUE;

    // 1: Set the control register: CR = MSEN + SVDIS
    writeSame70Register(control, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS));

    // 2:Set the master mode register for a One-byte internal device address
    writeSame70Register(masterMode, mmrSetup);

    // 3. Set the internal address in the IADR register.
    writeSame70Register(iadr, command);

    // 4: Check RXRDY. The SAME70 data sheet does not show the following that checks RXRDY after setting the IADR.
    // Without it, it has been observed that inconsistent data is be returned, specifically: the last byte from
    // the previous I2C read is returned this time around. Checking RXRDY then reading holding and status fixes this.
    statusValue = readSame70Register(status);
    if ((statusValue & TWIHS_SR_RXRDY) == TWIHS_SR_RXRDY) {
        holdingValue = readSame70Register(rcvHolding);
        statusValue  = readSame70Register(status);
    }

    // Start the transfer by loading START or START/STOP into the control register.
    if (dataType == I2C_DATA_TYPE_BYTE) {
        // 5: Start the transfer by loading START | STOP in the control register.
        writeSame70Register(control, (TWIHS_CR_START | TWIHS_CR_STOP));

        // 6: Read status until RXRDY is clear.
        if (readI2cStatusUntilReady(status, TWIHS_SR_RXRDY)) {

            // 7: Read the receive holding register. This clears TWIHS_SR_RXRDY.
            holdingValue = readSame70Register(rcvHolding);

            // The interaction with the device is complete. Prepare to return the data, in this case: 1 byte.
            *pData = (U16)(holdingValue & 0x000000FF);
        } else { readResult = FALSE; }            
    } else {
        // 5: Start the transfer by loading START only in the control register.
        writeSame70Register(control, TWIHS_CR_START);

        // 6: Read status until RXRDY is clear.
        if (readI2cStatusUntilReady(status, TWIHS_SR_RXRDY)) {

            // 7.1: Read the receive holding register to get the LSB portion of the word.
            holdingValue = readSame70Register(rcvHolding);
            byteLsb = (U8)(holdingValue & 0x000000FF);

            // 7.2: when preparing to read the 2nd byte (i.e., the last byte): load STOP in the control register.
            writeSame70Register(control, TWIHS_CR_STOP);

            // 7.3: Read status until RXRDY is clear.
            if (readI2cStatusUntilReady(status, TWIHS_SR_RXRDY)) {

                // 7.4: Read the receive holding register to get the MSB portion of the word.
                holdingValue = readSame70Register(rcvHolding);
                byteMsb = (U8)(holdingValue & 0x000000FF);

                // The interaction with the device is complete. Prepare to return the data, in this case: 2 bytes.
                *pData = (byteMsb << 8) | byteLsb;
            } else { readResult = FALSE; }
        } else { readResult = FALSE; }     
    }

    if (readResult) {
        // 8: Read status until TXCOMP is clear.
        if (readI2cStatusUntilReady(status, TWIHS_SR_TXCOMP)) { /* nothing to do */ }
        else /* this read-until-ready failed so ... */        { readResult = FALSE; }
    }        

    return readResult;
}


// i2cSuperReadGenericData
//
// Will read 4 bytes from a device over I2C. So far, only tested for use with the TPS23882 POE device.
//
//
//
// TODO: this function is only called with I2C_DEVICE_TYPE_POE as the device type, so it is not validated.
//       The other device type is the temp/fan controller, however that data is not gathered through this
//       function, due to differences in the use of the protocol for the 2 major device types. Both should
//       use the exact same protocol as per the I2C standard, but it fails when accessing info for the temp/fan
//       device. Because of such minor differences, tem[/fan data is managed by updateTemperatureReading()
//       and checkTemperatureForFanSpeed() funcitons in the apparent.c module.
//
//       Consequently, the I2C_REGISTER_MAP_LOOKUP table could be jettisoned. Maybe in a future near you.
//
BOOL i2cSuperReadGenericData(U32 deviceType, U32 command, U32 *pData)
{
    U32  holdingValue; // receive holding register value
    U32  statusValue;
    U32  control     = I2C_REGISTER_MAP_LOOKUP[deviceType].control;
    U32  masterMode  = I2C_REGISTER_MAP_LOOKUP[deviceType].masterMode;
    U32  mmrSetup    = I2C_REGISTER_MAP_LOOKUP[deviceType].masterModeSetup;
    U32  iadr        = I2C_REGISTER_MAP_LOOKUP[deviceType].iadr;
    U32  status      = I2C_REGISTER_MAP_LOOKUP[deviceType].status;
    U32  rcvHolding  = I2C_REGISTER_MAP_LOOKUP[deviceType].rcvHolding;
    U32  data        = 0xFFFFFFFF;
    U8   pByteByte[4];
    U32  iggy;
    BOOL readResult = TRUE;

    // 1: Set the control register: CR = MSEN + SVDIS
    writeSame70Register(control, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS));

    // 2:Set the master mode register for a One-byte internal device address
    writeSame70Register(masterMode, mmrSetup);

    // 3. Set the internal address in the IADR register.
    writeSame70Register(iadr, command);

    // 4: Check RXRDY. The SAME70 data sheet does not show the following that checks RXRDY after setting the IADR.
    // Without it, it has been observed that inconsistent data is be returned, specifically: the last byte from
    // the previous I2C read is returned this time around.  Checking RXRDY then reading holding and status fixes this.
    statusValue = readSame70Register(status);
    if ((statusValue & TWIHS_SR_RXRDY) == TWIHS_SR_RXRDY) {
        holdingValue = readSame70Register(rcvHolding);
        statusValue  = readSame70Register(status);
    }

    memset(&pByteByte[0], 0, 4);

    // 5: Start the transfer by loading START only in the control register.
    writeSame70Register(control, TWIHS_CR_START);

    // Do for all 3 bytes except for the last byte:
    for (iggy = 0; iggy < 3; iggy++) {
        // 6: Read status until RXRDY is clear.
        if (readI2cStatusUntilReady(status, TWIHS_SR_RXRDY)) {
            // 7: Read the receive holding register to get the MSB portion of the word.
            holdingValue = readSame70Register(rcvHolding);
            pByteByte[iggy] = (U8) (holdingValue & 0x000000FF);
        } else { readResult = FALSE; }            
    }

    if (readResult) {

        // 8: When preparing to read the LAST byte: load STOP in the control register.
        writeSame70Register(control, TWIHS_CR_STOP);

        // 9: Read status until RXRDY is clear.
        if (readI2cStatusUntilReady(status, TWIHS_SR_RXRDY)) {

            // 10: Read the receive holding register to get the last byte of the 4-byte read.
            holdingValue = readSame70Register(rcvHolding);
            pByteByte[3] = (U8) (holdingValue & 0x000000FF);

            // The interaction with the device is complete. Prepare to return the data, in this case: 4 bytes.
            data = (pByteByte[0] << 24) | (pByteByte[1] << 16) | (pByteByte[2] << 8) | (pByteByte[3] << 0);

            // Always read status until TXCOMP is clear.
            if (!readI2cStatusUntilReady(status, TWIHS_SR_TXCOMP)) {
                readResult = FALSE;
                data = 0xFFFFFFFF;
            }
        } else { readResult = FALSE; }            
    }    

    // Write the U32 value to the caller's memory, even if the above failed.
    *pData = data;

    return readResult;
}


// poeWriteGenericData
//
// This interface will write 1 or 2 bytes of data over the POE interface.
//
//
//   TODO: return a TRUE or FALSE depending on good read, and write the "data" to a pointer to the variable passed to this function as a parameter.
//
//
BOOL i2cWriteGenericData(U32 command, U32 data, U32 dataType)
{
    U32  twihs1Holding;
    BOOL writeResult = TRUE;

    // 1: Set the control register: CR = MSEN + SVDIS
    writeSame70Register(POE_REG_TWIHS_CR, (TWIHS_CR_MSEN | TWIHS_CR_SVDIS));

    // 2: Set the master mode register: - set internal address size
    //                                  - set device address
    //                                  - MREAD bit = 0 for this write operation
    writeSame70Register(POE_REG_TWIHS_MMR, TWIHS1_MMR_POE_WRITE_SETUP_1BYTE);
    
    // 3. Set the internal address in the IADR register.
    writeSame70Register(POE_REG_TWIHS_IADR, command);

    // Note: when writing over I2C, there is no "START" command.

    if (dataType == I2C_DATA_TYPE_BYTE)
    {
        // 4: Load the THR register with the byte to write.
        twihs1Holding =  data & 0x000000FF;
        writeSame70Register(POE_REG_TWIHS_THOLD, twihs1Holding);

        // 5: Write STOP to the CR register.
        writeSame70Register(POE_REG_TWIHS_CR, TWIHS_CR_STOP);

        // 6: Read status until TXRDY is clear.
        if (readI2cStatusUntilReady(POE_REG_TWIHS_STATUS, TWIHS_SR_TXRDY)) {

            // 7: Check that TXCOMP bit is clear in status register.
            if (readI2cStatusUntilReady(POE_REG_TWIHS_STATUS, TWIHS_SR_TXCOMP)) {
                writeResult = FALSE;
            }
        } else { writeResult = FALSE; }            
    } else {
        // 4.1: Load the THR register with the MSB portion of the word to write.
        twihs1Holding = (data >> 8) & 0x000000FF; // extract the MSB portion of the word
        writeSame70Register(POE_REG_TWIHS_THOLD, twihs1Holding);

        // 5.1: Read status until TXRDY is clear.
        if (readI2cStatusUntilReady(POE_REG_TWIHS_STATUS, TWIHS_SR_TXRDY)) {

            // 4.2: Load the THR register with the LSB portion of the data.
            twihs1Holding = data & 0x000000FF; // extract the LSB portion of the word
            writeSame70Register(POE_REG_TWIHS_THOLD, twihs1Holding);

            // 5: Read status until TXRDY is clear.
            if (readI2cStatusUntilReady(POE_REG_TWIHS_STATUS, TWIHS_SR_TXRDY)) {

                // 6: Write STOP to the CR register.
                writeSame70Register(POE_REG_TWIHS_CR, TWIHS_CR_STOP);

                // 8: Check that TXCOMP bit is clear in status register.
                if (readI2cStatusUntilReady(POE_REG_TWIHS_STATUS, TWIHS_SR_TXCOMP)) { /* nothing to do */  }
                else                                                                { writeResult = FALSE; }
            } else { writeResult = FALSE; }                
        } else { writeResult = FALSE; }            
    }

    return writeResult;

}


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
