/**
* \file
*
* \brief Apparent Application Definitions for the "PRIMARY" KSZ9897 Switch Controller.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _PRIMARY_SWITCH_H_
#define _PRIMARY_SWITCH_H_


// ESSENTIALS TO MAINTAIN CONTROL AND STATUS REGISTERS OF KSZ9897 ETHERNET CHIP CONTROLLER.
//
// There are 2 switch controllers in the system:
//
// - the 1st is referred to as the "PRIMARY" KSZ9897 Switch Controller
// - the 2nd is referred to as the "SECONDARY" KSZ8795 Switch Controller.
//
// This file maintains info for the PRIMARY switch.


// Data sheet Section 5.1.1.1/2/3/4:
#define PRIMARY_GLOBAL_CHIP_ID_0_REGISTER_ADDRESS     0x0000
#define PRIMARY_GLOBAL_CHIP_ID_1_REGISTER_ADDRESS     0x0001
#define PRIMARY_GLOBAL_CHIP_ID_2_REGISTER_ADDRESS     0x0002
#define PRIMARY_GLOBAL_CHIP_ID_3_REGISTER_ADDRESS     0x0003


// The set of register addresses for control register N100 for N=1..5
#define PORT_1_CONTROL_REGISTER_ADDRESS               0x1100 // port 1
#define PORT_2_CONTROL_REGISTER_ADDRESS               0x2100 // port 2
#define PORT_3_CONTROL_REGISTER_ADDRESS               0x3100 // port 3
#define PORT_4_CONTROL_REGISTER_ADDRESS               0x4100 // port 4
#define PORT_5_CONTROL_REGISTER_ADDRESS               0x5100 // port 5 (LAN)

// The set of register addresses for status register N102 for N=1..5
#define PORT_1_STATUS_REGISTER_ADDRESS                0x1102 // port 1
#define PORT_2_STATUS_REGISTER_ADDRESS                0x2102 // port 2
#define PORT_3_STATUS_REGISTER_ADDRESS                0x3102 // port 3
#define PORT_4_STATUS_REGISTER_ADDRESS                0x4102 // port 4
#define PORT_5_STATUS_REGISTER_ADDRESS                0x5102 // port 5 (LAN)


// The set of register addresses to read the primary mac:
#define PRIMARY_MAC_REGISTER_00_ADDRESS               0x0302
#define PRIMARY_MAC_REGISTER_01_ADDRESS               0x0303
#define PRIMARY_MAC_REGISTER_02_ADDRESS               0x0304
#define PRIMARY_MAC_REGISTER_03_ADDRESS               0x0305
#define PRIMARY_MAC_REGISTER_04_ADDRESS               0x0306
#define PRIMARY_MAC_REGISTER_05_ADDRESS               0x0307


// The -> STATUS <- REGISTER BIT DEFINITIONS OF THE PRIMARY CONTROLLER:
// Address: 0xN102 where N= port numbers 1..5 most-significant byte
//          0xN103                           least-significant byte
//
// -> from the data sheet:
//
// - the most significant byte contains various capabilities, they are not managed.
//
// - in the least significant byte, the only bit of interest and relevance is bit [2] = LINK UP/DOWN.
//
//   If any other bit changes, it is detected, reported, but subsequent engineering development
//   would be required for future hardening of the code on both the AMU and the SBC/Gateway.
//
// Bit [2] "Link Status": 1 = link is UP (active device on the other end), 0 = link is DOWN.
#define PORT_STATUS_LINK_UP_DOWN_MASK                      0x0004
#define PORT_STATUS_LINK_IS_UP                             0x0004
#define PORT_STATUS_LINK_IS_DOWN                           0x0000

// All other bits seemingly do not change based on device activity (come and go).
// Based on testing, it is not known why "any other" bit would change. So they are
// simply tracked as "any other bit":
// 1111 1111 1111 1011 = 0xFFFB
#define PORT_STATUS_ANY_OTHER_BIT_MASK                     0xFFFB




// The -> CONTROL <- REGISTER BIT DEFINITIONS OF THE PRIMARY CONTROLLER:
// Address: 0xN100 where N= port numbers 1..5 most-significant  byte containing bits [15:8]
//          0xN101                            least-significant byte containing bits [7:0]

// Bit [15] "PHY Software Reset": 1000 0000 0000 0000
// if set to 1 will reset the PHY. It is self-clearing.
#define PHY_CONTROL_SOFTWARE_RESET_MASK                    0x8000
#define PHY_CONTROL_SOFTWARE_RESET_DEFAULT                 0x0000

// Bit [14] "Local Loopback Mode": 0100 0000 0000 0000
// set this bit to 1 for loopback, 0 = normal operation.
#define PHY_CONTROL_LOCAL_LOOPBACK_MODE_MASK               0x4000
#define PHY_CONTROL_LOCAL_LOOPBACK_MODE_DEFAULT            0x0000

// Bits [6,13] "Speed Select": [6]  = MSB 0000 0000 0100 0000
//                             [13] = LSB 0010 0000 0000 0000
// Speed select is ignored if auto-negotiate is set.
// For the Apparent application:
// - ports 1/2/3/4 speed is set to 100Mb/s  so bits [6,1]  = 01
// - port 5/LAN    speed is set to 1000Mb/s so bits [6,13] = 10
#define PHY_CONTROL_SPEED_SELECT_BIT6_MASK                 0x0040
#define PHY_CONTROL_SPEED_SELECT_BIT13_MASK                0x2000

#define PHY_CONTROL_SPEED_SELECT_BIT6_PORTS_1234_DEFAULT   0x0000
#define PHY_CONTROL_SPEED_SELECT_BIT6_PORT_5LAN_DEFAULT    0x0040
#define PHY_CONTROL_SPEED_SELECT_BIT13_PORTS_1234_DEFAULT  0x2000
#define PHY_CONTROL_SPEED_SELECT_BIT13_PORT_5LAN_DEFAULT   0x0000

#define PHY_CONTROL_SPEED_SELECT_PORTS_1234_DEFAULT        PHY_CONTROL_SPEED_SELECT_BIT6_PORTS_1234_DEFAULT | PHY_CONTROL_SPEED_SELECT_BIT13_PORTS_1234_DEFAULT
#define PHY_CONTROL_SPEED_SELECT_PORT_5LAN_DEFAULT         PHY_CONTROL_SPEED_SELECT_BIT6_PORT_5LAN_DEFAULT  | PHY_CONTROL_SPEED_SELECT_BIT13_PORT_5LAN_DEFAULT

// Bit [12] "Auto-Negotiation Enable": 0001 0000 0000 0000
// 1=auto-negotiation ENABLED, 0=disabled. Value determined by strapping option.
// Auto-negotiation is enabled for port 5/LAN ONLY.
#define PHY_CONTROL_AN_ENABLE_MASK                         0x1000
#define PHY_CONTROL_AN_ENABLE_PORTS_1234_DEFAULT           0x0000
#define PHY_CONTROL_AN_ENABLE_PORT_5LAN_DEFAULT            0x1000

// Bit [11] "Power Down": 0000 1000 0000 0000
// 1=power-down mode, 0=normal operation. Set this bit to 1, then minimum 1msec delay,
// then set to 0 for internal reset. Going from 0 to 1 then to 0 is transitory when PHY is commanded to power down/up.
#define PHY_CONTROL_POWER_DOWN_MODE_MASK                   0x0800
#define PHY_CONTROL_POWER_DOWN_MODE_POWER_DOWN             0x0800
#define PHY_CONTROL_POWER_DOWN_MODE_NORMAL_OPERATION       0x0000
#define PHY_CONTROL_POWER_DOWN_MODE_DEFAULT                PHY_CONTROL_POWER_DOWN_MODE_NORMAL_OPERATION

// Bit [10] "Isolate": 0000 0100 0000 0000
#define PHY_CONTROL_ISOLATE_MASK                           0x0400
#define PHY_CONTROL_ISOLATE_NORMAL_DEFAULT                 0x0000

// Bit [9] "restart auto-negotiation: 0000 0010 0000 0000
#define PHY_CONTROL_RESTART_AN_MASK                        0x0200
#define PHY_CONTROL_RESTART_AN_DEFAULT                     0x0000

// Bit [8] "Duplex Mode": 0000 0001 0000 0000
// 1 = full duplex, 0 = half duplex
#define PHY_CONTROL_DUPLEX_MODE_MASK                       0x0100
#define PHY_CONTROL_DUPLEX_MODE_DEFAULT                    0x0100

// Bit [7] "Collision test": 0000 0000 1000 0000
// 1 = full duplex, 0 = half duplex
#define PHY_CONTROL_COLLISION_TEST_MASK                    0x0010
#define PHY_CONTROL_COLLISION_TEST_DEFAULT                 0x0000

// Bits[5:0] are reserved: 0000 0000 0001 1111
#define PHY_CONTROL_RESERVED_BITS_MASK                     0x001F
#define PHY_CONTROL_RESERVED_BITS_DEFAULT                  0x0000


// *** end of definitions for Control register N101/N102.




// KSZ9897 Primary switch: GLOBAL SWITCH CONTROL OPERATION REGISTERS (0x0300->0x03FF)
//
// Switch Operation register, 0x0300 (8 bits):
//
// Bit [7]    "Double Tag Enable" is ignored by this application, so CLEAR this bit to 0.
// Bits [6:2] are reserved, clear these bits to 0.
// bit [1]    "Soft hardware reset" *** CURRENTLY NOT USED, NOT SURE IF IT IS SELF-CLEARING***
// Bit [0]    "Start Switch": 1 = Switch function is enabled, 0 = Switch function is disabled, i.e.," NO ETHERNET TRAFFIC!!!

#define GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS 0x0300

#define GLOBAL_OPERATION_SOFT_HW_RESET_MASK                0x02
#define GLOBAL_OPERATION_START_SWITCH_MASK                 0x01
#define GLOBAL_OPERATION_START_SWITCH_ENABLED_VALUE        0x01


// OUTPUT CLOCK CONTROL
// Section 5.1.2.2 "Output Clock Control Register" (data sheet page 70):
//
// Address: 0x0103 size: 8 bits
// Bit [7..5] are reserved (RO)
// Bit [4..2] are reserved (RW)
// bit [1]    1 = CLKO output clock is enabled 0 = CLKO output is DISABLED
// Bit [0]    1 = 125Mhz CLKO output clock   0 = 25Mhz CLKO output clock

#define GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS   0x0103

#define GLOBAL_OPERATION_CLKO_OUTPUT_ENABLE_MASK           0x02
#define GLOBAL_OPERATION_CLKO_OUTPUT_DISABLE_MASK          0x00
#define GLOBAL_OPERATION_CLKO_125MHZ_OUTPUT_MASK           0x01   // <- no no no, not for the Apparent application
#define GLOBAL_OPERATION_CLKO_25MHZ_OUTPUT_MASK            0x00   // <- yes yes yes, for the Appardent application

#define ENABLE_CLKO_OUTPUT_VALUE                           (GLOBAL_OPERATION_CLKO_OUTPUT_ENABLE_MASK  | GLOBAL_OPERATION_CLKO_25MHZ_OUTPUT_MASK)
#define DISABLE_CLKO_OUTPUT_VALUE                          (GLOBAL_OPERATION_CLKO_OUTPUT_DISABLE_MASK | GLOBAL_OPERATION_CLKO_25MHZ_OUTPUT_MASK)

// The KSZ9897 supports 5 "external" ports: 4 general purpose "downstream" ports [0], [1], [2], [3] and the Gateway/LAN port [4]:
#define PRIMARY_SWITCH_PORT_NUMBER_MIN                     1
#define PRIMARY_SWITCH_PORT_NUMBER_MAX                     5
#define NUMBER_PRIMARY_SWITCH_PORTS                        5

// Primary switch MIB INDEx values. 2 sets:
// - 1st set are simple 30-bit counters,
// - the 2nd set are 36-bit counters.
#define PRIMARY_SWITCH_MIB_INDEX_MIN             0x01
#define PRIMARY_SWITCH_MIB_INDEX_MAX             0x1F
#define PRIMARY_SWITCH_36BIT_MIB_INDEX_MIN       0x80
#define PRIMARY_SWITCH_36BIT_MIB_INDEX_MAX       0x83

#define PRIMARY_READ_COMMAND_AS_BIT_MASK         0x011     // <- not used
#define PRIMARY_WRITE_COMMANDAS__BIT_MASK        0x010     // <- not used
#define PRIMARY_READ_COMMAND                     0x03
#define PRIMARY_WRITE_COMMAND                    0x02


// For use implementing aspects of the KSZ9897 ERRATA:
#define PHY_MMD_SETUP_REGISTER_BASE_ADDRESS_011A 0x011A
#define PHY_MMD_SETUP_REGISTER_BASE_ADDRESS_DNC  0x011B  // <- do not care: base 0x011A written as a 16-byte WORD
#define PHY_MMD_DATA_REGISTER_BASE_ADDRESS_011C  0x011C
#define PHY_MMD_DATA_REGISTER_BASE_ADDRESS_DNC   0x011D  // <- do not care: base 0x011C written as a 16-byte WORD

// For setup: only REGISTER and DATA are used:
// *** USING THESE MADE THE CODE REALLY UGLY, HARD TO VISUALIZE *** 
#define PHY_MMD_SETUP_MODE_REGISTER_VALUE        0x0000
#define PHY_MMD_SETUP_MODE_DATA_VALUE            0x4000
#define PHY_MMD_SETUP_DEVICE_ADDRESS_MASK        0x001F



// COMMAND COMMAND COMMAND COMMAND COMMAND 

// When constructing the 40-bit/5-byte set to access the primary registers for read and write:
// YYY = the 3-bit KSZ command
// XXXXXXXXXXXXXXXX = 16-bit register address
//
// - the YYY 3-bit command is placed in the 3 most significant bits of byte [0]
// - the upper 5 bits [15:11] of the register address occupy the lower 5 bits of byte[1]
// - the 8 bits [10:3] of the register address occupy byte [2]
// - and the last 3 bits [2:0] are placed in the upper 3 bits of byte [3]
// 
// YYY00000  000XXXXX XXXXXXXX XXX00000 DDDDDDDD
// [command] [addr 0] [addr 1] [addr 2]
#define COMMAND_FIELD_BIT_LEFT_SHIFT             5   // shift the 3-bit read/write command to the left for placement in byte [0]
#define ADDRESS_PART_0_RIGHT_SHIFT               11  // shift the 16-bit address to the right by these many bits for placement in byte [1]
#define ADDRESS_PART_1_RIGHT_SHIFT               3   // shift the 16-bit address to the right by these many bits for placement in byte [2]
#define ADDRESS_PART_2_LEFT_SHIFT                5   // shift the 16-bit address to the left by these many bits for placement in byte [3]




// MIB MIB  MIB  MIB  MIB  MIB  MIB  MIB  MIB  MIB 

// Base MIB addresses when extracting MIB counters. These are used in conjunction with the "port key" (1..5)
// that is used to construct the actual register address based on the port.
#define MIB_SET_MIB_READ_ENABLE_BASE_ADDRESS_N500     0x0500
#define MIB_SET_MIB_INDEX_BASE_ADDRESS_N501           0x0501
#define MIB_COUNTER_BYTE_4_BASE_ADDRESS_N503          0x0503
#define MIB_COUNTER_BYTE_3_BASE_ADDRESS_N504          0x0504
#define MIB_COUNTER_BYTE_2_BASE_ADDRESS_N505          0x0505
#define MIB_COUNTER_BYTE_1_BASE_ADDRESS_N506          0x0506
#define MIB_COUNTER_BYTE_0_BASE_ADDRESS_N507          0x0507


// Bit settings for 0xN500 MIB Control register. The bit location as specified in the KSZ9897
// data sheet is translated to a bit position in an 8-bit register of 0xN500, N501, N502, N503:
//
// - "MIB Counter Overflow Indication": bit [31] of the CONTROL MIB or 0x80 of N500 register
// - reserved [30:2]:                   don't touch me ...
// - "MIB Read Enable/Count valid":     bit [25] of the CONTROL MIB or 0x02 of N500 register
// - "MIB Flush and Freeze  Enable":    bit [24] of the CONTROL MIB or 0x01 of N500 register
#define MIB_COUNTER_OVERFLOW_INDICATION_BIT_MASK 0x80
#define MIB_READ_ENABLE_COUNT_VALID_BIT_MASK     0x02
#define MIB_FLUSH_AND_FREEZE_ENABLE_BIT_MASK     0x01


// To support the FLUSH, FREEZE and UNFREEZE MIB counter operation in the "Switch MIB Control Register", address 0x0336
#define MIB_CONTROL_FLUSH_MIB_COUNTERS_MASK      0x80
#define MIB_CONTROL_FREEZE_MIB_COUNTERS_MASK     0x40
#define SWITCH_MIB_CONTROL_REGISTER_ADDRESS      0x0336


// Primary switch info, on a per-port basis.
typedef struct s_primarySwitchInfo
{
    U16  control;                 // value of the control register
    U16  status;                  // value of the status register
    BOOL portPoweredUp;           // configuration from the gateway
    BOOL restored;                // control reg was restored to configured value
} t_primarySwitchInfo;


// ALU


#define MAX_PRIMARY_ALU_INDEX                    0x0FFF


// The ALU : MAC Address Lookup section.


// Define the register addresses of the ALU.


//              ***** SETUP *****
// From section 5.1.5.6 "ALU Table Index 0 Register" (page 102):
// - bit  [31]     = RESERVED, always 0 for the Apparent application
// - bits [30..23] = RESERVED, always 0 for the Apparent application
// - bits [22:16]  = FID index unused by the Apparent application
// - bits [15:0]   = upper-most 2 bytes of the MAC:
//                 = 00 00 for read by sequence  
//                 = MAC[0..1] when performing a read/search with MAC-as-index.
#define ALU_TABLE_INDEX_0410_REGISTER_ADDRESS         0x0410
#define ALU_TABLE_INDEX_0411_REGISTER_ADDRESS         0x0411
#define ALU_TABLE_INDEX_0412_REGISTER_ADDRESS         0x0412
#define ALU_TABLE_INDEX_0413_REGISTER_ADDRESS         0x0413
// BIT, MASKS and VALUES:
#define ALU_TABLE_INDEX_0410_DEFAULT_VALUE            0x00
#define ALU_TABLE_INDEX_0411_DEFAULT_VALUE            0x00
#define ALU_TABLE_INDEX_0412_DEFAULT_VALUE            0x00
#define ALU_TABLE_INDEX_0413_DEFAULT_VALUE            0x00

#define  ALU_TABLE_INDEX_0411_FID_INDEX_MASK          0b01111111


// From section 5.1.5.7 "ALU Table Index 1 Register" (page 103):
//
// If read/search the ALU by MAC: 
//     - bits [31:0] = remaining 4 bytes of the MAC (first 2 bytes of MAC are in 0x0412/0x0413)
//
// If "direct addressing by index":
//     - bits [11:0] = index the address lookup table
//                     so 0b111111111111 = 0x0FFF = 4095 (12 bits).
//                     reg 0x416 contains the upper 4 bits of the index
//                     reg 0x417 contains the lower 8 bits of the index
#define ALU_TABLE_INDEX_0414_REGISTER_ADDRESS         0x0414
#define ALU_TABLE_INDEX_0415_REGISTER_ADDRESS         0x0415
#define ALU_TABLE_INDEX_0416_REGISTER_ADDRESS         0x0416
#define ALU_TABLE_INDEX_0417_REGISTER_ADDRESS         0x0417
// BITS, MASKS and VALUES:
#define ALU_TABLE_INDEX_0414_DEFAULT_VALUE            0x00
#define ALU_TABLE_INDEX_0415_DEFAULT_VALUE            0x00

#define ALU_TABLE_DIRECT_INDEX_MASK                   0b0000111111111111
#define ALU_TABLE_INDEX_0416_INDEX_RIGHT_SHIFT        8     // 0x0416 register: shift the index the RIGHT, keeping the upper 8 bits
#define ALU_TABLE_INDEX_0417_INDEX_MASK               0xFF  // 0x0417 register: no shift, just keep the lower 8 bits



//              ***** CONTROL *****
// From section 5.1.5.8 "ALU Table Access Control Register: (page 103):
// - bits [31..30] = RESERVED
// - bits [29:16]  = VALID_COUNT (14 bits)
// - bits [15:8]   = RESERVED
// - bit[7]        = START_FINISH: 1 = start the action 0 = action finished
// - bit[6]        = SEARCH VALID: 1 = next valid entry is ready to read 0 = next valid entry is not ready <- for SEARCH OPERATION , NOT USED BY THE APPARENT APPLICATION
// - bit[5]        = VALID_ENTRY_OR_SEARCH_END: 1 = next valid entry is ready 0 = not ready
// - bits[4:3]     = reserved
// - bit[2]        = DIRECT - for debug use only according to the data sheet
// - bits[1:0]     = ACTION: 00=no op 01=write 10=read 11=search.
#define ALU_ACCESS_CTRL_0418_REGISTER_ADDRESS         0x0418
#define ALU_ACCESS_CTRL_0419_REGISTER_ADDRESS         0x0419
#define ALU_ACCESS_CTRL_041A_REGISTER_ADDRESS         0x041A
#define ALU_ACCESS_CTRL_041B_REGISTER_ADDRESS         0x041B

// BITS, MASKS and and VALUES:
#define ALU_ACCESS_CTRL_0419_VALID_COUNT_MASK         0x03FFF
// 0418 mask and shift HERE!
// 0419 mask and shift HERE!

#define ALU_ACCESS_CTRL_041B_START_FINISH_MASK        0b10000000
#define ALU_ACCESS_CTRL_041B_ACTION_START_VALUE       0b10000000
#define ALU_ACCESS_CTRL_041B_ACTION_FINISHED_VALUE    0b00000000

#define ALU_ACCESS_CTRL_041B_VALID_MASK               0b01000000
#define ALU_ACCESS_CTRL_041B_VALID_READY_VALUE        0b01000000
#define ALU_ACCESS_CTRL_041B_VALID_NOT_READY_VALUE    0b00000000

#define ALU_ACCESS_CTRL_041B_SEARCH_MASK              0b00100000
#define ALU_ACCESS_CTRL_041B_SEARCH_READY_VALUE       0b00100000
#define ALU_ACCESS_CTRL_041B_SEARCH_NOT_READY_VALUE   0b00000000

#define ALU_ACCESS_CTRL_041B_DIRECT_MASK              0b00000100
#define ALU_ACCESS_CTRL_041B_BY_DIRECT_ADDR           0b00000100
#define ALU_ACCESS_CTRL_041B_BY_HASH_FUNCTION         0b00000000

#define ALU_ACCESS_CTRL_041B_ACTION_MASK              0b00000011
#define ALU_ACCESS_CTRL_041B_NO_OP_VALUE              0b00000000
#define ALU_ACCESS_CTRL_041B_WRITE_VALUE              0b00000001
#define ALU_ACCESS_CTRL_041B_READ_VALUE               0b00000010
#define ALU_ACCESS_CTRL_041B_SEARCH_VALUE             0b00000011


// When reading register 0x041B: waiting for the START_FINISH bit mask to clear,
// limit the number of reads to the following:
#define START_FINISH_READ_LIMIT                               10

// From section 5.1.5.9 "Static Address and Reserved Multicast Table Control Register" (page 104):
#define ALU_STATIC_CONTROL_041C_REGISTER_ADDRESS      0x041C
#define ALU_STATIC_CONTROL_041D_REGISTER_ADDRESS      0x041D
#define ALU_STATIC_CONTROL_041E_REGISTER_ADDRESS      0x041E
#define ALU_STATIC_CONTROL_041F_REGISTER_ADDRESS      0x041F

// BIT definitions for these 4 registers:
// - bits[31:22] = reserved so 0x041C is not written
// - bits 21:16] = TABLE INDEX (0x041D). The 1st 2 bits are reserved, so TABLE INDEX uses the remaining/lower 6 bits
//                                       The Apparent application only writes 1 static LAN into the ALU, and will
//                                       write it to entry [0].
// - bits [18:8] = reserved, so 0x041E is not written
// - bit[7]      = START_FINISH    <- 0x041F
// - bits [6:2]  = reserved        <- 0x041F
// - bit[1]      = TABLE SELECT    <- 0x041F
// - bit[0]      = ACTION          <- 0x041F
#define ALU_STATIC_CONTROL_041D_TABLE_INDEX_MASK       0b00001111
#define ALU_STATIC_CONTROL_041D_TABLE_INDEX_DEFAULT    0b00000000

#define ALU_STATIC_CONTROL_041F_START_FINISH_MASK      0b10000000
#define ALU_STATIC_CONTROL_041F_START_FINISH_START     0b10000000
#define ALU_STATIC_CONTROL_041F_START_FINISH_DONE      0b00000000

#define ALU_STATIC_CONTROL_041F_TABLE_SELECT_MASK      0b00000010
#define ALU_STATIC_CONTROL_041F_TABLE_SELECT_MCAST     0b00000010
#define ALU_STATIC_CONTROL_041F_TABLE_SELECT_STATIC    0b00000000

#define ALU_STATIC_CONTROL_041F_ACTION_MASK            0b00000001
#define ALU_STATIC_CONTROL_041F_ACTION_READ            0b00000001
#define ALU_STATIC_CONTROL_041F_ACTION_WRITE           0b00000000


//              ***** DATA *****
// From section 5.3.1.4 "ALU Table Entry 1 Register" (page 157):
//
// This register set is for static ALU entries.
//
#define ALU_DATA_ENTRY_0420_REGISTER_ADDRESS          0x0420
#define ALU_DATA_ENTRY_0421_REGISTER_ADDRESS          0x0421
#define ALU_DATA_ENTRY_0422_REGISTER_ADDRESS          0x0422
#define ALU_DATA_ENTRY_0423_REGISTER_ADDRESS          0x0423

// BIT definitions for these 4 registers:
// (only the STATIC bit is used, the rest: unused, unloved).
// - bit[31] = STATIC (= 1: STATIC, 0: DYNAMIC)
#define ALU_DATA_ENTRY_0420_STATIC_MASK               0b10000000
#define ALU_DATA_ENTRY_0420_STATIC_ENTRY              0b10000000
#define ALU_DATA_ENTRY_0420_DYNAMIC_ENTRY             0b00000000


// From section 5.3.1.5 "ALU Table Entry 2 Register" (page 157):
// for bit definitions for these 4 registers.
// For the Apparent application, the 4th register contains the port number the read result.
#define ALU_DATA_ENTRY_0424_REGISTER_ADDRESS           0x0424
#define ALU_DATA_ENTRY_0425_REGISTER_ADDRESS           0x0425
#define ALU_DATA_ENTRY_0426_REGISTER_ADDRESS           0x0426
#define ALU_DATA_ENTRY_0427_REGISTER_ADDRESS           0x0427
// BITS:
// ALU Table Entry 2 Register (5.3.1.5):
// - bit[31]    = OVERRIDE, unused
// - bits[30:7] = RESERVED
// - bits[6:0]  = PORT FORWARD. Each bit corresponds to a device port.
//                bit[0] = port 1
//                bit[1] = port 2
//                bit[2] = port 3
//                bit[3] = port 4
//                bit[4] = port 5/LAN
//                bit[5] = port 6/RGMII
#define ALU_DATA_ENTRY_0427_PORT_FORWARD_MASK         0b01111111
#define ALU_DATA_ENTRY_0427_PORT_1                    0b00000001
#define ALU_DATA_ENTRY_0427_PORT_2                    0b00000010
#define ALU_DATA_ENTRY_0427_PORT_3                    0b00000100
#define ALU_DATA_ENTRY_0427_PORT_4                    0b00001000
#define ALU_DATA_ENTRY_0427_PORT_5                    0b00010000
#define ALU_DATA_ENTRY_0427_PORT_6                    0b00100000


// From section 5.3.1.6 "ALU Table Entry 3 Register" (page 158):
// For the Apparent application: the 3rd/4th registers contain the MAC[0] and MAC[1] read result.
#define ALU_DATA_ENTRY_0428_REGISTER_ADDRESS          0x0428
#define ALU_DATA_ENTRY_0429_REGISTER_ADDRESS          0x0429
#define ALU_DATA_ENTRY_042A_REGISTER_ADDRESS          0x042A
#define ALU_DATA_ENTRY_042B_REGISTER_ADDRESS          0x042B
// BITS:
// - bits[31:23] = RESERVED
// - bits[22:16] = FID ("filter ID"), unused
// - bit[15:0]   = MAC[47:32], i.e., the 1st 2 most significant bytes of the MAC in 0x042A and 0x042B
#define ALU_DATA_ENTRY_0429_FID_MASK                  0b01111111


// From section 5.1.5.13: ALU Table Entry 4.
// From section 5.3.1.7 "ALU Table Entry 2 Register"
// for bit definitions for these 4 registers.
// For the Apparent application: these 4 registers contain the MAC[2..5] read result.
#define ALU_DATA_ENTRY_042C_REGISTER_ADDRESS          0x042C
#define ALU_DATA_ENTRY_042D_REGISTER_ADDRESS          0x042D
#define ALU_DATA_ENTRY_042E_REGISTER_ADDRESS          0x042E
#define ALU_DATA_ENTRY_042F_REGISTER_ADDRESS          0x042F


// When an external interface to the PRIMARY SWITCH module reads form the AMU by MAC-AS-INDEX,
// the results are written to the following define data block:
typedef struct s_macAsIndexReadResults
{
    U16   validCount;
    U8    portForward;
    U8    kszPort;
    char *pDownstreamPort;
    U32   elapsed;
    U8   *pMac;
    BOOL  secondAluReadRequired;
} t_macAsIndexReadResults;





// External prototypes:
void  readPrimaryGlobalRegisters            (void);
U8    readByteFromPrimaryByAddress          (U16 registerAddress);
void  writeByteToPrimaryByAddress           (U16 registerAddress, U8  someByte);
void  writeWordToPrimaryByAddress           (U16 registerAddress, U16 someWord);

void  readPrimaryPortControlAndStatusInfo   (void);
U16   getPrimaryStatusByPortIndex           (U32 portIndex);
U16   getPrimaryControlByPortIndex          (U32 portIndex);

BOOL  isPrimaryLinkStatusLinkUp             (U16 statusRegister);
BOOL  isPrimaryLinkControlPoweredUp         (U16 controlRegister);

void  initPrimarySwitchData                 (void);
void  configurePrimarySwitchController      (void);
void  configurePrimaryPortPowerBit          (U16 portIndex, U8 portKey, BOOL enablePort);
void  userLanPortUpDown                     (U32 portCommand);

void  checkPrimaryEthernetService           (void);
void  checkPrimaryEthernetPort              (U32 portRank);
void  checkPrimaryEthernetPortControlRegister    (U32 portRank);
void  checkPrimaryEthernetPortStatusRegister     (U32 portRank);
BOOL  isCurrentPrimaryLinkStatusUp               (U32 portRank);

void  disablePrimaryEthernetService         (void);
void  primaryControllerReset                (void);

void  readPrimaryMac                        (void);
U32   readFromPrimarySwitch                 (const U8 *pFiveBytes);
void  writeByteToPrimarySwitch              (const U8* pFiveBytes);
void  writeWordToPrimarySwitch              (const U8* pSixBytes);
void  readUsart0ModeStatus                  (void);

void  initPrimaryMibCommandSequences        (void);
void  getPrimarySwitchMibCounter            (U16 primaryPortNumber, U8 mibIndex, char *pPortNumberString);

void  enableFlushFreezeByPortKey            (U16 primaryPortNumber, char *pPortNumberString);
void  disableFlushFreezeByPortKey           (U16 primaryPortNumber, char *pPortNumberString);

void  flushPrimarySwitchMibCounters         (void);
void  freezePrimarySwitchMibCounters        (void);
void  unfreezePrimarySwitchMibCounters      (void);

void  enable25MhzClockOutput                (U16 whodunit);
void  disable25MhzClockOutput               (U16 whodunit);


// ALU:
void  userReadAluPrimaryByIndex             (char *pUserInput);
BOOL  isReadAluPrimaryControlByIndex        (U32 aluIndex,     U16 *pValidCount);
U8    readAluPrimaryPortForwardRegister     (void);
void  readAluPrimaryDataRegisters           (U8 *pMac);
BOOL  isConfigPrimaryStaticAluMac           (U8 *pMac);
BOOL  isReadPrimaryStaticInfo               (U16 tableIndex, U8* pPortForward, U8* pMac);

void  userReadAluPrimaryWithMacAsIndex      (char *pUserInput);
void  userReadAluPrimaryRange               (char *pUserInput);
void  readAluPrimaryWithMacAsIndex          (U8   *pMac, t_macAsIndexReadResults *pReadResults);
void  macResolutionParamUpdate              (char *pUserInput);

char *getPrimaryPortStringFromPortForward   (U8    portForward);

/*----------------------------------------------------------------------------*/
#endif   /* _PRIMARY_SWITCH_H_ */
