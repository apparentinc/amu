/**
 *
 * Apparent MAC Resolution utilities and state machine module on the AMU (Apparent Management Unit).
 *
 * Copyright (c) 2022 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains the state machine and utility support routines for the Apparent MAC Resolution feature..
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include "asf.h"
#include "apparent_strings.h"
#include "apparent.h"
#include "primary_switch.h"
#include "secondary_switch.h"
#include "alu_mac_info.h"
#include "mac_resolution.h"


// In main.c module:
extern void mdelay       (uint32_t ul_dly_ticks);
extern U32  g_ul_ms_ticks;
extern char  pCraftPortDebugData[];
extern const U8   PORT_KEY_TO_PORT_INDEX_LOOKUP[];


// Static Module prototypes:


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond




static t_getAluMacInfoCb getAluMacInfoCb;



const char ALU_SM_STATE_IDLE_STRING[]     = "\"IDLE\"";
const char ALU_SM_STATE_LAUNCH_STRING[]   = "\"LAUNCH\"";
const char ALU_SM_STATE_PRI_READ_STRING[] = "\"PRI READ\"";
const char ALU_SM_STATE_SEC_READ_STRING[] = "\"SEC READ\"";

const char * STATE_INDEX_TO_STATE_STRING_LOOKUP[] = { (const char *)&ALU_SM_STATE_IDLE_STRING,     // key [0]
                                                      (const char *)&ALU_SM_STATE_LAUNCH_STRING,   // key [1]
                                                      (const char *)&ALU_SM_STATE_PRI_READ_STRING, // key [2]
                                                      (const char *)&ALU_SM_STATE_SEC_READ_STRING, // key [3]
                                                    };


// Exported/external functions from this module:


// LOCAL FUNCTIONS:
U8 getKszPortForwardFromPortKey(U32 portKey);


// initAluMacInfoData
//
// Init the data that controls the GET ALU MAC INFO feature and state machine:
//
//
void initAluMacInfoData(void)
{
    getAluMacInfoCb.stateMachineState      = ALU_MAC_INFO_STATE_DEAD_QUIET;
    getAluMacInfoCb.aluGetType             = ALU_GET_TYPE_UNKNOWN;
    getAluMacInfoCb.pUserCommand           = NULL;
    getAluMacInfoCb.userPort               = 0;
    getAluMacInfoCb.aluStartIndex          = 0xFFFFFFFF;
    getAluMacInfoCb.startReadTick          = 0;
    getAluMacInfoCb.lastReadTick           = 0;
    getAluMacInfoCb.portAsKey              = 0xFFFFFFFF;
    getAluMacInfoCb.portAsIndex            = 0xFFFFFFFF;
    getAluMacInfoCb.kszValidCount          = 0;
    getAluMacInfoCb.kszPortForward         = 0xFF;
    getAluMacInfoCb.lastKszPortForwardRead = 0xFF;
    getAluMacInfoCb.controller             = UNKNOWN_ETHERNET_CONTROLLER;
    getAluMacInfoCb.maxAluIndex            = 0xFFFFFFFF;
    getAluMacInfoCb.current_index          = 0xFFFFFFFF;
    memset(&getAluMacInfoCb.mac[0], 0x00, 6);
}


// cancelGetAluMacResult
//
//
//
// {"CLI":{"COMMAND":"cancel_get_alu_mac_info","ARGUMENTS":"NONE","CODE":0,"RESULT":"SUCCESS","RESPONSE":{"PROCESS":"CANCELED"}}}
void cancelGetAluMacResult(void)
{
    getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_IDLE;
    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
           CLI_STRING, COMMAND_STRING,   CANCEL_GET_ALU_MAC_RESULT_CMD,
                       ARGUMENTS_STRING, NONE_STRING,
                       CODE_STRING,      0,
                       RESULT_STRING,    SUCCESS_STRING,
                       RESPONSE_STRING,  PROCESS_STRING, CANCELED_STRING);
}



// getKszPortForwardFromPortKey
//
// "Port forward" is the terminology used in the KSZ data sheet.
//
// On the primary controller, port key:   0 = LAN port          -> ksz port forward = 0x10 (0b00010000)
//                                        1 = downstream port 1 -> ksz port forward = 0x01 (0b00000001)
//                                        2 = downstream port 2 -> ksz port forward = 0x02 (0b00000010)
//                                        3 = downstream port 3 -> ksz port forward = 0x04 (0b00000100)
//                                        4 = downstream port 4 -> ksz port forward = 0x08 (0b00001000)
// On the secondary controller, port key: 5 = downstream port 5 -> ksz port forward = 0b000 (0b00000000) (bits [58..56] 3 bits in register 0x71
//                                        6 = downstream port 6 -> ksz port forward = 0b010 (0b00000000)
//                                        7 = downstream port 7 -> ksz port forward = 0b011 (0b00000000)
//                                        8 = downstream port 8 -> ksz port forward = 0b100 (0b00000000)
U8 getKszPortForwardFromPortKey(U32 portKey)
{
    U8 kszPortForward;
    if      (portKey == 0) {kszPortForward = 0x10; } // 0 = LAN port          -> ksz port forward = 0x10  (0b00010000)
    else if (portKey == 1) {kszPortForward = 0x01; } // 1 = downstream port 1 -> ksz port forward = 0x01  (0b00000001)
    else if (portKey == 2) {kszPortForward = 0x02; } // 2 = downstream port 2 -> ksz port forward = 0x02  (0b00000010)
    else if (portKey == 3) {kszPortForward = 0x04; } // 3 = downstream port 3 -> ksz port forward = 0x04  (0b00000100)
    else if (portKey == 4) {kszPortForward = 0x08; } // 4 = downstream port 4 -> ksz port forward = 0x08  (0b00001000)
    else if (portKey == 5) {kszPortForward = 0x00; } // 5 = downstream port 5 -> ksz port forward = 0b000 (0b00000000) (bits [58..56] 3 bits in register 0x71
    else if (portKey == 6) {kszPortForward = 0x02; } // 6 = downstream port 6 -> ksz port forward = 0b010 (0b00000000)
    else if (portKey == 7) {kszPortForward = 0x03; } // 7 = downstream port 7 -> ksz port forward = 0b011 (0b00000000)
    else if (portKey == 8) {kszPortForward = 0x04; } // 8 = downstream port 8 -> ksz port forward = 0b100 (0b00000000)
    else                   {kszPortForward = 0xFF; } // hmmmmm ...
    return kszPortForward;
}


// launchAluInfoByPortIndex
//
// This functions launches the "GET ALUMAC INFO" state machine. The state machine must be IDLE.
// So too, must the AMU RESOLUTiON state machine. If so, the state machine can be launched.
//
// The CLI command is of the form:  get_alu_mac_by_port_index port=<1..8|LAN> index=<0..4095 | 0..1023>
//
// If the process is launched, respond with:
//
// {"CLI":{"COMMAND":"launch_alu_info_by_port_index","ARGUMENTS":"port=1","CODE":0,"RESULT":"SUCCESS","RESPONSE":{"PROCESS":"LAUNCHED","CONTROLLER":"<PRIMARY|SECONDARY>,"PORT KEY":2,"PORT INDEX":1,"MAX ALU INDEX":4095}}}
//
// If the process failed to launch, one of the following is returned:
// 
// {"CLI":{"COMMAND":"get_alu_mac_by_port_index","ARGUMENTS":"port=1","CODE":0,"RESULT":"FAIL","RESPONSE":{"REASON":"FAIL","INFO":"ALU MAC RUNNING"}}}
// {"CLI":{"COMMAND":"get_alu_mac_by_port_index","ARGUMENTS":"port=1","CODE":0,"RESULT":"FAIL","RESPONSE":{"REASON":"FAIL","INFO":"MAC RESOLUTION RUNNING"}}}
// {"CLI":{"COMMAND":"get_alu_mac_by_port_index","ARGUMENTS":"blah=1","CODE":0,"RESULT":"FAIL","RESPONSE":{"REASON":"FAIL","INFO":"MISSING PORT ARG"}}}
// {"CLI":{"COMMAND":"get_alu_mac_by_port_index","ARGUMENTS":"blah=1","CODE":0,"RESULT":"FAIL","RESPONSE":{"REASON":"FAIL","INFO":"MISSING INDEX ARG"}}}
// {"CLI":{"COMMAND":"get_alu_mac_by_port_index","ARGUMENTS":"blah=1","CODE":0,"RESULT":"FAIL","RESPONSE":{"REASON":"FAIL","INFO":"INDEX OUT OF RANGE"}}}
//
void launchAluInfoByPortIndex(char *pUserInput)
{
    char  *pArguments;
    char   pPortString[AMU_COMPONENT_DATA_SIZE_MAX];
    char   pAluStartIndexString[AMU_COMPONENT_DATA_SIZE_MAX];
    U32    portKey;
    U32    portIndex;
    U32    aluIndex;

    pArguments = &pUserInput[strlen(LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD) + 1];
    if (pArguments == NULL) {
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                           ARGUMENTS_STRING, NULL_STRING,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  REASON_STRING, INVALID_ARGUMENTS_STRING);

    } else if (getAluMacInfoCb.stateMachineState != ALU_MAC_INFO_STATE_IDLE) {
        // This state machine is currently active.
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                           ARGUMENTS_STRING, pArguments,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  REASON_STRING, ALU_MAC_RUNNING_STRING);
        
    } else if (isMacResolutionSmIdle() == FALSE) {
        // The MAC RESOLUTION state machine is currently active.
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD, ARGUMENTS_STRING, pArguments,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  REASON_STRING, MAC_RESOLUTION_RUNNING_STRING);
    } else {
        // Neither is active, so proceed with parameter verification.
        // If they are good, then the process can be launched.

        memset(pPortString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
        findComponentStringData(CLI_ARG_PORT_STRING, pPortString, pArguments);

        memset(pAluStartIndexString, 0, AMU_COMPONENT_DATA_SIZE_MAX);
        findComponentStringData(CLI_ARG_ALU_START_INDEX_STRING, pAluStartIndexString, pArguments);

        if (strlen(pPortString) == 0) {
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                   CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                               ARGUMENTS_STRING, pArguments,
                               CODE_STRING,      0,
                               RESULT_STRING,    FAIL_STRING,
                               RESPONSE_STRING,  REASON_STRING, MISSING_PORT_ARG_STRING);
        } else if (strlen(pAluStartIndexString) == 0) {
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                   CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                               ARGUMENTS_STRING, pArguments,
                               CODE_STRING,      0,
                               RESULT_STRING,    FAIL_STRING,
                               RESPONSE_STRING,  REASON_STRING, MISSING_ALU_START_INDEX_ARG_STRING);
        } else {
            // Get the port "key", a value from 0..8 where 0 = LAN.
            // Here, a valid portKey is between 0 and 8. 
            portKey = getPortKeyFromPortArg(pPortString, LAN_OK);
            if (portKey != 0xFF) {
                // A valid port 1..8 (or "lan") was entered. Validate the alu_index from the user to
                // ensure it is not beyond the controller's limit, be it primary or secondary. 

                portIndex = PORT_KEY_TO_PORT_INDEX_LOOKUP[portKey];
                aluIndex  = atoi(pAluStartIndexString);
                if (portKey <= PRIMARY_CONTROLLER_MAX_KEY) {
                    if (aluIndex > PRIMARY_ALU_INDEX_MAX) {
                        // Invalid index for the primary controller.
                        portKey = 0xFF;
                    }
                } else {
                    // Validate the MAX index for the PRIMARY controller.
                    if (aluIndex > SECONDARY_ALU_INDEX_MAX) {
                        // Invalid index for the primary controller.
                        portKey = 0xFF;
                    }
                }
                // After evaluating the portKey variable, if it has been set to 0xFF due
                // to an invalid ALU index, then ...
                if (portKey == 0xFF) {
                    printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                           CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                                       ARGUMENTS_STRING, pArguments,
                                       CODE_STRING,      0,
                                       RESULT_STRING,    FAIL_STRING,
                                       RESPONSE_STRING,  REASON_STRING, ALU_INDEX_OUT_OF_RANGE_STRING);
                }                
            } else {
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                       CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESULT_STRING,    FAIL_STRING,
                                   RESPONSE_STRING,  REASON_STRING, INVALID_PORT_STRING);
            }

            // If all was good, then prepare to launch the state machine.
            if (portKey != 0xFF) {
                // All was good, prepare to launch the state machine. Retain data so that the subsequent
                // query for result will echo the data that launched the process in the 1st place.
                getAluMacInfoCb.stateMachineState      = ALU_MAC_INFO_STATE_LAUNCH;
                getAluMacInfoCb.aluGetType             = ALU_GET_TYPE_ALU_MAC_BY_PORT_INDEX;
                getAluMacInfoCb.pUserCommand           = (char *)&LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD;
                getAluMacInfoCb.userPort               = atoi(pPortString);
                getAluMacInfoCb.aluStartIndex          = aluIndex;
                getAluMacInfoCb.startReadTick          = g_ul_ms_ticks;
                getAluMacInfoCb.lastReadTick           = 0;
                getAluMacInfoCb.kszValidCount          = 0;
                getAluMacInfoCb.kszPortForward         = getKszPortForwardFromPortKey(portKey);
                getAluMacInfoCb.lastKszPortForwardRead = 0xFF;
                getAluMacInfoCb.portAsKey              = portKey;
                getAluMacInfoCb.portAsIndex            = portIndex;
                getAluMacInfoCb.current_index          = aluIndex;
                memset(&getAluMacInfoCb.mac[0], 0, 6);

                if (portKey <= PRIMARY_CONTROLLER_MAX_KEY) {
                    // PRIMARY controller port. Ports 1,2,3,4,5 equate on 1-to-1 basis with primary KSZ ports 1,2,3,4,LAN
                    getAluMacInfoCb.controller        = PRIMARY_ETHERNET_CONTROLLER;
                    getAluMacInfoCb.maxAluIndex       = PRIMARY_ALU_INDEX_MAX;

                } else if (portKey <= SECONDARY_CONTROLLER_MAX_KEY) {
                    // SECONDARY controller port. Ports 5,6,7,8 equate on 1-to-1 basis with secondary KSZ ports 1,2,3,4 so ... subtract 4.
                    getAluMacInfoCb.controller        = SECONDARY_ETHERNET_CONTROLLER;
                    getAluMacInfoCb.maxAluIndex       = SECONDARY_ALU_INDEX_MAX;
                }

                // Return the reply to the user.
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s,%s:%s,%s:%lu,%s:%lu,%s:\"0x%02X\",%s:%lu,%s:%lu}}}\r\n",
                       CLI_STRING, COMMAND_STRING,   LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESULT_STRING,    SUCCESS_STRING,
                                   RESPONSE_STRING,  PROCESS_STRING,           LAUNCHED_STRING,
                                                     CONTROLLER_STRING,        (getAluMacInfoCb.controller == PRIMARY_ETHERNET_CONTROLLER ? PRIMARY_STRING : RUNNING_STRING),
                                                     PORT_KEY_STRING,          portKey,
                                                     PORT_INDEX_STRING,        portIndex,
                                                     KSZ_PORT_FORWARD_STRING,  getAluMacInfoCb.kszPortForward,
                                                     MAX_ALU_INDEX_STRING,     getAluMacInfoCb.maxAluIndex,
                                                     CURRENT_ALU_INDEX_STRING, getAluMacInfoCb.current_index);

#if ENABLE_ALU_MAC_DEBUG == TRUE
                sprintf(&pCraftPortDebugData[0], "MIPRI LAUNCH: IDX=%lu CTRL=%lu KPF/MAC=0x%02X/%02X %02X %02X %02X %02X %02X\r\n",
                        getAluMacInfoCb.current_index, getAluMacInfoCb.controller, getAluMacInfoCb.kszPortForward,
                        getAluMacInfoCb.mac[0], getAluMacInfoCb.mac[1], getAluMacInfoCb.mac[2],
                        getAluMacInfoCb.mac[3], getAluMacInfoCb.mac[4], getAluMacInfoCb.mac[5]);
                UART1_WRITE_PACKET_MACRO
#endif

            } else {} // A reply will have been returned for any error coming here.
        }
    } 
}


// getAluInfoByPortIndexResult
//
// Return the current result of the "get_alu_mac_result" process. The process iterates over the ALU of the PRIMARY or SECONDARY controller by index,
// searching for the "1st valid row of the ALU corresponding to the port specified by the user when the process was launched".
//
// So the ALU search process may be running, or idle:
//
// - if no request was ever made, respond with:
// {"CLI":{"COMMAND":"get_alu_mac_result","ARGUMENTS":"NONE","RESULT":"SUCCESS","RESPONSE":{"PROCESS":"IDLE","INFO":"NO REQUEST MADE"}}}
//
// - else return whatever remains in the control block. The process could be on-going, not yet complete, in which case
//   intermediary results are returned indicate the process is still "RUNNING". Or the process is IDLE, so the results
//   will remain static - until the next LAUNCH command. The following is returned:
//
// {"CLI":{"COMMAND":"get_alu_mac_result","ARGUMENTS":"NONE","CODE":0,"RESULT":"SUCCESS",
//         "RESPONSE":{"PROCESS":"<IDLE|RUNNING>",
//                     "USER COMMAND":"get_alu_info_by_port_index_result",
//                     "USER PORT":2,
//                     "ALU START INDEX":0,
//                     "START READ TICK":1234,
//                     "LAST READ TICK":5678,
//                     "ELASPED TICK":4444,
//                     "CONTROLLER":"<PRIMARY|SECONDARY>",
//                     "MAX ALU INDEX":<4095|1023>,
//                     "NUMBER DATA READS":1,
//                     "CURRENT INDEX":123,
//                     "PORT KEY":2
//                     "PORT INDEX":1,
//                     "KSZ PORT FORWARD":1,
//                     "KSZ VALID COUNT":0,
//                     "KSZ PORT FORWARD":0,":0,
//                     "MAC":"01:02:03:04:05:06"}}}
//
void getAluInfoByPortIndexResult(void)
{

    if (getAluMacInfoCb.stateMachineState == ALU_MAC_INFO_STATE_DEAD_QUIET) {
        // No such process was ever run.
        // const char NO_REQUEST_MADE_STRING[]
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:%s,%s:%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD,
                           ARGUMENTS_STRING, NONE_STRING,
                           CODE_STRING,      0,
                           RESULT_STRING,    SUCCESS_STRING,
                           RESPONSE_STRING,  PROCESS_STRING, DEAD_QUIET_STRING,
                                             INFO_STRING,    NO_REQUEST_MADE_STRING);
    } else {
        // The state machine is either in the idle state (indicating it has completed the processing of a
        // previous request), or is actively running. Return whatever is currently in the control block. 
        // Break up the somewhat large response into several print statements.
        printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:%s,%s:\"%s\",%s:%lu,%s:%lu,%s:%lu,%s:%lu,%s:%lu,",
               CLI_STRING, COMMAND_STRING,   GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD,
                           ARGUMENTS_STRING, NONE_STRING,
                           CODE_STRING,      0,
                           RESULT_STRING,    SUCCESS_STRING,
                           RESPONSE_STRING,  PROCESS_STRING,         (getAluMacInfoCb.stateMachineState == ALU_MAC_INFO_STATE_IDLE ? IDLE_STRING : RUNNING_STRING),
                                             USER_COMMAND_STRING,    getAluMacInfoCb.pUserCommand,
                                             USER_PORT_STRING,       getAluMacInfoCb.userPort,
                                             ALU_START_INDEX_STRING, getAluMacInfoCb.aluStartIndex,
                                             START_READ_TICK_STRING, getAluMacInfoCb.startReadTick,
                                             LAST_READ_TICK_STRING,  getAluMacInfoCb.lastReadTick,
                                             ELAPSED_TICK_STRING,    (getAluMacInfoCb.lastReadTick - getAluMacInfoCb.startReadTick));

        printf("%s:%s,%s:%lu,%s:%lu,%s:%lu,%s:%lu,%s:\"0x%02X\",%s:\"0x%02X\",%s:%u,",
               CONTROLLER_STRING,           (getAluMacInfoCb.controller == PRIMARY_ETHERNET_CONTROLLER ? PRIMARY_STRING : SECONDARY_STRING),
               MAX_ALU_INDEX_STRING,         getAluMacInfoCb.maxAluIndex,
               CURRENT_ALU_INDEX_STRING,     getAluMacInfoCb.current_index,
               PORT_KEY_STRING,              getAluMacInfoCb.portAsKey,
               PORT_INDEX_STRING,            getAluMacInfoCb.portAsIndex,
               KSZ_PORT_FORWARD_STRING,      getAluMacInfoCb.kszPortForward,
               LAST_KSZ_PORT_FORWARD_STRING, getAluMacInfoCb.lastKszPortForwardRead,
               KSZ_VALID_COUNT_STRING,       getAluMacInfoCb.kszValidCount);

        printf("%s:\"%02X:%02X:%02X:%02X:%02X:%02X\"}}}\r\n",
                MAC_STRING, getAluMacInfoCb.mac[0], getAluMacInfoCb.mac[1], getAluMacInfoCb.mac[2],
                            getAluMacInfoCb.mac[3], getAluMacInfoCb.mac[4], getAluMacInfoCb.mac[5]);
    }    
}


// isAluMacInfoSmIdle
//
//
//
BOOL isAluMacInfoSmIdle (void)
{
    if      (getAluMacInfoCb.stateMachineState == ALU_MAC_INFO_STATE_IDLE)       { return TRUE;  }
    else if (getAluMacInfoCb.stateMachineState == ALU_MAC_INFO_STATE_DEAD_QUIET) { return TRUE;  }
    else                                                                         { return FALSE; }
}


// checkAluMacInfoSm
//
// Called from main. The state machine will transition from IDLE to the next state if:
//
// - reading a single entry of the primary's ALU takes on order 200msec
// - so reading 10 entries in the loop below is in excess of 2 seconds.
// - so make sure the watchdog is taken case of.
//
//
//
void checkAluMacInfoSm(void)
{
    U32 iggy;
    U8  pData[9];

    switch (getAluMacInfoCb.stateMachineState) {
        case ALU_MAC_INFO_STATE_DEAD_QUIET:
            // Coming out of reset, no request was ever made. The state machine remains in this state until the
            // first request for ALU info is made. Thereafter, once it leaves this state, it never returns here.
            break;

        case ALU_MAC_INFO_STATE_IDLE:
            // Nothing to do, so do plenty of it. Remain in the IDLE state. This state is pretty much the same as
            // the dead-quiet state, except it enters this state upon completion of a request to get ALU into. It
            // allows for the gateway to request data of a previously active state, which includes the notion that
            // a previous request is complete. The data in the control block remains untouched.
            break;

        case ALU_MAC_INFO_STATE_LAUNCH:
            // A request from the Gateway for ALU info was made, the state machine's state is set to LAUNCH.
            // Check which controller to read from, and set the state accordingly.
            if (getAluMacInfoCb.controller == PRIMARY_ETHERNET_CONTROLLER) { getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_PRI_READ; }
            else                                                           { getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_SEC_READ; }
            break;

        case ALU_MAC_INFO_STATE_PRI_READ:
            // Read ALU entry by index from the primary controller and verify the results.
            for (iggy = 0; iggy < NUMBER_READ_ROWS_PER_CYCLE; iggy++) {

                // Read the PRIMARY ALU by the current index.
                getAluMacInfoCb.lastReadTick = g_ul_ms_ticks;
                if (isReadAluPrimaryControlByIndex(getAluMacInfoCb.current_index, &getAluMacInfoCb.kszValidCount)) {
                    getAluMacInfoCb.lastKszPortForwardRead = readAluPrimaryPortForwardRegister();
                    if (getAluMacInfoCb.lastKszPortForwardRead == getAluMacInfoCb.kszPortForward) {
                        // Found it: save the result for the eventually request_for-data cli,
                        //           read the data registers and stop the search.
                        readAluPrimaryDataRegisters(&getAluMacInfoCb.mac[0]);
                        getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_IDLE;

#if ENABLE_ALU_MAC_DEBUG == TRUE
                        sprintf(&pCraftPortDebugData[0], "MIPRI DONE: %lu %lu 0x%02X 0x%04X MAC: %02X %02X %02X %02X %02X %02X\r\n",
                                getAluMacInfoCb.current_index, getAluMacInfoCb.controller, getAluMacInfoCb.kszPortForward, getAluMacInfoCb.kszValidCount,
                                getAluMacInfoCb.mac[0], getAluMacInfoCb.mac[1], getAluMacInfoCb.mac[2],
                                getAluMacInfoCb.mac[3], getAluMacInfoCb.mac[4], getAluMacInfoCb.mac[5]);
                        UART1_WRITE_PACKET_MACRO
#endif

                        break; // out of for loop
                    } else {
                        // Found ... nothing in the row that was just read, or it's a row for some other port/mac. Keep searching.

#if ENABLE_ALU_MAC_DEBUG == TRUE
                        sprintf(&pCraftPortDebugData[0], "MIPRI: %lu %04X P:0x%02X\r\n", getAluMacInfoCb.current_index,   getAluMacInfoCb.kszValidCount, getAluMacInfoCb.lastKszPortForwardRead);
                        UART1_WRITE_PACKET_MACRO
#endif

                        if (getAluMacInfoCb.current_index < getAluMacInfoCb.maxAluIndex) {
                            getAluMacInfoCb.current_index++;
                        } else {
                            // ALU table end of the line ...
                            getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_IDLE;
                            break; // out of for loop ...
                        }
                    }
                } else {
                    // Failed to read.
                    incrementBadEventCounter(PRI_ALU_CONTROL_REG_READ_LIMIT);
                    getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_IDLE;
                    break; // out of for loop ...
                }                        

                pokeWatchdog();

            } // for ...

            break;

        case ALU_MAC_INFO_STATE_SEC_READ:
            // Read an entry by index from the secondary controller and verify the results. 
            for (iggy = 0; iggy < NUMBER_READ_ROWS_PER_CYCLE; iggy++) {

                // Read the SECONDARY ALU by the current index. 9 bytes are written starting at pData[0]:
                // - the 1st 3 bytes of pData[0..2] contain control info
                // - the remaining 6 bytes from [3..8] contain the MAC, which may or may not be valid.
                // When the raw data is read out, call the routine to extract the relevant data.
                getAluMacInfoCb.lastReadTick      = g_ul_ms_ticks;
                readAluSecondaryDynamicByIndex((U16)getAluMacInfoCb.current_index, &pData[0]);

#if ENABLE_ALU_MAC_DEBUG == TRUE
                sprintf(&pCraftPortDebugData[0], "MISEC: %lu  = %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",
                        getAluMacInfoCb.current_index, pData[0], pData[1], pData[2], pData[3], pData[4], pData[5], pData[6], pData[7], pData[8]);
                UART1_WRITE_PACKET_MACRO
#endif

                memcpy(&getAluMacInfoCb.mac[0], &pData[3], 6);           // <- may or may not be valid, depends on the kszPort.
                processSecondaryAluDynamicRawData(&pData[0], &getAluMacInfoCb.lastKszPortForwardRead, &getAluMacInfoCb.kszValidCount);

                // The data sheet is somewhat unclear: sometimes the desired port is extracted, yet the validCount variable = 0.
                // The observation is that if validCount = 0 the ALU row entry is "invalid", and reading should continue.
                if (getAluMacInfoCb.kszValidCount != 0) {
                    // A row with valid data. Is it the desired port?
                    if (getAluMacInfoCb.lastKszPortForwardRead == getAluMacInfoCb.kszPortForward) {
                        // Found it: save the result for the eventually request_for-data, stop the search.
                        getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_IDLE;
                        memcpy(&getAluMacInfoCb.mac[0], &pData[3], 6);

#if ENABLE_ALU_MAC_DEBUG == TRUE
                        sprintf(&pCraftPortDebugData[0], "MISEC DONE: DATA IN [%lu]\r\n", getAluMacInfoCb.current_index); UART1_WRITE_PACKET_MACRO
#endif

                        break; // out of for loop
                    }                            
                }

                // Found ... nothing of value in the row that was just read, or it's a row for some other port/mac. Keep searching.
                if (getAluMacInfoCb.current_index < getAluMacInfoCb.maxAluIndex) {
                    getAluMacInfoCb.current_index++;
                } else {
                    // ALU table end of the line ...
                    getAluMacInfoCb.stateMachineState = ALU_MAC_INFO_STATE_IDLE;
                    break; // out of for loop ...
                }

                pokeWatchdog();

            } // for ...

            break;

        default:
            // Coding error. Increment a counter.
            break;
    }
}


/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
