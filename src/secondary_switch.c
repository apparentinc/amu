/**
 *
 * \brief Apparent Application Functions for the "SECONDARY" KSZ8795 Switch Controller.

 * Copyright (c) 2021 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains utility support routines for the Apparent Gateway PCBA system,
 * to support the management of the SECONDARY KSZ8795 ethernet switch controller.
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "usart.h"
#include "status_codes.h"
#include "uart_serial.h"

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "board.h"

#include "apparent_strings.h"
#include "apparent.h"
#include "secondary_switch.h"
#include "poe.h"


// Really ought to have a main.h header file to be proper.
extern void  mdelay              (uint32_t ul_dly_ticks);
extern U32   g_ul_ms_ticks;
extern char  pCraftPortDebugData[];
extern char  pSingleJsonEventString[];


// To read from, or write to any register of the "SECONDARY" KSZ8795 ethernet chip controller (section 3.5 Figure 3-7/3-8 page 28 of
// the data sheet), a sequence of 3 bytes are sent to the controller over the SPI interface using the usart_putchar() facility.
// The 3 bytes come from the 24 bits as specified in section 3.5, structured as follows:
//
//                 |<-----             24 bits total  = 3 bytes                    ----->|
//                       |<---     12-bit address     ---> |       |<--- 8-bit DATA  --->|
// Register Read:  XXX   11 10 9  8  7  6  5  4  3  2  1  0   Z    D7 D6 D5 D4 D3 D2 D1 D0
// Register write: YYY
//
// where: - to read:  1st 3 bits XXX = 011
//        - to write: 1st 3 bits YYY = 010
//        - 1 bits Z = don't care (set to 0)
//        - 8-bit DATA is 00000000 for a read command
//
// Example: read global address 0x00 ("CHIP ID"):
//                 011   11 10 9  8  7  6  5  4  3  2  1  0   Z    D7 D6 D5 D4 D3 D2 D1 D0
//        bits ->  011    0  0 0  0  0  0  0  0  0  0  0  0   0     0  0  0  0  0  0  0  0
//        bits ->  011    0  0 0  0  0  0  0  0  0  0  0  0   0     0  0  0  0  0  0  0  0
//    expanded ->  0110 0000    0000 0000    0000 0000
//                    6    0       0    0       0    0
//    so the 3 bytes to send to the controller to read the 1 byte of register address 0x00 = 60 00 00
//
//
// HENCEFORTH: all references to the KSZ8795 ethernet chip control will be with the word "SECONDARY", "Secondary", or "secondary".
//             WHY? Because the primary ethernet chip controller - the KSZ9897 is "too close" in its nomenclature, and so
//             prone to confusion and error.
//

static U8 SEC_READ_SEQ_GLOBAL_REG_00[3];                        // Register number  0 <- chip family, default 0x87
static U8 SEC_READ_SEQ_GLOBAL_REG_01[3];                        // Register number  1 <- chip ID [7..4] + Start Switch bit [0x01]
static U8 SEC_READ_SEQ_GLOBAL_REG_02[3];                        // Register number  2 <- sw reset, flush dynamic mac + a few other things

static U8 SEC_WRITE_SEQ_ENABLE_SOFT_RESET_GLOBAL_REG_02[3];     // Register number 2 <- Global sw reset Enable  bit[6] = 1 to "reset all FSM and data path". Bits [3..2] MUST be [1,1]
static U8 SEC_WRITE_SEQ_DISABLE_SOFT_RESET_GLOBAL_REG_02[3];    // Register number 2 <- Global sw reset Disable bit[6] = 0 to disable this reset


// The set of registers to access: TRANSMIT ENABLE/DISABLE, RECIEVE ENABLE/DISABLE
// Port 5 is a valid port (it is not a RESERVED port), but not defined.
static U8 SEC_READ_SEQ_PORT_1_CONTROL_2_REG_12[3];              // Register number 18
static U8 SEC_READ_SEQ_PORT_2_CONTROL_2_REG_22[3];              // Register number 34
static U8 SEC_READ_SEQ_PORT_3_CONTROL_2_REG_32[3];              // Register number 50
static U8 SEC_READ_SEQ_PORT_4_CONTROL_2_REG_42[3];              // Register number 66

// The set of registers to access: DISABLE AN, FORCED SPEED, FORCED DUPLEX
// Port 5 is RESERVED, so not defined.
static U8 SEC_READ_SEQ_PORT_1_CONTROL_9_REG_1C[3];              // Register number 28
static U8 SEC_READ_SEQ_PORT_2_CONTROL_9_REG_2C[3];              // Register number 44
static U8 SEC_READ_SEQ_PORT_3_CONTROL_9_REG_3C[3];              // Register number 60
static U8 SEC_READ_SEQ_PORT_4_CONTROL_9_REG_4C[3];              // Register number 76

// The set of registers to access: PORT POWER DOWN bit[???]
// Port 5 is RESERVED, so not defined.
static U8 SEC_READ_SEQ_PORT_1_CONTROL_10_REG_1D[3];             // Register number 29
static U8 SEC_READ_SEQ_PORT_2_CONTROL_10_REG_2D[3];             // Register number 45
static U8 SEC_READ_SEQ_PORT_3_CONTROL_10_REG_3D[3];             // Register number 61
static U8 SEC_READ_SEQ_PORT_4_CONTROL_10_REG_4D[3];             // Register number 77


// The set of STATUS registers to access: MDIX status bit[7], Auto-negotiation done [6], Link UP/DOWN bit[5]
// Port 5 is RESERVED, so not defined. This is a read-only register, so the write sequences are not defined.
static U8 SEC_READ_SEQ_PORT_1_STATUS_2_REG_1E[3];               // Register number 30
static U8 SEC_READ_SEQ_PORT_2_STATUS_2_REG_2E[3];               // Register number 46
static U8 SEC_READ_SEQ_PORT_3_STATUS_2_REG_3E[3];               // Register number 62
static U8 SEC_READ_SEQ_PORT_4_STATUS_2_REG_4E[3];               // Register number 78


// The set of registers to access: soft reset bit[4], Force Link bit[3] and Port Operation Mode bits[2..0]
// Port 5 is RESERVED, so not defined.
static U8 SEC_READ_SEQ_PORT_1_CONTROL_11_REG_1F[3];             // Register number 31
static U8 SEC_READ_SEQ_PORT_2_CONTROL_11_REG_2F[3];             // Register number 47
static U8 SEC_READ_SEQ_PORT_3_CONTROL_11_REG_3F[3];             // Register number 63
static U8 SEC_READ_SEQ_PORT_4_CONTROL_11_REG_4F[3];             // Register number 79


// Data to manage and support ethernet service over the secondary switch controller.
static t_secondarySwitchInfo secondarySwitchInfoCb [NUMBER_SECONDARY_SWITCH_PORTS];


// To read the 8-bit CONTROL register "2" for each of the 4 ports on the secondary switch:
static const U8 *CONTROL_REG_2_READ_SEQ_BY_PORT_INDEX_LOOKUP []  = { (U8 *)&SEC_READ_SEQ_PORT_1_CONTROL_2_REG_12[0],   // [0] : external port 5
                                                                     (U8 *)&SEC_READ_SEQ_PORT_2_CONTROL_2_REG_22[0],   // [1] : external port 6
                                                                     (U8 *)&SEC_READ_SEQ_PORT_3_CONTROL_2_REG_32[0],   // [2] : external port 7
                                                                     (U8 *)&SEC_READ_SEQ_PORT_4_CONTROL_2_REG_42[0]    // [3] : external port 8
                                                                   };

// To read the 8-bit CONTROL register "9" for each of the 4 ports on the secondary switch:
static const U8 *CONTROL_REG_9_READ_SEQ_BY_PORT_INDEX_LOOKUP []  = { (U8 *)&SEC_READ_SEQ_PORT_1_CONTROL_9_REG_1C[0],   // [0] : external port 5
                                                                     (U8 *)&SEC_READ_SEQ_PORT_2_CONTROL_9_REG_2C[0],   // [1] : external port 6
                                                                     (U8 *)&SEC_READ_SEQ_PORT_3_CONTROL_9_REG_3C[0],   // [2] : external port 7
                                                                     (U8 *)&SEC_READ_SEQ_PORT_4_CONTROL_9_REG_4C[0]    // [3] : external port 8
                                                                   };

// To read the 8-bit CONTROL register "10" for each of the 4 ports on the secondary switch:
static const U8 *CONTROL_REG_10_READ_SEQ_BY_PORT_INDEX_LOOKUP [] = { (U8 *)&SEC_READ_SEQ_PORT_1_CONTROL_10_REG_1D[0],   // [0] : external port 5
                                                                     (U8 *)&SEC_READ_SEQ_PORT_2_CONTROL_10_REG_2D[0],   // [1] : external port 6
                                                                     (U8 *)&SEC_READ_SEQ_PORT_3_CONTROL_10_REG_3D[0],   // [2] : external port 7
                                                                     (U8 *)&SEC_READ_SEQ_PORT_4_CONTROL_10_REG_4D[0]    // [3] : external port 8
                                                                   };

// To read the set of 8-bit "STATUS 2" registers for each of the 4 ports on the secondary switch:
static const U8 *STATUS_2_READ_SEQ_BY_PORT_INDEX_LOOKUP []       = { (U8 *)&SEC_READ_SEQ_PORT_1_STATUS_2_REG_1E[0],   // [0] : external port 5
                                                                     (U8 *)&SEC_READ_SEQ_PORT_2_STATUS_2_REG_2E[0],   // [1] : external port 6
                                                                     (U8 *)&SEC_READ_SEQ_PORT_3_STATUS_2_REG_3E[0],   // [2] : external port 7
                                                                     (U8 *)&SEC_READ_SEQ_PORT_4_STATUS_2_REG_4E[0]    // [3] : external port 8
                                                                   };

// To read the 8-bit CONTROL register "11" for each of the 4 ports on the secondary switch:
static const U8 *CONTROL_11_READ_SEQ_BY_PORT_INDEX_LOOKUP []     = { (U8 *)&SEC_READ_SEQ_PORT_1_CONTROL_11_REG_1F[0],   // [0] : controller port 1/external  port 5
                                                                     (U8 *)&SEC_READ_SEQ_PORT_2_CONTROL_11_REG_2F[0],   // [1] : controller port 2/external  port 6
                                                                     (U8 *)&SEC_READ_SEQ_PORT_3_CONTROL_11_REG_3F[0],   // [2] : controller port 3/external  port 7
                                                                     (U8 *)&SEC_READ_SEQ_PORT_4_CONTROL_11_REG_4F[0]    // [3] : controller port 4/external  port 8
                                                                   };


static const U8 STATUS_1_REG_ADDRESS_BY_PORT_LOOKUP[]   = { PORT_REGISTER_PORT_1_ADDRESS_STATUS_1,   // [0]: controller port 1/external  port 5
                                                            PORT_REGISTER_PORT_2_ADDRESS_STATUS_1,   // [1]: controller port 2/external  port 6
                                                            PORT_REGISTER_PORT_3_ADDRESS_STATUS_1,   // [2]: controller port 3/external  port 7
                                                            PORT_REGISTER_PORT_4_ADDRESS_STATUS_1    // [3]: controller port 4/external  port 8
                                                          };
static const U8 CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[]  = { PORT_REGISTER_PORT_1_ADDRESS_CONTROL_2,   // [0]: controller port 1/external  port 5
                                                            PORT_REGISTER_PORT_2_ADDRESS_CONTROL_2,   // [1]: controller port 2/external  port 6
                                                            PORT_REGISTER_PORT_3_ADDRESS_CONTROL_2,   // [2]: controller port 3/external  port 7
                                                            PORT_REGISTER_PORT_4_ADDRESS_CONTROL_2    // [3]: controller port 4/external  port 8
                                                          };
static const U8 CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[]  = { PORT_REGISTER_PORT_1_ADDRESS_CONTROL_9,   // [0]: controller port 1/external  port 5
                                                            PORT_REGISTER_PORT_2_ADDRESS_CONTROL_9,   // [1]: controller port 2/external  port 6
                                                            PORT_REGISTER_PORT_3_ADDRESS_CONTROL_9,   // [2]: controller port 3/external  port 7
                                                            PORT_REGISTER_PORT_4_ADDRESS_CONTROL_9    // [3]: controller port 4/external  port 8
                                                          };
static const U8 CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[] = { PORT_REGISTER_PORT_1_ADDRESS_CONTROL_10,   // [0]: controller port 1/external  port 5
                                                            PORT_REGISTER_PORT_2_ADDRESS_CONTROL_10,   // [1]: controller port 2/external  port 6
                                                            PORT_REGISTER_PORT_3_ADDRESS_CONTROL_10,   // [2]: controller port 3/external  port 7
                                                            PORT_REGISTER_PORT_4_ADDRESS_CONTROL_10    // [3]: controller port 4/external  port 8
                                                          };
static const U8 CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[] = { PORT_REGISTER_PORT_1_ADDRESS_CONTROL_11_STATUS_3,   // [0]: controller port 1/external  port 5
                                                            PORT_REGISTER_PORT_2_ADDRESS_CONTROL_11_STATUS_3,   // [1]: controller port 2/external  port 6
                                                            PORT_REGISTER_PORT_3_ADDRESS_CONTROL_11_STATUS_3,   // [2]: controller port 3/external  port 7
                                                            PORT_REGISTER_PORT_4_ADDRESS_CONTROL_11_STATUS_3    // [3]: controller port 4/external  port 8
                                                          };
static const U8 STATUS_REG_2_ADDRESS_BY_PORT_LOOKUP[]   = { PORT_REGISTER_PORT_1_ADDRESS_STATUS_2,   // [0]: controller port 1/external  port 5
                                                            PORT_REGISTER_PORT_2_ADDRESS_STATUS_2,   // [1]: controller port 2/external  port 6
                                                            PORT_REGISTER_PORT_3_ADDRESS_STATUS_2,   // [2]: controller port 3/external  port 7
                                                            PORT_REGISTER_PORT_4_ADDRESS_STATUS_2    // [3]: controller port 4/external  port 8
                                                          };

// End of SBC/GATEWAY sensitive definitions.
//
// ----------------------------------------------------------------------------


// Static Module prototypes:

static void setSecondaryControlRegistersDefaults        (void);
static U8   readFromSecondarySwitch                     (const U8 *pThreeBytes);
static void writeToSecondarySwitch                      (const U8 *pThreeBytes);

static void readUsart2UntilEmpty                        (U32 counterIndex);
static void build3ByteCommandSequence                   (U8 command, U8 address, U8 data, U8 *pBytes);

static BOOL isSecondaryEthernetServiceEnabled           (void);
static void verifySecondaryEthernetService              (void);

static void getSecPortInfoFromDynamicSourcePort         (U8 sourcePort, U8 *pKszPort, char **pDownstreamPort);

static void verifySecondaryRegister                     (U8 address, U8 expectedValue, U16 identifier);
static void ksz8795ErrataFixes                          (void);
static void updateSecondaryPortStatus                   (void);

// MACRO FOR TESTING:
#define UART1_WRITE_PACKET_MACRO usart_serial_write_packet((usart_if)UART1, (uint8_t *)pCraftPortDebugData, strlen(pCraftPortDebugData));


#if 0  // YO2
static bool isPrimaryMibIndexValid          (U8 someIndex);
#endif // YO2

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond


// Exported/external functions from this module:


void  readCoreSecondaryGlobalRegisters(void)
{
    // Part of the output when processing the HELLO command. Only the core global registers are output.
    // "SECONDARY GLOBAL REGISTERS" : {"GLOBAL REGISTER 0x00":"0x00", "GLOBAL REGISTER 0x01":"0x01" ... , "GLOBAL REGISTER 0x0F":"0x0F"}

    U8 registerValue;

    printf("%s:{", SECONDARY_GLOBAL_REGISTERS_STRING);
    registerValue = readFromSecondarySwitch(SEC_READ_SEQ_GLOBAL_REG_00); printf("%s:\"0x%02X\",", GLOBAL_REGISTER_00_STRING, registerValue);
    registerValue = readFromSecondarySwitch(SEC_READ_SEQ_GLOBAL_REG_01); printf("%s:\"0x%02X\",", GLOBAL_REGISTER_01_STRING, registerValue);
    registerValue = readFromSecondarySwitch(SEC_READ_SEQ_GLOBAL_REG_02); printf("%s:\"0x%02X\"",  GLOBAL_REGISTER_02_STRING, registerValue); // no comma
    printf("}"); // close off the inner and outer components.
}


void readSecondaryPortControlAndStatusInfo(void)
{
    U16   iggy;
    char *pGenericString;

    // Read/display/print the set of CONTROL and STATUS register values, as a JSON-like string.
    // The ability to "write" a JSON-like string of arbitrary length for processing by the
    // SBC is controlled by multiple printf with a CR/LF when the JSON-like string is terminated.

    // There are 4 sets of control registers (2, 9, 10, 11) and 1 status register. The "control 10"
    // register contains the "power down" bit setting when configured up/down by the SBC, so it is
    // included in that output.
    // The output string should look something like:
    //
    // "SECONDARY CONTROL REGISTERS":{"CONTROL_02":{"PORT 5":{"CONTROL":"0x22"},"PORT 6":{"CONTROL":"0x22"},"PORT 7":{"CONTROL":"0x22"},"PORT 8":{"CONTROL":"0x22"}},
    //                                     "CONTROL_09":{"PORT 5":{"CONTROL":"0x11"},"PORT 6":{"CONTROL":"0x11"},"PORT 7":{"CONTROL":"0x11"},"PORT 8":{"CONTROL":"0x11"}},
    //                                     "CONTROL_10":{"PORT 5":{"CONTROL":"0x06","CONFIG":"POWERED UP"},"PORT 6":{"CONTROL":"0x06","CONFIG":"POWERED DOWN"},
    //                                                   "PORT 7":{"CONTROL":"0x06","CONFIG":"POWERED UP"},"PORT 8":{"CONTROL":"0x06","CONFIG":"POWERED UP"}},
    //                                     "CONTROL_11":{"PORT 5":{"CONTROL":"0x33"},"PORT 6":{"CONTROL":"0x33"},"PORT 7":{"CONTROL":"0x33"},"PORT 8":{"CONTROL":"0x33"}}},
    // "SECONDARY STATUS REGISTERS":{"PORT 5":{"STATUS":"0x66","LINK":"UP"},"PORT 6":{"STATUS":"0x66","LINK":"UP"},"PORT 7":{"STATUS":"0x77","LINK":"DOWN"},"PORT 8":{"STATUS":"0x77","LINK":"DOWN"}}

    // 1: open the JSON-like thingamajig:
    printf("%s:{", SECONDARY_CONTROL_REGISTERS_STRING);

    // 2: control register 2 set:
    printf("%s:{", SECONDARY_CONTROL_REGISTER_02_STRING);
    // Add each port.
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
        printf("\"%s %s\":{ %s:\"0x%02X\"}", PORT_NO_QUOTES_STRING, pGimmeSecondaryPortString(iggy), CONTROL_STRING, secondarySwitchInfoCb[iggy].controlReg_2);
        if ((iggy+1) == NUMBER_SECONDARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                           { printf(","); }  // trailing comma for the next element
    } // for ...
    // Terminate this inner "control reg 9" JSON with 1 curly bracket and a comma.
    printf("},");

    // 3: control register 9 set:
    printf("%s:{", SECONDARY_CONTROL_REGISTER_09_STRING);
    // Add each port.
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
        printf("\"%s %s\":{ %s:\"0x%02X\"}", PORT_NO_QUOTES_STRING, pGimmeSecondaryPortString(iggy), CONTROL_STRING, secondarySwitchInfoCb[iggy].controlReg_9);
        if ((iggy+1) == NUMBER_SECONDARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                           { printf(","); }  // trailing comma for the next element
    } // for ...
    // Terminate this inner "control reg 9" JSON with 1 curly bracket and a comma.
    printf("},");

    // 4: control register 10 set:
    printf("%s:{", SECONDARY_CONTROL_REGISTER_10_STRING);
    // Add each port.
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
        if (secondarySwitchInfoCb[iggy].portPoweredUp == TRUE) { pGenericString = (char *)&PORT_CONFIG_POWERED_UP_STRING;   }
        else                                                   { pGenericString = (char *)&PORT_CONFIG_POWERED_DOWN_STRING; }
        printf("\"%s %s\":{ %s:\"0x%02X\",%s:%s}",
               PORT_NO_QUOTES_STRING, pGimmeSecondaryPortString(iggy),
               CONTROL_STRING,        secondarySwitchInfoCb[iggy].controlReg_10,
               CONFIG_STRING,         pGenericString);
        if ((iggy+1) == NUMBER_SECONDARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                           { printf(","); } // trailing comma for the next element
    } // for ...
    // Terminate this inner "control reg 10" JSON with 1 curly bracket and a comma.
    printf("},");

    // 5: control register 11 set:
    printf("%s:{", SECONDARY_CONTROL_REGISTER_11_STRING);
    // Add each port.
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
        printf("\"%s %s\":{ %s:\"0x%02X\"}", PORT_NO_QUOTES_STRING, pGimmeSecondaryPortString(iggy), CONTROL_STRING, secondarySwitchInfoCb[iggy].controlReg_11);
        if ((iggy+1) == NUMBER_SECONDARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                           { printf(","); } // trailing comma for the next element
    } // for ...
    // Terminate this JSON with 3 curly bracket and <CR><LF>: - 1 bracket to close off the last "control reg 11" JSON
    //                                                        - 1 bracket to close off the inner title section
    //                                                        - 1 last bracket to close off the entire JSON-like string.
    printf("}},");

    // 6: status 2 register set. Among other things it contains the LINK = UP/DOWN indicator
    // (in Microchip parlance, it is called LINK GOOD or LINK BAD):
    //
    // {"SECONDARY STATUS REGISTERS":{"PORT 5":{"STATUS":"0x66","LINK":"UP"},"PORT 6":{"STATUS":"0x66","LINK":"UP"},
        //                            "PORT 7":{"STATUS":"0x77","LINK":"DOWN"},"PORT 8":{"STATUS":"0x77","LINK":"DOWN"}}
    printf("%s:{ ", SECONDARY_STATUS_REGISTERS_STRING);
    // Add each port.
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
        if (isSecondaryLinkStatusLinkUp(secondarySwitchInfoCb[iggy].status2)) { pGenericString = (char *)&LINK_STATUS_UP_STRING;   }
        else                                                                  { pGenericString = (char *)&LINK_STATUS_DOWN_STRING; }
        printf("\"%s %s\":{%s:\"0x%02X\",%s:%s}", PORT_NO_QUOTES_STRING, pGimmeSecondaryPortString(iggy), STATUS_STRING, secondarySwitchInfoCb[iggy].status2, LINK_STRING, pGenericString);
        if ((iggy+1) == NUMBER_SECONDARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                           { printf(","); } // trailing comma for the next element
    } // for ...
    // Terminate the JSON with 2 curly bracket and <CR><LF>: 1 to close the inner list, and 1 to close the outer JSON string).
    printf("}");

}


// getSecondaryStatusByPortIndex
//
// Return the status register for a port by port index 0..3
//
//
U16 getSecondaryStatusByPortIndex(U32 portIndex)
{
    return secondarySwitchInfoCb[portIndex].status2;
}


// getSecondaryControl10ByPortIndex
//
// Return the control 10 register for a port by port index 0..3
//
//
U16 getSecondaryControl10ByPortIndex(U32 portIndex)
{
    return secondarySwitchInfoCb[portIndex].controlReg_10;
}


U8 readFromSecondarySwitchByAddress(U8 address)
{
    U8 pThreeBytes[3];
    U8 someByte;
    build3ByteCommandSequence(SECONDARY_READ_COMMAND, address, 0x00, &pThreeBytes[0]);
    someByte = readFromSecondarySwitch(pThreeBytes);
    return someByte;
}


static U8 readFromSecondarySwitch(const U8 *pThreeBytes)
{
    U32 myData;

    usart_spi_force_chip_select(USART2);
    readUsart2UntilEmpty(PHY2_DATA_PRESENT_BEFORE_READ); // The receiver is expected to be empty.
    usart_putchar(USART2, pThreeBytes[0]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART2) == 1) { usart_getchar(USART2, &myData);  } // [0]
    usart_putchar(USART2, pThreeBytes[1]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART2) == 1) { usart_getchar(USART2, &myData);  } // [1]
    usart_putchar(USART2, pThreeBytes[2]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART2) == 1) { usart_getchar(USART2, &myData);  } // [2] <- KSZ8795 "turn around"
    readUsart2UntilEmpty(PHY2_DATA_PRESENT_AFTER_READ); // The receiver is expected to be empty.
    usart_spi_release_chip_select(USART2);
    return myData;

}


void writeToSecondaryByAddress(U8 registerAddress, U8 registerValue)
{
    U8 pThreeBytes[3];
    build3ByteCommandSequence(SECONDARY_WRITE_COMMAND, registerAddress, registerValue, &pThreeBytes[0]);
    writeToSecondarySwitch(pThreeBytes);
    return;
}


static void writeToSecondarySwitch(const U8* pThreeBytes)
{
    U32 myData;

    usart_spi_force_chip_select(USART2);
    readUsart2UntilEmpty(PHY2_DATA_PRESENT_BEFORE_WRITE); // The receiver is expected to be empty.
    usart_putchar(USART2, pThreeBytes[0]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART2) == 1) { usart_getchar(USART2, &myData);  } // [0]
    usart_putchar(USART2, pThreeBytes[1]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART2) == 1) { usart_getchar(USART2, &myData);  } // [1]
    usart_putchar(USART2, pThreeBytes[2]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART2) == 1) { usart_getchar(USART2, &myData);  } // [2] <- expected KSZ8795 "turn around"
    readUsart2UntilEmpty(PHY2_DATA_PRESENT_AFTER_WRITE); // The receiver is expected to be empty.
    usart_spi_release_chip_select(USART2);
}


static void readUsart2UntilEmpty(U32 counterIndex)
{
    U8  keepReading = TRUE;
    U8  totalBytesRead;
    U8  bytesReadRank;
    U32 myData[3];

    // This function called when the receiver is expected to be empty. If not, it is an anomaly, so empty it.
    if (usart_is_rx_ready(USART2) == 1) {
        // Keep reading until empty. During initial development and testing, only 1 byte was on occasion
        // detected. Maybe 2 bytes. So the following while loop is setup to track up to 5 bytes.
        totalBytesRead = 0; bytesReadRank = 0; memset(myData, 0xFF, 3);
        while (keepReading) {
            usart_getchar(USART2, &myData[bytesReadRank++]);
            totalBytesRead++;
            // Check for and prevent a stuck-loop condition:
            if (bytesReadRank > 2) { incrementBadEventCounter(PHY2_EMPTY_CHECK_POTENTIAL_STUCK_LOOP); keepReading = FALSE; break; }
            mdelay(KSV_DELAY);
            pokeWatchdog();
            if (usart_is_rx_ready(USART2) == 0) {
                keepReading = FALSE;
            }
        } // while ...
        incrementBadEventCounter(counterIndex);
    }
}


static void setSecondaryControlRegistersDefaults(void)
{
    // Configure those registers of relevance to the Apparent application:
    //
    // The following registers have no relevance in the Apparent application, expected defaults should be:
    // - registers 16/32/48/64/80 address 0x10/20/30/40/50 default = 0x00
    // - registers 17/33/49/65/81 address 0x11/21/31/41/51 default = 0x1F
    // - registers 19/35/51/67/83 address 0x13/23/33/43/53 default = 0x00
    // - registers 20/36/52/68/84 address 0x14/24/34/44/54 default = 0x01
    // - registers 21/37/53/69/85 address 0x15/25/35/45/55 default = 0x00
    // - registers 22/38/54/70/86 address 0x16/26/36/46/56 default = bit [7] [6] [5] [3] [2] [1:0] are SET = 11101111 = 0xDF
    // - registers 23/39/55/71/87 address 0x17/27/37/47/57 default = bit [5:4] [3] [2] [1] [0]     are SET = 00111111 = 0x3F
    // - status registers 24/40/56/72/88 address 0x18/28/38/48/58 default = 0x00 (read only)
    // - status registers 25/41/57/73/89 address 0x19/29/39/49/59 default = 0x80 with operation speed and duplex but are read only
    // - PHY Control - registers 26/42/58/74/90 address 0x1A/2A/3A/4A/5A default = 0x00
    // - Link MD registers 27/43/59/75/91 address 0x1B/2B/3B/4B/5B default = 0x00

    // 1. Disable the "Global Soft Reset Enable" bit in global register address  0x02.
    writeToSecondarySwitch(SEC_WRITE_SEQ_DISABLE_SOFT_RESET_GLOBAL_REG_02);


    // 2. Set the TRANSMIT and RECEIVE PACKET ENABLE for all ports 1/2/3/4 (registers 18/34/50/66 address 0x12/22/32/42).
    //    Typical value coming of out reset: 0x06
    // Supposedly could be done in a for loop with a lookup table ...
    writeToSecondaryByAddress(CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[PORT_5_INDEX], PORT_CONTROL_2_DEFAULT); // port 5 rank[]
    writeToSecondaryByAddress(CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[PORT_6_INDEX], PORT_CONTROL_2_DEFAULT); // port 6 rank[]
    writeToSecondaryByAddress(CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[PORT_7_INDEX], PORT_CONTROL_2_DEFAULT); // port 7 rank[]
    writeToSecondaryByAddress(CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[PORT_8_INDEX], PORT_CONTROL_2_DEFAULT); // port 8 rank[]

  
    // 3. Set the AN/SPEED/DUPLEX for all ports 1/2/3/4 (registers 28/44/60/76/92 address 0x1C/2C/3C/4C/5C).
    //    Typical value coming of out reset: 0x5F
    // I know ... supposedly could be done in a for loop with a lookup table ...
    writeToSecondaryByAddress(CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[PORT_5_INDEX], PORT_CONTROL_9_DEFAULT); // port 5 rank[0]
    writeToSecondaryByAddress(CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[PORT_6_INDEX], PORT_CONTROL_9_DEFAULT); // port 6 rank[1]
    writeToSecondaryByAddress(CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[PORT_7_INDEX], PORT_CONTROL_9_DEFAULT); // port 7 rank[2]
    writeToSecondaryByAddress(CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[PORT_8_INDEX], PORT_CONTROL_9_DEFAULT); // port 8 rank[3]

    // 4. Set the CLEAR POWER DOWN BIT for ports 1/2/3/4 (registers 29/45/61/77 address 0x1D/2D/3D/4D).
    //    Typical value coming of out reset: 0x00
    // I know ... supposedly could be done in a for loop with a lookup table ...
    writeToSecondaryByAddress(CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[PORT_5_INDEX], PORT_CONTROL_10_DEFAULT); // port 5 rank[0]
    writeToSecondaryByAddress(CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[PORT_6_INDEX], PORT_CONTROL_10_DEFAULT); // port 6 rank[1]
    writeToSecondaryByAddress(CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[PORT_7_INDEX], PORT_CONTROL_10_DEFAULT); // port 7 rank[2]
    writeToSecondaryByAddress(CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[PORT_8_INDEX], PORT_CONTROL_10_DEFAULT); // port 8 rank[3]

    // 5. Set the PHY AND LINK NORMAL OPERATION for ports 1/2/3/4 (registers 31/47/63/79 address 0x1F/2F/3F/4F).
    //    Typical value coming of out reset: 0x00 if no device connected, 0x06 if a device is connected.
    // yea yea yea ... supposedly could be done in a for loop with a lookup table ...
    writeToSecondaryByAddress(CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[PORT_5_INDEX], PORT_CONTROL_11_STATUS_3_DEFAULT); // port 5 rank[0]
    writeToSecondaryByAddress(CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[PORT_6_INDEX], PORT_CONTROL_11_STATUS_3_DEFAULT); // port 6 rank[1]
    writeToSecondaryByAddress(CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[PORT_7_INDEX], PORT_CONTROL_11_STATUS_3_DEFAULT); // port 7 rank[2]
    writeToSecondaryByAddress(CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[PORT_8_INDEX], PORT_CONTROL_11_STATUS_3_DEFAULT); // port 8 rank[3]

}


void  readSecondaryMac(void)
{
    U8 registerValue;
    // "SECONDARY MAC":"00:10:A1:FF:FF:FF"}
    printf("%s:", SECONDARY_MAC_STRING);
    registerValue = readFromSecondarySwitchByAddress(SECONDARY_MAC0_REG_68); printf("\"%02X:", registerValue); // leading quote
    registerValue = readFromSecondarySwitchByAddress(SECONDARY_MAC1_REG_69); printf("%02X:",   registerValue);
    registerValue = readFromSecondarySwitchByAddress(SECONDARY_MAC2_REG_6A); printf("%02X:",   registerValue);
    registerValue = readFromSecondarySwitchByAddress(SECONDARY_MAC3_REG_6B); printf("%02X:",   registerValue);
    registerValue = readFromSecondarySwitchByAddress(SECONDARY_MAC4_REG_6C); printf("%02X:",   registerValue);
    registerValue = readFromSecondarySwitchByAddress(SECONDARY_MAC5_REG_6D); printf("%02X\"}", registerValue); // terminating quote
}


// Check if the Link up/down bit is set in the status register.
BOOL isSecondaryLinkStatusLinkUp(U16 statusRegister)
{
    if ((statusRegister & PORT_STATUS_2_LINK_STATUS_MASK) == PORT_STATUS_2_LINK_STATUS_LINK_UP)     { return TRUE;  } // 1 = link UP
    else                                                  /* PORT_STATUS_2_LINK_STATUS_LINK_DOWN */ { return FALSE; } // 0 = link DOWN
}


// Check the "Power Down" bit[3] in the control "10" register. This is a misnomer, as the port is not actually powered down,
// rather, ethernet service is disabled. In any event, return TRUE if service is enabled, i.e., powered up.
BOOL isSecondaryLinkControl10PoweredUp(U16 control10Register)
{
    if ((control10Register & PORT_CONTROL_10_POWER_MASK) == PORT_CONTROL_10_POWER_NORMAL_OPERATION) { return TRUE;  } // 1 = enabled/"power up"
    else                                                 /* PORT_CONTROL_10_POWER_DOWN */           { return FALSE; } // 0 = disabled/"powered down"
}


static void verifySecondaryRegister(U8 address, U8 expectedValue, U16 identifier)
{
    U8 readValue;
    readValue = readFromSecondarySwitchByAddress(address);
    if (readValue != expectedValue) { printf("VERIFY ID %u FAILED: expect: 0x%02X got: 0x%02X\r\n", identifier, expectedValue, readValue); incrementBadEventCounter(ERRATA_VERIFY_ERROR_SECONDARY_CONTROLLER); }
}


static void ksz8795ErrataFixes(void)
{
    U16 iggy;
    U8  addressStatus_1;
    U8  registerControl_9;
    U8  registerControl_10;
    U8  registerStatus_1;
    U8  addressReg_9;
    U8  addressReg_10;

    // From the KSZ8795 Silicon Errata and Data Sheet Clarification

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ8795 ERRATA FIXES:\r\n");
    #endif

    // Module 1: Transmission errors in 100BASE-TX Mode when Auto-Negotiation is disabled
    //
    // DESCRIPTION
    // When a PHY port is configured to disable Auto-Negotiation (AN) and forced to 100BASE-TX full/half-duplex mode,
    // transmission errors may be output from the egress port.
    //
    // END USER IMPLICATIONS
    // When this issue occurs, transmitted errors output from egress port may result in an unstable link with the partner.
    // Only PHY ports (Ports 1 to 4 for the KSZ8765/KSZ8775/KSZ8795, ports 1 to 3 for the KSZ8794) are affected by this
    // issue. The Gigabit MAC processor port is unaffected. Additionally, PHY ports will not exhibit this issue if configured to
    // Auto-Negotiation mode or forced to 10BASE-T mode.
    //
    // Work Around
    // This issue can be prevented by using the appropriate script below when setting the port to force 100BASE-TX
    // link (full-duplex) mode:
    // � Auto-MDI/MDIX Mode:
    // w nd 40 //Disable TX by writing 40h register NDh
    // w n9 00 //Set to Auto-MDI/MDIX by writing 00h to register N9h                               THIS ONE!!!!!!!!!!!!!!!!!!
    // w nc ff //Force 100 FD mode (disable AN)by writing FFh to register NCh
    // w nd 00 //Enable TX by writing 00h to register NDh
    // � Force-MDI Mode:
    // w nd 44 //Disable TX and Force to MDI by writing 44h to register NCh            <- APPARENT:they really mean NDh (NOT NCh)
    // w nc ff //Force 100 FD mode (disable AN) by writing FFh to register NCh         <- APPARENT: NCh register set already configured with 0xFF (reviewed and verified)
    // w nd 04 //Enable TX by writing 04h to regsiter NDh
    // � Force-MDIX Mode:
    // w nd 46 //Disable TX and Force to MDIX by writing 46h to register NDh
    // w nc ff //Force 100 FD mode (disable AN) by writing FFh to register NCh     <- Apparent note: why this, a 23rd time???
    // w nd 06 //Enable TX by writing 06h to register NDh

    // Do for all ports 1..4:
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {

        addressStatus_1    = STATUS_1_REG_ADDRESS_BY_PORT_LOOKUP[iggy];
        addressReg_9       = CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[iggy];
        addressReg_10      = CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[iggy];
        registerStatus_1   = readFromSecondarySwitchByAddress(addressStatus_1);
        registerControl_9  = readFromSecondarySwitchByAddress(addressReg_9);
        registerControl_10 = readFromSecondarySwitchByAddress(addressReg_10);

        // Step 1: TXDIS is disabled (SET the bit)
        registerControl_10 |= PORT_CONTROL_10_TXDIS_TRANSMITTER_DISABLE;
        writeToSecondaryByAddress(addressReg_10, registerControl_10); verifySecondaryRegister(addressReg_10, registerControl_10, 50);

        // Step 2: AUTO MDI is set to "Microchip" (CLEAR the bit).
        registerStatus_1 &= ~(PORT_STATUS_1_HP_MDIX_MICROCHIP_MODE | PORT_STATUS_1_HP_MDIX_MASK);
        writeToSecondaryByAddress(addressStatus_1, registerStatus_1); verifySecondaryRegister(addressReg_10, registerControl_10, 51);

        // Step 3: 100_BASE_T is enabled (SET the bit), although this was already SET
        registerControl_9 |= PORT_CONTROL_9_FORCE_100_BASE_T_ENABLE;
        writeToSecondaryByAddress(addressReg_9, registerControl_9); verifySecondaryRegister(addressReg_10, registerControl_10, 52);

        // Step 4: // TXDIS is enabled (restored, CLEAR the bit)
        registerControl_10 &= ~(PORT_CONTROL_10_TXDIS_TRANSMITTER_DISABLE | PORT_CONTROL_10_TXDIS_TRANSMITTER_MASK);
        writeToSecondaryByAddress(addressReg_10, registerControl_10); verifySecondaryRegister(addressReg_10, registerControl_10, 53);

        // The other 2 steps of the errata disable the "Force-MDI Mode" and "Force-MDIX Mode" bits,
        // however, they are already disabled during initial controller configuration. Presumably,
        // the premise is that TXDIS should first be disabled, then the 2 MDI bits are disabled.
        // This would have been accomplished in step 5, so here, register 10 is simply read back
        // validated. 
        registerControl_10 = readFromSecondarySwitchByAddress(addressReg_10);
        if (((registerControl_10 & PORT_CONTROL_10_AUTO_MDI_MDIX_DISABLE)    == PORT_CONTROL_10_AUTO_MDI_MDIX_DISABLE)
         && ((registerControl_10 & PORT_CONTROL_10_FORCED_MDI_MODE_DISABLED) == PORT_CONTROL_10_FORCED_MDI_MODE_DISABLED) )
        {
            // Both bits are SET, so DISABLED. All is good.
        } else {
            // Something wrong ...
            incrementBadEventCounter(ERRATA_VERIFY_ERROR_SECONDARY_CONTROLLER);
        }
    }


    // Module 2: Link drops with some EEE link partners
    // DESCRIPTION
    // An issue with the EEE next page exchange between the KSZ879x/KSZ877x/KSZ876x and some EEE link partners may
    // result in the link dropping.
    // END USER IMPLICATIONS
    // With certain EEE link partners, the KSZ879x/KSZ877x/KSZ876x may inadvertently drop the link between devices.
    // Work Around
    // Disable the EEE next page exchange in EEE Global Register 2 (0x35) by setting bits [3:0] to 0x0 (bits [3:0] default
    // to 0xF).
    writeToSecondaryByAddress(0x6E, 0xA0); // write 0xA0 to register 0x6E to set up an indirect register write
    writeToSecondaryByAddress(0x6F, 0x35); // write 0x35 to register 0x6F to set the indirect register address to 0x35
    writeToSecondaryByAddress(0xA0, 0x0F); // write 0x0F to the indirect register

    // Module 3: Establishing a link through low loss connections
    // DESCRIPTION
    // The receiver of the embedded PHYs is tuned by default to support long cable length applications. This was developed
    // using low quality, high loss cables. Because of this, the equalizer in the PHY may amplify high amplitude receiver signals
    // to the point that the signal is distorted internally, preventing a link from being established.
    // END USER IMPLICATIONS
    // When connecting the PHY ports to link partners through low loss connections, the port may take a long time to establish
    // a link, or may not link at all. This includes standard unshielded twisted pair cables less than 5m, capacitive coupled connections
    // between two chips on the same PCB, or custom cables with low impedance loading.
    //
    // Apparent note: the errata describes Work Around 1 (implemented here), and "If work around 1 does not solve the short cable issue ...",
    //                a "Work Around 2" is proposed in lieu - which is not implemented. The errata specifies writing to indirect offset
    //                register 0x3C, but this register is not described in the data sheet.
    writeToSecondaryByAddress(0x6E, 0xA0); // write 0xA0 to register 0x6E to set up an indirect register write
    writeToSecondaryByAddress(0x6F, 0x3C); // write 0x3C to register 0x6F to set the indirect register address to 0x3C
    writeToSecondaryByAddress(0xA0, 0x15); // write 0x15 to the indirect register

    // Module 4: Idle transmitter common mode voltage drift at cold temperature
    // DESCRIPTION
    // Below 0�C, the voltage controlled output of the transmitter, when inactive, can become unstable and affect the receiver
    // of the link partner. This voltage instability is only observed when the transmitter is in a low activity state, such as auto-negotiation
    // and 10Base-T idle conditions.
    // END USER IMPLICATIONS
    // This errata can manifest in long delays for the port to establish a link at cold temperatures, as well as errors when connected
    // with a 10Base-T connection.
    //
    // Apparent note: as with 0x3C above, indirect register address 0x4E is not described in the data sheet.
    writeToSecondaryByAddress(0x6E, 0xA0); // write 0xA0 to register 0x6E to set up an indirect register write
    writeToSecondaryByAddress(0x6F, 0x4E); // write 0x4E to register 0x6F to set the indirect register address to 0x4E
    writeToSecondaryByAddress(0xA0, 0x40); // write 0x40 to enable the transmitter to actively transmit at all times


    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ8795 ERRATA FIXES: DONE\r\n");
    #endif
}


void  initSecondarySwitchData(void)
{
    U16 iggy;

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("-- Initializing secondary KSZ8795 switch controller data\r\n");
    #endif

    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  GLOBAL_REGISTER_CHIP_ID_FAMILY_REG_00_ADDR,       0x00, &SEC_READ_SEQ_GLOBAL_REG_00[0]);                // Register number  0 <- chip family, default 0x87
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_ADDR,      0x00, &SEC_READ_SEQ_GLOBAL_REG_01[0]);                // Register number  1 <- chip ID [7..4] + Start Switch bit [0x01]
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR,           0x00, &SEC_READ_SEQ_GLOBAL_REG_02[0]);                // Register number  2 <- sw reset, flush dynamic mac + a few other things

    // For these 2: bits [3..2] must be 11:
    build3ByteCommandSequence(SECONDARY_WRITE_COMMAND, GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR,           0x4C, &SEC_WRITE_SEQ_ENABLE_SOFT_RESET_GLOBAL_REG_02[0]);     // Register number 2 <- Global sw reset Enable  bit[6] = 1 to "reset all FSM and data path"
    build3ByteCommandSequence(SECONDARY_WRITE_COMMAND, GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR,           0x0C, &SEC_WRITE_SEQ_DISABLE_SOFT_RESET_GLOBAL_REG_02[0]);    // Register number 2 <- Global sw reset Disable bit[6] = 0 to disable this reset

    // control_2
    // The set of registers 18/34/50/55/82 (0x12/22/32/42):
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_1_ADDRESS_CONTROL_2,           0x00,  &SEC_READ_SEQ_PORT_1_CONTROL_2_REG_12[0]);     // Register number 28
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_2_ADDRESS_CONTROL_2,           0x00,  &SEC_READ_SEQ_PORT_2_CONTROL_2_REG_22[0]);     // Register number 44
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_3_ADDRESS_CONTROL_2,           0x00,  &SEC_READ_SEQ_PORT_3_CONTROL_2_REG_32[0]);     // Register number 60
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_4_ADDRESS_CONTROL_2,           0x00,  &SEC_READ_SEQ_PORT_4_CONTROL_2_REG_42[0]);     // Register number 76

    // The set of registers 28/44/60/76 (0x1C/2C/3C/4C):
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_1_ADDRESS_CONTROL_9,           0x00,  &SEC_READ_SEQ_PORT_1_CONTROL_9_REG_1C[0]);     // Register number 28
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_2_ADDRESS_CONTROL_9,           0x00,  &SEC_READ_SEQ_PORT_2_CONTROL_9_REG_2C[0]);     // Register number 44
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_3_ADDRESS_CONTROL_9,           0x00,  &SEC_READ_SEQ_PORT_3_CONTROL_9_REG_3C[0]);     // Register number 60
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_4_ADDRESS_CONTROL_9,           0x00,  &SEC_READ_SEQ_PORT_4_CONTROL_9_REG_4C[0]);     // Register number 76

    // The set of registers 29/45/61/77 (0x1D/2D/3D/4D):
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_1_ADDRESS_CONTROL_10,          0x00,  &SEC_READ_SEQ_PORT_1_CONTROL_10_REG_1D[0]);    // Register number 29
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_2_ADDRESS_CONTROL_10,          0x00,  &SEC_READ_SEQ_PORT_2_CONTROL_10_REG_2D[0]);    // Register number 45
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_3_ADDRESS_CONTROL_10,          0x00,  &SEC_READ_SEQ_PORT_3_CONTROL_10_REG_3D[0]);    // Register number 61
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_4_ADDRESS_CONTROL_10,          0x00,  &SEC_READ_SEQ_PORT_4_CONTROL_10_REG_4D[0]);    // Register number 77

    // The set for registers 30/6/62/78 (0x1E/2E/3E/4E):
    // (status registers are read-only, so no write sequences):
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_1_ADDRESS_STATUS_2,            0x00,   &SEC_READ_SEQ_PORT_1_STATUS_2_REG_1E[0]);     // Register number 30
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_2_ADDRESS_STATUS_2,            0x00,   &SEC_READ_SEQ_PORT_2_STATUS_2_REG_2E[0]);     // Register number 46
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_3_ADDRESS_STATUS_2,            0x00,   &SEC_READ_SEQ_PORT_3_STATUS_2_REG_3E[0]);     // Register number 62
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_4_ADDRESS_STATUS_2,            0x00,   &SEC_READ_SEQ_PORT_4_STATUS_2_REG_4E[0]);     // Register number 78

    // The set for registers 31/47/63/79 (0x1F/2F/3F/4F):
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_1_ADDRESS_CONTROL_11_STATUS_3, 0x00,   &SEC_READ_SEQ_PORT_1_CONTROL_11_REG_1F[0]);   // Register number 31
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_2_ADDRESS_CONTROL_11_STATUS_3, 0x00,   &SEC_READ_SEQ_PORT_2_CONTROL_11_REG_2F[0]);   // Register number 47
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_3_ADDRESS_CONTROL_11_STATUS_3, 0x00,   &SEC_READ_SEQ_PORT_3_CONTROL_11_REG_3F[0]);   // Register number 63
    build3ByteCommandSequence(SECONDARY_READ_COMMAND,  PORT_REGISTER_PORT_4_ADDRESS_CONTROL_11_STATUS_3, 0x00,   &SEC_READ_SEQ_PORT_4_CONTROL_11_REG_4F[0]);   // Register number 79

    // Any more?

    // Anything else?
    for (iggy = 0; iggy < NUMBER_SECONDARY_SWITCH_PORTS; iggy++) {
        secondarySwitchInfoCb[iggy].controlReg_2  = PORT_CONTROL_2_DEFAULT;
        secondarySwitchInfoCb[iggy].controlReg_9  = PORT_CONTROL_9_DEFAULT;
        secondarySwitchInfoCb[iggy].controlReg_10 = PORT_CONTROL_10_DEFAULT;
        secondarySwitchInfoCb[iggy].controlReg_11 = PORT_CONTROL_11_STATUS_3_DEFAULT;
        secondarySwitchInfoCb[iggy].portPoweredUp = TRUE;
        secondarySwitchInfoCb[iggy].status2       = 0xFF;
    }
}


// Configure the ethernet chip controller for SWITCH1/PRIMARY/over USART2.
//
// The sequence of actions is based on the Handy Randy hardware test script.
void configureSecondarySwitchController(void)
{
    U32 pioMask;
    U32 some32Bit;

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("- KSZ8795/USART2 SECONDARY ethernet controller configuration: START (tick:%lu)\r\n", g_ul_ms_ticks);
    #endif

    // 1.
    // From the data sheet section 41.6.3 Interrupt:
    // The SPI interface has an interrupt line connected to the interrupt controller. Handling the SPI interrupt requires
    // programming the interrupt controller before configuring the SPI.
    // Similar to UART0/1 where Tx is disabled, Rx is enabled:
    NVIC_EnableIRQ((IRQn_Type) USART2_IRQn);

    usart_enable_interrupt(USART2,  SW2_MISO_GPIO);      // receive  is enabled
    usart_disable_interrupt(USART2, SW2_MOSI_GPIO);      // transmit is disabled

    // These need to be disabled. Probably not required, but doesn't hurt.
    usart_disable_interrupt(USART2, SW2_CSN_GPIO);
    usart_disable_interrupt(USART2, SW2_SCLK_GPIO);

    // 2.
    // Enable Peripheral Clocks for USART2.
    // No facility existed to write PMC_PCER0 register, it was created:
    // script: PMC = 0x400E0600
    // script: PMC_PCER0 = 0x0010
    // script: write to PCM_PCER0 = 0x400e0610 value 0000a000
    pmc_enable_periph_clk_pcer0(ID_USART2);  // <- formerly part of a printf(xxx) which is no longer done on startup

    // 3.
    // Configure PD15,PD16,PD17,PD18 for USART2 Peripheral B Function
    // The script sets: PIO_PDR     = 00078000
    //                  PIO_ABCDSR1 = 00078000

    // From same70q21b.h: PIO_PD15 (1u << 15)    PIO_PD16 (1u << 16)    PIO_PD17 (1u << 17)    PIO_PD18 (1u << 18)
    pioMask = PIO_PD15 | PIO_PD16 | PIO_PD17 | PIO_PD18; // = 78000 verified
    pio_set_peripheral((Pio *)(REG_PIOD_PDR),    PIO_PERIPH_B, pioMask); // REG_PIOD_PDR (0x400E1404U) /**< \brief (PIOD) PIO Disable Register */
    pio_set_peripheral((Pio *)(REG_PIOD_ABCDSR), PIO_PERIPH_B, pioMask); // REG_PIOD_ABCDSR 0x400E1470

    {
        #define BAUD_RATE_TO_USE      19200
        #define CLOCK_TO_USE        1000000
        const usart_spi_opt_t p_usart_opt = { .baudrate     = BAUD_RATE_TO_USE,
                                              .char_length  = US_MR_CHRL_8_BIT,
                                              .spi_mode     = SPI_MODE_3,
                                              .channel_mode = US_MR_CHMODE_NORMAL,
                                            };
        // 4.
        // Set USART2 SCLK Frequency (22 MHz max) SCLK to 1 MHz')
        if (usart_set_async_baudrate(USART2, BAUD_RATE_TO_USE, CLOCK_TO_USE) == 0) {                                                              }
        else                                                                       { incrementBadEventCounter(SECONDARY_CONTROLLER_CONFIG_ERROR); }

        // 5.
        // # Set USART2 to SPI Master Mode 3, CPOL=1, CPHA=0, 8-bit data
        // USART2->US_BRGR will have been written with: 0x9C
        if (usart_init_spi_master(USART2, &p_usart_opt, CLOCK_TO_USE) == 0)  { /* No news is good news. */                                  }
        else                                                                 { incrementBadEventCounter(SECONDARY_CONTROLLER_CONFIG_ERROR); }

        // No specification was provided for the baudrate to be configured. As with the primary controller,
        // it is assumed that the hardware script is correct (since it was also on advice from the Microchip
        // service), so the BRGR register is validated to contain 0x84.
        some32Bit = get_brgr_value(USART2);
        if (some32Bit != 0x84) { set_brgr_value(USART2, 0x84); }

    }

    // 6.
    // Enable SPI0 TX and RX.
    usart_enable_tx(USART2);
    usart_enable_rx(USART2);
    usart_get_status(USART2), usart_get_mode(USART2), get_brgr_value(USART2); // <- formerly part of a printf(xxx) which is no longer done on startup

    // Check if USART2 transmit ready.
    if (usart_is_tx_ready(USART2) == 0) {   } // 0 There is data in the Transmit Holding Register.
    else                                {   } // 1 No data is in the Transmit Holding Register.
    // Check if USART2 receive ready.
    if (usart_is_rx_ready(USART2) == 0) {} // 0 No data in the receiver, as expected on startup.
    else                                {  // Data in the receiver. NOT expected. Empty the receiver.
                                           readUsart2UntilEmpty(PHY2_DATA_PRESENT_DURING_STARTUP);
                                           incrementBadEventCounter(SECONDARY_CONTROLLER_CONFIG_ERROR);
                                        }

    usart_reset_status(USART2); // Command the controller to reset the status register. During testing, at one point when things
                                // were not working well, the US_MR_OVER register bit was set - this clears it, among other things.

    // Set the switch CONTROL registers for all ports to prescribed defaults.
    setSecondaryControlRegistersDefaults();

    // Implementing all applicable fixes of the KSZ8795 errata:
    ksz8795ErrataFixes();

    // Enable ethernet functionality.
    verifySecondaryEthernetService();

    // Probably too early for this, but what the heck.
    updateSecondaryPortStatus();

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ8795/USART2 SECONDARY ethernet controller configuration: DONE (tick:%ld)\r\n", g_ul_ms_ticks);
    #endif
}


void enableSecondaryEthernetService(void)
{
    writeToSecondaryByAddress(GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_ADDR, GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_SWITCH);
    // ETHERNET HAS BEEN INADVERTANTLY DISABLED!!! There is no data with this event,
    // other than it was found disabled and was automatically re-enabled.
    // {"EVT":{EI":8,"TICK":12345678}
    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%ld}}", EVT_STRING,
                                                               EVENT_ID_STRING, AEI_SECONDARY_ETH_FOUND_DISABLED,
                                                               TICK_STRING,     g_ul_ms_ticks);
    addJsonToAutonomnousString(pSingleJsonEventString);
    incrementBadEventCounter(SECONDARY_ETHERNET_SERVICE_RE_ENABLED);
}


// secondaryControllerReset
//
//
//
void secondaryControllerReset(void)
{
    U32 startRegisterValue;
    U32 setRegisterValue;
    U32 clearRegisterValue;
    U32 finalRegisterValue;


    // THIS FUNCTION DOES NOT WORK. The controller is not reset, bit[6] of 0x02 is not what you want to access.
    //                              Instead, refer to the data sheet: 4.11 MIIM Registers page 109.


    // To reset the secondary controller: SET bit[6] of the "Global Control 0" register (address 0x02).
    startRegisterValue = readFromSecondarySwitchByAddress(GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR);
    setRegisterValue   = startRegisterValue | GLOBAL_SOFT_RESET_ENABLE_REG_02_ENABLE_MASK;
    writeToSecondaryByAddress(GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR, setRegisterValue);

    // You would think this bit to be self-clearing like the primary controller, but you'd be wrong.
    // So the bit has to be cleared.
    mdelay(250);
    clearRegisterValue = setRegisterValue & (~GLOBAL_SOFT_RESET_ENABLE_REG_02_ENABLE_MASK);
    writeToSecondaryByAddress(GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR, clearRegisterValue);
    mdelay(250);
    finalRegisterValue = readFromSecondarySwitchByAddress(GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR);
    printf("SECONDARY CONTROLLER RESET: 0x%02lX -> 0x%02lX -> 0x%02lX (RE-READ:0x%02lX) in REG ADDR 0x%02X\r\n", startRegisterValue, setRegisterValue, clearRegisterValue, finalRegisterValue, GLOBAL_REGISTER_SOFT_RESET_REG_02_ADDR);
}


// checkSecondaryEthernetService
//
// Called from main on periodic basis.
//
// Make sure that the "Start Switch" bit[0] in global register address 0x01 is set.
// As with the primary controller, during early testing, it was observed to be cleared,
// probably during an AMU restart, or perhaps during development when the code was not
// quite right. Not seen since. But checked nevertheless.
void checkSecondaryEthernetService(void)
{
    if (isSecondaryEthernetServiceEnabled()) { /* NO news is good news ... */    }
    else                                     { enableSecondaryEthernetService(); }
}


static BOOL isSecondaryEthernetServiceEnabled(void)
{
    // The "start switch" bit[0] in global register 0x01 determines if the overall ethernet service
    // through the secondary controller is enabled or not.
    U32 registerValue;
    registerValue = readFromSecondarySwitchByAddress(GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_ADDR);
    if ((registerValue & GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_MASK) == GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_MASK) { return TRUE;  }
    else                                                                                                                          { return FALSE; }
    return TRUE;
}



static void updateSecondaryPortStatus(void)
{
    // For loops are good for looping, but I don't feel loopy at this point in time.
    secondarySwitchInfoCb[0].status2 = readFromSecondarySwitchByAddress(STATUS_REG_2_ADDRESS_BY_PORT_LOOKUP[0]); // external port 5
    secondarySwitchInfoCb[1].status2 = readFromSecondarySwitchByAddress(STATUS_REG_2_ADDRESS_BY_PORT_LOOKUP[1]); // external port 6
    secondarySwitchInfoCb[2].status2 = readFromSecondarySwitchByAddress(STATUS_REG_2_ADDRESS_BY_PORT_LOOKUP[2]); // external port 7
    secondarySwitchInfoCb[3].status2 = readFromSecondarySwitchByAddress(STATUS_REG_2_ADDRESS_BY_PORT_LOOKUP[3]); // external port 8
}


void configureSecondaryPortPowerBit(U16 portIndex, U8 portKey,  BOOL powerUp)
{
    U8 controlRegisterBefore;
    U8 controlRegisterAfter;

    // Read out the current value of "control 10" register for this port,
    // set/clear the "Power Down" bit[3] accordingly and write it back.
    //
    // As with the equivalent bit for the primary switch, this one is just as counter-intuitive:
    //
    // - if the port is to be POWERED UP,    then CLEAR bit[3]
    // - if the port is to be POWRERED DOWN, then SET   bit[3]
    controlRegisterBefore = readFromSecondarySwitch(CONTROL_REG_10_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);

    if (powerUp == TRUE) { controlRegisterAfter = controlRegisterBefore & ~(PORT_CONTROL_10_POWER_MASK | PORT_CONTROL_10_POWER_NORMAL_OPERATION); } // <- CLEAR bit[11], port is ENABLED
    else                 { controlRegisterAfter = controlRegisterBefore |  (PORT_CONTROL_10_POWER_MASK & PORT_CONTROL_10_POWER_DOWN);             } // <- SET bit[11],   port is DISABLED
    writeToSecondaryByAddress(CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[portIndex], controlRegisterAfter); // write the updated control register value to the controller
    secondarySwitchInfoCb[portIndex].controlReg_10 = controlRegisterAfter;                             // save the control register value for auditing
    secondarySwitchInfoCb[portIndex].portPoweredUp = powerUp;                                          // save the configuration boolean

#if ENABLE_CRAFT_DEBUG == TRUE
    // Secondary port 5..8: index + 5
    sprintf(&pCraftPortDebugData[0], "ETH SEC PORT %u: CONFIG POWER 0x%04X -> 0x%04X (%lu)\r\n", (portIndex+5), controlRegisterBefore, controlRegisterAfter, g_ul_ms_ticks);
    UART1_WRITE_PACKET_MACRO
#endif

    // The output is part of a list of ports whose power bit is to be re-configured. Print something like:
    // {"PORT":5,"COMMAND":"POWER_UP","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"}
    printf("{%s:%u,%s:%s,%s:\"0x%04X\",%s:\"0x%04X\",%s:\"0x%04X\"}", PORT_STRING,           portKey,
                                                                      COMMAND_STRING,        (powerUp == TRUE ? PORT_COMMAND_POWER_UP_STRING : PORT_COMMAND_POWER_DOWN_STRING),
                                                                      REG_ADDR_STRING,       CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                                                      CONTROL_BEFORE_STRING, controlRegisterBefore,
                                                                      CONTROL_AFTER_STRING,  controlRegisterAfter);
}


static void verifySecondaryEthernetService(void)
{
    // Called during system initialization. Normally, ethernet service always enabled. It is never disabled.
    if (isSecondaryEthernetServiceEnabled() == TRUE) {
        //
    } else {
        // YIKES! Totally unexpected. Enable and holler. In addition to a simple printf,
        // add an autonomous event in a JSON-like string of the form:
        // {"EVT":{"SECONDARY SWITCH":"ETH ENABLED",EI":8,"TICK":12345678}}
        // {"EVT":{"EI":9,"INFO":"SEC ETH CONTROLLER ENABLED DURING AMU STARTUP","TICK":12345678}}
        writeToSecondaryByAddress(GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_ADDR, GLOBAL_REGISTER_SWITCH_FUNCTION_REG_01_START_SWITCH);
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%s,%s:%lu}}",
                                            EVT_STRING,      EVENT_ID_STRING, AEI_SECONDARY_ETH_ENABLE,
                                                             INFO_STRING,     SECONDARY_ENABLED_ON_STARTUP_STRING,
                                                             TICK_STRING,     g_ul_ms_ticks);
        addJsonToAutonomnousString(pSingleJsonEventString);
        incrementBadEventCounter(SECONDARY_ENABLED_ON_STARTUP);
    }
}


// TODO: the next 4 functions are soooo similar ...
//       how similar are they?
//       they are so similar, they deserve to be tabularized.
// - checkSecondaryControlReg_02
// - checkSecondaryControlReg_09
// - checkSecondaryControlReg_10
// - checkSecondaryControlReg_11
//
// or ... all 4 functions collapsed into 1 in a for loop.


// checkSecondaryControlReg_02
//
// {"EVT":{"EI":26,"PORT":"<5|6|7|8>","REG ADDR":"0x12","EXPECTED VALUE":"0x12","ACTUAL VALUE":"0x34"}}
//
//
void checkSecondaryControlReg_02(U32 portIndex)
{
    U8   controlRegister;
    controlRegister = readFromSecondarySwitch(CONTROL_REG_2_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);
    if (controlRegister != secondarySwitchInfoCb[portIndex].controlReg_2) {
        // Write the autonomous event and restore the port's control register.
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%02X\",%s:\"0x%02X\",%s:\"0x%02X\"}}",
                                            EVT_STRING,
                                            EVENT_ID_STRING,     AEI_SECONDARY_CONTROL_02_RESTORED,
                                            PORT_STRING,         pGimmeSecondaryPortString(portIndex),
                                            REG_ADDR_STRING,     CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                            CONTROL_FROM_STRING, controlRegister,
                                            CONTROL_TO_STRING,   secondarySwitchInfoCb[portIndex].controlReg_2);
        writeToSecondaryByAddress(CONTROL_REG_2_ADDRESS_BY_PORT_LOOKUP[portIndex], secondarySwitchInfoCb[portIndex].controlReg_2);
        addJsonToAutonomnousString(pSingleJsonEventString);

        if (secondarySwitchInfoCb[portIndex].restored02 == FALSE) {
            // First time through after having to restore the control register. It is expected to be fine upon the next cycle.
            secondarySwitchInfoCb[portIndex].restored02 = TRUE;
        } else {
            // Was restored in last cycle, and required to be restored yet again. Not good. Not good at all.
            // Increment a bad event counter, this will show up in the gateway log.
            secondarySwitchInfoCb[portIndex].restored02 = FALSE;
            incrementBadEventCounter(SECONDARY_CONTROL_02_RESTORE_FAILED);
        }

#if ENABLE_CRAFT_DEBUG == TRUE
        // Secondary port 5..8: index + 5
        sprintf(&pCraftPortDebugData[0], "ETH SEC PORT %lu: CONTROL REG 2=0x%02X EXPECTING 0x%02X (POWERED UP=%u)\r\n",
                (portIndex+5), controlRegister, secondarySwitchInfoCb[portIndex].controlReg_2, secondarySwitchInfoCb[portIndex].portPoweredUp);
        UART1_WRITE_PACKET_MACRO
#endif

    } else {
        //
        secondarySwitchInfoCb[portIndex].restored02 = FALSE;
    }
}


// checkSecondaryControlReg_09
//
// {"EVT":{"EI":27,"PORT":"<5|6|7|8>","REG ADDR":"0x12","EXPECTED VALUE":"0x12","ACTUAL VALUE":"0x34"}}
//
//
void checkSecondaryControlReg_09(U32 portIndex)
{
    U8   controlRegister;
    controlRegister = readFromSecondarySwitch(CONTROL_REG_9_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);
    if (controlRegister != secondarySwitchInfoCb[portIndex].controlReg_9) {
        // Write the autonomous event and restore the port's control register.
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%02X\",%s:\"0x%02X\",%s:\"0x%02X\"}}",
                                            EVT_STRING,
                                            EVENT_ID_STRING,     AEI_SECONDARY_CONTROL_09_RESTORED,
                                            PORT_STRING,         pGimmeSecondaryPortString(portIndex),
                                            REG_ADDR_STRING,     CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                            CONTROL_FROM_STRING, controlRegister,
                                            CONTROL_TO_STRING,   secondarySwitchInfoCb[portIndex].controlReg_9);
        writeToSecondaryByAddress(CONTROL_REG_9_ADDRESS_BY_PORT_LOOKUP[portIndex], secondarySwitchInfoCb[portIndex].controlReg_9);
        addJsonToAutonomnousString(pSingleJsonEventString);

        if (secondarySwitchInfoCb[portIndex].restored09 == FALSE) {
            // First time through after having to restore the control register. It is expected to be fine upon the next cycle.
            secondarySwitchInfoCb[portIndex].restored09 = TRUE;
        } else {
            // Was restored in last cycle, and required to be restored yet again. Not good. Not good at all.
            // Increment a bad event counter, this will show up in the gateway log.
            secondarySwitchInfoCb[portIndex].restored09 = FALSE;
            incrementBadEventCounter(SECONDARY_CONTROL_09_RESTORE_FAILED);
        }

#if ENABLE_CRAFT_DEBUG == TRUE
        // Secondary port 5..8: index + 5
        sprintf(&pCraftPortDebugData[0], "ETH SEC PORT %lu: CONTROL REG 9=0x%02X EXPECTING 0x%02X (POWERED UP=%u)\r\n",
                (portIndex+5), controlRegister, secondarySwitchInfoCb[portIndex].controlReg_9, secondarySwitchInfoCb[portIndex].portPoweredUp);
        UART1_WRITE_PACKET_MACRO
#endif

    } else {
        //
        secondarySwitchInfoCb[portIndex].restored09 = FALSE;
    }
}


// checkSecondaryControlReg_10
//
// {"EVT":{"EI":28,"PORT":"<5|6|7|8>","REG ADDR":"0x12","EXPECTED VALUE":"0x12","ACTUAL VALUE":"0x34"}}
//
//
void checkSecondaryControlReg_10(U32 portIndex)
{
    U8   controlRegister;
    controlRegister = readFromSecondarySwitch(CONTROL_REG_10_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);
    if (controlRegister != secondarySwitchInfoCb[portIndex].controlReg_10) {
        // Write the autonomous event and restore the port's control register.
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%02X\",%s:\"0x%02X\",%s:\"0x%02X\"}}",
                                            EVT_STRING,
                                            EVENT_ID_STRING,     AEI_SECONDARY_CONTROL_10_RESTORED,
                                            PORT_STRING,         pGimmeSecondaryPortString(portIndex),
                                            REG_ADDR_STRING,     CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                            CONTROL_FROM_STRING, controlRegister,
                                            CONTROL_TO_STRING,   secondarySwitchInfoCb[portIndex].controlReg_10);
        writeToSecondaryByAddress(CONTROL_REG_10_ADDRESS_BY_PORT_LOOKUP[portIndex], secondarySwitchInfoCb[portIndex].controlReg_10);
        addJsonToAutonomnousString(pSingleJsonEventString);

        if (secondarySwitchInfoCb[portIndex].restored10 == FALSE) {
            // First time through after having to restore the control register. It is expected to be fine upon the next cycle.
            secondarySwitchInfoCb[portIndex].restored10 = TRUE;
        } else {
            // Was restored in last cycle, and required to be restored yet again. Not good. Not good at all.
            // Increment a bad event counter, this will show up in the gateway log.
            secondarySwitchInfoCb[portIndex].restored10 = FALSE;
            incrementBadEventCounter(SECONDARY_CONTROL_10_RESTORE_FAILED);
        }

#if ENABLE_CRAFT_DEBUG == TRUE
        // Secondary port 5..8: index + 5
        sprintf(&pCraftPortDebugData[0], "ETH SEC PORT %lu: CONTROL REG 10=0x%02X EXPECTING 0x%02X (POWERED UP=%u)\r\n",
                (portIndex+5), controlRegister, secondarySwitchInfoCb[portIndex].controlReg_10, secondarySwitchInfoCb[portIndex].portPoweredUp);
        UART1_WRITE_PACKET_MACRO
#endif

    } else {
        //
        secondarySwitchInfoCb[portIndex].restored10 = FALSE;
    }
}


// checkSecondaryControlReg_11
//
// {"EVT":{"EI":29,"PORT":"<5|6|7|8>","REG ADDR":"0x12","EXPECTED VALUE":"0x12","ACTUAL VALUE":"0x34"}}
//
//
void checkSecondaryControlReg_11(U32 portIndex)
{
    U8   controlRegister;

    // Control 11 register set: the bits [2..0] are the "port Operation Mode", is R/O,
    // and will change depending on whether or not a device is connected on the port.
    // So ignore those bits.
    controlRegister = readFromSecondarySwitch(CONTROL_11_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);
    controlRegister = controlRegister & (~PORT_CONTROL_11_STATUS_3_PORT_OPERATION_MODE_MASK);
    if (controlRegister != secondarySwitchInfoCb[portIndex].controlReg_11) {
        // Write the autonomous event and restore the port's control register.
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%02X\",%s:\"0x%02X\",%s:\"0x%02X\"}}",
                                            EVT_STRING,
                                            EVENT_ID_STRING,     AEI_SECONDARY_CONTROL_11_RESTORED,
                                            PORT_STRING,         pGimmeSecondaryPortString(portIndex),
                                            REG_ADDR_STRING,     CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                            CONTROL_FROM_STRING, controlRegister,
                                            CONTROL_TO_STRING,   secondarySwitchInfoCb[portIndex].controlReg_11);
        writeToSecondaryByAddress(CONTROL_REG_11_ADDRESS_BY_PORT_LOOKUP[portIndex], secondarySwitchInfoCb[portIndex].controlReg_11);
        addJsonToAutonomnousString(pSingleJsonEventString);

        if (secondarySwitchInfoCb[portIndex].restored11 == FALSE) {
            // First time through after having to restore the control register. It is expected to be fine upon the next cycle.
            secondarySwitchInfoCb[portIndex].restored11 = TRUE;
        } else {
            // Was restored in last cycle, and required to be restored yet again. Not good. Not good at all.
            // Increment a bad event counter, this will show up in the gateway log.
            secondarySwitchInfoCb[portIndex].restored11 = FALSE;
            incrementBadEventCounter(SECONDARY_CONTROL_11_RESTORE_FAILED);
        }

#if ENABLE_CRAFT_DEBUG == TRUE
        // Secondary port 5..8: index + 5
        sprintf(&pCraftPortDebugData[0], "ETH SEC PORT %lu: CONTROL REG 11=0x%02X EXPECTING 0x%02X (POWERED UP=%u)\r\n",
                (portIndex+5), controlRegister, secondarySwitchInfoCb[portIndex].controlReg_11, secondarySwitchInfoCb[portIndex].portPoweredUp);
        UART1_WRITE_PACKET_MACRO
#endif

    } else {
        //
        secondarySwitchInfoCb[portIndex].restored11 = FALSE;
    }
}


void checkSecondaryStatusRegisters(U32 portIndex)
{
    U8  statusRegister;
    U8  anStatus;
    U8  linkStatus;
    U8  currentAnStatus;
    U8  currentLinkStatus;
    U16  eventId;

    // Only the AUTO-NEGOTIATION and LINK bit statuses are of interest.
    // AN is never expected to be set. *** HAS NOT BEEN OBSERVED ***
    // LINK ... is whatever is happening in that part of its universe.
    // If link is up, so is MDIX.
    statusRegister    = readFromSecondarySwitch(STATUS_2_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);
    anStatus          = statusRegister & PORT_STATUS_2_AUTO_NEGOTIATION_MASK;
    linkStatus        = statusRegister & PORT_STATUS_2_LINK_STATUS_MASK;
    currentAnStatus   = secondarySwitchInfoCb[portIndex].status2 & PORT_STATUS_2_AUTO_NEGOTIATION_MASK;
    currentLinkStatus = secondarySwitchInfoCb[portIndex].status2 & PORT_STATUS_2_LINK_STATUS_MASK;

    if (anStatus != currentAnStatus) {
        // ??? What happened? The recovery consists of ... not sure yet,
        // since this has never been observed. Count this as a BAD event.
        incrementBadEventCounter(PORT_STATUS_2_AUTO_NEGOTIATION_FLIP);
    }

    if (linkStatus == currentLinkStatus) {
        // No change in link status on this port.
    } else {
        // A port change in the LINK STATUS register. Is the link status UP or DOWN? Inform the POE subsystem.
        if (linkStatus == PORT_STATUS_2_LINK_STATUS_LINK_UP) { eventId = AEI_STATUS_LINK_UP; secondaryPortLinkStatusChange(portIndex, PORT_IS_UP_HIGH_LEVEL);   }
        else                                                 { eventId = AEI_STATUS_LINK_DN; secondaryPortLinkStatusChange(portIndex, PORT_IS_DOWN_HIGH_LEVEL); }

        setAutoClassConfigFastCheck();

        // Build the JSON element, add it to the autonomous event string.
        // "EVT":{"EI":30,"PORT":"5","STATUS BEFORE":"0x06","STATUS AFTER":"0x00"}     // <- 2 = LINK UP
        // "EVT":{"EI":31,"PORT":"5","STATUS BEFORE":"0x06","STATUS AFTER":"0x00"}     // <- 2 = LINK DOWN
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%02X\",%s:\"0x%02X\"}}",
                                            EVT_STRING, 
                                            EVENT_ID_STRING,      eventId,
                                            PORT_STRING,          pGimmeSecondaryPortString(portIndex),
                                            STATUS_BEFORE_STRING, secondarySwitchInfoCb[portIndex].status2,
                                            STATUS_AFTER_STRING,  statusRegister);
        addJsonToAutonomnousString(pSingleJsonEventString);

#if ENABLE_CRAFT_DEBUG == TRUE
        // Secondary port 5..8: index + 5
        sprintf(&pCraftPortDebugData[0], "ETH SEC PORT %lu: STATUS 0x%04X -> 0x%04X (%lu)\r\n", (portIndex+5), secondarySwitchInfoCb[portIndex].status2, statusRegister, g_ul_ms_ticks);
        UART1_WRITE_PACKET_MACRO
#endif

        secondarySwitchInfoCb[portIndex].status2 = statusRegister;

    }
}


BOOL isCurrentSecondaryLinkStatusUp(U32 portIndex)
{
    BOOL currentStatusUp = FALSE;
    U8   statusRegister;
    U8   linkStatus;
    statusRegister = readFromSecondarySwitch(STATUS_2_READ_SEQ_BY_PORT_INDEX_LOOKUP[portIndex]);
    linkStatus     = statusRegister & PORT_STATUS_2_LINK_STATUS_MASK;
    if (linkStatus == PORT_STATUS_2_LINK_STATUS_LINK_UP) { currentStatusUp = TRUE; }
    return currentStatusUp;
}


#if 0 // NOT READY FOR SECONDARY MIB STUFF


static bool isPrimaryMibIndexValid(U8 someIndex)
{
    BOOL validIndex = FALSE;
    if      ((someIndex >= PRIMARY_SWITCH_MIB_INDEX_MIN)       && (someIndex <= PRIMARY_SWITCH_MIB_INDEX_MAX))       { validIndex = TRUE; } // the set of 30-bit counters
    else if ((someIndex >= PRIMARY_SWITCH_36BIT_MIB_INDEX_MIN) && (someIndex <= PRIMARY_SWITCH_36BIT_MIB_INDEX_MAX)) { validIndex = TRUE; } // the set of 36-bit counters
    return validIndex;
}


// Get a mib counter for this port. Getting a MIB counter requires a multi-step interaction with the controller.
// For each action, print a specific JSON-like string. The JSON-like structure for each action is shown below as
// processing progresses. The overall result that is returned to the caller/user is a single-line string, terminated
// with CR/LF as the delimiter for processing of the results.
//
// If there was an error, an error string component is returned within the overall JSON-like string.
void getPrimarySwitchMibCounter(U16 primaryPortNumber, U8 mibIndex, char *pPortNumberString)
{
    U16  actionNumber = 1;
    U8   writeN500[5];
    U8   readN500[5];
    U8   writeN501[5];
    BOOL readEnableSet = TRUE;
    BOOL okToReadCounterRegisters = FALSE;
    U32  readEnableValue;
    U16  readAttempts = 0;
    U8   writeData;

    U8   readN503[5];
    U8   readN504[5];
    U8   readN505[5];
    U8   readN506[5];
    U8   readN507[5];
    U8   readN503Value;
    U8   readN504Value;
    U8   readN505Value;
    U8   readN506Value;
    U8   readN507Value;

    U16  registerAddressN503;
    U16  registerAddressN504;
    U16  registerAddressN505;
    U16  registerAddressN506;
    U16  registerAddressN507;

    U16  registerAddress;
    char pAddressAsString[10];

    U32  timestampStart;
    U32  timestartEnd;

    uint64_t longLongCounter; // <- what the caller/user desires!
    U8      *pLongLongAsByte;

    char   pTitleString[64];

    timestampStart = g_ul_ms_ticks;

    // { "MIB COUNTER":{"PORT":2,"MIB INDEX":"81",
    //printf("{ %s:{ ",         MIB_COUNTER_STRING);                 // The main title
    //printf("\"%s\":\"%s\", ", PORT_STRING, pPortNumberString);     // add the PORT component lookup
    //printf("%s:\"%02X\", ",   MIB_INDEX_STRING, mibIndex);         // add the MIB INDEX component
    sprintf(pTitleString, "{ %s:{\"%s\":\"%s\",%s:\"%02X\", ", MIB_COUNTER_STRING, PORT_STRING, pPortNumberString,   MIB_INDEX_STRING, mibIndex);

    // First, validate the port and mib index. If invalid, return a JSON-like string with the reason.
    //
    // Then return the following action notifications to the caller:
    //
    // ACTION 1: set the MIB INDEX as a write command in the N501 register
    // ACTION 2: set the MIB Read Enable bit as a write command in the N501 register
    // ACTION 3: check that the MIB Read Enable clears itself. For this, enter a while loop until the bit clears itself
    // ACTION 4: - read the individual bytes of the counter from registers N503/504/505/506/507
    //           - concatenate into a double long
    //           - printf a formated hex and decimal
    // Finally: close off the JSON-like string and go home.

    // First things first: validate port and index.
    if ((primaryPortNumber < PRIMARY_SWITCH_PORT_NUMBER_MIN) || (primaryPortNumber > PRIMARY_SWITCH_PORT_NUMBER_MAX)) {
        // Set the error in the return string, and close off the JSON-like string.
        // Return here and now. Returning in the middle of a function is "not nice",
        // but it does avoid code indentation for processing valid data.
        incrementBadEventCounter(INVALID_PRIMARY_PORT_NUMBER);
        printf("%s%s:\"INVALID PRIMARY PORT NUMBER SPECIFIED\"}}\r\n", pTitleString, ERROR_STRING);
        return;
    } else {
        // And now the index.
        if (!isPrimaryMibIndexValid(mibIndex)) {
            incrementBadEventCounter(INVALID_PRIMARY_MIB_INDEX);
            printf("%s%s:\"INVALID PRIMARY MIB INDEX SPECIFIED\"}}\r\n", pTitleString, ERROR_STRING);
            return;
        }
    }

    // FOR THIS PORT:
    printf("%s", pTitleString);

    // ACTION 1: set the MIB INDEX as a write command in the N501 register:
    //           - build the 5-byte sequence to write the MIB INDEX to the 0xN500 register
    //           - spoon-feed those those 5 bytes to the switch.
    //           - include this info in the response
    // This part of the response should look something like:
    // "ACTION 1":{"COMMAND":"SET MIB INDEX","ADDRESS":"0x5501","BYTE SEQUENCE":"40 0A A0 20 0C" },
    sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_SET_MIB_INDEX_BASE_ADDRESS_N501);
    registerAddress = atoh(pAddressAsString);
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, mibIndex, &writeN501[0]);
    writeToPrimarySwitch(writeN501);
    printf("\"%s %u\":{ %s:%s,%s:\"0x%04X\",%s:\"\%02X %02X %02X %02X %02X\"}, ",
           ACTION_STRING, actionNumber++, COMMAND_STRING, SET_MIB_INDEX_STRING, ADDRESS_STRING, registerAddress, BYTE_SEQUENCE_STRING, writeN501[0], writeN501[1], writeN501[2], writeN501[3], writeN501[4]);

    // ACTION 2: set the MIB Read Enable bit as a write command in the N500 register:
    //           - build the 5-byte sequence to set the MIB Read Enable [25] bit in the 0xN500 register;
    //           - include this info in the response
    //           - spoon-feed those those 5 bytes to the switch.
    // This part of the response should look something like:
    // "ACTION 2":{"COMMAND":"SET MIB READ ENABLE","ADDRESS":"0x5500","BYTE SEQUENCE":"40 0A A0 00 02" }
    sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_SET_MIB_READ_ENABLE_BASE_ADDRESS_N500);
    registerAddress = atoh(pAddressAsString);
    writeData = 0xFF & MIB_READ_ENABLE_COUNT_VALID_BIT_MASK;
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, writeData, &writeN500[0]);
    writeToPrimarySwitch(writeN500);
    printf("\"%s %u\":{%s:%s,%s:\"0x%04X\",%s:\"\%02X %02X %02X %02X %02X\"},",
           ACTION_STRING, actionNumber++, COMMAND_STRING, SET_MIB_READ_ENABLE_STRING, ADDRESS_STRING, registerAddress, BYTE_SEQUENCE_STRING, writeN500[0], writeN500[1], writeN500[2], writeN500[3], writeN500[4]);

    // ACTION 3: check that the MIB Read Enable clears itself.
    //           - build the 5-byte sequence to access the MIB Read Enable [25] bit in the 0xN500 register
    //           - the data bit is donut-care, so 0 is passed to the build function.
    //           - enter into a while loop reading the N500 register:
    //               - if the MIB Read Enable is still set, then wait and read again
    //               - if the MIB Read Enable is cleared, exit the while loop
    //               - the overflow bit [31] of N500 is extracted, and OVERFLOW value is included in the response.  
    // A read limit of 5 is arbitrarily set.
    // This part of the response should look something like:
    // "ACTION 3":{"COMMAND":"CHECK MIB READ ENABLE","ADDRESS":"0x5500","BYTE SEQUENCE":"60 0A A0 00 00","READ ATTEMPTS":1,"COUNTER OVERFLOW":"NO"},
    build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddress, 0, &readN500[0]); // <- RE-USING registerAddress FROM ABOVE!!!
    printf("\"%s %u\":{ %s:%s,%s:\"0x%04X\",%s:\"\%02X %02X %02X %02X %02X\",",
           ACTION_STRING, actionNumber++, COMMAND_STRING, CHECK_MIB_READ_ENABLE_STRING, ADDRESS_STRING, registerAddress, BYTE_SEQUENCE_STRING, readN500[0], readN500[1], readN500[2], readN500[3], readN500[4]);
    while (readEnableSet) {
        readEnableValue = readFromPrimarySwitch(readN500);
        readAttempts++;
        if ((readEnableValue & MIB_READ_ENABLE_COUNT_VALID_BIT_MASK) == 0) {
            // The bit is cleared. Ready to read the registers containing the actual counter value.
            // Don't forget to check the overflow bit.
            printf("%s:%u,", VALUE_STRING, (U8)readEnableValue);
            printf("%s:%u,", READ_ATTEMPTS_STRING, readAttempts);
            if ((readEnableValue & MIB_COUNTER_OVERFLOW_INDICATION_BIT_MASK) == 0) { printf("%s:%s},", COUNTER_OVERFLOW_STRING, NO_STRING);   }
            else                                                                   { printf("%s:%s},", COUNTER_OVERFLOW_STRING, YES_STRING);  }
            readEnableSet = FALSE;
            okToReadCounterRegisters = TRUE;
            break; // just in case ...
        } else {
            // Bit is still set.
            if (readAttempts < 5) {
                // Wait and try again. A 100 msecs seems about right ...
                mdelay(100); // TODO: MAKE THIS A DEFINITION IN apparent.h
            } else {                
                // Limit reached. Something wrong. Tell the caller. okToReadCounterRegisters remains FALSE.
                readEnableSet = FALSE;
                printf("%s:\"N500 READ LIMIT REACHED, CHECK MIB READ ENABLE FAILED TO CLEAR\"", ERROR_STRING);
                break;
            }
        }
    } // while ...

    if (okToReadCounterRegisters) {
        // ACTION 4: get that bugger of a counter:
        //           - build the address set for registers N503/504/505/506/507
        //           - build the 5-byte sequences for these registers
        //           - read register N503, N504, N505, N506, N507
        //           - concatenate
        //           - display the counter as hex
        //           - display the counter as decimal
        //           - close off JSON-like string, print, go home.
        // This part of the response should look something like (and it's a biggie):
        // "ACTION 4":{"COMMAND":"READ N503/504/505/506/507","ADDRESS LIST":[{"ADDRESS":"0x5503","BYTE SEQUENCE":"60 0A A0 60 00","VALUE":"00"},
        //                                                                   {"ADDRESS":"0x5504","BYTE SEQUENCE":"60 0A A0 80 00","VALUE":"00"},
        //                                                                   {"ADDRESS":"0x5505","BYTE SEQUENCE":"60 0A A0 A0 00","VALUE":"0B"},
        //                                                                   {"ADDRESS":"0x5506","BYTE SEQUENCE":"60 0A A0 C0 00","VALUE":"40"},
        //                                                                   {"ADDRESS":"0x5507","BYTE SEQUENCE":"60 0A A0 E0 00","VALUE":"66"}],
        // "CONCATENATED":0x00000B4066,"COUNTER VALUE":737382,"MIB RESULT":"SUCCESS"}}}
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_4_BASE_ADDRESS_N503);   registerAddressN503 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_3_BASE_ADDRESS_N504);   registerAddressN504 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_2_BASE_ADDRESS_N505);   registerAddressN505 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_1_BASE_ADDRESS_N506);   registerAddressN506 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_0_BASE_ADDRESS_N507);   registerAddressN507 = atoh(pAddressAsString);

        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN503, 0, &readN503[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN504, 0, &readN504[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN505, 0, &readN505[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN506, 0, &readN506[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN507, 0, &readN507[0]);
        readN503Value = (U8)readFromPrimarySwitch(readN503);    // most significant byte of the counter
        readN504Value = (U8)readFromPrimarySwitch(readN504);
        readN505Value = (U8)readFromPrimarySwitch(readN505);
        readN506Value = (U8)readFromPrimarySwitch(readN506);
        readN507Value = (U8)readFromPrimarySwitch(readN507);    // least significant byte of the counter

        // Construct the long long counter value.
        //
        // NOTE: all counters are 30-bit counters except for 0x80 and 0x81 which are 36-bit counters.
        //       It so happens that 80/81 are of interest to the SBC/gateway, and will certainly be called for.
        //       All counters are treated uniformly as 36-bit counters, consequently, all counters will be
        //       processed as LONG LONG counters. The upper-most bits of the counter is in the N503 register.
        longLongCounter    = 0x0000000000000000;
        pLongLongAsByte    = (U8 *)&longLongCounter;
        pLongLongAsByte[4] = readN503Value;
        pLongLongAsByte[3] = readN504Value;
        pLongLongAsByte[2] = readN505Value;
        pLongLongAsByte[1] = readN506Value;
        pLongLongAsByte[0] = readN507Value;

        //    printf("{\"STUFF\":{\"1\":\"%02X\",\"2\":\"%02X\",\"3\":\"%02X\",\"4\":\"%02X\",\"5\":\"%02X\",\"long hex\":\"%llX\",\"long decimal\":%llu}}, ",

        // PRETTY MUCH DONE!
        // The last "action" is expected to look something like this:
        //
        // "ACTION 4":{"COMMAND":"READ N503/4/5/6",
        //                "ADDRESS LIST":[{"ADDRESS":"0x1503","BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x00"},
        //                                {"ADDRESS":"0x1504", "BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x12"},
        //                                {"ADDRESS":"0x1505", "BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x34"},
        //                                {"ADDRESS":"0x1506", "BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x56"},
        //                                {"ADDRESS":"0x1507", "BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x78"}],
        //                "CONCATENATED":0x0012345678",}
        //
        // The above will be constructed with individual print statements.
        // It's the final element in the JSON-like string, so it has no trailing comma.

        printf("\"%s %u\":{ %s:%s,", ACTION_STRING, actionNumber++, COMMAND_STRING, READ_N503_4_5_6_7_STRING);
        printf("%s:[", ADDRESS_LIST_STRING);
        printf("{%s:\"0x%04X\", %s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN503, BYTE_SEQUENCE_STRING, readN503[0], readN503[1], readN503[2], readN503[3], readN503[4], VALUE_STRING, readN503Value);
        printf("{%s:\"0x%04X\", %s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN504, BYTE_SEQUENCE_STRING, readN504[0], readN504[1], readN504[2], readN504[3], readN504[4], VALUE_STRING, readN504Value);
        printf("{%s:\"0x%04X\", %s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN505, BYTE_SEQUENCE_STRING, readN505[0], readN505[1], readN505[2], readN505[3], readN505[4], VALUE_STRING, readN505Value);
        printf("{%s:\"0x%04X\", %s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN506, BYTE_SEQUENCE_STRING, readN506[0], readN506[1], readN506[2], readN506[3], readN506[4], VALUE_STRING, readN506Value);
        printf("{%s:\"0x%04X\", %s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"}]},", ADDRESS_STRING, registerAddressN507, BYTE_SEQUENCE_STRING, readN507[0], readN507[1], readN507[2], readN507[3], readN507[4], VALUE_STRING, readN507Value);

        // TOTALLY DONE! Return the actual "32-bit counter" as both hex and decimal, and signal to the caller/user that all went well.
        printf("%s:\"%llX\",",   VALUE_AS_HEX_STRING,     longLongCounter);           // omit the leading 0x please.
        printf("%s:%llu,",       VALUE_AS_DECIMAL_STRING, longLongCounter);
        timestartEnd = g_ul_ms_ticks;
        printf("%s:%lu,",        MIB_READ_ELAPSED_MSECS_STRING, (timestartEnd - timestampStart));
        printf("%s:%s",           MIB_RESULT_STRING,       MIB_RESULT_SUCCESS_STRING); // <- VERY LAST ITEM IN THIS JSON-LIKE STRING, SO NO TRAILING COMMA!!!

    } else {
        // NOT OK ... Hmmmmmm
        printf("%s:%s}", MIB_RESULT_STRING, MIB_RESULT_FAILED_STRING); // <- VERY LAST ITEM IN THIS JSON-LIKE STRING, SO NO TRAILING COMMA!!!
    }

    // Done. For the benefit and amusement of the caller/user: close off the JSOM-like string and print.
    printf("}}\r\n");

    return;

}


// TODO: MERGE THE FOLLOWING 2 FUNCTIONS INTO 1 WITH PARAMETER INDICATING ENABLE/DISABLE. PLEASE.
void enableFlushFreezeByPortKey(U16 primaryPortNumber, char *pPortNumberString)
{
    U8   writeN500[5];
    U8   registerValue;
    U16  registerAddress;
    char pAddressAsString[10];

    sprintf(pAddressAsString, "%u500", primaryPortNumber);
    registerAddress = atoh(pAddressAsString);
    registerValue = MIB_FLUSH_AND_FREEZE_ENABLE_BIT_MASK;
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, registerValue, &writeN500[0]);
    writeToPrimarySwitch(writeN500);

    // {"port 1":"DONE","ADDRESS":"0x1500"}
    printf("{\"%s %s\":%s,%s:\"0x%s\"}", PORT_STRING, pPortNumberString, EXECUTION_DONE_STRING,  ADDRESS_STRING, pAddressAsString);
}


void disableFlushFreezeByPortKey(U16 primaryPortNumber, char *pPortNumberString)
{
    U8   writeN500[5];
    U8   registerValue;
    U16  registerAddress;
    char pAddressAsString[10];

    sprintf(pAddressAsString, "%u500", primaryPortNumber);
    registerAddress = atoh(pAddressAsString);
    registerValue = 0; // clear the bit.
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, registerValue, &writeN500[0]);
    writeToPrimarySwitch(writeN500);

    // { "port 1":"DONE", "ADDRESS":"0x1500"}
    printf("{\"%s %s\":%s,%s:\"0x%s\"}", PORT_STRING, pPortNumberString, EXECUTION_DONE_STRING,  ADDRESS_STRING, pAddressAsString);
}


void flushPrimarySwitchMibCounters(void)
{
    U8   write0336[5];

    // Set the upper-most bit [7] of address 0x0336 of the primary switch.
    // The bit is self-clearing.
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, SWITCH_MIB_CONTROL_REGISTER_ADDRESS, MIB_CONTROL_FLUSH_MIB_COUNTERS_MASK, &write0336[0]);
    writeToPrimarySwitch(write0336);
}


void freezePrimarySwitchMibCounters(void)
{
    U8   write0336[5];

    // Set bit [6] of address 0x0336 of the primary switch.
    // This bit is NOT self clearing.
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, SWITCH_MIB_CONTROL_REGISTER_ADDRESS, MIB_CONTROL_FREEZE_MIB_COUNTERS_MASK, &write0336[0]);
    writeToPrimarySwitch(write0336);
}


void unfreezePrimarySwitchMibCounters(void)
{
    U8   write0336[5];

    // Clear bit [6] of address 0x0336 of the primary switch.
    // This returns the controller's MIB operation back to "normal counter operation".
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, SWITCH_MIB_CONTROL_REGISTER_ADDRESS, 0, &write0336[0]);
    writeToPrimarySwitch(write0336);
}



#endif // NOT READY FOR SECONDARY MIB STUFF


// Build a 24-bit/3-byte data sequence to interact with the SECONDARY KSZ8795 controller.
//
//                 |<-----                   24 bits total  = 3 bytes                  ----->|
//                 |     |<---     12-bit address     --->| don't care |<--- 8-bit DATA  --->|
// Register Read:  011   11 10 9  8  7  6  5  4  3  2  1  0   0        D7 D6 D5 D4 D3 D2 D1 D0
// (write:         010   ...)
// All addresses are a 8-bit/1 byte entities The 24-bit/3-byte train ride looks like:
//
//            |<-- 12-bit address -->| don't care |<---    DATA     --->|
//      CCC    0 0 0 0 A A A A A A A A     0      D7 D6 D5 D4 D3 D2 D1 D0
// 
// Splitting up the 24-bits into the 3-byte set:
//
// C C C 0 0 0 0 A A A A A A A A 0 D D D D D D D
// byte [0]     byte [1]     byte [2]
// CCC0 000A    AAAA AAA0    DDDD DDDD
// CCC0 000A    AAAA AAA0    DDDD DDDD
//
// or:
//
// 0110000A     AAAA AAA0    DDDD DDDD   <- read command
// 0100000A     AAAA AAA0    DDDD DDDD   <- write command
//
// The 8-bit register address is spread out among the upper 2 bytes.
// Perform the appropriate shifting and masking.
static void build3ByteCommandSequence(U8 command, U8 address, U8 data, U8 *pBytes)
{
    pBytes[0]   = (U8)(command << SECONDARY_COMMAND_FIELD_BIT_LEFT_SHIFT) | (address >> SECONDARY_ADDRESS_PART_0_RIGHT_SHIFT);
    pBytes[1]   = (U8)(address << SECONDARY_ADDRESS_PART_1_LEFT_SHIFT);
    pBytes[2]   = data;
    return;
}


// ALU:


// getSecPortInfoFromDynamicSourcePort
//
//
//
static void getSecPortInfoFromDynamicSourcePort(U8 sourcePort, U8 *pKszPort, char **pDownstreamPort)
{
    // Source port:
    if      (sourcePort == ALU_DYNAMIC_REG_113_71_SOURCE_PORT_1_VALUE)     { *pKszPort = 1;    *pDownstreamPort = (char *)&PORT_5_STRING[0];       }
    else if (sourcePort == ALU_DYNAMIC_REG_113_71_SOURCE_PORT_2_VALUE)     { *pKszPort = 2;    *pDownstreamPort = (char *)&PORT_6_STRING[0];       }
    else if (sourcePort == ALU_DYNAMIC_REG_113_71_SOURCE_PORT_3_VALUE)     { *pKszPort = 3;    *pDownstreamPort = (char *)&PORT_7_STRING[0];       }
    else if (sourcePort == ALU_DYNAMIC_REG_113_71_SOURCE_PORT_4_VALUE)     { *pKszPort = 4;    *pDownstreamPort = (char *)&PORT_8_STRING[0];       }
    else if (sourcePort == ALU_DYNAMIC_REG_113_71_SOURCE_PORT_RGMII_VALUE) { *pKszPort = 5;    *pDownstreamPort = (char *)&PORT_RGMII_STRING[0];   }
    else                                                                   { *pKszPort = 0xFF; *pDownstreamPort = (char *)&PORT_UNKNOWN_STRING[0]; }
}


// userReadAluSecondaryDynamicByIndex
//
// Command takes the form:
//
// read_alu_sec_dynamic_by_index index=<0..1023>
//
// Output as json, of the form:
//
// {"CLI":{"COMMAND":"read_alu_sec_dynamic_by_index","ARGUMENTS":"index=2","RESULT":{"RAW REG":"11 22 33 44 55 66 77 88 99","VALID DATA":0,"NUMBER OF ENTRIES":2,
//                                                                                   "TIMESTAMP":2,"KSZ PORT":1,"DOWNSTREAM PORT":"1","DATA READY":0,"FILTER ID":0,
//                                                                                   "ELAPSED":123,"MAC":"11:22:33:44:55:66"}}}
//
void userReadAluSecondaryDynamicByIndex(char *pUserInput)
{
    char  *pArguments;
    char   pIndexString[32];
    U16    index;
    U8     pDynamicAluData[9];

    pArguments = &pUserInput[strlen(READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD) + 1];
    memset(pIndexString, 0, 32);
    findComponentStringData(CLI_ARG_INDEX_STRING, pIndexString, pArguments);

    if (strlen(pIndexString) == 0) {
        printf("{%s:{%s:\"%s\",%s:%s,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,  pUserInput,                RESULT_STRING, FAIL_STRING,
                                                                     RESPONSE_STRING, INVALID_PARAMETERS_STRING, INFO_STRING,   NONE_STRING);
    } else {
        index = atoh(pIndexString);
        if (index <= MAX_SECONDARY_ALU_INDEX) {

            readAluSecondaryDynamicByIndex(index, &pDynamicAluData[0]);

            // {"CLI":{"COMMAND":"read_alu_sec_by_index","ARGUMENTS":"index=2","CODE":0,"RESULT":"SUCCES","RESPONSE":{"INFO":"00 14 00 00 07 32 84 BB 66"}}}
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:\"%02X %02X %02X %02X %02X %02X %02X %02X %02X\"}}}\r\n",
                   CLI_STRING, COMMAND_STRING,    READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD,
                               ARGUMENTS_STRING,  pArguments,
                               CODE_STRING,       0,
                               RESULT_STRING,     SUCCESS_STRING,
                               RESPONSE_STRING,   INFO_STRING, pDynamicAluData[0], pDynamicAluData[1], pDynamicAluData[2],
                                                               pDynamicAluData[3], pDynamicAluData[4], pDynamicAluData[5],
                                                               pDynamicAluData[6], pDynamicAluData[7], pDynamicAluData[8]);

        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s:{%s:%s}}}\r\n",
                   CLI_STRING, COMMAND_STRING,  pUserInput,
                               ARGUMENTS_STRING, pArguments,
                               CODE_STRING,      0,
                               RESULT_STRING,    FAIL_STRING,
                               RESPONSE_STRING,  REASON_STRING, ALU_INDEX_OUT_OF_RANGE_STRING);
        }
    }
}


// readAluSecondaryRange
//
//    *** FIX OUT, BUT ONLY IF IT CAN BE MADE TO WORK
//
// Output is of the form:
//
// {"CLI":{"COMMAND":"read_alu_sec_range","ARGUMENTS":"start=<0..1023> end=<1..1023>","RESULT":[
//            {"0":{"ELAPSED":123,"INFO":"11 12 13 14 15 16 17 18 19"}},
//            {"1":{"ELAPSED":123,"INFO":"21 22 23 24 25 26 27 28 29"}},
//            .
//            .
//            .
//            {"10":{"ELAPSED":123,"INFO":"01 02 03 04 05 06 07 08 09"}}]}}
//
//
void readAluSecondaryRange(char *pUserInput)     // DYNAMIC
{
    char  *pArguments;
    char   pStartIndexString[32];
    char   pEndIndexString[32];
    U16    iggy;
    U16    startIndex;
    U16    endIndex;
    U32    startTick;
    U32    elapsed;
    BOOL   firstTime = TRUE;
    U8     pAluData[9];

    pArguments = &pUserInput[strlen(READ_ALU_SEC_RANGE_CMD) + 1];
    memset(pStartIndexString, 0, 32);
    memset(pEndIndexString,   0, 32);

    findComponentStringData(CLI_ARG_START_STRING, pStartIndexString, pArguments);
    findComponentStringData(CLI_ARG_END_STRING,   pEndIndexString,   pArguments);

    if ((strlen(pStartIndexString) == 0) || (strlen(pEndIndexString) == 0)) {
        // TODO: RETURN JSON-FORMATED REJECTION
        printf("missing 1 or 2 mandatory args\r\n");
    } else {
        startIndex = atoi(pStartIndexString);
        endIndex   = atoi(pEndIndexString);

        // {"CLI":{"COMMAND":"read_alu_sec_range","ARGUMENTS":"start=<0..1023> end=<1..1023>","RESULT":[
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:[", CLI_STRING, COMMAND_STRING, READ_ALU_SEC_RANGE_CMD, ARGUMENTS_STRING, pArguments, RESULT_STRING);

        startTick = g_ul_ms_ticks;
        for (iggy = startIndex; iggy <= endIndex; iggy++) {

            startTick = g_ul_ms_ticks;
            readAluSecondaryDynamicByIndex(iggy, &pAluData[0]);
            elapsed = g_ul_ms_ticks - startTick;

            // Print the result.
            // {"0":{"ELAPSED":123,"INFO":"11 12 13 14 15 16 17 18 19"}},
            if (firstTime) { firstTime = FALSE; }
            else           { printf(",");       } // subsequent entries: comma list separator

            printf("{\"%u\":{%s:%lu,%s:\"%02X %02X %02X %02X %02X %02X %02X %02X %02X\"}}", iggy, ELAPSED_STRING, elapsed,
                                                                                                   INFO_STRING,    pAluData[0], pAluData[1], pAluData[2],
                                                                                                                   pAluData[3], pAluData[4], pAluData[5],
                                                                                                                   pAluData[6], pAluData[7], pAluData[8]);
        } // for ...
        printf("]}}\r\n");
    }
}


// readAluSecondaryDynamicByIndex
//
// Read the ALU by index. The ALU control registers 0/1 are written with the index to initiate
// the read process. The registers are read, accounting for the DATA READY bit clearing. When
// the DATA READY bit is cleared, the 9 bytes of data are written to the user's memory area.
// It is the responsibility of the user to parse and interpret the data in those 9 bytes.
//
// This function follows the example as per the KSZ8795CLX data sheet (page 85).
void readAluSecondaryDynamicByIndex(U32 aluIndex, U8 *pData)
{
    U16    someIndex;
    U8     control_110_6E;
    U8     control_111_6F;
    U8     register114;
    BOOL   keepChecking;
    U32    loopCount = 0;
    U8     index_110_6E;
    U8     index_111_6F;

    if (aluIndex <= MAX_SECONDARY_ALU_INDEX) {
        // The control 0 register will contain the upper 8 bits of the index,
        // and control 1 register will contain the lower 8 bits.
        someIndex      = (U16) aluIndex;
        index_110_6E   = (someIndex >> 8) & ALU_CONTROL_110_6E_INDIRECT_ADDRESSING_MASK; // upper 2 bits only in 11/6E
        index_111_6F   = someIndex & ALU_CONTROL_111_6F_INDIRECT_ADDRESSING_MASK;        // lower 8 bits in 111/6F
        control_110_6E = ALU_CONTROL_110_6E_FUNCTION_INDIRECT | ALU_CONTROL_110_6E_ACTION_READ | ALU_CONTROL_110_6E_TABLE_SELECT_DYNAMIC | index_110_6E;
        control_111_6F = index_111_6F;
        writeToSecondaryByAddress(ALU_CONTROL_110_6E_REGISTER_ADDRESS, control_110_6E);                                // 0x6E
        writeToSecondaryByAddress(ALU_CONTROL_111_6F_REGISTER_ADDRESS, control_111_6F);                                // 0x6F

        pData[0] = readFromSecondarySwitchByAddress(ALU_DATA_112_70_REGISTER_ADDRESS);                                 // 0x70
        pData[1] = readFromSecondarySwitchByAddress(ALU_DATA_113_71_REGISTER_ADDRESS);                                 // 0x71

        // register 114 contains the data ready bit. Read until ready is detected.
        keepChecking = TRUE;
        while (keepChecking) {
            register114 = readFromSecondarySwitchByAddress(ALU_DATA_114_72_REGISTER_ADDRESS);                          // 0x72
            if ((register114 & ALU_DYNAMIC_REG_114_72_DATA_READY_MASK) == ALU_DYNAMIC_REG_114_72_DATA_IS_READY) {
                keepChecking = FALSE;
                // Read the remaining 6 registers and write them to the caller's memory.
                pData[2] = register114;
                pData[3] = readFromSecondarySwitchByAddress(ALU_DATA_115_73_REGISTER_ADDRESS);                         // 0x73
                pData[4] = readFromSecondarySwitchByAddress(ALU_DATA_116_74_REGISTER_ADDRESS);                         // 0x74
                pData[5] = readFromSecondarySwitchByAddress(ALU_DATA_117_75_REGISTER_ADDRESS);                         // 0x75
                pData[6] = readFromSecondarySwitchByAddress(ALU_DATA_118_76_REGISTER_ADDRESS);                         // 0x76
                pData[7] = readFromSecondarySwitchByAddress(ALU_DATA_119_77_REGISTER_ADDRESS);                         // 0x77
                pData[8] = readFromSecondarySwitchByAddress(ALU_DATA_120_78_REGISTER_ADDRESS);                         // 0x78
                break;
            }

            // Bail if coming here too often ...
            if (loopCount++ > MAX_READ_LOOP_LIMIT) { incrementBadEventCounter(SEC_ALU_READ_READY_LOOP_LIMIT); break; }

            // Keep reading.
            mdelay(20); // Sufficient? Necessary?
            pokeWatchdog();

        } // while ...

    } else {
        // The index from the user is validated before calling this function; an invalid index results
        // in a reply indicating the error. So coming here is not expected. Otherwise: nothing to do.
    }            
}



// isConfigSecondaryStaticAluMac
//
// This is something of a hard-coded function: configure the given MAC into the STATIC ALU table
//                                             in entry [0] against the RGMII port.
//
//
BOOL isConfigSecondaryStaticAluMac(U8 *pMac)
{
    U8   registerValue;
    U8   index_110_6E;
    U8   index_111_6F;

    // DATA WRITE preparation: write the indicate MAC, set against the RGMII port.
    // Note: register 112/0x70 is not written.
    // 0x71 = 0
    // 0x72 -> port forward = RGMII = "port 5" value = 0b10000 = 0x10   ALSO: set bit [53] so 0x72 is written with: 0 0 1 1 0 0 0 0 = 0x30
    // 0x73..0x78 = MAC[0..5]
    registerValue =   ALU_STATIC_WRITE_REG_114_72_USE_FID_DEFAULT
                    | ALU_STATIC_WRITE_REG_114_72_OVERRIDE_DEFAULT
                    | ALU_STATIC_WRITE_REG_114_72_VALID_DEFAULT
                    | ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORT_RGMII;
    writeToSecondaryByAddress(ALU_DATA_113_71_REGISTER_ADDRESS, ALU_STATIC_WRITE_REG_113_71_FID_DEFAULT);
    writeToSecondaryByAddress(ALU_DATA_114_72_REGISTER_ADDRESS, registerValue);
    writeToSecondaryByAddress(ALU_DATA_115_73_REGISTER_ADDRESS, pMac[0]);
    writeToSecondaryByAddress(ALU_DATA_116_74_REGISTER_ADDRESS, pMac[1]);
    writeToSecondaryByAddress(ALU_DATA_117_75_REGISTER_ADDRESS, pMac[2]);
    writeToSecondaryByAddress(ALU_DATA_118_76_REGISTER_ADDRESS, pMac[3]);
    writeToSecondaryByAddress(ALU_DATA_119_77_REGISTER_ADDRESS, pMac[4]);
    writeToSecondaryByAddress(ALU_DATA_120_78_REGISTER_ADDRESS, pMac[5]);

    // Write the control to trigger the operation:
    // From page 64/65. This MAC is written to the STATIC table entry [0] so:
    // - the upper 2 bits of INDIRECT_ADDRESSING = 0 in the lower 2 bits of register 11/0x6E
    // - the lower 8 bits of INDIRECT_ADDRESSING = 0 in register 111/0x6F
    index_110_6E = ALU_CONTROL_110_6E_FUNCTION_INDIRECT | ALU_CONTROL_110_6E_ACTION_WRITE | ALU_CONTROL_110_6E_TABLE_SELECT_STATIC;
    index_111_6F = 0x00;
    writeToSecondaryByAddress(ALU_CONTROL_110_6E_REGISTER_ADDRESS, index_110_6E);                            // 0x6E
    writeToSecondaryByAddress(ALU_CONTROL_111_6F_REGISTER_ADDRESS, index_111_6F);                            // 0x6F

    return TRUE;
}


// isReadSecondaryStaticInfo
//
//
// NOT 100% ready for prime time - 
//
BOOL isReadSecondaryStaticInfo(U16 someIndex, U8 *pPortForward, U8 *pMac)
{
    U8   registerValue;
    U8   index_110_6E;
    U8   index_111_6F;
    U32  loopCount  = 0;
    BOOL keepChecking = TRUE;
    BOOL readResult   = TRUE;

    // From page 64/65: - the upper 2 bits of the index will be written to the lower 2 bits of 110/0x6E
    //                  - the remaining 8 bits of the index are written to 111/0x6F:
    registerValue  = (U8)((someIndex >> 8) & ALU_CONTROL_110_6E_INDIRECT_ADDRESSING_MASK);
    index_110_6E   = ALU_CONTROL_110_6E_FUNCTION_INDIRECT | ALU_CONTROL_110_6E_ACTION_READ | ALU_CONTROL_110_6E_TABLE_SELECT_STATIC | registerValue;
    index_111_6F   = (U8)(someIndex & ALU_CONTROL_111_6F_INDIRECT_ADDRESSING_MASK);
    writeToSecondaryByAddress(ALU_CONTROL_110_6E_REGISTER_ADDRESS, index_110_6E);                            // 0x6E
    writeToSecondaryByAddress(ALU_CONTROL_111_6F_REGISTER_ADDRESS, index_111_6F);                            // 0x6F

    // For read: - 112/0x70: nothing is defined, so is not read
    //           - 113/0x71: contains FID info, not used by the Apparent application, so not read
    //           - 114/0x72: check the VALID bit, if yes, retrieve the FORWARDING PORT
    //           - 115/0x73..120/0x78 contain the 6 bytes of the MAC
    //pData[0] = readFromSecondarySwitchByAddress(ALU_DATA_112_70_REGISTER_ADDRESS);                         // 0x70  
    //pData[1] = readFromSecondarySwitchByAddress(ALU_DATA_113_71_REGISTER_ADDRESS);                         // 0x71

    while (keepChecking) {
        registerValue = readFromSecondarySwitchByAddress(ALU_DATA_114_72_REGISTER_ADDRESS);                  // 0x72

        if ((registerValue & ALU_STATIC_READ_REG_114_72_VALID_MASK) == ALU_STATIC_READ_REG_114_72_VALID_YES) {

            // Read the remaining 6 registers and write them to the caller's memory.
            *pPortForward = registerValue & ALU_STATIC_WRITE_REG_114_72_FORWARDING_PORTS_MASK;
            pMac[0]       = readFromSecondarySwitchByAddress(ALU_DATA_115_73_REGISTER_ADDRESS);              // 0x73
            pMac[1]       = readFromSecondarySwitchByAddress(ALU_DATA_116_74_REGISTER_ADDRESS);              // 0x74
            pMac[2]       = readFromSecondarySwitchByAddress(ALU_DATA_117_75_REGISTER_ADDRESS);              // 0x75
            pMac[3]       = readFromSecondarySwitchByAddress(ALU_DATA_118_76_REGISTER_ADDRESS);              // 0x76
            pMac[4]       = readFromSecondarySwitchByAddress(ALU_DATA_119_77_REGISTER_ADDRESS);              // 0x77
            pMac[5]       = readFromSecondarySwitchByAddress(ALU_DATA_120_78_REGISTER_ADDRESS);              // 0x78
            keepChecking  = FALSE;
            break;

        }

        // Bail if coming here too often ...
        if (loopCount++ > MAX_READ_LOOP_LIMIT) { incrementBadEventCounter(SEC_ALU_READ_READY_LOOP_LIMIT); break; }

        // Keep reading.
        mdelay(20); // Sufficient? Necessary?
        pokeWatchdog();

    } // while ...

    return readResult;
}


// processSecondaryAluDynamicRawData
//
// Extract port forward and valid count from the secondary ALU row of data that was read out.
//
// - valid count: is in bits [70..61] in registers 0x72 and 0x73 (112 and 13) = pData[0,1]
// - port forward: (or "source port" in the data sheet) is in bits [2..0] in register 0x72/114 = pData[2]
//
void processSecondaryAluDynamicRawData(U8 *pData,  U8 *pSourcePort, U16 *pValidCount)
{
    // Valid count: check bit[71] in register 0x70/112:
    if ((pData[0] & ALU_DYNAMIC_REG_112_70_VALID_ENTRIES_MASK) == ALU_DYNAMIC_REG_112_70_VALID_ENTRIES_NONE_VALUE) {
        // This bit states there are no valid entries in the secondary controller's ALU.
        *pValidCount = 0;
    } else {
        // 1 or more valid entries.
        *pValidCount =   ( (pData[0] & ALU_DYNAMIC_REG_112_70_NUMBER_OF_ENTRIES_MASK) << ALU_DYNAMIC_REG_112_70_NUMBER_BITS_SHIFT_LEFT  )
                       | ( (pData[1] & ALU_DYNAMIC_REG_113_71_NUMBER_OF_ENTRIES_MASK) >> ALU_DYNAMIC_REG_113_71_NUMBER_BITS_SHIFT_RIGHT );

    }

    // Port forward: check bits[2..0] in register 0x71/113:
    *pSourcePort = pData[1] & ALU_DYNAMIC_REG_113_71_SOURCE_PORT_MASK;
}


// readOneSecondaryDynamicAluEntry
//
//
//
//
void readOneSecondaryDynamicAluEntry(U16 index, t_aluReadDataResults *pAluReadData)
{
    U8     control_110_6E;
    U8     control_111_6F;
    BOOL   keepChecking;
    U8     index_110_6E;
    U8     index_111_6F;
    U32    loopCount = 0;

    // The index passed here is a 16-bit entity, but only 10 bits are used (so a max of 1024 entries):
    // - register 0x6E holds the upper 2-bits of index
    // - register 0x6F holds the lower 8 bits.
    index_110_6E   = (index >> 8) & ALU_CONTROL_110_6E_INDIRECT_ADDRESSING_MASK; // upper 2 bits only in 11/6E
    index_111_6F   = index & ALU_CONTROL_111_6F_INDIRECT_ADDRESSING_MASK;                                               // lower 8 bits in 111/6F
    control_110_6E = ALU_CONTROL_110_6E_FUNCTION_INDIRECT | ALU_CONTROL_110_6E_ACTION_READ | ALU_CONTROL_110_6E_TABLE_SELECT_DYNAMIC | index_110_6E;
    control_111_6F = index_111_6F;
    writeToSecondaryByAddress(ALU_CONTROL_110_6E_REGISTER_ADDRESS, control_110_6E);                                    // 0x6E
    writeToSecondaryByAddress(ALU_CONTROL_111_6F_REGISTER_ADDRESS, control_111_6F);                                    // 0x6F

    pAluReadData->pData[0] = readFromSecondarySwitchByAddress(ALU_DATA_112_70_REGISTER_ADDRESS);                       // 0x70
    pAluReadData->pData[1] = readFromSecondarySwitchByAddress(ALU_DATA_113_71_REGISTER_ADDRESS);                       // 0x71

    // register 114/0x72 contains the data ready bit. Read until read is detected.
    keepChecking = TRUE;
    while (keepChecking) {
        pAluReadData->pData[2] = readFromSecondarySwitchByAddress(ALU_DATA_114_72_REGISTER_ADDRESS);                   // 0x72

        if ((pAluReadData->pData[2] & ALU_DYNAMIC_REG_114_72_DATA_READY_MASK) == ALU_DYNAMIC_REG_114_72_DATA_IS_READY) {
            keepChecking = FALSE;
            // Read the remaining 6 register.
            pAluReadData->pData[3] = readFromSecondarySwitchByAddress(ALU_DATA_115_73_REGISTER_ADDRESS);               // 0x73
            pAluReadData->pData[4] = readFromSecondarySwitchByAddress(ALU_DATA_116_74_REGISTER_ADDRESS);               // 0x74
            pAluReadData->pData[5] = readFromSecondarySwitchByAddress(ALU_DATA_117_75_REGISTER_ADDRESS);               // 0x75
            pAluReadData->pData[6] = readFromSecondarySwitchByAddress(ALU_DATA_118_76_REGISTER_ADDRESS);               // 0x76
            pAluReadData->pData[7] = readFromSecondarySwitchByAddress(ALU_DATA_119_77_REGISTER_ADDRESS);               // 0x77
            pAluReadData->pData[8] = readFromSecondarySwitchByAddress(ALU_DATA_120_78_REGISTER_ADDRESS);               // 0x78

#if ENABLE_ALU_MAC_DEBUG == TRUE
            sprintf(&pCraftPortDebugData[0], "R1ALU[%u]: %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n",
                    index, pAluReadData->pData[0], pAluReadData->pData[1], pAluReadData->pData[2],
                           pAluReadData->pData[3], pAluReadData->pData[4], pAluReadData->pData[5],
                           pAluReadData->pData[6], pAluReadData->pData[7], pAluReadData->pData[8]);
            UART1_WRITE_PACKET_MACRO
#endif

            break;
        }

        // Bail if coming here too often ...
        if (loopCount++ > MAX_READ_LOOP_LIMIT) { incrementBadEventCounter(SEC_ALU_READ_READY_LOOP_LIMIT); break; }

        // Keep reading.
        mdelay(20); // Sufficient? Necessary?
        pokeWatchdog();

    } // while ...

    // Setup the data based on the extracted data on behalf of the caller:
    pAluReadData->timestamp     = (pAluReadData->pData[1] & ALU_DYNAMIC_REG_113_71_AGING_TIMESTAMP_MASK) >> ALU_DYNAMIC_REG_113_71_AGING_BITS_SHIFT_RIGHT;;
    pAluReadData->sourcePort    =  pAluReadData->pData[1] & ALU_DYNAMIC_REG_113_71_SOURCE_PORT_MASK;

    getSecPortInfoFromDynamicSourcePort(pAluReadData->sourcePort, &pAluReadData->kszPort, &pAluReadData->pDownstreamPort);

    if ((pAluReadData->pData[2] & ALU_DYNAMIC_REG_114_72_DATA_READY_MASK) == ALU_DYNAMIC_REG_114_72_DATA_IS_READY) { pAluReadData->dataReady = TRUE;  }
    else                                                                                                           { pAluReadData->dataReady = FALSE; }
    pAluReadData->filterId      = pAluReadData->pData[2] & ALU_DYNAMIC_REG_114_72_FILTER_ID_MASK;
    pAluReadData->numberEntries =   ( (pAluReadData->pData[0] & ALU_DYNAMIC_REG_112_70_NUMBER_OF_ENTRIES_MASK)  << ALU_DYNAMIC_REG_112_70_NUMBER_BITS_SHIFT_LEFT)
                                  | ( (pAluReadData->pData[1] & ALU_DYNAMIC_REG_113_71_NUMBER_OF_ENTRIES_MASK) >> ALU_DYNAMIC_REG_113_71_NUMBER_BITS_SHIFT_RIGHT);
}


// getSecondaryPortStringFromStatic
//
// Based on the port information from the STATIC ALU table, evaluate the
// pointer to the string for that port ("1", "2, "4", "4" or "RGMII").
//
char *getSecondaryPortStringFromStatic(U8 forwardingPort)
{
    char *pPortString;

    if      (forwardingPort == ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_1)     { pPortString = (char *) PORT_1_STRING;       }
    else if (forwardingPort == ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_2)     { pPortString = (char *) PORT_2_STRING;       }
    else if (forwardingPort == ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_3)     { pPortString = (char *) PORT_3_STRING;       }
    else if (forwardingPort == ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_4)     { pPortString = (char *) PORT_4_STRING;       }
    else if (forwardingPort == ALU_STATIC_READ_REG_114_72_FORWARDING_PORT_RGMII) { pPortString = (char *) PORT_RGMII_STRING;   }
    else                                                                         { pPortString = (char *) PORT_UNKNOWN_STRING; }

    return pPortString;
}




/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
