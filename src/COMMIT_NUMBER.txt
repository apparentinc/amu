COMMIT NUMBER

Not sure if this file will continue to exist. Intent of this file: assign a single number to a
commit to master. As SVN provides a single check-in number upon commit to its repo, it is hoped
that the same can be achieved with git. Not knowing how this can be accomplished with git, this
file has been defined. Purpose of a single commit number to the repo: make it easy to reference
a particular build by number for field issues.

BUILD# INITIAL/1-LINE NOTE

R96  EP - re-sync with amu_common submodule which now contains the RGB LED subsystem (common with the BOOTLOADR project).
R95  EP - updates to flash erase, user signature programing, fix output of the help menu
R94  EP - I forgot to make release notes.
R93  EP - include static LAN MAC info in hello response; CLI get_static_lan_mac_info command
R92  EP - Corrections and cleanup accesssing the dynamic and static ALU tables on the secondary ethernet controller.
R91  EP - CLI test/debug commands related to dynamic MAC configuration
R90  EP - MAC resolution issue: some MACs fail to resolve
R89  EP - POE auto class fast check after port status UP/DOWN detection
R88  EP - ALU search-by-index feature
R87  EP - manage the set of 4 POE "event" registers and the POE supply/fault register
R86  EP - include the SAME70 reset reason in the response to the "program" command
R85  EP - fix ARGUMENTS component when returning response to poe_only_port_power_up/down
R84  EP - large upgrade packets; fix string corruption due to strstr(xxx) C library function; miscellaneous efficiency improvements
R83  EP - fix ethernet port checking bug; report poe port power to 1 decimal place
R82  EP - improved autonomous operation and general cleanup; temperature reading when below 0C; watchdog at -20C temperature
R81  EP - temperatures not reporting negative when below 0 degrees C
R80  EP - POE controller checking boolean default on startup is TRUE
R79  EP - output of "program" modified; handling of upgrade END or COMMIT re-send
R78  EP - input contact closure checking is now a state machine on a per-input CC basis
R77  EP - 1st commit to the git amu repo importing the git amu_common submodule
R76  EP - eliminate remaining potential AMU bricking with removal of flash-related commands
R75: EP - added the flashinator module from the bootloader project for bootloader upgrade
R74: EP - improved port checking; watchdog enabled
R73: EP - revamp counters, generate a generic bad-things-happened autonomous event
R72: EP - reduce ethernet port check state machine idle timer from 5 to 3 seconds
R71: EP - remove CLI that enables/disables ethernet service, remove a few counters no longer used
R70: EP - 2 forms of keep-alive: "hello" (returns config and running data), and "hello terse" (returns uptime and temperature only)
R69: EP - added CLI to facilitate peripheral device verification by a crafts person directly from the CLI command line
R68: EP - support for frequent link UP/DOWN condition
R67: EP - refactor User Signature; HW revision from PCBA jumpers
R66: EP - function portPowerUpDown(xxx): fix CLI output (invalid json list), and use corrected "poe port index".
R65: EP - CLI output of the flash User Signature commands are now json-formatted
R64: EP - output of the command "program" has a few fields in the wrong order
R63: EP - minor changes in CLI to be more consistent with SBC/Gateway and the BOOTLOADER w.r.t. firmware upgrade
R62: EP - feature: "link down with POE power" detection and conditional application of the work-around
R61: EP - feature: "mac resolution" based on 2 new CLI commands: "resolve_mac_list" and "get_mac_resolution"
R60: EP - feature: "detect LINK DOWN with POE power" and report as POE_PORT_LINK_DOWN_WITH_POWER autonomous event
R59: EP - cleanup of the "read MAC tables" w.r.t. KSZ ethernet controllers; enable CHECKSUM on input CLI; removed all "login" notion.
R58: EP - remove "-- controller port checking: ENABLED" from the banner output
R57: EP - POE auto-class: class status, class config, PAC (Peak Average Calculation), power on per-port basis; CLI to test excess powe; rother minor improvements
R56: EP - new i2c.c/.h modules for all TWISH/I2C reads and writes; modified I2C read as per TI TPS23882 datasheet
R55: EP - per-port voltage/current readings/power calculation, POE TPS device input voltage reading
R54: EP - fix output of the cc_status command 
R53: EP - timing delay accessing both KSZ controllers adjusted, unnecessary delays removed 
R52: EP - some test code to access the MAC Address Lookup tables (ALU).
R51: EP - clean USB interface on startup; RGB LED CLI to flash brightly; isDigit(xxx) facility; fixed response to CLI with invalid parameters.
R50: EP - application loaded at new flash address; hello is a single json-like object; jump-to-boot for firmware application upgrade; RGB RED LED activity status

-------------------------------

R28: EP - bug fix: last release caused checksum to be invalid
R27: EP - 2 new cli: poe_info and poe_detail_info; hello response has user-friendly data in the POE component
R26: EP - TI POE power fixes: manage OPERATING MODE (0x12) and POWER ENABLE (0x19) registers; include POE temperature reading in HELLO response
R25: EP - TI POE controller cleanup/fixes, including Config B enabled for 16-bit/1-word read/write over the I2C interface
R24: EP - initilization of the I2C/TWIHS1 interface to the TI POE controller *** BUT CURRENTLY NOT WORKING *** 
R23: EP - port "enable" name change to "port power" (UP/DOWN/CYCLE); improved output of  port_power_up/down/cycle CLI commands; minor cleanup
R22: EP - module 3 errata added; improved comments in apparent.h; extra mdelay() for some primary and secondary reads and writes
R21: EP - temperature reading and fan speed control based on low/max temperature settings
R20: EP - extract temperature reading from sensor over TWIHS
R19: EP - compile option = CLI not active in attempt to enable/disable ethernet service; counter if errors during errata fix processing
R18: EP - augment/fix the software CLI "reset" command to include SET/CLEAR of the "global soft reset" bits in both primary and secondary controllers
R17: EP - input and output contact closure feature; single, ordered checking of all downstram and LAN ports
R16: EP - final cleanup for phase 1
R15: EP - implementation of KSZ8795 errata; enable/disable/power reset any port
R14: EP - improved packet throughput between the LAN and the 4 downstream ports 1/2/3/4
R13: EP - miscellaneous changes, improved autonomous event for detection by the SBC/Gateway
R12: EP - the mib_index echo'd to output must be formatted to 2 hex characters
R11: EP - all manually built 5-byte register access sequences replaced with build5ByteCommandSequence(xx) facility
R10: EP - moved all primary switch code to the primary_switch.h/c modules for clarity; generalized get MIB counters infrastructure
R9:  EP - management of ports 1..4 + LAN; autonomous events detection + reporting to SBC infra-structure
R8:  EP - KZ9897 ethernet chip controller configuration on system startup
R7:  EP - re-wrote user signature based on group discussion and conclusion
R6:  EP - user signature in SAME70 internal flash facilities
R5:  EP - loging + checksum features
R4:  EP - add apparent.h for proprietary needs + user input/output over SBC COM/J21 as well as CRAFT/J50
R3:  EP - minor cleanup after some exploration with ethernet
R2:  EP - 2nd commit, user input/output over SBC COM/J21 interface
R1:  EP - very first commit to the amu repo

