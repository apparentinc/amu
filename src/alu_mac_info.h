/**
* \file
*
* \brief Apparent ALU MAC INFO Definitions.
*
* Copyright (c) 2023 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _ALU_MAC_INFO_H_
#define _ALU_MAC_INFO_H_


// Define the states of the MAC resolution state machine:


// To support the "get the MAC from the ALU for a specified port" feature.
//
// Generally: the "get the first ALU MAC for this port" command will be the more typical command,
// so when the MAC from the ALU is encountered against the specified port, processing stops and the
// MAC read out of the ALU is stored in the [0] rank of the MAC list. This data will be returned
// to the Gateway when it issues the get-result command.
//
//is the
typedef struct s_getAluMacInfoCb
{
    U32    stateMachineState;
    U32    aluGetType;
    char  *pUserCommand;
    U32    userPort;
    U32    aluStartIndex;
    U32    startReadTick;
    U32    lastReadTick;
    U32    portAsKey;
    U32    portAsIndex;
    U16    kszValidCount;          // <----
    U8     kszPortForward;         //      |  these 3 within a 32-bit entity
    U8     lastKszPortForwardRead; // <----
    U32    controller;
    U32    maxAluIndex;
    U32    current_index;
    U8     mac[6];
} t_getAluMacInfoCb;

#define ALU_MAC_INFO_STATE_DEAD_QUIET       1
#define ALU_MAC_INFO_STATE_IDLE             2
#define ALU_MAC_INFO_STATE_LAUNCH           3
#define ALU_MAC_INFO_STATE_PRI_READ         4
#define ALU_MAC_INFO_STATE_SEC_READ         5

#define ALU_GET_TYPE_UNKNOWN                1
#define ALU_GET_TYPE_ALU_MAC_BY_PORT_INDEX  2

// When reading the ALU of the primary controller:
// - number of rows to read in a loop
// - tick timeout before reading the next set of rows
#define NUMBER_READ_ROWS_PER_CYCLE         10
#define TICKS_BETWEEN_READ_ROWS          1000


// External Function Prototypes
void  initAluMacInfoData          (void);
void  cancelGetAluMacResult       (void);

void  launchAluInfoByPortIndex    (char *pUserInput);
void  getAluInfoByPortIndexResult (void);

void  getAluMacInfoData           (void);

BOOL  isAluMacInfoSmIdle          (void);
void  checkAluMacInfoSm           (void);


 
/*----------------------------------------------------------------------------*/
#endif   /* _ALU_MAC_INFO_H_ */
