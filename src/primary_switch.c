/**
 *
 * \brief Apparent Application Functions for the "PRIMARY" KSZ9897 Switch Controller.

 * Copyright (c) 2021 Apparent Inc., All rights reserved
 * http://www.apparent.com
 *
 *
 * This module contains utility support routines for the Apparent Gateway PCBA system,
 * to support the management of the PRIMARY KSZ9897 ethernet switch controller.
 *
 *
 */


#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#include "usart.h"
#include "status_codes.h"
#include "uart_serial.h"

#include "asf.h"
#include "stdio_serial.h"
#include "conf_board.h"
#include "conf_clock.h"
#include "board.h"

#include "apparent_strings.h"
#include "apparent.h"
#include "primary_switch.h"
#include "secondary_switch.h"
#include "poe.h"



// Really ought to have a main.h header file to be proper.
extern void mdelay              (uint32_t ul_dly_ticks);
extern U32  g_ul_ms_ticks;
extern char pCraftPortDebugData[];
extern char  pSingleJsonEventString[];


// To read from, or write to any register of the "PRIMARY" KSZ9897 ethernet chip controller (section 4.9.1/TABLE 4-25/page 54 of
// the data sheet), a sequence of 5 bytes are sent to the controller over the SPI interface using the usart_putchar() facility.
// The 5 bytes come from the 40 bits as specified in section 4.9.1, structured as follows:
//
//                 |<-----                                        40 bits total  = 5 bytes                                 ----->|
//                       |<---                  24-bit address                           ---> |            |<---    DATA     --->|
// Register Read:  XXX   23 22 21 20 19 18 17 16 15 14 13 12 11 10 9  8  7  6  5  4  3  2  1  0   ZZZZZ    D7 D6 D5 D4 D3 D2 D1 D0
// Register write: YYY
//
// where: - to read:  1st 3 bits XXX = 011
//        - to write: 1st 3 bits YYY = 010
//        - 5 bits ZZZZ = don't care (set to 0)
//        - 8-bit DATA is 00000000 for a read command
//
// Example: read global address 0x0300:
//                 011   23 22 21 20 19 18 17 16 15 14 13 12 11 10 9  8  7  6  5  4  3  2  1  0   XXXXX    D7 D6 D5 D4 D3 D2 D1 D0
//        bits ->  011   0  0  0  0  0  0  0  0  0  0  0  0  0  0  1  1  0  0  0  0  0  0  0  0   00000    0  0  0  0  0  0  0  0
//        bits ->  011000000000000001100000000000000000000
//    expanded ->  0110 0000   0000 0000   0110 0000   0000 0000   0000 0000
//                 6    0      0    0      6    0      0    0      0    0
//    so the 5 bytes to send to the controller to read the 1 byte of register address 0x0300 = 60 00 60 00 00
//
// In the very initial implementation of this module, the variables defining the 5-byte sequences were hand-built, a rather
// tedious and error-prone process. Subsequently, in a later version of this module, they were automatically built using the
// general purpose build5ByteCommandSequence() routine - simpler, and less error-prone.
//
//
// HENCEFORTH: all references to the KSZ9897 ethernet chip control will be with the word "PRIMARY", "Primary", or "primary".
//             WHY? Because the secondary ethernet chip controller - the KSZ8795 is "too close" in its nomenclature, and so
//             prone to confusion and error.
//


// A simple lookup table to get the "control register" address for ports 1..5:
static const U16 CONTROL_REG_ADDRESS_BY_PORT_LOOKUP []     = { PORT_1_CONTROL_REGISTER_ADDRESS, // [0]: port 1
                                                               PORT_2_CONTROL_REGISTER_ADDRESS, // [1]: port 2
                                                               PORT_3_CONTROL_REGISTER_ADDRESS, // [2]: port 3
                                                               PORT_4_CONTROL_REGISTER_ADDRESS, // [3]: port 4
                                                               PORT_5_CONTROL_REGISTER_ADDRESS  // [4]: port 5/LAN
                                                             };

// A simple lookup table to get the "status register" address for ports 1..5:
static const U16 STATUS_REG_ADDRESS_BY_PORT_LOOKUP  []     = { PORT_1_STATUS_REGISTER_ADDRESS,  // [0]: port 1
                                                               PORT_2_STATUS_REGISTER_ADDRESS,  // [1]: port 2
                                                               PORT_3_STATUS_REGISTER_ADDRESS,  // [2]: port 3
                                                               PORT_4_STATUS_REGISTER_ADDRESS,  // [3]: port 4
                                                               PORT_5_STATUS_REGISTER_ADDRESS   // [4]: port 5 (LAN)
                                                             };


// Data to manage and support ethernet service over the primary switch controller.
static t_primarySwitchInfo primarySwitchInfoCb[NUMBER_PRIMARY_SWITCH_PORTS];

static U16 aluAccessReadLoopLimit  = 10;
static U32 aluWaitActionBeforeRead = 150;
static U8  aluAccessCtrlHashing    = ALU_ACCESS_CTRL_041B_BY_HASH_FUNCTION;
static U8  aluAccessCtrlAction     = ALU_ACCESS_CTRL_041B_READ_VALUE;




// End of SBC/GATEWAY sensitive definitions.
//
// ----------------------------------------------------------------------------



// Static Module prototypes:
static BOOL isLinkStatusLinkChanged            (U16 thisStatus, U16 thatStatus);
static BOOL isStatusAnyOtherBitsChanged        (U16 statusOne, U16 statusTwo);
static void readUsart0UntilEmpty               (U32 counterIndex);
static void build5ByteCommandSequence          (U8 command, U16 address, U8 data, U8 *pBytes);
// NO MIB FOR NOW: static bool isPrimaryMibIndexValid             (U8 someIndex);
static void setPrimaryControlRegistersDefaults (void);
static void verifyPrimaryEthernetEnabled       (void);
static void enablePrimaryEthernetServiceStartup(void);
static void enablePrimaryEthernetService       (void);

static void updatePrimaryPortStatus            (void);
static U16  buildControlRegPort_1234Default    (void);
static U16  buildControlRegPort_5LanDefault    (void);

static void verifyPrimary25MhzClockOutput      (void);
static BOOL isPrimaryEthernetServiceEnabled    (void);
static BOOL isPrimary25MhzClockEnabled         (void);

static void setN108PhyAnAdvertisement          (void);
static void setN112Phy1000BaseTControl         (void);
static void ksz9897ErrataFixes                 (void);
static void singleLedModeFix                   (void);
static void verifyPrimaryRegisterWord          (U32 registerAddress, U16 expectedValue, U16 identifier);
static void verifyPrimaryRegisterByte          (U32 registerAddress, U8  expectedValue, U16 identifier);

static U16  readWordFromPrimaryByAddress       (U16 registerAddress);

/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
extern "C" {
#endif
/**INDENT-ON**/
/// @endcond





// Exported/external functions from this module:


void  readPrimaryGlobalRegisters(void)
{
    // Called by the CLI when the user wants to read out the global registers of the primary controller.
    // The following 5 global registers are read from these addresses: 0x0000, 0x0001, 0x0002, 0x0003, 0x0103, 0x0300
    // Print it out as a json-like string like:
    // "PRIMARY GLOBAL REGISTERS":{"GLOBAL REGISTER 0x00":"0x00","GLOBAL REGISTER 0x01":"0x01"...,"GLOBAL REGISTER 0x0F":"0x0F"}

    U32 registerValue;

    printf("%s:{", PRIMARY_GLOBAL_REGISTERS_STRING);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_GLOBAL_CHIP_ID_0_REGISTER_ADDRESS);          printf("%s:\"0x%02lX\",", GLOBAL_REGISTER_0000_STRING, registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_GLOBAL_CHIP_ID_1_REGISTER_ADDRESS);          printf("%s:\"0x%02lX\",", GLOBAL_REGISTER_0001_STRING, registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_GLOBAL_CHIP_ID_2_REGISTER_ADDRESS);          printf("%s:\"0x%02lX\",", GLOBAL_REGISTER_0002_STRING, registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_GLOBAL_CHIP_ID_3_REGISTER_ADDRESS);          printf("%s:\"0x%02lX\",", GLOBAL_REGISTER_0003_STRING, registerValue);
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS);     printf("%s:\"0x%02lX\",", GLOBAL_REGISTER_0103_STRING, registerValue);
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS); printf("%s:\"0x%02lX\"",  GLOBAL_REGISTER_0300_STRING, registerValue); // no comma
    printf("}"); // close off the inner and outer components.

}


U8 readByteFromPrimaryByAddress(U16 registerAddress)
{
    U8  pFiveBytes[5];
    U32 someLong;
    build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddress, 0x00, &pFiveBytes[0]);
    someLong = readFromPrimarySwitch(pFiveBytes);
    return (U8)someLong;
}


// All reads from the KSZ subsystem are 1-byte/8-bits. No such a thing as reading a word.
// This function is a convenience for the user that wants to read a 16-bit entity from
// 2 register addresses.
U16 readWordFromPrimaryByAddress(U16 registerAddress)
{
    U8  pFiveBytes[5];
    U32 someLong1, someLong2;
    U16 readValue;
    // Read the 1 byte from the specified address:
    build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddress, 0x00, &pFiveBytes[0]);
    someLong1 = readFromPrimarySwitch(pFiveBytes);

    // Read the 1-byte from the address + 1:
    registerAddress += 1;
    build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddress, 0x00, &pFiveBytes[0]);
    someLong2 = readFromPrimarySwitch(pFiveBytes);
    
    // Now combine:
    readValue = ((0x00FF & someLong1) << 8) | (0x00FF & someLong2);

    return readValue;
}


void  writeByteToPrimaryByAddress(U16 registerAddress, U8 someByte)
{
    U8 pFiveBytes[5];
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, someByte, &pFiveBytes[0]);
    writeByteToPrimarySwitch(pFiveBytes);
}


// Write a U16 word to the primary switch controller by some U16 register address:
//
// - declare a 6-byte array
// - build a 5-byte write sequence where the second-last byte of the sequence (byte[4]) contains the most-significant byte of the word-to-write
// - assign the least-significant byte of the word-to-write to the last byte of the byte sequence (byte[5])
// - then write the word to the controller using the constructed 6-byte sequence 
void  writeWordToPrimaryByAddress(U16 registerAddress, U16 someWord)
{
    U8 pSixBytes[6];
    U8 byte1, byte2;
    byte1 = someWord >> 8; // MSB
    byte2 = someWord;      // LSB
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, byte1, &pSixBytes[0]);
    pSixBytes[5] = byte2;
    writeWordToPrimarySwitch(pSixBytes);
}


U32 readFromPrimarySwitch(const U8 *pFiveBytes)
{
    U32 myData;

    usart_spi_force_chip_select(USART0);
    readUsart0UntilEmpty(PHY0_DATA_PRESENT_BEFORE_READ); // The receiver is expected to be empty.
    usart_putchar(USART0, pFiveBytes[0]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData); } // [0]
    usart_putchar(USART0, pFiveBytes[1]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData); } // [1]
    usart_putchar(USART0, pFiveBytes[2]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData); } // [2]
    usart_putchar(USART0, pFiveBytes[3]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData); } // [3]
    usart_putchar(USART0, pFiveBytes[4]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData); } // [4] <- KSZ9897 "turn around"
    readUsart0UntilEmpty(PHY0_DATA_PRESENT_AFTER_READ); // The receiver is expected to be empty.
    usart_spi_release_chip_select(USART0);
    return myData;
}


void writeByteToPrimarySwitch(const U8* pFiveBytes)
{
    U32 myData;

    usart_spi_force_chip_select(USART0);
    readUsart0UntilEmpty(PHY0_DATA_PRESENT_BEFORE_WRITE); // The receiver is expected to be empty.
    usart_putchar(USART0, pFiveBytes[0]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [0]
    usart_putchar(USART0, pFiveBytes[1]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [1]
    usart_putchar(USART0, pFiveBytes[2]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [2]
    usart_putchar(USART0, pFiveBytes[3]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [3]
    usart_putchar(USART0, pFiveBytes[4]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [4] <- KSZ9897 "turn around"
    readUsart0UntilEmpty(PHY0_DATA_PRESENT_AFTER_WRITE); // The receiver is expected to be empty.
    usart_spi_release_chip_select(USART0);
}


void writeWordToPrimarySwitch(const U8* pSixBytes)
{
    U32 myData;

    usart_spi_force_chip_select(USART0);
    readUsart0UntilEmpty(PHY0_DATA_PRESENT_BEFORE_WRITE); // The receiver is expected to be empty.
    usart_putchar(USART0, pSixBytes[0]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [0]
    usart_putchar(USART0, pSixBytes[1]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [1]
    usart_putchar(USART0, pSixBytes[2]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [2]
    usart_putchar(USART0, pSixBytes[3]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [3]
    usart_putchar(USART0, pSixBytes[4]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [4] <- KSZ9897 "turn around"
    usart_putchar(USART0, pSixBytes[5]); mdelay(KSV_DELAY); if (usart_is_rx_ready(USART0) == 1) { usart_getchar(USART0, &myData);  } // [5] <- KSZ9897 "turn around"
    readUsart0UntilEmpty(PHY0_DATA_PRESENT_AFTER_WRITE); // The receiver is expected to be empty.
    usart_spi_release_chip_select(USART0);
}


void readPrimaryPortControlAndStatusInfo(void)
{
    U16   iggy;
    char *pGenericString;

    // Read/display/print the set of CONTROL and STATUS register values, as a JSON-like string.
    // The ability to "write" a JSON-like string of arbitrary length for processing by the SBC
    // is controlled by several printf calls with a CR/LF when the JSON-like string is terminated.

    // First: the control registers:
    // Example:
    // "PRIMARY CONTROL REGISTERS":{"PORT 1":{"CONTROL":"0x2100","CONFIG":"POWERED UP"}, ... ,"PORT LAN":{"CONTROL":"0x1140","CONFIG":"POWERED UP"}},

    // Print the 1st part of this overall JSON-like string, i.e.: {"PRIMARY CONTROL REGISTERS":{ ",
    printf("%s:{", PRIMARY_CONTROL_REGISTERS_STRING);

    // Add each port.
    for (iggy = 0; iggy < NUMBER_PRIMARY_SWITCH_PORTS; iggy++) {
        if (primarySwitchInfoCb[iggy].portPoweredUp == TRUE) { pGenericString = (char *)&PORT_CONFIG_POWERED_UP_STRING;   }
        else                                                 { pGenericString = (char *)&PORT_CONFIG_POWERED_DOWN_STRING; }
        printf("\"%s %s\":{%s:\"0x%04X\",%s:%s}",
               PORT_NO_QUOTES_STRING, pGimmePrimaryPortString(iggy),
               CONTROL_STRING,        primarySwitchInfoCb[iggy].control,
               CONFIG_STRING,         pGenericString);
        if ((iggy+1) == NUMBER_PRIMARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                         { printf(","); }  // trailing comma for the next element
    } // for ...
    // Terminate the JSON with a comma to prepare for the next component.
    printf("},");

    // Next: the status registers. It's a port list with the STATUS register value and a LINK = UP or DOWN indicator:
    // Example of the primary status registers string:
    // "PRIMARY STATUS REGISTERS":"PORT 1":{"STATUS":"0x7949","LINK":"UP"}, ... ,"PORT LAN":{"STATUS":"0x7949","LINK":"UP"}}
    printf("%s:{", PRIMARY_STATUS_REGISTERS_STRING);
    // Add each port.
    for (iggy = 0; iggy < NUMBER_PRIMARY_SWITCH_PORTS; iggy++) {
        if (isPrimaryLinkStatusLinkUp(primarySwitchInfoCb[iggy].status)) { pGenericString = (char *)&LINK_STATUS_UP_STRING;   }
        else                                                             { pGenericString = (char *)&LINK_STATUS_DOWN_STRING; }
        printf("\"%s %s\":{%s:\"0x%04X\",%s:%s}",
               PORT_NO_QUOTES_STRING, pGimmePrimaryPortString(iggy),
               STATUS_STRING,         primarySwitchInfoCb[iggy].status,
               LINK_STRING,           pGenericString);
        if ((iggy+1) == NUMBER_PRIMARY_SWITCH_PORTS) {              } // last item, no comma, nothing to do
        else                                         { printf(","); } // trailing comma for the next element
    } // for ...
    // Terminate the JSON with a bracket to close the outer JSON string.
    printf("}");
}


// getPrimaryStatusByPortIndex
//
// Return the status register for a port by index. Called from the "display_port_info" CLI.
//
//
U16 getPrimaryStatusByPortIndex(U32 portIndex)
{
    return primarySwitchInfoCb[portIndex].status;
}


//  getPrimaryControlByPortIndex
//
// Return the status register for a port by index. Called from the "display_port_info" CLI.
//
//
U16 getPrimaryControlByPortIndex(U32 portIndex)
{
    return primarySwitchInfoCb[portIndex].control;
}


// The static functions of this module:


static void readUsart0UntilEmpty(U32 counterIndex)
{
    U8  keepReading = TRUE;
    U8  totalBytesRead;
    U8  bytesReadIndex;
    U32 myData[5];

    // This function called when the receiver is expected to be empty. If not, it is an anomaly, so empty it.
    if (usart_is_rx_ready(USART0) == 1) {
        // Keep reading until empty. During initial development and testing, only 1 byte was on occasion
        // detected. Maybe 2 bytes. So the following while loop is setup to track up to 5 bytes.
        totalBytesRead = 0; bytesReadIndex = 0; memset(myData, 0xFF, 5);
        while (keepReading) {
            usart_getchar(USART0, &myData[bytesReadIndex++]);
            totalBytesRead++;
            // Check for and prevent a stuck-loop condition:
            if (bytesReadIndex > 4) { incrementBadEventCounter(PHY0_EMPTY_CHECK_POTENTIAL_STUCK_LOOP); keepReading = FALSE; break; } 
            mdelay(KSV_DELAY);
            pokeWatchdog();
            if (usart_is_rx_ready(USART0) == 0) { 
                keepReading = FALSE;
            }
        } // while ...
        incrementBadEventCounter(counterIndex);
    }
}


static U16 buildControlRegPort_1234Default(void)
{
    U16 controlRegister;
    // For AMU/Apparent application, the following default values are prescribed
    // in the control register on a per-port basis for the 4 downstream ports 1, 2, 3 and 4:
    controlRegister = 0x0000; // initialized

    // PHY software reset bit [15]:
    controlRegister |= (PHY_CONTROL_SOFTWARE_RESET_MASK & PHY_CONTROL_SOFTWARE_RESET_DEFAULT);

    // Loopback mode bit [14]:
    controlRegister |= (PHY_CONTROL_LOCAL_LOOPBACK_MODE_MASK & PHY_CONTROL_LOCAL_LOOPBACK_MODE_DEFAULT);

    // Speed Select bits[6] and [13]:
    controlRegister |= (PHY_CONTROL_SPEED_SELECT_BIT6_MASK  & PHY_CONTROL_SPEED_SELECT_BIT6_PORTS_1234_DEFAULT);
    controlRegister |= (PHY_CONTROL_SPEED_SELECT_BIT13_MASK & PHY_CONTROL_SPEED_SELECT_BIT13_PORTS_1234_DEFAULT);

    // Auto-Negotiation bit [12]:
    controlRegister |= (PHY_CONTROL_AN_ENABLE_MASK & PHY_CONTROL_AN_ENABLE_PORTS_1234_DEFAULT);

    // Power Down bit [11]: it is based on the config parameter in the info control block.
    // On startup, by default, all ports are configured as ENABLED.
    controlRegister |= (PHY_CONTROL_POWER_DOWN_MODE_MASK & PHY_CONTROL_POWER_DOWN_MODE_DEFAULT);

    // Isolate bit [10]:
    controlRegister |= (PHY_CONTROL_ISOLATE_MASK & PHY_CONTROL_ISOLATE_NORMAL_DEFAULT);

    // Restart auto-negotiation bit [9]:
    controlRegister |= (PHY_CONTROL_RESTART_AN_MASK & PHY_CONTROL_RESTART_AN_DEFAULT);

    // Duplex Mode but [8]:
    controlRegister |= (PHY_CONTROL_DUPLEX_MODE_MASK & PHY_CONTROL_DUPLEX_MODE_DEFAULT);

    // Collision test bit [7]:
    controlRegister |= (PHY_CONTROL_COLLISION_TEST_MASK & PHY_CONTROL_COLLISION_TEST_DEFAULT);

    // Reserved Bits[5:0]:
    controlRegister |= (PHY_CONTROL_RESERVED_BITS_MASK & PHY_CONTROL_RESERVED_BITS_DEFAULT);

    return controlRegister;
}


static U16 buildControlRegPort_5LanDefault(void)
{
    U16 controlRegister;
    // For AMU/Apparent application, the following default values are prescribed
    // in the control register for the upstream port 5 "LAN":
    controlRegister = 0x0000; // initialized

    // PHY software reset bit [15]:
    controlRegister |= (PHY_CONTROL_SOFTWARE_RESET_MASK & PHY_CONTROL_SOFTWARE_RESET_DEFAULT);

    // Loopback mode bit [14]:
    controlRegister |= (PHY_CONTROL_LOCAL_LOOPBACK_MODE_MASK & PHY_CONTROL_LOCAL_LOOPBACK_MODE_DEFAULT);

    // Speed Select bits[6] and [13]:
    controlRegister |= (PHY_CONTROL_SPEED_SELECT_BIT6_MASK  & PHY_CONTROL_SPEED_SELECT_BIT6_PORT_5LAN_DEFAULT);
    controlRegister |= (PHY_CONTROL_SPEED_SELECT_BIT13_MASK & PHY_CONTROL_SPEED_SELECT_BIT13_PORT_5LAN_DEFAULT);

    // Auto-Negotiation bit [12]:
    controlRegister |= (PHY_CONTROL_AN_ENABLE_MASK & PHY_CONTROL_AN_ENABLE_PORT_5LAN_DEFAULT);

    // Power Down bit [11]: as with the other downstream ports 1/2/3/4, it is based on the config parameter
    // in the info control block. On startup, by default, the LAN port is configured as ENABLED.
    controlRegister |= (PHY_CONTROL_POWER_DOWN_MODE_MASK & PHY_CONTROL_POWER_DOWN_MODE_DEFAULT);

    // Isolate bit [10]:
    controlRegister |= (PHY_CONTROL_ISOLATE_MASK & PHY_CONTROL_ISOLATE_NORMAL_DEFAULT);

    // Restart auto-negotiation bit [9]:
    controlRegister |= (PHY_CONTROL_RESTART_AN_MASK & PHY_CONTROL_RESTART_AN_DEFAULT);

    // Duplex Mode but [8]:
    controlRegister |= (PHY_CONTROL_DUPLEX_MODE_MASK & PHY_CONTROL_DUPLEX_MODE_DEFAULT);

    // Collision test bit [7]:
    controlRegister |= (PHY_CONTROL_COLLISION_TEST_MASK & PHY_CONTROL_COLLISION_TEST_DEFAULT);

    // Reserved Bits[5:0]:
    controlRegister |= (PHY_CONTROL_RESERVED_BITS_MASK & PHY_CONTROL_RESERVED_BITS_DEFAULT);

    return controlRegister;
}


static void setPrimaryControlRegistersDefaults(void)
{
    // On startup, the defaults are the same for downstream ports 1/2/3/4:
    writeWordToPrimaryByAddress(PORT_1_CONTROL_REGISTER_ADDRESS, primarySwitchInfoCb[PORT_1_INDEX].control);
    writeWordToPrimaryByAddress(PORT_2_CONTROL_REGISTER_ADDRESS, primarySwitchInfoCb[PORT_2_INDEX].control);
    writeWordToPrimaryByAddress(PORT_3_CONTROL_REGISTER_ADDRESS, primarySwitchInfoCb[PORT_3_INDEX].control);
    writeWordToPrimaryByAddress(PORT_4_CONTROL_REGISTER_ADDRESS, primarySwitchInfoCb[PORT_4_INDEX].control);

    // The LAN port is slightly different (speed mostly):
    writeWordToPrimaryByAddress(PORT_5_CONTROL_REGISTER_ADDRESS, primarySwitchInfoCb[PORT_LAN_INDEX].control);
}


// checkPrimaryEthernetService
//
// Make sure that the "Start Switch" bit[0] in global register address 0x300 is set.
// During early testing, it was observed to be cleared - probably during an AMU restart,
// or perhaps during development when the code was not quite right. Not seen since.
// But checked nevertheless.
void checkPrimaryEthernetService(void)
{
    if (isPrimaryEthernetServiceEnabled()) { /* NO news is good news ... */  }
    else                                   { enablePrimaryEthernetService(); }
}


static void verifyPrimaryEthernetEnabled(void)
{
    // On startup, by default, the "ethernet enabled" bit[0] in the global register 0x0300 is set. Set it if not.
    if (isPrimaryEthernetServiceEnabled() == TRUE) {                                        }
    else                                           { enablePrimaryEthernetServiceStartup(); }
}


static void enablePrimaryEthernetServiceStartup(void)
{
    U32 registerValue;

    // SET the START bit to enable ethernet service.
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
    registerValue = registerValue | GLOBAL_OPERATION_START_SWITCH_MASK;
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS, registerValue);
}


static void enablePrimaryEthernetService(void)
{
    U32 registerValue;

    // SET the START bit to enable ethernet service.
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
    registerValue = registerValue | GLOBAL_OPERATION_START_SWITCH_MASK;
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS, registerValue);

    // ETHERNET HAS BEEN INADVERTANTLY DISABLED!!! There is no data with this event,
    // other than it was found disabled and was automatically re-enabled.
    // {"evt":{EI":7,"TICK":12345678}
    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:%ld}}", EVT_STRING,
                                                               EVENT_ID_STRING, AEI_PRIMARY_ETH_FOUND_DISABLED,
                                                               TICK_STRING,     g_ul_ms_ticks);
    addJsonToAutonomnousString(pSingleJsonEventString);
    incrementBadEventCounter(PRIMARY_ETHERNET_SERVICE_RE_ENABLED);
}


void disablePrimaryEthernetService(void)
{
    U32 registerValue;

    // To disable ethernet service: CLEAR the bit.
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
    registerValue = registerValue & (~GLOBAL_OPERATION_START_SWITCH_MASK);
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS, registerValue);
    printf("  - Primary register 0x%04X written with 0x%02lX (NOT START SWITCH)\r\n", GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS, registerValue);
}


void primaryControllerReset(void)
{
    U32 startRegisterValue;
    U32 setRegisterValue;
    U32 finalRegisterValue;

    // To reset the primary controller: SET bit[1] of the Switch Operation Register (address 0x0300).
    startRegisterValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
    setRegisterValue   = startRegisterValue | GLOBAL_OPERATION_SOFT_HW_RESET_MASK;
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS, setRegisterValue);

    // The bit is supposed to be self-clearing. But test and observation shows this is not the case,
    // resulting in subsequent auditing of the start switch bit[0] (checkEthernetPortsSm() from main)
    // that had to SET this bit anew. Wait a bit, then set the register back to its original content.
    mdelay(250);
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS, startRegisterValue);
    mdelay(250);
    finalRegisterValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
    printf("PRIMARY CONTROLLER RESET: 0x%02lX -> 0x%02lX -> 0x%02lX in REG ADDR 0x%04X\r\n", startRegisterValue, setRegisterValue, finalRegisterValue, GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
}


void enable25MhzClockOutput(U16 whodunit)
{
    // ENABLE the 25Mhz clock output. If by CLI, output a json-like string.
    // {"25MHZ CLOCK OUTPUT":{"COMMAND":"ENABLE","REG ADDR":"0x0103","REG VALUE":"0x02"}}
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS, ENABLE_CLKO_OUTPUT_VALUE);
    if (whodunit == ENABLE_DISABLE_IS_BY_COMMAND) {
        printf("{%s:{%s:%s,%s:\"0x%04X\",%s:\"0x%02X\"}}\r\n", MHZ_25_CLOCK_OUTPUT_STRING,
                                                               COMMAND_STRING,     ENABLE_STRING,
                                                               REG_ADDR_STRING,    GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS,
                                                               REG_VALUE_STRING,   ENABLE_CLKO_OUTPUT_VALUE);
        displayEndOfOutput();
    }
}


void disable25MhzClockOutput(U16 whodunit)
{
    // DISABLE the 25Mhz clock output. If by CLI, output a json-like string.
    // {"25MHZ CLOCK OUTPUT":{"COMMAND":"DISABLE","REG ADDR":"0x0103","REG VALUE":"0x00"}}
    writeByteToPrimaryByAddress(GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS, DISABLE_CLKO_OUTPUT_VALUE);
    if (whodunit == ENABLE_DISABLE_IS_BY_COMMAND) {
        printf("{%s:{%s:%s,%s:\"0x%04X\",%s:\"0x%02X\"}}\r\n", MHZ_25_CLOCK_OUTPUT_STRING,
                                                               COMMAND_STRING,     DISABLE_STRING,
                                                               REG_ADDR_STRING,    GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS,
                                                                REG_VALUE_STRING,   DISABLE_CLKO_OUTPUT_VALUE);
        displayEndOfOutput();
    }
}


static void verifyPrimary25MhzClockOutput(void)
{
    // Enable the CLKO 25Mhz clock output on the primary switch if not enabled.
    if (isPrimary25MhzClockEnabled() == TRUE) {                                                    }
    else                                      { enable25MhzClockOutput(ENABLE_DISABLE_INTERNALLY); }
}


static BOOL isPrimaryEthernetServiceEnabled(void)
{
    U32 registerValue;
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_SWITCH_OPERATION_REGISTER_ADDRESS);
    if ((registerValue & GLOBAL_OPERATION_START_SWITCH_MASK) == GLOBAL_OPERATION_START_SWITCH_ENABLED_VALUE) { return TRUE;  }
    else                                                                                                     { return FALSE; }
}

static BOOL isPrimary25MhzClockEnabled(void)
{
    U32 registerValue;
    registerValue = readByteFromPrimaryByAddress(GLOBAL_OPERATION_OUTPUT_CLOCK_REGISTER_ADDRESS);
    if ((registerValue & ENABLE_CLKO_OUTPUT_VALUE) == ENABLE_CLKO_OUTPUT_VALUE) { return TRUE;  }
    else                                                                        { return FALSE; }
}


void  readPrimaryMac(void)
{
    U32 registerValue;
    // "PRIMARY MAC":"00:10:A1:FF:FF:FF"
    printf("%s:", PRIMARY_MAC_STRING);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_MAC_REGISTER_00_ADDRESS); printf("\"%02X:", (U8)registerValue); // leading quote
    registerValue = readByteFromPrimaryByAddress(PRIMARY_MAC_REGISTER_01_ADDRESS); printf("%02X:",   (U8)registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_MAC_REGISTER_02_ADDRESS); printf("%02X:",   (U8)registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_MAC_REGISTER_03_ADDRESS); printf("%02X:",   (U8)registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_MAC_REGISTER_04_ADDRESS); printf("%02X:",   (U8)registerValue);
    registerValue = readByteFromPrimaryByAddress(PRIMARY_MAC_REGISTER_05_ADDRESS); printf("%02X\"",  (U8)registerValue); // terminating quote
}


// Check bit[2] of the status register: if it is SET, then link is UP.
BOOL isPrimaryLinkStatusLinkUp(U16 statusRegister)
{
    if ((statusRegister & PORT_STATUS_LINK_UP_DOWN_MASK) == PORT_STATUS_LINK_IS_UP)     { return TRUE;  } // 1 = link UP
    else                                                 /* PORT_STATUS_LINK_IS_DOWN */ { return FALSE; } // 0 = link DOWN
}


// Check the "Power Down" bit[11] in the control register. This is a misnomer, as the port is not actually powered down,
// rather, ethernet service is disabled. In any event, return TRUE if service is enabled, i.e., powered up.
BOOL isPrimaryLinkControlPoweredUp(U16 controlRegister)
{
    if ((controlRegister & PHY_CONTROL_POWER_DOWN_MODE_MASK) == PHY_CONTROL_POWER_DOWN_MODE_NORMAL_OPERATION) { return TRUE;  } // 1 = enabled/"power up"
    else                                                     /* PHY_CONTROL_POWER_DOWN_MODE_POWER_DOWN */     { return FALSE; } // 0 = disabled/"powered down"
}


// Check if the link up/down bit in the status register has changed.
static BOOL isLinkStatusLinkChanged(U16 thisStatus, U16 thatStatus)
{
    U16 thisBit;
    U16 thatBit;
    thisBit = thisStatus & PORT_STATUS_LINK_UP_DOWN_MASK;
    thatBit = thatStatus & PORT_STATUS_LINK_UP_DOWN_MASK;
    if (thisBit == thatBit) { return FALSE; } // Link up/down status did not change
    else                    { return TRUE;  } // Link up/down status changed either way
}


// Check if any STATUS bit other than bit[2] changed:
static BOOL isStatusAnyOtherBitsChanged(U16 statusOne, U16 statusTwo)
{
    U16 bitsOne;
    U16 bitsTwo;
    bitsOne = statusOne & PORT_STATUS_ANY_OTHER_BIT_MASK;
    bitsTwo = statusTwo & PORT_STATUS_ANY_OTHER_BIT_MASK;
    if (bitsOne == bitsTwo) { return FALSE; }
    else                    { return TRUE;  }
}


void  initPrimarySwitchData(void)
{
    U16 iggy;
    U16 controlRegister;

    // Initialize all the variables containing the "5-byte data sequences" that are used to access the registers of the
    // primary ethernet controller, be it a READ or WRITE command.
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("-- Initializing primary KSZ9897 switch controller data\r\n");
    #endif

    // By default, all ports are ENABLED until commanded otherwise by the Gateway.
    // The control register value default is the same for all ports except for
    // port "LAN" which gets re-assigned outside of the following loop.
    controlRegister = buildControlRegPort_1234Default();
    for (iggy = 0; iggy < NUMBER_PRIMARY_SWITCH_PORTS; iggy++) {
        primarySwitchInfoCb[iggy].control        = controlRegister;
        primarySwitchInfoCb[iggy].status         = 0xFFFF;
        primarySwitchInfoCb[iggy].portPoweredUp  = TRUE;
        primarySwitchInfoCb[iggy].restored       = FALSE;
    }
    controlRegister = buildControlRegPort_5LanDefault();
    primarySwitchInfoCb[PORT_LAN_INDEX].control = controlRegister;

}


// Configure the ethernet chip controller for SWITCH1/PRIMARY/over USART0.
//
// The sequence of actions is based on the Handy Randy hardware test script.
//
void configurePrimarySwitchController(void)
{
    U32 pioMask;
    U32 some32Bit;
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    U32 startTick = g_ul_ms_ticks;
    #endif

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ9897/USART0 PRIMARY ethernet controller configuration: START (tick:%ld)\r\n", startTick);
    #endif

    // 1.
    // From the data sheet section 41.6.3 Interrupt:
    // The SPI interface has an interrupt line connected to the interrupt controller. Handling the SPI interrupt requires
    // programming the interrupt controller before configuring the SPI.
    // Similar to UART0/1 where Tx is disabled, Rx is enabled:
    NVIC_EnableIRQ((IRQn_Type) USART0_IRQn);
    usart_enable_interrupt(USART0,  SW1_MISO_GPIO);        // receive  is enabled
    usart_disable_interrupt(USART0, SW1_MOSI_GPIO);        // transmit is disabled

    // These need to be disabled. Probably not required, but doesn't hurt.
    usart_disable_interrupt(USART0, SW1_CSN_GPIO);
    usart_disable_interrupt(USART0, SW1_SCLK_GPIO);

    // 2.
    // Enable Peripheral Clocks for USART0.
    //
    // No facility existed to write PMC_PCER0 register, it was created:
    // script: PMC = 0x400E0600
    // script: PMC_PCER0 = 0x0010
    // script: write to PCM_PCER0 = 0x400e0610 value 0000a000
    pmc_enable_periph_clk_pcer0(ID_USART0); // <- formerly part of a printf(xxx) which is no longer done on startup

    // 3.
    // Configure PB0,PB1,PB3,PB13 for USART0 Peripheral C Function
    // The script sets: PIO_PDR     = 0000200b
    //                  PIO_ABCDSR2 = 0000200b
    //
    
    // From same70q21b.h: PIO_PB0  (1u << 0)  PIO_PB1  (1u << 1)  PIO_PB3  (1u << 3) PIO_PB13 (1u << 13) so:
    pioMask = PIO_PB0 | PIO_PB1 | PIO_PB3 | PIO_PB13; // = 0x200B verified (also as per the hardware script)
    pio_set_peripheral((Pio *)(REG_PIOB_PDR),    PIO_PERIPH_C, pioMask);  // REG_PIOB_PDR (0x400E1004U) /**< \brief (PIOB) PIO Disable Register */
    pio_set_peripheral((Pio *)(REG_PIOB_ABCDSR), PIO_PERIPH_C, pioMask);  // REG_PIOB_ABCDSR 0x400E1070U  <- does not match script
    // Note: the hardware script write to REG_PIO_ABCDSR2 = at offset 0x0074 but this is not defined in piob.h header file
    //       so this function writes to REG_PIOB_ABCDSR at offset 0x0070 instead.

    {
        #define BAUD_RATE_TO_USE      19200
        #define CLOCK_TO_USE        1000000
        const usart_spi_opt_t p_usart_opt = { .baudrate     = BAUD_RATE_TO_USE,
                                              .char_length  = US_MR_CHRL_8_BIT,
                                              .spi_mode     = SPI_MODE_3,
                                              .channel_mode = US_MR_CHMODE_NORMAL,
                                            };
        // 4.
        // Set USART0 SCLK Frequency (22 MHz max) SCLK to 1 MHz')
        if (usart_set_async_baudrate(USART0, BAUD_RATE_TO_USE, CLOCK_TO_USE) == 0) {                                                            }
        else                                                                       { incrementBadEventCounter(PRIMARY_CONTROLLER_CONFIG_ERROR); }

        // 5.
        // # Set USART0 to SPI Master Mode 3, CPOL=1, CPHA=0, 8-bit data
        if (usart_init_spi_master(USART0, &p_usart_opt, CLOCK_TO_USE) == 0)  {                                                            }
        else                                                                 { incrementBadEventCounter(PRIMARY_CONTROLLER_CONFIG_ERROR); }

        // Section 47.5.1 "Baud Rate Generator" of the data sheet:
        // The baud rate generator provides the bit period clock named baud rate clock to both the receiver and the transmitter.
        // The baud rate clock is the peripheral clock divided by 16 times the clock divisor (CD) value written in the Baud Rate
        // Generator register (UART_BRGR).
        //
        // However, there is no specification as to what the baud rate should be. It is assumed that the hardware script is correct
        // (since it was also on advice from the Microchip service), so the BRGR register is validated to contain 0x84.
        some32Bit = get_brgr_value(USART0);
        if (some32Bit == 0x84) { set_brgr_value(USART0, 0x84); }
    }

    // 6.
    // Enable SPI0 TX and RX.
    usart_enable_tx(USART0);
    usart_enable_rx(USART0);
    usart_get_status(USART0), usart_get_mode(USART0), get_brgr_value(USART0); // <- formerly part of a printf(xxx) which is no longer done on startup

    // Check if USART0 transmit ready. Under normal conditions, there is no data in either the transmit or receive registers.
    if (usart_is_tx_ready(USART0) == 0) { incrementBadEventCounter(PRIMARY_CONTROLLER_CONFIG_ERROR); } // 0 Unexpected: data in the Transmit Holding Register.
    else                                {                                                            } // 1 Normal:     No data in the Transmit Holding Register.
    // Check if USART0 receive ready.
    if (usart_is_rx_ready(USART0) == 0) {} // 0 Normal: No data in the receiver, as expected on startup.
    else                                {  // 1 Unexpected: data in the receive Holding Register. Empty the receiver.
                                           incrementBadEventCounter(PRIMARY_CONTROLLER_CONFIG_ERROR);
                                           readUsart0UntilEmpty(PHY0_DATA_PRESENT_DURING_STARTUP);
                                        }

    usart_reset_status(USART0); // Command the controller to reset the status register. During testing, at one point when things
                                // were not working well, the US_MR_OVER register bit was set - this clears it, among other things.

    // Set the switch CONTROL registers for all ports to prescribed defaults.
    setPrimaryControlRegistersDefaults();

    // PHY Auto-Negotiation Advertisement:
    setN108PhyAnAdvertisement();

    // PHY 1000BaseT Control:
    setN112Phy1000BaseTControl();

    // Implementing all applicable fixes of the KSZ9897 errata:
    ksz9897ErrataFixes();

    // Implementing the "single LED mode" fix:
    singleLedModeFix();

    // Ensure that ethernet service is enabled on the primary switch.
    verifyPrimaryEthernetEnabled();

    // Ensure that the CLKO 25Mhz clock output is enabled on the primary switch.
    verifyPrimary25MhzClockOutput();

    // Probably too early for this, but what the heck.
    updatePrimaryPortStatus();

#ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ9897/USART0 PRIMARY ethernet controller configuration: DONE (ticks:%ld elapsed=%ld)\r\n", g_ul_ms_ticks, (g_ul_ms_ticks - startTick));
#endif
}


static void updatePrimaryPortStatus(void)
{
    // For loops are good for looping, but I don't feel loopy at this point in time.
    primarySwitchInfoCb[0].status = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[0]); // external port 1
    primarySwitchInfoCb[1].status = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[1]); // external port 2
    primarySwitchInfoCb[2].status = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[2]); // external port 3
    primarySwitchInfoCb[3].status = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[3]); // external port 4
    primarySwitchInfoCb[4].status = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[4]); // external port LAN
}


void configurePrimaryPortPowerBit(U16 portIndex, U8 portKey, BOOL powerUp)
{
    U16 controlRegisterBefore;
    U16 controlRegisterAfter;

    // Read out the current value of the control register for this port,
    // set/clear the "Power Down" bit[11] accordingly and write it back.
    //
    // This one is a wee bit counter-intuitive:
    //
    // - if the port is to be ENABLED:  then CLEAR bit[11]
    // - if the port is to be DISABLED: then SET   bit[11]
    controlRegisterBefore = readWordFromPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex]);
    
    if (powerUp == TRUE) { controlRegisterAfter = controlRegisterBefore & (~(PHY_CONTROL_POWER_DOWN_MODE_MASK | PHY_CONTROL_POWER_DOWN_MODE_NORMAL_OPERATION)); } // <- CLEAR bit[11], port is POWERED UP
    else                 { controlRegisterAfter = controlRegisterBefore | (  PHY_CONTROL_POWER_DOWN_MODE_MASK & PHY_CONTROL_POWER_DOWN_MODE_POWER_DOWN);        } // <- SET bit[11],   port is POWERED DOWN
    writeWordToPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex], controlRegisterAfter); // write the updated control register value to the controller
    primarySwitchInfoCb[portIndex].control       = controlRegisterAfter;                              // save the control register value for auditing
    primarySwitchInfoCb[portIndex].portPoweredUp = powerUp;                                           // save the configuration boolean

#if ENABLE_CRAFT_DEBUG == TRUE
    sprintf(&pCraftPortDebugData[0], "ETH PRI PORT %u: CONFIG POWER 0x%04X -> 0x%04X (%lu)\r\n", (portIndex+1), controlRegisterBefore, controlRegisterAfter, g_ul_ms_ticks);
    UART1_WRITE_PACKET_MACRO
#endif

    // The output is part of a list of ports whose power bit is to be re-configured. It is the responsibility
    // of the caller to ensure correct json formatting for the entire string output.
    // {"PORT":1,"COMMAND":"POWER_UP","REG_ADDR":"0x1234","CONTROL_BEFORE":"0x1234","CONTROL_AFTER":"0x2345"}
    printf("{%s:%u,%s:%s,%s:\"0x%04X\",%s:\"0x%04X\",%s:\"0x%04X\"}", PORT_STRING,           portKey,
                                                                      COMMAND_STRING,        (powerUp == TRUE ? PORT_COMMAND_POWER_UP_STRING : PORT_COMMAND_POWER_DOWN_STRING),
                                                                      REG_ADDR_STRING,       CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                                                      CONTROL_BEFORE_STRING, controlRegisterBefore,
                                                                      CONTROL_AFTER_STRING,  controlRegisterAfter);
}


// userLanPortUpDown
//
// Called from the CLI typically in a local test environment.
// It enables or disables ethernet service over the LAN port.
//
// {"CLI":{"COMMAND":"lan_port_enable" | "lan_port_disable",
//         "ARGUMENTS":"NONE",
//         "CODE":0,
//         "RESULT":"SUCCESS",
//         "RESPONSE":{"INFO":{"CONTROL BEFORE":"0x1234","CONTROL AFTER":0x5678"}}}}
void userLanPortUpDown(U32 portCommand)
{
    U16 controlRegisterBefore;
    U16 controlRegisterAfter;

    controlRegisterBefore = readWordFromPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[PORT_LAN_INDEX]);

    if (portCommand == PORT_COMMAND_ETH_ONLY_UP)     { controlRegisterAfter = controlRegisterBefore & (~(PHY_CONTROL_POWER_DOWN_MODE_MASK | PHY_CONTROL_POWER_DOWN_MODE_NORMAL_OPERATION)); } // <- CLEAR bit[11], port is POWERED UP/ENABLED
    else            /* PORT_COMMAND_ETH_ONLY_DOWN */ { controlRegisterAfter = controlRegisterBefore | (  PHY_CONTROL_POWER_DOWN_MODE_MASK & PHY_CONTROL_POWER_DOWN_MODE_POWER_DOWN);        } // <- SET bit[11],   port is POWERED DOWN/DISABLED
    writeWordToPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[PORT_LAN_INDEX], controlRegisterAfter);        // write the updated control register value to the controller
    primarySwitchInfoCb[PORT_LAN_INDEX].control       = controlRegisterAfter;                                     // save the control register value for auditing
    primarySwitchInfoCb[PORT_LAN_INDEX].portPoweredUp = (portCommand == PORT_COMMAND_ETH_ONLY_UP ? TRUE : FALSE); // save the configuration boolean

    printf("{%s:{%s:\"%s\",%s:%s,%s:%u,%s:%s,%s{%s:{%s:\"0x%04X\",%s:\"0x%04X\"}}}}\r\n",
            CLI_STRING, COMMAND_STRING, (portCommand == PORT_COMMAND_ETH_ONLY_UP ? LAN_PORT_ENABLE_CMD : LAN_PORT_DISABLE_CMD),
                        ARGUMENTS_STRING, NONE_STRING,
                        CODE_STRING,      0,
                        RESULT_STRING,    SUCCESS_STRING,
                        RESPONSE_STRING,  INFO_STRING, CONTROL_BEFORE_STRING, controlRegisterBefore, CONTROL_AFTER_STRING,  controlRegisterAfter);

}


static void verifyPrimaryRegisterWord( U32 registerAddress, U16 expectedValue, U16 identifier)
{
    U16 readValue;
    readValue = readWordFromPrimaryByAddress(registerAddress);
    if (readValue != expectedValue) { incrementBadEventCounter(ERRATA_VERIFY_ERROR_PRIMARY_CONTROLLER); }                                      
}


static void verifyPrimaryRegisterByte(U32 registerAddress, U8 expectedValue, U16 identifier)
{
    U8 readValue;
    readValue = readByteFromPrimaryByAddress(registerAddress);
    if (readValue != expectedValue) { incrementBadEventCounter(ERRATA_VERIFY_ERROR_PRIMARY_CONTROLLER); } 
}


static void setN112Phy1000BaseTControl(void)
{
    // Section 5.2.2.10 "PHY 1000BASE-T Control Register" N112/N113:
    // - ports 1..4: bit[9] = 0 so: N112=0x00 N113=0x00 -> 0x0000     ... 1000BaseT Full Duplex capable = NO
    // - port LAN:   bit[9] = 1 so: N112=0x02 N113=0x00 -> 0x0200     ... 1000BaseT Full Duplex capable = YES
    // TODO: replace the data values 0x0000 and 0x0200 magic numbers with appropriate #define
    writeWordToPrimaryByAddress(0x1112, 0x0000); verifyPrimaryRegisterWord(0x1112, 0x0000,  1);
    writeWordToPrimaryByAddress(0x2112, 0x0000); verifyPrimaryRegisterWord(0x2112, 0x0000,  2);
    writeWordToPrimaryByAddress(0x3112, 0x0000); verifyPrimaryRegisterWord(0x3112, 0x0000,  3);
    writeWordToPrimaryByAddress(0x4112, 0x0000); verifyPrimaryRegisterWord(0x4112, 0x0000,  4);
    writeWordToPrimaryByAddress(0x5112, 0x0200); verifyPrimaryRegisterWord(0x5112, 0x0200,  5);
}


static void setN108PhyAnAdvertisement(void)
{
    // Section 5.2.2.5 "PHY Auto-Negotiation Advertisement Register" N108/N109:
    // - set bit[8] = 1
    // - set bits[4:0] = 00001
    // so: N108 = 0x01 N109 = 0x01 -> 0x0101
    // TODO: replace the data values 0x0101 magic number with appropriate #define
    writeWordToPrimaryByAddress(0x1108, 0x0101); verifyPrimaryRegisterWord(0x1108, 0x0101,  6);
    writeWordToPrimaryByAddress(0x2108, 0x0101); verifyPrimaryRegisterWord(0x2108, 0x0101,  7);
    writeWordToPrimaryByAddress(0x3108, 0x0101); verifyPrimaryRegisterWord(0x3108, 0x0101,  8);
    writeWordToPrimaryByAddress(0x4108, 0x0101); verifyPrimaryRegisterWord(0x4108, 0x0101,  9);
    writeWordToPrimaryByAddress(0x5108, 0x0101); verifyPrimaryRegisterWord(0x5108, 0x0101, 10);
}


static void ksz9897ErrataFixes(void)
{
    U16 iggy;
    U32 mmdSetupAddress;
    U32 mmdDataAddress;
    U32 someRegisterValue;
    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    U32 tickStart = g_ul_ms_ticks;
    #endif

    // From the KSZ9897R Silicon Errata and Data Sheet Clarification.
    // Writing to various MMD register sets is required and rather cryptic.
    // Go to this page for more clarification and examples:
    // https://microchipsupport.force.com/s/article/KSZ9897-Family-PHY-MMD-Register-Access-Example

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ9897 ERRATA FIXES (tick:%ld):\r\n", tickStart);
    #endif

    // MODULE 1
    // Module 1: Register settings are needed to improve PHY receive performance
    // DESCRIPTION
    // The default receiver settings are not optimized. Receive errors may occur, especially at longer cable lengths.
    // END USER IMPLICATIONS
    // For best receiver performance, users should write the following PHY MMD registers. This is done individually for each
    // port (1-5) using any of the management interfaces: MDC/MDIO, I2C, SPI, or in-band.
    // Work around
    // Write to the following MMD registers for each PHY port [1-5]:
    // [MMD] [register] [data]
    // 0x01  0x6F       0xDD0B
    // 0x01  0x8F       0x6032
    // 0x01  0x9D       0x248C
    // 0x01  0x75       0x0060
    // 0x01  0xD3       0x7777
    // 0x1C  0x06       0x3008
    // 0x1C  0x08       0x2001
    //
    // Each of the above 7 items are written for all ports 1..5.
    //
    // EXAMPLE: to write MMD 0x01 register 0x6F data: DD0B (data sheet: section 5.2.2.12/5.2.2.13, page 120):
    //
    // 1: write to the "PHY MMD Setup Register 0x0D, 0xN11A/0xN11B":  00 01     = 0x0001 where 00 = "setup type register" and 01 = MMD 01
    // 2: write to the "PHY MMD Data  Register 0x0E, 0xN11C/0xN11D:   00 6F     = 0x006F this specifies register 6F
    // 3: write to the "PHY MMD Setup Register 0x0D, 0xN11A/0xN11B":  40 01     = 0x4001 where 40 = "setup type data" and 01 = MMD 01
    // 4: write to the "PHY MMD Data  Register 0x0E, 0xN11C/0xN11D:   DD 08     = 0xDD08 the actual U16 data
    // 5: read back and verify that 0xN11C/0xN11D = DD 08

    // TODO: replace the data values 0x0001/006F/4001/DD0B etc. magic numbers with appropriate #define

    // Do for all ports 1..5 (where 5 = LAN):
    for (iggy = 1; iggy <= 5; iggy++) {
        mmdSetupAddress = (0x1000 * iggy) + PHY_MMD_SETUP_REGISTER_BASE_ADDRESS_011A; // Base setup address for ports 1..5 = 0xN11A
        mmdDataAddress  = (0x1000 * iggy) + PHY_MMD_DATA_REGISTER_BASE_ADDRESS_011C;  // Base data  address for ports 1..5 = 0xN11A
        // Item 1/7, port [1..5]:  0x01  0x6F       0xDD0B:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x0001);     // setup type: register, MMD 01             <- bits[15:14] = 00 "register"
        writeWordToPrimaryByAddress(mmdDataAddress,  0x006F);     // write to specify register 6F of MMD 01
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x4001);     // setup type: data, for MMD 01/register 6F <- bits[15:14] = 10 "Data"
        writeWordToPrimaryByAddress(mmdDataAddress,  0xDD0B);     // write data type value DD08 to MMD 01/register 6F
        verifyPrimaryRegisterWord(mmdDataAddress,    0xDD0B, 11); // read-back expects this value
        // Item 2/7, port [1..5]: 0x01  0x8F       0x6032:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x0001); 
        writeWordToPrimaryByAddress(mmdDataAddress,  0x008F); 
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x4001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x6032);
        verifyPrimaryRegisterWord(mmdDataAddress,    0x6032, 12);
        // Item 3/7, port [1..5]:  0x01  0x9D       0x248C:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x0001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x009D);
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x4001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x248C);
        verifyPrimaryRegisterWord(mmdDataAddress,    0x248C, 13);
        // Item 4/7, port [1..5]:  0x01  0x75       0x0060:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x0001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x0075);
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x4001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x0060);
        verifyPrimaryRegisterWord(mmdDataAddress,    0x0060, 14);
        // Item 5/7, port [1..5]:  0x01  0xD3       0x7777:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x0001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x00D3);
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x4001);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x7777);
        verifyPrimaryRegisterWord(mmdDataAddress,    0x7777, 15);
        // Item 6/7, port [1..5]:  0x1C  0x06       0x3008:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x0006);
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x3008);
        verifyPrimaryRegisterWord(mmdDataAddress,    0x3008, 16);
        // Item 7/7, port [1..5]:  0x1C  0x08       0x2001:
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x0008);
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);
        writeWordToPrimaryByAddress(mmdDataAddress,  0x2001);
        // This one is dependent on the port being equipped (read back = 0x2001) or
        // non-equipped (read-back = 0x2000). So plain read back only, and check for
        // either value.
        someRegisterValue = readWordFromPrimaryByAddress(mmdDataAddress);
        if      (someRegisterValue == 0x2001) { /*all is good, nothing to do */                                   }
        else if (someRegisterValue == 0x2000) {                                                                   }
        else                                  { incrementBadEventCounter(ERRATA_VERIFY_ERROR_PRIMARY_CONTROLLER); }               
        //verifyPrimaryRegisterWord(mmdDataAddress,    0x2001, 17);
    }    

    // Module 2: Transmit waveform amplitude can be improved (1000BASE-T, 100BASE-TX, 10BASE-Te)
    // DESCRIPTION
    // The transmit waveform amplitude can be improved for 10BASE-Te, 100BASE-TX and 1000BASE-T.
    // END USER IMPLICATIONS
    // With the default settings, the waveform amplitude may be outside the specifications in some corner case conditions,
    // and the transmitter may not be fully compliant with the IEEE standard. This may degrade performance under some con-
    // ditions.
    // Work around
    // Write to the following MMD registers for each PHY port [1-5]:
    // [MMD] [register] [data]
    //  0x1C    0x4     0x00D0
    for (iggy = 1; iggy <= 5; iggy++) {
        mmdSetupAddress = (0x1000 * iggy) + PHY_MMD_SETUP_REGISTER_BASE_ADDRESS_011A; // Base setup address for ports 1..5 = 0xN11A
        mmdDataAddress  = (0x1000 * iggy) + PHY_MMD_DATA_REGISTER_BASE_ADDRESS_011C;  // Base data  address for ports 1..5 = 0xN11A
        // TODO: replace the data values 0x001C/0004/401C/00D0 magic numbers with appropriate #define
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);  // setup type: register, MMD 1C         <- bits[15:14] = 00 "register"
        writeWordToPrimaryByAddress(mmdDataAddress,  0x0004);  // register       = 04
        writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);  // setup type: data, MMD 1C             <- bits[15:14] = 40 "register"
        writeWordToPrimaryByAddress(mmdDataAddress,  0x00D0);  // data           = 0x00D0
        verifyPrimaryRegisterWord(mmdDataAddress,    0x00D0, 18); 
    }

    // Module 3: Default RGMII ingress timing does not comply with the RGMII specification
    // DESCRIPTION: The RGMII defining document specifies a typical data-to-clock setup time into the receiver
    //              (switch signals TXD6_[3:0], TX_ER6 and TX_EN6 to TX_CLK6) of 1.8ns. However, port 6 requires
    //              additional setup time in order to avoid ingress data errors on this interface.
    // END USER IMPLICATIONS: Careful analysis of the RGMII timing must be performed, considering the
    //                        timing of both connected devices, and relative signal delay times on the PCB.
    // Work around:  Another option is to set the ingress delay bit [4] in register 0x6301.
    //
    // Section 5.2.3.2 "XMII Port Control Register", page 127 <- This register is 1-byte/8-bits wide. Set bit[4] in register address 0x6301
    someRegisterValue = readByteFromPrimaryByAddress(0x6301);
    if ((someRegisterValue & 0x08) == 0x08) {                                                                                                       }
    else                                    { writeByteToPrimaryByAddress(0x6301, 0x08); incrementBadEventCounter(PRIMARY_CONTROLLER_CONFIG_ERROR); }

    // Module 4: Energy Efficient Ethernet (EEE) feature select must be manually disabled.
    //  If not disabled, the PHY ports can auto-negotiate to enable EEE, and this feature can cause link drops when linked
    // to another device supporting EEE.
    // Disable EEE by writing to the following registers. This is done individually for each PHY port (1-5) using any of
    // the management interfaces: MDC/MDIO, I2C, SPI, or in-band.
    // EEE is disabled by clearing bits [2:1] of the PHY indirect register: MMD 7, address 3Ch.
    // MMD register:
    // [MMD] [register] [data]
    //  7      0x3C      0x0000
    //
    // Applies only to port 5/LAN since it is the only one where auto-negotiation is enabled.
    // TODO: replace the data values 0x0007/003C/4007/0000 magic numbers with appropriate #define
    //
    // Table 4-27: STANDARD MIIM REGISTERS (page 56) of the data sheet:
    // 0xN11A - 0xN11B PHY MMD Setup Register <- LAN port (port 5) = 0x511A
    // 0xN11C - 0xN11D PHY MMD Data Register  <- LAN port (port 5) = 0x511C
    writeWordToPrimaryByAddress(0x511A, 0x0007);    // setup type: register, MMD 07               <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(0x511C, 0x003C);    // write to specify register 3C of MMD 07
    writeWordToPrimaryByAddress(0x511A, 0x4007);    // setup type: data, for MMD 07/register 04   <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(0x511C, 0x0000);     // write data 0000 to MMD 07/register 3C
    verifyPrimaryRegisterWord(0x511C,   0x0000, 19);

    
    // Module 5: Toggling PHY Power down can cause errors or link failures in adjacent PHYs.
    // Work around:
    // Avoid dynamically changing the power down state of any PHYs if other PHYs may be linked and active. Only
    // change the power up or power down state of a PHY when no other PHYs on the chip are linked and possibly
    // passing traffic.
    // NO WORK AROUND. CONDITION OBSERVED IN SYSTEM. LIVE WITH IT.

    // Module 6: Certain PHY registers must be written as pairs instead of singly.
    // THE REGISER SET SPECIFIED IN THE ERRATA ARE NOT WRITTEN TO BY THE APPARENT APPLICATION.

    // Module 7: Register settings are required to meet data sheet supply current specifications.
    // " total chip power is approximately 26% greater when all PHY ports are linked at 1000Mb/s. "
    // SO NOT APPLICABLE TO THE APPARENT APPLCIATION. EXCEPT: port 5/LAN - since it is 1000Mb/s.
    // [MMD] [register] [data]
    // 0x1C    0x13     0x6EFF
    // 0x1C    0x14     0xE6FF
    // 0x1C    0x15     0x6EFF
    // 0x1C    0x16     0xE6FF
    // 0x1C    0x17     0x00FF
    // 0x1C    0x18     0x43FF
    // 0x1C    0x19     0xC3FF
    // 0x1C    0x1A     0x6FFF
    // 0x1C    0x1B     0x07FF
    // 0x1C    0x1C     0x0FFF
    // 0x1C    0x1D     0xE7FF
    // 0x1C    0x1E     0xEFFF
    // 0x1C    0x20     0xEEEE

    iggy = 5; // re-use iggy for the LAN port only - no loop here.
    mmdSetupAddress = (0x1000 * iggy) + PHY_MMD_SETUP_REGISTER_BASE_ADDRESS_011A; // Base setup address for ports 1..5 = 0xN11A
    mmdDataAddress  = (0x1000 * iggy) + PHY_MMD_DATA_REGISTER_BASE_ADDRESS_011C;  // Base data  address for ports 1..5 = 0xN11A
    // Item 1:  0x1C    0x13     0x6EFF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0013);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x6EFF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x6EFF, 20);

    // Item 2:  0x1C    0x14     0xE6FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0014);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0xE6FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0xE6FF, 21);

    // Item 3:  0x1C    0x15     0x6EFF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0015);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x6EFF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x6EFF, 22);

    // Item 4:  0x1C    0x16     0xE6FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0016);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0xE6FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0xE6FF, 23);

    // Item 5:  0x1C    0x17     0x00FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0017);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x00FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x00FF, 24);

    // Item 6:  0x1C    0x18     0x43FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0018);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x43FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x43FF, 25);

    // Item 7:  0x1C    0x19     0xC3FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0019);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0xC3FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0xC3FF, 26);

    // Item 8:  0x1C    0x1A     0x6FFF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x001A);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x6FFF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x6FFF, 27);

    // Item 9:  0x1C    0x1B     0x07FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x001B);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x07FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x07FF, 28);

    // Item 10: 0x1C    0x1C     0x0FFF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x001C);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0FFF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0x0FFF, 29);

    // Item 11: 0x1C    0x1D     0xE7FF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x001D);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0xE7FF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0xE7FF, 30);

    // Item 12: 0x1C    0x1E     0xEFFF
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x001E);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0xEFFF);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0xEFFF, 31);

    // Item 13: 0x1C    0x20     0xEEEE
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x001C);    // setup: register, MMD 1C     <- bits[15:14] = 00 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0x0020);    // data:  register 04 of MMD
    writeWordToPrimaryByAddress(mmdSetupAddress, 0x401C);    // setup: data, for MMD 1C     <- bits[15:14] = 40 "register"
    writeWordToPrimaryByAddress(mmdDataAddress,  0xEEEE);    // data to write
    verifyPrimaryRegisterWord(mmdDataAddress,    0xEEEE, 32);


    // Module 8: Automatic SPI Data Out Edge Select may cause issues
    // The SPI is operating at below 15Mhz so:

    // Module 9: 1000BASE-T Transmitter Distortion fails to meet IEEE compliance specification

    // Module 10: No Pause frames are generated for ingress rate limiting with an EEE lin
    //
    // Global registers:
    // [addr] [data]
    // 0x03C0 0x4090
    // 0x03C2 0x0080
    // 0x03C4 0x2000
    writeWordToPrimaryByAddress(0x03C0, 0x4090); verifyPrimaryRegisterWord(0x03C0, 0x4090, 33);
    writeWordToPrimaryByAddress(0x03C2, 0x0080); verifyPrimaryRegisterWord(0x03C2, 0x0080, 34);
    writeWordToPrimaryByAddress(0x03C4, 0x2000); verifyPrimaryRegisterWord(0x03C4, 0x2000, 35);


    // FIX: Module 11: Link drop can occur when back pressure is enabled in 100BASE-TX half-duplex mode
    // 0x0331 0xD0
    writeByteToPrimaryByAddress(0x0331, 0xD0); verifyPrimaryRegisterByte(0x0331, 0xD0, 0001);

    #ifdef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP
    printf("  - KSZ9897 ERRATA FIXES: DONE (tick:%ld elapsed=%ld)\r\n", g_ul_ms_ticks, (g_ul_ms_ticks - tickStart));
    #endif
}


static void singleLedModeFix(void)
{
    // This has no bearing on PHY data integrity, but the "single LED mode" issue was raised
    // when the Gateway PCBA was being designed and built. This fix is implemented here.
    //
    // FIX: "Single-LED mode in the KSZ9897 and KSZ9893 Ethernet switch families".
    // From the Microchip site at:
    // https://microchipsupport.force.com/s/article/Single-LED-mode-in-the-KSZ9897-and-KSZ9893-Ethernet-switch-families
    //
    //      Select Single-LED mode by setting bit 4 in the MMD LED MODE REGISTER (PHY MMD address 0x02, register 0x00).
    //      Implement the workaround by writing 0xFA00 to register 0xN13C-0xN13D (PHY register 0x1E). This sets bit 9, which corrects Single-LED mode operation.
    writeWordToPrimaryByAddress(0x113C, 0xFA00); verifyPrimaryRegisterWord(0x113C, 0xFA00, 36);
    writeWordToPrimaryByAddress(0x213C, 0xFA00); verifyPrimaryRegisterWord(0x213C, 0xFA00, 37);
    writeWordToPrimaryByAddress(0x313C, 0xFA00); verifyPrimaryRegisterWord(0x313C, 0xFA00, 38);
    writeWordToPrimaryByAddress(0x413C, 0xFA00); verifyPrimaryRegisterWord(0x413C, 0xFA00, 39);
    writeWordToPrimaryByAddress(0x513C, 0xFA00); verifyPrimaryRegisterWord(0x513C, 0xFA00, 40);
}


// checkPrimaryEthernetPortControlRegister
//
// Read out the CONTROL register from the controller for a specified port, and compare to configured ("expected") value:
//
//   - if equal, then no trouble found.
//   - if not: trouble. An abnormal condition has been detected. This is a known bug, refer to "Module 5: Toggling PHY
//             Powerdown can cause errors or link failures in adjacent PHYs." in ksz9897ErrataFixes(). There is no known
//             fix for this. The control register is restored by configuration. If restored, the following autonomous
//             event is written: 
//
//             {"EVT":{"EI":25,"PORT":"<LAN|1|2|3|4>","REG ADDR":"0x1234","EXPECTED VALUE":"0x2100","ACTUAL VALUE":"0x2204"}}
//
void checkPrimaryEthernetPortControlRegister(U32 portIndex)
{
    U16  controlRegister;
    controlRegister = readWordFromPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex]);
    if (controlRegister != primarySwitchInfoCb[portIndex].control) {
        // Write the autonomous event and restore the port's control register.
        sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%04X\",%s:\"0x%04X\",%s:\"0x%04X\"}}",
                                            EVT_STRING,
                                            EVENT_ID_STRING,       AEI_PRIMARY_CONTROL_RESTORED,
                                            PORT_STRING,           pGimmePrimaryPortString(portIndex),
                                            REG_ADDR_STRING,       CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex],
                                            EXPECTED_VALUE_STRING, primarySwitchInfoCb[portIndex].control,
                                            ACTUAL_VALUE_STRING,   controlRegister);
        writeWordToPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex], primarySwitchInfoCb[portIndex].control);
        addJsonToAutonomnousString(pSingleJsonEventString);

        if (primarySwitchInfoCb[portIndex].restored == FALSE) {
            // First time through after having to restore the control register. It is expected to be fine upon the next cycle.
            primarySwitchInfoCb[portIndex].restored = TRUE;
        } else {
            // Was restored in last cycle, and required to be restored yet again. Not good. Not good at all.
            // Increment a bad event counter, this will show up in the gateway log.
            primarySwitchInfoCb[portIndex].restored = FALSE;
            incrementBadEventCounter(PRIMARY_CONTROL_RESTORE_FAILED);
        }

#if ENABLE_CRAFT_DEBUG == TRUE
        sprintf(&pCraftPortDebugData[0], "ETH PRI PORT %lu: CONTROL=0x%04X EXPECTED 0x%04X (POWERED UP=%u)\r\n",
                (portIndex+1), controlRegister, primarySwitchInfoCb[portIndex].control, primarySwitchInfoCb[portIndex].portPoweredUp);
        UART1_WRITE_PACKET_MACRO
#endif

    } else {
        //
        primarySwitchInfoCb[portIndex].restored = FALSE;
    }
}


BOOL isCurrentPrimaryLinkStatusUp(U32 portIndex)
{
    BOOL currentStatusUp = FALSE;
    U16  statusRegister;
    statusRegister  = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[portIndex]);
    if (isPrimaryLinkStatusLinkUp(statusRegister) == TRUE) { currentStatusUp = TRUE; }
    return currentStatusUp;
}


//
//
//
//
void  checkPrimaryEthernetPortStatusRegister(U32 portIndex)
{
    U16  statusRegister;
    U16  controlRegister;
    U16  eventId;

    // If the STATUS register for any port changes, write to the autonomous event string
    // that the SBC will poll for. The string is JSON-structured list, of the form:
    //
    // {"EVT":{"EI":30,"PORT":"1","STATUS BEFORE":"0x7949","STATUS AFTER":"0x796D"}     // <- LINK IS UP
    // {"EVT":{"EI":31,"PORT":"1","STATUS BEFORE":"0x796D","STATUS AFTER":"0x7949"}     // <- LINK IS DOWN
    //
    // Note: port number field is in quotes since on the gateway, the string value "1" .. "8" could also be "LAN"
    statusRegister = readWordFromPrimaryByAddress(STATUS_REG_ADDRESS_BY_PORT_LOOKUP[portIndex]);

    // STATUS REGISTER CHECK:
    //
    // Check the status registers. They are prone to changing depending on what's going on in the universe.
    if (primarySwitchInfoCb[portIndex].status == 0xFFFF) {
        // First time through.
        primarySwitchInfoCb[portIndex].status = statusRegister;
    } else {
        if (statusRegister != primarySwitchInfoCb[portIndex].status) {
            // A port change in the STATUS register. Only 1 bit is known to change on the basis of
            // device activity (LINK UP or LINK DOWN), it is specifically checked. If any other bit
            // changed, it is considered "any other".
            if (isLinkStatusLinkChanged(primarySwitchInfoCb[portIndex].status, statusRegister)) {
                // The link up/down status changed. Reset the POE class verification timer so that class
                // determination will be done quickly. Normally this is a long audit, but with a change
                // in port status, check auto class asap.
                if (isPrimaryLinkStatusLinkUp(statusRegister) == TRUE) { eventId = AEI_STATUS_LINK_UP; primaryPortLinkStatusChange(portIndex, PORT_IS_UP_HIGH_LEVEL);   }
                else                                                   { eventId = AEI_STATUS_LINK_DN; primaryPortLinkStatusChange(portIndex, PORT_IS_DOWN_HIGH_LEVEL); }

                setAutoClassConfigFastCheck();

                if (portIndex == PORT_LAN_INDEX) {
                    // EXCEPTION PROCESSING: LAN STATUS. There are no facilities to configure the LAN as UP or DOWN.
                    // Always expected to remain UP. Unless of course some miscreant fiddles with the ethernet cable
                    // inside the Gateway enclosure. The status is known to change on its own if any other port has
                    // been configured as UP or DOWN. Refer to "Module 5" of the primary controller errata.
                    // As the LAN status has changed, check the control register.
                    // {"EVT":{"EI":30,"PORT":"1","STATUS BEFORE":"0x7949","STATUS AFTER":"0x796D","CONTROL":{"EXPECTED VALUE":"0x1234","ACTUAL VALUE":"0x5678"}}}
                    // {"EVT":{"EI":30,"PORT":"1","STATUS BEFORE":"0x7949","STATUS AFTER":"0x796D","CONTROL":{"EXPECTED SAME AS ACTUAL":"0x5678"}}}
                     controlRegister = readWordFromPrimaryByAddress(CONTROL_REG_ADDRESS_BY_PORT_LOOKUP[portIndex]);
                     if (controlRegister == primarySwitchInfoCb[portIndex].control) {
                         sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%04X\",%s:\"0x%04X\",%s:{%s:\"0x%04X\"}}}",
                                                             EVT_STRING,
                                                             EVENT_ID_STRING,       eventId,
                                                             PORT_STRING,           pGimmePrimaryPortString(portIndex),
                                                             STATUS_BEFORE_STRING,  primarySwitchInfoCb[portIndex].status,
                                                             STATUS_AFTER_STRING,   statusRegister,
                                                             CONTROL_STRING,        EXPECTED_SAME_AS_ACTUAL_STRING, controlRegister);
                     } else {
                         sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%04X\",%s:\"0x%04X\",%s:{%s:\"0x%04X\",%s:\"0x%04X\"}}}",
                                                             EVT_STRING,
                                                             EVENT_ID_STRING,       eventId,
                                                             PORT_STRING,           pGimmePrimaryPortString(portIndex),
                                                             STATUS_BEFORE_STRING,  primarySwitchInfoCb[portIndex].status,
                                                             STATUS_AFTER_STRING,   statusRegister,
                                                             CONTROL_STRING,        EXPECTED_VALUE_STRING, primarySwitchInfoCb[portIndex].control,
                                                                                    ACTUAL_VALUE_STRING,   controlRegister);
                             
                     }                                        
                } else {
                    // For the other 4 downstream ports:
                    // Build the JSON element, add it to the autonomous event string.
                    sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%04X\",%s:\"0x%04X\"}}",
                                                        EVT_STRING,
                                                        EVENT_ID_STRING,      eventId,
                                                        PORT_STRING,          pGimmePrimaryPortString(portIndex),
                                                        STATUS_BEFORE_STRING, primarySwitchInfoCb[portIndex].status,
                                                        STATUS_AFTER_STRING,  statusRegister);
                }                                
                addJsonToAutonomnousString(pSingleJsonEventString);
                primarySwitchInfoCb[portIndex].status = statusRegister;

            } else if (isStatusAnyOtherBitsChanged(primarySwitchInfoCb[portIndex].status, statusRegister)) {
                // 1 or more of the remaining miscellaneous bits changed. Based on testing, bit[5] = "Auto-negotiation complete = 1"
                // is most likely to change, which happens when a port that was DISABLED by command is then ENABLED.
                // Build the JSON element, add it to the autonomous event string.
                sprintf(&pSingleJsonEventString[0], "{%s:{%s:%u,%s:\"%s\",%s:\"0x%04X\",%s:\"0x%04X\"}}",
                                                    EVT_STRING,
                                                    EVENT_ID_STRING,      AEI_STATUS_ANY,
                                                    PORT_STRING,          pGimmePrimaryPortString(portIndex),
                                                    STATUS_BEFORE_STRING, primarySwitchInfoCb[portIndex].status,
                                                    STATUS_AFTER_STRING,  statusRegister);
                addJsonToAutonomnousString(pSingleJsonEventString);
                primarySwitchInfoCb[portIndex].status = statusRegister;
                incrementBadEventCounter(PRIMARY_ANY_STATUS_BITS_CHANGED);
            } else {
                // Nothing has changed, nothing to see, keep moving.
            }
        }        
    }
}



#if 0   // All KSZ "MIB" counters processing removed until a firm set of requirements is defined.


static bool isPrimaryMibIndexValid(U8 someIndex)
{
    BOOL validIndex = FALSE;
    if      ((someIndex >= PRIMARY_SWITCH_MIB_INDEX_MIN)       && (someIndex <= PRIMARY_SWITCH_MIB_INDEX_MAX))       { validIndex = TRUE; } // the set of 30-bit counters
    else if ((someIndex >= PRIMARY_SWITCH_36BIT_MIB_INDEX_MIN) && (someIndex <= PRIMARY_SWITCH_36BIT_MIB_INDEX_MAX)) { validIndex = TRUE; } // the set of 36-bit counters
    return validIndex;
}


// Get a mib counter for this port. Getting a MIB counter requires a multi-step interaction with the controller.
// For each action, print a specific JSON-like string. The JSON-like structure for each action is shown below as
// processing progresses. The overall result that is returned to the caller/user is a single-line string, terminated
// with CR/LF as the delimiter for processing of the results.
//
// If there was an error, an error string component is returned within the overall JSON-like string.
void getPrimarySwitchMibCounter(U16 primaryPortNumber, U8 mibIndex, char *pPortNumberString)
{
    U16  actionNumber = 1;
    U8   writeN500[5];
    U8   readN500[5];
    U8   writeN501[5];
    BOOL readEnableSet = TRUE;
    BOOL okToReadCounterRegisters = FALSE;
    U32  readEnableValue;
    U16  readAttempts = 0;
    U8   writeData;

    U8   readN503[5];
    U8   readN504[5];
    U8   readN505[5];
    U8   readN506[5];
    U8   readN507[5];
    U8   readN503Value;
    U8   readN504Value;
    U8   readN505Value;
    U8   readN506Value;
    U8   readN507Value;

    U16  registerAddressN503;
    U16  registerAddressN504;
    U16  registerAddressN505;
    U16  registerAddressN506;
    U16  registerAddressN507;

    U16  registerAddress;
    char pAddressAsString[10];

    U32  timestampStart;
    U32  timestartEnd;

    uint64_t longLongCounter; // <- what the caller/user desires!
    U8      *pLongLongAsByte;

    char   pTitleString[64];

    timestampStart = g_ul_ms_ticks;

    // { "MIB COUNTER":{"PORT":2,"MIB INDEX":"81",
    //printf("{ %s:{",         MIB_COUNTER_STRING);                 // The main title
    //printf("\"%s\":\"%s\",", PORT_STRING, pPortNumberString);     // add the PORT component lookup
    //printf("%s:\"%02X\",",   MIB_INDEX_STRING, mibIndex);         // add the MIB INDEX component
    sprintf(pTitleString, "{ %s:{%s:\"%s\", %s:\"%02X\",", MIB_COUNTER_STRING, PORT_STRING, pPortNumberString,   MIB_INDEX_STRING, mibIndex);

    // First, validate the port and mib index. If invalid, return a JSON-like string with the reason.
    //
    // Then return the following action notifications to the caller:
    //
    // ACTION 1: set the MIB INDEX as a write command in the N501 register
    // ACTION 2: set the MIB Read Enable bit as a write command in the N501 register
    // ACTION 3: check that the MIB Read Enable clears itself. For this, enter a while loop until the bit clears itself
    // ACTION 4: - read the individual bytes of the counter from registers N503/504/505/506/507
    //           - concatenate into a double long
    //           - printf a formated hex and decimal
    // Finally: close off the JSON-like string and go home.

    // First things first: validate port and index.
    if ((primaryPortNumber < PRIMARY_SWITCH_PORT_NUMBER_MIN) || (primaryPortNumber > PRIMARY_SWITCH_PORT_NUMBER_MAX)) {
        // Set the error in the return string, and close off the JSON-like string.
        // Return here and now. Returning in the middle of a function is "not nice",
        // but it does avoid code indentation for processing valid data.
        incrementBadEventCounter(INVALID_PRIMARY_PORT_NUMBER);
        printf("%s%s:\"INVALID PRIMARY PORT NUMBER SPECIFIED\"}}\r\n", pTitleString, ERROR_STRING);
        return;
    } else {
        // And now the index.
        if (!isPrimaryMibIndexValid(mibIndex)) {
            incrementBadEventCounter(INVALID_PRIMARY_MIB_INDEX);
            printf("%s%s:\"INVALID PRIMARY MIB INDEX SPECIFIED\"}}\r\n", pTitleString, ERROR_STRING);
            return;
        }
    }

    // FOR THIS PORT:
    printf("%s", pTitleString);

    // ACTION 1: set the MIB INDEX as a write command in the N501 register:
    //           - build the 5-byte sequence to write the MIB INDEX to the 0xN500 register
    //           - spoon-feed those those 5 bytes to the switch.
    //           - include this info in the response
    // This part of the response should look something like:
    // "ACTION 1":{"COMMAND":"SET MIB INDEX","ADDRESS":"0x5501","BYTE SEQUENCE":"40 0A A0 20 0C"},
    sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_SET_MIB_INDEX_BASE_ADDRESS_N501);
    registerAddress = atoh(pAddressAsString);
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, mibIndex, &writeN501[0]);    // <- TODO: REPLACE WITH WRITE-BY-ADDRESS FACILITY PLEASE!!!
    writeByteToPrimarySwitch(writeN501);
    printf("\"%s %u\":{%s:%s,%s:\"0x%04X\",%s:\"\%02X %02X %02X %02X %02X\"}, ",
           ACTION_STRING, actionNumber++, COMMAND_STRING, SET_MIB_INDEX_STRING, ADDRESS_STRING, registerAddress, BYTE_SEQUENCE_STRING, writeN501[0], writeN501[1], writeN501[2], writeN501[3], writeN501[4]);

    // ACTION 2: set the MIB Read Enable bit as a write command in the N500 register:
    //           - build the 5-byte sequence to set the MIB Read Enable [25] bit in the 0xN500 register;
    //           - include this info in the response
    //           - spoon-feed those those 5 bytes to the switch.
    // This part of the response should look something like:
    // "ACTION 2":{"COMMAND":"SET MIB READ ENABLE","ADDRESS":"0x5500","BYTE SEQUENCE":"40 0A A0 00 02"}
    sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_SET_MIB_READ_ENABLE_BASE_ADDRESS_N500);
    registerAddress = atoh(pAddressAsString);
    writeData = 0xFF & MIB_READ_ENABLE_COUNT_VALID_BIT_MASK;
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, writeData, &writeN500[0]);    // <- TODO: REPLACE WITH WRITE-BY-ADDRESS FACILITY PLEASE!!!
    writeByteToPrimarySwitch(writeN500);
    printf("\"%s %u\":{%s:%s,%s:\"0x%04X\",%s:\"\%02X %02X %02X %02X %02X\"}, ",
           ACTION_STRING, actionNumber++, COMMAND_STRING, SET_MIB_READ_ENABLE_STRING, ADDRESS_STRING, registerAddress, BYTE_SEQUENCE_STRING, writeN500[0], writeN500[1], writeN500[2], writeN500[3], writeN500[4]);

    // ACTION 3: check that the MIB Read Enable clears itself.
    //           - build the 5-byte sequence to access the MIB Read Enable [25] bit in the 0xN500 register
    //           - the data bit is donut-care, so 0 is passed to the build function.
    //           - enter into a while loop reading the N500 register:
    //               - if the MIB Read Enable is still set, then wait and read again
    //               - if the MIB Read Enable is cleared, exit the while loop
    //               - the overflow bit [31] of N500 is extracted, and OVERFLOW value is included in the response.  
    // A read limit of 5 is arbitrarily set.
    // This part of the response should look something like:
    // "ACTION 3":{"COMMAND":"CHECK MIB READ ENABLE","ADDRESS":"0x5500","BYTE SEQUENCE":"60 0A A0 00 00","READ ATTEMPTS":1,"COUNTER OVERFLOW":"NO"},
    build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddress, 0, &readN500[0]); // <- RE-USING registerAddress FROM ABOVE!!!
    printf("\"%s %u\":{%s:%s,%s:\"0x%04X\",%s:\"\%02X %02X %02X %02X %02X\",",
           ACTION_STRING, actionNumber++, COMMAND_STRING, CHECK_MIB_READ_ENABLE_STRING, ADDRESS_STRING, registerAddress, BYTE_SEQUENCE_STRING, readN500[0], readN500[1], readN500[2], readN500[3], readN500[4]);
    while (readEnableSet) {
        readEnableValue = readFromPrimarySwitch(readN500);
        readAttempts++;
        if ((readEnableValue & MIB_READ_ENABLE_COUNT_VALID_BIT_MASK) == 0) {
            // The bit is cleared. Ready to read the registers containing the actual counter value.
            // Don't forget to check the overflow bit.
            printf("%s:%u,", VALUE_STRING, (U8)readEnableValue);
            printf("%s:%u,", READ_ATTEMPTS_STRING, readAttempts);
            if ((readEnableValue & MIB_COUNTER_OVERFLOW_INDICATION_BIT_MASK) == 0) { printf("%s:%s},", COUNTER_OVERFLOW_STRING, NO_STRING);  }
            else                                                                   { printf("%s:%s},", COUNTER_OVERFLOW_STRING, YES_STRING); }
            readEnableSet = FALSE;
            okToReadCounterRegisters = TRUE;
            break; // just in case ...
        } else {
            // Bit is still set.
            if (readAttempts < 5) {
                // Wait and try again. It's a relatively long delay.
                mdelay(RETRY_MIB_INDEX_READ_WAIT_DELAY);
            } else {                
                // Limit reached. Something wrong. Tell the caller. okToReadCounterRegisters remains FALSE.
                readEnableSet = FALSE;
                printf("%s:\"N500 READ LIMIT REACHED, CHECK MIB READ ENABLE FAILED TO CLEAR\"", ERROR_STRING);
                break;
            }
        }
    } // while ...

    if (okToReadCounterRegisters) {
        // ACTION 4: get that bugger of a counter:
        //           - build the address set for registers N503/504/505/506/507
        //           - build the 5-byte sequences for these registers
        //           - read register N503, N504, N505, N506, N507
        //           - concatenate
        //           - display the counter as hex
        //           - display the counter as decimal
        //           - close off JSON-like string, print, go home.
        // This part of the response should look something like (and it's a biggie):
        // "ACTION 4":{"COMMAND":"READ N503/504/505/506/507","ADDRESS LIST":[{"ADDRESS":"0x5503","BYTE SEQUENCE":"60 0A A0 60 00","VALUE":"00"},
        //                                                                   {"ADDRESS":"0x5504","BYTE SEQUENCE":"60 0A A0 80 00","VALUE":"00"},
        //                                                                   {"ADDRESS":"0x5505","BYTE SEQUENCE":"60 0A A0 A0 00","VALUE":"0B"},
        //                                                                   {"ADDRESS":"0x5506","BYTE SEQUENCE":"60 0A A0 C0 00","VALUE":"40"},
        //                                                                   {"ADDRESS":"0x5507","BYTE SEQUENCE":"60 0A A0 E0 00","VALUE":"66"}],
        // "CONCATENATED":0x00000B4066,"COUNTER VALUE":737382,"MIB RESULT":"SUCCESS"}}}
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_4_BASE_ADDRESS_N503);   registerAddressN503 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_3_BASE_ADDRESS_N504);   registerAddressN504 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_2_BASE_ADDRESS_N505);   registerAddressN505 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_1_BASE_ADDRESS_N506);   registerAddressN506 = atoh(pAddressAsString);
        sprintf(pAddressAsString, "%u%u", primaryPortNumber, MIB_COUNTER_BYTE_0_BASE_ADDRESS_N507);   registerAddressN507 = atoh(pAddressAsString);

        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN503, 0, &readN503[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN504, 0, &readN504[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN505, 0, &readN505[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN506, 0, &readN506[0]);
        build5ByteCommandSequence(PRIMARY_READ_COMMAND, registerAddressN507, 0, &readN507[0]);
        readN503Value = (U8)readFromPrimarySwitch(readN503);    // most significant byte of the counter
        readN504Value = (U8)readFromPrimarySwitch(readN504);
        readN505Value = (U8)readFromPrimarySwitch(readN505);
        readN506Value = (U8)readFromPrimarySwitch(readN506);
        readN507Value = (U8)readFromPrimarySwitch(readN507);    // least significant byte of the counter

        // Construct the long long counter value.
        //
        // NOTE: all counters are 30-bit counters except for 0x80 and 0x81 which are 36-bit counters.
        //       It so happens that 80/81 are of interest to the SBC/gateway, and will certainly be called for.
        //       All counters are treated uniformly as 36-bit counters, consequently, all counters will be
        //       processed as LONG LONG counters. The upper-most bits of the counter is in the N503 register.
        longLongCounter    = 0x0000000000000000;
        pLongLongAsByte    = (U8 *)&longLongCounter;
        pLongLongAsByte[4] = readN503Value;
        pLongLongAsByte[3] = readN504Value;
        pLongLongAsByte[2] = readN505Value;
        pLongLongAsByte[1] = readN506Value;
        pLongLongAsByte[0] = readN507Value;

        //    printf("{\"STUFF\":{\"1\":\"%02X\",\"2\":\"%02X\",\"3\":\"%02X\",\"4\":\"%02X\",\"5\":\"%02X\",\"long hex\":\"%llX\",\"long decimal\":%llu}}, ",

        // PRETTY MUCH DONE!
        // The last "action" is expected to look something like this:
        //
        // "ACTION 4":{"COMMAND":"READ N503/4/5/6",
        //                "ADDRESS LIST":[{"ADDRESS":"0x1503","BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x00"},
        //                                {"ADDRESS":"0x1504","BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x12"},
        //                                {"ADDRESS":"0x1505","BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x34"},
        //                                {"ADDRESS":"0x1506","BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x56"},
        //                                {"ADDRESS":"0x1507","BYTE SEQUENCE":"60 02 A0 00 00","VALUE":"0x78"}],
        //                "CONCATENATED":"0x0012345678", }
        //
        // The above will be constructed with individual print statements.
        // It's the final element in the JSON-like string, so it has no trailing comma.

        printf("\"%s %u\":{%s:%s,", ACTION_STRING, actionNumber++, COMMAND_STRING, READ_N503_4_5_6_7_STRING);
        printf("%s:[", ADDRESS_LIST_STRING);
        printf("{%s:\"0x%04X\",%s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN503, BYTE_SEQUENCE_STRING, readN503[0], readN503[1], readN503[2], readN503[3], readN503[4], VALUE_STRING, readN503Value);
        printf("{%s:\"0x%04X\",%s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN504, BYTE_SEQUENCE_STRING, readN504[0], readN504[1], readN504[2], readN504[3], readN504[4], VALUE_STRING, readN504Value);
        printf("{%s:\"0x%04X\",%s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN505, BYTE_SEQUENCE_STRING, readN505[0], readN505[1], readN505[2], readN505[3], readN505[4], VALUE_STRING, readN505Value);
        printf("{%s:\"0x%04X\",%s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"},",   ADDRESS_STRING, registerAddressN506, BYTE_SEQUENCE_STRING, readN506[0], readN506[1], readN506[2], readN506[3], readN506[4], VALUE_STRING, readN506Value);
        printf("{%s:\"0x%04X\",%s:\"%02X %02X %02X %02X %02X\",%s:\"%02X\"}]},", ADDRESS_STRING, registerAddressN507, BYTE_SEQUENCE_STRING, readN507[0], readN507[1], readN507[2], readN507[3], readN507[4], VALUE_STRING, readN507Value);
        // Note: last one above, ] square bracket to close off the list and } bracket to close off this last "action".

        // TOTALLY DONE! Return the actual "32-bit counter" as both hex and decimal, and signal to the caller/user that all went well.
        printf("%s:\"%llX\",",   VALUE_AS_HEX_STRING,     longLongCounter);           // omit the leading 0x please.
        printf("%s:%llu,",       VALUE_AS_DECIMAL_STRING, longLongCounter);
        timestartEnd = g_ul_ms_ticks;
        printf("%s:%lu,",        MIB_READ_ELAPSED_MSECS_STRING, (timestartEnd - timestampStart));
        printf("%s:%s",          MIB_RESULT_STRING,       MIB_RESULT_SUCCESS_STRING); // <- VERY LAST ITEM IN THIS JSON-LIKE STRING, SO NO TRAILING COMMA!!!

    } else {
        // NOT OK ... Hmmmmmm
        printf("%s:%s}", MIB_RESULT_STRING, MIB_RESULT_FAILED_STRING); // <- VERY LAST ITEM IN THIS JSON-LIKE STRING, SO NO TRAILING COMMA!!!
    }

    // Done. For the benefit and amusement of the caller/user: close off the JSOM-like string and print.
    printf("}}\r\n");

    return;

}


// TODO: MERGE THE FOLLOWING 2 FUNCTIONS INTO 1 WITH PARAMETER INDICATING ENABLE/DISABLE. PLEASE.
void enableFlushFreezeByPortKey(U16 primaryPortNumber, char *pPortNumberString)
{
    U8   writeN500[5];
    U8   registerValue;
    U16  registerAddress;
    char pAddressAsString[10];

    sprintf(pAddressAsString, "%u500", primaryPortNumber);
    registerAddress = atoh(pAddressAsString);
    registerValue = MIB_FLUSH_AND_FREEZE_ENABLE_BIT_MASK;
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, registerValue, &writeN500[0]);
    writeByteToPrimarySwitch(writeN500);

    // {"port 1":"DONE","ADDRESS":"0x1500"}
    printf("{\"%s %s\":%s,%s:\"0x%s\"}", PORT_NO_QUOTES_STRING, pPortNumberString, EXECUTION_DONE_STRING,  ADDRESS_STRING, pAddressAsString);
}


void disableFlushFreezeByPortKey(U16 primaryPortNumber, char *pPortNumberString)
{
    U8   writeN500[5];
    U8   registerValue;
    U16  registerAddress;
    char pAddressAsString[10];

    sprintf(pAddressAsString, "%u500", primaryPortNumber);
    registerAddress = atoh(pAddressAsString);
    registerValue = 0; // clear the bit.
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, registerAddress, registerValue, &writeN500[0]);
    writeByteToPrimarySwitch(writeN500);

    // {"port 1":"DONE","ADDRESS":"0x1500"}
    printf("{\"%s %s\":%s,%s:\"0x%s\"}", PORT_NO_QUOTES_STRING, pPortNumberString, EXECUTION_DONE_STRING,  ADDRESS_STRING, pAddressAsString);
}


void flushPrimarySwitchMibCounters(void)
{
    U8   write0336[5];

    // Set the upper-most bit [7] of address 0x0336 of the primary switch.
    // The bit is self-clearing.
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, SWITCH_MIB_CONTROL_REGISTER_ADDRESS, MIB_CONTROL_FLUSH_MIB_COUNTERS_MASK, &write0336[0]);
    writeByteToPrimarySwitch(write0336);
}


void freezePrimarySwitchMibCounters(void)
{
    U8   write0336[5];

    // Set bit [6] of address 0x0336 of the primary switch.
    // This bit is NOT self clearing.
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, SWITCH_MIB_CONTROL_REGISTER_ADDRESS, MIB_CONTROL_FREEZE_MIB_COUNTERS_MASK, &write0336[0]);
    writeByteToPrimarySwitch(write0336);
}


void unfreezePrimarySwitchMibCounters(void)
{
    U8   write0336[5];

    // Clear bit [6] of address 0x0336 of the primary switch.
    // This returns the controller's MIB operation back to "normal counter operation".
    build5ByteCommandSequence(PRIMARY_WRITE_COMMAND, SWITCH_MIB_CONTROL_REGISTER_ADDRESS, 0, &write0336[0]);
    writeByteToPrimarySwitch(write0336);
}


#endif // REMOVE KSZ "MIB" STUFF.


// Build a 40-bit/5-byte data sequence to interact with the PRIMARY KSZ9897 controller.
//
//                 |<-----                                        40 bits total  = 5 bytes                                 ----->|
//                 |     |<---         24-bit address = 00000000 + 16-bit address         --->| don't care |<---    DATA     --->|
// Register Read:  011   23 22 21 20 19 18 17 16 15 14 13 12 11 10 9  8  7  6  5  4  3  2  1  0   00000    D7 D6 D5 D4 D3 D2 D1 D0
// (write:         010   ...)
// The actual address is a 16-bit word, so the upper [23:16] bits are 0/don't care. Therefore the 40-bit/5-byte train ride looks like:
//
//                            |<---   16-bit address    --->| don't care |<---    DATA     --->|
//      YYY   0 0 0 0 0 0 0 0 X X X X X X X X X X X X X X X X  00000    D7 D6 D5 D4 D3 D2 D1 D0
// 
// Splitting up the 40-bits into the 5-byte set:
//
// 0 1 1 0 0 0 0 0 0 0 0 X X X X X X X X X X X X X X X X 0 0 0 0 0 D D D D D D D D
// 0110 0000 000X XXXX XXXX XXXX XXX0 0000 DDDD DDDD
// 01100000 000XXXXX XXXXXXXX XXX00000 DDDDDDDD
//
// The 16-bit register address is spread out among 3 bytes.
// Perform the appropriate shifting and masking.
static void build5ByteCommandSequence(U8 command, U16 address, U8 data, U8 *pBytes)
{
    pBytes[0]   = (U8)(command << COMMAND_FIELD_BIT_LEFT_SHIFT);
    pBytes[1]   = (U8)(address >> ADDRESS_PART_0_RIGHT_SHIFT);  // lower bits to be tossed; 1st 5 bits retained in lower part of the byte
    pBytes[2]   = (U8)(address >> ADDRESS_PART_1_RIGHT_SHIFT);  // lower bits to be tossed; the upper bits are "lost" when retained as 1 byte
    pBytes[3]   = (U8)(address << ADDRESS_PART_2_LEFT_SHIFT);   // lowest 3 bits retained in the upper part of the byte.
    pBytes[4]   = data;
    return;
}



// ALU:


// userReadAluPrimaryByIndex
//
// Command takes the form:
//
// read_alu_pri_by_index index=<0..4095>
//
//
// Output as json, of the form:
//
//
// {"CLI":{"COMMAND":"read_alu_pri_by_index","ARGUMENTS":"index=0","CODE":0,"RESULT":"SUCCESS",
//                                                                          "RESPONSE":{"VALID COUNT":0,
//                                                                                      "PORT FORWARD":"0x01",
//                                                                                      "KSZ_PORT":1,
//                                                                                      "DOWNSTREAM PORT":"1",
//                                                                                      "ELAPSED":123,
//                                                                                      "MAC":"11:22:33:44:55:66"}}}
//
//
void userReadAluPrimaryByIndex(char *pUserInput)
{
    char  *pArguments;
    char   pIndexString[32];
    U16    index;
    U8     pMac[6];
    U8     portForward;
    U8     kszPort;
    char *pDownstreamPort;
    U16    validCount;
    U32    startTick;
    U32    endTick;
    U32    elapsed;

    pArguments = &pUserInput[strlen(READ_ALU_PRI_BY_INDEX_CMD) + 1];
    memset(pIndexString, 0, 32);
    findComponentStringData(CLI_ARG_INDEX_STRING, pIndexString, pArguments);

    if (strlen(pIndexString) == 0) {
        printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s,%s}}}\r\n",
               CLI_STRING, COMMAND_STRING,   pUserInput,
                           ARGUMENTS_STRING, pArguments,
                           CODE_STRING,      0,
                           RESULT_STRING,    FAIL_STRING,
                           RESPONSE_STRING,  REASON_STRING, INVALID_PARAMETERS_STRING);
    } else {
        index = atoi(pIndexString);
        if (index <= MAX_PRIMARY_ALU_INDEX) {

            // Setup the ALU Table Index 0 and 1 registers. The last 2 registers of Index 1 hold the index value.
            //
            // NOTE: FOR READ-BY-INDEX, IT MAY NOT BE NECESSARY TO WRITE TO INDEX 0 REGISTER SET
            //
            startTick = g_ul_ms_ticks;

            // Use the general purpose function interface to read 1 ALU entry by index:
            if (isReadAluPrimaryControlByIndex(index, &validCount)) {
                portForward = readAluPrimaryPortForwardRegister();
                readAluPrimaryDataRegisters(&pMac[0]);

                // Validate and display the results.
                endTick = g_ul_ms_ticks;
                elapsed = endTick - startTick;

                // Which port? THIS SECTION IS REPEATED BELOW, SO MAKE IT A NEW FUNCTION.
                if      (portForward == 0x01) { kszPort = 1;    pDownstreamPort = (char *)&PORT_1_STRING[0];       }
                else if (portForward == 0x02) { kszPort = 2;    pDownstreamPort = (char *)&PORT_2_STRING[0];       }
                else if (portForward == 0x04) { kszPort = 3;    pDownstreamPort = (char *)&PORT_3_STRING[0];       }
                else if (portForward == 0x08) { kszPort = 4;    pDownstreamPort = (char *)&PORT_4_STRING[0];       }
                else if (portForward == 0x10) { kszPort = 5;    pDownstreamPort = (char *)&PORT_LAN_STRING[0];     }
                else if (portForward == 0x20) { kszPort = 6;    pDownstreamPort = (char *)&PORT_RGMII_STRING[0];   }
                else                          { kszPort = 0xFF; pDownstreamPort = (char *)&PORT_UNKNOWN_STRING[0]; }

                // Output the final result.
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%u,%s:\"%02X\",%s:%u,%s:\"%s\",%s:%lu,%s:\"%02X:%02X:%02X:%02X:%02X:%02X\"}}}\r\n",
                       CLI_STRING, COMMAND_STRING, READ_ALU_PRI_BY_INDEX_CMD,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESULT_STRING,    SUCCESS_STRING,
                                   RESPONSE_STRING,  VALID_COUNT_STRING,       validCount,
                                                     PORT_FORWARD_STRING,      portForward,
                                                     KSZ_PORT_STRING,          kszPort,
                                                     DOWNSTREAM_PORT_STRING,   pDownstreamPort,
                                                     ELAPSED_STRING,           elapsed,
                                                     MAC_STRING,               pMac[0], pMac[1], pMac[2], pMac[3], pMac[4], pMac[5]);
            } else {
                // Failed to read. Not observed during testing. Maybe weird things happen
                // in a very busy system with a large number of devices.
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s,%s}}}\r\n",
                       CLI_STRING, COMMAND_STRING,   pUserInput,
                                   ARGUMENTS_STRING, pArguments,
                                   CODE_STRING,      0,
                                   RESULT_STRING,    FAIL_STRING,
                                   RESPONSE_STRING,  REASON_STRING, PRI_ALU_CONTROL_REG_READ_LIMIT_STRING);
            }            
        } else {
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s,%s}}}\r\n",
                   CLI_STRING, COMMAND_STRING,   pUserInput,
                               ARGUMENTS_STRING, pArguments,
                               CODE_STRING,      0,
                               RESULT_STRING,    FAIL_STRING,
                               RESPONSE_STRING,  REASON_STRING, ALU_INDEX_OUT_OF_RANGE_STRING);
        }
    }    
}


// userReadAluPrimaryWithMacAsIndex
//
// Command takes the form:
//
// read_alu_pri_with_mac_as_index mac=11:22:33:44:55:66
//
//
// Output as json, of the form:
//
// {"CLI":{"COMMAND":"read_alu_pri_by_mac_as_index","ARGUMENTS":"mac=11:22:33:44:55:66","RESULT":{"VALID COUNT":0,"PORT FORWARD":"0x01","KSZ_PORT":1,"DOWNSTREAM PORT":"1","ELAPSED":123,"MAC":"11:22:33:44:55:66"}}}
//
void userReadAluPrimaryWithMacAsIndex(char *pUserInput)
{
    char  *pArguments;
    char   pMacAsString[32];
    U8     pMac[6];
    t_macAsIndexReadResults primaryReadResults;

    pArguments = &pUserInput[strlen(READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD) + 1];
    memset(pMacAsString, 0, 32);
    findComponentStringData(CLI_ARG_MAC_STRING, pMacAsString, pArguments);

    // A MAC string must be exactly these many characters, including the colons.
    if (strlen(pMacAsString) == MAX_CHARS_IN_MAC_STRING) {
        if (isConvertMacString(pMacAsString, pMac)) {

            readAluPrimaryWithMacAsIndex(&pMac[0], &primaryReadResults);

            // Output the final result.
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%u,%s:\"%02X\",%s:%u,%s:\"%s\",%s:%lu,%s:\"%02X:%02X:%02X:%02X:%02X:%02X\"}}}\r\n",
                     CLI_STRING, COMMAND_STRING,   READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD,
                                 ARGUMENTS_STRING, pArguments,
                                 CODE_STRING,      0,
                                 RESULT_STRING,    SUCCESS_STRING,
                                 RESPONSE_STRING,  VALID_COUNT_STRING,     primaryReadResults.validCount,
                                                   PORT_FORWARD_STRING,    primaryReadResults.portForward,
                                                   KSZ_PORT_STRING,        primaryReadResults.kszPort,
                                                   DOWNSTREAM_PORT_STRING, primaryReadResults.pDownstreamPort,
                                                   ELAPSED_STRING,         primaryReadResults.elapsed,
                                                   MAC_STRING, primaryReadResults.pMac[0], primaryReadResults.pMac[1], primaryReadResults.pMac[2],
                                                               primaryReadResults.pMac[3], primaryReadResults.pMac[4], primaryReadResults.pMac[5]);
        } else {
            printf("{%s:{%s:\"%s\",%s:%s,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,  pUserInput,         RESULT_STRING, FAIL_STRING,
                                                                         RESPONSE_STRING, INVALID_MAC_STRING, INFO_STRING,   NONE_STRING);
        }
    } else {
        printf("{%s:{%s:\"%s\",%s:%s,%s:%s,%s:%s}}\r\n", CLI_STRING, COMMAND_STRING,  pUserInput,                RESULT_STRING, FAIL_STRING,
                                                                     RESPONSE_STRING, INVALID_PARAMETERS_STRING, INFO_STRING,   NONE_STRING);
    }
}


// userReadAluPrimaryRange
//
// For debug/test/analysis only. Care must be taken not to enter too large a range,
// since this function enters a loop for the whole range, and never looks up, which
// can starve main from doing other useful work.
//
// THIS FUNCTION REALLY OUGHT TO BE A STATE MACHING, WHERE: - it outputs say 10 entries only,
//                                                          - waits for a CLI to display and empty the buffer
//                                                          - before going on to the next set of entries.
//
// On this basis, and until this function is converted to a state machine, a range of
// no more than 10 entries will be allowed.
void userReadAluPrimaryRange(char *pUserInput)
{
    char  *pArguments;
    char   pStartIndexString[32];
    char   pEndIndexString[32];
    U16    iggy;
    U16    startIndex;
    U16    endIndex;
    U16    validCount;
    U16    portForward;
    U8     pMac[6];

    pArguments = &pUserInput[strlen(READ_ALU_PRI_RANGE_CMD) + 1];
    memset(pStartIndexString, 0, 32);
    memset(pEndIndexString,   0, 32);

    findComponentStringData(CLI_ARG_START_STRING, pStartIndexString, pArguments);
    findComponentStringData(CLI_ARG_END_STRING,   pEndIndexString,   pArguments);

    if ((strlen(pStartIndexString) == 0) || (strlen(pEndIndexString) == 0)) {
        printf("{%s:{%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n", CLI_STRING, READ_ALU_PRI_RANGE_CMD, pUserInput,
                                                                          CODE_STRING,            0,
                                                                          RESULT_STRING,          FAIL_STRING,
                                                                          RESPONSE_STRING,        INFO_STRING, MISSING_ARGUMENTS_STRING);
    } else {
        startIndex = atoi(pStartIndexString);
        endIndex   = atoi(pEndIndexString);

        if (startIndex < endIndex) {
            if (endIndex - startIndex <= 10) {
                // Write the preamble, prepare the actual output as a list with the [ bracket.
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:[", CLI_STRING, COMMAND_STRING, READ_ALU_PRI_RANGE_CMD,
                                                                                    ARGUMENTS_STRING, pArguments,
                                                                                    CODE_STRING, 0,
                                                                                    RESULT_STRING, SUCCESS_STRING,
                                                                                    RESPONSE_STRING, INFO_STRING);
                for (iggy = startIndex; iggy <= endIndex; iggy++) {
                    // Use the general purpose function interface to read 1 ALU entry by index. Each row output if of the form:
                    // {"ROW":1,"VC":1,"PF":"0x1234","MAC":"01:02:03:04:05:06"}
                    if (isReadAluPrimaryControlByIndex(iggy, &validCount)) {
                        portForward = readAluPrimaryPortForwardRegister();
                        readAluPrimaryDataRegisters(&pMac[0]);
                        printf("{\"ROW\":%u,\"VC\":%u,\"PF\":\"0x%04X\",\"MAC\":\"%02X:%02X:%02X:%02X:%02X:%02X:\"}", iggy, validCount, portForward,
                                                                                                                      pMac[0], pMac[1], pMac[2], pMac[3], pMac[4], pMac[5]);
                    } else { printf("{\"ROW\":%u,\"ERROR\":\"isReadAluPrimaryControlByIndex RETURNED FALSE\"}", iggy); } // not observed during testing.
                    mdelay(10);
                    pokeWatchdog();
                } // for ...
                // Close off the overall output:
                printf("]}}}\r\n");
            } else {
                printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n", CLI_STRING, COMMAND_STRING,   READ_ALU_PRI_RANGE_CMD,
                                                                                      ARGUMENTS_STRING, pUserInput,
                                                                                      CODE_STRING,      0,
                                                                                      RESULT_STRING,    FAIL_STRING,
                                                                                      RESPONSE_STRING,  INFO_STRING, RANGE_TOO_LARGE_STRING);
            }
        } else {
            printf("{%s:{%s:\"%s\",%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n", CLI_STRING, COMMAND_STRING,       READ_ALU_PRI_RANGE_CMD, 
                                                                              ARGUMENTS_STRING,     pUserInput,
                                                                              CODE_STRING,          0,
                                                                              RESULT_STRING,        FAIL_STRING,
                                                                              RESPONSE_STRING,      INFO_STRING, END_INDEX_LESS_THAN_START_STRING);
        }
    }

}


// getPrimaryPortStringFromPortForward
//
//
//
char *getPrimaryPortStringFromPortForward(U8 portForward)
{
    char *pPrimaryPort;

    if      ((portForward & ALU_DATA_ENTRY_0427_PORT_1) == ALU_DATA_ENTRY_0427_PORT_1)   { pPrimaryPort = (char *) PORT_1_STRING;       }
    else if ((portForward & ALU_DATA_ENTRY_0427_PORT_2) == ALU_DATA_ENTRY_0427_PORT_2)   { pPrimaryPort = (char *) PORT_2_STRING;       }
    else if ((portForward & ALU_DATA_ENTRY_0427_PORT_3) == ALU_DATA_ENTRY_0427_PORT_3)   { pPrimaryPort = (char *) PORT_3_STRING;       }
    else if ((portForward & ALU_DATA_ENTRY_0427_PORT_4) == ALU_DATA_ENTRY_0427_PORT_4)   { pPrimaryPort = (char *) PORT_4_STRING;       }
    else if ((portForward & ALU_DATA_ENTRY_0427_PORT_5) == ALU_DATA_ENTRY_0427_PORT_5)   { pPrimaryPort = (char *) PORT_LAN_STRING;     }
    else if ((portForward & ALU_DATA_ENTRY_0427_PORT_6) == ALU_DATA_ENTRY_0427_PORT_6)   { pPrimaryPort = (char *) PORT_RGMII_STRING;   }
    else                                                                                 { pPrimaryPort = (char *) PORT_UNKNOWN_STRING; }

    return pPrimaryPort;
}


// isReadAluPrimaryControlByIndex
//
//
//
//
BOOL isReadAluPrimaryControlByIndex(U32 aluIndex, U16 *pValidCount)
{
    BOOL   readResult = FALSE;
    U8     control0Byte;
    U8     control1Byte;
    U8     writeRegisterValue;
    U8     readRegisterValue;
    U8     reg0x0418;
    U8     reg0x0419;
    U32    numberDataReads = 0;
    U32    loopCount       = 0;
    BOOL   keepChecking;

    //writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0410_REGISTER_ADDRESS, ALU_TABLE_INDEX_0410_DEFAULT_VALUE); // 0x0410 <- READ ONLY
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0411_REGISTER_ADDRESS, ALU_TABLE_INDEX_0411_DEFAULT_VALUE); // 0x0411
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0412_REGISTER_ADDRESS, ALU_TABLE_INDEX_0412_DEFAULT_VALUE); // 0x0412
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0413_REGISTER_ADDRESS, ALU_TABLE_INDEX_0413_DEFAULT_VALUE); // 0x0413

    control0Byte = aluIndex >> ALU_TABLE_INDEX_0416_INDEX_RIGHT_SHIFT; // The upper bits [31..16] ignored.
    control1Byte = aluIndex & ALU_TABLE_INDEX_0417_INDEX_MASK;         // The upper bits [31..16] ignored.
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0414_REGISTER_ADDRESS, ALU_TABLE_INDEX_0414_DEFAULT_VALUE); // 0x0414
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0415_REGISTER_ADDRESS, ALU_TABLE_INDEX_0415_DEFAULT_VALUE); // 0x0415
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0416_REGISTER_ADDRESS, control0Byte);                       // 0x0416
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0417_REGISTER_ADDRESS, control1Byte);                       // 0x0417

    // Setup the Access Control register set: the 1st 3 registers of this set are READ ONLY, so not written.
    // Only the 4th register need be setup.
    writeRegisterValue = ALU_ACCESS_CTRL_041B_ACTION_START_VALUE | ALU_ACCESS_CTRL_041B_BY_DIRECT_ADDR | ALU_ACCESS_CTRL_041B_READ_VALUE;
    writeByteToPrimaryByAddress(ALU_ACCESS_CTRL_041B_REGISTER_ADDRESS, writeRegisterValue); // 0x041B

    // Read access control register 0 and verify "action finished".
    keepChecking = TRUE;
    while (keepChecking) {
        readRegisterValue = readByteFromPrimaryByAddress(ALU_ACCESS_CTRL_041B_REGISTER_ADDRESS); // 0x041B
        numberDataReads++;
        if ((readRegisterValue & ALU_ACCESS_CTRL_041B_START_FINISH_MASK) == ALU_ACCESS_CTRL_041B_ACTION_FINISHED_VALUE) {
            keepChecking = FALSE;
            // Read access control registers (0x0418/0x0419) to retrieve the VALID_COUNT
            reg0x0418    = readByteFromPrimaryByAddress(ALU_ACCESS_CTRL_0418_REGISTER_ADDRESS); // 0x0418
            reg0x0419    = readByteFromPrimaryByAddress(ALU_ACCESS_CTRL_0419_REGISTER_ADDRESS); // 0x0419
            *pValidCount = (reg0x0418 << 8) | reg0x0419;
            readResult = TRUE;
            break;
        } else if (numberDataReads >= START_FINISH_READ_LIMIT) {
            keepChecking = FALSE;
            break;
        }

        // Bail if coming here too often ...
        if (loopCount++ > MAX_READ_LOOP_LIMIT) { incrementBadEventCounter(PRI_ALU_READ_READY_LOOP_LIMIT); break; }

        // Keep reading.
        mdelay(20);
        pokeWatchdog();
    } // while ...

    // The ALU control registers have been read out. The result of this read operation
    // are in register set 0x0420..0x042F: it is up to the caller of this function to
    // read those registers separately - assuming of course, that TRUE is returned.

    return readResult;
}


// readAluPrimaryPortForwardRegister
//
// Read register 0x027 of the primary ethernet controller:
//
// - it contains the port number, defined in documentation as "port forward", hence retained.
// - bits [6..0] is the port number
// - bit  [7]    is reserved
//
U8 readAluPrimaryPortForwardRegister(void)
{
    U8 portForwardValue;
    portForwardValue = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_0427_REGISTER_ADDRESS); // 0x0427
    return portForwardValue;
}



// readAluPrimaryDataRegisters
//
// The assumption is that the control registers 0x0410..0x0419 have been read out and
// processed, with the START_FINISH bit in 0x0418 cleared. So the set of data registers
// 0x0420..0x042F can now be read out. This register set contains:
//
// - "ALU Table Entry 1 Register" set (5.3.1.4): 0x0420 .. 0x0423: nothing useful, ignored.
// - "ALU Table Entry 2 Register" set (5.3.1.5): 0x0424 .. 0x0246: reserved, ignored
//                                                         0x0427: "port forward"
// - "ALU Table Entry 3 Register" set (5.3.1.6): 0x0428 .. 0x0429: reserved/FID: nothing useful, ignored
//                                               0x042A    0x042B: 1st  2 bytes of the MAC
//                                               0x042C    0x042F: last 4 bytes of the MAC
//
void readAluPrimaryDataRegisters(U8 *pMac)
{
    pMac[0]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042A_REGISTER_ADDRESS); // 0x042A
    pMac[1]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042B_REGISTER_ADDRESS); // 0x042B
    pMac[2]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042C_REGISTER_ADDRESS); // 0x042C
    pMac[3]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042D_REGISTER_ADDRESS); // 0x042D
    pMac[4]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042E_REGISTER_ADDRESS); // 0x042E
    pMac[5]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042F_REGISTER_ADDRESS); // 0x042F
    return;
}


// isConfigPrimaryStaticAluMac
//
// Configure the given MAC in the ALU table as static entry [0].
//
//
BOOL isConfigPrimaryStaticAluMac(U8 *pMac)
{
    U8   staticControlValue;
    BOOL keepChecking;
    BOOL configSuccess = FALSE;
    U32  loopCount = 0;

    // 1: Data setup. Write the data to the following 4 register sets
    // 1.1: Static address table 1 register set. Only STATIC is written (0x0420 bit[7]), 0x0421/0422/0423 are not written.
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_0420_REGISTER_ADDRESS, ALU_DATA_ENTRY_0420_STATIC_ENTRY);                    // 0x0420

    // 1.2: Static address table 2 register set. The "port forward" is in 0x0427 bits[6:0], 0x0424/0425/0426 are not written.
    //      In this case, the physical port on the AMU is the SBC/LAN port, which on the primary controller, is port: 5.
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_0427_REGISTER_ADDRESS, ALU_DATA_ENTRY_0427_PORT_5);                          // 0x0427

    // 1.3: Static address table 3 register set. The 1st 2 bytes of the MAC are in 0x042A and 0x042B, 0x0428/0429 are not written.
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_042A_REGISTER_ADDRESS, pMac[0]);                                             // 0x042A
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_042B_REGISTER_ADDRESS, pMac[1]);                                             // 0x042B

    // 1.4: Static address table 4 register set. These 4 registers contain the remaining 4 bytes of the MAC to write. 
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_042C_REGISTER_ADDRESS, pMac[2]);                                             // 0x042C
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_042D_REGISTER_ADDRESS, pMac[3]);                                             // 0x042D
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_042E_REGISTER_ADDRESS, pMac[4]);                                             // 0x042E
    writeByteToPrimaryByAddress(ALU_DATA_ENTRY_042F_REGISTER_ADDRESS, pMac[5]);                                             // 0x042F

    // 2: Control setup. Write to the Static Address Control register to command the controller with the above data.
    //    From the data sheet: a) Write the TABLE_INDEX field with the 4-bit index value   <- DEFAULT TO USING STATIC ENTRY [0] So WRITE 0.
    //                         b) Set the TABLE_SELECT bit to 0 to select the Static Address Table.
    //                         c) Set the ACTION bit to 0 to indicate a write operation.
    //                         d) Set the START_FINISH bit to 1 to initiate the operation.
    writeByteToPrimaryByAddress(ALU_STATIC_CONTROL_041D_REGISTER_ADDRESS, ALU_STATIC_CONTROL_041D_TABLE_INDEX_DEFAULT);     // 0x041D
    staticControlValue =   ALU_STATIC_CONTROL_041F_START_FINISH_START
                         | ALU_STATIC_CONTROL_041F_TABLE_SELECT_STATIC
                         | ALU_STATIC_CONTROL_041F_ACTION_WRITE;
    writeByteToPrimaryByAddress(ALU_STATIC_CONTROL_041F_REGISTER_ADDRESS, staticControlValue);                              // 0x041F

    // 3: Verify finish. Enter into a loop and check the START_FINISH bit in register 0x041F.
    keepChecking = TRUE;
    while (keepChecking) {
        staticControlValue = readByteFromPrimaryByAddress(ALU_STATIC_CONTROL_041F_REGISTER_ADDRESS);                        // 0x041F

        if ((staticControlValue & ALU_STATIC_CONTROL_041F_START_FINISH_MASK) == ALU_STATIC_CONTROL_041F_START_FINISH_DONE) {
            // Action finished.
            keepChecking  = FALSE;
            configSuccess = TRUE;
            break;
        }

        // Bail if coming here too often ...
        if (loopCount++ > MAX_READ_LOOP_LIMIT) { incrementBadEventCounter(PRI_ALU_READ_READY_LOOP_LIMIT); break; }

        // Not done,try again.
        mdelay(50);
    } // while ...                    

    return configSuccess;

}


// readPrimaryStaticInfo
//
//
//
// 5.3.2.2 Static Address Table Read Operation
// 1. Write to the Static Address and Reserved Multicast Table Control Register.                                       <- 0x041C..0x041F
//    a) Write the TABLE_INDEX field with the 4-bit index value.
//    b) Set the TABLE_SELECT bit to 0 to select the Static Address Table.
//    d) Set the START_FINISH bit to 1 to initiate the operation.
// 2. When the operation is complete, the START_FINISH bit will be cleared automatically.                              <- 0x041F only
//    a) Read the contents of the indexed entry from the:
//
//    - Static Address Table Entry 1 Register <- 0x0420..0x0423 VALID bit[31]            or: VALID        = 0x0420 bit[7]
//    - Static Address Table Entry 2 Register <- 0x0424..0x0427 PORT FORWARD bits[6:0]   or: PORT FORWARD = 0x0427 bits[6:0]
//    - Static Address Table Entry 3 Register <- 0x0428..0x042B 1st 2 bytes of the MAC   or: MAC[0,1]     = 0x042A and 0x042B
//    - Static Address Table Entry 4 Register <- 0x042C..0x042F last 4 bytes of the MAC  or: MAC[2,3,4,5] = 0x042C/042D/042E/042F
//
// Note: that the Apparent application normally only writes 1 entry to the static ALU table in [0],
//       but this interface accepts any other index.


// TODO: MIGHT NEED TO CONVERT THIS FUNCTION TO BOOL TO ACCOUNT FOR FAILURES FOR PROCESSING BY THE CALLER.


BOOL isReadPrimaryStaticInfo(U16 tableIndex, U8* pPortForward, U8* pMac)
{
    BOOL keepChecking;
    U8   someValue;
    U32  loopCount = 0;
    BOOL readResult = FALSE;

    // Setup the control register:
    // - TABLE_INDEX = 0                                       <- 0x041D
    // - START_FINISH = 1 TABLE_SELECT = STATIC  ACTION=READ   <- 0x-41F
    someValue = (U8)tableIndex & ALU_STATIC_CONTROL_041D_TABLE_INDEX_MASK;
    writeByteToPrimaryByAddress(ALU_STATIC_CONTROL_041D_REGISTER_ADDRESS, someValue);                                       // 0x041D

    someValue =   ALU_STATIC_CONTROL_041F_START_FINISH_START
                | ALU_STATIC_CONTROL_041F_TABLE_SELECT_STATIC
                | ALU_STATIC_CONTROL_041F_ACTION_READ;
    writeByteToPrimaryByAddress(ALU_STATIC_CONTROL_041F_REGISTER_ADDRESS, someValue);                                       // 0x041F

    // 3: Verify finish. Enter into a loop and check the START_FINISH bit in register 0x041F.
    keepChecking = TRUE;
    while (keepChecking) {
        someValue = readByteFromPrimaryByAddress(ALU_STATIC_CONTROL_041F_REGISTER_ADDRESS);                                 // 0x041F

        if ((someValue & ALU_STATIC_CONTROL_041F_START_FINISH_MASK) == ALU_STATIC_CONTROL_041F_START_FINISH_DONE) {
            // Action finished. Read out the port forward and MAC.
            //
            // - data in 0x0420..0x0423  is ignored, not read
            // - data in 0x0424..00x0426 is ignored, not read
            // - 0x0427 is read to get PORT FORWARD            <- re-use someValue and mask off the upper bits.
            // - data in 0x0428/0x0429   is ignored, not read
            // - data in 0x042A and 0x042B = MAC[0,1]
            // - data in 0x042C and 0x042F = MAC[2:5]
            someValue     = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_0427_REGISTER_ADDRESS);                             // 0x0427
            *pPortForward = someValue & ALU_DATA_ENTRY_0427_PORT_FORWARD_MASK;                                              // 0x0427
            pMac[0]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042A_REGISTER_ADDRESS);                             // 0x042A
            pMac[1]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042B_REGISTER_ADDRESS);                             // 0x042B
            pMac[2]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042C_REGISTER_ADDRESS);                             // 0x042C
            pMac[3]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042D_REGISTER_ADDRESS);                             // 0x042D
            pMac[4]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042E_REGISTER_ADDRESS);                             // 0x042E
            pMac[5]       = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042F_REGISTER_ADDRESS);                             // 0x042F

            keepChecking = FALSE;
            readResult   = TRUE;
            break;
        }

        // Bail if coming here too often ...
        if (loopCount++ > MAX_READ_LOOP_LIMIT) { incrementBadEventCounter(PRI_ALU_READ_READY_LOOP_LIMIT); break; }

        // Not done,try again.
        mdelay(50);
    } // while ...

    return readResult;

}




// readAluPrimaryWithMacAsIndex
//
// Prepare and read the set of register in the ALU subsystem of the primary ethernet controller.
// The intent is to extract the port number on which a device is directly connected to the
// primary ethernet controller. Typically this would be ports 1..4 if the device in on one of the
// 4 downstream ports 1..4, or the LAN port if the given MAC is that of the Gateway's LAN port,
// or if it is any other MAC that is on the secondary ethernet controller: it is on the RGMII port
// of the primary ethernet controller. In all other cases, the port is unknown.
//
// All extracted data is written to a structure pointed to by the pReadResults argument. It is left
// to the caller to do as it pleases with the data. 
//
void readAluPrimaryWithMacAsIndex(U8 *pMac, t_macAsIndexReadResults *pReadResults)
{
    U8     registerSetupValue;
    U8     registerReadyValue;
    U8     portForward;
    BOOL   keepChecking;
    U8     reg0x0418;
    U8     reg0x0419;
    U16    validCount = 0;
    U32    startTick;
    U32    endTick;
    U32    loopLimit = 0; // protect against excess reading in the loop below.

#if ENABLE_CRAFT_DEBUG == TRUE
    sprintf(&pCraftPortDebugData[0], "PRI ALU BY MAC: MAC=%02X:%02X:%02X:%02X:%02X:%02X  <- FIND THIS MAC\r\n", pMac[0], pMac[1], pMac[2], pMac[3], pMac[4], pMac[5]);
    UART1_WRITE_PACKET_MACRO
#endif

    // Setup the ALU Table Index 0 and 1 registers. The last 2 registers of Index 1 hold the index value.
    startTick = g_ul_ms_ticks;

    // Write NO OP in the ALU control register:
    registerSetupValue = ALU_ACCESS_CTRL_041B_ACTION_FINISHED_VALUE | ALU_ACCESS_CTRL_041B_NO_OP_VALUE;
    writeByteToPrimaryByAddress(ALU_ACCESS_CTRL_041B_REGISTER_ADDRESS, registerSetupValue);                       // 0x041B

    // Write the MAC to search for:
    //writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0410_REGISTER_ADDRESS, ALU_TABLE_INDEX_0_3_DEFAULT_VALUE);      // 0x0410 <- READ ONLY
    //writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0411_REGISTER_ADDRESS, ALU_TABLE_INDEX_0411_DEFAULT_VALUE);     // 0x0411 <- default 0
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0412_REGISTER_ADDRESS, pMac[0]);                                  // 0x0412
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0413_REGISTER_ADDRESS, pMac[1]);                                  // 0x0413
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0414_REGISTER_ADDRESS, pMac[2]);                                  // 0x0414
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0415_REGISTER_ADDRESS, pMac[3]);                                  // 0x0415
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0416_REGISTER_ADDRESS, pMac[4]);                                  // 0x0416
    writeByteToPrimaryByAddress(ALU_TABLE_INDEX_0417_REGISTER_ADDRESS, pMac[5]);                                  // 0x0417

    // Setup the Access Control register to initiate the search:
    //   - the 1st 3 registers of this set (0x0418, 0x0419, 0x041A) are READ ONLY, so not written.
    //   - only the 4th register is setup: HASHING (bit[2]) = 0 and ACTION (bit[1,0]) = b10
    //     By the way: ABSOLUTELY *DO NOT USE* ALU_ACCESS_CTRL_041B_SEARCH_VALUE (you've been warned).
    //     "SEARCH" and "READ" (ACTION bits[1,0] of 0x041B) mean very different things!!!
    // aluAccessCtrlHashing = ALU_ACCESS_CTRL_041B_BY_HASH_FUNCTION;
    // aluAccessCtrlAction  = ALU_ACCESS_CTRL_041B_READ_VALUE;

    // About this delay: 1: the 50msec delay is likely unnecessary.
    //                   2: anecdotally, the 2nd delay improves the results in locating the desired MAC. If the delay is omitted
    //                      or very short, on some systems (the MAC of the LAN port on MOREX especially), the MAC is never found
    //                      in the primary ALU. With the delay however, although there is sometimes a first-time failure to find
    //                      the desired MAC, it is always found on the 2nd and subsequent attempts.
    //
    //                      Conclusion: this aspect of the primary controller is ... buggy.
    mdelay(50);
    registerSetupValue = ALU_ACCESS_CTRL_041B_ACTION_START_VALUE | aluAccessCtrlHashing | aluAccessCtrlAction;
    writeByteToPrimaryByAddress(ALU_ACCESS_CTRL_041B_REGISTER_ADDRESS, registerSetupValue);                       // 0x041B
    mdelay(aluWaitActionBeforeRead);

    // Read access control register 0 and verify "action finished".
    keepChecking = TRUE;
    while (keepChecking) {
        registerReadyValue = readByteFromPrimaryByAddress(ALU_ACCESS_CTRL_041B_REGISTER_ADDRESS);                 // 0x041B

        if ((registerReadyValue & ALU_ACCESS_CTRL_041B_START_FINISH_MASK) == ALU_ACCESS_CTRL_041B_ACTION_FINISHED_VALUE) {
            // Action finished. Read the valid count, port forward and MAC from the data registers.
            // Note: VALID/bit[6] and SEARCH_END/BIT[5] are not verified, they presumably for action=search
            //       which is not used by the Apparent application.
            keepChecking = FALSE;

            reg0x0418  = readByteFromPrimaryByAddress(ALU_ACCESS_CTRL_0418_REGISTER_ADDRESS);                     // 0x0418
            reg0x0419  = readByteFromPrimaryByAddress(ALU_ACCESS_CTRL_0419_REGISTER_ADDRESS);                     // 0x0419
            validCount = (reg0x0418 << 8) | reg0x0419;

            // These registers are not read:
            //   - 0x0420..0x0423:       nothing useful, ignored.
            //   - 0x0424/0x0245/0x0246: reserved, ignored.
            //   - 0x0428/0x0429:    reserved/FID: nothing useful, ignored.
            portForward           = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_0427_REGISTER_ADDRESS);           // 0x0427
            pReadResults->pMac[0] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042A_REGISTER_ADDRESS);           // 0x042A
            pReadResults->pMac[1] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042B_REGISTER_ADDRESS);           // 0x042B
            pReadResults->pMac[2] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042C_REGISTER_ADDRESS);           // 0x042C
            pReadResults->pMac[3] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042D_REGISTER_ADDRESS);           // 0x042D
            pReadResults->pMac[4] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042E_REGISTER_ADDRESS);           // 0x042E
            pReadResults->pMac[5] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042F_REGISTER_ADDRESS);           // 0x042F
            endTick               = g_ul_ms_ticks;

            // If a valid port is indicated, the desired MAC is expected to have been retrieved.
            // Usually, only a single reading of the resulting MAC register set is required.
            pReadResults->secondAluReadRequired = FALSE;
            if (((portForward & ALU_DATA_ENTRY_0427_PORT_6) != ALU_DATA_ENTRY_0427_PORT_6) && (portForward != 0)) {
                // Is it the desired MAC?
                if (memcmp(&pReadResults->pMac[0], &pMac[0], 6) != 0) {
                    // It is not the desired MAC. THIS IS A KNOWN BUG: the work-around consists simply of re-reading the register set.

#if ENABLE_CRAFT_DEBUG == TRUE
                    sprintf(&pCraftPortDebugData[0], "PRI ALU BY MAC: PF/MAC=0x%02X/%02X:%02X:%02X:%02X:%02X:%02X  <- BAD MAC\r\n",
                                                      portForward, pReadResults->pMac[0], pReadResults->pMac[1], pReadResults->pMac[2],
                                                                   pReadResults->pMac[3], pReadResults->pMac[4], pReadResults->pMac[5]);
                    UART1_WRITE_PACKET_MACRO
#endif

                    mdelay(50); // Anecdotally and by observation, this delay is helpful. On the other hand,
                                // if CRAFT_DEBUG is enabled, there is an implied delay.
                    pReadResults->pMac[0] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042A_REGISTER_ADDRESS);           // 0x042A
                    pReadResults->pMac[1] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042B_REGISTER_ADDRESS);           // 0x042B
                    pReadResults->pMac[2] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042C_REGISTER_ADDRESS);           // 0x042C
                    pReadResults->pMac[3] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042D_REGISTER_ADDRESS);           // 0x042D
                    pReadResults->pMac[4] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042E_REGISTER_ADDRESS);           // 0x042E
                    pReadResults->pMac[5] = readByteFromPrimaryByAddress(ALU_DATA_ENTRY_042F_REGISTER_ADDRESS);           // 0x042F

                    // Since a 2nd read of the MAC registers was performed, then:
                     pReadResults->secondAluReadRequired = TRUE;

#if ENABLE_CRAFT_DEBUG == TRUE
                    sprintf(&pCraftPortDebugData[0], "PRI ALU BY MAC: PF/MAC=0x%02X/%02X:%02X:%02X:%02X:%02X:%02X  <- 2ND READ\r\n",
                                                     portForward, pReadResults->pMac[0], pReadResults->pMac[1], pReadResults->pMac[2],
                                                                  pReadResults->pMac[3], pReadResults->pMac[4], pReadResults->pMac[5]);
                    UART1_WRITE_PACKET_MACRO
#endif

                }                    
            }            

#if ENABLE_CRAFT_DEBUG == TRUE
            sprintf(&pCraftPortDebugData[0], "PRI ALU BY MAC: SETUP=0x%02X RDY=0x%02X PF/MAC=0x%02X/%02X:%02X:%02X:%02X:%02X:%02X  <- DONE\r\n",
                                                registerSetupValue, registerReadyValue, portForward, pReadResults->pMac[0], pReadResults->pMac[1], pReadResults->pMac[2],
                                                pReadResults->pMac[3], pReadResults->pMac[4], pReadResults->pMac[5]);
            UART1_WRITE_PACKET_MACRO
#endif

            // Which port the device is on is based on its bit position:
            if      ((portForward & ALU_DATA_ENTRY_0427_PORT_1) == ALU_DATA_ENTRY_0427_PORT_1) { pReadResults->kszPort = 1;           pReadResults->pDownstreamPort = (char *)&PORT_1_STRING[0];       }
            else if ((portForward & ALU_DATA_ENTRY_0427_PORT_2) == ALU_DATA_ENTRY_0427_PORT_2) { pReadResults->kszPort = 2;           pReadResults->pDownstreamPort = (char *)&PORT_2_STRING[0];       }
            else if ((portForward & ALU_DATA_ENTRY_0427_PORT_3) == ALU_DATA_ENTRY_0427_PORT_3) { pReadResults->kszPort = 3;           pReadResults->pDownstreamPort = (char *)&PORT_3_STRING[0];       }
            else if ((portForward & ALU_DATA_ENTRY_0427_PORT_4) == ALU_DATA_ENTRY_0427_PORT_4) { pReadResults->kszPort = 4;           pReadResults->pDownstreamPort = (char *)&PORT_4_STRING[0];       }
            else if ((portForward & ALU_DATA_ENTRY_0427_PORT_5) == ALU_DATA_ENTRY_0427_PORT_5) { pReadResults->kszPort = 5;           pReadResults->pDownstreamPort = (char *)&PORT_LAN_STRING[0];     }
            else if ((portForward & ALU_DATA_ENTRY_0427_PORT_6) == ALU_DATA_ENTRY_0427_PORT_6) { pReadResults->kszPort = 6;           pReadResults->pDownstreamPort = (char *)&PORT_RGMII_STRING[0];   }
            else                                                                               { pReadResults->kszPort = portForward; pReadResults->pDownstreamPort = (char *)&PORT_UNKNOWN_STRING[0]; }

            // Write the remaining results.
            pReadResults->validCount  = validCount;
            pReadResults->portForward = portForward;
            pReadResults->elapsed     = endTick - startTick;

            break;

        }

        // Prepare to read again. Never observed during debug/test. Most likely a bug.
        incrementBadEventCounter(PRI_ALU_READ_NOT_READY_DETECTED);
        mdelay(50); // not 10?
        pokeWatchdog();
        if ((loopLimit++) > aluAccessReadLoopLimit) { keepChecking = FALSE;  break; }

    } // while ...

    return;

}


// macResolutionParamUpdate
//
// For debugging only.
//
// {"CLI":{"COMMAND":"alu_mac_reso_param","ARGUMENTS":"loop_limit=10 alu_action=2 wait_action=10","CODE":0,"RESULT":"SUCCESS",
//         "RESPONSE":{"LOOP LIMIT (msec)":10,"ALU ACTION":2,"WAIT ACION":10}}}
void macResolutionParamUpdate(char *pUserInput)
{
    char  *pArguments;
    char   pLoopLimitString[32];
    char   pAluActionString[32];
    char   pWaitActionString[32];

    pArguments = &pUserInput[strlen(ALU_MAC_RESO_PARAM_CMD) + 1];
    memset(pLoopLimitString,  0, 32);
    memset(pAluActionString,  0, 32);
    memset(pWaitActionString, 0, 32);
    findComponentStringData(CLI_ARG_LOOP_LIMIT_STRING,  pLoopLimitString,  pArguments);
    findComponentStringData(CLI_ARG_ALU_ACTION_STRING,  pAluActionString,  pArguments);
    findComponentStringData(CLI_ARG_WAIT_ACTION_STRING, pWaitActionString, pArguments);

    if ((strlen(pLoopLimitString) == 0) || (strlen(pAluActionString) == 0) || (strlen(pWaitActionString) == 0)) {
        printf("{%s:{%s:\"%s\",%s:%u,%s:%s,%s:{%s:%s}}}\r\n", CLI_STRING, ALU_MAC_RESO_PARAM_CMD, pUserInput,
                                                                          CODE_STRING,            0,
                                                                          RESULT_STRING,          FAIL_STRING,
                                                                          RESPONSE_STRING,        INFO_STRING, MISSING_ARGUMENTS_STRING);
    } else {
        aluAccessReadLoopLimit  = atoi(pLoopLimitString);
        aluWaitActionBeforeRead = atoi(pWaitActionString);
        if      (strncmp(pAluActionString, CLI_ARG_NO_OP_STRING,  strlen(CLI_ARG_NO_OP_STRING))  == 0) { aluAccessCtrlAction = ALU_ACCESS_CTRL_041B_NO_OP_VALUE;  }
        else if (strncmp(pAluActionString, CLI_ARG_WRITE_STRING,  strlen(CLI_ARG_WRITE_STRING))  == 0) { aluAccessCtrlAction = ALU_ACCESS_CTRL_041B_WRITE_VALUE;  }
        else if (strncmp(pAluActionString, CLI_ARG_READ_STRING,   strlen(CLI_ARG_READ_STRING))   == 0) { aluAccessCtrlAction = ALU_ACCESS_CTRL_041B_READ_VALUE;   }
        else if (strncmp(pAluActionString, CLI_ARG_SEARCH_STRING, strlen(CLI_ARG_SEARCH_STRING)) == 0) { aluAccessCtrlAction = ALU_ACCESS_CTRL_041B_SEARCH_VALUE; }
        else                                                                                           { printf("INVAL ACTION %s\r\n",pAluActionString ); return; }
        printf("{%s:{%s:\"%s\",\"%s\":\"%s\",%s:%u,%s:%s,%s:{%s:%u,%s:\"0x%02X\",%s:%lu}}}\r\n",
               CLI_STRING, COMMAND_STRING, ALU_MAC_RESO_PARAM_CMD,
                           ARGUMENTS_STRING, pArguments,
                           CODE_STRING, 0,
                           RESULT_STRING, SUCCESS_STRING,
                           RESPONSE_STRING, LOOP_LIMIT_MSEC_STRING, aluAccessReadLoopLimit,
                                            ALU_ACTION_STRING,      aluAccessCtrlAction,
                                            WAIT_ACTION_STRING,     aluWaitActionBeforeRead);
   }    

    return;

}




/// @cond 0
/**INDENT-OFF**/
#ifdef __cplusplus
}
#endif
/**INDENT-ON**/
/// @endcond
