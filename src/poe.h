/**
* \file
*
* \brief Apparent POE Definitions.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _POE_H_
#define _POE_H_


// An 8-port data table will be defined where:
// port rank [0] [1] [2] [3] -> primary controller
// port rank [4] [5] [6] [7] -> secondary controller.
// Define the max port rank for the primary controller:
#define PRIMARY_PORT_RANK_MAX     3

#define MINI_JSON_STRING_SIZE 128


// POE info control block.
//
typedef struct s_poeInfoControlBlock
{
    U32  lastCheckPowerEnableTick;     // ensures the TPS POE registers are checked periodically
    U16  powerEnableRegister;          // placeholder for register value read out of the POE device
    U16  operatingModeRegister;        // operating mode register for downstream ports
    U16  temperatureRegister;          // register value with which to calculate temperature
    U16  padWithWord1;                 // keep data aligned to 32-bits
    F32  inputVoltage;                 // input voltage to the TPS device

    // Checking POE device temperature and input voltage.
    U32  lastCheckTemperatureTick;     // check the device temperature periodically
    U32  lastCheckVoltageTick;         // check the device input voltage periodically

    // Checking POE port power:
    U32  checkPortPowerTick;           // read port-pair voltage/current to calculate power
    U32  nextRankPortPairViToRead;     // rank of the next port-pair voltage/current to read

    // Checking POE auto class get-config data:
    U32  lastCheckAutoClassConfigTick; // periodically read POE auto class config
    U32  checkAutoClassConfigTimeout;  // timeout period before reading POE auto class config
    U32  nexRankPortPairAcToRead;      // rank of the next port pair auto class config to read

    // Checking POE PAC (Peak Average power Calculation):
    U16  pacCommandRegisterValue;      // keep track of the control command register value
    U16  padWithWord2;                 // keep data aligned to 32-bits
    U32  lastCheckPacTick;             // periodically launch POE PAC get-data
    U32  nexRankPortPairPacToRead;     // rank of the next port pair PAC to read
    U32  pacSmState;                   // state of the PAC state machine.
    U32  pacReadLoopCount;             // prevents stuck loop if control bits never clear

    // Checking POE auto class Status register:
    U32  lastCheckAutoClassStatusTick; // periodically read POE auto class status register
    U16  acStatusRegister;             // auto class status register as per section 9.6.2.24 of the data sheet (page 70)
    U16  padWithWord3;                 // helps to keep data aligned to 32-bits

    // Checking POE supply/status register:
    U32  lastCheckSupplyStatusTick;    // periodically read POE supply/status register
    U16  supplyStatusRegistervalue;    // value of the supply/fault register as per section 9.6.2.7 of the data sheet (page 42/43)
    U16  padWithWord4;                 // helps to keep data aligned to 32-bits

    // Checking the set of port-based event registers:
    U32  lastCheckEventRegistersTick;  // periodically read POE supply/status register
    U32  nextEventCommand;             // the next event command ID to use to read
    U16  powerEventRegisterValue;      // 
    U16  detectionEventRegisterValue;  // 
    U16  faultEventRegisterValue;      // 
    U16  startEventRegisterValue;      //
    U32  lastPowerChangeTick;          // tick when the power register last changed
    U32  lastDetectionChangeTick;      // tick when the detection register last changed
    U32  lastFaultChangeTick;          // tick when the fault register last changed
    U32  lastStartChangeTick;          // tick when the start register last changed

    // To control the state machine itself:
    U32  controllerCheckingState;      // state of the checkPoeControllerSm state machine
    U32  poeCheckingStartedTick;       // the tick once POE checking has been enabled.
    BOOL checkingEnabled;              // POE checking is enabled
    U8   padWithByte1;                 // helps to keep data aligned to 32-bits
    U16  padWithWord5;                 // helps to keep data aligned to 32-bits

    // For testing:
    BOOL poeDebugOutputEnabled;
    U8   padWithByte2;
    U8   padWithByte3;
} t_poeInfoControlBlock;

// Timers to control and guide the behavior of the POE controller checking component:
#define POE_CHECK_ALL_SETTLING_DOWN_TIMEOUT    10000       // check POE controller components after a settling down period: 10 seconds

#define POE_CHECK_POWER_ENABLE_TIMEOUT          5000       // write the power enable register every:      5 seconds
#define POE_CHECK_DEVICE_TEMPERATURE_TIMEOUT   30000       // check the POE device temperature every:    30 seconds
#define POE_CHECK_DEVICE_VOLTAGE_TIMEOUT       60000       // check the POE device input voltage every:  60 seconds (i.e., 1 minute)
#define POE_CHECK_PORT_POWER_TIMEOUT           30000       // read POE V/I for all 4 port-pairs every:   30 seconds
#define POE_CHECK_PAC_TIMEOUT                  60000       // check POE PAC for all 4 port-pairs every:  60 seconds (i.e., 1 minute)
#define POE_CHECK_AC_CONFIG_TIMEOUT           120000       // check POE Auto Class Config every:        120 seconds (i.e., 2 minutes)
#define POE_CHECK_AC_CONFIG_FAST_TIMEOUT        5000       // upon port status change: quickly check POE Auto Class Config within the next 5 seconds
#define POE_CHECK_AC_STATUS_TIMEOUT           120000       // check POE Auto Class Status every:        120 seconds (i.e., 2 minutes)
#define POE_CHECK_SUPPLY_STATUS_REG_TIMEOUT     5000       // check POE supply/status register every:     5 seconds
#define POE_CHECK_EVENT_REGISTERS_TIMEOUT       5000       // check POE event register set every:         5 seconds

#define POE_PAC_READ_LOOP_LIMIT                   30       // max number of reads when expecting PAC or AC register to clear


// Setting and waiting for PAC control bits to clear takes a long time.
// So does reading of the 4 PAC port par registers. So a state machine
// is called to return to main in a timely manner, so as to not hog a
// lot of CPU time.

// Define the states of the PAC state machine:
#define PAC_SM_IDLE                         1
#define PAC_SM_WAIT_CTRL_BITS_TO_CLEAR      2
#define PAC_SM_READ_PORT_PAIR_REGISTERS     3

// Define the states for the POE controller checking state machine:
#define PCC_SM_START                         1
#define PCC_SM_PRELIM_PHASE_1                2
#define PCC_SM_PRELIM_PHASE_2                3
#define PCC_SM_PRELIM_PHASE_3                4
#define PCC_SM_PRELIM_PHASE_4                5
#define PCC_SM_PRELIM_PHASE_5_POWER          6
#define PCC_SM_PRELIM_PHASE_5_DETECTION      7
#define PCC_SM_PRELIM_PHASE_5_FAULT          8
#define PCC_SM_PRELIM_PHASE_5_START          9
#define PCC_SM_IDLE                         10
#define PCC_SM_REWRITE_POWER_ENABLE         11
#define PCC_SM_READ_TEMPERATURE             12
#define PCC_SM_READ_INPUT_VOLTAGE           13
#define PCC_SM_PORT_POWER                   14
#define PCC_SM_PAC_SM                       15
#define PCC_SM_AUTO_CLASS_CONFIG            16
#define PCC_SM_AUTO_CLASS_STATUS            17
#define PCC_SM_SUPPLY_FAULT_REGISTER        18
#define PCC_SM_EVENT_REGISTERS              19

// From the Texas Instruments POE "TPS23882" data sheet, page 59:
//
// Set bit [5] for "register access Configuration B" to enable 16-bit register access in the GENERAL MASK command register.
// Set bit [3] for "Class change Enable bit" or "CLCHE":  enables classification changes in the DETECTION register 
// Set bit [2] for "Detect Change Enable bit" or "DECHE": enables cycle detection  changes in the DETECTION register
#define POE_N_BIT_ACC_MASK                  0b00100000
#define POE_CLCHE_MASK                      0b00001000
#define POE_DECHE_MASK                      0b00000100


// TPS23882 POE OPERATING MODE REGISTER (data sheet page 49).
//
// The 16-bit register is structured as follows:
//
//   |<-----------------------------------------------    16 bits    ------------------------------------------------>|
//   |  [15]   [14]   [13]   [12]   [11]   [10]   [9]    [8]    [7]    [6]    [5]    [4]    [3]    [2]    [1]    [0]  |
//   |  C4M1   C4M0   C3M1   C3M0   C2M1   C2M0   C1M1   C1M0   C8M1   C8M0   C7M1   C7M0   C6M1   C6M0   C5M1   C5M0 |
//   |  <- port 4->   <- port 3->   <- port 2->   <- port 1->   <- port 8->   <- port 7->   <- port 6->   <- port 5-> 

#define POE_OPERATING_MODE_OFF_MASK         0b00
#define POE_OPERATING_MODE_MANUAL_MASK      0b01
#define POE_OPERATING_MODE_SEMI_AUTO_MASK   0b10
#define POE_OPERATING_MODE_AUTOMATIC_MASK   0b11

// By default, downstream ports are set to automatic, so that on system startup, all connected ports
// will be powered. Only those ports that are specifically managed with power down are set to manual.
#define POE_OPERATING_MODE_DEFAULT_MASK     POE_OPERATING_MODE_AUTOMATIC_MASK

// Anything smart to say about this?
#define POE_OPERATING_MODE_PORT_4_SHIFT          14
#define POE_OPERATING_MODE_PORT_3_SHIFT          12
#define POE_OPERATING_MODE_PORT_2_SHIFT          10
#define POE_OPERATING_MODE_PORT_1_SHIFT           8
#define POE_OPERATING_MODE_PORT_8_SHIFT           6
#define POE_OPERATING_MODE_PORT_7_SHIFT           4
#define POE_OPERATING_MODE_PORT_6_SHIFT           2
#define POE_OPERATING_MODE_PORT_5_SHIFT           0

#define POE_OPERATING_MODE_PORT_4_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_4_SHIFT)
#define POE_OPERATING_MODE_PORT_3_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_3_SHIFT)
#define POE_OPERATING_MODE_PORT_2_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_2_SHIFT)
#define POE_OPERATING_MODE_PORT_1_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_1_SHIFT)
#define POE_OPERATING_MODE_PORT_8_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_8_SHIFT)
#define POE_OPERATING_MODE_PORT_7_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_7_SHIFT)
#define POE_OPERATING_MODE_PORT_6_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_6_SHIFT)
#define POE_OPERATING_MODE_PORT_5_DEFAULT        (POE_OPERATING_MODE_DEFAULT_MASK << POE_OPERATING_MODE_PORT_5_SHIFT)

#define POE_OPERATING_MODE_REGISTER_DEFAULT      (POE_OPERATING_MODE_PORT_4_DEFAULT | POE_OPERATING_MODE_PORT_3_DEFAULT | POE_OPERATING_MODE_PORT_2_DEFAULT | POE_OPERATING_MODE_PORT_1_DEFAULT | \
                                                  POE_OPERATING_MODE_PORT_8_DEFAULT | POE_OPERATING_MODE_PORT_7_DEFAULT | POE_OPERATING_MODE_PORT_6_DEFAULT | POE_OPERATING_MODE_PORT_5_DEFAULT )

// Bit settings for ports 1..8 to send a port into MANUAL mode.
#define POE_OPERATING_MODE_MANUAL_PORT_4         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_4_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_3         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_3_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_2         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_2_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_1         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_1_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_8         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_8_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_7         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_7_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_6         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_6_SHIFT)
#define POE_OPERATING_MODE_MANUAL_PORT_5         (POE_OPERATING_MODE_MANUAL_MASK << POE_OPERATING_MODE_PORT_5_SHIFT)

// Bit settings for ports 1..8 to send a port into AUTOMATIC mode.
#define POE_OPERATING_MODE_AUTOMATIC_PORT_4      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_4_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_3      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_3_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_2      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_2_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_1      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_1_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_8      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_8_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_7      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_7_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_6      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_6_SHIFT)
#define POE_OPERATING_MODE_AUTOMATIC_PORT_5      (POE_OPERATING_MODE_AUTOMATIC_MASK << POE_OPERATING_MODE_PORT_5_SHIFT)


// TPS23882 POE POWER ENABLE REGISTER (data sheet page 63).
//
// The 16-bit register is structured as follows:
//
//   |<-----------------------------------------    16 bits    ----------------------------------------->|
//   |  [15]  [14]  [13]  [12]  [11]  [10]   [9]   [8]    [7]   [6]   [5]   [4]   [3]   [2]   [1]   [0]  |                                    
//   | POFF4 POFF3 POFF2 POFF1 PWON4 PWON3 PWON2 PWON1 | POFF8 POFF7 POFF6 POFF5 PWON8 PWON7 PWON6 PWON5 |
//
// To enable/apply   power to any port 1..8: CLEAR POFFx bit to 0, SET   PWONx bit to 1
// To disable/remove power to any port 1..8: SET   POFFx bit to 1, CLEAR PWONx bit to 0

#define POE_POWER_ENABLE_PORT_4             0x0800
#define POE_POWER_ENABLE_PORT_3             0x0400
#define POE_POWER_ENABLE_PORT_2             0x0200
#define POE_POWER_ENABLE_PORT_1             0x0100
#define POE_POWER_ENABLE_PORT_8             0x0008
#define POE_POWER_ENABLE_PORT_7             0x0004
#define POE_POWER_ENABLE_PORT_6             0x0002
#define POE_POWER_ENABLE_PORT_5             0x0001

// To remove power from a port, set POFFx to 1 and PWONn to 0:
#define POE_POWER_DISABLE_PORT_4            0x8000
#define POE_POWER_DISABLE_PORT_3            0x4000
#define POE_POWER_DISABLE_PORT_2            0x2000
#define POE_POWER_DISABLE_PORT_1            0x1000
#define POE_POWER_DISABLE_PORT_8            0x0080
#define POE_POWER_DISABLE_PORT_7            0x0040
#define POE_POWER_DISABLE_PORT_6            0x0020
#define POE_POWER_DISABLE_PORT_5            0x0010

// In the apparent application, POWER ENABLE is the default power setting for all downstream ports.
#define POE_POWER_ENABLE_REGISTER_DEFAULT   (POE_POWER_ENABLE_PORT_4 | POE_POWER_ENABLE_PORT_3 | POE_POWER_ENABLE_PORT_2 | POE_POWER_ENABLE_PORT_1 | \
                                             POE_POWER_ENABLE_PORT_8 | POE_POWER_ENABLE_PORT_7 | POE_POWER_ENABLE_PORT_6 | POE_POWER_ENABLE_PORT_5)


// POE command codes:
#define POE_POWER_STATUS_COMMAND                 0x00000010L
#define POE_OPERATING_MODE_COMMAND               0x00000012L
#define POE_GENERAL_MASK_COMMAND                 0x00000017L
#define POE_POWER_ENABLE_COMMAND                 0x00000019L
#define POE_MFR_ID_COMMAND                       0x0000001BL
#define POE_TEMPERATURE_COMMAND                  0x0000002CL
#define POE_FIRMWARE_VERSION_COMMAND             0x00000041L
#define POE_DEVICE_ID_COMMAND                    0x00000043L

// Command code for POE TPS23882 device input voltage:
#define POE_INPUT_VOLTAGE_COMMAND                0x0000002EL

// Command codes for voltage/current readings for all 8 ports. All reads are based
// on 4-byte reads over I2C. Voltage pairs as specified in Table 9-24 have been
// demonstrated to be incorrect. See poeUserGetPowerReadings() function in poe.c module.
#define POE_VOLTAGE_PORT_1_5_COMMAND             0x00000032L
#define POE_VOLTAGE_PORT_2_6_COMMAND             0x00000036L
#define POE_VOLTAGE_PORT_3_7_COMMAND             0x0000003AL
#define POE_VOLTAGE_PORT_4_8_COMMAND             0x0000003EL

#define POE_CURRENT_PORT_1_5_COMMAND             0x00000030L
#define POE_CURRENT_PORT_2_6_COMMAND             0x00000034L
#define POE_CURRENT_PORT_3_7_COMMAND             0x00000038L
#define POE_CURRENT_PORT_4_8_COMMAND             0x0000003CL

// OR ... Command codes to read the 2-byte register voltage or current
// value for a single port.
#define POE_VOLTAGE_PORT_1_COMMAND               0x00000032L
#define POE_VOLTAGE_PORT_2_COMMAND               0x00000036L
#define POE_VOLTAGE_PORT_3_COMMAND               0x0000003AL
#define POE_VOLTAGE_PORT_4_COMMAND               0x0000003EL
#define POE_VOLTAGE_PORT_5_COMMAND               0x00000033L
#define POE_VOLTAGE_PORT_6_COMMAND               0x00000037L
#define POE_VOLTAGE_PORT_7_COMMAND               0x0000003BL
#define POE_VOLTAGE_PORT_8_COMMAND               0x0000003FL

#define POE_CURRENT_PORT_1_COMMAND               0x00000030L
#define POE_CURRENT_PORT_2_COMMAND               0x00000034L
#define POE_CURRENT_PORT_3_COMMAND               0x00000038L
#define POE_CURRENT_PORT_4_COMMAND               0x0000003CL
#define POE_CURRENT_PORT_5_COMMAND               0x00000031L
#define POE_CURRENT_PORT_6_COMMAND               0x00000035L
#define POE_CURRENT_PORT_7_COMMAND               0x00000039L
#define POE_CURRENT_PORT_8_COMMAND               0x0000003DL

// For Auto-class status (section 9.6.2.24 "Connection Check and Auto Class Status Register", page 70):
#define POE_AUTO_CLASS_STATUS_COMMAND       0x0000001CL

// For policing auto class config (section 9.6.2.25 "2-Pair Police CH-1 Configuration Register", page 71):
#define POE_POLICE_CONFIG_PORT_15_COMMAND        0x0000001EL
#define POE_POLICE_CONFIG_PORT_26_COMMAND        0x0000001FL
#define POE_POLICE_CONFIG_PORT_37_COMMAND        0x00000020L
#define POE_POLICE_CONFIG_PORT_48_COMMAND        0x00000021L




//
//
// POE EVENT REGISTER INFO SECTION (PAGE 38->43 OF THE DATA SHEET)
//
//

// POE command codes for the set of EVENT registers: - section 9.6.2.3 POWER EVENT Register
//                                                   - section 9.6.2.4 DETECTTION EVENT Register
//                                                   - section 9.6.2.5 FAULT EVENT Register
//                                                   - section 9.6.2.6 START/ILIM EVENT Register
//                                                   - section 9.6.2.7 SUPPLY and FAULT EVENT Register
//
// These commands are available in 2 categories: read-only and read-only-clear-on-read. 
// The above register sets are described in the data sheet as "1 Data Byte, Read only",
// and will return data for ports 4/3/2/1. In the Apparent application, a 1-word/16-bit
// read is used to get relevant data for the remaining ports 8/7/6/5.
//
// This read-only set is not used in the Apparent application:
#define POE_POWER_EVENT_RO_COMMAND               0x00000002L
#define POE_DETECTTION_EVENT_RO_COMMAND          0x00000004L
#define POE_FAULT_EVENT_RO_COMMAND               0x00000006L
#define POE_START_EVENT_RO_COMMAND               0x00000008L
#define POE_SUPPLY_RO_COMMAND                    0x0000000AL

// The set used by the Apparent application, i.e., "read-only-and-clear-on-read":
#define POE_POWER_EVENT_RO_CLEAR_COMMAND         0x00000003L
#define POE_DETECTION_EVENT_RO_CLEAR_COMMAND     0x00000005L
#define POE_FAULT_EVENT_RO_CLEAR_COMMAND         0x00000007L
#define POE_START_EVENT_RO_CLEAR_COMMAND         0x00000009L
#define POE_SUPPLY_RO_CLEAR_COMMAND              0x0000000BL

#define POE_EVENT_UNKNOWN_INDEX                            0
#define POE_EVENT_POWER_INDEX                              1
#define POE_EVENT_DETECTION_INDEX                          2
#define POE_EVENT_FAULT_INDEX                              3
#define POE_EVENT_START_INDEX                              4
#define POE_EVENT_SUPPLY_INDEX                             5
#define POE_EVENT_ALL_INDEX                                6 // For CLI use when all of the above are requested




// POWER EVENT REGISTER INFO
//
// The set of bits in the POWER event register, as per table 9-7 "POWER EVENT Register Field Descriptions" (page 38 of the data sheet):
//
//
// The register is read out of the POE device as a 16-bit word to contain the following data for all 8 ports:
#define POE_POWER_GOOD_STATUS_MASK                    0b1111000011110000
#define POE_POWER_GOOD_STATUS_PORT_8_MASK             0b1000000000000000
#define POE_POWER_GOOD_STATUS_PORT_7_MASK             0b0100000000000000
#define POE_POWER_GOOD_STATUS_PORT_6_MASK             0b0010000000000000
#define POE_POWER_GOOD_STATUS_PORT_5_MASK             0b0001000000000000
#define POE_POWER_GOOD_STATUS_PORT_4_MASK             0b0000000010000000
#define POE_POWER_GOOD_STATUS_PORT_3_MASK             0b0000000001000000
#define POE_POWER_GOOD_STATUS_PORT_2_MASK             0b0000000000100000
#define POE_POWER_GOOD_STATUS_PORT_1_MASK             0b0000000000010000

#define POE_POWER_ENABLE_STATUS_MASK                  0b0000111100001111
#define POE_POWER_ENABLE_STATUS_PORT_8_MASK           0b0000100000000000
#define POE_POWER_ENABLE_STATUS_PORT_7_MASK           0b0000010000000000
#define POE_POWER_ENABLE_STATUS_PORT_6_MASK           0b0000001000000000
#define POE_POWER_ENABLE_STATUS_PORT_5_MASK           0b0000000100000000
#define POE_POWER_ENABLE_STATUS_PORT_4_MASK           0b0000000000001000
#define POE_POWER_ENABLE_STATUS_PORT_3_MASK           0b0000000000000100
#define POE_POWER_ENABLE_STATUS_PORT_2_MASK           0b0000000000000010
#define POE_POWER_ENABLE_STATUS_PORT_1_MASK           0b0000000000000001




// DETECTION EVENT REGISTER INFO
//
// The set of bits in the DETECTION event register, as per table 9-8 "DETECTION EVENT Register Field Descriptions" (page 39 of the data sheet):
//
//
// The register is read out of the POE device as a 16-bit word to contain the following data for all 8 ports:
#define POE_DETECTION_CHANGE_OF_CLASS_MASK            0b1111000011110000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_8_MASK     0b1000000000000000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_7_MASK     0b0100000000000000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_6_MASK     0b0010000000000000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_5_MASK     0b0001000000000000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_4_MASK     0b0000000010000000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_3_MASK     0b0000000001000000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_2_MASK     0b0000000000100000
#define POE_DETECTION_CHANGE_OF_CLASS_PORT_1_MASK     0b0000000000010000

#define POE_DETECTION_CHANGE_IN_DETECTION_MASK        0b0000111100001111
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_8_MASK 0b0000100000000000
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_7_MASK 0b0000010000000000
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_6_MASK 0b0000001000000000
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_5_MASK 0b0000000100000000
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_4_MASK 0b0000000000001000
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_3_MASK 0b0000000000000100
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_2_MASK 0b0000000000000010
#define POE_DETECTION_CHANGE_IN_DETECTION_PORT_1_MASK 0b0000000000000001


// FAULT EVENT REGISTER INFO
//
// The set of bits in the FAULT event register, as per table 9-9 "FAULT EVENT Register Field Descriptions" (page 40 of the data sheet):
//
//
// The register is read out of the POE device as a 16-bit word to contain the following data for all 8 ports:
//
// DISCONNECT. A change in any of these bits indicates a plain port disconnect event occurred.
//             It usually clears, BUT PROBABLY BECAUSE THE READ-WITH-CLEAR-ON-READ command is used.
#define POE_FAULT_DISCONNECT_MASK                 0b1111000011110000
#define POE_FAULT_DISCONNECT_PORT_8_MASK          0b1000000000000000
#define POE_FAULT_DISCONNECT_PORT_7_MASK          0b0100000000000000
#define POE_FAULT_DISCONNECT_PORT_6_MASK          0b0010000000000000
#define POE_FAULT_DISCONNECT_PORT_5_MASK          0b0001000000000000
#define POE_FAULT_DISCONNECT_PORT_4_MASK          0b0000000010000000
#define POE_FAULT_DISCONNECT_PORT_3_MASK          0b0000000001000000
#define POE_FAULT_DISCONNECT_PORT_2_MASK          0b0000000000100000
#define POE_FAULT_DISCONNECT_PORT_1_MASK          0b0000000000010000

// OVERLOAD. A change in any of these bits indicates an overload fault occurred.
//           NOTE: not seen during testing, not sure if it clears/
#define POE_FAULT_OVERLOAD_MASK                   0b0000111100001111
#define POE_FAULT_OVERLOAD_PORT_8_MASK            0b0000100000000000
#define POE_FAULT_OVERLOAD_PORT_7_MASK            0b0000010000000000
#define POE_FAULT_OVERLOAD_PORT_6_MASK            0b0000001000000000
#define POE_FAULT_OVERLOAD_PORT_5_MASK            0b0000000100000000
#define POE_FAULT_OVERLOAD_PORT_4_MASK            0b0000000000001000
#define POE_FAULT_OVERLOAD_PORT_3_MASK            0b0000000000000100
#define POE_FAULT_OVERLOAD_PORT_2_MASK            0b0000000000000010
#define POE_FAULT_OVERLOAD_PORT_1_MASK            0b0000000000000001


// START/ILIM EVENT REGISTER INFO
//
// The set of bits in the START/ILIM event register, as per table 9-10 "START/ILIM EVENT Register Field Descriptions" (page 41 of the data sheet):
//
// Note: nowhere in the data sheet was the ILIM acronym described. But it seems to refer to "current limit" or "current limitation".
//
// The register is read out of the POE device as a 16-bit word to contain the following data for all 8 ports:
//
// ILIM: if set, indicates a fault occurred, so the channel has limited its output current. If set: expect sub-par operation
//       from the device. ILIM occupies bits[16,15,14,13] for ports 8,7,6,5 and bits[7,6,5,4] for ports 4,3,2,1. 
//       This bit set is a "bit complicated": if an ILIM bit is SET, then the PECn bit in the POWER EVEN register must be
//       read: if PECn/POWER EVENT is SET:   there was an "Inrush fault"
//             if PECn/POWER EVENT is CLEAR: fault cause is obtained from the POWER-ON FAULT/0x24 register.
//                                            
#define POE_START_ILIM_ILIM_MASK                 0b1111000011110000
#define POE_START_ILIM_PORT_8_ILIM_MASK          0b1000000000000000
#define POE_START_ILIM_PORT_7_ILIM_MASK          0b0100000000000000
#define POE_START_ILIM_PORT_6_ILIM_MASK          0b0010000000000000
#define POE_START_ILIM_PORT_5_ILIM_MASK          0b0001000000000000
#define POE_START_ILIM_PORT_4_ILIM_MASK          0b0000000010000000
#define POE_START_ILIM_PORT_3_ILIM_MASK          0b0000000001000000
#define POE_START_ILIM_PORT_2_ILIM_MASK          0b0000000000100000
#define POE_START_ILIM_PORT_1_ILIM_MASK          0b0000000000010000

// START: if set, indicates a fault or class/detect error on startup.
//        Typically: if 0/CLEAR = there is an operational device on the poirt
//                   if 1/SET   = the port is not equipped.
// All in all, the set of STARAT bits are more status then anything else.
#define POE_START_ILIM_START_MASK                0b0000111100001111
#define POE_START_ILIM_PORT_8_START_MASK         0b0000100000000000
#define POE_START_ILIM_PORT_7_START_MASK         0b0000010000000000
#define POE_START_ILIM_PORT_6_START_MASK         0b0000001000000000
#define POE_START_ILIM_PORT_5_START_MASK         0b0000000100000000
#define POE_START_ILIM_PORT_4_START_MASK         0b0000000000001000
#define POE_START_ILIM_PORT_3_START_MASK         0b0000000000000100
#define POE_START_ILIM_PORT_2_START_MASK         0b0000000000000010
#define POE_START_ILIM_PORT_1_START_MASK         0b0000000000000001


// POE DEVICE SUPPLY/FAULT EVENT REGISTER INFO
//
// The set of bits in the supply/fault event register, as per table 9-11 "SUPPLY and FAULT EVENT Register Field Descriptions" (page 42 of the data sheet):
//
// bit[7]:   TSD "thermal shutdown occurred"
// bit[6]:   VDUV "VDD UVLO occurred"
// bit[5]:   VDWRN "VDD UV Warning occurred"
// bit[4]:   VPUV "VPWR undervoltage occurred
// bit[3,2]: reserved
// bit[1]:   OSSE "OSS event occurred"
// bit[0]:   RAMFLT "SRAM fault occurred"
#define POE_SUPPLY_FAULT_TSD_MASK                0b10000000
#define POE_SUPPLY_FAULT_VDUV_MASK               0b01000000
#define POE_SUPPLY_FAULT_VDWRN_MASK              0b00100000
#define POE_SUPPLY_FAULT_VPUV_MASK               0b00010000
#define POE_SUPPLY_FAULT_RESERVED_MASK           0b00001100
#define POE_SUPPLY_FAULT_OSSE_MASK               0b00000010
#define POE_SUPPLY_FAULT_RAMFLT_MASK             0b00000001


//
//
// END OF THE ... POE EVENT REGISTER INFO SECTION
//
//





// Valid set of values for police config. With the exception of the "ZERO" class:
// -> during testing, it was observed that a connected SG424 inverter had a class
//    register value of 0, which does not match any of the values as specified in
//    table 9-35/page 72 of the data sheet. So the "ZERO" class was defined, though
//    not used in this application.
#define POE_AUTO_CLASS_1                    0b00001000
#define POE_AUTO_CLASS_2                    0b00001110
#define POE_AUTO_CLASS_3                    0b00011111
#define POE_AUTO_CLASS_4                    0b00111100
#define POE_AUTO_CLASS_NOT_POE              0b11111111
#define POE_AUTO_CLASS_ZERO                 0b00000000 // Apparent defined. But not used.

// Power thresholds (watts), per class:
#define POE_AUTO_CLASS_1_MIN_THRESHOLD       4.0
#define POE_AUTO_CLASS_2_MIN_THRESHOLD       7.0
#define POE_AUTO_CLASS_3_MIN_THRESHOLD      15.5
#define POE_AUTO_CLASS_4_MIN_THRESHOLD      30.0
#define POE_AUTO_CLASS_NO_THRESHOLD          0.0

#define POE_AUTO_CLAS_1_INDEX 0
#define POE_AUTO_CLAS_2_INDEX 1
#define POE_AUTO_CLAS_3_INDEX 2
#define POE_AUTO_CLAS_4_INDEX 3

// Auto Class Control (section 9.6.2.61 "AUTO CLASS CONTROL Register", page 95):
// - set the port bit in the control register, then wait for it to clear
// - then read the peak average power on a port-pair basis.
#define POE_PAC_CONTROL_COMMAND             0x00000050L
#define POE_PEAK_AVE_POWER_PORT_1_5_COMMAND 0x00000051L
#define POE_PEAK_AVE_POWER_PORT_2_6_COMMAND 0x00000052L
#define POE_PEAK_AVE_POWER_PORT_3_7_COMMAND 0x00000053L
#define POE_PEAK_AVE_POWER_PORT_4_8_COMMAND 0x00000054L

// From the data sheet (Table 9-58 "AUTO CLASS POWER Register Fields Descriptions", page 96):
#define POE_PAC_STEP_FACTOR                 0.5



// To support the POE per-port info readings:
typedef struct s_portPoeInfoElement
{
    U16  voltageRegRawValue;
    U16  amperageRegRawValue;
    F32  volts;
    F32  amps;
    F32  wattsPresent;
    F32  wattsPrevious;
    U8   acConfigPresent;
    U8   acConfigPrevious;
    U8   pacRegRawValuePresent;
    U8   pacRegRawValuePrevious;
} t_portPoeInfoElement;


// To support a look-up table with the POE commands to read port voltage and current. 
typedef struct s_viCommandsElement
{
    U32  voltageCommand;
    U32  currentCommand;
    U32  firstPortRank;
    U32  secondPortRank;
} t_viCommandsElement;


// To support a look-up table with the POE commands to read police auto class config register set.
typedef struct s_autoClassCommandsElement
{
    U32  autoClassConfigCommand;
    U32  firstPortRank;
    U32  secondPortRank;
} t_autoClassCommandsElement;


// To support a look-up table with the POE commands to read PAC port pair register set.
typedef struct s_pacCommandsElement
{
    U32  pacCommand;
    U32  firstPortRank;
    U32  secondPortRank;
} t_pacCommandsElement;


// Extracted from the hardware test script, to configure POE control:
#define TWISH1_CLOCK_ENABLE_VALUE           (0x00000200U)
#define TWIHS1_SCLK_FREQUENCY_VALUE         (0x00035353U)
#define TWIHS1_ENABLE_MM_DISABLE_OTHERS     (0x25002a24U)

// NOTE: the definition of the POE I2C address above is a bit of a mystery: both the H/W script and the data sheet specify a
//       zero/0x0 address, but this causes the "TWIHS_SR_NACK ... Not Acknowledged" bit to be set in the status register.
//       And although the H/W python script comments specifies a "zero" I2C address, the actual address used by the script is 0x20.
#define POE_CONTROL_I2C_ADDRESS             0b00100000

// To READ from the POE controller (MREAD bit must be set to 1):
#define POE_MMR_READ_SETUP                  TWIHS_MMR_MREAD | TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(POE_CONTROL_I2C_ADDRESS)

// To read 4 consecutive bytes:
#define POE_MMR_RW_0_SETUP                                    TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(POE_CONTROL_I2C_ADDRESS)
#define POE_MMR_RW_1_SETUP                  TWIHS_MMR_MREAD | TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(POE_CONTROL_I2C_ADDRESS)

// To WRITE to the POE controller (MREAD bit must be clear to 0):
#define TWIHS1_MMR_POE_WRITE_SETUP_1BYTE    (0x00000000 & (~TWIHS_MMR_MREAD)) | TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(POE_CONTROL_I2C_ADDRESS)


#define TI_POE_CONTROLLER_DEV_ID_EXPECTED   0x33
#define TI_POE_CONTROLLER_MFR_ID_EXPECTED   0x55
#define POE_PORT_POWER_UP                   1
#define POE_PORT_POWER_DOWN                 2


// When calculating the POE device temperature, use this formula (page 79 of TPS23882 data sheet) - an extra 0.5 is added for rounding up:
#define POE_TEMPERATURE_CALCULATION(regValue)         ((regValue*0.652) + 0.5 - 20)
#define POE_TEMERATURE_FORMULA_STRING       "T=-20 + N*0.652"

// To facilitate access to the POE device register set over the SAME70's "TWIHS two-wire interface": 
#define POE_REG_TWIHS_CR                    (uint32_t)&REG_TWIHS1_CR
#define POE_REG_TWIHS_MMR                   (uint32_t)&REG_TWIHS1_MMR
#define POE_REG_TWIHS_IADR                  (uint32_t)&REG_TWIHS1_IADR
#define POE_REG_TWIHS_STATUS                (uint32_t)&REG_TWIHS1_SR
#define POE_REG_TWIHS_RHOLD                 (uint32_t)&REG_TWIHS1_RHR
#define POE_REG_TWIHS_THOLD                 (uint32_t)&REG_TWIHS1_THR


// POE TPS DEVICE INPUT VOLTAGE
// 
// 14-bit Data conversion result of input voltage.
// The equation defining the voltage measured is:
// V = N � VSTEP
// Where VSTEP is defined below as well as the full scale value:
// Mode Full Scale Value VSTEP
// Any 60 V 3.662 mV
// MASK: if by 16-bit word (MSB lsb): 0x3FFF
//       if by byte: LSB mask = 0xFF, MSB mask = 0x3F
#define INPUT_VOLTAGE_WORD_MASK             0x3FFF
#define INPUT_VOLTAGE_VSTEP_FACTOR          3.662


// POE PER-PORT CURRENT:
//
//14-bit Data conversion result of current for channel n. The update rate is around once per 100 ms
//in powered state.
//The equation defining the current measured is:
//I = N � ISTEP
//Where ISTEP is defined below as well as the full scale value, according to the operating mode:
//Mode Full Scale Value ISTEP
//Powered and Classification 1.15 A (with0.255-? Rsense) 70.19 ?A
#define PORT_CURRENT_WORD_MASK              0x3FFF
#define PORT_POWER_I_STEP_FACTOR            70.19

// POE PER-PORT VOLTAGE:
// -> interpretation: same as for device input voltage from above <-
#define PORT_VOLTAGE_WORD_MASK              INPUT_VOLTAGE_WORD_MASK
#define PORT_VOLTAGE_VSTEP_FACTOR           INPUT_VOLTAGE_VSTEP_FACTOR

// External Function Prototypes

void  configurePoeController                (void);
void  enablePoeCheckingCmd                  (BOOL enable);
void  setPoeEnabled                         (BOOL enable);
void  poeUpdateTemperatureRegister          (void);
void  checkPoeControllerSm                  (void);
void  poePortPowerUpDown                    (char *pUserInput, U32 portCommand);
void  poeReadByte                           (char *pUserInput);
void  poeReadWord                           (char *pUserInput);
void  poeWriteByte                          (char *pUserInput);
void  poeWriteWord                          (char *pUserInput);
void  poeHighLevelGeneralInfo               (void);
void  displayPoeInfo                        (void);
void  displayPoeTemperature                 (void);
BOOL  isPoePortEnabled                      (U32 portIndex);
F32   getPoePortPower                       (U32 portIndex);
F32   getPoePortPac                         (U32 portIndex);
char *getACSupportStringFromPort            (U32 portIndex);
void  primaryPortLinkStatusChange           (U32 primaryPortRank,   U32 portStatusHighLevel);
void  secondaryPortLinkStatusChange         (U32 secondaryPortRank, U32 portStatusHighLevel);
U8    poeReadDeviceId                       (void);
U8    poeReadManufacturerId                 (void);
void  poeSetPortPowerUpDown                 (U8 portIndex, U32 powerAction);
void  poeReadControllerInputVoltage         (void);
void  togglePoeExtraInfoDisplay             (void);
void  poeDebugOutputOnOff                   (BOOL onOrOff);
void  userDisplayPoePortPower               (void);
void  userPoeReadPortEventRegister          (char *pUserInput);
void  userPoeReadSupplyEventRegister        (void);
void  setAutoClassConfigFastCheck           (void);


 
/*----------------------------------------------------------------------------*/
#endif   /* _POE_H_ */
