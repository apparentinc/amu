/**
* \file
*
* \brief Apparent Application Definitions.
*
* Copyright (c) 2021 Apparent, Inc.
*
*
*
* ----- Licensing info, if any, should go here. -----
*
*
*/

#ifndef _APPARENT_H_
#define _APPARENT_H_


// See the note in apparent.c regarding this odd include:
#include "ASF/sam/flash_efc/flash_efc.h"


// Is console output on system started enabled? Normally not defined
#undef CONSOLE_OUTPUT_ON_SYSTEM_STARTUP


// Exported globals from the apparent.c module:
extern volatile U32  amuOutputChecksum;
extern volatile U16  charsWrittenByInterrupt;

#define INCLUDE_CLI_FLASH_COMMANDS    FALSE
#define ENABLE_ALU_MAC_DEBUG          FALSE
#define ENABLE_CRAFT_DEBUG            FALSE

#define TRUE  true
#define FALSE false

#define CONF_BOARD_TWIHS0 1
#define CONF_BOARD_TWIHS1 1

// NOTE: the following "HIGH-LEVEL SAME70 INTERNAL FLASH INFO" section would normally
//       be in the flashninator.h header file. However, 2 other projects (BOOTSTRAP and
//       APPLICATION) have the identical information in their respective apparent.h header
//       files


// FLASH: the smallest block of flash that can be erased is: 16 pages. Attempting to erase anything
//        smaller simply fails on SAME70. On the other hand, setting the smallest block that can be
//        erased to 32 pages means that the signature zone will need to be significantly larger just
//        to hold a 32-bit long. So 16 pages is the best compromise.
//
// This provides a "flash holding block" size of: 512*16 = 8192 = 0x2000 bytes
// An upgrade holding block in RAM is declared with this size. This seems like
// a lot of RAM, however: plenty exists on the SAME70, and next to nothing else
// is going on in the bootloader anyway.
#define APPARENT_IFLASH_ERASE_PAGES              IFLASH_ERASE_PAGES_16
#define MAX_FLASH_ERASE_BLOCK_SIZE               IFLASH_PAGE_SIZE*16
#define MAX_FLASH_HOLDING_BLOCK_BYTES            MAX_FLASH_ERASE_BLOCK_SIZE
#define MAX_FLASH_HOLDING_BLOCK_LONGS            (MAX_FLASH_HOLDING_BLOCK_BYTES / 4)


// ----------------------------------------------------------------------------
//
// HIGH-LEVEL SAME70 INTERNAL FLASH INFO
//
// This small section is expected to be identical on the following 3 git repos:
//
// - AMU_BOOTSTRAP
// - AMU_BOOTLOADER
// - AMU
//
// Program signature values. They are used to determine if a program (app or boot)
// is valid in order to jump to it.
//
// It is good practice to use prime numbers in a situation like this. Prime numbers,
// 32-bit especially, can be found on the net. At random, the following were chosen:
//
// -> 3769807637 = 0xE0B2B315
// ->  940609807 = 0x3810910F
#define APPLICATION_SIGNATURE_VALUE         0xE0B2B315UL
#define BOOTLOADER_SIGNATURE_VALUE          0x3810910FUL
#define NULL_SIGNATURE_VALUE                0xFFFFFFFFUL


// Program signature addresses. They are located in sector[1] which starts at 0x20000.
// They are offset by 0x2000 bytes in flash, since that is the smallest chunk of flash
// that can be erased. Each will hold a single 32-bit "signature" as defined above. It
// seems like a waste that 0x2000 bytes (8k!!!) of memory will be used to store a single
// 32-bit number. When completing the final actions of a program upgrade, the final commit
// must be the smallest atomic action the CPU can perform, in this case: write a 32-bit
// number to flash.
#define APPLICATION_SIGNATURE_ADDRESS       0x00020000UL
#define BOOTLOADER_SIGNATURE_ADDRESS        0x00022000UL

// Define the limit of the flash address where the bootstrap zone/app-boot signature reside.
#define SIGNATURE_INFO_LIMIT_ADDRESS        BOOTLOADER_SIGNATURE_ADDRESS + MAX_FLASH_ERASE_BLOCK_SIZE


// Program load/jump addresses. They are located to start on a sector boundary.
// There are 16 sectors of flash, with each sector holding 0x20000 bytes of memory.
//
// - sector [0]      at 0x0      contains the bootstrap,
// - sector [1]      at 0x20000  contains the program signatures
// - sector [2..11]  at 0x40000  contains the primary AMU Apparent application = 10 sectors worth of flash = 0x140000bytes = 1.3Mbytes
// - sectors[12..15] at 0x180000 contains the bootloader                       =  4 sectors worth of flash = 0x080000bytes = 524 288 bytes
#define APP_PROGRAM_SECTORS_MAX            10
#define BOOT_PROGRAM_SECTORS_MAX            4

#define APPARENT_APPLICATION_ADDRESS        0x00040000UL
#define BOOTLOADER_FLASH_ADDRESS            0x00180000UL


// If commanded to jump to bootloader or to reset (hardware or software):
typedef struct s_jumpToBootOrResetControlBlock
{
    // Keep things aligned on a 32-bit boundary.
    // When commanded to jump-to-bootloader:
    U32  jumpToBootTimestamp;

    // When commanded to perform a SAME70 reset:
    U32  resetTimestamp;

    // Padding not required for trailing components.
    BOOL jumpToBootPlease;
    BOOL resetPlease;
    BOOL softwareResetPlease;
} t_jumpToBootOrResetControlBlock;

//
//
// END OF HIGH-LEVEL SAME70 INTERNAL FLASH INFO
//
// ----------------------------------------------------------------------------


// The SAME70 reset controller info block.
typedef struct s_same70ResetInfoDataBlock
{
    U32 reason;              //
    U32 status;              //
    U32 statusMasked;        //
} t_same70ResetInfoDataBlock;



// SAME70 reset reasons, based on "ASF/sam/utils/cmsis/same70/include/component/rstc.h" header file. Once the reset reason from
// the SAME70's RST register is evaluated, it is mapped to one of the following with a string to be reported to the SBC/Gateway.
#define RESET_REASON_UNKNOWN           1 // in case the reset reason fails to map to the following set
#define RESET_REASON_GENERAL           2 // RSTC_SR_RSTTYP_GENERAL_RST (0x0u << 8) /**< \brief (RSTC_SR) First power-up reset */
#define RESET_REASON_BACKUP            3 // RSTC_SR_RSTTYP_BACKUP_RST  (0x1u << 8) /**< \brief (RSTC_SR) Return from Backup Mode */
#define RESET_REASON_WATCHDOG          4 // RSTC_SR_RSTTYP_WDT_RST     (0x2u << 8) /**< \brief (RSTC_SR) Watchdog fault occurred */
#define RESET_REASON_SOFTWARE          5 // RSTC_SR_RSTTYP_SOFT_RST    (0x3u << 8) /**< \brief (RSTC_SR) Processor reset required by the software */
#define RESET_REASON_NRST_LOW          6 // RSTC_SR_RSTTYP_USER_RST    (0x4u << 8) /**< \brief (RSTC_SR) NRST pin detected low */

// The value of the watchdog control restart register:
#define WATCHDOG_CONTROL_RESTART       (U32)( WDT_CR_KEY_PASSWD | WDT_CR_WDRSTT )


// ----------------------------------------------------------------------------
//
// Apparent High-level Application COMPILE SWITCHES:

// This allow the USER SIGNATURE to be erased and re-flashed. Generally, this is set to FALSE,
// so the user is prevented from erasing it. In the event it has been incorrectly flashed to
// begin with, changing it requires that it be erased before it can be re-programmed. So a
// special build would need to be applied to erase before the re-programing. 
#define USER_SIGNATURE_REFLASH                   TRUE

// The original action of pushbutton 1 (associated to contact closure #1) now removed.
#define PUSH_BUTTON_1_ACTION_ENABLED             FALSE

// END OF SWITCHES ...

//
// ----------------------------------------------------------------------------


// How often should the relay/LED flash when enabled:
#define RELAY_FLASH_DELAY                    10000

#define APPARENT_SYSTEM_HEADER \
"\r\n\r\n---------------------------------------------------\r\n" \
"--\r\n-- Apparent Gateway Peripheral PBCA Management Unit\r\n" \
"-- Copyright 2023 Apparent, Inc.\r\n" \
"-- Compiled:    "__DATE__" "__TIME__"\r\n"

// The hardware revision is read out from the PCBA Option Jumpers. A set of 8 GPIO pins are tied
// to ground via resistors, which together in the following structure form the hardware revision.
// The revision has 2 components: a major and a minor, displayed to the user as:
// (example): rev 1.0 where: - "rev " is pre-pended
//                           - "1" is the major component, and
//                           - "0" is the minor component.
typedef struct s_hardwareRevision
{
    U8 major;
    U8 minor;
} t_hardwareRevision;


// Until the group decides what the release/version should look like, it's just a number based on the COMMIT NOTES.
#define APPARENT_RELEASE_TITLE_STRING  "-- Apparent Inc. firmware release R"
#define APPARENT_RELEASE_STRING        "96"


// To support the bad events counters.
typedef enum {
    UART0_INPUT_BUFFER_OVERFLOW              =  0,         // <-----------
    UART1_INPUT_BUFFER_OVERFLOW              =  1,         //            |
    UART0_INPUT_BUFFER_ABOVE_SAFETY_MARGIN   =  2,         //            |
    UART1_INPUT_BUFFER_ABOVE_SAFETY_MARGIN   =  3,         //            |
    INPUT_CHECKSUM_GENERAL_ERROR             =  4,         //     common on bootloader and application
    MISSING_END_OF_INPUT_SIGNATURE           =  5,         //            |
    INVALID_END_OF_INPUT_FORMAT              =  6,         //            |
    SBC_INPUT_WHILE_PROCESSING               =  7,         //            |
    INVAL_COUNTER_TO_INCREMENT               =  8,         // <-----------

    // The remaining counters are used by the application.
    AUTONOMOUS_EVENT_STRING_LIMIT_REACHED    =  9,
    AUTO_STRING_EXCEEDS_SAFETY_MARGIN        = 10,
    FAILED_FLASH_USER_SIGNATURE_READ         = 11,
    FAILED_FLASH_USER_SIGNATURE_WRITE        = 12,
    FAILED_FLASH_USER_SIGNATURE_ERASE        = 13,
    PHY0_DATA_PRESENT_DURING_STARTUP         = 14,
    PHY0_DATA_PRESENT_BEFORE_READ            = 15,
    PHY0_DATA_PRESENT_AFTER_READ             = 16,
    PHY0_DATA_PRESENT_BEFORE_WRITE           = 17,
    PHY0_DATA_PRESENT_AFTER_WRITE            = 18,
    PHY0_EMPTY_CHECK_POTENTIAL_STUCK_LOOP    = 19,
    PHY2_DATA_PRESENT_DURING_STARTUP         = 20,
    PHY2_DATA_PRESENT_BEFORE_READ            = 21,
    PHY2_DATA_PRESENT_AFTER_READ             = 22,
    PHY2_DATA_PRESENT_BEFORE_WRITE           = 23,
    PHY2_DATA_PRESENT_AFTER_WRITE            = 24,
    PHY2_EMPTY_CHECK_POTENTIAL_STUCK_LOOP    = 25,
    POE_AUTO_STRING_EXCEEDS_SAFETY_MARGIN    = 26,
    I2C_LOOP_STATUS_READ_LIMIT_REACHED       = 27,
    POE_PAC_LOOP_READY_LIMIT_REACHED         = 28,
    POE_AC_STATUS_0_CHANGE_JSON_STRING       = 29,
    POE_POWER_THRESHOLD_EVENT_LIMIT_REACHED  = 30,
    PRIMARY_ANY_STATUS_BITS_CHANGED          = 31,
    PRIMARY_ETHERNET_SERVICE_RE_ENABLED      = 32,
    SECONDARY_ETHERNET_SERVICE_RE_ENABLED    = 33,
    INVALID_PRIMARY_PORT_NUMBER              = 34,
    ERRATA_VERIFY_ERROR_PRIMARY_CONTROLLER   = 35,
    ERRATA_VERIFY_ERROR_SECONDARY_CONTROLLER = 36,
    POE_CHECK_CONTROLLER_SM_ERROR            = 37,
    CC_INPUT_STATUS_CHECK_CODING_ERROR       = 38,
    INVALID_PRIMARY_MIB_INDEX                = 39,    // commented out from the code
    INVALID_PORT_IN_GET_MIB_COUNTER          = 40,    // commented out from the code
    INVALID_MIB_AS_HEX_IN_GET_MIB_COUNTER    = 41,    // commented out from the code
    PRIMARY_CONTROL_RESTORE_FAILED           = 42,
    SECONDARY_CONTROL_02_RESTORE_FAILED      = 43,
    SECONDARY_CONTROL_09_RESTORE_FAILED      = 44,
    SECONDARY_CONTROL_10_RESTORE_FAILED      = 45,
    SECONDARY_CONTROL_11_RESTORE_FAILED      = 46,
    PORT_STATUS_2_AUTO_NEGOTIATION_FLIP      = 47,
    UNEXPECTED_POE_DEVICE_ID                 = 48,
    UNEXPECTED_POE_MANUFACTURER_ID           = 49,
    CHECK_ETHERNET_PORTS_SM_CODING_ERROR     = 50,
    POE_PAC_SM_CODING_ERROR                  = 51,
    CHECK_MAC_RESOLUTION_SM_CODING_ERROR     = 52,
    WRITE_U11_FAN_SPEED_CODING_ERROR         = 53,
    POE_STARTUP_CONFIG_ERROR                 = 54,
    PRIMARY_CONTROLLER_CONFIG_ERROR          = 55,
    SECONDARY_CONTROLLER_CONFIG_ERROR        = 56,
    CHECK_POE_PORT_EVENT_CODING_ERROR        = 57,
    SECONDARY_ENABLED_ON_STARTUP             = 58,
    PRI_ALU_CONTROL_REG_READ_LIMIT           = 59,
    PRI_ALU_READ_NOT_READY_DETECTED          = 60,
    SEC_ALU_READ_READY_LOOP_LIMIT            = 61,
    PRI_ALU_READ_READY_LOOP_LIMIT            = 62,
    MAX_BAD_EVENTS_COUNTERS                  = 63  // <- always last, reflecting list size.
} e_bad_events_counters;


// MAC chars in a MAC string including the colons.
#define MAX_CHARS_IN_MAC_STRING        17


/*----------------------------------------------------------------------------*/

// 2 plain serial ports over micro USB: - SBCCOM port interface over J21
//                                      - CRAFT port interface over J50
//
// By default: standard I/O IS OVER THE SBC COM/J21 INTERFACE.
#define CONSOLE_UART         UART0
#define CONSOLE_UART_ID      ID_UART0
#define STDIO_UART           (usart_if)UART0


// Input buffer for the SCB COM and Craft/debug ports. The size of the SBC COM port is set to the same as for the
// BOOTLOADER program, which originally required at least 512 bytes + a few extra overhead bytes for the firmware
// upgrade feature. This resulted in a rather lengthy time to complete a firmware upgrade, about 12 minutes or more.
// Increasing the size of the SBC COM buffer dramatically reduced upgrade time, now less than 2 minutes.
#define SBCCOM_INPUT_BUFFER_LENGTH              2048    // increased from the original 600 bytes
#define SBCCOM_INPUT_BUFFER_SAFETY_MARGIN       1946    // about 95% of the buffer
#define CRAFT_INPUT_BUFFER_LENGTH                256    // increased from 128
#define CRAFT_INPUT_BUFFER_SAFETY_MARGIN         244    // about 95% of the buffer
#define UPGRADE_PACKET_DATA_SIZE_MAX            1848    // increased from the original 512 bytes

// The max length of a CLI input argument component. There is no practical limit on the
// number of input arguments other than the above max buffer length.
#define AMU_COMPONENT_DATA_SIZE_MAX    32


// A prime number I found in the bottom of my lunch bag.
#define APPARENT_USER_SIG_VALIDATED      0x98765FD1

// Data structure for Apparent "basic AMU info" data stored in SAME70 flash "User Signature".
// 512 bytes: - 1st 4 bytes    = validation signature
//            - next 504 chars = a single string (currently: only the serial number is written, + anything else that is to be added)
//                               so = 512 - 4 bytes (validation signature) - 4 (crc at end of block)
//            - last 4 bytes   = 32-bit CRC over the previous 508 bytes
//
// The FLASH_USER_SIG_SIZE declaration provided by the SAME70 EFC component is set to 512.
// Care must be taken accessing EFC facilities which expect sizing in longs, not bytes!
#define FLASH_USER_SIG_SIZE_IN_BYTES        FLASH_USER_SIG_SIZE
#define FLASH_USER_SIG_SIZE_IN_LONGS        FLASH_USER_SIG_SIZE / sizeof(U32)
#define USER_SIGNATURE_CRC_SIZE             FLASH_USER_SIG_SIZE - sizeof(U32)
#define USER_SIGNATURE_DATA_SIZE_BYTES      FLASH_USER_SIG_SIZE - sizeof(U32) - sizeof(U32)

typedef struct s_apparentUserSignature
{
    U32  signatureValidation;
    char apparentData[USER_SIGNATURE_DATA_SIZE_BYTES];
    U32  calculatedCrc;
} t_apparentUserSignature; 


#define CRC_32BIT_SEED       0x00000000

#define SUSPEND    1
#define RESUME     2

#define ENABLE_DISABLE_IS_BY_COMMAND   1
#define ENABLE_DISABLE_INTERNALLY      2

#define PORT_COMMAND_POWER_UP          1
#define PORT_COMMAND_POWER_DOWN        2
#define PORT_COMMAND_POWER_CYCLE       3
#define PORT_COMMAND_ETH_ONLY_UP       4
#define PORT_COMMAND_ETH_ONLY_DOWN     5

#define JSON_OUTPUT                    TRUE
#define NO_JSON_OUTPUT                 FALSE

#define NUM_OUTPUT_CONTACT_CLOSURES    4
#define NUM_INPUT_CONTACT_CLOSURES     4

#define OUTPUT_CC_OPEN                 0
#define OUTPUT_CC_CLOSE                1

// As per the "pin level value" when the GPIO is read out:
#define INPUT_CC_OPEN                  0
#define INPUT_CC_CLOSE                 1

// The input contact closures:
// - checking: about once every 100msec seems about right.
// - debounce: should remain steady for at least 500msec
// Note: these values could be made tighter, but there is little point doing so on the AMU,
//       since the SBC/Gateway checks the autonomous event facility every 5 seconds or so.
#define CONTACT_CLOSURE_CHECK_FREQUENCY     100
#define CONTACT_CLOSURE_DEBOUNCE_TIMEOUT    500

// When checking a change in input contact closure status:
// - IDLE:     stay in this state until the timer expires before checking the status
// - CHECK:    has the status changed since the last time? If not: back to IDLE, else: go to DEBOUNCE
// - DEBOUNCE: wait in this state until the debounce timer expires: check if the status is changed
//             before it is committed in the control block and reported as an autonomous event.
// debounce period, waiting for a status change to settle down.
#define INPUT_CC_CHECK_STATUS_SM_IDLE         1
#define INPUT_CC_CHECK_STATUS_SM_CHECK        2
#define INPUT_CC_CHECK_STATUS_SM_DEBOUNCE     3

// An output contact closures control block will be allocated for all 4 contacts.
// Each contact has its configuration as OPEN/CLOSED, and the gpio that controls it.
typedef struct s_outputContactClosureInfo
{
    U32  config;
    U32  gpio;
} t_outputContactClosureInfo;

// An input contact closures control block will be allocated for all 4 contacts.
// Each contact has its status as OPEN/CLOSED, and the gpio to check it.
typedef struct s_inputContactClosureInfo
{
    U32  status;
    U32  gpio;
    U32  smState;
    U32  lastTickCheck;
} t_inputContactClosureInfo;


// TEMPERATURE AND FAN CONTROL DATA.

// 3 instances where a temperature reading might be called for:
//
// - from the command line as a cli command: full output to standard I/O, including the SAME70 registers that are accessed.
// - as part of the hello processing: output the temperature data as a simple json-like string with the C/F temperature reading.
// - for periodic checking for fan speed control (called from main): output nothing to standard I/O.
#define OUTPUT_FULL_REG_ACCESS                  1
#define OUTPUT_TEMPERATURE_ONLY                 2
#define OUTPUT_NOTHING                          3


// To protect against an infinite loop when re-reading the status register of an I2C link while waiting for
// the "RXRDY bit to be set (after which the holding register is ready to be read), limit the number of
// status reads before giving up.
#define I2C_STATUS_READ_LOOP_LIMIT_PROTECTION    5


// SAME70 Register values for temperature reading and fan control.
//
// The following bits are to be SET in the TWIHS0 master mode register (from twihs.h header file):
// - set the master read direction
// - set the size of the I2C address (1 byte)
// - set the I2C address
//
// For the temperature sensor: I2C address = 1001000 (as per the schematic)
// For the fan control device: I2C address = 0011011 (as per the schematic)
#define TEMP_SENSOR_I2C_ADDRESS                  0b01001000
#define FAN_CONTROL_I2C_ADDRESS                  0b00011011
#define TWIHS0_MMR_READ_TEMPERATURE_SETUP        TWIHS_MMR_MREAD | TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(TEMP_SENSOR_I2C_ADDRESS)

// Control register bits setting to start the read process:
#define TEMP_SENSOR_CR_SETUP (TWIHS_CR_FIFODIS | TWIHS_CR_ACMDIS | TWIHS_CR_PECDIS | TWIHS_CR_SMBDIS | TWIHS_CR_HSDIS | TWIHS_CR_SVDIS | TWIHS_CR_MSEN)
    
// To read the FAN Manufacturer ID, command is 0x07 and the (read value) ID = 0x54.
// These values come straight out of the hardware test script, but nothing is done with these values.
#define U11_FAN_MFR_ID_READ_COMMAND              0x00000007L
#define U11_FAN_VERSION_ID_READ_COMMAND          0x00000008L
#define U11_FAN_STATUS_READ_COMMAND              0x00000005L
#define U11_FAN_CONFIG_READ_COMMAND              0x00000004L
#define U11_FAN_CONFIG_WRITE_COMMAND             U11_FAN_CONFIG_READ_COMMAND

// To READ from the fan controller (MREAD bit must be set to 1):
#define U11_FAN_MMR_READ_SETUP                   TWIHS_MMR_MREAD | TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(FAN_CONTROL_I2C_ADDRESS)

// To WRITE to the fan controller (MREAD bit must be clear to 0):
#define TWIHS0_MMR_FAN_WRITE_SETUP               (0x00000000 & (~TWIHS_MMR_MREAD)) | TWIHS_MMR_IADRSZ_1_BYTE | TWIHS_MMR_DADR(FAN_CONTROL_I2C_ADDRESS)

#define U11_FAN_COMMAND_START_STOP               (TWIHS_CR_START | TWIHS_CR_STOP)
#define U11_FAN_COMMAND_START                    TWIHS_CR_START
#define U11_FAN_COMMAND_STOP                     TWIHS_CR_STOP
#define U11_FAN_COMMAND_CONFIG                   0x00000004

// These values were determined by trial and error on the bench.
#define U11_FAN_SPEED_SETTING_OFF                0x01
#define U11_FAN_SPEED_SETTING_LOW                0x20
#define U11_FAN_SPEED_SETTING_MAX                0x00

#define FAN_EXPECTED_MFR_ID_VALUE                0x54
#define FAN_EXPECTED_VERSION_ID_VALUE            0x02

// To support the function call to read various U11 FAN information data:
#define CLI_READ_U11_FAN_MFR_ID                  1
#define CLI_READ_U11_FAN_VERSION_ID              2
#define CLI_READ_U11_FAN_STATUS                  3
#define CLI_READ_U11_FAN_CONFIG                  4

// Fan speed will be set to: - MAX speed if temperature is greater than the MAX temperature setting
//                           - else LOW speed if temperature is greater than the LOW temperature setting
//                           - else the fan speed is set to OFF
//
// How often should fan speed be checked? Every 30 seconds for both (i.e., every 15 seconds for either) seems about right. 
#define FAN_SPEED_LOW_TEMPERTURE_DEFAULT         35.0
#define FAN_SPEED_MAX_TEMPERTURE_DEFAULT         45.0
#define FAN_SPEED_CHECK_TIMEOUT                  15000

#define DEVICE_TO_CHECK_FAN                      1
#define DEVICE_TO_CHECK_TEMPERATURE              2

typedef struct s_temperatureFanControlCb
{
    S32    currentTempCelsius;              // signed value, evaluated from the temperature sensor
    U16    rawRegister;                     // raw register after shift/concatenate of the 2 1-byte register reads
    U16    pad;                             // align data on a 32-bit boundary
    S32    lowSpeedTemperatureSetting;      // hard-coded default, can be changed via CLI
    S32    maxSpeedTemperatureSetting;      // hard-coded default, can be changed via CLI
    U32    currentU11FanSpeed;              // for reporting as part of hello processing
    U32    lastCheckTick;                   // for the mini state machine called from main
    U32    deviceToCheck;                   // check fan speed, or check/updte temperature
} t_temperatureFanControlCb;


// When reading out the raw temperature data from the sensor:
#define RAW_TEMPERATURE_DATA_NEGATIVE_BIT       0x8000     // MSB = 1 = below C
#define RAW_TEMPERATURE_DATA_MASK               0x7FFF     // mask off the most significant bit
#define RAW_TEMPERATURE_DATA_REGISTER_SHIFT     5          // shift the 16-bit raw temperature register to the right to get the 11-bit data
#define RAW_TEMPERATURE_DATA_FACTOR             0.125      // multiplication factor, as per the data sheet

// To facilitate access to the temperature sensor device register set over the SAME70's "TWIHS two-wire interface":
#define TEMP_FAN_REG_TWIHS_CR               (uint32_t)&REG_TWIHS0_CR
#define TEMP_FAN_REG_TWIHS_CWGR             (uint32_t)&REG_TWIHS0_CWGR
#define TEMP_FAN_REG_TWIHS_MMR              (uint32_t)&REG_TWIHS0_MMR
#define TEMP_FAN_REG_TWIHS_IADR             (uint32_t)&REG_TWIHS0_IADR
#define TEMP_FAN_REG_TWIHS_STATUS           (uint32_t)&REG_TWIHS0_SR
#define TEMP_FAN_REG_TWIHS_RHOLD            (uint32_t)&REG_TWIHS0_RHR
#define TEMP_FAN_REG_TWIHS_THOLD            (uint32_t)&REG_TWIHS0_THR


// END OF TEMPERATURE AND FAN CONTROL DATA.


// Extracted from the hardware test script, to configure the PMC/TWI/PIA for fan control control:
#define TWISH0_CLOCK_ENABLE_VALUE           (0x00180000U)
#define TWIHS0_SCLK_FREQUENCY_VALUE         (0x00035353U)
#define TWIHS0_ENABLE_MM_DISABLE_OTHERS     (0x25002a24U)

// The user might want to enable or disable ethernet service on any port.
#define ETHERNET_SERVICE_DISABLE                0
#define ETHERNET_SERVICE_ENABLE                 1


// A higher-lever entity might want to know which controller it is interfacing with.
#define UNKNOWN_ETHERNET_CONTROLLER            1
#define PRIMARY_ETHERNET_CONTROLLER            2
#define SECONDARY_ETHERNET_CONTROLLER          3


// To support the Ethernet Port Checking State Machine:
#define ETH_SM_START                           1
#define ETH_SM_IDLE                            2
#define ETH_SM_PRIMARY_CONTROL_REGISTER        3
#define ETH_SM_PRIMARY_STATUS_REGISTER         4
#define ETH_SM_SECONDARY_CONTROL_REGISTER_02   5
#define ETH_SM_SECONDARY_CONTROL_REGISTER_09   6
#define ETH_SM_SECONDARY_CONTROL_REGISTER_10   7
#define ETH_SM_SECONDARY_CONTROL_REGISTER_11   8
#define ETH_SM_SECONDARY_STATUS_REGISTER       9
#define ETH_SM_PRIMARY_ETH_SERVICE            10
#define ETH_SM_SECONDARY_ETH_SERVICE          11

typedef struct s_ethernetPortCheckingSmCb
{
    U32    lastCheckTick;
    U32    state;
    U32    primaryNextIndex;
    U32    secondaryNextIndex;
    BOOL   enabled;
} t_ethernetPortCheckingSmCb;


// Control and status register data is in a structured array of ports. Use these to access the appropriate port.
#define PORT_1_INDEX                             0 // primary switch "port 1" ... downstream port 1 of the PCBA
#define PORT_2_INDEX                             1 // primary switch "port 2" ...       "      "  2  "   "   "
#define PORT_3_INDEX                             2 // primary switch "port 3" ...       "      "  3  "   "   "
#define PORT_4_INDEX                             3 // primary switch "port 4" ...       "      "  4  "   "   "
#define PORT_LAN_INDEX                           4 // primary switch "port 5" ... "UPSTREAM" LAN     "   "   "
#define PRIMARY_PORT_INDEX_MAX                   PORT_LAN_INDEX

// For the secondary controller ports:
#define PORT_5_INDEX                             0 // secondary switch "port 1" ... downstream port 1 of the PCBA
#define PORT_6_INDEX                             1 // secondary switch "port 2" ...       "      "  2  "   "   "
#define PORT_7_INDEX                             2 // secondary switch "port 3" ...       "      "  3  "   "   "
#define PORT_8_INDEX                             3 // secondary switch "port 4" ...       "      "  4  "   "   "
#define SECONDARY_PORT_INDEX_MAX                 PORT_8_INDEX

// Considering the downstream ports 1..8 (omitting the LAN port), define the max downstream PRIMARY port "KEY":
#define PRIMARY_DOWNSTREAM_PORT_KEY_MAX          4

// Port "keys", not to be confused with "port index", where "index" always refers to an integer
// to be used to index into a table of things.
#define PORT_KEY_PORT_LAN                        0
#define PORT_KEY_PORT_1                          1
#define PORT_KEY_PORT_2                          2
#define PORT_KEY_PORT_3                          3
#define PORT_KEY_PORT_4                          4
#define PORT_KEY_PORT_5                          5
#define PORT_KEY_PORT_6                          6
#define PORT_KEY_PORT_7                          7
#define PORT_KEY_PORT_8                          8

// To support accessing the ALU of both ethernet controllers:
#define PRIMARY_CONTROLLER_MAX_KEY               PORT_KEY_PORT_4
#define SECONDARY_CONTROLLER_MAX_KEY             PORT_KEY_PORT_8
#define PRIMARY_ALU_INDEX_MAX                 4095
#define SECONDARY_ALU_INDEX_MAX               1023

// When in a loop reading and checking the "DATA IS READY" bit for either controller:
#define MAX_READ_LOOP_LIMIT                           5

// To support the exchange of the notion of port UP or DOWN between
// the primary/secondary ethernet controllers and the POE subsystem.
#define PORT_IS_DOWN_HIGH_LEVEL                  0
#define PORT_IS_UP_HIGH_LEVEL                    1


// Used by ALU MAC resolution: The RGMII port is excluded as a "valid" port.
#define VALID_PRIMARY_KSZ_PORT_NUMBER_MIN        1
#define VALID_PRIMARY_KSZ_PORT_NUMBER_MAX        5

#define RGMII_PRIMARY_KSZ_PORT_NUMBER            6

#define VALID_SECONDARY_KSZ_PORT_NUMBER_MIN      1
#define VALID_SECONDARYRY_KSZ_PORT_NUMBER_MAX    4

// There are 9 ports overall that are exposed to the outside world:
#define MAX_NUMBER_APPARENT_PORTS      (PRIMARY_PORT_INDEX_MAX + 1) + (SECONDARY_PORT_INDEX_MAX + 1)

// Downstream ports managed by various tables: primary   downstream port indices = 0..3
//                                             secondary downstream port indices = 4..7
#define MAX_NUMBER_DOWNSTREAM_PORTS              8
#define MAX_PRIMARY_DOWNSTREAM_PORT_INDEX        3

#define NULL_ETHERNET_PORT                    0xFF

// Some facilities may or may not allow the LAN port to be specified:
#define LAN_OK                                TRUE
#define LAN_NOK                               FALSE

// AUTONOMOUS EVENT INFO REPORTING
//
// Max string size into which autonomous event data is written. The string is JSON-structured.
//
// For a single catastrophic event where "bad thing happened" on all 8 ethernet ports + the LAN, the string is
// size to hold the 139 characters for 9 ports. The same for a worse-case scenario with the POE controller.
//
// Testing shows that under normal circumstances, the following limits meet system reporting needs insofar as
// autonomous event reporting is concerned. When string limits are reached (or approach the safety margins), the
// worse that would happen is 1 or more events are dropped. This always happens if the SBC/gateway or USB link
// is down
#define AUTONOMOUS_EVENTS_STRING_SIZE                    2048    // max size of the autonomous event block for SBC/Gateway reporting
#define AUTONOMOUS_EVENTS_SAFETY_MARGIN                  1840    // about 90% of the size. 
#define SINGLE_JSON_EVENT_STRING_SIZE                     256    // size of a single event written to the autonomous event block
#define POE_SINGLE_JSON_EVENT_STRING_SIZE                 680    // worse-case POE event involving all 8 ports
#define POE_JSON_SAFETY_MARGIN_CHARS                      648    // about 95% of the size of a single really bad POE event


// AUTONOMOUS EVENT ID (AEI) code identifiers. These need to be sync'd with the Gateway.
// Add new ones as required. Old ones are deprecated.
#define AEI_AMU_STARTUP                           1    // AMU STARTUP
#define AEI_DEPRECATED_2                          2    // deprecated
#define AEI_DEPRECATED_3                          3    // deprecated
#define AEI_DEPRECATED_4                          4    // deprecated
#define AEI_DEPRECATED_5                          5    // deprecated
#define AEI_DEPRECATED_6                          6    // deprecated
#define AEI_PRIMARY_ETH_FOUND_DISABLED            7    // PRIMARY ETHERNET WAS FOUND TO HAVE BEEN DISABLED
#define AEI_SECONDARY_ETH_FOUND_DISABLED          8    // SECONDARY ETHERNET WAS FOUND TO HAVE BEEN DISABLED
#define AEI_SECONDARY_ETH_ENABLE                  9    // SECONDARY SWITCH ETHERNET SERVICE REQUIRED TO BE SET
#define AEI_DEPRECATED_10                        10    // deprecated
#define AEI_DEPRECATED_11                        11    // deprecated
#define AEI_DEPRECATED_12                        12    // deprecated
#define AEI_DEPRECATED_13                        13    // deprecated
#define AEI_DEPRECATED_14                        14    // deprecated
#define AEI_CC_INPUT_STATUS_CHANGE               15    // contact closure input status change
#define AEI_DEPRECATED_16                        16    // deprecated
#define AEI_DEPRECATED_17                        17    // deprecated
#define AEI_POE_PORT_POWER_CHANGE                18    // POE port POE power changed on 1 or more ports
#define AEI_POE_PORT_AUTO_CLASS_CONFIG_CHANGE    19    // POE port POE police auto class config changed on 1 or more ports
#define AEI_POE_PORT_PAC_CHANGE                  20    // POE port POE PAC changed on 1 or more ports
#define AEI_POE_PORT_AC_STATUS_CHANGE            21    // POE port POE Auto Class Status changed on 1 or more ports
#define AEI_POE_PORT_POWER_THRESHOLD_EXCEEDED    22    // POE port power threshold on 1 or more ports exceeded
#define AEI_DEPRECATED_23                        23    // deprecated
#define AEI_GENERIC_BAD_EVENT                    24    // bad event counter was incremented
#define AEI_PRIMARY_CONTROL_RESTORED             25    // PRIMARY SWITCH:   control register    restored to configured value
#define AEI_SECONDARY_CONTROL_02_RESTORED        26    // SECONDARY SWITCH: control register  2 restored to configured value
#define AEI_SECONDARY_CONTROL_09_RESTORED        27    // SECONDARY SWITCH: control register  9 restored to configured value
#define AEI_SECONDARY_CONTROL_10_RESTORED        28    // SECONDARY SWITCH: control register 10 restored to configured value
#define AEI_SECONDARY_CONTROL_11_RESTORED        29    // SECONDARY SWITCH: control register 11 restored to configured value
#define AEI_STATUS_LINK_UP                       30    // LINK UP
#define AEI_STATUS_LINK_DN                       31    // LINK DOWN
#define AEI_STATUS_ANY                           32    // ANY OTHER STATUS BIT CHANGE
#define AEI_TEMPERATURE_CHANGE                   33    // new temperature reading
#define AEI_FAN_SPEED_CHANGE                     34    // fan speed was changed
#define AEI_POE_SUPPLY_FAULT_EVENT_DETECTED      35    // supply/fault event detected in the POE event register
#define AEI_POE_PORT_EVENT_DETECTED              36    // port event detected: power,detection, fault or start.


// How often should the controllers be checked? Currently set to 3 seconds.
#define CHECK_ETHERNET_PORTS_TIMEOUT        3000

// When writing the 5-byte (PRIMARY) or 3-byte SECONDARY) sequence to read or write a
// switch register, the following delay in msecs is required between each byte.
#define KSV_DELAY                           2

// When accessing the SAME70 registers for a series of multiple, back-to-back
// read/write operation, it is advisable to wait a bit before the next operation
// in order to assure register interface readiness.
#define SAME70_REG_DELAY                    10

// When reading a MIB index counter from the primary ethernet controller,
// wait this long before reading again if the "ready" bit was not set.
#define RETRY_MIB_INDEX_READ_WAIT_DELAY     100

#define READ_INPUT_VOLTAGE_BY_BYTE          1
#define READ_INPUT_VOLTAGE_BY_WORD          2

// To support upgrade testing in a debug environment:
#define PLEASE_DROP_THE_NEXT_DATA_PACKET         1
#define PLEASE_DROP_THE_NEXT_PACKET_ACK          2

//To support writing to the Craft/debug port, if and when enabled by compilation:
#define UART1_WRITE_PACKET_MACRO usart_serial_write_packet((usart_if)UART1, (uint8_t *)pCraftPortDebugData, strlen(pCraftPortDebugData));

// External Function Prototypes

void  getProcessorResetReason               (void);
void  configureEthernetSubsystem            (void);
BOOL  isCraftDebugEnabled                   (void);
void  initApparentSubsystemData             (void);
void  writeToSbcComInputBuffer              (char oneChar);
void  writeToCraftInputBuffer               (char oneChar);
void  incrementBadEventCounter              (U16  badEventIndex);
void  initCraftInputBuffer                  (void);
void  initSbcComInputBuffer                 (void);
void  checkSbcComUserInput                  (void);
void  checkCraftUserInput                   (void);
void  checkKeepAlive                        (void);
void  checkJumpToBootOrReset                (void);
void  readPcbaJumpers                       (BOOL outputJson);
void  displayApparentBanner                 (void);
void  displayEndOfOutput                    (void);
U8    getPortKeyFromPortArg                 (char *pPortString, BOOL lanAllowed);
char *pGimmePrimaryPortString               (U16 primaryPortIndex);
char *pGimmeSecondaryPortString             (U16 secondaryPortIndex);
void  writeAutonomousAmuStartupNotice       (void);
void  configureContactClosures              (void);
void  configureTempAndFanControl            (void);
void  checkInputContactClosures             (void);
void  checkFanSpeedOrTemperature            (void);
void  checkEthernetPortsSm                  (void);
U32   readSame70Register                    (U32 registerAddress);
void  writeSame70Register                   (U32 registerAddress,   U32 registerValue);
void  findComponentStringData               (const char *pComponent, char *pValueString, char *pArguments);
void  displayInvalidParameters              (char *pUserInput, char *pExtraInfo);
void  getPortKeyListFromPortArg             (char *pPortString,     U8  *pPortKeyList);
BOOL  isLanPortKeyPresent                   (U8  *pPortKeyList);
void  pokeWatchdog                          (void);
void  addJsonToAutonomnousString            (char *pJson);


// A SMALL SET OF UTILITY FUNCTIONS. EVENTUALLY: MOVE THEM TO A NEW apparent_helper.c/h modules,
//                                               to be contained in the common git submodule.
char  *pApparent_strstr                     (char *pSomeString, char *pSearchPart);
U32   calcCrc32Checksum                     (U32 crcSeed, U8 *buf, U16 len);
BOOL  isConvertMacString                    (char *pStringAsMac, U8 *pMac);
BOOL  isMacNull                             (U8 *pMac);
BOOL  isTwoFloatsEqual                      (F32 thisFloat, F32 thatFloat, U32 numberDecimals);
BOOL  isStringHexDigit                      (char *pString);
BOOL  isStringDecimalDigit                  (char *pString);
U32   longFromFloat                         (F32 thisFloat);
BOOL  newATOH                               (const char *String, U32 *pSomeLong);
U32   atoh                                  (const char *String);

/*----------------------------------------------------------------------------*/
#endif   /* _APPARENT_H_ */
