/**
* \file
*
* \brief Apparent strings-only definition module on the AMU (Apparent Management Unit).
*
 * Copyright (c) 2022 Apparent Inc., All rights reserved
 * http://www.apparent.com
*
* ----- Licensing info, if any, should go here. -----
*
* This header file externalizes all the string definitions for the Apparent application.
*
*/


#ifndef _APPARENT_STRINGS_H_
#define _APPARENT_STRINGS_H_


// All the string definitions in the apparent_strings.c module are externalized here:


// CLI Group 1:
extern const char GET_AUTONOMOUS_EVENTS_CMD[];                  //   0
extern const char KEEP_ALIVE_HELLO_CMD[];                       //   1
extern const char COUNTERS_CMD[];                               //   2
extern const char UPGRADE_CMD[];                                //   3
extern const char RESET_CMD[];                                  //   4
extern const char JUMP_TO_BOOT_CMD[];                           //   5
extern const char PROGRAM_CMD[];                                //   6
extern const char PROGRAM_SIGNATURES_CMD[];                     //   7
extern const char CONTACT_CLOSURE_OUTPUT_CLOSE_CMD[];           //   8
extern const char CONTACT_CLOSURE_OUTPUT_OPEN_CMD[];            //   9
extern const char PORT_POWER_UP_CMD[];                          //  10
extern const char PORT_POWER_DOWN_CMD[];                        //  11
extern const char POE_ONLY_PORT_POWER_UP_CMD[];                 //  12
extern const char POE_ONLY_PORT_POWER_DOWN_CMD[];               //  13
extern const char SET_FAN_SPEED_SETTINGS_CMD[];                 //  14
extern const char RESOLVE_MAC_LIST_CMD[];                       //  15
extern const char GET_MAC_RESOLUTION_RESULT_CMD[];              //  16
extern const char LAUNCH_ALU_INFO_BY_PORT_INDEX_CMD[];          //  17
extern const char GET_ALU_INFO_BY_PORT_INDEX_RESULT_CMD[];      //  18
extern const char CANCEL_GET_ALU_MAC_RESULT_CMD[];              //  19
extern const char CONFIG_STATIC_LAN_MAC_CMD[];                  //  20
extern const char GET_STATIC_LAN_MAC_INFO_CMD[];                //  21

// CLI Group 2:
extern const char DISPLAY_BANNER_CMD[];                         //  22
extern const char ETH_ONLY_PORT_ENABLE_CMD[];                   //  23
extern const char ETH_ONLY_PORT_DISABLE_CMD[];                  //  24
extern const char READ_ALU_PRI_BY_INDEX_CMD[];                  //  25
extern const char READ_ALU_PRI_RANGE_CMD[];                     //  26
extern const char READ_ALU_PRI_WITH_MAC_AS_INDEX_CMD[];         //  27
extern const char READ_ALU_SEC_DYNAMIC_BY_INDEX_CMD[];          //  28
extern const char READ_ALU_SEC_RANGE_CMD[];                     //  29
extern const char ALU_MAC_RESO_PARAM_CMD[];                     //  30
extern const char USER_SIGNATURE_READ_FLASH_CMD[];              //  31
extern const char USER_SIGNATURE_READ_RAM_CMD[];                //  32
extern const char USER_SIGNATURE_WRITE_CMD[];                   //  33
extern const char USER_SIGNATURE_ERASE_CMD[];                   //  34
extern const char READ_BOTH_KSZ_MAC_CMD[];                      //  35
extern const char SET_LOCAL_TESTING_CMD[];                      //  36
extern const char POE_GENERAL_INFO_CMD[];                       //  37
extern const char POE_CONTROLLER_INPUT_VOLTAGE_CMD[];           //  38
extern const char ETHERNET_CONTROLLER_RESET_CMD[];              //  39
extern const char TEMPERATURE_CMD[];                            //  40
extern const char TEMP_FAN_INFO_CMD[];                          //  41
extern const char FAN_INFO_CMD[];                               //  42
extern const char FAN_TASTIC_CMD[];                             //  43
extern const char READ_PCBA_JUMPERS_CMD[];                      //  44
extern const char DISPLAY_HELP_CMD[];                           //  45
extern const char LAN_PORT_ENABLE_CMD[];                        //  46
extern const char LAN_PORT_DISABLE_CMD[];                       //  47
extern const char DISPLAY_POE_PORT_POWER_CMD[];                 //  48
extern const char DISPLAY_PORT_INFO_CMD[];                      //  49
extern const char GET_CC_INFO_CMD[];                            //  50
extern const char ENABLE_SWITCH_CHECKING_CMD[];                 //  51
extern const char DISABLE_SWITCH_CHECKING_CMD[];                //  52
extern const char ENABLE_POE_CHECKING_CMD[];                    //  53
extern const char DISABLE_POE_CHECKING_CMD[];                   //  54
extern const char POE_READ_PORT_EVENT_REGISTER_CMD[];           //  55
extern const char POE_READ_SUPPLY_EVENT_REGISTER_CMD[];         //  56
// CLI Group 3
extern const char POE_DEVICE_ID_CMD[];                          //  57
extern const char POE_MFR_ID_CMD[];                             //  58
extern const char POE_READ_BYTE_CMD[];                          //  59
extern const char POE_READ_WORD_CMD[];                          //  60
extern const char POE_WRITE_BYTE_CMD[];                         //  61
extern const char POE_WRITE_WORD_CMD[];                         //  62
extern const char GET_SWITCH_MIB_COUNTER_CMD[];                 //  63
extern const char ENABLE_MIB_FLUSH_FREEZE_CMD[];                //  64
extern const char DISABLE_MIB_FLUSH_FREEZE_CMD[];               //  65
extern const char FLUSH_MIB_COUNTERS_CMD[];                     //  66
extern const char FREEZE_MIB_COUNTERS_CMD[];                    //  67
extern const char UNFREEZE_MIB_COUNTERS_CMD[];                  //  68
extern const char SAME70_READ_REG_CMD[];                        //  69
extern const char SAME70_WRITE_REG_CMD[];                       //  70
extern const char READ_5PORTS_BYTE_BY_ADDRESS_CMD[];            //  71
extern const char WRITE_5PORTS_BYTE_BY_ADDRESS_CMD[];           //  72
extern const char READ_5PORTS_WORD_BY_ADDRESS_CMD[];            //  73
extern const char WRITE_5PORTS_WORD_BY_ADDRESS_CMD[];           //  74
extern const char ENABLE_25MHZ_CLOCK_OUTPUT_CMD[];              //  75
extern const char DISABLE_25MHZ_CLOCK_OUTPUT_CMD[];             //  76
extern const char READ_PRI_BYTE_BY_ADDRESS_CMD[];               //  77
extern const char WRITE_PRI_BYTE_BY_ADDRESS_CMD[];              //  78
extern const char READ_PRI_WORD_BY_ADDRESS_CMD[];               //  79
extern const char WRITE_PRI_WORD_BY_ADDRESS_CMD[];              //  80
extern const char READ_SEC_BYTE_ADDRESS_CMD[];                  //  81
extern const char READ_SEC_4PORTS_BY_BASE_ADDR_CMD[];           //  82
extern const char WRITE_SEC_BYTE_BY_ADDRESS_CMD[];              //  83
extern const char UART0_READ_MODE_STATUS_CMD[];                 //  84
extern const char USART2_READ_MODE_STATUS_CMD[];                //  85
extern const char USART_RESET_STATUS_CMD[];                     //  86
extern const char RGB_LED_RED_MEDIUM_CMD[];                     //  87
extern const char RGB_LED_RED_OFF_CMD[];                        //  88
extern const char RGB_LED_RED_BRILLIANT_CMD[];                  //  89
extern const char RGB_LED_GREEN_MEDIUM_CMD[];                   //  90
extern const char RGB_LED_GREEN_OFF_CMD[];                      //  91
extern const char RGB_LED_GREEN_BRILLIANT_CMD[];                //  92
extern const char RGB_LED_BLUE_MEDIUM_CMD[];                    //  93
extern const char RGB_LED_BLUE_OFF_CMD[];                       //  94
extern const char RGB_LED_BLUE_BRILLIANT_CMD[];                 //  95
extern const char RGB_LED_ENABLE_CMD[];                         //  96
extern const char RGB_LED_DISABLE_CMD[];                        //  97
// CLI Group 4
extern const char ENABLE_CRAFT_DEBUG_OUTPUT_CMD[];              //  98
extern const char DISABLE_CRAFT_DEBUG_OUTPUT_CMD[];             //  99
extern const char ENABLE_POE_DEBUG_OUTPUT_CMD[];                // 100
extern const char DISABLE_POE_DEBUG_OUTPUT_CMD[];               // 101
extern const char ENABLE_I2C_FULL_OUTPUT_CMD[];                 // 102
extern const char DISABLE_I2C_FULL_OUTPUT_CMD[];                // 103
extern const char ENABLE_UPGRADE_INFO_CMD[];                    // 104
extern const char DISABLE_UPGRADE_INFO_CMD[];                   // 105
extern const char ENABLE_UPGRADE_HEX_INFO_CMD[];                // 106
extern const char DISABLE_UPGRADE_HEX_INFO_CMD[];               // 107
extern const char DROP_UPGRADE_DATA_PACKET_CMD[];               // 108
extern const char DROP_UPGRADE_PACKET_ACK_CMD[];                // 109
extern const char PRINT_FH_CMD[];                               // 110
extern const char SET_FH_PATTERN_CMD[];                         // 111
extern const char CRC_FLASH_BLOCK_CMD[];                        // 112
extern const char IS_FLASH_RANGE_ERASED_CMD[];                  // 113
extern const char READ_FLASH_LONG_CMD[];                        // 114
extern const char WRITE_FLASH_LONG_CMD[];                       // 115
extern const char READ_FLASH_BLOCK_CMD[];                       // 116
extern const char WRITE_FLASH_BLOCK_CMD[];                      // 117
extern const char SOFTWARE_RESET_CMD[];                         // 118
extern const char NOP_CMD[];                                    // 119
extern const char WATCHDOG_DISABLE_TEST_CMD[];                  // 120
// END OF THE SET OF CLI COMMANDS.

extern const char AMU_ATIRA_PRODUCT_DEFAULT[];
extern const char AMU_SERIAL_NUMBER_DEFAULT[];
extern const char AMU_MODEL_DEFAULT[];

extern const char CLI_ARG_ADDR_STRING[];
extern const char CLI_ARG_LONG_STRING[];
extern const char CLI_ARG_LENGTH_STRING[];
extern const char CLI_ARG_VALUE_STRING[];
extern const char CLI_ARG_SERIAL_NUMBER_STRING[];
extern const char CLI_ARG_PRODUCT_STRING[];
extern const char CLI_ARG_MODEL_STRING[];
extern const char CLI_ARG_UART_STRING[];
extern const char CLI_ARG_PORT_NUMBER_SWITCH_MIB_COUNTER_STRING[];
extern const char CLI_ARG_MIB_INDEX_SWITCH_MIB_COUNTER_STRING[];
extern const char CLI_ARG_CLOCK_STRING[];
extern const char CLI_ARG_BAUD_STRING[];
extern const char CLI_ARG_PORT_STRING[];
extern const char CLI_ARG_CONFIG_STRING[];
extern const char CLI_ARG_TERSE_STRING[];

extern const char CLI_ARG_WATTS_STRING[];
extern const char CLI_ARG_READ_FAN_MFR_ID_STRING[];
extern const char CLI_ARG_READ_FAN_VERSION_ID_STRING[];
extern const char CLI_ARG_READ_FAN_STATUS_ID_STRING[];
extern const char CLI_ARG_READ_FAN_SPEED_STRING[];
extern const char CLI_ARG_WRITE_FAN_SPEED_STRING[];
extern const char CLI_ARG_SPEED_STRING[];
extern const char CLI_ARG_FAN_SPEED_OFF_STRING[];
extern const char CLI_ARG_FAN_SPEED_LOW_STRING[];
extern const char CLI_ARG_FAN_SPEED_MAX_STRING[];
extern const char CLI_ARG_LOW_STRING[];
extern const char CLI_ARG_MAX_STRING[];
extern const char CLI_ARG_CC_STRING[];
extern const char CLI_ARG_ACTION_STRING[];
extern const char CLI_ARG_CLOSE_STRING[];
extern const char CLI_ARG_OPEN_STRING[];
extern const char CLI_ARG_FID_STRING[];
extern const char CLI_ARG_INDEX_STRING[];
extern const char CLI_ARG_ALU_START_INDEX_STRING[];
extern const char CLI_ARG_MAC_STRING[];
extern const char CLI_ARG_CTRL_0_STRING[];
extern const char CLI_ARG_CTRL_1_STRING[];
extern const char CLI_ARG_START_STRING[];
extern const char CLI_ARG_END_STRING[];
extern const char CLI_ARG_CONTROLLER_STRING[];
extern const char CLI_ARG_PRIMARY_STRING[];
extern const char CLI_ARG_SECONDARY_STRING[];
extern const char CLI_ARG_BOTH_STRING[];
extern const char CLI_ARG_MAC_LIST_STRING[];
extern const char CLI_ARG_RGB_COLOR_STRING[];
extern const char CLI_ARG_BLUE_COLOR_STRING[];
extern const char CLI_ARG_GREEN_COLOR_STRING[];
extern const char CLI_ARG_RED_COLOR_STRING[];
extern const char CLI_ARG_ALL_COLOR_STRING[];
extern const char CLI_ARG_ON_PERIOD_STRING[];
extern const char CLI_ARG_OFF_PERIOD_STRING[];
extern const char CLI_ARG_LOOP_LIMIT_STRING[];
extern const char CLI_ARG_ALU_ACTION_STRING[];
extern const char CLI_ARG_NO_OP_STRING[];
extern const char CLI_ARG_WRITE_STRING[];
extern const char CLI_ARG_READ_STRING[];
extern const char CLI_ARG_SEARCH_STRING[];
extern const char CLI_ARG_WAIT_ACTION_STRING[];
extern const char CLI_ARG_GROUP_STRING[];
extern const char CLI_ARG_LIST_STRING[];

extern const char COMMAND_STRING[];
extern const char RESPONSE_STRING[];
extern const char REASON_STRING[];
extern const char NOT_AVAILABLE_STRING[];

extern const char ARGUMENTS_STRING[];
extern const char RESULT_STRING[];
extern const char INVALID_ARGUMENTS_STRING[];
extern const char NULL_STRING[];

extern const char CLI_FLASHING_IS_DISABLED_STRING[];
extern const char CLI_FLASHING_IS_ENABLED_STRING[];
extern const char ETH_CONTROLLER_CHECKING_ENABLED_STRING[];
extern const char ETH_CONTROLLER_CHECKING_DISABLED_STRING[];
extern const char ETH_CONTROLLER_CHECKING_WAS_ENABLED_STRING[];
extern const char ETH_CONTROLLER_CHECKING_WAS_DISABLED_STRING[];
extern const char POE_CHECKING_ENABLED_STRING[];
extern const char POE_CHECKING_DISABLED_STRING[];
extern const char POE_CHECKING_WAS_ENABLED_STRING[];
extern const char POE_CHECKING_WAS_DISABLED_STRING[];

extern const char MAC_INFO_LIST_STRING[];
extern const char CURRENTLY_RUNNING_STRING[];
extern const char STATE_MACHINE_LAUNCHED_STRING[];
extern const char INVALID_MAC_LIST_STRING[];
extern const char EXTRACT_1_MAC_FAILED_STRING[];
extern const char END_OF_MAC_NOT_FOUND_STRING[];
extern const char STATE_MACHINE_STRING[];
extern const char IDLE_STRING[];
extern const char READING_FROM_PRIMARY_STRING[];
extern const char PREPARE_SECONDARY_READ_STRING[];
extern const char READING_FROM_SECONDARY_STRING[];

extern const char CLI_MAC_AS_INDEX_STRING[];
extern const char CLI_MAC_FOUND_STRING[];

extern const char LAUNCHED_STRING[];
extern const char PROCESS_STRING[];
extern const char DATA_STRING[];
extern const char PORT_INDEX_STRING[];
extern const char ALU_MAC_RUNNING_STRING[];
extern const char MAC_RESOLUTION_RUNNING_STRING[];
extern const char MISSING_PORT_ARG_STRING[];
extern const char MISSING_INDEX_ARG_STRING[];
extern const char INVALID_PORT_STRING[];
extern const char MAX_ALU_INDEX_STRING[];
extern const char CURRENT_ALU_INDEX_STRING[];
extern const char RUNNING_STRING[];
extern const char NO_REQUEST_MADE_STRING[];
extern const char CANCELED_STRING[];
extern const char USER_COMMAND_STRING[];
extern const char START_READ_TICK_STRING[];
extern const char LAST_READ_TICK_STRING[];
extern const char ELAPSED_TICK_STRING[];
extern const char ALU_INDEX_OUT_OF_RANGE_STRING[];
extern const char USER_PORT_STRING[];
extern const char USER_ALU_INDEX_STRING[];
extern const char KSZ_VALID_COUNT_STRING[];
extern const char KSZ_PORT_FORWARD_STRING[];
extern const char LAST_KSZ_PORT_FORWARD_STRING[];
extern const char DEAD_QUIET_STRING[];
extern const char ALU_START_INDEX_STRING[];
extern const char NUMBER_DATA_READS_STRING[];
extern const char MISSING_ALU_START_INDEX_ARG_STRING[];
extern const char PRI_ALU_CONTROL_REG_READ_LIMIT_STRING[];
extern const char LOOP_LIMIT_MSEC_STRING[];
extern const char ALU_ACTION_STRING[];
extern const char WAIT_ACTION_STRING[];
extern const char MISSING_ARGUMENTS_STRING[];
extern const char END_INDEX_LESS_THAN_START_STRING[];
extern const char RANGE_TOO_LARGE_STRING[];
extern const char SECOND_PRI_ALU_READ_STRING[];
extern const char REQUIRED_STRING[];
extern const char NOT_REQUIRED_STRING[];
extern const char READ_FROM_SECONDARY_FAILED_STRING[];
extern const char READ_FROM_PRIMARY_FAILED_STRING[];
extern const char STATIC_LAN_MAC_INFO_STRING[];
extern const char STATIC_LAN_MAC_CONFIG_PRIMARY_FAILED_STRING[];
extern const char STATIC_LAN_MAC_CONFIG_SECONDARY_FAILED_STRING[];
extern const char STATIC_LAN_MAC_CONFIG_STRING[];

extern const char CLI_PORT_AGING_STRING[];
extern const char CLI_ENTRY_TYPE_STRING[];
extern const char CLI_ENTRY_TYPE_DYNAMIC_STRING[];
extern const char CLI_ENTRY_TYPE_UNKNOWN_STRING[];
extern const char CLI_ENTRY_TYPE_STATIC_STRING[];
extern const char INDEX_STRING[];
extern const char INFO_STRING[];
extern const char RAW_REG_STRING[];
extern const char VALID_DATA_STRING[];
extern const char NUMBER_OF_ENTRIES_STRING[];
extern const char TIMESTAMP_STRING[];
extern const char KSZ_PORT_STRING[];
extern const char DOWNSTREAM_PORT_STRING[];
extern const char DATA_READY_STRING[];
extern const char FILTER_ID_STRING[];
extern const char ELAPSED_STRING[];
extern const char PORT_FORWARD_STRING[];
extern const char VALID_COUNT_STRING[];
extern const char TOTAL_ELAPSED_TIME_STRING[];
extern const char SECONDARY_NUMBER_ENTRIES_STRING[];
extern const char UNCHANGED_STRING[];
extern const char INCREASED_STRING[];
extern const char DECREASED_STRING[];
extern const char CONTROLLER_STRING[];
extern const char PRIMARY_STRING[];
extern const char SECONDARY_STRING[];
extern const char UNKNOWN_STRING[];

extern const char CLI_POE_REG_ADDR_STRING[];
extern const char CLI_MSB_STRING[];
extern const char CLI_LSB_STRING[];
extern const char CLI_LSB_MSB_STRING[];
extern const char CLI_VALUE_STRING[];
extern const char POE_CONTROLLER_INPUT_VOLTAGE_STRING[];
extern const char POE_LDWPP_DETECT_TIMEOUT_STRING[];
extern const char POE_APPLY_LDWPP_WORK_AROUND_STRING[];
extern const char POE_LDWPP_POWERED_DOWN_TIMEOUT_STRING[];

extern const char END_OF_INPUT_SIGNATURE_STRING[];
extern const char END_OF_OUTPUT_SIGNATURE_STRING[];

extern const char CLI_STRING[];
extern const char NONE_STRING[];
extern const char CODE_STRING[];
extern const char STATE_STRING[];
extern const char FLASH_ADDR_CRC_STRING[];
extern const char AMU_STARTUP_STRING[];
extern const char WATCHDOG_STRING[];
extern const char WATCHDOG_ENABLED_STRING[];
extern const char WATCHDOG_DISABLED_STRING[];
extern const char UPTIME_STRING[];
extern const char PRODUCT_STRING[];
extern const char SERIAL_NUMBER_STRING[];
extern const char MODEL_STRING[];
extern const char HARDWARE_REVISION_STRING[];

extern const char CLI_READY_FOR_JUMP_TO_BOOT[];
extern const char CLI_FLASH_FAIL_NO_BOOT_SIG_STRING[];
extern const char CLI_FLASH_FAIL_SET_WAIT_STATE_STRING[];
extern const char PROGRAM_STRING[];
extern const char FLASH_LOAD_ADDRESS_STRING[];
extern const char FLASH_SIGNATURES_STRING[];
extern const char FLASH_READ_FAIL_STRING[];
extern const char ERASE_USER_SIGNATURE_STRING[];
extern const char USER_SIGNATURE_ERASED_STRING[];
extern const char USER_SIGNATURE_ERASE_FAILED_STRING[];
extern const char USER_SIGNATURE_ERASE_DISABLED_STRING[];
extern const char UPDATE_USER_SIGNATURE_STRING[];
extern const char CALCULATED_CRC_STRING[];
extern const char NUMBER_OF_BYTES_STRING[];
extern const char USER_SIGNATURE_UPDATED_STRING[];
extern const char USER_SIGNATURE_CURRENTLY_WRITTEN_STRING[];
extern const char USER_SIGNATURE_MUST_BE_ERASED_STRING[];
extern const char USER_SIGNATURE_WRITE_FAILED_STRING[];
extern const char APPLICATION_STRING[];
extern const char BOOTLOADER_STRING[];
extern const char RELEASE_STRING[];
extern const char JUMP_TO_BOOT_STRING[];
extern const char SUCCESS_STRING[];
extern const char FAIL_STRING[];
extern const char FAIL_NO_BOOT_SIGNATURE[];
extern const char RESET_STRING[];
extern const char RESET_IN_5_SECONDS_STRING[];
extern const char JUMP_TO_BOOT_IN_5_SECONDS_STRING[];
extern const char PRIMARY_GLOBAL_REGISTERS_STRING[];
extern const char GLOBAL_REGISTER_0000_STRING[];
extern const char GLOBAL_REGISTER_0001_STRING[];
extern const char GLOBAL_REGISTER_0002_STRING[];
extern const char GLOBAL_REGISTER_0003_STRING[];
extern const char GLOBAL_REGISTER_0103_STRING[];
extern const char GLOBAL_REGISTER_0300_STRING[];

extern const char MAC_INFO_STRING[];
extern const char PRIMARY_MAC_STRING[];
extern const char SECONDARY_MAC_STRING[];
extern const char MAC_STRING[];

extern const char SECONDARY_GLOBAL_REGISTERS_STRING[];
extern const char GLOBAL_REGISTER_00_STRING[];
extern const char GLOBAL_REGISTER_01_STRING[];
extern const char GLOBAL_REGISTER_02_STRING[];
extern const char GLOBAL_REGISTER_03_STRING[];
extern const char GLOBAL_REGISTER_04_STRING[];
extern const char GLOBAL_REGISTER_05_STRING[];
extern const char GLOBAL_REGISTER_06_STRING[];
extern const char GLOBAL_REGISTER_07_STRING[];
extern const char GLOBAL_REGISTER_08_STRING[];
extern const char GLOBAL_REGISTER_09_STRING[]; 
extern const char GLOBAL_REGISTER_0A_STRING[];
extern const char GLOBAL_REGISTER_0B_STRING[];
extern const char GLOBAL_REGISTER_0C_STRING[];
extern const char GLOBAL_REGISTER_0D_STRING[];
extern const char GLOBAL_REGISTER_0E_STRING[];
extern const char GLOBAL_REGISTER_0F_STRING[];

extern const char PRIMARY_CONTROL_REGISTERS_STRING[];
extern const char PRIMARY_STATUS_REGISTERS_STRING[];
extern const char SECONDARY_CONTROL_REGISTERS_STRING[];
extern const char SECONDARY_STATUS_REGISTERS_STRING[];

extern const char SECONDARY_CONTROL_REGISTER_02_STRING[];
extern const char SECONDARY_CONTROL_REGISTER_09_STRING[];
extern const char SECONDARY_CONTROL_REGISTER_10_STRING[];
extern const char SECONDARY_CONTROL_REGISTER_11_STRING[];

extern const char AUTONOMOUS_EVENTS_LIST_STRING[];
extern const char EVT_STRING[];
extern const char DEFAULT_STRING[];

extern const char AMU_COUNTERS_STRING[];
extern const char RANK_STRING[];
extern const char COUNT_STRING[];

extern const char MIB_COUNTER_STRING[];
extern const char MIB_INDEX_STRING[];
extern const char ACTION_STRING[];
extern const char ENABLE_FLUSH_FREEZE_STRING[];
extern const char DISABLE_FLUSH_FREEZE_STRING[];
extern const char PRIMARY_SWITCH_LIST_STRING[];
extern const char SECONDARY_SWITCH_LIST_STRING[];
extern const char EXECUTION_DONE_STRING[];
extern const char MIB_RESULT_STRING[];
extern const char MIB_RESULT_SUCCESS_STRING[];
extern const char MIB_RESULT_FAILED_STRING[];
extern const char MIB_RESULT_MISSING_ARG_REASON_STRING[];
extern const char MIB_RESULT_INVAL_MIB_INDEX_REASON_STRING[];
extern const char MIB_RESULT_NOT_READY_REASON_STRING[];
extern const char MIB_RESULT_INVAL_PORT_REASON_STRING[];
extern const char MIB_READ_ELAPSED_MSECS_STRING[];
extern const char VALUE_AS_HEX_STRING[];
extern const char VALUE_AS_DECIMAL_STRING[];
extern const char ERROR_STRING[];
extern const char SET_MIB_INDEX_STRING[];
extern const char SET_MIB_READ_ENABLE_STRING[];
extern const char ADDRESS_STRING[];
extern const char ADDRESS_LIST_STRING[];
extern const char READ_N503_4_5_6_7_STRING[];
extern const char BYTE_SEQUENCE_STRING[];
extern const char VALUE_STRING[];
extern const char CHECK_MIB_READ_ENABLE_STRING[];
extern const char READ_ATTEMPTS_STRING[];
extern const char COUNTER_OVERFLOW_STRING[];
extern const char YES_STRING[];
extern const char NO_STRING[];
extern const char ETHERNET_ENABLED_STRING[];
extern const char SECONDARY_ENABLED_ON_STARTUP_STRING[];

extern const char PORT_1_STRING[];
extern const char PORT_2_STRING[];
extern const char PORT_3_STRING[];
extern const char PORT_4_STRING[];
extern const char PORT_5_STRING[];
extern const char PORT_6_STRING[];
extern const char PORT_7_STRING[];
extern const char PORT_8_STRING[];
extern const char PORT_LAN_STRING[];
extern const char PORT_RGMII_STRING[];
extern const char PORT_UNKNOWN_STRING[];

extern const char PORT_SHORT_STRING[];
extern const char CONTROL_FROM_STRING[];
extern const char CONTROL_TO_STRING[];
extern const char STATUS_FROM_STRING[];
extern const char STATUS_TO_STRING[];
extern const char EVENT_ID_STRING[];
extern const char TICK_STRING[];
extern const char COMMA_ONLY_STRING[];
extern const char CC_IN_STATUS_STRING[];
extern const char EXPECTED_VALUE_STRING[];
extern const char ACTUAL_VALUE_STRING[];

extern const char PORT_NO_QUOTES_STRING[];
extern const char CONFIG_STRING[];
extern const char CONTROL_STRING[];
extern const char STATUS_STRING[];
extern const char LINK_STRING[];
extern const char LINK_STATUS_UP_STRING[];
extern const char LINK_STATUS_DOWN_STRING[];
extern const char PORT_CONFIG_POWERED_UP_STRING[];
extern const char PORT_CONFIG_POWERED_DOWN_STRING[];
extern const char PORT_COMMAND_POWER_UP_STRING[];
extern const char PORT_COMMAND_POWER_DOWN_STRING[];
extern const char UP_STRING[];
extern const char DOWN_STRING[];

extern const char PORT_STRING[];
extern const char PRIMARY_PORT_CONFIGURED_STRING[];
extern const char SECONDARY_PORT_CONFIGURED_STRING[];
extern const char PORT_CONFIG_STRING[];
extern const char PORT_KEY_STRING[];
extern const char PORT_RANK_STRING[];
extern const char REG_ADDR_STRING[];
extern const char CONTROL_BEFORE_STRING[];
extern const char CONTROL_AFTER_STRING[];
extern const char STATUS_BEFORE_STRING[];
extern const char STATUS_AFTER_STRING[];
extern const char EXPECTED_SAME_AS_ACTUAL_STRING[];
extern const char PORT_LIST_STRING[];

extern const char CC_INFO_STRING[];
extern const char CC_STATUS_STRING[];
extern const char CC_OUTPUT_STRING[];
extern const char CC_INPUT_STRING[];
extern const char CC_OPEN_STRING[];
extern const char CC_CLOSE_STRING[];
extern const char CC_NUMBER_STRING[];
extern const char INVALID_PARAMETERS_STRING[];
extern const char INVALID_MAC_STRING[];
extern const char INVALID_COMMAND_STRING[];
extern const char NOW_OPEN_STRING[];
extern const char NOW_CLOSE_STRING[];
extern const char WAS_ALREADY_OPEN_STRING[];
extern const char WAS_ALREADY_CLOSE_STRING[];

extern const char SPEED_STRING[];
extern const char FAN_SPEED_MAX_STRING[];
extern const char FAN_SPEED_LOW_STRING[];
extern const char FAN_SPEED_OFF_STRING[];
extern const char TEMPERATURE_STRING[];
extern const char POE_TEMPERATURE_STRING[];

extern const char PORT_DATA_STRING[];
extern const char POWER_STRING[];
extern const char PAC_STRING[];
extern const char AUTO_CLASS_STRING[];
extern const char AC_SUPPORT_STRING[];

extern const char TEMPERATURE_CHANGE_STRING[];
extern const char RAW_REGISTER_STRING[] ;
extern const char FAN_SPEED_CHANGE_STRING[];
extern const char CURRENT_FAN_SPEED_STRING[];
extern const char TEMP_SPEED_SETTINGS_STRING[];
extern const char TEMP_FAN_INFO_STRING[];
extern const char POE_INFO_STRING[];
extern const char LOW_TEMP_SPEED_SETTING_STRING[];
extern const char MAX_TEMP_SPEED_SETTING_STRING[];
extern const char POE_PORT_ENABLE_STRING[];
extern const char POE_OPERATING_MODE_STRING[];
extern const char POE_OPERATING_MODE_ERROR_STRING[];
extern const char EXPECTED_STRING[];
extern const char READ_BACK_STRING[];

extern const char COMMAND_REGISTER_STRING[];
extern const char LAST_OPERATING_MODE_VALUE_STRING[];
extern const char LAST_POWER_ENABLE_VALUE_STRING[];
extern const char FORMULA_STRING[];

extern const char POE_AUTO_CLASS_1_STRING[];
extern const char POE_AUTO_CLASS_2_STRING[];
extern const char POE_AUTO_CLASS_3_STRING[];
extern const char POE_AUTO_CLASS_4_STRING[];
extern const char POE_AUTO_CLASS_NOT_POE_STRING[];
extern const char POE_AUTO_CLASS_UNKNOWN_STRING[];
extern const char POE_AUTO_CLASS_STRING[];
extern const char POE_AUTO_CLASS_LIMIT_STRING[];
extern const char POE_AUTO_CLASS_STATUS_REGISTER_STRING[];

extern const char MHZ_25_CLOCK_OUTPUT_STRING[];
extern const char DISABLE_STRING[];
extern const char ENABLE_STRING[];
extern const char REG_VALUE_STRING[];
extern const char POE_PORT_POWER_STRING[];
extern const char POE_PORT_AUTO_CLASS_STRING[];
extern const char POE_PORT_PAC_STRING[];
extern const char POE_AUTO_CLASS_STATUS_STRING[];
extern const char POE_POWER_THRESHOLD_EXCEEDED_STRING[];
extern const char POE_LINK_DOWN_WITH_POWER_STRING[];
extern const char CLASS_CONFIG_STRING[];
extern const char SUPPORTED_STRING[];
extern const char NOT_SUPPORTED_STRING[];

extern const char CLI_ARG_POE_EVENT_EVENT_STRING[];
extern const char CLI_ARG_POE_EVENT_POWER_STRING[];
extern const char CLI_ARG_POE_EVENT_DETECTION_STRING[];
extern const char CLI_ARG_POE_EVENT_FAULT_STRING[];
extern const char CLI_ARG_POE_EVENT_START_STRING[];
extern const char CLI_ARG_POE_EVENT_SUPPLY_STRING[];
extern const char CLI_ARG_POE_EVENT_ALL_STRING[];
extern const char CLI_POE_SUPPLY_FAULT_EVENTS_STRING[];
extern const char CLI_POE_NO_SUPPLY_NO_FAULTS_STRING[];
extern const char CLI_POE_I2C_READ_FAILED_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_TSD_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_VDUV_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_VDWRN_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_VPUV_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_OSSE_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_RAMFLT_STRING[];
extern const char POE_SUPPLY_FAULT_EVENT_STRING[];
extern const char POE_PORT_EVENT_DETECTED_STRING[];

extern const char EVENT_NAME_STRING[];
extern const char EVENT_INFO_STRING[];

extern const char POWER_GOOD_STATUS_STRING[];
extern const char POWER_ENABLE_STATUS_STRING[];
extern const char DETECTION_CHANGE_OF_CLASS_STRING[];
extern const char DETECTION_CHANGE_IN_DETECTION_STRING[];
extern const char FAULT_DISCONNECT_STRING[];
extern const char FAULT_OVERLOAD_STRING[];
extern const char START_ILIM_FAULT_STRING[];
extern const char START_CLASS_DETECT_STRING[];
extern const char TICK_INFO_STRING[];

extern const char POWER_EVENT_STRING[];
extern const char DETECTION_EVENT_STRING[];
extern const char FAULT_EVENT_STRING[];
extern const char START_EVENT_STRING[];
extern const char FROM_VALUE_STRING[];
extern const char TO_VALUE_STRING[];

extern const char EVENT_REGISTERS_STRING[];
extern const char DETECTION_STRING[];
extern const char FAULT_STRING[];
extern const char START_STRING[];
extern const char SUPPLY_FAULT_STRING[];

extern const char CLI_ARG_COMMAND_STRING[];
extern const char CLI_ARG_UPGRADE_START_STRING[];
extern const char CLI_ARG_UPGRADE_DATA_STRING[];
extern const char CLI_ARG_UPGRADE_END_STRING[];
extern const char CLI_ARG_UPGRADE_COMMIT_STRING[];
extern const char CLI_ARG_UPGRADE_CANCEL_STRING[];
extern const char CLI_ARG_UPGRADE_STATS_STRING[];
extern const char CLI_ARG_DATA_EQUAL_STRING[];
extern const char CLI_COMMAND_EQUAL_NOT_FOUND_STRING[];
extern const char CLI_DATA_FIELD_TOO_LONG_STRING[];
extern const char CLI_DATA_EQUAL_NOT_FOUND_STRING[];
extern const char CLI_INVALID_UPGRADE_COMMAND_STRING[];

extern const char  UPGRADE_STATE_MACHINE_STRING[];
extern const char  UPGRADE_PACKET_SIZE_STRING[];
extern const char  SM_INVALID_STATE_STRING[];
extern const char  SM_IDLE_STATE_STRING[];
extern const char  SM_WAIT_NEXT_DATA_PACKET_STATE_STRING[];
extern const char  SM_WAIT_END_TRANSFER_STATE_STRING[];
extern const char  SM_WAIT_APP_FINAL_COMMIT_STATE_STRING[];
extern const char  START_ACK_STRING[];
extern const char  RECEIVE_ACK_STRING[];
extern const char  END_ACK_STRING[];
extern const char  FINAL_COMMIT_ACK_STRING[];
extern const char  CANCEL_DATA_PKT_STATE_ACK_STRING[];
extern const char  CANCEL_WAIT_END_TRANSFER_STATE_ACK_STRING[];
extern const char  CANCEL_WAIT_COMMIT_STATE_ACK_STRING[];
extern const char  CANCEL_IDLE_STATE_ACK_STRING[];
extern const char  STATS_ACK_STRING[];
extern const char  AMU_CODING_ERROR_STRING[];
extern const char  XFER_COMPONENT_HEX_CONVERT_FAIL_NACK_STRING[];
extern const char  XFER_COMPONENT_CHECKSUM_FAIL_NACK_STRING[];
extern const char  XFER_COMPONENT_EVEN_LENGTH_NACK_STRING[];
extern const char  XFER_COMPONENT_MISSING_COLON_NACK_STRING[];
extern const char  INVAL_STATE_MACHINE_COMMAND_NACK_STRING[];
extern const char  INVAL_CMD_IDLE_STATE_NACK_STRING[];
extern const char  INVAL_CMD_NEXT_DATA_PKT_STATE_NACK_STRING[];
extern const char  INVAL_CMD_END_TRANSFER_STATE_NACK_STRING[];
extern const char  XFER_PACKET_COMPONENT_TOO_LONG_NACK_STRING[];
extern const char  SEGMENT_ADDRESS_INVAL_LENGTH_NACK_STRING[];
extern const char  SEG_ADDR_BUT_HOLD_BUFF_NOT_EMPTY_NACK_STRING[];
extern const char  SEG_ADDR_INVALID_LENGTH_NACK_STRING[];
extern const char  DATA_START_INVALID_LENGTH_NACK_STRING[];
extern const char  DATA_START_ADDRESS_NOT_ZERO_NACK_STRING[];
extern const char  EOF_RECORD_INVALID_STRUCTURE_NACK_STRING[];
extern const char  RELATIVE_ADDR_OUT_OF_SEQUENCE_NACK_STRING[]; 
extern const char  DATA_RECORD_NOT_WHOLE_LONGS_NACK_STRING[];
extern const char  INTERIM_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[];
extern const char  LAST_HOLDING_BLOCK_COMMIT_FAIL_NACK_STRING[];
extern const char  EXPECTED_EXT_SEG_ADDR_RECORD_NACK_STRING[];
extern const char  EXPECTED_END_OF_FILE_RECORD_NACK_STRING[];
extern const char  EXPECTED_DATA_RECORD_NACK_STRING[];
extern const char  UNEXPECTED_SLA_RECORD_NACK_STRING[];
extern const char  START_EXT_SEG_ADDR_INVALID_NACK_STRING[];
extern const char  FLASH_APP_SIGNATURE_ERASE_FAIL_NACK_STRING[];
extern const char  INVALID_COMMAND_IN_COMMIT_STATE_NACK_STRING[];
extern const char  COMMIT_FAIL_NACK_STRING[];
extern const char  RELATIVE_ADDR_NOT_ZERO_NACK_STRING[];
extern const char  READ_FLASH_TO_HOLDING_FAIL_NACK_STRING[];
extern const char  FLASH_ADDRESS_LOW_ZONE_NACK_STRING[];
extern const char  END_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[];
extern const char  START_ADDRESS_WITHIN_BOOT_ZONE_NACK_STRING[];
extern const char  WRITE_HOLD_TO_FLASH_GENERIC_FAIL_NACK_STRING[];
extern const char  HEX_CHARACTER_NOT_HEX_NACK_STRING[];

extern const char  SAME70_RESET_REASON_STRING[];
extern const char  RESET_INFO_STRING[];
extern const char  RESET_REASON_UNKNOWN_STRING[];
extern const char  RESET_REASON_GENERAL_STRING[];
extern const char  RESET_REASON_BACKUP_STRING[];
extern const char  RESET_REASON_WATCHDOG_STRING[];
extern const char  RESET_REASON_SOFTWARE_STRING[];
extern const char  RESET_REASON_NRST_LOW_STRING[];

extern const char  CRAFT_STRING_TO_PROCESS_STRING[];


#endif   /* _APPARENT_STRINGS_H_ */
